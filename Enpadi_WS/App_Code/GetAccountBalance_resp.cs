﻿public class GetAccountBalance_resp
{
    public string TerminalID { get; set; }
    public string MerchantID { get; set; }
    public string ResponseCode { get; set; }
    public string ResponseMessage { get; set; }
    public string AccountNumber { get; set; }
    public string CardHolder { get; set; }
    public decimal Balance { get; set; }
    public string CurrencyCode { get; set; }
    public string CurrencySymbol { get; set; }
}