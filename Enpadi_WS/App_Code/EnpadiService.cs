﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;

/// <summary>
/// Summary description for EnpadiService
/// </summary>
[WebService(Namespace = "http://enpadi.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class EnpadiService : System.Web.Services.WebService {

    public EnpadiHeader eHeader;

    [WebMethod]
    public Version_resp Version() {
        Version_resp respuesta = new Version_resp() { Version = "EnpadiServise Version 1.0 @ 2016 All Rights Reserved " };
        return respuesta;
    }

    [WebMethod]
    [SoapHeader("eHeader")]
    public SetAccountDeposit_resp SetAccountDeposit(SetAccountDeposit_req request)
    {
        SetAccountDeposit_resp response = new SetAccountDeposit_resp();
        return response;
    }

    [WebMethod]
    [SoapHeader("eHeader")]
    public GetAccountBalance_resp GetAccountBalance(GetAccountBalance_req request)
    {
        GetAccountBalance_resp response = new GetAccountBalance_resp();
        return response;
    }

    [WebMethod]
    [SoapHeader("eHeader")]
    public GetCategoryList_resp GetCategoryList(GetCategoryList_req request)
    {
        GetCategoryList_resp response = new GetCategoryList_resp();
        return response;
    }

    [WebMethod]
    [SoapHeader("eHeader")]
    public GetCarrierList_resp GetCarrierList(GetCategoryList_req request)
    {
        GetCarrierList_resp response = new GetCarrierList_resp();
        return response;
    }

    [WebMethod]
    [SoapHeader("eHeader")]
    public GetProductList_resp GetProductList (GetProductList_req request)
    {
        GetProductList_resp response = new GetProductList_resp();
        return response;
    }

    [WebMethod]
    [SoapHeader("eHeader")]
    public ServiceReference_resp ServiceReference(ServiceReference_req request)
    {
        ServiceReference_resp response = new ServiceReference_resp();
        return response;
    }

    [WebMethod]
    [SoapHeader("eHeader")]
    public ServicePayment_resp ServicePayment(ServicePayment_req request)
    {
        ServicePayment_resp response = new ServicePayment_resp();
        return response;
    }

}
