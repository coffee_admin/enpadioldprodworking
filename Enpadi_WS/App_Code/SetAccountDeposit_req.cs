﻿public class SetAccountDeposit_req
{
    public string TerminalID { get; set; }
    public sbyte TransactionID { get; set; }
    public string AccountNumber { get; set; }
    public string AmountUSD { get; set; }
    public string ExchangeRate { get; set; }
    public string AmountMXN { get; set; }
}