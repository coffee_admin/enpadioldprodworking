﻿public class ServicePayment_resp
{
    public string TerminalID { get; set; }
    public string MerchantID { get; set; }
    public string TransactionID { get; set; }
    public string Reference { get; set; }
    public string AccountNumber { get; set; }
    public string Amount { get; set; }
    public string CurrencyCode { get; set; }
    public string CurrencySymbol { get; set; }
    public string AuthorizationCode { get; set; }
    public decimal Balance { get; set; }
    public string Carrier { get; set; }
    public string Text1 { get; set; }
    public string Text2 { get; set; }
    public string Text3 { get; set; }
    public string Date { get; set; }
    public string ResponseCode { get; set; }
    public string ResponseMessage { get; set; }
}