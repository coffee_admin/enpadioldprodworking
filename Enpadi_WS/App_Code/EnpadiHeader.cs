﻿using System.Web.Services.Protocols;
public class EnpadiHeader : SoapHeader
{
    public string Username { get; set; }
    public string Password { get; set; }
}