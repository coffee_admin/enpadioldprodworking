﻿public class ServiceReference_req
{
    public string TerminalID { get; set; }
    public string MerchantID { get; set; }
    public string TransactionID { get; set; }
    public string Reference { get; set; }
    public string AccountNumber { get; set; }
    public string Amount { get; set; }
    public string CurrencyCode { get; set; }
    public string CurrencySymbol { get; set; }
    public string Fee1 { get; set; }
    public string Fee2 { get; set; }
    public string Fee3 { get; set; }
}