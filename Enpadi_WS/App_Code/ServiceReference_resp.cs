﻿public class ServiceReference_resp
{
    public string TerminalID { get; set; }
    public string TransactionID { get; set; }
    public string SKU { get; set; }
    public string Reference { get; set; }
}