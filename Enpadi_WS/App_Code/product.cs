﻿public class product
{
    public string SKU { get; set; }
    public string Description { get; set; }
    public string Carrier { get; set; }
    public string Category { get; set; }
    public string Amount { get; set; }
    public string CurrencyCode { get; set; }
    public string CurrencySymbol { get; set; }
    public string DiscountRate { get; set; }
}