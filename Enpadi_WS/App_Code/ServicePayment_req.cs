﻿public class ServicePayment_req
{
    public string TerminalID { get; set; }
    public string TransactionID { get; set; }
    public string SKU { get; set; }
    public string Reference { get; set; }
    public string Amount { get; set; }
    public string CurrencyCode { get; set; }
    public string AccountNumber { get; set; }
    public string PIN { get; set; }
}