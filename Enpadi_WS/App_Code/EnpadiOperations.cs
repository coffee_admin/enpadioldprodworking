﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EnpadiOperations
/// </summary>
public class EnpadiOperations
{
    
    public static string AccountDeposit()
    {
        return "";
    }

    public static DataSet ServiceCategories()
    {
        DataSet ds = new DataSet();
        using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbConexion_LOCAL"].ConnectionString))
        {
            string strSQL = "SELECT DISTINCT Tipo As Category FROM Servicios WHERE FechaBaja IS NULL AND Activo = 1";
            SqlDataAdapter categorias = new SqlDataAdapter();
            categorias.SelectCommand = new SqlCommand(strSQL, sqlConn);
            categorias.Fill(ds, "Categorias");
        }
        return ds;
    }

    public static DataSet ServiceProducts(string Category)
    {
        DataSet ds = new DataSet();
        using(SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbConexion_LOCAL"].ConnectionString))
        {
            string strSQL = " SELECT SKU, Nombre As Name, Descripcion As Description, ReferenciaLongitud As Lengthh, Instrucciones As Directions, Compañia As Company "
                          + " FROM Servicios WHERE FechaBaja IS NULL AND Activo = 1 AND Tipo = '"+ Category + "'"
                          + " ORDER BY Company, Name ";
            SqlDataAdapter servicios = new SqlDataAdapter();
            servicios.SelectCommand = new SqlCommand(strSQL, sqlConn);
            servicios.Fill(ds, "Servicios");
        }
        return ds;
    }

    public static string ServiceReference()
    {
        return "";
    }

    public static string ServicePayment()
    {
        return "";
    }

}