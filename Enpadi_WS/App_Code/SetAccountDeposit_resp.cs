﻿public class SetAccountDeposit_resp
{
    public string TerminalID { get; set; }
    public string MerchantID { get; set; }
    public string ResponseCode { get; set; }
    public string ResponseMessage { get; set; }
    public string AccountNumber { get; set; }
    public string CardHolder { get; set; }
    public decimal Amount { get; set; }
    public string AuthorizationCode { get; set; }
    public decimal Balance { get; set; }
}