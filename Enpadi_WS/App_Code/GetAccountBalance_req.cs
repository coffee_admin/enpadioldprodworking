﻿public class GetAccountBalance_req
{
    public string TerminalID { get; set; }
    public string AccountNumber { get; set; }
}