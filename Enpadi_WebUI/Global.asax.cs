﻿using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AutoMapper;
using Enpadi_WebUI.App_Start;
using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Enpadi_WebUI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            try {
                GlobalConfiguration.Configure(WebApiConfig.Register);
                Mapper.Initialize(c => c.AddProfile<MappingProfile>());
                AreaRegistration.RegisterAllAreas();
                FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
                RouteConfig.RegisterRoutes(RouteTable.Routes);
                BundleConfig.RegisterBundles(BundleTable.Bundles);
                AntiForgeryConfig.SuppressXFrameOptionsHeader = true;
            }
            catch (Exception e) {
                String error = e.Message;

            }
        }
    }
}
