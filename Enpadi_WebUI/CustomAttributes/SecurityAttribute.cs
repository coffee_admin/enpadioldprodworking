﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace EnpadiSpecialAttributes
{
    public class SecurityAttribute : AuthorizeAttribute
    {
        // Set default Unauthorized Page Url here
        private string _controllerToRedirect = "website";

        private string _actionToRedirect = "index";

        public string ControllerToRedirect
        {
            get { return _controllerToRedirect; }
            set { _controllerToRedirect = value; }
        }

        public string ActionToRedirect
        {
            get { return _actionToRedirect; }
            set { _actionToRedirect = value; }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
           //if not authenticated
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                filterContext.Result = new RedirectToRouteResult(new
                    RouteValueDictionary(new { controller = _controllerToRedirect, Action = _actionToRedirect }));
            }

        }
    }
}