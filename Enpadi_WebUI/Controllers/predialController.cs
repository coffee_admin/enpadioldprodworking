﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Enpadi_WebUI.Models;
using PagedList;
using Microsoft.AspNet.Identity;

namespace Enpadi_WebUI.Controllers
{
    public class predialController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: predial
        public ActionResult Index(string skin, string sortOrder, string currentFilter, string searchString, int? page)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();


            ViewBag.ExpedienteSort = sortOrder == "expediente" ? "expediente_desc" : "expediente";
            ViewBag.CalleSort = sortOrder == "calle" ? "calle_desc" : "calle";
            ViewBag.NombreSort = sortOrder == "nombre" ? "nombre_desc" : "nombre";
            ViewBag.PaternoSort = sortOrder == "paterno" ? "paterno_desc" : "paterno";

            var predials = db.predials.Include(p => p.ciudad).Include(p => p.estado).Include(p => p.pais);

            if (!String.IsNullOrEmpty(searchString))
            {
                predials = predials.Where(s => s.expedienteNo.Contains(searchString) || s.calle.Contains(searchString) || s.colonia.Contains(searchString) || s.nombre.Contains(searchString) || s.aPaterno.Contains(searchString) || s.aMaterno.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "expediente":
                    predials = predials.OrderBy(s => s.expedienteNo);
                    break;
                case "expediente_desc":
                    predials = predials.OrderByDescending(s => s.expedienteNo);
                    break;
                case "calle":
                    predials = predials.OrderBy(s => s.calle);
                    break;
                case "calle_desc":
                    predials = predials.OrderByDescending(s => s.calle);
                    break;
                case "nombre":
                    predials = predials.OrderBy(s => s.nombre);
                    break;
                case "nombre_desc":
                    predials = predials.OrderByDescending(s => s.nombre);
                    break;
                case "paterno":
                    predials = predials.OrderBy(s => s.aPaterno);
                    break;
                case "paterno_desc":
                    predials = predials.OrderByDescending(s => s.aPaterno);
                    break;
                default:
                    predials = predials.OrderBy(s => s.expedienteNo);
                    ViewBag.ExpedienteSort = "expediente";
                    break;
            }

            int pageSize = 50;
            int pageNumber = (page ?? 1);
            
            return View(predials.ToPagedList(pageNumber, pageSize));
        }

        // GET: predial/Details/5
        public ActionResult Details(string skin, int? id)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewModelPredial vm = new ViewModelPredial();

            predial predial = db.predials.Find(id);
            if (predial == null)
            {
                return HttpNotFound();
            }

            vm.predial = predial;

            var predialMovimientoes = db.predialMovimientoes.Where(p => p.predialID == id).Include(p => p.predialConcepto);
            vm.movimientos = predialMovimientoes.ToList();

            strSQL = String.Format("SELECT COALESCE(SUM(cargoTotal - abonoTotal),0) As Total FROM PredialMovimientos WHERE fechaBaja IS NULL AND predialID = {0}", id);
            IEnumerable<total> total = db.Database.SqlQuery<total>(strSQL);
            vm.saldoTotal = total.FirstOrDefault().Total;

            return View(vm);
        }

        // GET: predial/Create
        public ActionResult Create(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            ViewBag.cdID = new SelectList(db.ciudades, "cdID", "nombre");
            ViewBag.edoID = new SelectList(db.estados, "edoID", "nombre");
            ViewBag.paisID = new SelectList(db.paises, "paisID", "nombre");
            return View();
        }

        // POST: predial/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "predialID,socioID,expedienteNo,calle,numExterior,numInterior,referencia,colonia,cdID,edoID,paisID,cp,latitud,longitud,nombre,aPaterno,aMaterno,direccion,telefono,celular,email,fechaAlta,fechaActual")] predial predial, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                db.predials.Add(predial);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.cdID = new SelectList(db.ciudades, "cdID", "nombre", predial.cdID);
            ViewBag.edoID = new SelectList(db.estados, "edoID", "nombre", predial.edoID);
            ViewBag.paisID = new SelectList(db.paises, "paisID", "nombre", predial.paisID);
            return View(predial);
        }

        // GET: predial/Edit/5
        public ActionResult Edit(string skin, int? id)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            predial predial = db.predials.Find(id);
            if (predial == null)
            {
                return HttpNotFound();
            }
            ViewBag.cdID = new SelectList(db.ciudades, "cdID", "nombre", predial.cdID);
            ViewBag.edoID = new SelectList(db.estados, "edoID", "nombre", predial.edoID);
            ViewBag.paisID = new SelectList(db.paises, "paisID", "nombre", predial.paisID);
            return View(predial);
        }

        // POST: predial/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "predialID,socioID,expedienteNo,calle,numExterior,numInterior,referencia,colonia,cdID,edoID,paisID,cp,latitud,longitud,zoom,nombre,aPaterno,aMaterno,direccion,telefono,celular,email,fechaAlta,fechaActual")] predial predial, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                db.Entry(predial).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.cdID = new SelectList(db.ciudades, "cdID", "nombre", predial.cdID);
            ViewBag.edoID = new SelectList(db.estados, "edoID", "nombre", predial.edoID);
            ViewBag.paisID = new SelectList(db.paises, "paisID", "nombre", predial.paisID);
            return View(predial);
        }

        // GET: predial/Delete/5
        public ActionResult Delete(string skin, int? id)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            predial predial = db.predials.Find(id);
            if (predial == null)
            {
                return HttpNotFound();
            }
            return View(predial);
        }

        // POST: predial/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string skin, int id)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            predial predial = db.predials.Find(id);
            db.predials.Remove(predial);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Movimientos(string skin, int? id)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            ViewModelPredial vm = new ViewModelPredial();

            predial predial = db.predials.Find(id);
            vm.predial = predial;

            var predialMovimientoes = db.predialMovimientoes.Where(p => p.predialID == id).Include(p => p.predialConcepto);
            vm.movimientos = predialMovimientoes.ToList();

            strSQL = String.Format("SELECT COALESCE(SUM(cargoTotal - abonoTotal),0) As Total FROM PredialMovimientos WHERE fechaBaja IS NULL AND predialID = {0}", id);
            IEnumerable<total> total = db.Database.SqlQuery<total>(strSQL);
            vm.saldoTotal = total.FirstOrDefault().Total;
            
            return View(vm);
        }

        [HttpPost]
        public ActionResult Movimientos(string skin, int? predialID, decimal[] conceptos, decimal saldoTotal)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            strSQL = String.Format("SELECT expedienteNo As 'desc' FROM Predial WHERE predialID = {0}", predialID);
            string sExpediente = db.Database.SqlQuery<descripcion>(strSQL).FirstOrDefault().desc;

            var predialMovimientoes = db.predialMovimientoes.Where(p => p.predialID == predialID).Include(p => p.predialConcepto);

            foreach(decimal val in conceptos)
            {
                foreach(predialMovimiento mov in predialMovimientoes.ToList())
                {
                    if(mov.cargoTotal == val)
                    {
                        predialMovimiento predialMovimiento = db.predialMovimientoes.Find(mov.movID);
                        predialMovimiento.abono1 = val;
                        predialMovimiento.abonoTotal = val;
                        predialMovimiento.estatusID = 1;
                        db.Entry(predialMovimiento).State = EntityState.Modified;
                        db.SaveChanges();

                        trans t = new trans { estatusID = 1, origenID = 1, tipoID = 1, modoID = 1, socioID = 7, tarjetaID = usuario.tarjetaID, terminalID = 1, monedaID = 484, Concepto = String.Format("{0} {1}", sExpediente, mov.descripcion), transaccion = mov.transaccion, descripcion= mov.descripcion, referencia= mov.referencia, aprobacion = "239894", monto = val, abono1 = val, abonoTotal = val, geoCountryCode = 0, geoCountryName = String.Empty, geoRegion = String.Empty, geoCity = String.Empty, geoPostalCode = String.Empty };
                        db.transacciones.Add(t);
                        db.SaveChanges();
                    }
                }
            }

            TempData["conceptos"] = conceptos;

            return RedirectToAction("Recibo", new { predialID = predialID, saldoTotal = saldoTotal });
        }

        public ActionResult Recibo(string skin, int? predialID, decimal[] conceptos, decimal saldoTotal)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            ViewModelPredial vista = new ViewModelPredial();

            vista.folioRecibo = "3435";
            vista.fecha = DateTime.Now;

            //strSQL = String.Format("SELECT * FROM Predial WHERE predialID = {0}", predialID);
            //IEnumerable<predial> datos = db.Database.SqlQuery<predial>(strSQL);
            //vista.predial = datos.ToList().FirstOrDefault();

            predial predial = db.predials.Find(predialID);
            vista.predial = predial;

            var predialMovimientoes = db.predialMovimientoes.Where(p => p.predialID == predialID).Include(p => p.predialConcepto);
            
            List<predialMovimiento> movs = new List<predialMovimiento>();

            conceptos = (decimal[])TempData["conceptos"];

            foreach (decimal val in conceptos)
            {
                foreach (predialMovimiento mov in predialMovimientoes.ToList())
                {
                    if (mov.cargoTotal == val)
                    {
                        movs.Add(mov);
                    }
                }
            }

            vista.movimientos = movs;
            vista.saldoTotal = saldoTotal;
            vista.folioRifa = "158496216";

            return View(vista);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
