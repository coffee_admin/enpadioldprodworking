﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Enpadi_WebUI.Models;

namespace Enpadi_WebUI.Controllers
{
    public class ProductosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Productos
        public ActionResult Index(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View(db.productos.ToList());
        }

        // GET: Productos/Details/5
        public ActionResult Details(int? id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            ViewModelProducto info = new ViewModelProducto();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            info.producto = db.productos.Find(id);

            if (info.producto == null)
            {
                return HttpNotFound();
            }

            strSQL = " SELECT t.tarjetaID, t.productoID, m.codigo, t.tcNum, t.tcMes, t.tcAnio, t.tcDigitos, t.tcAsignada, t.tcActiva "
                   + " FROM Tarjetas t "
                   + " LEFT JOIN Monedas m ON t.monedaID = m.monedaID "
                   + " WHERE t.productoID = 1 AND t.fechaBaja IS NULL "
                   + " ORDER BY t.tarjetaID ";
            IEnumerable<tarjetasListado> tarjetas = db.Database.SqlQuery<tarjetasListado>(strSQL);
            info.tarjetas = tarjetas.ToList();

            return View(info);
        }

        // GET: Productos/Create
        public ActionResult Create(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        // POST: Productos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "productoID,nombre,descripcion,bin,identificador,inicial,final,activo")] producto producto, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                db.productos.Add(producto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(producto);
        }

        // GET: Productos/Edit/5
        public ActionResult Edit(int? id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            producto producto = db.productos.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // POST: Productos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "productoID,nombre,descripcion,bin,identificador,inicial,final,activo")] producto producto, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                db.Entry(producto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(producto);
        }

        // GET: Productos/Delete/5
        public ActionResult Delete(int? id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            producto producto = db.productos.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // POST: Productos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            producto producto = db.productos.Find(id);
            db.productos.Remove(producto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET : Productos/Accounts/5
        public ActionResult Accounts(int? id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            ViewModelProducto info = new ViewModelProducto();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            info.producto = db.productos.Find(id);

            if(info.producto == null)
            {
                return HttpNotFound();
            }

            strSQL = " SELECT (final - inicial) As total FROM Productos WHERE productoID = 1 ";
            IEnumerable<contador> rango = db.Database.SqlQuery<contador>(strSQL);
            info.Rango = rango.ToList().FirstOrDefault().total;

            strSQL = " SELECT COUNT(*) As total FROM Tarjetas WHERE productoID = 1 AND fechaBaja IS NULL ";
            IEnumerable<contador> configuradas = db.Database.SqlQuery<contador>(strSQL);
            info.Configuradas = configuradas.ToList().FirstOrDefault().total;

            strSQL = " SELECT COUNT(*) As total FROM Tarjetas WHERE productoID = 1 AND fechaBaja IS NULL AND tcAsignada = 1 ";
            IEnumerable<contador> asignadas = db.Database.SqlQuery<contador>(strSQL);
            info.Asignadas = asignadas.ToList().FirstOrDefault().total;

            info.Disponibles = info.Configuradas - info.Asignadas;
            info.Pendientes = info.Rango - info.Configuradas;

            return View(info);
        }

        [HttpPost]
        public ActionResult Accounts(int? id, int? nothing, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            producto p = new producto();

            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            p = db.productos.Find(id);

            int inicial = p.inicial;
            int final = p.final;
            
            for (int i = inicial; i <= final; i++ )
            {
                tarjetas newTarjeta = new tarjetas();
                newTarjeta.productoID = p.productoID;
                newTarjeta.monedaID = 484;
                string tc = string.Format("{0:D6}{1:D4}{2:D5}", p.bin, p.identificador, i);
                string validador = GetLuhnCheckDigit(tc);
                newTarjeta.tcNum = string.Format("{0:D6}{1:D4}{2:D5}{3}", p.bin, p.identificador, i, validador);
                newTarjeta.tcMD5 = newTarjeta.tcNum;
                Random r = new Random(DateTime.Now.Millisecond);
                newTarjeta.tcMes = string.Format("{0:D2}", r.Next(1,12));
                newTarjeta.tcAnio = string.Format("{0:D4}", r.Next(2017, 2024));
                newTarjeta.tcDigitos = string.Format("{0:D3}", r.Next(1,999));
                newTarjeta.tcNIP = string.Format("{0:D4}", r.Next(1,9999));
                newTarjeta.tcAsignada = false;
                newTarjeta.tcActiva = true;
                newTarjeta.limActivacion = 200;
                newTarjeta.limDepMin = 200;
                newTarjeta.limDepMax = 2000;
                newTarjeta.limDepDia = 2000;
                newTarjeta.limDepSem = 5000;
                newTarjeta.limDepMes = 10000;
                newTarjeta.limRetMin = 500;
                newTarjeta.limRetMax = 5000;
                newTarjeta.limRetDia = 5000;
                newTarjeta.limRetSem = 10000;
                newTarjeta.limRetMes = 10000;
                newTarjeta.limTransMin = 200;
                newTarjeta.limTransMax = 2000;
                newTarjeta.limTransDia = 5000;
                newTarjeta.limTransSem = 5000;
                newTarjeta.limTransMes = 10000;
                newTarjeta.paisID = 0;
                newTarjeta.edoID = 0;
                newTarjeta.cdID = 0;
                db.tarjetas.Add(newTarjeta);
            }

            db.SaveChanges();

            return RedirectToAction("Index", "Productos");
        }

        private static string GetLuhnCheckDigit(string number)
        {
            var sum = 0;
            var alt = true;
            var digits = number.ToCharArray();
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                var curDigit = (digits[i] - 48);
                if (alt)
                {
                    curDigit *= 2;
                    if (curDigit > 9)
                        curDigit -= 9;
                }
                sum += curDigit;
                alt = !alt;
            }
            if ((sum % 10) == 0)
            {
                return "0";
            }
            return (10 - (sum % 10)).ToString();
        }  

    }
}
