﻿using Enpadi_WebUI.Models;
using Enpadi_WebUI.Properties;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Xml;
using System.Xml.Serialization;
using EnpadiSpecialAttributes;

namespace Enpadi_WebUI.Controllers
{
    [Security(ControllerToRedirect = "enpadiadmin", ActionToRedirect = "login")]
    public class EnpadiAdminController : Controller
    {

        #region constructors and inicializers

        private static bool bModoProduccion = Convert.ToBoolean(ConfigurationManager.AppSettings["ModoProduccion"]);

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        //private ApplicationDbContext db = new ApplicationDbContext();

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        #region constructors

        #endregion constructors and inicializers

        public EnpadiAdminController()
        {
        }

        public EnpadiAdminController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }
        #endregion constructors

        #region views

        public ActionResult UserSettings()
        {

            return View();
        }

        public ActionResult KioskoReport()
        {

            return View();
        }

        // GET: EnpadiAdmin
        public ActionResult Index()
        {
            String userId = User.Identity.GetUserId();
            if (userId == null)
            {
                return RedirectToAction("LogIn");
            }
            else if (Guid.Parse(userId) != ApplicationUser.Key.AdminUserId)
            {
                AuthenticationManager.SignOut();
                return RedirectToAction("LogIn");
            }

            return View();
        }

        public ActionResult Reports()
        {
            String userId = User.Identity.GetUserId();
            if (userId == null)
            {
                return RedirectToAction("LogIn");
            }
            else if (Guid.Parse(userId) != ApplicationUser.Key.AdminUserId)
            {
                AuthenticationManager.SignOut();
                return RedirectToAction("LogIn");
            }

            return View();
        }

        public ActionResult Partner()
        {
            String userId = User.Identity.GetUserId();
            if (userId == null)
            {
                return RedirectToAction("LogIn");
            }
            else if (Guid.Parse(userId) != ApplicationUser.Key.AdminUserId)
            {
                AuthenticationManager.SignOut();
                return RedirectToAction("LogIn");
            }

            return View();
        }

        // GET: EnpadiAdmin
        [AllowAnonymous]
        public ActionResult LogIn(string errorMessage = "")
        {
            ApplicationDbContext db = new ApplicationDbContext();
            String skin = "clean";
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();
            ViewModelWebUsaLogIn model = new ViewModelWebUsaLogIn
            {
                Email = String.Empty,
                Password = String.Empty
            };

            ViewBag.ErrorMessage = errorMessage;

            return View(model);
        }

        // GET: EnpadiAdmin
        public ActionResult Fees()
        {
            String userId = User.Identity.GetUserId();
            if (userId == null)
            {
                return RedirectToAction("LogIn");
            }
            else if (Guid.Parse(userId) != ApplicationUser.Key.AdminUserId)
            {
                AuthenticationManager.SignOut();
                return RedirectToAction("LogIn");
            }

            return View();
        }

        public ActionResult FeeEditor(String id = "")
        {
            ApplicationDbContext db = new ApplicationDbContext();
            ViewModelFeeEditor model = new ViewModelFeeEditor();

            if (id != String.Empty)
            {
                Guid _id = Guid.Parse(id);
                model.Fee = db.Fee.FirstOrDefault(x => x.Id == _id);

                model.PartnerId = (Guid)(model.Fee.PartnerToPayId != null ? model.Fee.PartnerToPayId : Guid.Empty);
                model.PlatformsId = (Guid)(model.Fee.PlatformId != null ? model.Fee.PlatformId : Guid.Empty);
                model.UsersId = (Guid)(model.Fee.UserToPayId != null ? model.Fee.UserToPayId : Guid.Empty);
                model.CurrenciesId = (Guid)(model.Fee.CurrencyId != null ? model.Fee.CurrencyId : Guid.Empty);
                model.ServiceCategoriesId = (Guid)(model.Fee.ServiceCategoryId != null ? model.Fee.ServiceCategoryId : Guid.Empty);

                var noPartner = new SelectListItem { Text = "No Partner", Value = Guid.Empty.ToString() };
                var noPlatform = new SelectListItem { Text = "No Partner", Value = Guid.Empty.ToString() };
                var noUsers = new SelectListItem { Text = "No Partner", Value = Guid.Empty.ToString() };
                var noCurrency = new SelectListItem { Text = "No Partner", Value = Guid.Empty.ToString() };
                var noServiceCategory = new SelectListItem { Text = "No Partner", Value = Guid.Empty.ToString() };

                model.Partner.Add(noPartner);
                model.Partner.AddRange(db.socios
                .Select(x =>
                        new SelectListItem
                        {
                            Value = x.PartnerId.ToString(),
                            Text = x.nombreComercial
                        }).ToList());

                model.Platforms.Add(noPlatform);
                model.Platforms.AddRange(db.Platform
                    .Select(x =>
                        new SelectListItem
                        {
                            Value = x.Id.ToString(),
                            Text = x.Name
                        }).ToList());

                model.Users.Add(noUsers);
                model.Users.AddRange(db.Users
                    .Select(x =>
                        new SelectListItem
                        {
                            Value = x.Id.ToString(),
                            Text = x.Email
                        }).ToList());

                model.Currencies.Add(noCurrency);
                model.Currencies.AddRange(db.monedas
                    .Select(x =>
                        new SelectListItem
                        {
                            Value = x.CurrencyId.ToString(),
                            Text = x.codigo
                        }).ToList());

                model.ServiceCategories.Add(noServiceCategory);
                model.ServiceCategories.AddRange(db.ServiceCategory
                    .Select(x =>
                        new SelectListItem
                        {
                            Value = x.Id.ToString(),
                            Text = x.Name
                        }).ToList());
            }

            return View(model);
        }

        public ActionResult Services()
        {
            return View();
        }

        public ActionResult ServiceEditor(string id)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            servicios model = new servicios();

            if (id != String.Empty)
            {
                Guid _id = Guid.Parse(id);
                model = db.servicios.FirstOrDefault(x => x.Id == _id);

                model.ServiceTypeId = (Guid)(model.ServiceTypeId != null ? model.ServiceTypeId : Guid.Empty);
                model.ServiceCategoryId = (Guid)(model.ServiceCategoryId != null ? model.ServiceCategoryId : Guid.Empty);

                var noServiceType = new SelectListItem { Text = "No Service Type", Value = Guid.Empty.ToString() };
                var noServiceCategory = new SelectListItem { Text = "No Service Category", Value = Guid.Empty.ToString() };

                model.ServiceType.Add(noServiceType);
                model.ServiceType.AddRange(db.ServiceType
                .Select(x =>
                        new SelectListItem
                        {
                            Value = x.Id.ToString(),
                            Text = x.Name
                        }).ToList());

                model.ServiceCategory.Add(noServiceCategory);
                model.ServiceCategory.AddRange(db.ServiceCategory
                    .Select(x =>
                        new SelectListItem
                        {
                            Value = x.Id.ToString(),
                            Text = x.Name
                        }).ToList());
            }

            return View(model);
        }
        #endregion views

        #region methods

        #region selected user 

        public ActionResult LoadServiceReportFromUser(String email, Int32 periodo = 0)
        {

            List<trans> transactions = new List<trans>();

            if (email != null && email != string.Empty)
            {
                periodo = periodo * -1;
                ApplicationDbContext db = new ApplicationDbContext();


                Int32 enpadiCardId = db.tarjetas.FirstOrDefault(x => x.email == email).tarjetaID;

                transactions = db.transacciones.Where(x => x.tipoID == Tipo.Keys.Payment && DbFunctions.DiffMonths(DateTime.Now, x.fechaRegistro) == periodo && (x.socioID == socio.SocioID.Pago_de_servicios || x.socioID == socio.SocioID.Imss_int_id || x.socioID == socio.SocioID.Cemex_int_id) && x.tarjetaID == enpadiCardId).ToList();

                List<TransactionFee> transfees = (db.TransactionFee).ToList();

                foreach (trans transaction in transactions)
                {
                    if (transaction.socioID != 0)
                        transaction.Partner = db.socios.FirstOrDefault(x => x.socioID == transaction.socioID).nombreComercial;

                    if (transaction.tarjetaID != 0)
                        transaction.UserEmail = db.tarjetas.FirstOrDefault(x => x.tarjetaID == transaction.tarjetaID).email;

                    List<TransactionFee> transfeeCurrentTransfer = transfees.Where(x => x.TransaccionesId == transaction.Id).ToList();

                    if (transfeeCurrentTransfer.Count > 0)
                    {
                        transaction.Comissions = transfeeCurrentTransfer.Sum(x => x.FixAmount + x.TaxFeeAmount + x.VariableAmount);
                        transaction.Comissions += transaction.cargo2 + transaction.cargo2iva + transaction.cargo5 + transaction.cargo5iva;
                    }
                    else
                    {
                        transaction.Comissions = transaction.cargo2 + transaction.cargo2iva + transaction.cargo5 + transaction.cargo5iva;
                        transaction.cargoTotal += transaction.Comissions;
                    }
                    transaction.Partner = transaction.Partner + " " + transaction.descripcion;
                }


            }
            return Json(transactions.OrderByDescending(x => x.fechaRegistro), JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadCellphoneFromUser(String email, Int32 periodo = 0)
        {
            List<trans> transactions = new List<trans>();

            if (email != null && email != string.Empty)
            {
                periodo = periodo * -1;
                ApplicationDbContext db = new ApplicationDbContext();
                Int32 enpadiCardId = db.tarjetas.FirstOrDefault(x => x.email == email).tarjetaID;

                transactions = db.transacciones.Where(x => x.tipoID == Tipo.Keys.Payment && DbFunctions.DiffMonths(DateTime.Now, x.fechaRegistro) == periodo && x.socioID == socio.SocioID.Tiempo_aire && x.tarjetaID == enpadiCardId).ToList();

                List<TransactionFee> transfees = (db.TransactionFee).ToList();

                foreach (trans transaction in transactions)
                {
                    if (transaction.socioID != 0)
                        transaction.Partner = db.socios.FirstOrDefault(x => x.socioID == transaction.socioID).nombreComercial;

                    if (transaction.tarjetaID != 0)
                        transaction.UserEmail = db.tarjetas.FirstOrDefault(x => x.tarjetaID == transaction.tarjetaID).email;

                    List<TransactionFee> transfeeCurrentTransfer = transfees.Where(x => x.TransaccionesId == transaction.Id).ToList();

                    if (transfeeCurrentTransfer.Count > 0)
                    {
                        transaction.Comissions = transfeeCurrentTransfer.Sum(x => x.FixAmount + x.TaxFeeAmount + x.VariableAmount);
                        transaction.Comissions += transaction.cargo2 + transaction.cargo2iva + transaction.cargo5 + transaction.cargo5iva;
                    }
                    else
                    {
                        transaction.Comissions = transaction.cargo2 + transaction.cargo2iva + transaction.cargo5 + transaction.cargo5iva;
                        transaction.cargoTotal += transaction.Comissions;
                    }

                    transaction.Partner = transaction.Partner + " " + transaction.descripcion;
                }

            }

            return Json(transactions.OrderByDescending(x => x.fechaRegistro), JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadDepositsFromUser(String email, Int32 periodo = 0)
        {
            List<trans> transactions = new List<trans>();

            if (email != null && email != string.Empty)
            {

                periodo = periodo * -1;
                ApplicationDbContext db = new ApplicationDbContext();
                Int32 enpadiCardId = db.tarjetas.FirstOrDefault(x => x.email == email).tarjetaID;

                transactions = db.transacciones.Where(x => x.tipoID == Tipo.Keys.Deposit && DbFunctions.DiffMonths(DateTime.Now, x.fechaRegistro) == periodo && x.tarjetaID == enpadiCardId).ToList();

                List<TransactionFee> transfees = (db.TransactionFee).ToList();

                foreach (trans transaction in transactions)
                {
                    if (transaction.socioID != 0)
                        transaction.Partner = db.socios.FirstOrDefault(x => x.socioID == transaction.socioID).nombreComercial;

                    if (transaction.tarjetaID != 0)
                        transaction.UserEmail = db.tarjetas.FirstOrDefault(x => x.tarjetaID == transaction.tarjetaID).email;

                    List<TransactionFee> transfeeCurrentTransfer = transfees.Where(x => x.TransaccionesId == transaction.Id).ToList();

                    if (transfeeCurrentTransfer.Count > 0)
                    {
                        transaction.Comissions = transfeeCurrentTransfer.Sum(x => x.FixAmount + x.TaxFeeAmount + x.VariableAmount);
                        transaction.Comissions += transaction.cargo2 + transaction.cargo2iva + transaction.cargo5 + transaction.cargo5iva;
                    }
                    else
                    {
                        transaction.Comissions = transaction.cargo2 + transaction.cargo2iva + transaction.cargo5 + transaction.cargo5iva;
                    }

                    transaction.abonoTotal += transaction.Comissions;
                }

            }

            return Json(transactions.OrderByDescending(x => x.fechaRegistro), JsonRequestBehavior.AllowGet);
        }

        #endregion selected user 

        public async Task<ActionResult> Changepassword(String email, String password)
        {

            var contextDB = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
            var storeDB = new UserStore<IdentityUser>(contextDB);
            var managerDB = new UserManager<IdentityUser>(storeDB);
            var user = managerDB.FindByEmail(email);

            var token = await UserManager.GeneratePasswordResetTokenAsync(user.Id);

            var result = await UserManager.ResetPasswordAsync(user.Id, token, password);

            return Json(new object { }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllUsers()
        {

            ApplicationDbContext db = new ApplicationDbContext();

            List<ApplicationUser> users = new List<ApplicationUser>();

            users = db.Users.OrderBy(x => x.Email).ToList();

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadStoreReport(Int32 periodo = 0)
        {
            periodo = periodo * -1;
            ApplicationDbContext db = new ApplicationDbContext();
            List<trans> transactions = db.transacciones.Where(x => x.tipoID == Tipo.Keys.Payment && DbFunctions.DiffMonths(DateTime.Now, x.fechaRegistro) == periodo && x.socioID == socio.SocioID.Tienda).ToList();

            List<TransactionFee> transfees = (db.TransactionFee).ToList();

            foreach (trans transaction in transactions)
            {
                if (transaction.socioID != 0)
                    transaction.Partner = db.socios.FirstOrDefault(x => x.socioID == transaction.socioID).nombreComercial;

                if (transaction.tarjetaID != 0)
                    transaction.UserEmail = db.tarjetas.FirstOrDefault(x => x.tarjetaID == transaction.tarjetaID).email;

                List<TransactionFee> transfeeCurrentTransfer = transfees.Where(x => x.TransaccionesId == transaction.Id).ToList();

                if (transfeeCurrentTransfer.Count > 0)
                {
                    transaction.Comissions = transfeeCurrentTransfer.Sum(x => x.FixAmount + x.TaxFeeAmount + x.VariableAmount);
                    transaction.Comissions += transaction.cargo2 + transaction.cargo2iva + transaction.cargo5 + transaction.cargo5iva;
                }
                else
                {
                    transaction.Comissions = transaction.cargo2 + transaction.cargo2iva + transaction.cargo5 + transaction.cargo5iva;
                }

                transaction.abonoTotal += transaction.Comissions;
            }



            return Json(transactions.OrderByDescending(x => x.fechaRegistro), JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadBankReport(Int32 periodo = 0)
        {
            periodo = periodo * -1;
            ApplicationDbContext db = new ApplicationDbContext();
            List<trans> transactions = db.transacciones.Where(x => x.tipoID == Tipo.Keys.Payment && DbFunctions.DiffMonths(DateTime.Now, x.fechaRegistro) == periodo && x.socioID == socio.SocioID.Banco).ToList();

            List<TransactionFee> transfees = (db.TransactionFee).ToList();

            foreach (trans transaction in transactions)
            {
                if (transaction.socioID != 0)
                    transaction.Partner = db.socios.FirstOrDefault(x => x.socioID == transaction.socioID).nombreComercial;

                if (transaction.tarjetaID != 0)
                    transaction.UserEmail = db.tarjetas.FirstOrDefault(x => x.tarjetaID == transaction.tarjetaID).email;

                List<TransactionFee> transfeeCurrentTransfer = transfees.Where(x => x.TransaccionesId == transaction.Id).ToList();

                if (transfeeCurrentTransfer.Count > 0)
                {
                    transaction.Comissions = transfeeCurrentTransfer.Sum(x => x.FixAmount + x.TaxFeeAmount + x.VariableAmount);
                    transaction.Comissions += transaction.cargo2 + transaction.cargo2iva + transaction.cargo5 + transaction.cargo5iva;
                }
                else
                {
                    transaction.Comissions = transaction.cargo2 + transaction.cargo2iva + transaction.cargo5 + transaction.cargo5iva;
                }

                transaction.abonoTotal += transaction.Comissions;
            }



            return Json(transactions.OrderByDescending(x => x.fechaRegistro), JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadServiceReport(Int32 periodo = 0)
        {
            periodo = periodo * -1;
            ApplicationDbContext db = new ApplicationDbContext();
            List<trans> transactions = db.transacciones.Where(x => x.tipoID == Tipo.Keys.Payment && DbFunctions.DiffMonths(DateTime.Now, x.fechaRegistro) == periodo && (x.socioID == socio.SocioID.Pago_de_servicios || x.socioID == socio.SocioID.Imss_int_id || x.socioID == socio.SocioID.Cemex_int_id)).ToList();

            List<TransactionFee> transfees = (db.TransactionFee).ToList();

            foreach (trans transaction in transactions)
            {
                if (transaction.socioID != 0)
                    transaction.Partner = db.socios.FirstOrDefault(x => x.socioID == transaction.socioID).nombreComercial;

                if (transaction.tarjetaID != 0)
                    transaction.UserEmail = db.tarjetas.FirstOrDefault(x => x.tarjetaID == transaction.tarjetaID).email;

                List<TransactionFee> transfeeCurrentTransfer = transfees.Where(x => x.TransaccionesId == transaction.Id).ToList();

                if (transfeeCurrentTransfer.Count > 0)
                {
                    transaction.Comissions = transfeeCurrentTransfer.Sum(x => x.FixAmount + x.TaxFeeAmount + x.VariableAmount);
                    transaction.Comissions += transaction.cargo2 + transaction.cargo2iva + transaction.cargo5 + transaction.cargo5iva;
                }
                else
                {
                    transaction.Comissions = transaction.cargo2 + transaction.cargo2iva + transaction.cargo5 + transaction.cargo5iva;
                    transaction.cargoTotal += transaction.Comissions;
                }
                transaction.Partner = transaction.Partner + " " + transaction.descripcion;
            }



            return Json(transactions.OrderByDescending(x => x.fechaRegistro), JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadCellphone(Int32 periodo = 0)
        {
            periodo = periodo * -1;
            ApplicationDbContext db = new ApplicationDbContext();
            List<trans> transactions = db.transacciones.Where(x => x.tipoID == Tipo.Keys.Payment && DbFunctions.DiffMonths(DateTime.Now, x.fechaRegistro) == periodo && x.socioID == socio.SocioID.Tiempo_aire).ToList();

            List<TransactionFee> transfees = (db.TransactionFee).ToList();

            foreach (trans transaction in transactions)
            {
                if (transaction.socioID != 0)
                    transaction.Partner = db.socios.FirstOrDefault(x => x.socioID == transaction.socioID).nombreComercial;

                if (transaction.tarjetaID != 0)
                    transaction.UserEmail = db.tarjetas.FirstOrDefault(x => x.tarjetaID == transaction.tarjetaID).email;

                List<TransactionFee> transfeeCurrentTransfer = transfees.Where(x => x.TransaccionesId == transaction.Id).ToList();

                if (transfeeCurrentTransfer.Count > 0)
                {
                    transaction.Comissions = transfeeCurrentTransfer.Sum(x => x.FixAmount + x.TaxFeeAmount + x.VariableAmount);
                    transaction.Comissions += transaction.cargo2 + transaction.cargo2iva + transaction.cargo5 + transaction.cargo5iva;
                }
                else
                {
                    transaction.Comissions = transaction.cargo2 + transaction.cargo2iva + transaction.cargo5 + transaction.cargo5iva;
                    transaction.cargoTotal += transaction.Comissions;
                }

                transaction.Partner = transaction.Partner + " " + transaction.descripcion;
            }



            return Json(transactions.OrderByDescending(x => x.fechaRegistro), JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadDeposits(Int32 periodo = 0)
        {
            periodo = periodo * -1;
            ApplicationDbContext db = new ApplicationDbContext();
            List<trans> transactions = db.transacciones.Where(x => x.tipoID == Tipo.Keys.Deposit && DbFunctions.DiffMonths(DateTime.Now, x.fechaRegistro) == periodo).ToList();

            List<TransactionFee> transfees = (db.TransactionFee).ToList();

            foreach (trans transaction in transactions)
            {
                if (transaction.socioID != 0)
                    transaction.Partner = db.socios.FirstOrDefault(x => x.socioID == transaction.socioID).nombreComercial;

                if (transaction.tarjetaID != 0)
                    transaction.UserEmail = db.tarjetas.FirstOrDefault(x => x.tarjetaID == transaction.tarjetaID).email;

                List<TransactionFee> transfeeCurrentTransfer = transfees.Where(x => x.TransaccionesId == transaction.Id).ToList();

                if (transfeeCurrentTransfer.Count > 0)
                {
                    transaction.Comissions = transfeeCurrentTransfer.Sum(x => x.FixAmount + x.TaxFeeAmount + x.VariableAmount);
                    transaction.Comissions += transaction.cargo2 + transaction.cargo2iva + transaction.cargo5 + transaction.cargo5iva;
                }
                else
                {
                    transaction.Comissions = transaction.cargo2 + transaction.cargo2iva + transaction.cargo5 + transaction.cargo5iva;
                }

                transaction.abonoTotal += transaction.Comissions;
            }



            return Json(transactions.OrderByDescending(x => x.fechaRegistro), JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadTransactionFees(string transactionId)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            Guid _transactionId = Guid.Parse(transactionId);

            List<TransactionFee> transfees = db.TransactionFee.Where(x => x.TransaccionesId == _transactionId).ToList();
            List<Fee> fees = (db.Fee).ToList();

            foreach (TransactionFee transfee in transfees)
            {

                transfee.FeeName = fees.FirstOrDefault(x => x.Id == transfee.FeeId).Name;

            }

            return Json(transfees, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveService(
                String id,
                String name,
                String description,
                String referenceLength,
                String instructions,
                String serviceCategoryId,
                String amount,
                String doesAmountApply
            )
        {
            ApplicationDbContext db = new ApplicationDbContext();

            Guid _id = Guid.NewGuid();



            if (id != string.Empty || id != Guid.Empty.ToString())
            {
                _id = Guid.Parse(id);
                servicios service = db.servicios.FirstOrDefault(x => x.Id == _id);

                if (amount == string.Empty || amount == null)
                {
                    amount = "0";
                }


                service.Nombre = name;
                service.Descripcion = description;

                service.Amount = Decimal.Parse(amount);
                service.DoesAmountApply = Boolean.Parse(doesAmountApply);

                if (referenceLength != null)
                {
                    if (referenceLength != String.Empty)
                    {
                        service.ReferenciaLongitud = Int32.Parse(referenceLength);
                    }
                    else
                    {
                        service.ReferenciaLongitud = 0;
                    }

                }
                else
                {
                    service.ReferenciaLongitud = 0;
                }

                service.Instrucciones = instructions;
                service.ServiceCategoryId = Guid.Parse(serviceCategoryId);

                db.servicios.Add(service);
                db.Entry(service).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                //servicios service = new servicios
                //{
                //    Id =_id,
                //    SKU
                //};

                //db.servicios.Add(service);
                //db.Entry(service).State = System.Data.Entity.EntityState.Added;
                //db.SaveChanges();
            }

            return Json(new { Name = name }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveFee(
                String id,
                String name,
                String description,
                String variableFee,
                String fixFee,
                String taxFee,

                String upperRestriction,
                String lowerRestriction,
                String partnerToPayId,
                String currencyId,

                String userToPayId,
                String serviceCategoryId,
                String platformId
            )
        {

            ApplicationDbContext db = new ApplicationDbContext();
            Guid? _partnerToPayId = null;
            Guid? _userToPayId = null;
            Guid _id = Guid.NewGuid();

            if (Guid.Parse(partnerToPayId) != Guid.Empty)
            {
                _partnerToPayId = Guid.Parse(partnerToPayId);
            }
            if (Guid.Parse(userToPayId) != Guid.Empty)
            {
                _userToPayId = Guid.Parse(userToPayId);
            }
            if (id != string.Empty || id != Guid.Empty.ToString())
            {
                _id = Guid.Parse(id);
                Fee fee = db.Fee.FirstOrDefault(x => x.Id == _id);

                fee.Id = _id;
                fee.Name = name;
                fee.Description = description;
                fee.VariableFee = Decimal.Parse(variableFee);
                fee.FixFee = Decimal.Parse(fixFee);
                fee.TaxFee = Decimal.Parse(taxFee);
                fee.UpperRestriction = Decimal.Parse(upperRestriction);
                fee.LowerRestriction = Decimal.Parse(lowerRestriction);
                fee.PartnerToPayId = _partnerToPayId;
                fee.CurrencyId = Guid.Parse(currencyId);
                fee.UserToPayId = _userToPayId;
                fee.CreatedByUserId = Guid.Parse(User.Identity.GetUserId());
                fee.AllowDelete = false;
                fee.ServiceCategoryId = Guid.Parse(serviceCategoryId);
                fee.PlatformId = Guid.Parse(platformId);


                db.Fee.Add(fee);
                db.Entry(fee).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                Fee fee = new Fee
                {

                    Id = _id,
                    Name = name,
                    Description = description,
                    VariableFee = Decimal.Parse(variableFee),
                    FixFee = Decimal.Parse(fixFee),
                    TaxFee = Decimal.Parse(taxFee),
                    UpperRestriction = Decimal.Parse(upperRestriction),
                    LowerRestriction = Decimal.Parse(lowerRestriction),
                    PartnerToPayId = _partnerToPayId,
                    CurrencyId = Guid.Parse(currencyId),
                    UserToPayId = _userToPayId,
                    CreatedByUserId = Guid.Parse(User.Identity.GetUserId()),
                    AllowDelete = false,
                    ServiceCategoryId = Guid.Parse(serviceCategoryId),
                    PlatformId = Guid.Parse(platformId),

                };
                db.Fee.Add(fee);
                db.Entry(fee).State = System.Data.Entity.EntityState.Added;
                db.SaveChanges();
            }

            ViewBag.ChangeSucess = "Changes on " + name + " where successful.";

            return Json(new { Name = name }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadSistemFees()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            List<Fee> fees = (db.Fee).ToList();

            foreach (Fee fee in fees)
            {
                socio partner = db.socios.FirstOrDefault(x => x.PartnerId == fee.PartnerToPayId);
                ApplicationUser usuario = db.Users.FirstOrDefault(x => x.Id == fee.UserToPayId.ToString());

                fee.PartnerName = (partner != null ? partner.nombreComercial : "");
                fee.UserToPayEmail = (usuario != null ? usuario.Email : "");
                fee.CurrencyCode = db.monedas.FirstOrDefault(x => x.CurrencyId == fee.CurrencyId).codigo;
                fee.PlatformName = db.Platform.FirstOrDefault(x => x.Id == fee.PlatformId).Name;
                fee.ServiceCategoryName = db.ServiceCategory.FirstOrDefault(x => x.Id == fee.ServiceCategoryId).Name;
            }

            return Json(fees, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadBalance(int? periodo = 0)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            periodo = periodo * -1;

            string strSQL = String.Empty;

            strSQL = " SELECT numero, CONVERT(VARCHAR, Anio) + '  ' + CASE Mes WHEN 1 THEN 'ENERO' WHEN 2 THEN 'FEBRERO' WHEN 3 THEN 'MARZO' WHEN 4 THEN 'ABRIL' WHEN 5 THEN 'MAYO' WHEN 6 THEN 'JUNIO' WHEN 7 THEN 'JULIO' "
                      + " WHEN 8 THEN 'AGOSTO' WHEN 9 THEN 'SEPTIEMBRE' WHEN 10 THEN 'OCTUBRE' WHEN 11 THEN 'NOVIEMBRE' WHEN 12 THEN 'DICIEMBRE' END As Mes "
                      + " FROM ( "
                      + " SELECT 0 As numero, DATEPART(MONTH, DATEADD(MONTH, 0, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, 0, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -1 As numero, DATEPART(MONTH, DATEADD(MONTH, -1, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -1, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -2 As numero, DATEPART(MONTH, DATEADD(MONTH, -2, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -2, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -3 As numero, DATEPART(MONTH, DATEADD(MONTH, -3, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -3, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -4 As numero, DATEPART(MONTH, DATEADD(MONTH, -4, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -4, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -5 As numero, DATEPART(MONTH, DATEADD(MONTH, -5, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -5, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -6 As numero, DATEPART(MONTH, DATEADD(MONTH, -6, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -6, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -7 As numero, DATEPART(MONTH, DATEADD(MONTH, -7, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -7, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -8 As numero, DATEPART(MONTH, DATEADD(MONTH, -8, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -8, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -9 As numero, DATEPART(MONTH, DATEADD(MONTH, -9, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -9, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -10 As numero, DATEPART(MONTH, DATEADD(MONTH, -10, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -10, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -11 As numero, DATEPART(MONTH, DATEADD(MONTH, -11, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -11, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -12 As numero, DATEPART(MONTH, DATEADD(MONTH, -12, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -12, GETDATE())) As Anio "
                      + " ) As Meses "
                      + " ORDER BY numero DESC ";
            IEnumerable<periodo> meses = db.Database.SqlQuery<periodo>(strSQL);
            List<periodo> rmeses = meses.ToList();

            strSQL = " SELECT CASE DATEPART(MONTH, DATEADD(MONTH, " + periodo.ToString() + ", GetDate())) "
                   + " WHEN 1 THEN 'ENERO' WHEN 2 THEN 'FEBRERO' WHEN 3 THEN 'MARZO' WHEN 4 THEN 'ABRIL' "
                   + " WHEN 5 THEN 'MAYO' WHEN 6 THEN 'JUNIO' WHEN 7 THEN 'JULIO' WHEN 8 THEN 'AGOSTO' "
                   + " WHEN 9 THEN 'SEPTIEMBRE' WHEN 10 THEN 'OCTUBRE' WHEN 11 THEN 'NOVIEMBRE' WHEN 12 THEN 'DICIEMBRE' "
                   + " END + ' ' + CONVERT(varchar(10), DATEPART(YEAR, DATEADD(MONTH, " + periodo.ToString() + ", GetDate()))) AS [desc] ";
            IEnumerable<descripcion> descrip = db.Database.SqlQuery<descripcion>(strSQL);
            string periodo_descripcion = descrip.ToList().FirstOrDefault().desc;


            var result = new
            {
                periodo_descripcion,
                rmeses
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadUser()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            String userId = User.Identity.GetUserId();
            if (userId == null)
            {
                return RedirectToAction("LogIn");
            }
            else if (Guid.Parse(userId) != ApplicationUser.Key.AdminUserId)
            {
                AuthenticationManager.SignOut();
                return RedirectToAction("LogIn");
            }


            //tbbm no time and dont know why it does not add up, using old methods
            //List<trans> transactions = db.transacciones.Where(x => x.fechaBaja == null && x.socioID == socio.SocioID.Pago_de_servicios).ToList();

            //ViewModelEnpadiAdmin model = new ViewModelEnpadiAdmin
            //{
            //    DiestelPhoneTime = transactions.Where(x => x.socioID == socio.SocioID.Tiempo_aire).Sum(x => (x.abono1 + x.abono1iva + x.abono2 + x.abono2iva) - (x.cargo1 + x.cargo1iva + x.cargo2 + x.cargo2iva)),
            //    DiestelServiceTime = transactions.Where(x => x.socioID == socio.SocioID.Pago_de_servicios).Sum(x => (x.abono1 + x.abono1iva + x.abono2 + x.abono2iva) - (x.cargo1 + x.cargo1iva + x.cargo2 + x.cargo2iva))
            //};
            ViewModelEnpadiAdmin model = new ViewModelEnpadiAdmin();
            string strSQL = String.Empty;

            // Saldo Tiempo Aire
            strSQL = "SELECT SUM(abono1 + abono1iva + abono2 + abono2iva - cargo1 - cargo1iva - cargo2 - cargo2iva) As Total FROM Transacciones WHERE FechaBaja IS NULL AND socioID = 5";
            IEnumerable<total> TiempoAire = db.Database.SqlQuery<total>(strSQL);
            model.DiestelPhoneTime = TiempoAire.ToList().FirstOrDefault().Total;

            // Saldo Pago de Servicios
            strSQL = "SELECT SUM(abono1 + abono1iva + abono2 + abono2iva - cargo1 - cargo1iva - cargo2 - cargo2iva) As Total FROM Transacciones WHERE FechaBaja IS NULL AND socioID = 6";
            IEnumerable<total> PagoServicios = db.Database.SqlQuery<total>(strSQL);
            model.DiestelServiceTime = PagoServicios.ToList().FirstOrDefault().Total;

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public async Task<ActionResult> LogInValidation(String email, String password)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var result = await SignInManager.PasswordSignInAsync(email, password, false, shouldLockout: false);

            String skin = "clean";
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();
            ViewModelWebUsaLogIn model = new ViewModelWebUsaLogIn
            {
                Email = String.Empty,
                Password = String.Empty
            };

            switch (result)
            {
                case SignInStatus.Success:

                    String userId = db.Users.FirstOrDefault(x => x.Email == email).Id;
                    Guid _userId = Guid.Parse(userId);
                    if (_userId == ApplicationUser.Key.AdminUserId)
                    {

                        return RedirectToAction("Index", "EnpadiAdmin");
                    }
                    else
                    {
                        AuthenticationManager.SignOut();
                        return RedirectToAction("LogIn", "EnpadiAdmin", new { errorMessage = "Usuario incorrecto para esta plataforma." });
                    }
                case SignInStatus.LockedOut:
                    ViewBag.ErrorMessage = "Por seguridad el usuario se encuentra restringido.";
                    return View("LogIn", model);
                case SignInStatus.Failure:
                default:
                    ViewBag.ErrorMessage = "Mal usuario o contraseña.";
                    return View("LogIn", model);
            }

        }


        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("login", "enpadiadmin");
        }

        public JsonResult LoadGeneralReport(String periodo)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            periodo = (Int32.Parse(periodo) * -1).ToString();
            Int32 _periodo = Int32.Parse(periodo);
            if (periodo == String.Empty)
            {
                periodo = "0";
            }

            List<UserAccountReportModel> items = new List<UserAccountReportModel>();
            List<TransactionFee> transfee = (db.TransactionFee).ToList();
            List<trans> transaction = (db.transacciones.Where(x => DbFunctions.DiffMonths(DateTime.Now, x.fechaRegistro) == _periodo)).ToList();


            foreach (trans item in transaction)
            {
                items.Add(
                    new UserAccountReportModel
                    {
                        transaccion_transID = item.transID,
                        origin_descripcion = db.origenes.FirstOrDefault(x => x.origenID == item.origenID).descripcion.ToString(),
                        type_descripcion = db.Tipo.FirstOrDefault(x => x.tipoID == item.tipoID).descripcion.ToString(),
                        transaccion_socioID = item.socioID,
                        partner_nombreComercial = (db.socios.FirstOrDefault(x => x.socioID == item.socioID) != null ? db.socios.FirstOrDefault(x => x.socioID == item.socioID).nombreComercial.ToString() : ""),
                        transaccion_terminalID = item.terminalID,
                        currency_codigo = db.monedas.FirstOrDefault(x => x.monedaID == item.monedaID).codigo.ToString(),
                        transaccion_tarjetaID = item.tarjetaID,
                        transaccion_tarjeta = item.tarjeta,
                        transaccion_monto = item.monto,
                        transaccion_comision = (item.cargo2 + item.cargo3 + item.cargo4 + item.cargo5 + item.cargo6),
                        transaccion_iva = item.cargo2iva + item.cargo3iva + item.cargo4iva + item.cargo5iva + item.cargo6iva,
                        transaccion_concepto = item.concepto,
                        transaccion_descripcion = item.descripcion,
                        transaccion_referencia = item.referencia,
                        status_descripcion = db.Estatus.FirstOrDefault(x => x.estatusID == item.estatusID).descripcion.ToString(),
                        transaccion_aprobacion = item.aprobacion,
                        transaccion_ip = item.ip,
                        transaccion_fechaRegistro = (item.fechaRegistro != null ? (DateTime)item.fechaRegistro : DateTime.MinValue),
                        transaccion_transaccion = item.transaccion,

                        ComissionMX = item.ComissionMX,
                        TotalAmountMX = item.TotalMX,
                        ComissionUSD = item.ComissionUSD,
                        TotalAmountUSD = item.TotalUSD,
                        AmountForServiceMXN = item.ServiceInMXN,
                        AmountForServiceUSD = item.ServiceInDlls,
                        transactionUserEmail = db.tarjetas.Where(x => x.tarjetaID == item.tarjetaID).FirstOrDefault().email
                    }
                    );
            }


            foreach (UserAccountReportModel item in items)
            {

                Guid _transactionid = db.transacciones.FirstOrDefault(x => x.transID == item.transaccion_transID).Id;
                List<TransactionFee> filterTransfee = transfee.Where(x => x.TransaccionesId == _transactionid).ToList();

                decimal fixamount = filterTransfee.Sum(x => x.FixAmount);
                decimal variableamount = filterTransfee.Sum(x => x.VariableAmount);
                decimal taxfeeamount = filterTransfee.Sum(x => x.TaxFeeAmount);
                decimal chargebypartneramount = filterTransfee.Sum(x => x.ChargedByPartnerAmount);

                item.transaccion_comision += fixamount + variableamount + taxfeeamount;

                if (filterTransfee.Count() > 0)
                {
                    Guid feeId = filterTransfee.FirstOrDefault().FeeId;
                    Fee fee = db.Fee.Where(x => x.Id == feeId).FirstOrDefault();
                    item.platform = db.Platform.Where(x=>x.Id == fee.PlatformId).FirstOrDefault().Name;
                }
            }

            return Json(items.OrderByDescending(x => x.transaccion_transID), JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadServices()
        {

            ApplicationDbContext db = new ApplicationDbContext();

            List<servicios> items = (db.servicios).ToList();

            foreach (servicios item in items)
            {

                item.ServiceCategoryName = db.ServiceCategory.FirstOrDefault(x => x.Id == item.ServiceCategoryId).Name;
                item.ServiceTypeName = db.ServiceType.FirstOrDefault(x => x.Id == item.ServiceTypeId).Name;

            }

            return Json(items.OrderByDescending(x => x.Compañia), JsonRequestBehavior.AllowGet);
        }

        #endregion methods

        #region tools 

        private String GetProfilePicture()
        {
            string result = String.Empty;
            string currentUserId = User.Identity.GetUserId();
            var user = UserManager.FindById(currentUserId);

            string path = System.IO.Path.Combine(
                                           Server.MapPath("~/UpLoads/ProfilePictures/"), currentUserId + ".jpg");

            if (System.IO.File.Exists(path))
            {
                AspNetUsersModel tempUser = AspNetUsersModel.Get(Guid.Parse(currentUserId), user.Email);
                result = tempUser.ProfilePicUrl;
            }
            else
            {
                result = "Images/account_no_image.png";
            }

            return result;
        }


        #endregion tools
    }
}