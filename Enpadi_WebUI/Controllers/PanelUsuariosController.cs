﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Data.Entity;
using Enpadi_WebUI.Models;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Openpay;
using Openpay.Entities;
using Openpay.Entities.Request;
using System.Net;
using Enpadi_WebUI.WSDiestel;
using Enpadi_WebUI.Properties;
using System.Configuration;
using conekta;
using System.Web.Script.Serialization;
using BarcodeLib;
using System.Drawing;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Web.Helpers;
using System.IO;
using System.Runtime.Serialization.Json;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Text.RegularExpressions;

namespace Enpadi_WebUI.Controllers
{
    [Authorize]
    public class PanelUsuariosController : Controller
    {


        private static bool bModoProduccion = Convert.ToBoolean(ConfigurationManager.AppSettings["ModoProduccion"]);

        #region openpay

        private string OPENPAY_ID = (bModoProduccion) ? ConfigurationManager.AppSettings["Enpadi_OpenPay_Id"] : ConfigurationManager.AppSettings["Test_OpenPay_id"];
        private string OPENPAY_PUBLIC_KEY = (bModoProduccion) ? ConfigurationManager.AppSettings["Enpadi_OpenPay_PublicKey"] : ConfigurationManager.AppSettings["Test_OpenPay_PublicKey"];

        private string OPENPAY_PRIVATE_KEY = (bModoProduccion) ? Settings.Default.Enpadi_OpenPay_PrivateKey : Settings.Default.Test_OpenPay_PrivateKey;

        #endregion openpay

        #region conekta

        private string CONEKTA_PUBLIC_KEY = (bModoProduccion) ? conekta.SecurityKeys.Enpadi_conekta_publickey : conekta.SecurityKeys.Test_enpadi_conekta_publickey;
        private string CONEKTA_PRIVATE_KEY = (bModoProduccion) ? conekta.SecurityKeys.Enpadi_conekta_privatekey : conekta.SecurityKeys.Test_enpadi_conekta_privatekey;

        #endregion conketa

        #region diestel

        private string DIESTEL_URL = (bModoProduccion) ? Settings.Default.Enpadi_WebUI_WSDiestel_PxUniversal : Settings.Default.Test_WebUI_WSDiestel_PxUniversal;
        private int DIESTEL_GRUPO = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Grupo : Settings.Default.Test_WSDiestel_Grupo;
        private int DIESTEL_CADENA = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Cadena : Settings.Default.Test_WSDiestel_Cadena;
        private int DIESTEL_TIENDA = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Tienda : Settings.Default.Test_WSDiestel_Tienda;
        private string DIESTEL_USUARIO = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Usuario : Settings.Default.Test_WSDiestel_Usuario;
        private string DIESTEL_PASSWORD = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Password : Settings.Default.Test_WSDiestel_Password;

        #endregion diestel

        // GET: PanelUsuarios
        public ActionResult Index()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            //Listado de Movimientos
            string strSQL = string.Empty;
            strSQL = " SELECT t.transID, o.descripcion AS origen, i.descripcion AS tipo, s.nombreComercial As socio, terminalID, "
                   + " m.codigo AS moneda, t.tarjetaID, t.tarjeta, t.monto, t.transaccion, t.concepto, t.descripcion, t.referencia, "
                   + " e.descripcion As estatus, t.aprobacion, t.ip, t.fechaRegistro "
                   + " FROM Transacciones t "
                   + " LEFT JOIN Origen o ON t.origenID = o.origenID "
                   + " LEFT JOIN Tipo i ON t.tipoID = i.tipoID "
                   + " LEFT JOIN Socios s ON t.socioID = s.socioID "
                   + " LEFT JOIN Monedas m ON t.monedaID = m.monedaID "
                   + " LEFT JOIN Estatus e ON t.estatusID = e.estatusID "
                   + " ORDER BY t.fechaRegistro ";
            IEnumerable<movimientosListado> movs = db.Database.SqlQuery<movimientosListado>(strSQL);
            return View(movs.ToList());
        }

        // GET: PanelUsuarios
        public ActionResult Recargar(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (Session["country"] != null)
            {
                var country = Session["country"].ToString();
                ViewBag.country = country;
            }
            else
            {
                ViewBag.country = "MX";
            }
            return View();
        }

        public ActionResult AddCustomer(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            openpayCustomer opCustomer = new openpayCustomer() { Name = usuario.datNombre, Email = usuario.email };

            return View(opCustomer);
        }


        // POST: openpayCustomers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddCustomer([Bind(Include = "id,CustomerID,ExternalID,Name,LastName,Email,PhoneNumber,RequiresAccount,Address1,Address2,Address3,City,State,CountryCode,PostalCode")] openpayCustomer opCustomer, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                string currentUserId = User.Identity.GetUserId();
                tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
                opCustomer.ExternalID = usuario.tarjetaID;

                OpenpayAPI api = new OpenpayAPI(OPENPAY_PRIVATE_KEY, OPENPAY_ID, bModoProduccion);

                Openpay.Entities.Customer cliente = new Openpay.Entities.Customer()
                {
                    ExternalId = opCustomer.ExternalID.ToString(),
                    Name = opCustomer.Name,
                    LastName = opCustomer.LastName,
                    Email = opCustomer.Email,
                    PhoneNumber = opCustomer.PhoneNumber
                };

                Openpay.Entities.Address dir = new Openpay.Entities.Address()
                {
                    Line1 = opCustomer.Address1,
                    Line2 = opCustomer.Address2,
                    Line3 = opCustomer.Address3,
                    City = opCustomer.City,
                    State = opCustomer.State,
                    CountryCode = opCustomer.CountryCode,
                    PostalCode = opCustomer.PostalCode
                };

                cliente.Address = dir;

                cliente = api.CustomerService.Create(cliente);

                if (cliente.Id != null)
                {
                    opCustomer.CustomerID = cliente.Id;

                    opCustomer.CLABE = cliente.CLABE;

                    //MARCOS: this is marcos dev, not working with the id structure on the database, made the model and DAO
                    //db.openpayCustomers.Add(opCustomer);
                    //db.SaveChanges();

                    OpenPayCustomerModel.Save(opCustomer);

                    return RedirectToAction("RecargarTC");
                }
                else
                {
                    return View(opCustomer);
                }
            }

            return View(opCustomer);
        }

        public ActionResult AddCard(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        [HttpPost]
        public ActionResult AddCard(string holder_name, string card_number, string cvv2, string expiration_month, string expiration_year, string token_id, string deviceSessionId, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            strSQL = String.Format("SELECT COUNT(*) AS total FROM opCards WHERE ExternalID = {0}", usuario.tarjetaID);
            IEnumerable<contador> registradas = db.Database.SqlQuery<contador>(strSQL);
            int iRegistradas = registradas.ToList().FirstOrDefault().total;

            if (iRegistradas < 3)
            {
                openpayCard opCard = new openpayCard() { ExternalID = usuario.tarjetaID };
                openpayCustomer opCustomer = db.openpayCustomers.FirstOrDefault(x => x.ExternalID == usuario.tarjetaID);
                opCard.CustomerId = opCustomer.CustomerID;

                OpenpayAPI api = new OpenpayAPI(OPENPAY_PRIVATE_KEY, OPENPAY_ID, bModoProduccion);
                Card card = new Card();
                card.TokenId = token_id;
                card.DeviceSessionId = deviceSessionId;

                try
                {
                    card = api.CardService.Create(opCard.CustomerId.ToString(), card);

                    if (card.Id != null)
                    {
                        if (card.Address == null)
                        {
                            card.Address = new Openpay.Entities.Address();
                        }

                        opCard.HolderName = holder_name;
                        opCard.CardNumber = card_number;
                        opCard.ExpirationMonth = expiration_month;
                        opCard.ExpirationYear = expiration_year;
                        opCard.CVV2 = cvv2;
                        opCard.CardID = card.Id;
                        opCard.Brand = card.Brand;
                        opCard.Type = card.Type;
                        opCard.BankName = card.BankName;
                        opCard.BankCode = card.BankCode;
                        opCard.Address1 = card.Address.Line1;
                        opCard.Address2 = card.Address.Line2;
                        opCard.Address3 = card.Address.Line3;
                        opCard.PointsCard = card.PointsCard;
                        opCard.PostalCode = card.Address.PostalCode;
                        opCard.Country = card.Address.CountryCode;
                        opCard.AllowCharges = card.AllowsCharges;
                        opCard.AllowPayouts = card.AllowsPayouts;
                        opCard.BankCode = card.BankCode;
                        opCard.BankName = card.BankName;
                        opCard.City = card.Address.City;
                        opCard.ExpirationMonth = card.ExpirationMonth;
                        opCard.ExpirationYear = card.ExpirationYear;


                        OpenPayCardModel.Save(opCard);

                        //db.openpayCards.Add(opCard);
                        //db.SaveChanges();
                        return RedirectToAction("RecargarTC");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Ha ocurrido un Error. Intenta con otro Numero de Tarjeta");
                    }
                }
                catch (OpenpayException ex)
                {
                    Console.WriteLine(ex.Message);
                    return View(opCard);
                }
            }
            else
            {
                ModelState.AddModelError("", "No puedes tener Registradas mas de 3 Tarjetas en tu Cuenta");
            }

            return View();
        }

        public ActionResult AdminCard(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            return View(db.openpayCards.Where(x => x.ExternalID == usuario.tarjetaID).ToList());
        }


        // GET: openpayCards/Delete/5
        public ActionResult DeleteCard(int? id)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            openpayCard openpayCard = db.openpayCards.Find(id);
            if (openpayCard == null)
            {
                return HttpNotFound();
            }
            return View(openpayCard);
        }

        // POST: openpayCards/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            openpayCard openpayCard = db.openpayCards.Find(id);
            db.openpayCards.Remove(openpayCard);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult AddCharge(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            strSQL = string.Format("SELECT * FROM opCards WHERE DeleteDate IS NULL AND ExternalID = {0}", usuario.tarjetaID);
            IEnumerable<openpayCard> cards = db.Database.SqlQuery<openpayCard>(strSQL);

            return View(cards.ToList());
        }

        [HttpPost]
        public ActionResult AddCharge(string token_id, string deviceSessionId, string source_id, string amount, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {

                string currentUserId = User.Identity.GetUserId();
                tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
                trans t = new trans
                {
                    estatusID = 0,
                    origenID = 1,
                    tipoID = 1,
                    modoID = 1,
                    terminalID = 1,
                    monedaID = 484,
                    Concepto = "Deposito",
                    geoCountryCode = 0,
                    geoCountryName = String.Empty,
                    geoRegion = String.Empty,
                    geoCity = String.Empty,
                    geoPostalCode = String.Empty
                };

                strSQL = string.Format("SELECT COUNT(*) As total FROM logTC WHERE idTarjeta = {0} AND fechaAlta >= DATEADD(DAY, -7, getdate())", usuario.tarjetaID);
                IEnumerable<contador> intentos = db.Database.SqlQuery<contador>(strSQL);
                int iIntentos = intentos.ToList().FirstOrDefault().total;

                strSQL = string.Format("SELECT COUNT(*) As total FROM logTC WHERE idTarjeta = {0} AND result = 1 AND fechaAlta >= DATEADD(DAY, -7, getdate())", usuario.tarjetaID);
                IEnumerable<contador> exitosos = db.Database.SqlQuery<contador>(strSQL);
                int iExitosos = exitosos.ToList().FirstOrDefault().total;

                if (iIntentos < 5 && iExitosos < 2)
                {
                    strSQL = string.Format("SELECT * FROM opCards WHERE DeleteDate IS NULL AND ExternalID = {0} AND CardID = '{1}'", usuario.tarjetaID, source_id);
                    IEnumerable<openpayCard> opCard = db.Database.SqlQuery<openpayCard>(strSQL);

                    if (opCard.Count() > 0)
                    {
                        strSQL = string.Format("SELECT * FROM opCustomers WHERE DateDelete IS NULL AND ExternalID = {0}", usuario.tarjetaID);
                        IEnumerable<openpayCustomer> opCustomer = db.Database.SqlQuery<openpayCustomer>(strSQL);

                        if (opCustomer.Count() > 0)
                        {
                            OpenpayAPI api = new OpenpayAPI(OPENPAY_PRIVATE_KEY, OPENPAY_ID, bModoProduccion);

                            ChargeRequest request = new ChargeRequest();
                            request.Method = OpenpayAPI.RequestMethod.card;
                            request.SourceId = opCard.FirstOrDefault().CardID;
                            request.Amount = Convert.ToDecimal(amount);
                            request.Currency = "MXN";
                            request.Description = "Recarga de Monedero Enpadi";
                            request.DeviceSessionId = deviceSessionId;
                            request.OrderId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);

                            Openpay.Entities.Charge charge = api.ChargeService.Create(opCustomer.FirstOrDefault().CustomerID, request);

                            if (charge.Status == "completed")
                            {
                                t.estatusID = 1;
                                t.tarjetaID = usuario.tarjetaID;
                                t.tarjeta = usuario.tcNum.Substring(usuario.tcNum.Length - 4);
                                t.transaccion = charge.OrderId;
                                t.descripcion = charge.Description;
                                t.referencia = charge.Id;
                                t.aprobacion = charge.Authorization;
                                t.monto = charge.Amount;
                                t.abono1 = charge.Amount;
                                t.abonoTotal = charge.Amount;

                                decimal dIva = (decimal)0.16;
                                string strBrand = string.Empty;
                                if (!string.IsNullOrEmpty(opCard.FirstOrDefault().Brand))
                                    strBrand = opCard.FirstOrDefault().Brand.ToLower();

                                if (strBrand == "visa" || strBrand == "mastercard")
                                {
                                    t.cargo2 = t.monto * (decimal)(2.9 / 100);
                                    t.cargo2iva = t.cargo2 * dIva;
                                    t.cargo5 = (decimal)2.9;
                                    t.cargo5iva = t.cargo5 * dIva;
                                    t.cargoTotal += t.cargo2 + t.cargo2iva + t.cargo5 + t.cargo5iva;
                                    t.socioID = socio.SocioID.Visa;
                                }
                                else
                                {
                                    t.cargo2 = t.monto * (decimal)(4.9 / 100);
                                    t.cargo2iva = t.cargo2 * dIva;
                                    t.cargo5 = (decimal)2.9;
                                    t.cargo5iva = t.cargo5 * dIva;
                                    t.cargoTotal += t.cargo2 + t.cargo2iva + t.cargo5 + t.cargo5iva;
                                    t.socioID = socio.SocioID.Amex;
                                }

                                t.ip = "189.25.169.74";

                                decimal exchangeRateDown = Tools.GetPesoToDollarRate(-1);
                                decimal exchangeRateUp = Tools.GetPesoToDollarRate(1);
                                decimal TotalAmountUSD = (t.monto / exchangeRateDown);
                                Decimal TotalAmountMX = t.monto;

                                t.ComissionMX = 0;
                                t.ComissionUSD = 0;
                                t.TotalMX = TotalAmountMX;
                                t.TotalUSD = TotalAmountUSD;
                                t.ExchangeRateDown = exchangeRateDown;
                                t.ExchangeRateUp = exchangeRateUp;
                                t.PaidWithCredit = false;

                                t.ServiceInDlls = TotalAmountUSD + t.ComissionUSD;
                                t.ServiceInMXN = TotalAmountMX + t.ComissionMX;

                                db.transacciones.Add(t);
                                db.Entry(t).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();

                                //trans.Save(t);
                                //db.transacciones.Add(t);

                                recompensa r = new recompensa() { tarjetaID = usuario.tarjetaID, origenID = 1, socioID = 2, transaccion = charge.OrderId, concepto = "Recompensa Recarga", abono = charge.Amount * (decimal)0.01, cargo = 0 };
                                db.recompensas.Add(r);

                                logTC envio = new logTC() { idTarjeta = usuario.tarjetaID, result = 1 };
                                db.logTCs.Add(envio);

                                db.SaveChanges();

                                return RedirectToAction("RespuestaTC", "PanelUsuarios", new { estatus = "APROBADA", autorizacion = string.Format("AUTORIZACIÓN  {0}", t.aprobacion), total = string.Format("MONTO  ${0} MXN", t.monto) });
                            }
                            else
                            {
                                logTC envio = new logTC() { idTarjeta = usuario.tarjetaID, result = 0 };
                                db.logTCs.Add(envio);

                                db.SaveChanges();
                                return RedirectToAction("RespuestaTC", "PanelUsuarios", new { estatus = "DECLINADA", autorizacion = charge.Description, total = "" });
                            }

                        }
                        else
                        {
                            return RedirectToAction("RespuestaTC", "PanelUsuarios", new { estatus = "DECLINADA", autorizacion = "No se ha encontrado el Usuario Seleccionado" });
                        }
                    }
                    else
                    {
                        return RedirectToAction("RespuestaTC", "PanelUsuarios", new { estatus = "DECLINADA", autorizacion = "No se ha encontrado la Tarjeta Seleccionada", total = "" });
                    }
                }
                else
                {
                    return RedirectToAction("RespuestaTC", "PanelUsuarios", new { estatus = "DECLINADA", autorizacion = "Has excedido el Limite de Recargas Autorizado por Semana", total = "" });
                }


            }
            else
            {
                return View();
            }
        }


        // GET: PanelUsuarios
        public ActionResult RecargarTC(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            strSQL = string.Format("SELECT COUNT(*) AS total FROM opCustomers WHERE ExternalID = {0}", usuario.tarjetaID);
            IEnumerable<contador> custAddress = db.Database.SqlQuery<contador>(strSQL);
            int iCustAddress = custAddress.ToList().FirstOrDefault().total;

            strSQL = string.Format("SELECT COUNT(*) AS total FROM opCards WHERE ExternalID = {0}", usuario.tarjetaID);
            IEnumerable<contador> custCard = db.Database.SqlQuery<contador>(strSQL);
            int iCustCard = custCard.ToList().FirstOrDefault().total;

            if (iCustAddress <= 0)
                return RedirectToAction("AddCustomer");
            if (iCustCard <= 0)
                return RedirectToAction("AddCard");

            return RedirectToAction("AddCharge");
        }

        //TODO check this method, i thinks it has absolutly no use, Second thought, i think is the 20 buck to confirm creditcard
        [HttpPost]
        public ActionResult RecargarTC(string token_id, string holder_name, string amount, string deviceSessionId, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (!ModelState.IsValid)
            {
                return View();
            }
            else
            {
                string currentUserId = User.Identity.GetUserId();
                tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
                trans t = new trans { estatusID = 0, origenID = 1, tipoID = 1, modoID = 1, socioID = 2, terminalID = 1, monedaID = 484, Concepto = "TC", geoCountryCode = 0, geoCountryName = String.Empty, geoRegion = String.Empty, geoCity = String.Empty, geoPostalCode = String.Empty };

                strSQL = string.Format("SELECT COUNT(*) As total FROM logTC WHERE idTarjeta = {0} AND fechaAlta >= DATEADD(DAY, -7, getdate())", usuario.tarjetaID);
                IEnumerable<contador> intentos = db.Database.SqlQuery<contador>(strSQL);
                int iIntentos = intentos.ToList().FirstOrDefault().total;

                strSQL = string.Format("SELECT COUNT(*) As total FROM logTC WHERE idTarjeta = {0} AND result = 1 AND fechaAlta >= DATEADD(DAY, -7, getdate())", usuario.tarjetaID);
                IEnumerable<contador> exitosos = db.Database.SqlQuery<contador>(strSQL);
                int iExitosos = exitosos.ToList().FirstOrDefault().total;

                if (iIntentos < 5 && iExitosos < 2)
                {
                    OpenpayAPI api = new OpenpayAPI(OPENPAY_PRIVATE_KEY, OPENPAY_ID);

                    Openpay.Entities.Customer cliente = new Openpay.Entities.Customer();
                    cliente.ExternalId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);
                    cliente.Name = holder_name;
                    cliente.Email = "amorales@truewisdom.co"; //TODO why was Marcos mail in here?
                    cliente.RequiresAccount = false;

                    ChargeRequest request = new ChargeRequest();
                    request.Method = OpenpayAPI.RequestMethod.card;
                    request.SourceId = token_id;
                    request.Amount = Convert.ToDecimal(amount);
                    request.Currency = "MXN";
                    request.Description = "Recarga de Monedero Enpadi";
                    request.DeviceSessionId = deviceSessionId;
                    request.Customer = cliente;
                    request.OrderId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);

                    Openpay.Entities.Charge charge = api.ChargeService.Create(request);

                    if (charge.Status == "completed")
                    {
                        t.estatusID = 1;
                        t.tarjetaID = usuario.tarjetaID;
                        t.tarjeta = usuario.tcNum.Substring(usuario.tcNum.Length - 4);
                        t.transaccion = charge.OrderId;
                        t.descripcion = charge.Description;
                        t.referencia = charge.Id;
                        t.aprobacion = charge.Authorization;
                        t.monto = charge.Amount;
                        t.abono1 = charge.Amount;
                        t.cargo1 = 0;
                        t.abono2 = 0;
                        t.cargo2 = 0;
                        t.abono3 = 0;
                        t.cargo3 = 0;
                        t.abonoTotal = charge.Amount;
                        t.cargoTotal = 0;
                        t.ip = "189.25.169.74";

                        db.transacciones.Add(t);
                        db.Entry(t).State = System.Data.Entity.EntityState.Added;
                        db.SaveChanges();

                        decimal exchangeRateDown = Tools.GetPesoToDollarRate(-1);
                        decimal exchangeRateUp = Tools.GetPesoToDollarRate(1);
                        decimal TotalAmountUSD = (t.monto / exchangeRateDown);
                        Decimal TotalAmountMX = t.monto;

                        t.ComissionMX = 0;
                        t.ComissionUSD = 0;
                        t.TotalMX = TotalAmountMX;
                        t.TotalUSD = TotalAmountUSD;
                        t.ExchangeRateDown = exchangeRateDown;
                        t.ExchangeRateUp = exchangeRateUp;
                        t.PaidWithCredit = false;

                        t.ServiceInDlls = TotalAmountUSD + t.ComissionUSD;
                        t.ServiceInMXN = TotalAmountMX + t.ComissionMX;

                        db.transacciones.Add(t);
                        db.Entry(t).State = System.Data.Entity.EntityState.Added;
                        db.SaveChanges();

                        //trans.Save(t);
                        //db.transacciones.Add(t);

                        recompensa r = new recompensa() { tarjetaID = usuario.tarjetaID, origenID = 1, socioID = 2, transaccion = charge.OrderId, concepto = "Recompensa Recarga", abono = charge.Amount * (decimal)0.01, cargo = 0 };
                        db.recompensas.Add(r);

                        logTC envio = new logTC() { idTarjeta = usuario.tarjetaID, result = 1 };
                        db.logTCs.Add(envio);

                        db.SaveChanges();

                        return RedirectToAction("RespuestaTC", "PanelUsuarios", new { estatus = "APROBADA", autorizacion = string.Format("AUTORIZACIÓN  {0}", t.aprobacion), total = string.Format("MONTO  ${0} MXN", t.monto) });
                    }
                    else
                    {
                        logTC envio = new logTC() { idTarjeta = usuario.tarjetaID, result = 0 };
                        db.logTCs.Add(envio);

                        db.SaveChanges();
                        return RedirectToAction("RespuestaTC", "PanelUsuarios", new { estatus = "DECLINADA", autorizacion = charge.Description, total = "" });
                    }
                }
                else
                {
                    return RedirectToAction("RespuestaTC", "PanelUsuarios", new { estatus = "DECLINADA", autorizacion = "Has excedido el Limite de Recargas Autorizado por Semana", total = "" });
                }
            }
        }

        // GET: PanelUsuarios
        public ActionResult RespuestaTC(string estatus, string autorizacion, string total, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            respuestaTC r = new respuestaTC() { Estatus = estatus, Autorizacion = autorizacion, Total = total };
            return View(r);
        }

        // GET: PanelUsuarios
        public ActionResult Pagar(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        public ActionResult RecargarBanco(int? monto, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            OpenpayAPI api = new OpenpayAPI(OPENPAY_PRIVATE_KEY, OPENPAY_ID, bModoProduccion);

            Openpay.Entities.Customer cliente = new Openpay.Entities.Customer();
            cliente.ExternalId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);
            cliente.Name = String.Format("{0} {1} {2}", usuario.datNombre, usuario.datPaterno, usuario.datMaterno);
            cliente.Email = String.Format("{0}", usuario.email);
            cliente.RequiresAccount = false;

            ChargeRequest request = new ChargeRequest();
            request.Method = OpenpayAPI.RequestMethod.bank_account;
            request.Amount = (decimal)monto;
            request.Description = "Pago en Banco";
            request.OrderId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);
            request.Customer = cliente;

            Openpay.Entities.Charge charge = api.ChargeService.Create(request);

            OpenPayChargeModel.Save(charge);

            pagoBanco ordenBanco = new pagoBanco()
            {
                id = charge.Id,
                description = charge.Description,
                error_message = charge.ErrorMessage,
                authorization = charge.Authorization,
                amount = charge.Amount,
                operation_type = charge.OperationType,
                payment_method = request.Method,
                clabe = charge.PaymentMethod.CLABE,
                bank = charge.PaymentMethod.BankName,
                name = charge.PaymentMethod.Name,
                order_id = charge.OrderId,
                transaction_type = charge.TransactionType,
                creation_date = charge.CreationDate.ToString(),
                status = charge.Status,
                method = charge.Method
            };

            paystubModel paystub = new paystubModel();
            paystub.paystub_type_id = Guid.Parse(paystubModel.keys.PagoBanco);
            paystub.created_by_email = cliente.Email;
            paystub.open_pay_charge_id = charge.Id;
            paystub.open_pay_order_id = charge.OrderId;

            paystubModel.Save(paystub);

            PagoBancosModel.Save(ordenBanco);

            ViewBag.MontoRecarga = monto;
            return View(ordenBanco);
        }

        public ActionResult RecargarTiendas(int? monto, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            OpenpayAPI api = new OpenpayAPI(OPENPAY_PRIVATE_KEY, OPENPAY_ID, bModoProduccion);

            Openpay.Entities.Customer cliente = new Openpay.Entities.Customer();
            cliente.ExternalId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);
            cliente.Name = String.Format("{0} {1} {2}", usuario.datNombre, usuario.datPaterno, usuario.datMaterno);
            cliente.Email = String.Format("{0}", usuario.email);
            cliente.RequiresAccount = false;

            ChargeRequest request = new ChargeRequest();
            request.Method = OpenpayAPI.RequestMethod.store;
            request.Amount = (decimal)monto;
            request.Description = "Pago en Tienda";
            request.OrderId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);
            request.Customer = cliente;

            Openpay.Entities.Charge charge = api.ChargeService.Create(request);

            pagoTienda ordenTienda = new pagoTienda()
            {
                id = charge.Id,
                description = charge.Description,
                error_message = charge.ErrorMessage,
                authorization = charge.Authorization,
                amount = charge.Amount,
                operation_type = charge.OperationType,
                type = charge.PaymentMethod.Type,
                reference = charge.PaymentMethod.Reference,
                barcode_url = charge.PaymentMethod.BarcodeURL,
                order_id = charge.OrderId,
                transaction_type = charge.TransactionType,
                creation_date = charge.CreationDate.ToString(),
                status = charge.Status,
                method = charge.Method
            };

            //todo: save orden tienda

            paystubModel paystub = new paystubModel();
            paystub.paystub_type_id = Guid.Parse(paystubModel.keys.PagoTienda);
            paystub.created_by_email = cliente.Email;
            paystub.open_pay_charge_id = charge.Id;
            paystub.open_pay_order_id = charge.OrderId;

            paystubModel.Save(paystub);

            ViewBag.MontoRecarga = monto;
            return View(ordenTienda);
        }


        [HttpPost]
        public ActionResult RecargarTiendaOxxo(int monto, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            #region skin

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            #endregion skin

            #region data

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            var contextDB = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
            var storeDB = new UserStore<IdentityUser>(contextDB);
            var managerDB = new UserManager<IdentityUser>(storeDB);
            var userDB = managerDB.FindByEmail(usuario.email);

            #endregion data

            #region conekta

            #region conecta connection

            conekta.Api.apiKey = CONEKTA_PRIVATE_KEY;
            conekta.Api.version = conekta.SecurityKeys.Conekta_version;
            conekta.Api.locale = conekta.SecurityKeys.Conekta_lang_es;

            #endregion connection

            #region conekta customer

            //create the customer required for the order
            conekta.Customer customer = new conekta.Customer
            {
                //id = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID),
                name = String.Format("{0} {1} {2}", usuario.datNombre, usuario.datPaterno, usuario.datMaterno),
                email = String.Format("{0}", usuario.email),
                //phone = "+ " + String.Format("{0:0000000000000}", userDB.PhoneNumber) //"5215555555555"
                phone = ("0000000000000" + userDB.PhoneNumber).Substring(userDB.PhoneNumber.Length)
            };

            #endregion conekta client

            #region conekta product
            //Create a product and added to the conectalist 
            conekta.LineItem product = new conekta.LineItem
            {
                //id = "1",
                name = "Recarga en Oxxo",
                //description = "Recarga de " + monto.ToString() + " en tienda Oxxo",
                unit_price = monto,
                quantity = 1,
                //sku = "REC-" + monto.ToString()
            };

            //create the conekta list require for the order
            conekta.ConektaList lineItems = new ConektaList(typeof(conekta.LineItem));
            lineItems.data = new object[1];
            lineItems.data[0] = product;

            #endregion conekta product

            #region conekta shipping 

            //create the shipping line
            conekta.ShippingLine shippingLine = new conekta.ShippingLine
            {
                amount = 16,
                carrier = "Sin envio"
            };

            //create conekta list shipping lines required for the order
            conekta.ConektaList shippingLines = new ConektaList(typeof(conekta.ShippingLine));
            shippingLines.data = new object[1];
            shippingLines.data[0] = shippingLine;

            #endregion conekta shipping

            #region shipping contact

            //create the address required for the shipping contact
            conekta.Address shippingContactAddress = new conekta.Address
            {
                street1 = (usuario.datDir1 != null ? usuario.datDir1 : string.Empty),
                street2 = (usuario.datDir1 != null ? usuario.datDir2 : string.Empty),
                city = (usuario.ciudades != null ? usuario.ciudades.nombre : string.Empty),
                state = (usuario.estados != null ? usuario.estados.nombre : string.Empty),
                country = (usuario.paises != null ? usuario.paises.nombre : string.Empty),
                zip = (usuario.datCP != null ? usuario.datCP : string.Empty)
            };

            //create the shipping contact required for the order
            conekta.ShippingContact shippingContact = new conekta.ShippingContact
            {
                id = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID),
                address = shippingContactAddress,
                phone = ("0000000000000" + userDB.PhoneNumber).Substring(userDB.PhoneNumber.Length),
                //phone = "+ " + String.Format("{0:0000000000000}", userDB.PhoneNumber), //usuario.datTelefono
                receiver = String.Format("{0} {1} {2}", usuario.datNombre, usuario.datPaterno, usuario.datMaterno),
                email = usuario.email
            };

            #endregion shipping contact

            #region charges

            //create the payment method required for the charge
            conekta.PaymentMethod paymentMethod = new conekta.PaymentMethod
            {
                type = conekta.SecurityKeys.Conekta_payment_method_type,
                expires_at = DateTime.Now.AddDays(2).ToString("yyyyMMddHH")
            };

            //create the charge
            conekta.Charge charge = new conekta.Charge
            {
                //id= String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID),
                //created_at = DateTime.Now.ToString("yyyyMMddHHmmssfff"),
                //currency = conekta.SecurityKeys.Conekta_currency_MXN,
                payment_method = paymentMethod,
                //livemode = false,
                amount = monto
            };

            conekta.ConektaList charges = new ConektaList(typeof(conekta.Charge));
            charges.data = new object[1];
            charges.data[0] = charge;

            #endregion charges

            #region conekta order

            //create the order
            conekta.Order order = new conekta.Order
            {
                currency = conekta.SecurityKeys.Conekta_currency_MXN,
                customer_info = customer,
                line_items = lineItems,
                shipping_contact = shippingContact,
                //shipping_lines = shippingLines,

                charges = charges,
                param = ""
            };

            #endregion conekta order
            //customer.name 
            #region send to conekta

            //TODO i can not remove this because of the charge.amount.ToString() + "00", need to use the format function
            conekta.Order response = new conekta.Order().create(@"
                        {""currency"": """ + order.currency + @""" ,
                         ""customer_info"": {
                	        ""name"": """ + Regex.Replace(customer.name, "[0-9]", "") + @""", 
                	        ""phone"": """ + customer.phone + @""",
                	        ""email"": """ + customer.email + @"""
                        },
                         ""line_items"": [{
                           ""name"": """ + product.name + @""",
                           ""unit_price"": " + product.unit_price.ToString() + "00" + @",
                           ""quantity"": 1
                         }],
                        ""charges"": [{
                	        ""payment_method"": {
                		        ""type"": """ + charge.payment_method.type + @"""
                	        },
                	    ""amount"": " + charge.amount.ToString() + "00" + @"
                        }]
                        }");

            //TODO this works but do not understand the expire_at attribute
            //conekta.Order response = new conekta.Order().create(@"
            //            {""currency"": """ + order.currency + @""" ,
            //             ""customer_info"": {
            //    	        ""name"": """ + Regex.Replace(customer.name, "[0-9]", "") + @""", 
            //    	        ""phone"": """ + customer.phone + @""",
            //    	        ""email"": """ + customer.email + @"""
            //            },
            //             ""line_items"": [{
            //               ""name"": """ + product.name + @""",
            //               ""unit_price"": " + product.unit_price.ToString() + "00" + @",
            //               ""quantity"": 1
            //             }],
            //            ""charges"": [{
            //    	        ""payment_method"": {
            //    		        ""type"": """ + charge.payment_method.type + @""",
            //    		        ""expires_at"": " + DateTime.Now.AddDays(3).ToString("yyyyMMddHH") + @"
            //    	        },
            //    	    ""amount"": " + charge.amount.ToString() + "00" + @"
            //            }]
            //            }");

            #endregion send to conekta

            #region enpadi printed order

            #region response data

            JObject responseCharge = (JObject)response.charges.data[0];

            string responseReference = ((JObject)responseCharge.GetValue("payment_method")).GetValue("reference").ToString();
            string responsePaymentMethod = ((JObject)responseCharge.GetValue("payment_method")).GetValue("service_name").ToString();
            decimal responseAmount = decimal.Parse(responseCharge.GetValue("amount").ToString()) / 100;
            string responseDescription = responseCharge.GetValue("description").ToString();
            string responseOrderId = responseCharge.GetValue("order_id").ToString();
            string responseOrderCreationDate = responseCharge.GetValue("created_at").ToString();
            string responseOrderPaymentStatus = responseCharge.GetValue("status").ToString();
            Decimal responseFee = decimal.Parse(responseCharge.GetValue("fee").ToString()) / 100;
            //string responseMessage = response.message;

            #endregion response data

            #region barcode
            String barCodeUrl = String.Empty;
            ////TODO: need to check how to do this correctly
            //Barcode barcode = new Barcode(responseReference, TYPE.CODE128);
            //barcode.Encode(TYPE.CODE128, barcode.ToString());
            //barCodeUrl ="../UpLoads/CodeBars/" + responseOrderId + ".png";
            //barcode.SaveImage(Server.MapPath("~/UpLoads/CodeBars/") + responseOrderId + ".png", BarcodeLib.SaveTypes.PNG);
            //Thread.Sleep(2000);

            #endregion barcode

            pagoTienda ordenTienda = new pagoTienda()
            {
                id = responseOrderId,
                description = responseDescription,
                //error_message = responseMessage,
                //authorization = response.Authorization,
                amount = responseAmount,
                //operation_type = response.OperationType,
                type = responsePaymentMethod,
                reference = responseReference,
                barcode_url = barCodeUrl,
                order_id = responseOrderId,
                //transaction_type = response.TransactionType,
                creation_date = responseOrderCreationDate,
                status = responseOrderPaymentStatus,
                //method = response.Method
                fee = responseFee
            };

            #endregion enpadi printed order

            #endregion conekta

            //todo: save orden tienda

            paystubModel paystub = new paystubModel();
            paystub.paystub_type_id = Guid.Parse(paystubModel.keys.PagoTienda);
            paystub.created_by_email = usuario.email;
            paystub.open_pay_charge_id = String.Empty;
            paystub.open_pay_order_id = responseOrderId;

            paystubModel.Save(paystub);

            ViewBag.MontoRecarga = monto;
            return View(ordenTienda);
        }

        // GET: PanelUsuarios
        public ActionResult Transferir(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        // GET: PanelUsuarios
        public ActionResult Servicios(string tipo, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (tipo == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //strSQL = String.Format("SELECT SKU, Nombre, Descripcion, ReferenciaTipo, ReferenciaLongitud, Instrucciones, Compañia, imagenCompañia, Tipo, imagenTipo, Activo FROM Servicios WHERE FechaBaja IS NULL AND Activo = 1 AND Tipo = '{0}' ORDER BY Nombre", tipo);
            //IEnumerable<servicios> s = db.Database.SqlQuery<servicios>(strSQL);
            List<servicios> s = db.servicios.Where(x => x.Activo == true && x.Tipo == tipo && x.FechaBaja == null).OrderBy(x => x.Nombre).ToList();

            return View(s);
        }

        // GET: PanelUsuarios
        public ActionResult ServiciosCaptura(string sku, string referencia, string skin)
        {

            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            //strSQL = String.Format("SELECT SKU, Nombre, Descripcion, ReferenciaTipo, ReferenciaLongitud, Instrucciones, Compañia, imagenCompañia, Tipo, imagenTipo, Activo FROM Servicios WHERE FechaBaja IS NULL AND Activo = 1 AND SKU = '{0}'", sku);
            //IEnumerable<servicios> s = db.Database.SqlQuery<servicios>(strSQL);
            List<servicios> s = db.servicios.Where(x => x.Activo == true && x.FechaBaja == null && x.SKU == sku).ToList();

            pagoServicio oPago = new pagoServicio()
            {
                tarjetaID = usuario.tarjetaID,
                titular = String.Format("{0} {1} {2}", usuario.datNombre, usuario.datPaterno, usuario.datMaterno),
                sku = sku,
                referencia = referencia,
                tipoPago = "MELE",
                nombre = s.FirstOrDefault().Nombre,
                compañia = s.FirstOrDefault().Compañia,
                imagenCompañia = s.FirstOrDefault().imagenCompañia,
                tipo = s.FirstOrDefault().Tipo,
                imagenTipo = s.FirstOrDefault().imagenTipo,
                estatus = 2
            };

            oPago.fechaLocal = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
            oPago.horaLocal = String.Format("{0:HH:mm:ss}", DateTime.Now);
            oPago.fechaContable = String.Format("{0:dd/MM/yyyy}", DateTime.Now);

            PxUniversal ws = new PxUniversal() { Url = DIESTEL_URL, Timeout = 45000 };
            NetworkCredential oCredenciales = new NetworkCredential() { UserName = DIESTEL_USUARIO, Password = DIESTEL_PASSWORD };

            cCampo[] req = new cCampo[11];
            req[0] = new cCampo() { sCampo = "IDGRUPO", sValor = DIESTEL_GRUPO };
            req[1] = new cCampo() { sCampo = "IDCADENA", sValor = DIESTEL_CADENA };
            req[2] = new cCampo() { sCampo = "IDTIENDA", sValor = DIESTEL_TIENDA };
            req[3] = new cCampo() { sCampo = "IDPOS", sValor = 1 };
            req[4] = new cCampo() { sCampo = "IDCAJERO", sValor = 1 };
            req[5] = new cCampo() { sCampo = "FECHALOCAL", sValor = oPago.fechaLocal };
            req[6] = new cCampo() { sCampo = "HORALOCAL", sValor = oPago.horaLocal };
            req[7] = new cCampo() { sCampo = "SKU", sValor = sku };
            req[8] = new cCampo() { sCampo = "FECHACONTABLE", sValor = oPago.fechaContable };
            req[9] = new cCampo() { sCampo = "REFERENCIA", sValor = referencia };

            cCampo[] resp;

            try
            {
                db.pagoServicios.Add(oPago);
                db.SaveChanges();

                //var resultado = PagoServiciosModel.Save(oPago);
                req[10] = new cCampo() { sCampo = "TRANSACCION", sValor = oPago.pagoID };

                ws.Credentials = oCredenciales;
                resp = ws.Info(req);
                //TODO: this is imporant, nedd to catch all codes and make a log in systems errors
                if (((Enpadi_WebUI.WSDiestel.cCampo[])resp)[0].sCampo.ToString() != "CODIGORESPUESTA")
                {
                    oPago.Campos = resp;
                }
                return View(oPago);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return View(oPago);
            }
        }

        [HttpPost]
        public ActionResult ServiciosProcesa(pagoServicio oPago, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            decimal dMonto = 0;
            decimal dIva = (decimal)0.16;
            bool dConvert = false;
            //Valida Saldo del Usuario
            if (oPago != null)
            {
                dMonto = (Decimal)oPago.monto;
                dConvert = true;
            }

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
            strSQL = String.Format("SELECT ISNULL(SUM(abonoTotal-cargoTotal),0) As Total FROM Transacciones WHERE tarjetaID = {0} AND FechaBaja IS NUll", usuario.tarjetaID);
            IEnumerable<total> saldoActual = db.Database.SqlQuery<total>(strSQL);

            if (saldoActual.ToList().FirstOrDefault().Total >= dMonto)
            {
                PxUniversal ws = new PxUniversal() { Url = DIESTEL_URL, Timeout = 45000 };
                NetworkCredential oCredenciales = new NetworkCredential() { UserName = DIESTEL_USUARIO, Password = DIESTEL_PASSWORD };

                cCampo[] req = new cCampo[17];
                req[0] = new cCampo() { sCampo = "IDGRUPO", sValor = DIESTEL_GRUPO };
                req[1] = new cCampo() { sCampo = "IDCADENA", sValor = DIESTEL_CADENA };
                req[2] = new cCampo() { sCampo = "IDTIENDA", sValor = DIESTEL_TIENDA };
                req[3] = new cCampo() { sCampo = "IDPOS", sValor = 1 };
                req[4] = new cCampo() { sCampo = "IDCAJERO", sValor = 1 };
                req[5] = new cCampo() { sCampo = "FECHALOCAL", sValor = oPago.fechaLocal };
                req[6] = new cCampo() { sCampo = "HORALOCAL", sValor = oPago.horaLocal };
                req[7] = new cCampo() { sCampo = "TRANSACCION", sValor = oPago.pagoID };
                req[8] = new cCampo() { sCampo = "SKU", sValor = oPago.sku };
                req[9] = new cCampo() { sCampo = "REFERENCIA", sValor = oPago.referencia };
                req[10] = new cCampo() { sCampo = "FECHACONTABLE", sValor = oPago.fechaContable };
                req[11] = new cCampo() { sCampo = "MONTO", sValor = oPago.monto };
                req[12] = new cCampo() { sCampo = "TIPOPAGO", sValor = oPago.tipoPago };

                if (oPago.dv != null)
                    req[13] = new cCampo() { sCampo = "DV", sValor = oPago.dv };
                if (oPago.comision != null)
                    req[14] = new cCampo() { sCampo = "COMISION", sValor = oPago.comision };
                if (oPago.token != null)
                    req[15] = new cCampo() { sCampo = "TOKEN", sValor = oPago.token };
                if (oPago.referencia2 != null)
                    req[16] = new cCampo() { sCampo = "REFERENCIA2", sValor = oPago.referencia2 };

                cCampo[] resp;

                try
                {
                    //add petition as it was send to partner to process
                    Coffee.SaveEnpadiPetition(oPago, oPago.Id, oPago.pagoID);

                    ws.Credentials = oCredenciales;
                    resp = ws.Ejecuta(req);
                    oPago.Campos = resp;

                    //save petition as it was recived by partner to process
                    foreach (cCampo campo in resp)
                    {
                        Coffee.SaveEnpadiResponse(campo, oPago.Id, oPago.pagoID);
                    }

                    pagoServicio rowPago = db.pagoServicios.Where(x => x.pagoID == oPago.pagoID).FirstOrDefault();

                    foreach (var item in resp)
                    {
                        switch (item.sCampo)
                        {
                            case "MONTO":
                                oPago.monto = Decimal.Parse(item.sValor.ToString().Replace('$',' '));
                                rowPago.monto = Decimal.Parse(item.sValor.ToString().Replace('$',' '));
                                db.Entry(rowPago).Property(x => x.monto).IsModified = true;
                                break;
                            case "COMISION":
                                oPago.comision = Decimal.Parse(item.sValor.ToString().Replace('$', ' '));
                                rowPago.comision = Decimal.Parse(item.sValor.ToString().Replace('$', ' '));
                                db.Entry(rowPago).Property(x => x.comision).IsModified = true;
                                break;

                            case "CODIGORESPUESTA":
                                oPago.codigoRespuesta = Convert.ToInt32(item.sValor.ToString());
                                rowPago.codigoRespuesta = Convert.ToInt32(item.sValor.ToString());
                                db.Entry(rowPago).Property(x => x.codigoRespuesta).IsModified = true;
                                //Si la respuesta incluye un CODIGO RESPUESTA mayor a 0 se da por hecho que el pago fue RECHAZADO
                                if (oPago.codigoRespuesta > 0)
                                {
                                    //Se actualiza la informacion adicional de la operacion en la DB
                                    rowPago.monto = oPago.monto;
                                    db.Entry(rowPago).Property(x => x.monto).IsModified = true;
                                    rowPago.comision = oPago.comision;
                                    db.Entry(rowPago).Property(x => x.comision).IsModified = true;
                                }
                                break;
                            case "CODIGORESPUESTADESCR":
                                oPago.codigoRespuestaDescr = item.sValor.ToString();
                                rowPago.codigoRespuestaDescr = item.sValor.ToString();
                                db.Entry(rowPago).Property(x => x.codigoRespuestaDescr).IsModified = true;
                                break;
                            case "AUTORIZACION":
                                oPago.autorizacion = item.sValor.ToString();
                                rowPago.autorizacion = item.sValor.ToString();
                                db.Entry(rowPago).Property(x => x.autorizacion).IsModified = true;
                                //Si la respuesta incluye el campo AUTORIZACION se da por hecho que el pago fue ACEPTADO
                                oPago.estatus = 1;
                                rowPago.estatus = 1;
                                db.Entry(rowPago).Property(x => x.estatus).IsModified = true;
                                //Se debita el Monto Pagado del Monedero
                                string sTransaccion = DateTime.Now.ToString("yyyyMMddHHmmss");
                                trans cargo = new trans()
                                {
                                    origenID = 1,
                                    tipoID = 2,
                                    modoID = 1,
                                    socioID = socio.SocioID.Pago_de_servicios,
                                    terminalID = 1,
                                    monedaID = 484,
                                    tarjetaID = usuario.tarjetaID,
                                    monto = dMonto,
                                    abono1 = 0,
                                    cargo1 = dMonto,
                                    abono2 = 0,
                                    cargo2 = 0,
                                    abono3 = 0,
                                    cargo3 = 0,
                                    abonoTotal = 0,
                                    cargoTotal = dMonto,
                                    tarjeta = usuario.tcNum.Substring(usuario.tcNum.Length - 4),
                                    transaccion = sTransaccion,
                                    Concepto = oPago.compañia,
                                    descripcion = String.Format("{0} - {1} ({2})", oPago.sku, oPago.compañia, oPago.referencia),
                                    referencia = oPago.pagoID.ToString(),
                                    estatusID = 1,
                                    aprobacion = oPago.autorizacion,
                                    ip = "189.25.169.74",
                                    geoCountryCode = 0,
                                    geoCountryName = String.Empty,
                                    geoRegion = String.Empty,
                                    geoCity = String.Empty,
                                    geoPostalCode = String.Empty,
                                    fechaRegistro = DateTime.Now
                                };

                                // Realiza el calculo de Comision a Aplicar
                                strSQL = string.Format("SELECT ISNULL((SELECT TOP 1 socioID As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID > 0 AND tipoID = 1 AND tarjetaID = {0} ORDER BY fechaRegistro DESC),0) As total", usuario.tarjetaID);
                                IEnumerable<contador> ultimoDeposito = db.Database.SqlQuery<contador>(strSQL);
                                int iSocio = ultimoDeposito.ToList().FirstOrDefault().total;

                                if (oPago.compañia == "IUSACELL" ||
                                    oPago.compañia == "UNEFON" ||
                                    oPago.compañia == "NEXTEL" ||
                                    oPago.compañia == "MOVISTAR" ||
                                    oPago.compañia == "VIRGIN" ||
                                    oPago.compañia == "TELCEL")
                                {
                                    cargo.socioID = socio.SocioID.Tiempo_aire;
                                    decimal abono2_monto = dMonto * (decimal)0.06;
                                    decimal abono2_desglozado = Decimal.Divide(abono2_monto, (decimal)1.16);
                                    decimal abono2_redondeado = Decimal.Round(abono2_desglozado, 2);
                                    cargo.abono2 = abono2_redondeado;
                                    cargo.abono2iva = Decimal.Round((cargo.abono2 * dIva), 2);
                                    if ((dMonto == 50 || dMonto == 100) && iSocio != 1)
                                    {
                                        cargo.cargo4 = (decimal)2.00;
                                        cargo.cargo4iva = (decimal)0.32;

                                        cargo.cargoTotal += cargo.cargo4 + cargo.cargo4iva;
                                    }

                                    decimal exchangeRateDown = Tools.GetPesoToDollarRate(-1);
                                    decimal exchangeRateUp = Tools.GetPesoToDollarRate(1);
                                    decimal TotalAmountUSD = (cargo.monto / exchangeRateDown);
                                    Decimal TotalAmountMX = cargo.monto;

                                    cargo.ComissionMX = cargo.cargo2 + cargo.cargo2iva + cargo.cargo3 + cargo.cargo3iva + cargo.cargo4 + cargo.cargo4iva + cargo.cargo6 + cargo.cargo6iva;
                                    cargo.ComissionUSD = cargo.ComissionMX / exchangeRateDown;
                                    cargo.TotalMX = TotalAmountMX;
                                    cargo.TotalUSD = TotalAmountUSD;
                                    cargo.ExchangeRateDown = exchangeRateDown;
                                    cargo.ExchangeRateUp = exchangeRateUp;
                                    cargo.PaidWithCredit = false;

                                    cargo.ServiceInDlls = TotalAmountUSD + cargo.ComissionUSD;
                                    cargo.ServiceInMXN = TotalAmountMX + cargo.ComissionMX;
                                }
                                else
                                {
                                    decimal dComision = System.Convert.ToDecimal(oPago.comision);
                                    Decimal cargo2_desglozado = dComision / (decimal)1.16;
                                    cargo.cargo2 = Decimal.Round(cargo2_desglozado, 2);
                                    cargo.cargo2iva = Decimal.Round((cargo.cargo2 * dIva), 2);

                                    cargo.cargo6 = Decimal.Round((cargo.cargo2 * (decimal)0.40), 2);
                                    cargo.cargo6iva = Decimal.Round((cargo.cargo6 * dIva), 2);
                                    cargo.facturable1 = cargo.cargo2 - cargo.cargo6;
                                    cargo.facturable1iva = Decimal.Round((cargo.facturable1 * dIva), 2);


                                    cargo.cargoTotal += cargo.cargo2 + cargo.cargo2iva + cargo.cargo6 + cargo.cargo6iva;

                                    switch (iSocio)
                                    {
                                        case 1: //Visa-MasterCard
                                            cargo.cargo3 = dMonto * (decimal)0.029;
                                            cargo.cargo3iva = cargo.cargo3 * dIva;
                                            cargo.cargo4 = (decimal)2.90;
                                            cargo.cargo4iva = cargo.cargo4 * dIva;
                                            cargo.cargoTotal += cargo.cargo3 + cargo.cargo3iva + cargo.cargo4 + cargo.cargo4iva;
                                            break;
                                        case 2: //AmericanExpress
                                            cargo.cargo3 = dMonto * (decimal)0.049;
                                            cargo.cargo3iva = cargo.cargo3 * dIva;
                                            cargo.cargo4 = (decimal)2.90;
                                            cargo.cargo4iva = cargo.cargo4 * dIva;
                                            cargo.cargoTotal += cargo.cargo3 + cargo.cargo3iva + cargo.cargo4 + cargo.cargo4iva;
                                            break;
                                        case 3: //Banco
                                            cargo.cargo4 = (decimal)10.00;
                                            cargo.cargo4iva = cargo.cargo4 * dIva;
                                            cargo.cargoTotal += cargo.cargo3 + cargo.cargo3iva + cargo.cargo4 + cargo.cargo4iva;
                                            break;
                                        case 4: //Tienda
                                            cargo.cargo3 = dMonto * (decimal)0.029;
                                            cargo.cargo3iva = cargo.cargo3 * dIva;
                                            cargo.cargo4 = (decimal)2.90;
                                            cargo.cargo4iva = cargo.cargo4 * dIva;
                                            cargo.cargoTotal += cargo.cargo3 + cargo.cargo3iva + cargo.cargo4 + cargo.cargo4iva;
                                            break;
                                    }

                                    decimal exchangeRateDown = Tools.GetPesoToDollarRate(-1);
                                    decimal exchangeRateUp = Tools.GetPesoToDollarRate(1);
                                    decimal TotalAmountUSD = (cargo.monto / exchangeRateDown);
                                    Decimal TotalAmountMX = cargo.monto;

                                    cargo.ComissionMX = cargo.cargo2 + cargo.cargo2iva + cargo.cargo3 + cargo.cargo3iva +  cargo.cargo4 + cargo.cargo4iva + cargo.cargo6 + cargo.cargo6iva;
                                    cargo.ComissionUSD = cargo.ComissionMX/exchangeRateDown;
                                    cargo.TotalMX = TotalAmountMX;
                                    cargo.TotalUSD = TotalAmountUSD;
                                    cargo.ExchangeRateDown = exchangeRateDown;
                                    cargo.ExchangeRateUp = exchangeRateUp;
                                    cargo.PaidWithCredit = false;

                                    cargo.ServiceInDlls = TotalAmountUSD + cargo.ComissionUSD;
                                    cargo.ServiceInMXN = TotalAmountMX + cargo.ComissionMX;
                                }

                                //trans.Save(cargo);
                                db.transacciones.Add(cargo);
                                db.Entry(cargo).State = System.Data.Entity.EntityState.Added;
                                recompensa r = new recompensa()
                                {
                                    tarjetaID = usuario.tarjetaID,
                                    origenID = 1,
                                    socioID = socio.SocioID.Pago_de_servicios,
                                    PartnerId = socio.SocioID.Pago_de_servicios_guid_id,
                                    transaccion = sTransaccion,
                                    concepto = "Recompensa Pago de Servicios",
                                    abono = dMonto * (decimal)0.01,
                                    cargo = 0
                                };
                                db.recompensas.Add(r);
                                db.Entry(r).State = System.Data.Entity.EntityState.Added;
                                db.SaveChanges();
                                break;
                            case "REFERENCIA2":
                                oPago.referencia2 = item.sValor.ToString();
                                rowPago.referencia2 = item.sValor.ToString();
                                db.Entry(rowPago).Property(x => x.referencia2).IsModified = true;
                                break;
                            case "TOKEN":
                                oPago.token = item.sValor.ToString();
                                rowPago.token = item.sValor.ToString();
                                db.Entry(rowPago).Property(x => x.token).IsModified = true;
                                break;
                            case "BRAND":
                                oPago.brand = item.sValor.ToString();
                                rowPago.brand = item.sValor.ToString();
                                db.Entry(rowPago).Property(x => x.brand).IsModified = true;
                                break;
                            case "PROVEEDOR":
                                oPago.proveedor = item.sValor.ToString();
                                rowPago.proveedor = item.sValor.ToString();
                                db.Entry(rowPago).Property(x => x.proveedor).IsModified = true;
                                break;
                            case "LEYENDA":
                                oPago.leyenda = item.sValor.ToString();
                                rowPago.leyenda = item.sValor.ToString();
                                db.Entry(rowPago).Property(x => x.leyenda).IsModified = true;
                                break;
                            case "LEYENDA1":
                                oPago.leyenda1 = item.sValor.ToString();
                                rowPago.leyenda1 = item.sValor.ToString();
                                db.Entry(rowPago).Property(x => x.leyenda1).IsModified = true;
                                break;
                            case "LEYENDA2":
                                oPago.leyenda2 = item.sValor.ToString();
                                rowPago.leyenda2 = item.sValor.ToString();
                                db.Entry(rowPago).Property(x => x.leyenda2).IsModified = true;
                                break;
                            default:
                                break;
                        }
                    }

                    db.SaveChanges();

                    //Con este Codigo de Respuesta se Requiere el envio de Cancelación
                    if (oPago.codigoRespuesta == 8 || oPago.codigoRespuesta == 71 || oPago.codigoRespuesta == 72)
                    {
                        req[0] = new cCampo() { sCampo = "IDGRUPO", sValor = 7 };
                        req[1] = new cCampo() { sCampo = "IDCADENA", sValor = 1 };
                        req[2] = new cCampo() { sCampo = "IDTIENDA", sValor = 1 };
                        req[3] = new cCampo() { sCampo = "IDPOS", sValor = 1 };
                        req[4] = new cCampo() { sCampo = "IDCAJERO", sValor = 1 };
                        req[5] = new cCampo() { sCampo = "FECHALOCAL", sValor = oPago.fechaLocal };
                        req[6] = new cCampo() { sCampo = "HORALOCAL", sValor = oPago.horaLocal };
                        req[7] = new cCampo() { sCampo = "TRANSACCION", sValor = oPago.pagoID };
                        req[8] = new cCampo() { sCampo = "SKU", sValor = oPago.sku };
                        req[9] = new cCampo() { sCampo = "REFERENCIA", sValor = oPago.referencia };
                        req[10] = new cCampo() { sCampo = "FECHACONTABLE", sValor = oPago.fechaContable };
                        req[11] = new cCampo() { sCampo = "AUTORIZACION", sValor = oPago.autorizacion };
                        req[12] = null;
                        req[13] = null;
                        req[14] = null;
                        req[15] = null;
                        ws.Credentials = oCredenciales;
                        resp = ws.Reversa(req);

                        foreach (var item in resp)
                        {
                            switch (item.sCampo)
                            {
                                case "CODIGORESPUESTA":
                                    if (Convert.ToInt32(item.sValor.ToString()) > 0)
                                    {
                                        resp = ws.Reversa(req);
                                        foreach (var i in resp)
                                        {
                                            switch (i.sCampo)
                                            {
                                                case "CODIGORESPUESTA":
                                                    if (Convert.ToInt32(item.sValor.ToString()) > 0)
                                                    {
                                                        resp = ws.Reversa(req);
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                    break;
                            }
                        }
                    }

                    return View(oPago);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    if (ex.Message == "Se excedió el tiempo de espera de la operación")
                    {
                        try
                        {
                            resp = ws.Reversa(req);
                            foreach (var item in resp)
                            {
                                switch (item.sCampo)
                                {
                                    case "CODIGORESPUESTA":
                                        if (Convert.ToInt32(item.sValor.ToString()) > 0)
                                        {
                                            resp = ws.Reversa(req);
                                            foreach (var i in resp)
                                            {
                                                switch (i.sCampo)
                                                {
                                                    case "CODIGORESPUESTA":
                                                        if (Convert.ToInt32(item.sValor.ToString()) > 0)
                                                        {
                                                            resp = ws.Reversa(req);
                                                        }
                                                        break;
                                                }
                                            }
                                        }
                                        break;
                                }
                            }

                        }
                        catch (Exception ex1)
                        {
                            Console.WriteLine(ex1.Message);
                            if (ex.Message == "Se excedió el tiempo de espera de la operación")
                            {
                                try
                                {
                                    resp = ws.Reversa(req);
                                    foreach (var item in resp)
                                    {
                                        switch (item.sCampo)
                                        {
                                            case "CODIGORESPUESTA":
                                                if (Convert.ToInt32(item.sValor.ToString()) > 0)
                                                {
                                                    resp = ws.Reversa(req);
                                                    foreach (var i in resp)
                                                    {
                                                        switch (i.sCampo)
                                                        {
                                                            case "CODIGORESPUESTA":
                                                                if (Convert.ToInt32(item.sValor.ToString()) > 0)
                                                                {
                                                                    resp = ws.Reversa(req);
                                                                }
                                                                break;
                                                        }
                                                    }
                                                }
                                                break;
                                        }
                                    }

                                }
                                catch (Exception ex2)
                                {
                                    Console.WriteLine(ex2.Message);
                                    if (ex.Message == "Se excedió el tiempo de espera de la operación")
                                    {
                                        try
                                        {
                                            resp = ws.Reversa(req);
                                            foreach (var item in resp)
                                            {
                                                switch (item.sCampo)
                                                {
                                                    case "CODIGORESPUESTA":
                                                        if (Convert.ToInt32(item.sValor.ToString()) > 0)
                                                        {
                                                            resp = ws.Reversa(req);
                                                            foreach (var i in resp)
                                                            {
                                                                switch (i.sCampo)
                                                                {
                                                                    case "CODIGORESPUESTA":
                                                                        if (Convert.ToInt32(item.sValor.ToString()) > 0)
                                                                        {
                                                                            resp = ws.Reversa(req);
                                                                        }
                                                                        break;
                                                                }
                                                            }
                                                        }
                                                        break;
                                                }
                                            }

                                        }
                                        catch (Exception ex3)
                                        {
                                            Console.WriteLine(ex3.Message);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    return View(oPago);
                }
            }
            else
            {
                oPago.estatus = 2; oPago.codigoRespuesta = 908; oPago.codigoRespuestaDescr = "Saldo Insuficiente. Recargue su Cuenta";

                pagoServicio rowPago = db.pagoServicios.Find(oPago.pagoID);
                rowPago.estatus = 2; rowPago.codigoRespuesta = 908; rowPago.codigoRespuestaDescr = "Saldo Insuficiente. Recargue su Cuenta";
                db.Entry(rowPago).Property(x => x.estatus).IsModified = true;
                db.Entry(rowPago).Property(x => x.codigoRespuesta).IsModified = true;
                db.Entry(rowPago).Property(x => x.codigoRespuestaDescr).IsModified = true;
                db.SaveChanges();

                return View(oPago);
            }
        }

        public ActionResult ServiciosImpresion(string t, string m, string a, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            //strSQL = string.Format("SELECT pagoID, tarjetaID, sku, nombre, compañia, referencia, referencia2, CONVERT(VARCHAR(10), monto) AS monto, CONVERT(VARCHAR(10), comision) As comision, fechaLocal, horaLocal, fechaContable, codigoRespuesta, codigoRespuestaDescr, tipoPago, autorizacion, brand, leyenda, leyenda1, leyenda2, dv, token, proveedor, estatus, fechaRegistro FROM PagoServicios WHERE tarjetaID = {0} AND Monto = {1} AND autorizacion = '{2}'", t, m, a);
            //pagoServicio p = db.Database.SqlQuery<pagoServicio>(strSQL).FirstOrDefault();

            Int32 _t = Int32.Parse(t);
            Decimal? _m = Decimal.Parse(m);
            pagoServicio p = db.pagoServicios.Where(x=>  x.tarjetaID == _t && x.monto == _m && x.autorizacion == a).FirstOrDefault();
            return View(p);
        }

        [HttpPost]
        public ActionResult ServiciosImpresion(pagoServicio oPago, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View(oPago);
        }

        public ActionResult SantiagoNL(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();
            return View();
        }

        public ActionResult SantiagoNLBancos(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();
            return View();
        }

        public ActionResult SantiagoNLPredial(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();
            return View();
        }

        public ActionResult SantiagoNLTiendas(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();
            return View();
        }

        public ActionResult SantiagoNLResp(santiagoPredial oPredial, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            //Valida Saldo del Usuario
            decimal dMonto = 0;
            bool dConvert = decimal.TryParse(oPredial.saldo.ToString(), out dMonto);

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
            strSQL = String.Format("SELECT ISNULL(SUM(abonoTotal-cargoTotal),0) As Total FROM Transacciones WHERE tarjetaID = {0} AND FechaBaja IS NUll", usuario.tarjetaID);
            IEnumerable<total> saldoActual = db.Database.SqlQuery<total>(strSQL);

            if (saldoActual.ToList().FirstOrDefault().Total >= dMonto)
            {
                string sTransaccion = DateTime.Now.ToString("yyyyMMddHHmmss");
                oPredial.estatus = 1; oPredial.codigoRespuesta = 0; oPredial.Transaccion = sTransaccion;

                //Se debita el Monto Pagado del Monedero
                trans cargo = new trans()
                {
                    origenID = 1,
                    tipoID = 2,
                    modoID = 1,
                    socioID = 4,
                    terminalID = 1,
                    monedaID = 484,
                    tarjetaID = usuario.tarjetaID,
                    monto = dMonto,
                    abono1 = 0,
                    cargo1 = dMonto,
                    abono2 = 0,
                    cargo2 = 0,
                    abono3 = 0,
                    cargo3 = 0,
                    abonoTotal = 0,
                    cargoTotal = dMonto,
                    tarjeta = usuario.tcNum.Substring(usuario.tcNum.Length - 4),
                    transaccion = sTransaccion,
                    Concepto = "Pago de Predial",
                    descripcion = String.Format("{0}", oPredial.expediente),
                    referencia = oPredial.expediente.ToString(),
                    estatusID = 1,
                    aprobacion = "480276",
                    ip = "189.25.169.74",
                    geoCountryCode = 0,
                    geoCountryName = String.Empty,
                    geoRegion = String.Empty,
                    geoCity = String.Empty,
                    geoPostalCode = String.Empty,
                    fechaRegistro = DateTime.Now
                };

                decimal exchangeRateDown = Tools.GetPesoToDollarRate(-1);
                decimal exchangeRateUp = Tools.GetPesoToDollarRate(1);
                decimal TotalAmountUSD = (cargo.monto / exchangeRateDown);
                Decimal TotalAmountMX = cargo.monto;

                cargo.ComissionMX = 0;
                cargo.ComissionUSD = 0;
                cargo.TotalMX = TotalAmountMX;
                cargo.TotalUSD = TotalAmountUSD;
                cargo.ExchangeRateDown = exchangeRateDown;
                cargo.ExchangeRateUp = exchangeRateUp;
                cargo.PaidWithCredit = false;

                cargo.ServiceInDlls = TotalAmountUSD + cargo.ComissionUSD;
                cargo.ServiceInMXN = TotalAmountMX + cargo.ComissionMX;

                db.transacciones.Add(cargo);
                db.Entry(cargo).State = System.Data.Entity.EntityState.Added;
                db.SaveChanges();

                //trans.Save(cargo);
                //db.transacciones.Add(cargo);
                recompensa r = new recompensa() { tarjetaID = usuario.tarjetaID, origenID = 1, socioID = 4, transaccion = sTransaccion, concepto = "Recompensa Pago de Predial", abono = dMonto * (decimal)0.01, cargo = 0 };
                db.recompensas.Add(r);

                db.SaveChanges();
            }
            else
            {
                oPredial.estatus = 0; oPredial.codigoRespuesta = 2; oPredial.codigoRespuestaDescr = "Saldo Insuficiente. Recargue su Cuenta";
            }

            return View(oPredial);
        }

        public ActionResult Comprar(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            strSQL = string.Format("SELECT * FROM posTiendas WHERE FechaBaja IS NULL");
            IEnumerable<posTienda> tiendas = db.Database.SqlQuery<posTienda>(strSQL);

            return View(tiendas.ToList());
        }

        public ActionResult ComprarCategoria(int id, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            strSQL = string.Format("SELECT * FROM posCategorias WHERE FechaBaja IS NULL AND tiendaID = {0}", id);
            IEnumerable<posCategoria> categorias = db.Database.SqlQuery<posCategoria>(strSQL);

            return View(categorias.ToList());
        }

        public ActionResult ComprarProducto(int id, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            //Skin Application
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            strSQL = " SELECT p.productoID, p.tiendaID, t.nombre As tienda, t.socioID, p.categoriaID, c.categoria As categoria, p.producto, p.descripcion, p.imagen, p.unidadID, u.unidad As unidad, p.monto, p.monedaID, m.codigo As abreviacion, p.fechaAlta "
                   + " FROM posProductos p "
                   + " LEFT JOIN posTiendas t ON p.tiendaID = t.tiendaID "
                   + " LEFT JOIN posCategorias c ON p.categoriaID = c.categoriaID "
                   + " LEFT JOIN posUnidades u ON p.unidadID = u.unidadID "
                   + " LEFT JOIN Monedas m ON p.monedaID = m.monedaID "
                   + " WHERE p.fechaBaja IS NULL AND p.categoriaID = " + id.ToString();
            IEnumerable<posProducto> productos = db.Database.SqlQuery<posProducto>(strSQL);

            return View(productos.ToList());
        }

        public ActionResult ComprarPago(int id, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            //Skin Application
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            ViewModelTiendaVirtual vmTV = new ViewModelTiendaVirtual();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            strSQL = " SELECT p.productoID, p.tiendaID, t.nombre As tienda, t.socioID, p.categoriaID, c.categoria As categoria, p.producto, p.descripcion, p.imagen, p.unidadID, u.unidad As unidad, p.monto, p.monedaID, m.codigo As abreviacion, p.fechaAlta "
                   + " FROM posProductos p "
                   + " LEFT JOIN posTiendas t ON p.tiendaID = t.tiendaID "
                   + " LEFT JOIN posCategorias c ON p.categoriaID = c.categoriaID "
                   + " LEFT JOIN posUnidades u ON p.unidadID = u.unidadID "
                   + " LEFT JOIN Monedas m ON p.monedaID = m.monedaID "
                   + " WHERE p.fechaBaja IS NULL AND p.productoID = " + id.ToString();
            IEnumerable<posProducto> productos = db.Database.SqlQuery<posProducto>(strSQL);
            vmTV.producto = productos.ToList().FirstOrDefault();

            strSQL = String.Format("SELECT ISNULL(SUM(abonoTotal-cargoTotal),0) As Total FROM Transacciones WHERE tarjetaID = {0} AND FechaBaja IS NUll", usuario.tarjetaID);
            IEnumerable<total> saldoActual = db.Database.SqlQuery<total>(strSQL);
            vmTV.saldoActual = saldoActual.FirstOrDefault().Total;

            return View(vmTV);
        }

        public ActionResult ComprarRespuesta(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            //Skin Application
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        [HttpPost]
        public ActionResult ComprarRespuesta(int productoID, string referencia, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            //Skin Application
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            ViewModelTiendaVirtual vmTV = new ViewModelTiendaVirtual() { productoID = productoID, referencia = referencia };

            strSQL = " SELECT p.productoID, p.tiendaID, t.nombre As tienda, t.socioID, p.categoriaID, c.categoria As categoria, p.producto, p.descripcion, p.imagen, p.unidadID, u.unidad As unidad, p.monto, p.monedaID, m.codigo As abreviacion, p.fechaAlta "
                   + " FROM posProductos p "
                   + " LEFT JOIN posTiendas t ON p.tiendaID = t.tiendaID "
                   + " LEFT JOIN posCategorias c ON p.categoriaID = c.categoriaID "
                   + " LEFT JOIN posUnidades u ON p.unidadID = u.unidadID "
                   + " LEFT JOIN Monedas m ON p.monedaID = m.monedaID "
                   + " WHERE p.fechaBaja IS NULL AND p.productoID = " + productoID.ToString();
            posProducto producto = db.Database.SqlQuery<posProducto>(strSQL).FirstOrDefault();

            vmTV.producto = producto;

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            vmTV.tarjetaID = usuario.tarjetaID;

            strSQL = String.Format("SELECT ISNULL(SUM(abonoTotal-cargoTotal),0) As Total FROM Transacciones WHERE tarjetaID = {0} AND tipoID IN (1,2)", vmTV.tarjetaID);
            IEnumerable<total> saldoActual = db.Database.SqlQuery<total>(strSQL);

            vmTV.saldoActual = saldoActual.ToList().FirstOrDefault().Total;
            vmTV.transaccion = DateTime.Now.ToString("yyyyMMddHHmmss");
            vmTV.aprobacion = "875935";

            if (vmTV.saldoActual >= producto.monto)
            {
                trans cargo = new trans()
                {
                    origenID = 1,
                    tipoID = 2,
                    modoID = 1,
                    socioID = vmTV.producto.socioID,
                    terminalID = vmTV.producto.tiendaID,
                    monedaID = producto.monedaID,
                    tarjetaID = usuario.tarjetaID,
                    monto = producto.monto,
                    abono1 = 0,
                    cargo1 = producto.monto,
                    abono2 = 0,
                    cargo2 = 0,
                    abono3 = 0,
                    abono4 = 0,
                    abonoTotal = 0,
                    cargoTotal = producto.monto,
                    tarjeta = usuario.tcNum.Substring(usuario.tcNum.Length - 4),
                    transaccion = vmTV.transaccion,
                    Concepto = vmTV.producto.tienda,
                    descripcion = string.Format("{0} | {1}", producto.categoria, producto.producto),
                    referencia = referencia,
                    estatusID = 1,
                    aprobacion = vmTV.aprobacion,
                    ip = "189.25.169.74",
                    geoCountryCode = 0,
                    geoCountryName = string.Empty,
                    geoRegion = string.Empty,
                    geoCity = string.Empty,
                    geoPostalCode = string.Empty,
                    fechaRegistro = DateTime.Now
                };


                decimal exchangeRateDown = Tools.GetPesoToDollarRate(-1);
                decimal exchangeRateUp = Tools.GetPesoToDollarRate(1);
                decimal TotalAmountUSD = (cargo.monto / exchangeRateDown);
                Decimal TotalAmountMX = cargo.monto;

                cargo.ComissionMX = 0;
                cargo.ComissionUSD = 0;
                cargo.TotalMX = TotalAmountMX;
                cargo.TotalUSD = TotalAmountUSD;
                cargo.ExchangeRateDown = exchangeRateDown;
                cargo.ExchangeRateUp = exchangeRateUp;
                cargo.PaidWithCredit = false;

                cargo.ServiceInDlls = TotalAmountUSD + cargo.ComissionUSD;
                cargo.ServiceInMXN = TotalAmountMX + cargo.ComissionMX;

                db.transacciones.Add(cargo);
                db.Entry(cargo).State = System.Data.Entity.EntityState.Added;
                db.SaveChanges();

                //trans.Save(cargo);
                //db.transacciones.Add(cargo);
                recompensa r = new recompensa() { tarjetaID = usuario.tarjetaID, origenID = 1, socioID = vmTV.producto.socioID, transaccion = vmTV.transaccion, concepto = vmTV.producto.tienda, abono = producto.monto * (decimal)0.01, cargo = 0 };
                db.recompensas.Add(r);

                db.SaveChanges();
                vmTV.estatus = 1; vmTV.codigoRespuesta = 0;
            }
            else
            {
                vmTV.estatus = 0; vmTV.codigoRespuesta = 2; vmTV.mensajeRespuesta = "Saldo Insuficiente. Recargue su Cuenta";
            }

            return View(vmTV);
        }

        // GET: PanelUsuarios
        public ActionResult TransferirCuentasEnpadi(tarjetasContactos contactos, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            strSQL = string.Format("SELECT * FROM TarjetasContactos WHERE tarjetaID = {0}", usuario.tarjetaID);
            IEnumerable<tarjetasContactos> tarjetasContactos = db.Database.SqlQuery<tarjetasContactos>(strSQL);
            return View(tarjetasContactos);
        }

        // POST: Panel Usuarios
        [HttpPost]
        public ActionResult TransferirCuentasEnpadi(int contacto, string monto, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            decimal dMonto = 0;
            bool dConvert = decimal.TryParse(monto, out dMonto);

            if (dMonto <= 0)
                dMonto = 0;

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
            string sTarjetaEnvia = usuario.tcNum.Substring(usuario.tcNum.Length - 4);

            tarjetas amigo = db.tarjetas.FirstOrDefault(x => x.tarjetaID == contacto);
            string sTarjetaRecibe = amigo.tcNum.Substring(amigo.tcNum.Length - 4);

            if (dConvert)
            {
                strSQL = String.Format("SELECT ISNULL(SUM(abonoTotal-cargoTotal),0) As Total FROM Transacciones WHERE tarjetaID = {0} AND tipoID IN (1,2)", usuario.tarjetaID);
                IEnumerable<total> saldoActual = db.Database.SqlQuery<total>(strSQL);

                if (saldoActual.ToList().FirstOrDefault().Total >= dMonto)
                {
                    string sTransaccion = DateTime.Now.ToString("yyyyMMddHHmmss");
                    string sAprobacion = "329875";

                    trans cargo = new trans()
                    {
                        estatusID = 1,
                        origenID = 2,
                        tipoID = 2,
                        modoID = 1,
                        socioID = 0,
                        terminalID = 0,
                        monedaID = 484,
                        tarjetaID = usuario.tarjetaID,
                        tarjeta = sTarjetaRecibe,
                        transaccion = sTransaccion,
                        monto = dMonto,
                        abono1 = 0,
                        cargo1 = dMonto,
                        abono2 = 0,
                        cargo2 = 0,
                        abono3 = 0,
                        cargo3 = 0,
                        abonoTotal = 0,
                        cargoTotal = dMonto,
                        Concepto = "Transferencia",
                        descripcion = "Transferencia entre Cuentas",
                        referencia = amigo.tcNum,
                        aprobacion = sAprobacion,
                        ip = "189.25.169.74",
                        geoCountryCode = 0,
                        geoCountryName = string.Empty,
                        geoRegion = string.Empty,
                        geoCity = string.Empty,
                        geoPostalCode = string.Empty,
                        fechaRegistro = DateTime.Now
                    };

                    trans abono = new trans()
                    {
                        estatusID = 1,
                        origenID = 2,
                        tipoID = 1,
                        modoID = 1,
                        socioID = 0,
                        terminalID = 0,
                        monedaID = 484,
                        tarjetaID = contacto,
                        tarjeta = sTarjetaEnvia,
                        transaccion = sTransaccion,
                        monto = dMonto,
                        abono1 = dMonto,
                        cargo1 = 0,
                        abono2 = 0,
                        cargo2 = 0,
                        abono3 = 0,
                        cargo3 = 0,
                        abonoTotal = dMonto,
                        cargoTotal = 0,
                        Concepto = "Transferencia",
                        descripcion = "Transferencia entre Cuentas",
                        referencia = usuario.tcNum,
                        aprobacion = sAprobacion,
                        ip = "189.25.169.74",
                        geoCountryCode = 0,
                        geoCountryName = string.Empty,
                        geoRegion = string.Empty,
                        geoCity = string.Empty,
                        geoPostalCode = string.Empty,
                        fechaRegistro = DateTime.Now
                    };

                    // Realiza el calculo de Comision a Aplicar
                    strSQL = string.Format("SELECT ISNULL((SELECT TOP 1 socioID As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID > 0 AND tipoID = 1 AND tarjetaID = {0} ORDER BY fechaRegistro DESC),0) As total", usuario.tarjetaID);
                    IEnumerable<contador> ultimoDeposito = db.Database.SqlQuery<contador>(strSQL);
                    int iSocio = ultimoDeposito.ToList().FirstOrDefault().total;

                    decimal dIva = (decimal)0.16;

                    switch (iSocio)
                    {
                        case 1: //Visa-MasterCard
                            cargo.cargo3 = dMonto * (decimal)0.029;
                            cargo.cargo3iva = cargo.cargo3 * dIva;
                            cargo.cargo4 = (decimal)2.50;
                            cargo.cargo4iva = cargo.cargo4 * dIva;
                            cargo.cargoTotal += cargo.cargo3 + cargo.cargo3iva + cargo.cargo4 + cargo.cargo4iva;
                            break;
                        case 2: //AmericanExpress
                            cargo.cargo3 = dMonto * (decimal)0.049;
                            cargo.cargo3iva = cargo.cargo3 * dIva;
                            cargo.cargo4 = (decimal)2.50;
                            cargo.cargo4iva = cargo.cargo4 * dIva;
                            cargo.cargoTotal += cargo.cargo3 + cargo.cargo3iva + cargo.cargo4 + cargo.cargo4iva;
                            break;
                        case 3: //Banco
                            cargo.cargo4 = (decimal)8.00;
                            cargo.cargo4iva = cargo.cargo4 * dIva;
                            cargo.cargoTotal += cargo.cargo4 + cargo.cargo4iva;
                            break;
                        case 4: //Tienda
                            cargo.cargo3 = dMonto * (decimal)0.029;
                            cargo.cargo3iva = cargo.cargo3 * dIva;
                            cargo.cargo4 = (decimal)2.50;
                            cargo.cargo4iva = cargo.cargo4 * dIva;
                            cargo.cargoTotal += cargo.cargo3 + cargo.cargo3iva + cargo.cargo4 + cargo.cargo4iva;
                            break;
                    }

                    //trans.Save(cargo);
                    //trans.Save(abono);
                    db.transacciones.Add(cargo);
                    db.transacciones.Add(abono);
                    db.SaveChanges();
                    //Procedimiento Valido
                    return RedirectToAction("Index", "Home");
                }
                else
                    ModelState.AddModelError("", "Saldo Insuficiente");
            }
            else
                ModelState.AddModelError("", "Proporcione el Monto a Transferir");

            strSQL = string.Format("SELECT * FROM TarjetasContactos WHERE tarjetaID = {0}", usuario.tarjetaID);
            IEnumerable<tarjetasContactos> tarjetasContactos = db.Database.SqlQuery<tarjetasContactos>(strSQL);
            return View(tarjetasContactos);
        }

        // GET: PanelUsuarios
        public ActionResult Recompensas(int? periodo, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            ViewModelRecompensas vm = new ViewModelRecompensas();

            vm.periodo = 0;
            if (periodo != null)
                vm.periodo = (int)periodo;

            string currentUserId = User.Identity.GetUserId();
            tarjetas user = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
            vm.usuario = user;

            strSQL = " SELECT numero, CONVERT(VARCHAR, Anio) + '  ' + CASE Mes WHEN 1 THEN 'ENERO' WHEN 2 THEN 'FEBRERO' WHEN 3 THEN 'MARZO' WHEN 4 THEN 'ABRIL' WHEN 5 THEN 'MAYO' WHEN 6 THEN 'JUNIO' WHEN 7 THEN 'JULIO' "
                   + " WHEN 8 THEN 'AGOSTO' WHEN 9 THEN 'SEPTIEMBRE' WHEN 10 THEN 'OCTUBRE' WHEN 11 THEN 'NOVIEMBRE' WHEN 12 THEN 'DICIEMBRE' END As Mes "
                   + " FROM ( "
                   + " SELECT 0 As numero, DATEPART(MONTH, DATEADD(MONTH, 0, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, 0, GETDATE())) As Anio "
                   + " UNION "
                   + " SELECT -1 As numero, DATEPART(MONTH, DATEADD(MONTH, -1, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -1, GETDATE())) As Anio "
                   + " UNION "
                   + " SELECT -2 As numero, DATEPART(MONTH, DATEADD(MONTH, -2, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -2, GETDATE())) As Anio "
                   + " UNION "
                   + " SELECT -3 As numero, DATEPART(MONTH, DATEADD(MONTH, -3, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -3, GETDATE())) As Anio "
                   + " UNION "
                   + " SELECT -4 As numero, DATEPART(MONTH, DATEADD(MONTH, -4, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -4, GETDATE())) As Anio "
                   + " UNION "
                   + " SELECT -5 As numero, DATEPART(MONTH, DATEADD(MONTH, -5, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -5, GETDATE())) As Anio "
                   + " UNION "
                   + " SELECT -6 As numero, DATEPART(MONTH, DATEADD(MONTH, -6, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -6, GETDATE())) As Anio "
                   + " UNION "
                   + " SELECT -7 As numero, DATEPART(MONTH, DATEADD(MONTH, -7, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -7, GETDATE())) As Anio "
                   + " UNION "
                   + " SELECT -8 As numero, DATEPART(MONTH, DATEADD(MONTH, -8, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -8, GETDATE())) As Anio "
                   + " UNION "
                   + " SELECT -9 As numero, DATEPART(MONTH, DATEADD(MONTH, -9, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -9, GETDATE())) As Anio "
                   + " UNION "
                   + " SELECT -10 As numero, DATEPART(MONTH, DATEADD(MONTH, -10, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -10, GETDATE())) As Anio "
                   + " UNION "
                   + " SELECT -11 As numero, DATEPART(MONTH, DATEADD(MONTH, -11, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -11, GETDATE())) As Anio "
                   + " UNION "
                   + " SELECT -12 As numero, DATEPART(MONTH, DATEADD(MONTH, -12, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -12, GETDATE())) As Anio "
                   + " ) As Meses "
                   + " ORDER BY numero DESC ";
            IEnumerable<periodo> meses = db.Database.SqlQuery<periodo>(strSQL);
            vm.meses = meses.ToList();

            strSQL = " SELECT r.recompensaID, r.tarjetaID, t.tcNum, o.descripcion As origen, s.nombreComercial AS socio, r.transaccion, r.concepto, r.abono, r.cargo, r.fechaAlta "
                   + " FROM Recompensas r "
                   + " LEFT JOIN tarjetas t ON r.tarjetaID = t.tarjetaID "
                   + " LEFT JOIN Origen o ON r.origenID = o.origenID "
                   + " LEFT JOIN Socios s ON r.socioID = s.socioID "
                   + " WHERE r.fechaBaja IS NULL AND r.tarjetaID = " + user.tarjetaID + " AND DateDiff(MONTH, GetDate(), r.FechaAlta) = " + vm.periodo.ToString();
            IEnumerable<recompensaListado> recompensas = db.Database.SqlQuery<recompensaListado>(strSQL);
            vm.recompensas = recompensas.ToList();

            strSQL = " SELECT COALESCE(SUM(abono-cargo),0) As Total "
                   + " FROM Recompensas "
                   + " WHERE fechaBaja IS NULL AND tarjetaID = " + user.tarjetaID + " AND DateDiff(MONTH, GetDate(), FechaAlta) < " + vm.periodo.ToString();
            IEnumerable<total> saldoInicial = db.Database.SqlQuery<total>(strSQL);
            vm.saldoInicial = saldoInicial.ToList().FirstOrDefault().Total;

            strSQL = " SELECT COALESCE(SUM(abono),0) As Total "
                   + " FROM Recompensas "
                   + " WHERE fechaBaja IS NULL AND tarjetaID = " + user.tarjetaID + " AND DateDiff(MONTH, GetDate(), FechaAlta) = " + vm.periodo.ToString();
            IEnumerable<total> abonos = db.Database.SqlQuery<total>(strSQL);
            vm.abonos = abonos.ToList().FirstOrDefault().Total;

            strSQL = " SELECT COALESCE(SUM(cargo),0) As Total "
                   + " FROM Recompensas "
                   + " WHERE fechaBaja IS NULL AND tarjetaID = " + user.tarjetaID + " AND DateDiff(MONTH, GetDate(), FechaAlta) = " + vm.periodo.ToString();
            IEnumerable<total> cargos = db.Database.SqlQuery<total>(strSQL);
            vm.cargos = cargos.ToList().FirstOrDefault().Total;

            strSQL = " SELECT COALESCE(SUM(abono-cargo),0) As Total "
                   + " FROM Recompensas "
                   + " WHERE fechaBaja IS NULL AND tarjetaID = " + user.tarjetaID + " AND DateDiff(MONTH, GetDate(), FechaAlta) <= " + vm.periodo.ToString();
            IEnumerable<total> saldoFinal = db.Database.SqlQuery<total>(strSQL);
            vm.saldoFinal = saldoFinal.ToList().FirstOrDefault().Total;

            strSQL = " SELECT CASE DATEPART(MONTH, DATEADD(MONTH, " + vm.periodo.ToString() + ", GetDate())) "
                   + " WHEN 1 THEN 'ENERO' WHEN 2 THEN 'FEBRERO' WHEN 3 THEN 'MARZO' WHEN 4 THEN 'ABRIL' "
                   + " WHEN 5 THEN 'MAYO' WHEN 6 THEN 'JUNIO' WHEN 7 THEN 'JULIO' WHEN 8 THEN 'AGOSTO' "
                   + " WHEN 9 THEN 'SEPTIEMBRE' WHEN 10 THEN 'OCTUBRE' WHEN 11 THEN 'NOVIEMBRE' WHEN 12 THEN 'DICIEMBRE' "
                   + " END + ' ' + CONVERT(varchar(10), DATEPART(YEAR, DATEADD(MONTH, " + vm.periodo.ToString() + ", GetDate()))) AS [desc] ";
            IEnumerable<descripcion> descrip = db.Database.SqlQuery<descripcion>(strSQL);
            vm.periodo_descripcion = descrip.ToList().FirstOrDefault().desc;

            return View(vm);
        }

        public ActionResult Terminos(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

    }
}