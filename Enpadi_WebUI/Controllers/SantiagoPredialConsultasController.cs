﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Enpadi_WebUI.Models;
using PagedList;

namespace Enpadi_WebUI.Controllers
{
    public class SantiagoPredialConsultasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: SantiagoPredialConsultas
        public ActionResult Index(string skin, string sortOrder, string currentFilter, string searchString, int? page)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            strSQL = String.Format("SELECT * FROM SantiagoPredialConsulta");
            IEnumerable<santiagoPredialConsulta> consulta = db.Database.SqlQuery<santiagoPredialConsulta>(strSQL);

            int pageSize = 50;
            int pageNumber = (page ?? 1);

            return View(consulta.ToPagedList(pageNumber, pageSize));
        }

        // GET: SantiagoPredialConsultas/Details/5
        public ActionResult Details(int? id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            santiagoPredialConsulta santiagoPredialConsulta = db.santiagoPredialConsultas.Find(id);
            if (santiagoPredialConsulta == null)
            {
                return HttpNotFound();
            }
            return View(santiagoPredialConsulta);
        }

        // GET: SantiagoPredialConsultas/Create
        public ActionResult Create(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        // POST: SantiagoPredialConsultas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,fecha_registro,no_predial,nombre,paterno,materno,telefono,cel,correo")] santiagoPredialConsulta santiagoPredialConsulta, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                db.santiagoPredialConsultas.Add(santiagoPredialConsulta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(santiagoPredialConsulta);
        }

        // GET: SantiagoPredialConsultas/Edit/5
        public ActionResult Edit(int? id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            santiagoPredialConsulta santiagoPredialConsulta = db.santiagoPredialConsultas.Find(id);
            if (santiagoPredialConsulta == null)
            {
                return HttpNotFound();
            }
            return View(santiagoPredialConsulta);
        }

        // POST: SantiagoPredialConsultas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,fecha_registro,no_predial,nombre,paterno,materno,telefono,cel,correo")] santiagoPredialConsulta santiagoPredialConsulta, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                db.Entry(santiagoPredialConsulta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(santiagoPredialConsulta);
        }

        // GET: SantiagoPredialConsultas/Delete/5
        public ActionResult Delete(int? id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            santiagoPredialConsulta santiagoPredialConsulta = db.santiagoPredialConsultas.Find(id);
            if (santiagoPredialConsulta == null)
            {
                return HttpNotFound();
            }
            return View(santiagoPredialConsulta);
        }

        // POST: SantiagoPredialConsultas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            santiagoPredialConsulta santiagoPredialConsulta = db.santiagoPredialConsultas.Find(id);
            db.santiagoPredialConsultas.Remove(santiagoPredialConsulta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
