﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Enpadi_WebUI.Models;

namespace Enpadi_WebUI.Controllers
{
    public class predialMovimientosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: predialMovimientos
        public ActionResult Index(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            var predialMovimientoes = db.predialMovimientoes.Include(p => p.predialConcepto);
            return View(predialMovimientoes.ToList());
        }

        // GET: predialMovimientos/Details/5
        public ActionResult Details(string skin, int? id)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            predialMovimiento predialMovimiento = db.predialMovimientoes.Find(id);
            if (predialMovimiento == null)
            {
                return HttpNotFound();
            }
            return View(predialMovimiento);
        }

        // GET: predialMovimientos/Create
        public ActionResult Create(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            ViewBag.conceptoID = new SelectList(db.predialConceptoes, "conceptoID", "descripcion");
            return View();
        }

        // POST: predialMovimientos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "movID,predialID,transaccion,conceptoID,descripcion,referencia,abono1,abono1iva,cargo1,cargo1iva,abono2,abono2iva,cargo2,cargo2iva,abono3,abono3iva,cargo3,cargo3iva,abonoTotal,cargoTotal,estatusID,aprobacion,userID,ip,fechaRegistro")] predialMovimiento predialMovimiento, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                db.predialMovimientoes.Add(predialMovimiento);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.conceptoID = new SelectList(db.predialConceptoes, "conceptoID", "descripcion", predialMovimiento.conceptoID);
            return View(predialMovimiento);
        }

        // GET: predialMovimientos/Edit/5
        public ActionResult Edit(string skin, int? id)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            predialMovimiento predialMovimiento = db.predialMovimientoes.Find(id);
            if (predialMovimiento == null)
            {
                return HttpNotFound();
            }
            ViewBag.conceptoID = new SelectList(db.predialConceptoes, "conceptoID", "descripcion", predialMovimiento.conceptoID);
            return View(predialMovimiento);
        }

        // POST: predialMovimientos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "movID,predialID,transaccion,conceptoID,descripcion,referencia,abono1,abono1iva,cargo1,cargo1iva,abono2,abono2iva,cargo2,cargo2iva,abono3,abono3iva,cargo3,cargo3iva,abonoTotal,cargoTotal,estatusID,aprobacion,userID,ip,fechaRegistro")] predialMovimiento predialMovimiento, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                db.Entry(predialMovimiento).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.conceptoID = new SelectList(db.predialConceptoes, "conceptoID", "descripcion", predialMovimiento.conceptoID);
            return View(predialMovimiento);
        }

        // GET: predialMovimientos/Delete/5
        public ActionResult Delete(string skin, int? id)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            predialMovimiento predialMovimiento = db.predialMovimientoes.Find(id);
            if (predialMovimiento == null)
            {
                return HttpNotFound();
            }
            return View(predialMovimiento);
        }

        // POST: predialMovimientos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string skin, int id)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            predialMovimiento predialMovimiento = db.predialMovimientoes.Find(id);
            db.predialMovimientoes.Remove(predialMovimiento);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
