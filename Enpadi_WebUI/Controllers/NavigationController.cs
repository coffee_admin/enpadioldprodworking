﻿using Enpadi_WebUI.Models;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using System.Web.Mvc;
using System.Linq;
using System;
using System.Configuration;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Security.Claims;

namespace Enpadi_WebUI.Controllers
{
    public class NavigationController : Controller
    {
        //private ApplicationDbContext db = new ApplicationDbContext();
        private static Boolean Confirm_email = Boolean.Parse(ConfigurationManager.AppSettings[UserValidation.Confirm_email]);

        // GET: Header
        [ChildActionOnly]
        public ActionResult EnpadiHeader()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            ViewModelNavigation perfil = new ViewModelNavigation();

            //Entradas
            string strSQL = "SELECT COALESCE(SUM(abono1),0) As Total FROM Transacciones WHERE socioID > 0 AND TarjetaID > 0 AND tipoID IN (1,2) AND fechaBaja IS NULL";
            IEnumerable<total> Entradas = db.Database.SqlQuery<total>(strSQL);
            perfil.entradas = Entradas.ToList().FirstOrDefault().Total;

            //Salidas
            strSQL = "SELECT COALESCE(SUM(cargo1 * -1),0) As Total FROM Transacciones WHERE socioID > 0 AND TarjetaID > 0 AND tipoID IN (1,2) AND fechaBaja IS NULL";
            IEnumerable<total> Salidas = db.Database.SqlQuery<total>(strSQL);
            perfil.salidas = Salidas.ToList().FirstOrDefault().Total;

            //Comisiones Cobradas
            strSQL = "SELECT COALESCE(SUM(abono2 + abono2iva + cargo3 + cargo3iva + cargo4 + cargo4iva + cargo6 + cargo6iva),0) As Total FROM Transacciones WHERE  TarjetaID > 0 AND tipoID IN (1,2) AND fechaBaja IS NULL";
            IEnumerable<total> Cobradas = db.Database.SqlQuery<total>(strSQL);
            perfil.comisionesCobradas = Cobradas.ToList().FirstOrDefault().Total;

            List<TransactionFee> trasnfee = db.TransactionFee.ToList();
            perfil.comisionesCobradas += trasnfee.Sum(x => x.FixAmount);
            perfil.comisionesCobradas += trasnfee.Sum(x => x.VariableAmount);
            perfil.comisionesCobradas += trasnfee.Sum(x => x.TaxFeeAmount);
            perfil.comisionesCobradas += trasnfee.Sum(x => x.ChargedByPartnerAmount);


            //Comisiones Pagadas
            //strSQL = "SELECT COALESCE(SUM((cargo2 + cargo2iva + cargo5 + cargo5iva) * -1),0) As Total FROM Transacciones WHERE socioID > 0 AND TarjetaID > 0 AND tipoID IN (1,2) AND fechaBaja IS NULL";
            //IEnumerable<total> Pagadas = db.Database.SqlQuery<total>(strSQL);
            //perfil.comisionesPagadas = Pagadas.ToList().FirstOrDefault().Total;

            //Comisiones Facturables
            //strSQL = "SELECT COALESCE(SUM(facturable1 + facturable1iva),0) As Total FROM Transacciones WHERE fechaBaja IS NULL AND tipoID IN (1,2) AND pagado1 = 0";
            //IEnumerable<total> Facturables = db.Database.SqlQuery<total>(strSQL);
            //perfil.comisionesFacturables = Facturables.ToList().FirstOrDefault().Total;

            // Saldo Disponible
            strSQL = "SELECT COALESCE(SUM(abono2 + abono2iva - cargo2 - cargo2iva + cargo3 + cargo3iva + cargo4 + cargo4iva - cargo5 - cargo5iva + cargo6 + cargo6iva + facturable1 + facturable1iva),0) As Total FROM Transacciones WHERE TarjetaID > 0 AND tipoID IN (1,2) AND fechaBaja IS NULL";
            IEnumerable<total> saldoDisponible = db.Database.SqlQuery<total>(strSQL);
            perfil.saldoDisponible = saldoDisponible.ToList().FirstOrDefault().Total;

            perfil.saldoDisponible -= trasnfee.Sum(x => x.ChargedByPartnerAmount);

            foreach (TransactionFee item in trasnfee) {
                if(item.Fee.UserToPayId != null){
                    perfil.saldoDisponible -= trasnfee.Sum(x => x.FixAmount);
                    perfil.saldoDisponible -= trasnfee.Sum(x => x.VariableAmount);
                    perfil.saldoDisponible -= trasnfee.Sum(x => x.TaxFeeAmount);
                }
            }


            if (User.IsInRole("Administrador"))
            {
                return PartialView("_HeaderAdministrador", perfil);
            }

            return new EmptyResult();
        }


        // GET: Navigation
        [ChildActionOnly]
        public ActionResult EnpadiNavigation(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            ViewModelNavigation perfil = new ViewModelNavigation();
            skin = "clean";
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (User.IsInRole("Administrador"))
            {

                // Saldo Tiempo Aire
                strSQL = "SELECT SUM(abono1 + abono1iva + abono2 + abono2iva - cargo1 - cargo1iva - cargo2 - cargo2iva) As Total FROM Transacciones WHERE FechaBaja IS NULL AND socioID = 5";
                IEnumerable<total> TiempoAire = db.Database.SqlQuery<total>(strSQL);
                perfil.saldoTiempoAire = TiempoAire.ToList().FirstOrDefault().Total;

                // Saldo Pago de Servicios
                strSQL = "SELECT SUM(abono1 + abono1iva + abono2 + abono2iva - cargo1 - cargo1iva - cargo2 - cargo2iva) As Total FROM Transacciones WHERE FechaBaja IS NULL AND socioID = 6";
                IEnumerable<total> PagoServicios = db.Database.SqlQuery<total>(strSQL);
                perfil.saldoPagoServicios = PagoServicios.ToList().FirstOrDefault().Total;


                //// Saldo Tiempo Aire
                //strSQL = "SELECT SUM(abono1 + abono1iva + abono2 + abono2iva - cargo1 - cargo1iva - cargo2 - cargo2iva) As Total FROM Transacciones WHERE FechaBaja IS NULL AND socioID = 5";
                //IEnumerable<total> TiempoAire = db.Database.SqlQuery<total>(strSQL);
                //perfil.saldoTiempoAire = TiempoAire.ToList().FirstOrDefault().Total;

                //// Saldo Pago de Servicios
                //strSQL = "SELECT SUM((abono1 + abono1iva + abono2 + abono2iva) - (cargo1 - cargo1iva - cargo2 - cargo2iva)) As Total FROM Transacciones WHERE FechaBaja IS NULL AND socioID = 6";
                //IEnumerable<total> PagoServicios = db.Database.SqlQuery<total>(strSQL);
                //perfil.saldoPagoServicios = PagoServicios.ToList().FirstOrDefault().Total;

                return PartialView("_MenuAdministrador", perfil);
            }

            //TODO: if user is not on any role make a log of the error

            //get the user role

            //todo: check here

            Tools.GetUserRole(User);

            if (User.IsInRole("Socio"))
                return PartialView("_MenuSocio");

            if (User.IsInRole("Usuario"))
            {
                string currentUserId = User.Identity.GetUserId();
                tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
                perfil.usuario = usuario;

                strSQL = " SELECT ISNULL(SUM(abonoTotal-cargoTotal),0) As Total "
                       + " FROM Transacciones "
                       + " WHERE fechaBaja IS NULL AND tarjetaID = " + usuario.tarjetaID;
                IEnumerable<total> saldoActual = db.Database.SqlQuery<total>(strSQL);
                perfil.saldoActual = saldoActual.ToList().FirstOrDefault().Total;

                strSQL = " SELECT ISNULL(SUM(abono-cargo),0) As Total "
                       + " FROM Recompensas "
                       + "WHERE fechaBaja IS NULL AND tarjetaID = " + usuario.tarjetaID;
                IEnumerable<total> saldoRecompensas = db.Database.SqlQuery<total>(strSQL);
                perfil.saldoRecompensas = saldoRecompensas.ToList().FirstOrDefault().Total;

                return PartialView("_MenuUsuario", perfil);
            }

            if (User.IsInRole("Recompensas"))
                return PartialView("_MenuRecompensas");

            if (User.IsInRole("Remesas"))
                return PartialView("_MenuRemesas");

            if (User.IsInRole("Concentradora"))
            {
                string currentUserId = User.Identity.GetUserId();
                tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
                perfil.usuario = usuario;

                strSQL = " SELECT ISNULL(SUM(abonoTotal-cargoTotal),0) As Total "
                       + " FROM Transacciones "
                       + " WHERE fechaBaja IS NULL AND tarjetaID = " + usuario.tarjetaID;
                IEnumerable<total> saldoActual = db.Database.SqlQuery<total>(strSQL);
                perfil.saldoActual = saldoActual.ToList().FirstOrDefault().Total;

                strSQL = " SELECT ISNULL(SUM(abono-cargo),0) As Total "
                       + " FROM Recompensas "
                       + "WHERE fechaBaja IS NULL AND tarjetaID = " + usuario.tarjetaID;
                IEnumerable<total> saldoRecompensas = db.Database.SqlQuery<total>(strSQL);
                perfil.saldoRecompensas = saldoRecompensas.ToList().FirstOrDefault().Total;

                return PartialView("_MenuConcentradora", perfil);
            }

            return new EmptyResult();

        }
    }
}