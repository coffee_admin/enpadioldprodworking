﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Enpadi_WebUI.Models;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System.Net;
using Microsoft.Owin.Security;
using System.Threading.Tasks;

namespace Enpadi_WebUI.Controllers
{
    public class WebhookController : Controller
    {


        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;


        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "website");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //public ActionResult LogOff()
        //{
        //    AuthenticationManager.SignOut();
        //    return RedirectToAction("Index", "website");
        //}


        public WebhookController()
        {
        }

        public WebhookController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        [HttpPost]
        public ActionResult Index(whNotificacion data)
        {
            return View();
        }

        [HttpPost]
        public JsonResult PostUser(whNotificacion data)
        {
            Debug.Assert(data != null);
            return Json(data);
        }

        public ActionResult Services()
        {

            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<JsonResult> imsspaymenttoday(string InputEmail, string InputPassword)
        {

            var resultSignIn = await SignInManager.PasswordSignInAsync(InputEmail, InputPassword, false, shouldLockout: false);

            switch (resultSignIn)
            {
                case SignInStatus.Success:
                    List<ImssPaymentsModel> items = ImssPaymentsModel.GetAll().Where(x => x.payment_date_at >= DateTime.Today && x.beneficiary_imss_number == "").ToList();

                    List<object> result = new List<object>();

                    foreach (var item in items)
                    {
                        result.Add(new
                        {
                            data =
                            item.code_base +
                            item.code_number.ToString("D4") +
                            item.buyer_enpadi_card_ending +
                            item.buyer_name.PadRight(30) +
                            item.beneficiary_imss_number.PadRight(11) +
                            item.amout_pay.ToString("0000.00").Replace(".", "") +
                            (item.payment_date_at != null ? ((DateTime)item.payment_date_at).ToString("ddMMyyyyHHmm") : DateTime.MinValue.ToString("ddMMyyyyHHmm")) +
                            item.remitter_name
                        });
                    }

                    AuthenticationManager.SignOut();

                    return Json(result, JsonRequestBehavior.AllowGet);
                case SignInStatus.LockedOut:
                case SignInStatus.Failure:
                default:
                    return Json(new { data = "mal usuario o contraseña" }, JsonRequestBehavior.AllowGet);
            }


        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<Boolean> imsspaymentbyid(string InputEmail, string InputPassword, String saleId)
        {
            var resultSignIn = await SignInManager.PasswordSignInAsync(InputEmail, InputPassword, false, shouldLockout: false);

            switch (resultSignIn)
            {
                case SignInStatus.Success:
                    List<object> result = new List<object>();
                    if (saleId != string.Empty)
                    {
                        String code_base = saleId.Substring(0, 1);
                        Int32 code_number = Int32.Parse(saleId.Substring(1, 4));
                        String buyer_enpadi_card_ending = saleId.Substring(5, 4);

                        List<ImssPaymentsModel> items = ImssPaymentsModel.GetAll().Where(x => x.code_base == code_base && x.code_number == code_number && x.buyer_enpadi_card_ending == buyer_enpadi_card_ending).ToList();

                        //foreach (var item in items)
                        //{
                        //    result.Add(new
                        //    {
                        //        data =
                        //        item.code_base +
                        //        item.code_number.ToString("D4") +
                        //        item.buyer_enpadi_card_ending +
                        //        item.buyer_name.PadRight(30) +
                        //        item.beneficiary_imss_number.PadRight(11) +
                        //        item.amout_pay.ToString("0000.00").Replace(".", "") +
                        //        (item.payment_date_at != null ? ((DateTime)item.payment_date_at).ToString("ddMMyyyyHHmm") : DateTime.MinValue.ToString("ddMMyyyyHHmm")) +
                        //        item.remitter_name
                        //    });
                        //}
                        if (items.Count() > 0)
                        {
                            return true;
                        }
                        else
                        {

                            return false;
                        }

                    }
                    else
                    {
                        AuthenticationManager.SignOut();
                        return false;
                        //result.Add(new {
                        //    data = "Id invalido o no existente"
                        //});
                    }

                //return Json(result, JsonRequestBehavior.AllowGet);
                case SignInStatus.LockedOut:
                case SignInStatus.Failure:
                default:
                    //return Json(new { data = "mal usuario o contraseña" }, JsonRequestBehavior.AllowGet);
                    return false;
            }

        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<JsonResult> imsspaymentdates(string InputEmail, string InputPassword, String dateStart, String dateEnd)
        {
            var resultSignIn = await SignInManager.PasswordSignInAsync(InputEmail, InputPassword, false, shouldLockout: false);

            switch (resultSignIn)
            {
                case SignInStatus.Success:
                    try
                    {
                        DateTime _dateStart = DateTime.Parse(dateStart);
                        DateTime _dateEnd = DateTime.Parse(dateEnd).AddHours(23).AddMinutes(59);


                        List<ImssPaymentsModel> items = ImssPaymentsModel.GetAll().Where(x => x.payment_date_at >= _dateStart && x.payment_date_at <= _dateEnd).ToList();

                        List<object> result = new List<object>();

                        foreach (var item in items)
                        {
                            result.Add(new
                            {
                                data =
                                item.code_base +
                                item.code_number.ToString("D4") +
                                item.buyer_enpadi_card_ending +
                                item.buyer_name.PadRight(30) +
                                item.beneficiary_imss_number.PadRight(11) +
                                item.amout_pay.ToString("0000.00").Replace(".", "") +
                                (item.payment_date_at != null ? ((DateTime)item.payment_date_at).ToString("ddMMyyyyHHmm") : DateTime.MinValue.ToString("ddMMyyyyHHmm")) +
                                item.remitter_name
                            });
                        }

                        AuthenticationManager.SignOut();
                        return Json(result, JsonRequestBehavior.AllowGet);

                    }
                    catch (Exception e)
                    {
                        AuthenticationManager.SignOut();
                        return Json(new { }, JsonRequestBehavior.AllowGet);
                    }
                case SignInStatus.LockedOut:
                case SignInStatus.Failure:
                default:
                    return Json(new { data = "mal usuario o contraseña" }, JsonRequestBehavior.AllowGet);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<JsonResult> imsspayment(string InputEmail, string InputPassword)
        {
            var resultSignIn = await SignInManager.PasswordSignInAsync(InputEmail, InputPassword, false, shouldLockout: false);

            switch (resultSignIn)
            {
                case SignInStatus.Success:
                    List<ImssPaymentsModel> items = ImssPaymentsModel.GetAll();

                    List<object> result = new List<object>();

                    foreach (var item in items)
                    {
                        result.Add(new
                        {
                            data =
                            item.code_base +
                            item.code_number.ToString("D4") +
                            item.buyer_enpadi_card_ending +
                            item.buyer_name.PadRight(30) +
                            item.beneficiary_imss_number.PadRight(11) +
                            item.amout_pay.ToString("0000.00").Replace(".", "") +
                            (item.payment_date_at != null ? ((DateTime)item.payment_date_at).ToString("ddMMyyyyHHmm") : DateTime.MinValue.ToString("ddMMyyyyHHmm")) +
                            item.remitter_name
                        });
                    }

                    AuthenticationManager.SignOut();

                    return Json(result, JsonRequestBehavior.AllowGet);
                case SignInStatus.LockedOut:
                case SignInStatus.Failure:
                default:
                    return Json(new { data = "mal usuario o contraseña" }, JsonRequestBehavior.AllowGet);
            }


        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> imsspaymentstring(string InputEmail, string InputPassword)
        {

            var resultSignIn = await SignInManager.PasswordSignInAsync(InputEmail, InputPassword, false, shouldLockout: false);

            switch (resultSignIn)
            {
                case SignInStatus.Success:
                    List<ImssPaymentsModel> items = ImssPaymentsModel.GetAll().OrderByDescending(x => x.beneficiary_imss_number).ToList();

                    string result = string.Empty;

                    foreach (var item in items)
                    {
                        result +=
                            item.code_base +
                            item.code_number.ToString("D4") +
                            item.buyer_enpadi_card_ending +
                            item.buyer_name.PadRight(30) +
                            item.beneficiary_imss_number.PadRight(11) +
                            item.amout_pay.ToString("0000.00").Replace(".", "") +
                            (item.payment_date_at != null ? ((DateTime)item.payment_date_at).ToString("ddMMyyyyHHmm") : DateTime.MinValue.ToString("ddMMyyyyHHmm")) +
                            item.remitter_name + System.Environment.NewLine
                        ;
                    }

                    AuthenticationManager.SignOut();

                    return Content(result, "application/text");
                case SignInStatus.LockedOut:
                case SignInStatus.Failure:
                default:
                    return Json(new { data = "mal usuario o contraseña" }, JsonRequestBehavior.AllowGet);
            }


        }
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> imsspaymentstringnew(string InputEmail, string InputPassword)
        {

            var resultSignIn = await SignInManager.PasswordSignInAsync(InputEmail, InputPassword, false, shouldLockout: false);

            switch (resultSignIn)
            {
                case SignInStatus.Success:
                    List<ImssPaymentsModel> items = ImssPaymentsModel.GetAll().Where(x => x.beneficiary_imss_number == string.Empty).ToList();

                    string result = string.Empty;

                    foreach (var item in items)
                    {
                        result +=
                            item.code_base +
                            item.code_number.ToString("D4") +
                            item.buyer_enpadi_card_ending +
                            item.buyer_name.PadRight(30) +
                            item.beneficiary_imss_number.PadRight(11) +
                            item.amout_pay.ToString("0000.00").Replace(".", "") +
                            (item.payment_date_at != null ? ((DateTime)item.payment_date_at).ToString("ddMMyyyyHHmm") : DateTime.MinValue.ToString("ddMMyyyyHHmm")) +
                            item.remitter_name + System.Environment.NewLine
                        ;
                    }

                    AuthenticationManager.SignOut();

                    return Content(result, "application/text");
                case SignInStatus.LockedOut:
                case SignInStatus.Failure:
                default:
                    return Json(new { data = "mal usuario o contraseña" }, JsonRequestBehavior.AllowGet);
            }


        }
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> imsspaymentstringrenewal(string InputEmail, string InputPassword)
        {

            var resultSignIn = await SignInManager.PasswordSignInAsync(InputEmail, InputPassword, false, shouldLockout: false);

            switch (resultSignIn)
            {
                case SignInStatus.Success:
                    List<ImssPaymentsModel> items = ImssPaymentsModel.GetAll().Where(x => x.beneficiary_imss_number != String.Empty).ToList();

                    string result = string.Empty;

                    foreach (var item in items)
                    {
                        result +=
                            item.code_base +
                            item.code_number.ToString("D4") +
                            item.buyer_enpadi_card_ending +
                            item.buyer_name.PadRight(30) +
                            item.beneficiary_imss_number.PadRight(11) +
                            item.amout_pay.ToString("0000.00").Replace(".", "") +
                            (item.payment_date_at != null ? ((DateTime)item.payment_date_at).ToString("ddMMyyyyHHmm") : DateTime.MinValue.ToString("ddMMyyyyHHmm")) +
                            item.remitter_name + System.Environment.NewLine
                        ;
                    }

                    AuthenticationManager.SignOut();

                    return Content(result, "application/text");
                case SignInStatus.LockedOut:
                case SignInStatus.Failure:
                default:
                    return Json(new { data = "mal usuario o contraseña" }, JsonRequestBehavior.AllowGet);
            }


        }
    }



    public class whNotificacion
    {
        public string type { get; set; }
        public string event_date { get; set; }
        public string verification_code { get; set; }

    }

    public class whAddress
    {
        public string line1 { get; set; }
        public string line2 { get; set; }
        public string line3 { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string postal_code { get; set; }
        public string contry_code { get; set; }
    }
}