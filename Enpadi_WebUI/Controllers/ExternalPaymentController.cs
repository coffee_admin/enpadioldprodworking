﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Enpadi_WebUI.Models;
using System.Configuration;
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;
using System.Net;
using Newtonsoft.Json.Linq;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Web.Http.Cors;

namespace Enpadi_WebUI.Controllers
{
    public class ExternalPaymentController : Controller
    {

        private ApplicationDbContext db = new ApplicationDbContext();
        private static bool bModoProduccion = Convert.ToBoolean(ConfigurationManager.AppSettings["ModoProduccion"]);

        [HttpPost]
        public async Task<HttpResponseMessage> LogIn()
        {
            try
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        // GET: ExternalPayment
        public ActionResult Index()
        {
            #region skin
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", "clean");
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();
            #endregion skin

            #region read from url and get order and charge

            EnpadiPostOrder response = new EnpadiPostOrder();

            #endregion read from url and get order and charge

            #region load page to confirm payment or load payment paystub
            
            #endregion load page to confirm payment or load payment paystub
            
            return View(response);
        }

        public String ProcessEnpadiOrder()
        {
            try
            {
                String url = String.Empty;
                String parameters = "?";

                #region read jsonstring and create order

                Stream req = Request.InputStream;
                req.Seek(0, System.IO.SeekOrigin.Begin);
                string jsonString = new StreamReader(req).ReadToEnd();

                EnpadiPostOrder response = new EnpadiPostOrder();

                if (jsonString != String.Empty)
                {
                    //to read some propeties of the jsonstring
                    string type = ((JToken)JObject.Parse(jsonString))["type"].ToString();
                    if (type == EnpadiOrderModel.EnpadiOrderEstatus.Create)
                    {

                        response = (EnpadiPostOrder)EnpadiPostOrder.JsonStringToModel(jsonString);
                        //save empadi order
                        EnpadiPostOrder.SaveEnpadiOrderPost(response.order);

                        //save charge order
                        EnpadiPostOrder.SaveEnpadiChargePost(response.charge);

                        parameters += "ord=" + response.order.data_object.id.ToString() + "&cha=" + response.charge.data_object.id.ToString();

                        AspNetUsersModel user = AspNetUsersModel.Get(null, response.order.data_object.client_email.ToString());

                        if (user == null)
                        {
                            url = "https://www.enpadi.com/enpadiApp/Enpadi/Account/Login";//register form
                        }
                        else
                        {
                            url = "https://www.enpadi.com/enpadiApp/Account/Register";//login form
                        }
                    }
                }

                #endregion read jsonstring and create order 

                return url + parameters;
            }
            catch (Exception e)
            {
                return SystemErrorModel.ErrorList.GenericError + e.Message;/*throw new ArgumentException(SystemErrorModel.ErrorList.GenericError + e.Message);*/
            }
        }

        //public ActionResult ProcessEnpadiOrder()
        //{
        //    try
        //    {
        //        String url = String.Empty;
        //        String parameters = "?";

        //        #region read jsonstring and create order

        //        Stream req = Request.InputStream;
        //        req.Seek(0, System.IO.SeekOrigin.Begin);
        //        string jsonString = new StreamReader(req).ReadToEnd();

        //        EnpadiPostOrder response = new EnpadiPostOrder();

        //        if (jsonString != String.Empty)
        //        {
        //            //to read some propeties of the jsonstring
        //            string type = ((JToken)JObject.Parse(jsonString))["type"].ToString();
        //            if (type == EnpadiOrderModel.EnpadiOrderEstatus.Create)
        //            {

        //                response = (EnpadiPostOrder)EnpadiPostOrder.JsonStringToModel(jsonString);
        //                //save empadi order
        //                EnpadiPostOrder.SaveEnpadiOrderPost(response.order);

        //                //save charge order
        //                EnpadiPostOrder.SaveEnpadiChargePost(response.charge);


        //                parameters += "ord=" + response.order.data_object.id.ToString() + "&cha=" + response.charge.data_object.id.ToString();

        //                AspNetUsersModel user = AspNetUsersModel.Get(null, response.order.data_object.client_email.ToString());

        //                if (user == null)
        //                {
        //                    url = "https://www.enpadi.com/enpadiApp/Enpadi/Account/Login";//register form
        //                }
        //                else
        //                {
        //                    url = "https://www.enpadi.com/enpadiApp/Account/Register";//login form
        //                }
        //            }
        //        }

        //        #endregion read jsonstring and create order 

        //        return Content(url + parameters, "text / xml");
        //    }
        //    catch (Exception e)
        //    {
        //        throw new ArgumentException(SystemErrorModel.ErrorList.GenericError + e.Message);
        //    }
        //}
    }
}

