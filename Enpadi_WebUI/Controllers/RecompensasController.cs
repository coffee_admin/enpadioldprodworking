﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Enpadi_WebUI.Models;

namespace Enpadi_WebUI.Controllers
{
    public class RecompensasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Recompensas
        public ActionResult Index(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            var recompensas = db.recompensas.Include(r => r.origenes).Include(r => r.socios).Include(r => r.tarjetas);
            return View(recompensas.ToList());
        }

        // GET: Recompensas/Details/5
        public ActionResult Details(int? id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            recompensa recompensa = db.recompensas.Find(id);
            if (recompensa == null)
            {
                return HttpNotFound();
            }
            return View(recompensa);
        }

        // GET: Recompensas/Create
        public ActionResult Create(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            ViewBag.origenID = new SelectList(db.origenes, "origenID", "descripcion");
            ViewBag.socioID = new SelectList(db.socios, "socioID", "nombreComercial");
            ViewBag.tarjetaID = new SelectList(db.tarjetas, "tarjetaID", "tcNum");
            return View();
        }

        // POST: Recompensas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "recompensaID,tarjetaID,origenID,socioID,transaccion,concepto,abono,cargo")] recompensa recompensa, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                db.recompensas.Add(recompensa);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.origenID = new SelectList(db.origenes, "origenID", "descripcion", recompensa.origenID);
            ViewBag.socioID = new SelectList(db.socios, "socioID", "nombreComercial", recompensa.socioID);
            ViewBag.tarjetaID = new SelectList(db.tarjetas, "tarjetaID", "tcNum", recompensa.tarjetaID);
            return View(recompensa);
        }

        // GET: Recompensas/Edit/5
        public ActionResult Edit(int? id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            recompensa recompensa = db.recompensas.Find(id);
            if (recompensa == null)
            {
                return HttpNotFound();
            }
            ViewBag.origenID = new SelectList(db.origenes, "origenID", "descripcion", recompensa.origenID);
            ViewBag.socioID = new SelectList(db.socios, "socioID", "nombreComercial", recompensa.socioID);
            ViewBag.tarjetaID = new SelectList(db.tarjetas, "tarjetaID", "tcNum", recompensa.tarjetaID);
            return View(recompensa);
        }

        // POST: Recompensas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "recompensaID,tarjetaID,origenID,socioID,transaccion,concepto,abono,cargo")] recompensa recompensa, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                db.Entry(recompensa).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.origenID = new SelectList(db.origenes, "origenID", "descripcion", recompensa.origenID);
            ViewBag.socioID = new SelectList(db.socios, "socioID", "nombreComercial", recompensa.socioID);
            ViewBag.tarjetaID = new SelectList(db.tarjetas, "tarjetaID", "tcNum", recompensa.tarjetaID);
            return View(recompensa);
        }

        // GET: Recompensas/Delete/5
        public ActionResult Delete(int? id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            recompensa recompensa = db.recompensas.Find(id);
            if (recompensa == null)
            {
                return HttpNotFound();
            }
            return View(recompensa);
        }

        // POST: Recompensas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            recompensa recompensa = db.recompensas.Find(id);
            db.recompensas.Remove(recompensa);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
