﻿using System.Linq;
using System.Web.Mvc;
using Enpadi_WebUI.Models;
using System.Collections.Generic;

namespace Enpadi_WebUI.Controllers
{
    public class OperacionController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        // GET: Operacion
        public ActionResult TC(string skin)
        {
            // Skin Aplicacion
            string strSQL = string.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            ViewModelEstadoDeCuenta resumen = new ViewModelEstadoDeCuenta();

            //Listado de Movimientos
            strSQL = " SELECT t.transID, o.descripcion AS origen, i.descripcion AS tipo, s.nombreComercial As socio, terminalID, m.codigo AS moneda,  t.tarjetaID, "
                   + "t.tarjeta, t.monto, t.cargo2 as comision2, t.cargo2iva as iva2, t.cargo5 as comision5, t.cargo5iva as iva5, (t.abonoTotal - cargo2 - cargo2iva - cargo5 - cargo5iva) As abonoTotal, "
                   + " t.cargoTotal, t.transaccion, t.concepto, t.descripcion, t.referencia, e.descripcion As estatus, t.aprobacion, t.ip, t.fechaRegistro "
                   + " FROM Transacciones t "
                   + " LEFT JOIN Origen o ON t.origenID = o.origenID "
                   + " LEFT JOIN Tipo i ON t.tipoID = i.tipoID "
                   + " LEFT JOIN Socios s ON t.socioID = s.socioID "
                   + " LEFT JOIN Monedas m ON t.monedaID = m.monedaID "
                   + " LEFT JOIN Estatus e ON t.estatusID = e.estatusID "
                   + " WHERE t.fechaBaja IS NULL AND t.socioID IN (1,2) AND t.tarjetaID > 0 "
                   + " ORDER BY t.fechaRegistro ";
            IEnumerable<movimientosListado> movs = db.Database.SqlQuery<movimientosListado>(strSQL);
            resumen.movimientos = movs.ToList();

            strSQL = "SELECT SUM(abono1) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID IN (1,2)";
            IEnumerable<total> depositos = db.Database.SqlQuery<total>(strSQL);
            resumen.saldoInicial = depositos.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo2) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID IN (1,2)";
            IEnumerable<total> com1 = db.Database.SqlQuery<total>(strSQL);
            resumen.comision1 = com1.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo2iva) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID IN (1,2)";
            IEnumerable<total> iva1 = db.Database.SqlQuery<total>(strSQL);
            resumen.iva1 = iva1.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo5) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID IN (1,2)";
            IEnumerable<total> com5 = db.Database.SqlQuery<total>(strSQL);
            resumen.comision2 = com5.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo5iva) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID IN (1,2)";
            IEnumerable<total> iva5 = db.Database.SqlQuery<total>(strSQL);
            resumen.iva2 = iva5.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(abono1 - cargo2 - cargo2iva - cargo5 - cargo5iva) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID IN (1,2)";
            IEnumerable<total> balance = db.Database.SqlQuery<total>(strSQL);
            resumen.saldoFinal = balance.ToList().FirstOrDefault().Total;

            return View(resumen);
        }

        // GET: Operacion
        public ActionResult Bancos(string skin)
        {
            // Skin Aplicacion
            string strSQL = string.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            ViewModelEstadoDeCuenta resumen = new ViewModelEstadoDeCuenta();

            //Listado de Movimientos
            strSQL = " SELECT t.transID, o.descripcion AS origen, i.descripcion AS tipo, s.nombreComercial As socio, terminalID, m.codigo AS moneda,  t.tarjetaID, "
                   + "t.tarjeta, t.monto, t.cargo2 as comision2, t.cargo2iva as iva2, t.cargo5 as comision5, t.cargo5iva as iva5, (t.abonoTotal - cargo2 - cargo2iva - cargo5 - cargo5iva) As abonoTotal, "
                   + " t.cargoTotal, t.transaccion, t.concepto, t.descripcion, t.referencia, e.descripcion As estatus, t.aprobacion, t.ip, t.fechaRegistro "
                   + " FROM Transacciones t "
                   + " LEFT JOIN Origen o ON t.origenID = o.origenID "
                   + " LEFT JOIN Tipo i ON t.tipoID = i.tipoID "
                   + " LEFT JOIN Socios s ON t.socioID = s.socioID "
                   + " LEFT JOIN Monedas m ON t.monedaID = m.monedaID "
                   + " LEFT JOIN Estatus e ON t.estatusID = e.estatusID "
                   + " WHERE t.fechaBaja IS NULL AND t.socioID = 3 AND t.tarjetaID > 0 "
                   + " ORDER BY t.fechaRegistro ";
            IEnumerable<movimientosListado> movs = db.Database.SqlQuery<movimientosListado>(strSQL);
            resumen.movimientos = movs.ToList();

            strSQL = "SELECT SUM(abono1) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 3 ";
            IEnumerable<total> depositos = db.Database.SqlQuery<total>(strSQL);
            resumen.saldoInicial = depositos.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo2) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 3 ";
            IEnumerable<total> com1 = db.Database.SqlQuery<total>(strSQL);
            resumen.comision1 = com1.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo2iva) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 3 ";
            IEnumerable<total> iva1 = db.Database.SqlQuery<total>(strSQL);
            resumen.iva1 = iva1.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo5) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 3";
            IEnumerable<total> com5 = db.Database.SqlQuery<total>(strSQL);
            resumen.comision2 = com5.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo5iva) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 3";
            IEnumerable<total> iva5 = db.Database.SqlQuery<total>(strSQL);
            resumen.iva2 = iva5.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(abono1 - cargo2 - cargo2iva - cargo5 - cargo5iva) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 3";
            IEnumerable<total> balance = db.Database.SqlQuery<total>(strSQL);
            resumen.saldoFinal = balance.ToList().FirstOrDefault().Total;

            return View(resumen);
        }

        // GET: Operacion
        public ActionResult Tiendas(string skin)
        {
            // Skin Aplicacion
            string strSQL = string.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            ViewModelEstadoDeCuenta resumen = new ViewModelEstadoDeCuenta();

            //Listado de Movimientos
            strSQL = " SELECT t.transID, o.descripcion AS origen, i.descripcion AS tipo, s.nombreComercial As socio, terminalID, m.codigo AS moneda,  t.tarjetaID, "
                   + "t.tarjeta, t.monto, t.cargo2 as comision2, t.cargo2iva as iva2, t.cargo5 as comision5, t.cargo5iva as iva5, (t.abonoTotal - cargo2 - cargo2iva - cargo5 - cargo5iva) As abonoTotal, "
                   + " t.cargoTotal, t.transaccion, t.concepto, t.descripcion, t.referencia, e.descripcion As estatus, t.aprobacion, t.ip, t.fechaRegistro "
                   + " FROM Transacciones t "
                   + " LEFT JOIN Origen o ON t.origenID = o.origenID "
                   + " LEFT JOIN Tipo i ON t.tipoID = i.tipoID "
                   + " LEFT JOIN Socios s ON t.socioID = s.socioID "
                   + " LEFT JOIN Monedas m ON t.monedaID = m.monedaID "
                   + " LEFT JOIN Estatus e ON t.estatusID = e.estatusID "
                   + " WHERE t.fechaBaja IS NULL AND t.socioID = 4 AND t.tarjetaID > 0 "
                   + " ORDER BY t.fechaRegistro ";
            IEnumerable<movimientosListado> movs = db.Database.SqlQuery<movimientosListado>(strSQL);
            resumen.movimientos = movs.ToList();

            strSQL = "SELECT SUM(abono1) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 4";
            IEnumerable<total> depositos = db.Database.SqlQuery<total>(strSQL);
            resumen.saldoInicial = depositos.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo2) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 4";
            IEnumerable<total> com1 = db.Database.SqlQuery<total>(strSQL);
            resumen.comision1 = com1.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo2iva) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 4";
            IEnumerable<total> iva1 = db.Database.SqlQuery<total>(strSQL);
            resumen.iva1 = iva1.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo5) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 4";
            IEnumerable<total> com5 = db.Database.SqlQuery<total>(strSQL);
            resumen.comision2 = com5.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo5iva) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 4";
            IEnumerable<total> iva5 = db.Database.SqlQuery<total>(strSQL);
            resumen.iva2 = iva5.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(abono1 - cargo2 - cargo2iva - cargo5 - cargo5iva) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 4";
            IEnumerable<total> balance = db.Database.SqlQuery<total>(strSQL);
            resumen.saldoFinal = balance.ToList().FirstOrDefault().Total;

            return View(resumen);
        }

        public ActionResult Transferencias(string skin)
        {
            // Skin Aplicacion
            string strSQL = string.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            ViewModelEstadoDeCuenta resumen = new ViewModelEstadoDeCuenta();

            //Listado de Comisiones
            strSQL = " SELECT t.transID, o.descripcion AS origen, i.descripcion AS tipo, s.nombreComercial As socio, terminalID, m.codigo AS moneda, t.tarjetaID, t.tarjeta, "
                   + " t.monto, t.cargo3 as comision3, t.cargo3iva as iva3, t.cargo4 as comision4, t.cargo4iva as iva4, t.cargo6 as comision6, t.cargo6iva as iva6, "
                   + " t.abonoTotal, t.cargoTotal, t.transaccion, t.concepto, t.descripcion, t.referencia, e.descripcion As estatus, t.aprobacion, t.ip, t.fechaRegistro "
                   + " FROM Transacciones t "
                   + " LEFT JOIN Origen o ON t.origenID = o.origenID "
                   + " LEFT JOIN Tipo i ON t.tipoID = i.tipoID "
                   + " LEFT JOIN Socios s ON t.socioID = s.socioID "
                   + " LEFT JOIN Monedas m ON t.monedaID = m.monedaID "
                   + " LEFT JOIN Estatus e ON t.estatusID = e.estatusID "
                   + " WHERE t.fechaBaja IS NULL AND t.tipoID IN (1,2) AND t.socioID = 0 AND t.tarjetaID > 0 "
                   + " ORDER BY t.fechaRegistro ";

            IEnumerable<movimientosListado> movs = db.Database.SqlQuery<movimientosListado>(strSQL);
            resumen.movimientos = movs.ToList();

            strSQL = "SELECT SUM(cargo1) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 0 AND tarjetaID > 0";
            IEnumerable<total> depositos = db.Database.SqlQuery<total>(strSQL);
            resumen.saldoInicial = depositos.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo3) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 0 AND tarjetaID > 0";
            IEnumerable<total> com1 = db.Database.SqlQuery<total>(strSQL);
            resumen.comision1 = com1.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo3iva) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 0 AND tarjetaID > 0";
            IEnumerable<total> iva1 = db.Database.SqlQuery<total>(strSQL);
            resumen.iva1 = iva1.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo4) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 0 AND tarjetaID > 0";
            IEnumerable<total> com5 = db.Database.SqlQuery<total>(strSQL);
            resumen.comision2 = com5.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo4iva) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 0 AND tarjetaID > 0";
            IEnumerable<total> iva5 = db.Database.SqlQuery<total>(strSQL);
            resumen.iva2 = iva5.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo6) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 0 AND tarjetaID > 0";
            IEnumerable<total> com6 = db.Database.SqlQuery<total>(strSQL);
            resumen.comision3 = com6.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo6iva) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 0 AND tarjetaID > 0";
            IEnumerable<total> iva6 = db.Database.SqlQuery<total>(strSQL);
            resumen.iva3 = iva6.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo3 + cargo3iva + cargo4 + cargo4iva + cargo6 + cargo6iva) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 0 AND tarjetaID > 0";
            IEnumerable<total> balance = db.Database.SqlQuery<total>(strSQL);
            resumen.saldoFinal = balance.ToList().FirstOrDefault().Total;

            return View(resumen);

        }

        // GET: Operacion
        public ActionResult TAE(string skin)
        {
            // Skin Aplicacion
            string strSQL = string.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            ViewModelEstadoDeCuenta resumen = new ViewModelEstadoDeCuenta();

            //Listado de Movimientos
            strSQL = " SELECT t.transID, o.descripcion AS origen, i.descripcion AS tipo, s.nombreComercial As socio, terminalID, m.codigo AS moneda, "
                   + " t.tarjetaID, t.tarjeta, t.monto, t.abono2 as comision2, t.abono2iva as iva2, t.abono5 as comision5, t.abono5iva as iva5, "
                   + " t.abonoTotal, (t.cargo1 - t.abono2 - t.abono2iva - t.abono5 - t.abono5iva) AS cargoTotal, "
                   + " t.transaccion, t.concepto, t.descripcion, t.referencia, e.descripcion As estatus, t.aprobacion, t.ip, t.fechaRegistro "
                   + " FROM Transacciones t "
                   + " LEFT JOIN Origen o ON t.origenID = o.origenID "
                   + " LEFT JOIN Tipo i ON t.tipoID = i.tipoID "
                   + " LEFT JOIN Socios s ON t.socioID = s.socioID "
                   + " LEFT JOIN Monedas m ON t.monedaID = m.monedaID "
                   + " LEFT JOIN Estatus e ON t.estatusID = e.estatusID "
                   + " WHERE t.fechaBaja IS NULL AND t.socioID = 5 AND t.tarjetaID > 0 "
                   + " ORDER BY t.fechaRegistro ";
            IEnumerable<movimientosListado> movs = db.Database.SqlQuery<movimientosListado>(strSQL);
            resumen.movimientos = movs.ToList();

            strSQL = "SELECT SUM(cargo1) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 5";
            IEnumerable<total> depositos = db.Database.SqlQuery<total>(strSQL);
            resumen.saldoInicial = depositos.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(abono2) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 5";
            IEnumerable<total> com1 = db.Database.SqlQuery<total>(strSQL);
            resumen.comision1 = com1.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(abono2iva) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 5";
            IEnumerable<total> iva1 = db.Database.SqlQuery<total>(strSQL);
            resumen.iva1 = iva1.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(abono5) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 5";
            IEnumerable<total> com5 = db.Database.SqlQuery<total>(strSQL);
            resumen.comision2 = com5.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(abono5iva) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 5";
            IEnumerable<total> iva5 = db.Database.SqlQuery<total>(strSQL);
            resumen.iva2 = iva5.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo1 - abono2 - abono2iva - abono5 - abono5iva) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 5";
            IEnumerable<total> balance = db.Database.SqlQuery<total>(strSQL);
            resumen.saldoFinal = balance.ToList().FirstOrDefault().Total;

            return View(resumen);
        }

        // GET: Operacion
        public ActionResult Servicios(string skin)
        {
            // Skin Aplicacion
            string strSQL = string.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            ViewModelEstadoDeCuenta resumen = new ViewModelEstadoDeCuenta();

            //Listado de Movimientos
            strSQL = " SELECT t.transID, o.descripcion AS origen, i.descripcion AS tipo, s.nombreComercial As socio, terminalID, m.codigo AS moneda, "
                   + " t.tarjetaID, t.tarjeta, t.monto, t.cargo2 as comision2, t.cargo2iva as iva2, t.facturable1, t.facturable1iva, "
                   + " t.abonoTotal, (t.cargo1 + t.cargo2 + t.cargo2iva - t.facturable1 - t.facturable1iva) As cargoTotal, "
                   + " t.transaccion, t.concepto, t.descripcion, t.referencia, e.descripcion As estatus, t.aprobacion, t.ip, t.fechaRegistro "
                   + " FROM Transacciones t "
                   + " LEFT JOIN Origen o ON t.origenID = o.origenID "
                   + " LEFT JOIN Tipo i ON t.tipoID = i.tipoID "
                   + " LEFT JOIN Socios s ON t.socioID = s.socioID "
                   + " LEFT JOIN Monedas m ON t.monedaID = m.monedaID "
                   + " LEFT JOIN Estatus e ON t.estatusID = e.estatusID "
                   + " WHERE t.fechaBaja IS NULL AND t.socioID = 6 AND t.tarjetaID > 0 "
                   + " ORDER BY t.fechaRegistro ";
            IEnumerable<movimientosListado> movs = db.Database.SqlQuery<movimientosListado>(strSQL);
            resumen.movimientos = movs.ToList();

            strSQL = "SELECT SUM(cargo1) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 6";
            IEnumerable<total> depositos = db.Database.SqlQuery<total>(strSQL);
            resumen.saldoInicial = depositos.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo2) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 6";
            IEnumerable<total> com1 = db.Database.SqlQuery<total>(strSQL);
            resumen.comision1 = com1.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo2iva) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 6";
            IEnumerable<total> iva1 = db.Database.SqlQuery<total>(strSQL);
            resumen.iva1 = iva1.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(facturable1) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 6";
            IEnumerable<total> com5 = db.Database.SqlQuery<total>(strSQL);
            resumen.comision2 = com5.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(facturable1iva) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 6";
            IEnumerable<total> iva5 = db.Database.SqlQuery<total>(strSQL);
            resumen.iva2 = iva5.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo1 + cargo2 + cargo2iva - facturable1 - facturable1iva) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID = 6";
            IEnumerable<total> balance = db.Database.SqlQuery<total>(strSQL);
            resumen.saldoFinal = balance.ToList().FirstOrDefault().Total;

            return View(resumen);
        }

        public ActionResult Comisiones(string skin)
        {
            // Skin Aplicacion
            string strSQL = string.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            ViewModelEstadoDeCuenta resumen = new ViewModelEstadoDeCuenta();

            //Listado de Comisiones
            strSQL = " SELECT t.transID, o.descripcion AS origen, i.descripcion AS tipo, s.nombreComercial As socio, terminalID, m.codigo AS moneda, t.tarjetaID, t.tarjeta, "
                   + " t.monto, t.cargo3 as comision3, t.cargo3iva as iva3, t.cargo4 as comision4, t.cargo4iva as iva4, t.cargo6 as comision6, t.cargo6iva as iva6, "
                   + " t.abonoTotal, t.cargoTotal, t.transaccion, t.concepto, t.descripcion, t.referencia, e.descripcion As estatus, t.aprobacion, t.ip, t.fechaRegistro "
                   + " FROM Transacciones t "
                   + " LEFT JOIN Origen o ON t.origenID = o.origenID "
                   + " LEFT JOIN Tipo i ON t.tipoID = i.tipoID "
                   + " LEFT JOIN Socios s ON t.socioID = s.socioID "
                   + " LEFT JOIN Monedas m ON t.monedaID = m.monedaID "
                   + " LEFT JOIN Estatus e ON t.estatusID = e.estatusID "
                   + " WHERE t.fechaBaja IS NULL AND t.tipoID = 2 AND t.socioID > 0 AND t.tarjetaID > 0 "
                   + " ORDER BY t.fechaRegistro ";
            IEnumerable<movimientosListado> movs = db.Database.SqlQuery<movimientosListado>(strSQL);
            resumen.movimientos = movs.ToList();

            strSQL = "SELECT SUM(cargo1) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID > 0";
            IEnumerable<total> depositos = db.Database.SqlQuery<total>(strSQL);
            resumen.saldoInicial = depositos.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo3) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID > 0";
            IEnumerable<total> com1 = db.Database.SqlQuery<total>(strSQL);
            resumen.comision1 = com1.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo3iva) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID > 0";
            IEnumerable<total> iva1 = db.Database.SqlQuery<total>(strSQL);
            resumen.iva1 = iva1.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo4) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID > 0";
            IEnumerable<total> com5 = db.Database.SqlQuery<total>(strSQL);
            resumen.comision2 = com5.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo4iva) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID > 0";
            IEnumerable<total> iva5 = db.Database.SqlQuery<total>(strSQL);
            resumen.iva2 = iva5.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo6) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID > 0";
            IEnumerable<total> com6 = db.Database.SqlQuery<total>(strSQL);
            resumen.comision3 = com6.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo6iva) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID > 0";
            IEnumerable<total> iva6 = db.Database.SqlQuery<total>(strSQL);
            resumen.iva3 = iva6.ToList().FirstOrDefault().Total;

            strSQL = "SELECT SUM(cargo1 + cargo3 + cargo3iva + cargo4 + cargo4iva + cargo6 + cargo6iva) As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID > 0";
            IEnumerable<total> balance = db.Database.SqlQuery<total>(strSQL);
            resumen.saldoFinal = balance.ToList().FirstOrDefault().Total;

            return View(resumen);
        }

        //GET: Operacion
        public ActionResult Remesas(string skin)
        {
            // Skin Aplicacion
            string strSQL = string.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            //Listado de Remesas
            strSQL = "SELECT r.remesaID, s.nombreComercial As socio, r.concepto, r.transaccion,  r.referencia, " 
                   + " r.localAmount, r.foreignAmount, r.exchangeRate, r.fromFirstName + ' ' + r.fromLastName As fromName, "
                   + " r.fromPhone, r.fromEmail, r.toFirstName + ' ' + r.toLastName As toName, r.toPhone, r.toEmail, "
                   + " e.descripcion As estatus, r.fechaRegistro "
                   + " FROM Remesas r "
                   + " LEFT JOIN Origen o ON r.origenID = o.origenID "
                   + " LEFT JOIN Tipo t ON r.tipoID = t.tipoID "
                   + " LEFt JOIN Socios s ON r.socioID = s.socioID "
                   + " LEFT JOIN Estatus e ON r.estatus = e.estatusID "
                   + " ORDER BY fechaRegistro" ;
            IEnumerable<remesaListado> r = db.Database.SqlQuery<remesaListado>(strSQL);
            return View(r.ToList());
        }

        // GET: Operacion
        public ActionResult Recompensas(string skin)
        {
            // Skin Aplicacion
            string strSQL = string.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            //Listado de Recompensas
            strSQL = " SELECT r.recompensaID, r.tarjetaID, t.tcNum, o.descripcion As origen, s.nombreComercial As socio, "
                   + " r.transaccion, r.concepto, r.abono, r.cargo, r.fechaAlta "
                   + " FROM Recompensas r "
                   + " LEFT JOIN Tarjetas t ON r.tarjetaID = t.tarjetaID "
                   + " LEFT JOIN Origen o ON r.origenID = o.origenID "
                   + " LEFT JOIN Socios s ON r.socioID = s.socioID "
                   + " ORDER BY fechaAlta ";
            IEnumerable<recompensaListado> r = db.Database.SqlQuery<recompensaListado>(strSQL);
            return View(r.ToList());
        }
    }
}

// ------ CONSULTA REMESAS COMPLETA -----
//SELECT r.remesaID, o.descripcion As origen, t.descripcion As tipo, r.modoID, s.nombreComercial As socio, r.terminalID,
//    r.transaccion, r.concepto, r.descripcion, r.referencia, r.localAmount, r.foreignAmount, r.exchangeRate, r.fee1,
//    r.fee2, r.fee3, r.receiptCode, r.remitanceCode, r.confirmationCode, r.fromFirstName + ' ' + r.fromLastName As fromName, 
//    r.fromAddress1, r.fromAddress2, r.fromNeighborhood, r.fromCity, r.fromState, r.fromCountry, r.fromPhone, r.fromEmail, 
//    r.toFirstName, r.toLastName, r.toAddress1, r.toAddress2, r.toNeighborhood, r.toCity, r.toState, r.toCountry, r.toPhone, r.toEmail,
//    e.descripcion, r.fechaRegistro
//FROM Remesas r
//LEFT JOIN Origen o ON r.origenID = o.origenID
//LEFT JOIN Tipo t ON r.tipoID = t.tipoID
//LEFt JOIN Socios s ON r.socioID = s.socioID
//LEFT JOIN Estatus e ON r.estatus = e.estatusID
//ORDER BY fechaRegistro
