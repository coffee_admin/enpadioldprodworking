﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace Enpadi_WebUI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ErrorController : Controller
    {

        public ViewResult Index(string error)
        {
            var response = Response;
            return View("Error");
        }
        public ViewResult NotFound()
        {
            Response.StatusCode = 404;  //you may want to set this to 200
            return View("NotFound");
        }

        // GET: Error
        public ActionResult GenericError()
        {
            return View();
        }
    }
}