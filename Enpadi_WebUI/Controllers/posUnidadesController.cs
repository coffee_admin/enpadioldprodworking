﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Enpadi_WebUI.Models;

namespace Enpadi_WebUI.Controllers
{
    public class posUnidadesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: posUnidades
        public ActionResult Index(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View(db.posUnidads.ToList());
        }

        // GET: posUnidades/Details/5
        public ActionResult Details(int? id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            posUnidad posUnidad = db.posUnidads.Find(id);
            if (posUnidad == null)
            {
                return HttpNotFound();
            }
            return View(posUnidad);
        }

        // GET: posUnidades/Create
        public ActionResult Create(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        // POST: posUnidades/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "unidadID,unidad,abreviacion")] posUnidad posUnidad, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                db.posUnidads.Add(posUnidad);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(posUnidad);
        }

        // GET: posUnidades/Edit/5
        public ActionResult Edit(int? id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            posUnidad posUnidad = db.posUnidads.Find(id);
            if (posUnidad == null)
            {
                return HttpNotFound();
            }
            return View(posUnidad);
        }

        // POST: posUnidades/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "unidadID,unidad,abreviacion")] posUnidad posUnidad, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                db.Entry(posUnidad).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(posUnidad);
        }

        // GET: posUnidades/Delete/5
        public ActionResult Delete(int? id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            posUnidad posUnidad = db.posUnidads.Find(id);
            if (posUnidad == null)
            {
                return HttpNotFound();
            }
            return View(posUnidad);
        }

        // POST: posUnidades/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            posUnidad posUnidad = db.posUnidads.Find(id);
            db.posUnidads.Remove(posUnidad);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
