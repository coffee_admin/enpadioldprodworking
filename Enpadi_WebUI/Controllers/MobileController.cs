﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Enpadi_WebUI.Models;
using ZXing;
using System.Net;
using System.Data.Entity;
using Openpay;
using Openpay.Entities;
using Openpay.Entities.Request;
using Enpadi_WebUI.WSDiestel;
using Microsoft.Owin.Security;
using System.Configuration;
using Enpadi_WebUI.Properties;


namespace Enpadi_WebUI.Controllers
{
    [Authorize]
    public class MobileController : Controller
    {
        //private ApplicationDbContext db = new ApplicationDbContext();

        private static bool bModoProduccion = Convert.ToBoolean(ConfigurationManager.AppSettings["ModoProduccion"]);

        private string OPENPAY_ID = (bModoProduccion) ? ConfigurationManager.AppSettings["Enpadi_OpenPay_Id"] : ConfigurationManager.AppSettings["Test_OpenPay_id"];
        private string OPENPAY_PUBLIC_KEY = (bModoProduccion) ? ConfigurationManager.AppSettings["Enpadi_OpenPay_PublicKey"] : ConfigurationManager.AppSettings["Test_OpenPay_PublicKey"];

        private string OPENPAY_PRIVATE_KEY = (bModoProduccion) ? Settings.Default.Enpadi_OpenPay_PrivateKey : Settings.Default.Test_OpenPay_PrivateKey;

        private string DIESTEL_URL = (bModoProduccion) ? Settings.Default.Enpadi_WebUI_WSDiestel_PxUniversal : Settings.Default.Test_WebUI_WSDiestel_PxUniversal;
        private int DIESTEL_GRUPO = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Grupo : Settings.Default.Test_WSDiestel_Grupo;
        private int DIESTEL_CADENA = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Cadena : Settings.Default.Test_WSDiestel_Cadena;
        private int DIESTEL_TIENDA = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Tienda : Settings.Default.Test_WSDiestel_Tienda;
        private string DIESTEL_USUARIO = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Usuario : Settings.Default.Test_WSDiestel_Usuario;
        private string DIESTEL_PASSWORD = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Password : Settings.Default.Test_WSDiestel_Password;

        // Special Function to End the actual User Session
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ActionResult Index(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if(User.IsInRole("Usuario"))
            {
                ViewModelNavigation perfil = new ViewModelNavigation();

                string currentUserId = User.Identity.GetUserId();
                tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
                perfil.usuario = usuario;

                strSQL = string.Empty;
                strSQL = " SELECT ISNULL(SUM(abonoTotal-cargoTotal),0) As Total "
                       + " FROM Transacciones "
                       + " WHERE tarjetaID = " + usuario.tarjetaID;
                IEnumerable<total> saldoActual = db.Database.SqlQuery<total>(strSQL);
                perfil.saldoActual = saldoActual.ToList().FirstOrDefault().Total;
                return View(perfil);
            }

            return new EmptyResult();
        }

        public ActionResult Manage()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Reload(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        public ActionResult ReloadTC(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            strSQL = string.Format("SELECT COUNT(*) AS total FROM opCustomers WHERE ExternalID = {0}", usuario.tarjetaID);
            IEnumerable<contador> custAddress = db.Database.SqlQuery<contador>(strSQL);
            int iCustAddress = custAddress.ToList().FirstOrDefault().total;

            strSQL = string.Format("SELECT COUNT(*) AS total FROM opCards WHERE ExternalID = {0}", usuario.tarjetaID);
            IEnumerable<contador> custCard = db.Database.SqlQuery<contador>(strSQL);
            int iCustCard = custCard.ToList().FirstOrDefault().total;

            if (iCustAddress <= 0)
                return RedirectToAction("AddCustomerMobile");
            if (iCustCard <= 0)
                return RedirectToAction("AddCardMobile");

            return RedirectToAction("AddChargeMobile");
        }


        public ActionResult AddCustomerMobile(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            openpayCustomer opCustomer = new openpayCustomer() { Name = usuario.datNombre, Email = usuario.email };

            return View(opCustomer);
        }

        // POST: openpayCustomers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult AddCustomerMobile([Bind(Include = "id,CustomerID,ExternalID,Name,LastName,Email,PhoneNumber,RequiresAccount,Address1,Address2,Address3,City,State,CountryCode,PostalCode")] openpayCustomer opCustomer, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                string currentUserId = User.Identity.GetUserId();
                tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
                opCustomer.ExternalID = usuario.tarjetaID;

                OpenpayAPI api = new OpenpayAPI(OPENPAY_PRIVATE_KEY, OPENPAY_ID, bModoProduccion);

                Customer cliente = new Customer() { ExternalId = opCustomer.ExternalID.ToString(), Name = opCustomer.Name, LastName = opCustomer.LastName, Email = opCustomer.Email, PhoneNumber = opCustomer.PhoneNumber };
                Address dir = new Address() { Line1 = opCustomer.Address1, Line2 = opCustomer.Address2, Line3 = opCustomer.Address3, City = opCustomer.City, State = opCustomer.State, CountryCode = opCustomer.CountryCode, PostalCode = opCustomer.PostalCode };
                cliente.Address = dir;

                cliente = api.CustomerService.Create(cliente);

                if (cliente.Id != null)
                {
                    opCustomer.CustomerID = cliente.Id;
                    opCustomer.CLABE = cliente.CLABE;

                    db.openpayCustomers.Add(opCustomer);
                    db.SaveChanges();

                    return RedirectToAction("ReloadTC");
                }
                else
                {
                    return View(opCustomer);
                }
            }

            return View(opCustomer);
        }
        

        public ActionResult AddCardMobile(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }


        [HttpPost]
        public ActionResult AddCardMobile(string holder_name, string card_number, string cvv2, string expiration_month, string expiration_year, string token_id, string deviceSessionId, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            strSQL = String.Format("SELECT COUNT(*) AS total FROM opCards WHERE ExternalID = {0}", usuario.tarjetaID);
            IEnumerable<contador> registradas = db.Database.SqlQuery<contador>(strSQL);
            int iRegistradas = registradas.ToList().FirstOrDefault().total;

            if (iRegistradas < 3) {
                openpayCard opCard = new openpayCard() { ExternalID = usuario.tarjetaID };
                openpayCustomer opCustomer = db.openpayCustomers.FirstOrDefault(x => x.ExternalID == usuario.tarjetaID);
                opCard.CustomerId = opCustomer.CustomerID;

                OpenpayAPI api = new OpenpayAPI(OPENPAY_PRIVATE_KEY, OPENPAY_ID, bModoProduccion);
                Card card = new Card();
                card.TokenId = token_id;
                card.DeviceSessionId = deviceSessionId;

                try
                {
                    card = api.CardService.Create(opCard.CustomerId.ToString(), card);

                    if (card.Id != null)
                    {
                        opCard.HolderName = holder_name;
                        opCard.CardNumber = card_number;
                        opCard.ExpirationMonth = expiration_month;
                        opCard.ExpirationYear = expiration_year;
                        opCard.CVV2 = cvv2;
                        opCard.CardID = card.Id;
                        opCard.Brand = card.Brand;
                        opCard.Type = card.Type;
                        opCard.BankName = card.BankName;
                        opCard.BankCode = card.BankCode;

                        db.openpayCards.Add(opCard);
                        db.SaveChanges();
                        return RedirectToAction("ReloadTC");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Ha ocurrido un Error. Intenta con otro Numero de Tarjeta");
                    }
                }
                catch (OpenpayException ex)
                {
                    Console.WriteLine(ex.Message);
                    return View(opCard);
                }
            }
            else
            {
                ModelState.AddModelError("", "No puedes tener Registradas mas de 3 Tarjetas en tu Cuenta");
            }

            return View();
        }


        public ActionResult AddChargeMobile(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            strSQL = string.Format("SELECT * FROM opCards WHERE DeleteDate IS NULL AND ExternalID = {0}", usuario.tarjetaID);
            IEnumerable<openpayCard> cards = db.Database.SqlQuery<openpayCard>(strSQL);

            return View(cards.ToList());
        }


        [HttpPost]
        public ActionResult AddChargeMobile(string token_id, string deviceSessionId, string source_id, string amount, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {

                string currentUserId = User.Identity.GetUserId();
                tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
                trans t = new trans { estatusID = 0, origenID = 1, tipoID = 1, modoID = 1, socioID = 2, terminalID = 1, monedaID = 484, Concepto = "Deposito", geoCountryCode = 0, geoCountryName = String.Empty, geoRegion = String.Empty, geoCity = String.Empty, geoPostalCode = String.Empty };

                strSQL = string.Format("SELECT COUNT(*) As total FROM logTC WHERE idTarjeta = {0} AND fechaAlta >= DATEADD(DAY, -7, getdate())", usuario.tarjetaID);
                IEnumerable<contador> intentos = db.Database.SqlQuery<contador>(strSQL);
                int iIntentos = intentos.ToList().FirstOrDefault().total;

                strSQL = string.Format("SELECT COUNT(*) As total FROM logTC WHERE idTarjeta = {0} AND result = 1 AND fechaAlta >= DATEADD(DAY, -7, getdate())", usuario.tarjetaID);
                IEnumerable<contador> exitosos = db.Database.SqlQuery<contador>(strSQL);
                int iExitosos = exitosos.ToList().FirstOrDefault().total;

                if (iIntentos < 5 && iExitosos < 2)
                {
                    strSQL = string.Format("SELECT * FROM opCards WHERE DeleteDate IS NULL AND ExternalID = {0} AND CardID = '{1}'", usuario.tarjetaID, source_id);
                    IEnumerable<openpayCard> opCard = db.Database.SqlQuery<openpayCard>(strSQL);

                    if(opCard.Count() > 0)
                    {
                        strSQL = string.Format("SELECT * FROM opCustomers WHERE DateDelete IS NULL AND ExternalID = {0}", usuario.tarjetaID);
                        IEnumerable<openpayCustomer> opCustomer = db.Database.SqlQuery<openpayCustomer>(strSQL);

                        if(opCustomer.Count() > 0) {
                            OpenpayAPI api = new OpenpayAPI(OPENPAY_PRIVATE_KEY, OPENPAY_ID, bModoProduccion);

                            ChargeRequest request = new ChargeRequest();
                            request.Method = "card";
                            request.SourceId = opCard.FirstOrDefault().CardID;
                            request.Amount = Convert.ToDecimal(amount);
                            request.Currency = "MXN";
                            request.Description = "Recarga de Monedero Enpadi";
                            request.DeviceSessionId = deviceSessionId;
                            request.OrderId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);

                            Charge charge = api.ChargeService.Create(opCustomer.FirstOrDefault().CustomerID, request);

                            if (charge.Status == "completed")
                            {
                                t.estatusID = 1;
                                t.tarjetaID = usuario.tarjetaID;
                                t.tarjeta = usuario.tcNum.Substring(usuario.tcNum.Length - 4);
                                t.transaccion = charge.OrderId;
                                t.descripcion = charge.Description;
                                t.referencia = charge.Id;
                                t.aprobacion = charge.Authorization;
                                t.monto = charge.Amount; t.abono1 = charge.Amount; t.cargo1 = 0; t.abono2 = 0; t.cargo2 = 0; t.abono3 = 0; t.cargo3 = 0; t.abonoTotal = charge.Amount; t.cargoTotal = 0;

                                decimal dIva = (decimal)0.16;
                                string strBrand = string.Empty;
                                if (!string.IsNullOrEmpty(opCard.FirstOrDefault().Brand))
                                    strBrand = opCard.FirstOrDefault().Brand.ToLower();

                                if (strBrand == "visa" || strBrand == "mastercard")
                                {
                                    t.cargo2 = t.monto * (decimal)(2.9 / 100);
                                    t.cargo2iva = t.cargo2 * dIva;
                                    t.cargo5 = (decimal)2.5;
                                    t.cargo5iva = t.cargo5 * dIva;
                                }
                                else
                                {
                                    t.cargo2 = t.monto * (decimal)(4.5 / 100);
                                    t.cargo2iva = t.cargo2 * dIva;
                                    t.cargo5 = (decimal)2.5;
                                    t.cargo5iva = t.cargo5 * dIva;
                                }

                                t.ip = "189.25.169.74";
                                db.transacciones.Add(t);

                                recompensa r = new recompensa() { tarjetaID = usuario.tarjetaID, origenID = 1, socioID = 2, transaccion = charge.OrderId, concepto = "Recompensa Recarga", abono = charge.Amount * (decimal)0.01, cargo = 0 };
                                db.recompensas.Add(r);

                                logTC envio = new logTC() { idTarjeta = usuario.tarjetaID, result = 1 };
                                db.logTCs.Add(envio);

                                db.SaveChanges();

                                return RedirectToAction("ResponseTC", "Mobile", new { estatus = "APROBADA", autorizacion = string.Format("AUTORIZACIÓN  {0}", t.aprobacion), total = string.Format("MONTO  ${0} MXN", t.monto) });
                            }
                            else
                            {
                                logTC envio = new logTC() { idTarjeta = usuario.tarjetaID, result = 0 };
                                db.logTCs.Add(envio);

                                db.SaveChanges();
                                return RedirectToAction("ResponseTC", "Mobile", new { estatus = "DECLINADA", autorizacion = charge.Description, total = "" });
                            }

                        }
                        else
                        {
                            return RedirectToAction("ResponseTC", "Mobile", new { estatus = "DECLINADA", autorizacion = "No se ha encontrado el Usuario Seleccionado"});
                        }
                    }
                    else
                    {
                        return RedirectToAction("ResponseTC", "Mobile", new { estatus = "DECLINADA", autorizacion = "No se ha encontrado la Tarjeta Seleccionada", total = "" });
                    }
                }
                else
                {
                    return RedirectToAction("ResponseTC", "Mobile", new { estatus = "DECLINADA", autorizacion = "Has excedido el Limite de Recargas Autorizado por Semana", total = "" });
                }


            }
            else
            {
                return View();
            }
        }


        public ActionResult AdminCardMobile(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            return View(db.openpayCards.Where(x => x.ExternalID == usuario.tarjetaID).ToList());
        }

        // GET: Mobile
        public ActionResult ResponseTC(string estatus, string autorizacion, string total, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            respuestaTC r = new respuestaTC() { Estatus = estatus, Autorizacion = autorizacion, Total = total };
            return View(r);
        }

        public ActionResult ReloadBank(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        public ActionResult ReloadBankRef(int? monto, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            OpenpayAPI api = new OpenpayAPI(OPENPAY_PRIVATE_KEY, OPENPAY_ID, bModoProduccion);

            Customer cliente = new Customer();
            cliente.ExternalId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);
            cliente.Name = String.Format("{0} {1} {2}", usuario.datNombre, usuario.datPaterno, usuario.datMaterno);
            cliente.Email = String.Format("{0}", usuario.email);
            cliente.RequiresAccount = false;

            ChargeRequest request = new ChargeRequest();
            request.Method = "bank_account";
            request.Amount = (decimal)monto;
            request.Description = "Pago en Banco";
            request.OrderId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);
            request.Customer = cliente;

            Charge charge = api.ChargeService.Create(request);

            pagoBanco ordenBanco = new pagoBanco() { id = charge.Id, description = charge.Description, error_message = charge.ErrorMessage, authorization = charge.Authorization, amount = charge.Amount, operation_type = charge.OperationType, payment_method = request.Method, clabe = charge.PaymentMethod.CLABE, bank = charge.PaymentMethod.BankName, name = charge.PaymentMethod.Name, order_id = charge.OrderId, transaction_type = charge.TransactionType, creation_date = charge.CreationDate.ToString(), status = charge.Status, method = charge.Method };

            ViewBag.MontoRecarga = monto;
            return View(ordenBanco);
        }

        public ActionResult ReloadStore(string imagen, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            TempData["imagen"] = imagen;

            return View();
        }

        public ActionResult ReloadStoreRef(int? monto, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            OpenpayAPI api = new OpenpayAPI(OPENPAY_PRIVATE_KEY, OPENPAY_ID, bModoProduccion);

            Customer cliente = new Customer();
            cliente.ExternalId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);
            cliente.Name = String.Format("{0} {1} {2}", usuario.datNombre, usuario.datPaterno, usuario.datMaterno);
            cliente.Email = String.Format("{0}", usuario.email);
            cliente.RequiresAccount = false;

            ChargeRequest request = new ChargeRequest();
            request.Method = "store";
            request.Amount = (decimal)monto;
            request.Description = "Pago en Tienda";
            request.OrderId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);
            request.Customer = cliente;

            Charge charge = api.ChargeService.Create(request);

            pagoTienda ordenTienda = new pagoTienda() { id = charge.Id, description = charge.Description, error_message = charge.ErrorMessage, authorization = charge.Authorization, amount = charge.Amount, operation_type = charge.OperationType, type = charge.PaymentMethod.Type, reference = charge.PaymentMethod.Reference, barcode_url = charge.PaymentMethod.BarcodeURL, order_id = charge.OrderId, transaction_type = charge.TransactionType, creation_date = charge.CreationDate.ToString(), status = charge.Status, method = charge.Method };

            ViewBag.MontoRecarga = monto;
            return View(ordenTienda);
        }

        [AllowAnonymous]
        public ActionResult Payment(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        [AllowAnonymous]
        public ActionResult Services(string email, string tipo, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            //Force to SignOut any Session
            //AuthenticationManager.SignOut();

            if (tipo == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            //strSQL = String.Format("SELECT SKU, Nombre, Descripcion, ReferenciaTipo, ReferenciaLongitud, Instrucciones, Compañia, imagenCompañia, Tipo, imagenTipo, Activo FROM Servicios WHERE FechaBaja IS NULL AND Activo = 1 AND Tipo = '{0}' ORDER BY Nombre", tipo);
            //IEnumerable<servicios> s = db.Database.SqlQuery<servicios>(strSQL);

            IEnumerable<servicios> s = db.servicios.Where(x => x.Activo == true && x.Tipo == tipo);

            ViewBag.Email = email;

            return View(s);
        }


        [AllowAnonymous]
        public ActionResult ServicesReference(string sku, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            //strSQL = String.Format("SELECT SKU, Nombre, Descripcion, ReferenciaTipo, ReferenciaLongitud, Instrucciones, Compañia, imagenCompañia, Tipo, imagenTipo, Activo FROM Servicios WHERE SKU = '{0}'", sku);
            IEnumerable<servicios> s = db.servicios.Where(x => x.SKU == sku);

            return View(s);
        }


        public ActionResult ServicesCapture(string sku, string referencia, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            strSQL = String.Format("SELECT ISNULL(SUM(abonoTotal-cargoTotal),0) As Total FROM Transacciones WHERE tarjetaID = {0} AND FechaBaja IS NUll", usuario.tarjetaID);
            IEnumerable<total> saldoActual = db.Database.SqlQuery<total>(strSQL);


            //strSQL = String.Format("SELECT SKU, Nombre, Descripcion, ReferenciaTipo, ReferenciaLongitud, Instrucciones, Compañia, imagenCompañia, Tipo, imagenTipo, Activo FROM Servicios WHERE SKU = '{0}'", sku);
            IEnumerable<servicios> s = db.servicios.Where(x => x.SKU == sku);

            pagoServicio oPago = new pagoServicio() { tarjetaID = usuario.tarjetaID, cuenta = String.Format("{0}", usuario.tcNum), titular = String.Format("{0} {1} {2}", usuario.datNombre, usuario.datPaterno, usuario.datMaterno), saldo = saldoActual.FirstOrDefault().Total, sku = sku, referencia = referencia, descripcion = s.FirstOrDefault().Descripcion, tipoPago = "MELE", nombre = s.FirstOrDefault().Nombre, compañia = s.FirstOrDefault().Compañia, imagenCompañia = s.FirstOrDefault().imagenCompañia, tipo = s.FirstOrDefault().Tipo, imagenTipo = s.FirstOrDefault().imagenTipo, estatus = 2  };
            oPago.fechaLocal = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
            oPago.horaLocal = String.Format("{0:HH:mm:ss}", DateTime.Now);
            oPago.fechaContable = String.Format("{0:dd/MM/yyyy}", DateTime.Now);

            PxUniversal ws = new PxUniversal() { Url = DIESTEL_URL, Timeout = 45000 };
            NetworkCredential oCredenciales = new NetworkCredential() { UserName = DIESTEL_USUARIO, Password = DIESTEL_PASSWORD };

            cCampo[] req = new cCampo[11];
            req[0] = new cCampo() { sCampo = "IDGRUPO", sValor = DIESTEL_GRUPO };
            req[1] = new cCampo() { sCampo = "IDCADENA", sValor = DIESTEL_CADENA };
            req[2] = new cCampo() { sCampo = "IDTIENDA", sValor = DIESTEL_TIENDA };
            req[3] = new cCampo() { sCampo = "IDPOS", sValor = 1 };
            req[4] = new cCampo() { sCampo = "IDCAJERO", sValor = 1 };
            req[5] = new cCampo() { sCampo = "FECHALOCAL", sValor = oPago.fechaLocal };
            req[6] = new cCampo() { sCampo = "HORALOCAL", sValor = oPago.horaLocal };
            req[7] = new cCampo() { sCampo = "SKU", sValor = sku };
            req[8] = new cCampo() { sCampo = "FECHACONTABLE", sValor = oPago.fechaContable };
            req[9] = new cCampo() { sCampo = "REFERENCIA", sValor = referencia };

            cCampo[] resp;

            try
            {
                db.pagoServicios.Add(oPago);
                db.SaveChanges();
                req[10] = new cCampo() { sCampo = "TRANSACCION", sValor = oPago.pagoID };

                ws.Credentials = oCredenciales;
                resp = ws.Info(req);
                oPago.Campos = resp;
                return View(oPago);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return View(oPago);
            }
        }

        [HttpPost]
        public ActionResult ServicesProcess(pagoServicio oPago, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            decimal dMonto = 0;
            decimal dIva = (decimal)0.16;
            bool dConvert = false;
            //Valida Saldo del Usuario
            if (oPago != null)
            {
                dMonto = (Decimal)oPago.monto;
                dConvert = true;
            }
           
            //bool dConvert = decimal.TryParse(oPago.monto, out dMonto);

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
            strSQL = String.Format("SELECT ISNULL(SUM(abonoTotal-cargoTotal),0) As Total FROM Transacciones WHERE tarjetaID = {0} AND FechaBaja IS NUll", usuario.tarjetaID);
            IEnumerable<total> saldoActual = db.Database.SqlQuery<total>(strSQL);

            //strSQL = String.Format("SELECT SKU, Nombre, Descripcion, ReferenciaTipo, ReferenciaLongitud, Instrucciones, Compañia, imagenCompañia, Tipo, imagenTipo, Activo FROM Servicios WHERE SKU = '{0}'", oPago.sku);
            IEnumerable<servicios> s = db.servicios.Where(x => x.SKU == oPago.sku);

            if (saldoActual.ToList().FirstOrDefault().Total >= dMonto)
            {
                PxUniversal ws = new PxUniversal() { Url = DIESTEL_URL, Timeout = 45000 };
                NetworkCredential oCredenciales = new NetworkCredential() { UserName = DIESTEL_USUARIO, Password = DIESTEL_PASSWORD };

                cCampo[] req = new cCampo[17];
                req[0] = new cCampo() { sCampo = "IDGRUPO", sValor = DIESTEL_GRUPO };
                req[1] = new cCampo() { sCampo = "IDCADENA", sValor = DIESTEL_CADENA };
                req[2] = new cCampo() { sCampo = "IDTIENDA", sValor = DIESTEL_TIENDA };
                req[3] = new cCampo() { sCampo = "IDPOS", sValor = 1 };
                req[4] = new cCampo() { sCampo = "IDCAJERO", sValor = 1 };
                req[5] = new cCampo() { sCampo = "FECHALOCAL", sValor = oPago.fechaLocal };
                req[6] = new cCampo() { sCampo = "HORALOCAL", sValor = oPago.horaLocal };
                req[7] = new cCampo() { sCampo = "TRANSACCION", sValor = oPago.pagoID };
                req[8] = new cCampo() { sCampo = "SKU", sValor = oPago.sku };
                req[9] = new cCampo() { sCampo = "REFERENCIA", sValor = oPago.referencia };
                req[10] = new cCampo() { sCampo = "FECHACONTABLE", sValor = oPago.fechaContable };
                req[11] = new cCampo() { sCampo = "MONTO", sValor = oPago.monto };
                req[12] = new cCampo() { sCampo = "TIPOPAGO", sValor = oPago.tipoPago };

                if (oPago.dv != null)
                    req[13] = new cCampo() { sCampo = "DV", sValor = oPago.dv };
                if (oPago.comision != null)
                    req[14] = new cCampo() { sCampo = "COMISION", sValor = oPago.comision };
                if (oPago.token != null)
                    req[15] = new cCampo() { sCampo = "TOKEN", sValor = oPago.token };
                if (oPago.referencia2 != null)
                    req[16] = new cCampo() { sCampo = "REFERENCIA2", sValor = oPago.referencia2 };

                cCampo[] resp;

                try
                {
                    ws.Credentials = oCredenciales;
                    resp = ws.Ejecuta(req);
                    oPago.Campos = resp;

                    pagoServicio rowPago = db.pagoServicios.Find(oPago.pagoID);

                    foreach (var item in resp)
                    {
                        switch (item.sCampo)
                        {
                            case "MONTO":
                                oPago.monto = Decimal.Parse(item.sValor.ToString().Replace('$',' '));
                                rowPago.monto = Decimal.Parse(item.sValor.ToString().Replace('$', ' '));
                                db.Entry(rowPago).Property(x => x.monto).IsModified = true;
                                break;
                            case "COMISION":
                                oPago.comision = Decimal.Parse(item.sValor.ToString().Replace('$', ' '));
                                rowPago.comision = Decimal.Parse(item.sValor.ToString().Replace('$', ' '));
                                db.Entry(rowPago).Property(x => x.comision).IsModified = true;
                                break;

                            case "CODIGORESPUESTA":
                                oPago.codigoRespuesta = Convert.ToInt32(item.sValor.ToString());
                                rowPago.codigoRespuesta = Convert.ToInt32(item.sValor.ToString());
                                db.Entry(rowPago).Property(x => x.codigoRespuesta).IsModified = true;
                                //Si la respuesta incluye un CODIGO RESPUESTA mayor a 0 se da por hecho que el pago fue RECHAZADO
                                if (oPago.codigoRespuesta > 0)
                                {
                                    //Se actualiza la informacion adicional de la operacion en la DB
                                    rowPago.monto = oPago.monto;
                                    db.Entry(rowPago).Property(x => x.monto).IsModified = true;
                                    rowPago.comision = oPago.comision;
                                    db.Entry(rowPago).Property(x => x.comision).IsModified = true;
                                }
                                break;
                            case "CODIGORESPUESTADESCR":
                                oPago.codigoRespuestaDescr = item.sValor.ToString();
                                rowPago.codigoRespuestaDescr = item.sValor.ToString();
                                db.Entry(rowPago).Property(x => x.codigoRespuestaDescr).IsModified = true;
                                break;
                            case "AUTORIZACION":
                                oPago.autorizacion = item.sValor.ToString();
                                rowPago.autorizacion = item.sValor.ToString();
                                db.Entry(rowPago).Property(x => x.autorizacion).IsModified = true;
                                //Si la respuesta incluye el campo AUTORIZACION se da por hecho que el pago fue ACEPTADO
                                oPago.estatus = 1;
                                rowPago.estatus = 1;
                                db.Entry(rowPago).Property(x => x.estatus).IsModified = true;
                                //Se debita el Monto Pagado del Monedero
                                string sTransaccion = DateTime.Now.ToString("yyyyMMddHHmmss");
                                trans cargo = new trans() { origenID = 1, tipoID = 2, modoID = 1, socioID = 6, terminalID = 1, monedaID = 484, tarjetaID = usuario.tarjetaID, monto = dMonto, abono1 = 0, cargo1 = dMonto, abono2 = 0, cargo2 = 0, abono3 = 0, cargo3 = 0, abonoTotal = 0, cargoTotal = dMonto, tarjeta = usuario.tcNum.Substring(usuario.tcNum.Length - 4), transaccion = sTransaccion, Concepto = oPago.compañia, descripcion = String.Format("{0} - {1} ({2})", oPago.sku, oPago.compañia, oPago.referencia), referencia = oPago.pagoID.ToString(), estatusID = 1, aprobacion = oPago.autorizacion, ip = "189.25.169.74", geoCountryCode = 0, geoCountryName = String.Empty, geoRegion = String.Empty, geoCity = String.Empty, geoPostalCode = String.Empty };

                                // Realiza el calculo de Comision a Aplicar
                                strSQL = string.Format("SELECT TOP 1 socioID As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID > 0 AND tipoID = 1 AND tarjetaID = {0} ORDER BY fechaRegistro DESC", usuario.tarjetaID);
                                IEnumerable<contador> ultimoDeposito = db.Database.SqlQuery<contador>(strSQL);
                                int iSocio = ultimoDeposito.ToList().FirstOrDefault().total;

                                if (oPago.compañia == "IUSACELL" || oPago.compañia == "UNEFON" || oPago.compañia == "NEXTEL" || oPago.compañia == "MOVISTAR" || oPago.compañia == "VIRGIN" || oPago.compañia == "TELCEL")
                                {
                                    cargo.abono2 = dMonto * (decimal)0.06;
                                    cargo.abono2iva = cargo.abono2 * dIva;
                                    if ((dMonto == 50 || dMonto == 100) && iSocio != 1)
                                    {
                                        cargo.cargo4 = (decimal)2.00;
                                        cargo.cargo4iva = (decimal)0.32;
                                    }
                                }
                                else
                                {
                                    decimal dComision = System.Convert.ToDecimal(oPago.comision);
                                    cargo.abono2 = dComision / (decimal)1.16;
                                    cargo.abono2iva = dComision - cargo.abono2;
                                    switch (iSocio)
                                    {
                                        case 1: //Visa-MasterCard
                                            cargo.cargo3 = dMonto * (decimal)0.029;
                                            cargo.cargo3iva = cargo.cargo3 * dIva;
                                            cargo.cargo4 = (decimal)2.50;
                                            cargo.cargo4iva = cargo.cargo4 * dIva;
                                            break;
                                        case 2: //AmericanExpress
                                            cargo.cargo3 = dMonto * (decimal)0.049;
                                            cargo.cargo3iva = cargo.cargo3 * dIva;
                                            cargo.cargo4 = (decimal)2.50;
                                            cargo.cargo4iva = cargo.cargo4 * dIva;
                                            break;
                                        case 3: //Banco
                                            cargo.cargo4 = (decimal)8.00;
                                            cargo.cargo4iva = cargo.cargo4 * dIva;
                                            break;
                                        case 4: //Tienda
                                            cargo.cargo3 = dMonto * (decimal)0.029;
                                            cargo.cargo3iva = cargo.cargo3 * dIva;
                                            cargo.cargo4 = (decimal)2.50;
                                            cargo.cargo4iva = cargo.cargo4 * dIva;
                                            break;
                                    }
                                }

                                db.transacciones.Add(cargo);
                                recompensa r = new recompensa() { tarjetaID = usuario.tarjetaID, origenID = 1, socioID = 6, transaccion = sTransaccion, concepto = "Recompensa Pago de Servicios", abono = dMonto * (decimal)0.01, cargo = 0 };
                                db.recompensas.Add(r);
                                db.SaveChanges();
                                break;
                            case "REFERENCIA2":
                                oPago.referencia2 = item.sValor.ToString();
                                rowPago.referencia2 = item.sValor.ToString();
                                db.Entry(rowPago).Property(x => x.referencia2).IsModified = true;
                                break;
                            case "TOKEN":
                                oPago.token = item.sValor.ToString();
                                rowPago.token = item.sValor.ToString();
                                db.Entry(rowPago).Property(x => x.token).IsModified = true;
                                break;
                            case "BRAND":
                                oPago.brand = item.sValor.ToString();
                                rowPago.brand = item.sValor.ToString();
                                db.Entry(rowPago).Property(x => x.brand).IsModified = true;
                                break;
                            case "PROVEEDOR":
                                oPago.proveedor = item.sValor.ToString();
                                rowPago.proveedor = item.sValor.ToString();
                                db.Entry(rowPago).Property(x => x.proveedor).IsModified = true;
                                break;
                            case "LEYENDA":
                                oPago.leyenda = item.sValor.ToString();
                                rowPago.leyenda = item.sValor.ToString();
                                db.Entry(rowPago).Property(x => x.leyenda).IsModified = true;
                                break;
                            case "LEYENDA1":
                                oPago.leyenda1 = item.sValor.ToString();
                                rowPago.leyenda1 = item.sValor.ToString();
                                db.Entry(rowPago).Property(x => x.leyenda1).IsModified = true;
                                break;
                            case "LEYENDA2":
                                oPago.leyenda2 = item.sValor.ToString();
                                rowPago.leyenda2 = item.sValor.ToString();
                                db.Entry(rowPago).Property(x => x.leyenda2).IsModified = true;
                                break;
                            default:
                                break;
                        }
                    }

                    db.SaveChanges();

                    //Con este Codigo de Respuesta se Requiere el envio de Cancelación
                    if (oPago.codigoRespuesta == 8 || oPago.codigoRespuesta == 71 || oPago.codigoRespuesta == 72)
                    {
                        req[0] = new cCampo() { sCampo = "IDGRUPO", sValor = 7 };
                        req[1] = new cCampo() { sCampo = "IDCADENA", sValor = 1 };
                        req[2] = new cCampo() { sCampo = "IDTIENDA", sValor = 1 };
                        req[3] = new cCampo() { sCampo = "IDPOS", sValor = 1 };
                        req[4] = new cCampo() { sCampo = "IDCAJERO", sValor = 1 };
                        req[5] = new cCampo() { sCampo = "FECHALOCAL", sValor = oPago.fechaLocal };
                        req[6] = new cCampo() { sCampo = "HORALOCAL", sValor = oPago.horaLocal };
                        req[7] = new cCampo() { sCampo = "TRANSACCION", sValor = oPago.pagoID };
                        req[8] = new cCampo() { sCampo = "SKU", sValor = oPago.sku };
                        req[9] = new cCampo() { sCampo = "REFERENCIA", sValor = oPago.referencia };
                        req[10] = new cCampo() { sCampo = "FECHACONTABLE", sValor = oPago.fechaContable };
                        req[11] = new cCampo() { sCampo = "AUTORIZACION", sValor = oPago.autorizacion };
                        req[12] = null;
                        req[13] = null;
                        req[14] = null;
                        req[15] = null;
                        ws.Credentials = oCredenciales;
                        resp = ws.Reversa(req);

                        foreach (var item in resp)
                        {
                            switch (item.sCampo)
                            {
                                case "CODIGORESPUESTA":
                                    if (Convert.ToInt32(item.sValor.ToString()) > 0)
                                    {
                                        resp = ws.Reversa(req);
                                        foreach (var i in resp)
                                        {
                                            switch (i.sCampo)
                                            {
                                                case "CODIGORESPUESTA":
                                                    if (Convert.ToInt32(item.sValor.ToString()) > 0)
                                                    {
                                                        resp = ws.Reversa(req);
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                    break;
                            }
                        }
                    }

                    return View(oPago);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    if (ex.Message == "Se excedió el tiempo de espera de la operación")
                    {
                        try
                        {
                            resp = ws.Reversa(req);
                            foreach (var item in resp)
                            {
                                switch (item.sCampo)
                                {
                                    case "CODIGORESPUESTA":
                                        if (Convert.ToInt32(item.sValor.ToString()) > 0)
                                        {
                                            resp = ws.Reversa(req);
                                            foreach (var i in resp)
                                            {
                                                switch (i.sCampo)
                                                {
                                                    case "CODIGORESPUESTA":
                                                        if (Convert.ToInt32(item.sValor.ToString()) > 0)
                                                        {
                                                            resp = ws.Reversa(req);
                                                        }
                                                        break;
                                                }
                                            }
                                        }
                                        break;
                                }
                            }

                        }
                        catch (Exception ex1)
                        {
                            Console.WriteLine(ex1.Message);
                            if (ex.Message == "Se excedió el tiempo de espera de la operación")
                            {
                                try
                                {
                                    resp = ws.Reversa(req);
                                    foreach (var item in resp)
                                    {
                                        switch (item.sCampo)
                                        {
                                            case "CODIGORESPUESTA":
                                                if (Convert.ToInt32(item.sValor.ToString()) > 0)
                                                {
                                                    resp = ws.Reversa(req);
                                                    foreach (var i in resp)
                                                    {
                                                        switch (i.sCampo)
                                                        {
                                                            case "CODIGORESPUESTA":
                                                                if (Convert.ToInt32(item.sValor.ToString()) > 0)
                                                                {
                                                                    resp = ws.Reversa(req);
                                                                }
                                                                break;
                                                        }
                                                    }
                                                }
                                                break;
                                        }
                                    }

                                }
                                catch (Exception ex2)
                                {
                                    Console.WriteLine(ex2.Message);
                                    if (ex.Message == "Se excedió el tiempo de espera de la operación")
                                    {
                                        try
                                        {
                                            resp = ws.Reversa(req);
                                            foreach (var item in resp)
                                            {
                                                switch (item.sCampo)
                                                {
                                                    case "CODIGORESPUESTA":
                                                        if (Convert.ToInt32(item.sValor.ToString()) > 0)
                                                        {
                                                            resp = ws.Reversa(req);
                                                            foreach (var i in resp)
                                                            {
                                                                switch (i.sCampo)
                                                                {
                                                                    case "CODIGORESPUESTA":
                                                                        if (Convert.ToInt32(item.sValor.ToString()) > 0)
                                                                        {
                                                                            resp = ws.Reversa(req);
                                                                        }
                                                                        break;
                                                                }
                                                            }
                                                        }
                                                        break;
                                                }
                                            }

                                        }
                                        catch (Exception ex3)
                                        {
                                            Console.WriteLine(ex3.Message);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    return View(oPago);
                }
            }
            else
            {
                oPago.estatus = 2; oPago.codigoRespuesta = 908; oPago.codigoRespuestaDescr = "Saldo Insuficiente. Recargue su Cuenta";

                pagoServicio rowPago = db.pagoServicios.Find(oPago.pagoID);
                rowPago.estatus = 2; rowPago.codigoRespuesta = 908; rowPago.codigoRespuestaDescr = "Saldo Insuficiente. Recargue su Cuenta";
                db.Entry(rowPago).Property(x => x.estatus).IsModified = true;
                db.Entry(rowPago).Property(x => x.codigoRespuesta).IsModified = true;
                db.Entry(rowPago).Property(x => x.codigoRespuestaDescr).IsModified = true;
                db.SaveChanges();

                return View(oPago);
            }
        }

        [HttpPost]
        public ActionResult ServicesPrint(pagoServicio oPago, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View(oPago);
        }

        [AllowAnonymous]
        public ActionResult SantiagoNL(string email, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult SantiagoNL(santiagoPredialConsulta consulta, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                if (consulta.terminos == true || consulta.terminos == false)
                {
                    strSQL = String.Format("SELECT COUNT(*) AS Total FROM SantiagoPredial WHERE expediente = '{0}'", consulta.no_predial);
                    IEnumerable<contador> total = db.Database.SqlQuery<contador>(strSQL);
                    consulta.fecha_registro = DateTime.Now;
                    db.santiagoPredialConsultas.Add(consulta);
                    db.SaveChanges();

                    if (total.FirstOrDefault().total > 0)
                    {
                        return RedirectToAction("SantiagoNLPredial", "Mobile", consulta);
                    }
                    else
                    {
                        ModelState.AddModelError("terminos", "No hemos encontrado ningún Expediente con los datos proporcionados, por favor valide la información, de lo contrario favor de acudir a la Oficina de Tesorería para brindarle atención personalizada.");
                        return View(consulta);
                    }
                }
                else
                {
                    ModelState.AddModelError("terminos", "Debe ACEPTAR los Terminos del Aviso de Privacidad");
                    return View(consulta);
                }
            }
            else
            {
                return View(consulta);
            }

        }

        [AllowAnonymous]
        public ActionResult SantiagoNLPredial(santiagoPredialConsulta consulta, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            santiagoPredial predial = db.santiagoPredials.FirstOrDefault(x => x.expediente == consulta.no_predial);
            predial.email = consulta.correo;

            return View(predial);
        }

        [AllowAnonymous]
        public ActionResult SantiagoNLBancos(string expediente, string direccion, string saldoPredial)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            santiagoPredialBancos referencia = new santiagoPredialBancos() { expediente = expediente, ubicacionPredio = direccion, saldo = Convert.ToDecimal(saldoPredial), fechaAlta = DateTime.Now };
            db.santiagoPredialBancos.Add(referencia);
            db.SaveChanges();

            return View();
        }

        [AllowAnonymous]
        public ActionResult SantiagoNLTarjeta(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult SantiagoNLTarjeta(string token_id, string holder_name, string amount, string deviceSessionId, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (!ModelState.IsValid)
            {
                return View();
            }
            else
            {
                string currentUserId = User.Identity.GetUserId();
                tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
                trans t = new trans { estatusID = 0, origenID = 1, tipoID = 1, modoID = 1, socioID = 2, terminalID = 1, monedaID = 484, Concepto = "TC", geoCountryCode = 0, geoCountryName = String.Empty, geoRegion = String.Empty, geoCity = String.Empty, geoPostalCode = String.Empty };

                strSQL = string.Format("SELECT COUNT(*) As total FROM logTC WHERE idTarjeta = {0} AND fechaAlta >= DATEADD(DAY, -7, getdate())", usuario.tarjetaID);
                IEnumerable<contador> intentos = db.Database.SqlQuery<contador>(strSQL);
                int iIntentos = intentos.ToList().FirstOrDefault().total;

                strSQL = string.Format("SELECT COUNT(*) As total FROM logTC WHERE idTarjeta = {0} AND result = 1 AND fechaAlta >= DATEADD(DAY, -7, getdate())", usuario.tarjetaID);
                IEnumerable<contador> exitosos = db.Database.SqlQuery<contador>(strSQL);
                int iExitosos = exitosos.ToList().FirstOrDefault().total;

                if (iIntentos < 5 && iExitosos < 2)
                {
                    OpenpayAPI api = new OpenpayAPI(OPENPAY_PRIVATE_KEY, OPENPAY_ID);

                    Customer cliente = new Customer();
                    cliente.ExternalId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);
                    cliente.Name = holder_name;
                    cliente.Email = "mramireza@hotmail.com";
                    cliente.RequiresAccount = false;

                    ChargeRequest request = new ChargeRequest();
                    request.Method = "card";
                    request.SourceId = token_id;
                    request.Amount = Convert.ToDecimal(amount);
                    request.Currency = "MXN";
                    request.Description = "Recarga de Monedero Enpadi";
                    request.DeviceSessionId = deviceSessionId;
                    request.Customer = cliente;
                    request.OrderId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);

                    Charge charge = api.ChargeService.Create(request);

                    if (charge.Status == "completed")
                    {
                        t.estatusID = 1;
                        t.tarjetaID = usuario.tarjetaID;
                        t.tarjeta = usuario.tcNum.Substring(usuario.tcNum.Length - 4);
                        t.transaccion = charge.OrderId;
                        t.descripcion = charge.Description;
                        t.referencia = charge.Id;
                        t.aprobacion = charge.Authorization;
                        t.monto = charge.Amount; t.abono1 = charge.Amount; t.cargo1 = 0; t.abono2 = 0; t.cargo2 = 0; t.abono3 = 0; t.cargo3 = 0; t.abonoTotal = charge.Amount; t.cargoTotal = 0;
                        t.ip = "189.25.169.74";
                        db.transacciones.Add(t);

                        recompensa r = new recompensa() { tarjetaID = usuario.tarjetaID, origenID = 1, socioID = 2, transaccion = charge.OrderId, concepto = "Recompensa Recarga", abono = charge.Amount * (decimal)0.01, cargo = 0 };
                        db.recompensas.Add(r);

                        logTC envio = new logTC() { idTarjeta = usuario.tarjetaID, result = 1 };
                        db.logTCs.Add(envio);

                        db.SaveChanges();

                        return RedirectToAction("SantiagoNLResp", "Mobile", new { estatus = "APROBADA", autorizacion = string.Format("AUTORIZACIÓN  {0}", t.aprobacion), total = string.Format("MONTO  ${0} MXN", t.monto) });
                    }
                    else
                    {
                        logTC envio = new logTC() { idTarjeta = usuario.tarjetaID, result = 0 };
                        db.logTCs.Add(envio);

                        db.SaveChanges();
                        return RedirectToAction("SantiagoNLResp", "Mobile", new { estatus = "DECLINADA", autorizacion = charge.Description, total = "" });
                    }
                }
                else
                {
                    return RedirectToAction("SantiagoNLResp", "Mobile", new { estatus = "DECLINADA", autorizacion = "Has excedido el Limite de Recargas Autorizado por Semana", total = "" });
                }
            }
        }

        [AllowAnonymous]
        public ActionResult SantiagoNLResp(string estatus, string autorizacion, string total, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            respuestaTC r = new respuestaTC() { Estatus = estatus, Autorizacion = autorizacion, Total = total };
            return View(r);
        }

        [AllowAnonymous]
        public ActionResult SantiagoNLMantenimiento(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        public ActionResult Transfer(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        // GET: PanelUsuarios
        public ActionResult TransferEnpadi(tarjetasContactos contactos, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            strSQL = string.Format("SELECT * FROM TarjetasContactos WHERE tarjetaID = {0}", usuario.tarjetaID);
            IEnumerable<tarjetasContactos> tarjetasContactos = db.Database.SqlQuery<tarjetasContactos>(strSQL);
            return View(tarjetasContactos);
        }

        // POST: Panel Usuarios
        [HttpPost]
        public ActionResult TransferEnpadi(int contacto, string monto, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            strSQL = string.Empty;
            decimal dMonto = 0;
            bool dConvert = decimal.TryParse(monto, out dMonto);

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
            string sTarjetaEnvia = usuario.tcNum.Substring(usuario.tcNum.Length - 4);

            tarjetas amigo = db.tarjetas.FirstOrDefault(x => x.tarjetaID == contacto);
            string sTarjetaRecibe = amigo.tcNum.Substring(amigo.tcNum.Length - 4);

            if (dConvert)
            {
                strSQL = String.Format("SELECT ISNULL(SUM(abonoTotal-cargoTotal),0) As Total FROM Transacciones WHERE tarjetaID = {0} AND tipoID IN (1,2)", usuario.tarjetaID);
                IEnumerable<total> saldoActual = db.Database.SqlQuery<total>(strSQL);

                if (saldoActual.ToList().FirstOrDefault().Total >= dMonto)
                {
                    string sTransaccion = DateTime.Now.ToString("yyyyMMddHHmmss");
                    string sAprobacion = "329875";
                    trans cargo = new trans() { estatusID = 1, origenID = 2, tipoID = 2, modoID = 1, socioID = 0, terminalID = 0, monedaID = 484, tarjetaID = usuario.tarjetaID, tarjeta = sTarjetaRecibe, transaccion = sTransaccion, monto = dMonto, abono1 = 0, cargo1 = dMonto, abono2 = 0, cargo2 = 0, abono3 = 0, cargo3 = 0, abonoTotal = 0, cargoTotal = dMonto, Concepto = "Transferencia", descripcion = "Transferencia entre Cuentas", referencia = amigo.tcNum, aprobacion = sAprobacion, ip = "189.25.169.74", geoCountryCode = 0, geoCountryName = string.Empty, geoRegion = string.Empty, geoCity = string.Empty, geoPostalCode = string.Empty };
                    trans abono = new trans() { estatusID = 1, origenID = 2, tipoID = 1, modoID = 1, socioID = 0, terminalID = 0, monedaID = 484, tarjetaID = contacto, tarjeta = sTarjetaEnvia, transaccion = sTransaccion, monto = dMonto, abono1 = dMonto, cargo1 = 0, abono2 = 0, cargo2 = 0, abono3 = 0, cargo3 = 0, abonoTotal = dMonto, cargoTotal = 0, Concepto = "Transferencia", descripcion = "Transferencia entre Cuentas", referencia = usuario.tcNum, aprobacion = sAprobacion, ip = "189.25.169.74", geoCountryCode = 0, geoCountryName = string.Empty, geoRegion = string.Empty, geoCity = string.Empty, geoPostalCode = string.Empty };
                    db.transacciones.Add(cargo);
                    db.transacciones.Add(abono);
                    db.SaveChanges();
                    //Procedimiento Valido
                    return RedirectToAction("Index", "Mobile");
                }
                else
                    ModelState.AddModelError("", "Saldo Insuficiente");
            }
            else
                ModelState.AddModelError("", "Proporcione el Monto a Transferir");

            strSQL = string.Format("SELECT * FROM TarjetasContactos WHERE tarjetaID = {0}", usuario.tarjetaID);
            IEnumerable<tarjetasContactos> tarjetasContactos = db.Database.SqlQuery<tarjetasContactos>(strSQL);
            return View(tarjetasContactos);
        }

        public ActionResult Balance(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            ViewModelEstadoDeCuenta edoCuenta = new ViewModelEstadoDeCuenta();

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
            int tID = usuario.tarjetaID;

            edoCuenta.usuario = usuario;

            strSQL = " SELECT t.transID, o.descripcion AS origen, i.descripcion AS tipo, s.nombreComercial As socio, terminalID, "
                   + " m.codigo AS moneda, t.tarjetaID, t.tarjeta, t.monto, t.transaccion, t.concepto, t.descripcion, t.referencia, "
                   + " e.descripcion As estatus, t.aprobacion, t.ip, t.fechaRegistro "
                   + " FROM Transacciones t "
                   + " LEFT JOIN Origen o ON t.origenID = o.origenID "
                   + " LEFT JOIN Tipo i ON t.tipoID = i.tipoID "
                   + " LEFT JOIN Socios s ON t.socioID = s.socioID "
                   + " LEFT JOIN Monedas m ON t.monedaID = m.monedaID "
                   + " LEFT JOIN Estatus e ON t.estatusID = e.estatusID "
                   + " WHERE t.tarjetaID = " + tID + " AND DateDiff(MONTH, GetDate(), t.FechaRegistro) = 0 "
                   + " ORDER BY t.fechaRegistro ";
            IEnumerable<movimientosListado> movs = db.Database.SqlQuery<movimientosListado>(strSQL);
            edoCuenta.movimientos = movs.ToList();

            strSQL = " SELECT ISNULL(SUM(abonoTotal-cargoTotal), 0) As Total "
                   + " FROM Transacciones "
                   + " WHERE tarjetaID = " + tID + " AND DateDiff(MONTH, GetDate(), FechaRegistro) < 0 ";
            IEnumerable<total> saldoInicial = db.Database.SqlQuery<total>(strSQL);
            edoCuenta.saldoInicial = saldoInicial.ToList().FirstOrDefault().Total;

            strSQL = " SELECT ISNULL(SUM(abonoTotal), 0) As Total "
                   + " FROM Transacciones "
                   + " WHERE tarjetaID = " + tID + " AND tipoID = 1 AND DateDiff(MONTH, GetDate(), FechaRegistro) = 0 ";
            IEnumerable<total> abonos = db.Database.SqlQuery<total>(strSQL);
            edoCuenta.abonos = abonos.ToList().FirstOrDefault().Total;

            strSQL = " SELECT ISNULL(SUM(cargoTotal), 0) As Total "
                   + " FROM Transacciones "
                   + " WHERE tarjetaID = " + tID + " AND tipoID = 2 AND DateDiff(MONTH, GetDate(), FechaRegistro) = 0 ";
            IEnumerable<total> cargos = db.Database.SqlQuery<total>(strSQL);
            edoCuenta.cargos = cargos.ToList().FirstOrDefault().Total;

            strSQL = " SELECT ISNULL(SUM(abonoTotal-cargoTotal),0) As Total "
                   + " FROM Transacciones "
                   + " WHERE tarjetaID = " + tID + " AND DateDiff(MONTH, GetDate(), FechaRegistro) <= 0 ";
            IEnumerable<total> saldoFinal = db.Database.SqlQuery<total>(strSQL);
            edoCuenta.saldoFinal = saldoFinal.ToList().FirstOrDefault().Total;

            return View(edoCuenta);

        }

        public ActionResult Rewards()
        {
            return View();
        }

        public ActionResult Barcode(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (User.IsInRole("Usuario"))
            {
                ViewModelNavigation perfil = new ViewModelNavigation();

                string currentUserId = User.Identity.GetUserId();
                tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
                perfil.usuario = usuario;

                strSQL = string.Empty;
                strSQL = " SELECT ISNULL(SUM(abonoTotal-cargoTotal),0) As Total "
                       + " FROM Transacciones "
                       + " WHERE tarjetaID = " + usuario.tarjetaID + " AND tipoID IN (1,2) ";
                IEnumerable<total> saldoActual = db.Database.SqlQuery<total>(strSQL);
                perfil.saldoActual = saldoActual.ToList().FirstOrDefault().Total;

                ViewBag.ImageData = GenerateQR(250, 60, usuario.tcNum);

                return View(perfil);
            }

            return new EmptyResult();
        }

        public byte[] GenerateQR(int width, int height, string text)
        {
            var bw = new BarcodeWriter();
            var encOptions = new ZXing.Common.EncodingOptions() { Width = width, Height = height, Margin = 0 };
            bw.Options = encOptions;
            bw.Format = BarcodeFormat.PDF_417;
            var bmp = new Bitmap(bw.Write(text));
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(bmp, typeof(byte[]));
        }


        public ActionResult ContactIndex(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            strSQL = string.Format("SELECT * FROM TarjetasContactos WHERE tarjetaID = {0}", usuario.tarjetaID);
            IEnumerable<tarjetasContactos> tarjetasContactos = db.Database.SqlQuery<tarjetasContactos>(strSQL);
            return View(tarjetasContactos);
        }

        public ActionResult ContactNew(string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        // POST: tarjetasContactos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult ContactNew([Bind(Include = "contactoID,tarjetaID,contactoTarjeta,contactoNumero,contactoAlias")] tarjetasContactos tarjetasContactos, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                string currentUserId = User.Identity.GetUserId();
                tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
                tarjetasContactos.tarjetaID = usuario.tarjetaID;

                strSQL = string.Format("SELECT Count(*) As total FROM Tarjetas WHERE tcNum = '{0}' AND tcAsignada = 1 AND tcActiva = 1 AND fechaBaja IS NULL", tarjetasContactos.contactoTarjeta);
                IEnumerable<contador> tarjetaExiste = db.Database.SqlQuery<contador>(strSQL);
                int iExiste = tarjetaExiste.ToList().FirstOrDefault().total;

                if (iExiste > 0)
                {
                    strSQL = string.Format("SELECT tarjetaID As total FROM Tarjetas WHERE tcNum = '{0}' AND tcAsignada = 1 AND tcActiva = 1 AND fechaBaja IS NULL", tarjetasContactos.contactoTarjeta);
                    IEnumerable<contador> contacto = db.Database.SqlQuery<contador>(strSQL);
                    tarjetasContactos.contactoNumero = contacto.ToList().FirstOrDefault().total;

                    if (tarjetasContactos.tarjetaID == tarjetasContactos.contactoNumero)
                    {
                        ModelState.AddModelError("", "No puede agregar su propia cuenta como Contacto");
                        return View(tarjetasContactos);
                    }
                    else
                    {
                        db.tarjetasContactos.Add(tarjetasContactos);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Numero de Cuenta Invalido");
                    return View(tarjetasContactos);
                }
            }
            return View(tarjetasContactos);
        }


        // GET: tarjetasContactos/Edit/5
        public ActionResult ContactEdit(int? id, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tarjetasContactos tarjetasContactos = db.tarjetasContactos.Find(id);
            if (tarjetasContactos == null)
            {
                return HttpNotFound();
            }
            return View(tarjetasContactos);
        }

        // POST: tarjetasContactos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult ContactEdit([Bind(Include = "contactoID,tarjetaID,contactoTarjeta,contactoNumero,contactoAlias")] tarjetasContactos tarjetasContactos, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                db.Entry(tarjetasContactos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ContactIndex");
            }
            return View(tarjetasContactos);
        }


        // GET: tarjetasContactos/Details/5
        public ActionResult ContactDetails(int? id, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tarjetasContactos tarjetasContactos = db.tarjetasContactos.Find(id);
            if (tarjetasContactos == null)
            {
                return HttpNotFound();
            }
            return View(tarjetasContactos);
        }

        // GET: tarjetasContactos/Delete/5
        public ActionResult ContactDelete(int? id, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tarjetasContactos tarjetasContactos = db.tarjetasContactos.Find(id);
            if (tarjetasContactos == null)
            {
                return HttpNotFound();
            }
            return View(tarjetasContactos);
        }

        // POST: tarjetasContactos/Delete/5
        [HttpPost]
        public ActionResult ContactDelete(int id, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            tarjetasContactos tarjetasContactos = db.tarjetasContactos.Find(id);
            db.tarjetasContactos.Remove(tarjetasContactos);
            db.SaveChanges();
            return RedirectToAction("ContactIndex","Mobile");
        }

    }
}