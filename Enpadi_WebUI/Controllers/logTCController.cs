﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Enpadi_WebUI.Models;

namespace Enpadi_WebUI.Controllers
{
    public class logTCController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: logTC
        public ActionResult Index()
        {
            return View(db.logTCs.ToList());
        }

        // GET: logTC/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            logTC logTC = db.logTCs.Find(id);
            if (logTC == null)
            {
                return HttpNotFound();
            }
            return View(logTC);
        }

        // GET: logTC/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: logTC/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idLog,idTarjeta,result")] logTC logTC)
        {
            if (ModelState.IsValid)
            {
                db.logTCs.Add(logTC);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(logTC);
        }

        // GET: logTC/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            logTC logTC = db.logTCs.Find(id);
            if (logTC == null)
            {
                return HttpNotFound();
            }
            return View(logTC);
        }

        // POST: logTC/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idLog,idTarjeta,result")] logTC logTC)
        {
            if (ModelState.IsValid)
            {
                db.Entry(logTC).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(logTC);
        }

        // GET: logTC/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            logTC logTC = db.logTCs.Find(id);
            if (logTC == null)
            {
                return HttpNotFound();
            }
            return View(logTC);
        }

        // POST: logTC/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            logTC logTC = db.logTCs.Find(id);
            db.logTCs.Remove(logTC);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
