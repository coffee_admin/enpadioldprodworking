﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Enpadi_WebUI.Models;

namespace Enpadi_WebUI.Controllers
{
    public class openpayCustomersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: openpayCustomers
        public ActionResult Index()
        {
            return View(db.openpayCustomers.ToList());
        }

        // GET: openpayCustomers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            openpayCustomer openpayCustomer = db.openpayCustomers.Find(id);
            if (openpayCustomer == null)
            {
                return HttpNotFound();
            }
            return View(openpayCustomer);
        }

        // GET: openpayCustomers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: openpayCustomers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,CustomerID,ExternalID,Name,LastName,Email,PhoneNumber,RequiresAccount,Address1,Address2,Address3,City,State,CountryCode,PostalCode")] openpayCustomer openpayCustomer)
        {
            if (ModelState.IsValid)
            {
                db.openpayCustomers.Add(openpayCustomer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(openpayCustomer);
        }

        // GET: openpayCustomers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            openpayCustomer openpayCustomer = db.openpayCustomers.Find(id);
            if (openpayCustomer == null)
            {
                return HttpNotFound();
            }
            return View(openpayCustomer);
        }

        // POST: openpayCustomers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,CustomerID,ExternalID,Name,LastName,Email,PhoneNumber,RequiresAccount,Address1,Address2,Address3,City,State,CountryCode,PostalCode")] openpayCustomer openpayCustomer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(openpayCustomer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(openpayCustomer);
        }

        // GET: openpayCustomers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            openpayCustomer openpayCustomer = db.openpayCustomers.Find(id);
            if (openpayCustomer == null)
            {
                return HttpNotFound();
            }
            return View(openpayCustomer);
        }

        // POST: openpayCustomers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            openpayCustomer openpayCustomer = db.openpayCustomers.Find(id);
            db.openpayCustomers.Remove(openpayCustomer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
