﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using Enpadi_WebUI.Models;
using Openpay;
using Openpay.Entities;
using Openpay.Entities.Request;
using Microsoft.AspNet.Identity;
using System.Configuration;
using Enpadi_WebUI.Properties;

namespace Enpadi_WebUI.Controllers
{
    public class SantiagoNLController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private static bool bModoProduccion = Convert.ToBoolean(ConfigurationManager.AppSettings["ModoProduccion"]);

        private string OPENPAY_ID = (bModoProduccion) ? ConfigurationManager.AppSettings["Enpadi_OpenPay_Id"] : ConfigurationManager.AppSettings["Test_OpenPay_id"];
        private string OPENPAY_PUBLIC_KEY = (bModoProduccion) ? ConfigurationManager.AppSettings["Enpadi_OpenPay_PublicKey"] : ConfigurationManager.AppSettings["Test_OpenPay_PublicKey"];

        private string OPENPAY_PRIVATE_KEY = (bModoProduccion) ? Settings.Default.Enpadi_OpenPay_PrivateKey : Settings.Default.Test_OpenPay_PrivateKey;

        private string DIESTEL_URL = (bModoProduccion) ? Settings.Default.Enpadi_WebUI_WSDiestel_PxUniversal : Settings.Default.Test_WebUI_WSDiestel_PxUniversal;
        private int DIESTEL_GRUPO = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Grupo : Settings.Default.Test_WSDiestel_Grupo;
        private int DIESTEL_CADENA = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Cadena : Settings.Default.Test_WSDiestel_Cadena;
        private int DIESTEL_TIENDA = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Tienda : Settings.Default.Test_WSDiestel_Tienda;
        private string DIESTEL_USUARIO = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Usuario : Settings.Default.Test_WSDiestel_Usuario;
        private string DIESTEL_PASSWORD = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Password : Settings.Default.Test_WSDiestel_Password;

        // GET: SantiagoNL
        [AllowAnonymous]
        public ActionResult Index(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult Index(santiagoPredialConsulta consulta, string skin)
        {

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                if (consulta.terminos)
                {
                    strSQL = String.Format("SELECT COUNT(*) AS Total FROM SantiagoPredial WHERE expediente = '{0}'", consulta.no_predial);
                    IEnumerable<contador> total = db.Database.SqlQuery<contador>(strSQL);
                    consulta.fecha_registro = DateTime.Now;
                    db.santiagoPredialConsultas.Add(consulta);
                    db.SaveChanges();

                    if (total.FirstOrDefault().total > 0)
                    {
                        return RedirectToAction("Predial", "SantiagoNL", consulta);
                    }
                    else
                    {
                        ModelState.AddModelError("no_predial", "No hemos encontrado ningun Expediente con los datos proporcionados. Favor de acudir a la Oficina de Tesoreria para brindarle atención personalizada.");
                        return View(consulta);
                    }
                } 
                else
                {
                    ModelState.AddModelError("terminos", "Debe ACEPTAR los Terminos del Aviso de Privacidad");
                    return View(consulta);
                }
            }
            else
            {
                return View(consulta);
            }
        }

        [AllowAnonymous]
        public ActionResult Predial(santiagoPredialConsulta consulta, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            santiagoPredial predial = db.santiagoPredials.FirstOrDefault(x => x.expediente == consulta.no_predial);
            predial.email = consulta.correo;

            return View(predial);
        }

        [AllowAnonymous]
        public ActionResult ReferenciaBancaria(string expediente, string direccion, string saldoPredial)
        {
            santiagoPredialBancos referencia = new santiagoPredialBancos() { expediente = expediente, ubicacionPredio = direccion, saldo = Convert.ToDecimal(saldoPredial), fechaAlta = DateTime.Now };
            db.santiagoPredialBancos.Add(referencia);
            db.SaveChanges();

            return View();
        }


        [AllowAnonymous]
        public ActionResult Tarjeta(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult Tarjeta(string token_id, string holder_name, string amount, string deviceSessionId, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (!ModelState.IsValid)
            {
                return View();
            }
            else
            {
                string currentUserId = User.Identity.GetUserId();
                tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
                trans t = new trans { estatusID = 0, origenID = 1, tipoID = 1, modoID = 1, socioID = 2, terminalID = 1, monedaID = 484, Concepto = "TC", geoCountryCode = 0, geoCountryName = String.Empty, geoRegion = String.Empty, geoCity = String.Empty, geoPostalCode = String.Empty };

                strSQL = string.Format("SELECT COUNT(*) As total FROM logTC WHERE idTarjeta = {0} AND fechaAlta >= DATEADD(DAY, -7, getdate())", usuario.tarjetaID);
                IEnumerable<contador> intentos = db.Database.SqlQuery<contador>(strSQL);
                int iIntentos = intentos.ToList().FirstOrDefault().total;

                strSQL = string.Format("SELECT COUNT(*) As total FROM logTC WHERE idTarjeta = {0} AND result = 1 AND fechaAlta >= DATEADD(DAY, -7, getdate())", usuario.tarjetaID);
                IEnumerable<contador> exitosos = db.Database.SqlQuery<contador>(strSQL);
                int iExitosos = exitosos.ToList().FirstOrDefault().total;

                if (iIntentos < 5 && iExitosos < 2)
                {
                    OpenpayAPI api = new OpenpayAPI(OPENPAY_PRIVATE_KEY, OPENPAY_ID);

                    Customer cliente = new Customer();
                    cliente.ExternalId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);
                    cliente.Name = holder_name;
                    cliente.Email = "mramireza@hotmail.com";
                    cliente.RequiresAccount = false;

                    ChargeRequest request = new ChargeRequest();
                    request.Method = "card";
                    request.SourceId = token_id;
                    request.Amount = Convert.ToDecimal(amount);
                    request.Currency = "MXN";
                    request.Description = "Recarga de Monedero Enpadi";
                    request.DeviceSessionId = deviceSessionId;
                    request.Customer = cliente;
                    request.OrderId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);

                    Charge charge = api.ChargeService.Create(request);

                    if (charge.Status == "completed")
                    {
                        t.estatusID = 1;
                        t.tarjetaID = usuario.tarjetaID;
                        t.tarjeta = usuario.tcNum.Substring(usuario.tcNum.Length - 4);
                        t.transaccion = charge.OrderId;
                        t.descripcion = charge.Description;
                        t.referencia = charge.Id;
                        t.aprobacion = charge.Authorization;
                        t.monto = charge.Amount; t.abono1 = charge.Amount; t.cargo1 = 0; t.abono2 = 0; t.cargo2 = 0; t.abono3 = 0; t.cargo3 = 0; t.abonoTotal = charge.Amount; t.cargoTotal = 0;
                        t.ip = "189.25.169.74";
                        db.transacciones.Add(t);

                        recompensa r = new recompensa() { tarjetaID = usuario.tarjetaID, origenID = 1, socioID = 2, transaccion = charge.OrderId, concepto = "Recompensa Recarga", abono = charge.Amount * (decimal)0.01, cargo = 0 };
                        db.recompensas.Add(r);

                        logTC envio = new logTC() { idTarjeta = usuario.tarjetaID, result = 1 };
                        db.logTCs.Add(envio);

                        db.SaveChanges();

                        return RedirectToAction("TarjetaResp", "SantiagoNL", new { estatus = "APROBADA", autorizacion = string.Format("AUTORIZACIÓN  {0}", t.aprobacion), total = string.Format("MONTO  ${0} MXN", t.monto) });
                    }
                    else
                    {
                        logTC envio = new logTC() { idTarjeta = usuario.tarjetaID, result = 0 };
                        db.logTCs.Add(envio);

                        db.SaveChanges();
                        return RedirectToAction("TarjetaResp", "SantiagoNL", new { estatus = "DECLINADA", autorizacion = charge.Description, total = "" });
                    }
                }
                else
                {
                    return RedirectToAction("TarjetaResp", "SantiagoNL", new { estatus = "DECLINADA", autorizacion = "Has excedido el Limite de Recargas Autorizado por Semana", total = "" });
                }
            }
        }


        // GET: PanelUsuarios
        [AllowAnonymous]
        public ActionResult TarjetaResp(string estatus, string autorizacion, string total, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            respuestaTC r = new respuestaTC() { Estatus = estatus, Autorizacion = autorizacion, Total = total };
            return View(r);
        }


        [AllowAnonymous]
        public ActionResult Mantenimiento(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }


    }
}