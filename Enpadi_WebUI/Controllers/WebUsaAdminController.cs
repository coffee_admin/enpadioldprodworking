﻿using Enpadi_WebUI.Models;
using Enpadi_WebUI.Properties;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Xml;
using System.Xml.Serialization;

namespace Enpadi_WebUI.Controllers
{
    public class WebUsaAdminController : Controller
    {

        #region constructors and inicializers

        private static bool bModoProduccion = Convert.ToBoolean(ConfigurationManager.AppSettings["ModoProduccion"]);

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        #region constructors
        public WebUsaAdminController()
        {
        }

        public WebUsaAdminController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }
        #endregion constructors


        #endregion constructors and inicializers 

        #region views
        // GET: WebUsaAdmin
        public ActionResult Index()
        {
            ApplicationDbContext db = new ApplicationDbContext();

            String userId = User.Identity.GetUserId();
            if (userId == null)
            {
                return RedirectToAction("LogIn");
            }

            String skin = "clean";
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        // GET: WebUsaAdmin
        public ActionResult LogIn(String errorMessage = "")
        {
            ApplicationDbContext db = new ApplicationDbContext();

            String skin = "clean";
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();
            ViewModelWebUsaLogIn model = new ViewModelWebUsaLogIn
            {
                Email = String.Empty,
                Password = String.Empty
            };

            ViewBag.ErrorMessage = errorMessage;

            return View(model);
        }
        #endregion views

        #region methods
        public ActionResult LoadUser()
        {
            ApplicationDbContext db = new ApplicationDbContext();

            String userId = User.Identity.GetUserId();
            if (userId == null)
            {
                return RedirectToAction("LogIn");
            }

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            var contextDB = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
            var storeDB = new UserStore<IdentityUser>(contextDB);
            var managerDB = new UserManager<IdentityUser>(storeDB);
            var userDB = managerDB.FindByEmail(usuario.email);

            Decimal accountBalance = trans.GetAccountStatus(usuario.tarjetaID);

            KioskoIndexViewModel model = new KioskoIndexViewModel
            {
                enpadiCard = EnpadiCardModel.Get(usuario.tarjetaID),
                user = AspNetUsersModel.Get(null, usuario.email),
                AccountBalance = accountBalance
            };

            model.user.ProfilePicUrl = GetProfilePicture();

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> LogInValidation(String email, String password)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            var result = await SignInManager.PasswordSignInAsync(email, password, false, shouldLockout: false);

            String skin = "clean";
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();
            ViewModelWebUsaLogIn model = new ViewModelWebUsaLogIn
            {
                Email = String.Empty,
                Password = String.Empty
            };

            switch (result)
            {
                case SignInStatus.Success:

                    String userId = db.Users.FirstOrDefault(x => x.Email == email).Id;
                    Guid _userId = Guid.Parse(userId);
                    if (_userId == ApplicationUser.Key.ConsulateId)
                    {

                        return RedirectToAction("Index", "WebUsaAdmin");
                    }
                    else
                    {
                        AuthenticationManager.SignOut();
                        return RedirectToAction("LogIn", "WebUsaAdmin", new { errorMessage = "Usuario incorrecto para esta plataforma." });
                    }
                case SignInStatus.LockedOut:
                    ViewBag.ErrorMessage = "Por seguridad el usuario se encuentra restringido.";
                    return View("LogIn", model);
                case SignInStatus.Failure:
                default:
                    ViewBag.ErrorMessage = "Mal usuario o contraseña.";
                    return View("LogIn", model);
            }

        }

        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("LogIn", "WebUsaAdmin");
        }

        public JsonResult LoadGeneralReport(String periodo)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            periodo = (Int32.Parse(periodo) * -1).ToString();
            Int32 _periodo = Int32.Parse(periodo);
            if (periodo == String.Empty)
            {
                periodo = "0";
            }

            string currentUserId = User.Identity.GetUserId();
            Guid _currentUserId = Guid.Parse(currentUserId);
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            var contextDB = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
            var storeDB = new UserStore<IdentityUser>(contextDB);
            var managerDB = new UserManager<IdentityUser>(storeDB);
            var userDB = managerDB.FindByEmail(usuario.email);

            List<UserAccountReportModel> items = new List<UserAccountReportModel>();
            List<Fee> fees = db.Fee.Where(x => x.UserToPayId == ApplicationUser.Key.ConsulateId).ToList();
            List<TransactionFee> transfee = new List<TransactionFee>();

            foreach (Fee fee in fees)
            {
                transfee.AddRange(db.TransactionFee.Where(x => x.FeeId == fee.Id).ToList());
            }

            items = Tools.GetGeneralReport(usuario.tarjetaID, _periodo);

            foreach (TransactionFee item in transfee)
            {
                trans trans = db.transacciones.FirstOrDefault(x => x.Id == item.TransaccionesId && DbFunctions.DiffMonths(DateTime.Now, x.fechaRegistro) == _periodo);
                if (trans != null)
                {
                    string _transactionDescripion = trans.descripcion;

                    string _beneficiary = "N/A";
                    string _buyer = "N/A";
                    string _amountToShowInWebUsaAdminReport = "";
                    string _type_descripcion = "";
                    string _status_descripcion = "";

                    if (trans.socioID == 11)
                    {
                        ImssPaymentsModel imssPayment = db.ImssPaymentsModel.Where(x => x.transaccionesId == trans.Id).FirstOrDefault();
                        if (imssPayment != null)
                        {
                            _beneficiary = imssPayment.beneficiary_name + " " + imssPayment.beneficiary_last + " " + imssPayment.beneficiary_mothers + " - " + imssPayment.beneficiary_phone_number;
                            _buyer = imssPayment.buyer_name + " - " + imssPayment.beneficiary_cell_phone_number;
                        }
                        _amountToShowInWebUsaAdminReport = trans.ServiceInDlls.GetValueOrDefault().ToString("C") + " USD ";
                    }
                    else if (trans.socioID == 12)
                    {
                        CemexPayments cemexPayment = db.CemexPayments.Where(x => x.transaccionesId == trans.Id).FirstOrDefault();
                        if (cemexPayment != null)
                        {
                            _beneficiary = cemexPayment.beneficiary_name + " " + cemexPayment.beneficiary_last + " " + cemexPayment.beneficiary_mothers + " - " + cemexPayment.beneficiary_phone_number;
                            _buyer = cemexPayment.buyer_name + " - " + cemexPayment.beneficiary_cell_phone_number;
                        }

                        _amountToShowInWebUsaAdminReport = trans.ServiceInDlls.GetValueOrDefault().ToString("C") + " USD ";
                    }
                    else
                    {
                        pagoServicio oPago = db.pagoServicios.Where(x => x.transaccionesId == trans.Id).FirstOrDefault();
                        if (oPago != null)
                        {
                            _beneficiary = oPago.referencia;
                        }

                        _amountToShowInWebUsaAdminReport = trans.ServiceInMXN.GetValueOrDefault().ToString("C") + " MXN ";
                    }

                    switch (trans.tipoID)
                    {
                        case 1:
                            _type_descripcion = "<label class='lang' style='font-weight: normal' key='tabledesc.deposit'></label>";
                            break;
                        case 2:
                            _type_descripcion = "<label class='lang' style='font-weight: normal' key='tabledesc.charge'></label>";
                            break;
                        case 3:
                            _type_descripcion = "<label class='lang' style='font-weight: normal' key='tabledesc.transfer'></label>";
                            break;
                        case 4:
                            _type_descripcion = "<label class='lang' style='font-weight: normal' key='tabledesc.return'></label>";
                            break;
                        case 5:
                            _type_descripcion = "<label class='lang' style='font-weight: normal' key='tabledesc.chargeback'></label>";
                            break;
                        case 6:
                            _type_descripcion = "<label class='lang' style='font-weight: normal' key='tabledesc.remittance'></label>";
                            break;
                        case 7:
                            _type_descripcion = "<label class='lang' style='font-weight: normal' key='tabledesc.reward'></label>";
                            break;
                        default:
                            _type_descripcion = "N/A";
                            break;
                    }

                    switch (trans.estatusID)
                    {
                        case 1:
                            _status_descripcion = "<label class='lang' style='font-weight: normal' key='tabledesc.approved'></label>";
                            break;
                        case 2:
                            _status_descripcion = "<label class='lang' style='font-weight: normal' key='tabledesc.rejected'></label>";
                            break;
                        case 3:
                            _status_descripcion = "<label class='lang' style='font-weight: normal' key='tabledesc.pending'></label>";
                            break;
                        default:
                            _status_descripcion = "N/A";
                            break;
                    }

                    //separate comission from services payed in MXN and services payed in dlls
                    Decimal ComissionDllsServicesInUSD = 0;
                    Decimal ComissionMXServicesInMX = 0;
                    switch (trans.socioID)
                    {
                        case socio.SocioID.Cemex_int_id:
                            ComissionDllsServicesInUSD += trans.ComissionUSD.GetValueOrDefault();
                            break;
                        case socio.SocioID.Imss_int_id:
                            ComissionDllsServicesInUSD += trans.ComissionUSD.GetValueOrDefault();
                            break;
                        default:
                            ComissionMXServicesInMX += trans.ComissionMX.GetValueOrDefault();
                            break;
                    }

                    //get transactions separeted between services payed in usd and in mxn
                    Decimal TransactionDllsServicesInUSD = 0;
                    Decimal TransactionMXServicesInMX = 0;

                    switch (trans.socioID)
                    {
                        case socio.SocioID.Cemex_int_id:
                            TransactionDllsServicesInUSD += trans.ServiceInDlls.GetValueOrDefault();
                            break;
                        case socio.SocioID.Imss_int_id:
                            TransactionDllsServicesInUSD += trans.ServiceInDlls.GetValueOrDefault();
                            break;
                        default:
                            TransactionMXServicesInMX += trans.ServiceInMXN.GetValueOrDefault();
                            break;
                    }


                    //get only the usa consulate fee
                    Fee fee = db.Fee.Where(x => x.Id == item.FeeId).FirstOrDefault();

                    Decimal comissionInUsdOnlyConsulate = 0;
                    Decimal comissionInMXNOnlyConsulate = 0;

                    if (fee.PartnerToPayId != socio.SocioID.Enpadi)
                    {
                        comissionInMXNOnlyConsulate += item.FixAmount + item.VariableAmount + item.TaxFeeAmount + item.ChargedByPartnerAmount;
                        comissionInUsdOnlyConsulate += item.FixAmountDlls.GetValueOrDefault() + item.VariableAmountDlls.GetValueOrDefault() + item.TaxFeeAmountDlls.GetValueOrDefault() + item.ChargedByPartnerAmount;
                    }

                    items.Add(
                        new UserAccountReportModel
                        {
                            transaccion_transID = trans.transID,
                            origin_descripcion = db.origenes.FirstOrDefault(x => x.origenID == trans.origenID).descripcion.ToString(),
                            type_descripcion = _type_descripcion,
                            transaccion_socioID = trans.socioID,
                            partner_nombreComercial = db.socios.FirstOrDefault(x => x.socioID == trans.socioID).nombreComercial,
                            transaccion_terminalID = trans.terminalID,
                            currency_codigo = db.monedas.FirstOrDefault(x => x.monedaID == trans.monedaID).codigo.ToString(),
                            transaccion_tarjetaID = trans.tarjetaID,
                            transaccion_tarjeta = trans.tarjeta,
                            transaccion_monto = trans.ServiceInDlls.GetValueOrDefault(),
                            transaccion_comision = (item.FixAmount / trans.ExchangeRateDown.GetValueOrDefault()),
                            transaccion_iva = trans.cargo2iva + trans.cargo3iva + trans.cargo4iva + trans.cargo5iva + trans.cargo6iva,
                            transaccion_concepto = trans.concepto,
                            transaccion_descripcion = _transactionDescripion,
                            transaccion_referencia = trans.referencia,
                            status_descripcion = _status_descripcion,
                            transaccion_aprobacion = trans.aprobacion,
                            transaccion_ip = trans.ip,
                            transaccion_fechaRegistro = (trans.fechaRegistro != null ? (DateTime)trans.fechaRegistro : DateTime.Now),
                            transaccion_transaccion = trans.transaccion,

                            exchangeRateDown = trans.ExchangeRateDown.GetValueOrDefault(),
                            exchangeRateUp = trans.ExchangeRateUp.GetValueOrDefault(),
                            
                            ComissionMX = comissionInMXNOnlyConsulate,
                            TotalAmountMX = trans.TotalMX,
                            ComissionUSD = comissionInUsdOnlyConsulate,
                            TotalAmountUSD = trans.TotalUSD,
                            AmountForServiceMXN = trans.ServiceInMXN,
                            AmountForServiceUSD = trans.ServiceInDlls,
                            transactionUserEmail = db.tarjetas.Where(x => x.tarjetaID == trans.tarjetaID).FirstOrDefault().email,
                            
                            beneficiary = _beneficiary,
                            buyer = _buyer,

                            AmountToShowInWebUsaAdminReport = _amountToShowInWebUsaAdminReport,

                            ComissionDllsServicesInUSD = ComissionDllsServicesInUSD,
                            ComissionMXServicesInMX = ComissionMXServicesInMX,

                            TransactionDllsServicesInUSD = TransactionDllsServicesInUSD,
                            TransactionMXServicesInMX = TransactionMXServicesInMX
                        }
                        );
                }
            }
            
            foreach (UserAccountReportModel item in items)
            {
                Guid _transactionid = db.transacciones.FirstOrDefault(x => x.transID == item.transaccion_transID).Id;

                decimal fixamount = transfee.Where(x => x.TransaccionesId == _transactionid).Sum(x => x.FixAmountDlls.GetValueOrDefault());
                decimal variableamount = transfee.Where(x => x.TransaccionesId == _transactionid).Sum(x => x.VariableAmountDlls.GetValueOrDefault());
                decimal taxfeeamount = transfee.Where(x => x.TransaccionesId == _transactionid).Sum(x => x.TaxFeeAmountDlls.GetValueOrDefault());
                decimal chargebypartneramount = transfee.Where(x => x.TransaccionesId == _transactionid).Sum(x => x.ChargedByPartnerAmount);

                item.transaccion_comision = fixamount + variableamount + taxfeeamount + chargebypartneramount;


            }


            return Json(items.OrderByDescending(x => x.transaccion_transID), JsonRequestBehavior.AllowGet);
        }


        #endregion methods

        #region tools 

        private String GetProfilePicture()
        {
            string result = String.Empty;
            string currentUserId = User.Identity.GetUserId();
            var user = UserManager.FindById(currentUserId);

            string path = System.IO.Path.Combine(
                                           Server.MapPath("~/UpLoads/ProfilePictures/"), currentUserId + ".jpg");

            if (System.IO.File.Exists(path))
            {
                AspNetUsersModel tempUser = AspNetUsersModel.Get(Guid.Parse(currentUserId), user.Email);
                result = tempUser.ProfilePicUrl;
            }
            else
            {
                result = "Images/account_no_image.png";
            }

            return result;
        }


        #endregion tools

    }
}