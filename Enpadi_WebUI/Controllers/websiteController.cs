﻿using Enpadi_WebUI.Models;
using Enpadi_WebUI.Properties;
using Enpadi_WebUI.WSDiestel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Openpay;
using Openpay.Entities;
using Openpay.Entities.Request;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Xml;
using System.Xml.Serialization;

namespace Enpadi_WebUI.Controllers
{
    public class websiteController : Controller
    {
        #region website

        #region global variables

        private static bool bModoProduccion = Convert.ToBoolean(ConfigurationManager.AppSettings["ModoProduccion"]);
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        #endregion global variables

        #region constructors and properties

        public websiteController()
        {
        }

        public websiteController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        #endregion constructors and properties

        #region Helpers

        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "website");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        #endregion helpers

        #region diestel

        private string DIESTEL_URL = (bModoProduccion) ? Settings.Default.Enpadi_WebUI_WSDiestel_PxUniversal : Settings.Default.Test_WebUI_WSDiestel_PxUniversal;
        private int DIESTEL_GRUPO = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Grupo : Settings.Default.Test_WSDiestel_Grupo;
        private int DIESTEL_CADENA = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Cadena : Settings.Default.Test_WSDiestel_Cadena;
        private int DIESTEL_TIENDA = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Tienda : Settings.Default.Test_WSDiestel_Tienda;
        private string DIESTEL_USUARIO = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Usuario : Settings.Default.Test_WSDiestel_Usuario;
        private string DIESTEL_PASSWORD = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Password : Settings.Default.Test_WSDiestel_Password;

        #endregion diestel

        #region openpay

        private string OPENPAY_ID = (bModoProduccion) ? ConfigurationManager.AppSettings["Enpadi_OpenPay_Id"] : ConfigurationManager.AppSettings["Test_OpenPay_id"];
        private string OPENPAY_PUBLIC_KEY = (bModoProduccion) ? ConfigurationManager.AppSettings["Enpadi_OpenPay_PublicKey"] : ConfigurationManager.AppSettings["Test_OpenPay_PublicKey"];
        private string OPENPAY_PRIVATE_KEY = (bModoProduccion) ? Settings.Default.Enpadi_OpenPay_PrivateKey : Settings.Default.Test_OpenPay_PrivateKey;

        #endregion openpay

        public ActionResult Profile()
        {
            string currentUserId = User.Identity.GetUserId();
            var user = UserManager.FindById(currentUserId);

            return View("Profile", new ViewModelProfile
            {
                Name = user.Name,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                ProfilePicUrl = GetProfilePicture()
            });


        }

        public ActionResult RedirectInformation()
        {
            return RedirectPermanent("~/website/index");
        }

        // GET: website
        public ActionResult Index()
        {
            ViewBag.color_1 = "FFFFFF";
            ViewBag.color_2 = "66CCFF";
            ViewBag.color_3 = "3399CC";
            ViewBag.color_4 = "006699";
            ViewBag.color_5 = "003366";
            ViewBag.color_6 = "000033";

            ViewBag.company = "Enpadi";

            ViewBag.website_seccion_title_1 = "Soluciones";
            ViewBag.website_seccion_title_2 = "Empresas";
            ViewBag.website_seccion_title_2_2 = "Individuos";
            ViewBag.website_seccion_title_3 = "Atención";

            ViewBag.contact = "Contacto";
            ViewBag.us = "Nosotros";
            ViewBag.fre_question = "Preguntas Frecuentes";

            ViewBag.carousel_image_1 = "Images/website/slide/1-EmpresasSlide-1-1.jpg";
            ViewBag.carousel_image_2 = "Images/website/slide/1-SlideShow-1-01.jpg";
            ViewBag.carousel_image_3 = "Images/website/slide/SlideIndividuos-1-1.jpg";

            ViewBag.index_image = "Images/website/slide/FondoEmpresas-Contacto1.jpg";

            return View();
        }

        // GET: website
        public ActionResult Company()
        {
            ViewBag.color_1 = "FFFFFF";
            ViewBag.color_2 = "66CCFF";
            ViewBag.color_3 = "3399CC";
            ViewBag.color_4 = "006699";
            ViewBag.color_5 = "003366";
            ViewBag.color_6 = "000033";

            ViewBag.company = "Enpadi";

            ViewBag.website_seccion_title_1 = "Como Empresa";
            ViewBag.website_seccion_title_2 = "Permítenos Ayudarte";
            ViewBag.website_seccion_title_3 = "Atención";

            ViewBag.contact = "Contacto";
            ViewBag.us = "Nosotros";
            ViewBag.fre_question = "Preguntas Frecuentes";


            ViewBag.carousel_image_1 = "Images/website/slide/1-EmpresasSlide-1-1.jpg";
            ViewBag.carousel_image_2 = "Images/website/slide/1-SlideShow-1-01.jpg";
            ViewBag.carousel_image_3 = "Images/website/slide/SlideIndividuos-1-1.jpg";

            ViewBag.index_image = "Images/website/background/Fondo.jpg";

            return View();
        }

        public ActionResult Clients()
        {
            ViewBag.color_1 = "FFFFFF";
            ViewBag.color_2 = "66CCFF";
            ViewBag.color_3 = "3399CC";
            ViewBag.color_4 = "006699";
            ViewBag.color_5 = "003366";
            ViewBag.color_6 = "000033";

            ViewBag.company = "Enpadi";

            ViewBag.website_seccion_title_1 = "Como Cliente";
            ViewBag.website_seccion_title_2 = "Permítenos Ayudarte";
            ViewBag.website_seccion_title_3 = "Atención";

            ViewBag.contact = "Contacto";
            ViewBag.us = "Nosotros";
            ViewBag.fre_question = "Preguntas Frecuentes";

            ViewBag.carousel_image_1 = "Images/website/slide/1-EmpresasSlide-1-1.jpg";
            ViewBag.carousel_image_2 = "Images/website/slide/1-SlideShow-1-01.jpg";
            ViewBag.carousel_image_3 = "Images/website/slide/SlideIndividuos-1-1.jpg";

            ViewBag.index_image = "Images/website/background/Fondo.jpg";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.color_1 = "FFFFFF";
            ViewBag.color_2 = "66CCFF";
            ViewBag.color_3 = "3399CC";
            ViewBag.color_4 = "006699";
            ViewBag.color_5 = "003366";
            ViewBag.color_6 = "000033";

            ViewBag.company = "Enpadi";

            ViewBag.website_seccion_title_1 = "Contáctanos";

            ViewBag.carousel_image_1 = "Images/website/slide/1-EmpresasSlide-1-1.jpg";
            ViewBag.carousel_image_2 = "Images/website/slide/1-SlideShow-1-01.jpg";
            ViewBag.carousel_image_3 = "Images/website/slide/SlideIndividuos-1-1.jpg";

            return View();
        }

        public ActionResult Registration()
        {
            ViewBag.color_1 = "FFFFFF";
            ViewBag.color_2 = "66CCFF";
            ViewBag.color_3 = "3399CC";
            ViewBag.color_4 = "006699";
            ViewBag.color_5 = "003366";
            ViewBag.color_6 = "000033";

            ViewBag.company = "Enpadi";

            ViewBag.website_seccion_title_1 = "Registro";

            ViewBag.carousel_image_1 = "Images/website/slide/1-EmpresasSlide-1-1.jpg";
            ViewBag.carousel_image_2 = "Images/website/slide/1-SlideShow-1-01.jpg";
            ViewBag.carousel_image_3 = "Images/website/slide/SlideIndividuos-1-1.jpg";

            return View();
        }

        public ActionResult LogIn()
        {
            ViewBag.color_1 = "FFFFFF";
            ViewBag.color_2 = "66CCFF";
            ViewBag.color_3 = "3399CC";
            ViewBag.color_4 = "006699";
            ViewBag.color_5 = "003366";
            ViewBag.color_6 = "000033";

            ViewBag.company = "Enpadi";

            ViewBag.website_seccion_title_1 = "Inicia sesión";

            ViewBag.carousel_image_1 = "Images/website/slide/1-EmpresasSlide-1-1.jpg";
            ViewBag.carousel_image_2 = "Images/website/slide/1-SlideShow-1-01.jpg";
            ViewBag.carousel_image_3 = "Images/website/slide/SlideIndividuos-1-1.jpg";

            return View();
        }

        public ActionResult Faq()
        {
            ViewBag.color_1 = "FFFFFF";
            ViewBag.color_2 = "66CCFF";
            ViewBag.color_3 = "3399CC";
            ViewBag.color_4 = "006699";
            ViewBag.color_5 = "003366";
            ViewBag.color_6 = "000033";

            ViewBag.company = "Enpadi";

            ViewBag.website_seccion_title_1 = "Preguntas frecuentes";

            ViewBag.carousel_image_1 = "Images/website/slide/1-EmpresasSlide-1-1.jpg";
            ViewBag.carousel_image_2 = "Images/website/slide/1-SlideShow-1-01.jpg";
            ViewBag.carousel_image_3 = "Images/website/slide/SlideIndividuos-1-1.jpg";

            return View();
        }

        public ActionResult Notice()
        {
            ViewBag.color_1 = "FFFFFF";
            ViewBag.color_2 = "66CCFF";
            ViewBag.color_3 = "3399CC";
            ViewBag.color_4 = "006699";
            ViewBag.color_5 = "003366";
            ViewBag.color_6 = "000033";

            ViewBag.company = "Enpadi";

            ViewBag.website_seccion_title_1 = "Aviso de privacidad";

            ViewBag.carousel_image_1 = "Images/website/slide/1-EmpresasSlide-1-1.jpg";
            ViewBag.carousel_image_2 = "Images/website/slide/1-SlideShow-1-01.jpg";
            ViewBag.carousel_image_3 = "Images/website/slide/SlideIndividuos-1-1.jpg";

            return View();
        }

        public ActionResult Terms()
        {
            ViewBag.color_1 = "FFFFFF";
            ViewBag.color_2 = "66CCFF";
            ViewBag.color_3 = "3399CC";
            ViewBag.color_4 = "006699";
            ViewBag.color_5 = "003366";
            ViewBag.color_6 = "000033";

            ViewBag.company = "Enpadi";

            ViewBag.website_seccion_title_1 = "Terminos y condiciones";

            ViewBag.carousel_image_1 = "Images/website/slide/1-EmpresasSlide-1-1.jpg";
            ViewBag.carousel_image_2 = "Images/website/slide/1-SlideShow-1-01.jpg";
            ViewBag.carousel_image_3 = "Images/website/slide/SlideIndividuos-1-1.jpg";

            return View();
        }

        public ActionResult AdminCard()
        {
            // Skin Aplicacion
            string strSQL = String.Empty;
            ApplicationDbContext db = new ApplicationDbContext();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            return View(db.openpayCards.Where(x => x.ExternalID == usuario.tarjetaID).ToList());
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(string Name, string Email, string PhoneNumber, string Password)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var contextDB = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
            var storeDB = new UserStore<IdentityUser>(contextDB);
            var managerDB = new UserManager<IdentityUser>(storeDB);
            var userDB = managerDB.Users.Where(u => u.PhoneNumber == PhoneNumber || u.Email == Email).FirstOrDefault();

            if (userDB != null)
            {
                ViewBag.color_1 = "FFFFFF";
                ViewBag.color_2 = "66CCFF";
                ViewBag.color_3 = "3399CC";
                ViewBag.color_4 = "006699";
                ViewBag.color_5 = "003366";
                ViewBag.color_6 = "000033";

                ViewBag.company = "Enpadi";

                ViewBag.website_seccion_title_1 = "Registro";

                ViewBag.carousel_image = "Images/website/slide/SlideIndividuos-1-1.jpg";

                ViewBag.ErrorMessage = "Ya existe una cuenta registrada con el mismo Número Movil o correo electrónico";

                ViewBag.Name = Name;
                ViewBag.Email = Email;
                ViewBag.PhoneNumber = PhoneNumber;

                return View("Registration");
            }
            else
            {
                var user = new ApplicationUser
                {
                    UserName = Email,
                    Email = Email,
                    Name = Name,
                    PhoneNumber = PhoneNumber,
                    TwoFactorEnabled = false,
                    EmailConfirmed = false
                };

                var result = await UserManager.CreateAsync(user, Password);

                var context = new ApplicationDbContext();
                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);

                if (result.Succeeded)
                {
                    userManager.AddToRole(user.Id, SecurityRoles.Usuario);
                    tarjetas tc = db.tarjetas.Where(t => t.tcAsignada == false).FirstOrDefault();
                    tc.tcAsignada = true;
                    tc.datNombre = Name;
                    tc.email = Email;
                    tc.password = user.Id;
                    db.Entry(tc).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                    await SignInManager.PasswordSignInAsync(Email, Password, false, shouldLockout: false);

                    return RedirectToAction("UserIndex", "website");

                }
                else
                {
                    return View("Registration");
                }
            }
        }

        [AllowAnonymous]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(string email, string password)
        {
            var result = await SignInManager.PasswordSignInAsync(email, password, false, shouldLockout: false);

            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToActionPermanent("UserIndex", "website");
                case SignInStatus.LockedOut:
                    ViewBag.color_1 = "FFFFFF";
                    ViewBag.color_2 = "66CCFF";
                    ViewBag.color_3 = "3399CC";
                    ViewBag.color_4 = "006699";
                    ViewBag.color_5 = "003366";
                    ViewBag.color_6 = "000033";

                    ViewBag.company = "Enpadi";

                    ViewBag.website_seccion_title_1 = "Inicia sesión";

                    ViewBag.carousel_image = "Images/website/slide/SlideIndividuos-1-1.jpg";


                    ViewBag.ErrorMessage = "El usuario ha sido bloqueado, favor de comunicarse a Enpadi.";
                    return View("LogIn");
                case SignInStatus.Failure:
                default:
                    ViewBag.color_1 = "FFFFFF";
                    ViewBag.color_2 = "66CCFF";
                    ViewBag.color_3 = "3399CC";
                    ViewBag.color_4 = "006699";
                    ViewBag.color_5 = "003366";
                    ViewBag.color_6 = "000033";

                    ViewBag.company = "Enpadi";

                    ViewBag.website_seccion_title_1 = "Inicia sesión";

                    ViewBag.carousel_image = "/Images/website/slide/SlideIndividuos-1-1.jpg";
                    ViewBag.ErrorMessage = "Usuario o contraseña incorrecta.";
                    return View("LogIn");
            }
        }

        #endregion website

        #region views user inner system

        public ActionResult PendingOrders()
        {

            return View();
        }

        public ActionResult AddCustomer([Bind(Include = "id,CustomerID,ExternalID,Name,LastName,Email,PhoneNumber,RequiresAccount,Address1,Address2,Address3,City,State,CountryCode,PostalCode")] openpayCustomer opCustomer)
        {
            // Skin Aplicacion
            string strSQL = String.Empty;
            ApplicationDbContext db = new ApplicationDbContext();

            if (ModelState.IsValid)
            {
                string currentUserId = User.Identity.GetUserId();
                tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
                opCustomer.ExternalID = usuario.tarjetaID;

                OpenpayAPI api = new OpenpayAPI(OPENPAY_PRIVATE_KEY, OPENPAY_ID, bModoProduccion);

                Openpay.Entities.Customer cliente = new Openpay.Entities.Customer()
                {
                    ExternalId = opCustomer.ExternalID.ToString(),
                    Name = opCustomer.Name,
                    LastName = opCustomer.LastName,
                    Email = opCustomer.Email,
                    PhoneNumber = opCustomer.PhoneNumber
                };

                Openpay.Entities.Address dir = new Openpay.Entities.Address()
                {
                    Line1 = opCustomer.Address1,
                    Line2 = opCustomer.Address2,
                    Line3 = opCustomer.Address3,
                    City = opCustomer.City,
                    State = opCustomer.State,
                    CountryCode = opCustomer.CountryCode,
                    PostalCode = opCustomer.PostalCode
                };

                cliente.Address = dir;

                cliente = api.CustomerService.Create(cliente);

                if (cliente.Id != null)
                {
                    opCustomer.CustomerID = cliente.Id;

                    opCustomer.CLABE = cliente.CLABE;

                    //MARCOS: this is marcos dev, not working with the id structure on the database, made the model and DAO
                    //db.openpayCustomers.Add(opCustomer);
                    //db.SaveChanges();

                    OpenPayCustomerModel.Save(opCustomer);

                    return RedirectToAction("RechargeCredit");
                }
                else
                {
                    return View(opCustomer);
                }
            }

            return View(opCustomer);

        }

        public ActionResult AddCard(string holder_name, string card_number, string cvv2, string expiration_month, string expiration_year, string token_id, string deviceSessionId)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            string strSQL = String.Empty;
            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            strSQL = String.Format("SELECT COUNT(*) AS total FROM opCards WHERE ExternalID = {0}", usuario.tarjetaID);
            IEnumerable<contador> registradas = db.Database.SqlQuery<contador>(strSQL);
            int iRegistradas = registradas.ToList().FirstOrDefault().total;

            if (holder_name != null ||
                card_number != null ||
                cvv2 != null ||
                expiration_month != null ||
                expiration_year != null ||
                token_id != null ||
                deviceSessionId != null)
            {
                if (iRegistradas < 3)
                {
                    openpayCard opCard = new openpayCard() { ExternalID = usuario.tarjetaID };
                    openpayCustomer opCustomer = db.openpayCustomers.FirstOrDefault(x => x.ExternalID == usuario.tarjetaID);
                    opCard.CustomerId = opCustomer.CustomerID;

                    OpenpayAPI api = new OpenpayAPI(OPENPAY_PRIVATE_KEY, OPENPAY_ID, bModoProduccion);
                    Card card = new Card();
                    card.TokenId = token_id;
                    card.DeviceSessionId = deviceSessionId;

                    try
                    {
                        card = api.CardService.Create(opCard.CustomerId.ToString(), card);

                        if (card.Id != null)
                        {
                            if (card.Address == null)
                            {
                                card.Address = new Openpay.Entities.Address();
                            }

                            opCard.HolderName = holder_name;
                            opCard.CardNumber = card_number;
                            opCard.ExpirationMonth = expiration_month;
                            opCard.ExpirationYear = expiration_year;
                            opCard.CVV2 = cvv2;
                            opCard.CardID = card.Id;
                            opCard.Brand = card.Brand;
                            opCard.Type = card.Type;
                            opCard.BankName = card.BankName;
                            opCard.BankCode = card.BankCode;
                            opCard.Address1 = card.Address.Line1;
                            opCard.Address2 = card.Address.Line2;
                            opCard.Address3 = card.Address.Line3;
                            opCard.PointsCard = card.PointsCard;
                            opCard.PostalCode = card.Address.PostalCode;
                            opCard.Country = card.Address.CountryCode;
                            opCard.AllowCharges = card.AllowsCharges;
                            opCard.AllowPayouts = card.AllowsPayouts;
                            opCard.BankCode = card.BankCode;
                            opCard.BankName = card.BankName;
                            opCard.City = card.Address.City;
                            opCard.ExpirationMonth = card.ExpirationMonth;
                            opCard.ExpirationYear = card.ExpirationYear;

                            OpenPayCardModel.Save(opCard);


                            return RedirectToAction("RechargeCredit");
                        }
                        else
                        {
                            ModelState.AddModelError("", "<div class='lang' key='addcard.carderror'></div>");
                        }
                    }
                    catch (OpenpayException ex)
                    {
                        Console.WriteLine(ex.Message);
                        return View(opCard);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "<div class='lang' key='addcard.cardslimit'></div>");
                }

            }
            return View();
        }

        public ActionResult ProcessIMSS()
        {
            return View();
        }

        public ActionResult AddCharge(string skin)
        {
            // Skin Aplicacion
            ApplicationDbContext db = new ApplicationDbContext();
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            strSQL = string.Format("SELECT * FROM opCards WHERE DeleteDate IS NULL AND ExternalID = {0}", usuario.tarjetaID);
            IEnumerable<openpayCard> cards = db.Database.SqlQuery<openpayCard>(strSQL);

            return View(cards.ToList());
        }

        public ActionResult GetCreditCardFee(decimal amount, string CardId)
        {

            decimal totalFee = GetFeeForCharge(amount, CardId);

            return Json(totalFee.ToString("C"), JsonRequestBehavior.AllowGet);
        }

        public decimal GetFeeForCharge(decimal amount, string CardId)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            Guid _currentUserId = Guid.Parse(User.Identity.GetUserId());
            openpayCard card = db.openpayCards.FirstOrDefault(x => x.CardID == CardId);

            Decimal totalFee = 0;

            if (card.Brand == "visa")
            {
                totalFee = Enpadi_WebUI.Models.Fee.GetTotalFeeInMXN(db.Fee.Where(x => x.Id == Models.Fee.Keys.VisaFeeId).ToList(), amount, 20, Coffee.GetPlatformFromRegion(), 0, false);
            }
            else if (card.Brand == "mastercard")
            {
                totalFee = Enpadi_WebUI.Models.Fee.GetTotalFeeInMXN(db.Fee.Where(x => x.Id == Models.Fee.Keys.masterCardFeeId).ToList(), amount, 20, Coffee.GetPlatformFromRegion(), 0, false);
            }
            else
            {
                totalFee = Enpadi_WebUI.Models.Fee.GetTotalFeeInMXN(db.Fee.Where(x => x.Id == Models.Fee.Keys.AmexFeeId).ToList(), amount, 20, Coffee.GetPlatformFromRegion(), 0, false);
            }

            return totalFee;
        }

        [HttpPost]
        public ActionResult AddCharge(string token_id, string deviceSessionId, string source_id, string amount)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            string strSQL = String.Empty;

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            if (ModelState.IsValid)
            {
                trans t = new trans
                {
                    Id = Guid.NewGuid(),
                    estatusID = 0,
                    origenID = 1,
                    tipoID = 1,
                    modoID = 1,
                    terminalID = 1,
                    monedaID = 484,
                    Concepto = "Deposito",
                    geoCountryCode = 0,
                    geoCountryName = String.Empty,
                    geoRegion = String.Empty,
                    geoCity = String.Empty,
                    geoPostalCode = String.Empty,
                    fechaRegistro = DateTime.Now
                };

                strSQL = string.Format("SELECT * FROM opCards WHERE DeleteDate IS NULL AND ExternalID = {0} AND CardID = '{1}'", usuario.tarjetaID, source_id);
                IEnumerable<openpayCard> opCard = db.Database.SqlQuery<openpayCard>(strSQL);

                if (opCard.Count() > 0)
                {
                    strSQL = string.Format("SELECT * FROM opCustomers WHERE DateDelete IS NULL AND ExternalID = {0}", usuario.tarjetaID);
                    IEnumerable<openpayCustomer> opCustomer = db.Database.SqlQuery<openpayCustomer>(strSQL);

                    if (opCustomer.Count() > 0)
                    {
                        OpenpayAPI api = new OpenpayAPI(OPENPAY_PRIVATE_KEY, OPENPAY_ID, bModoProduccion);

                        ChargeRequest request = new ChargeRequest();
                        request.Method = OpenpayAPI.RequestMethod.card;
                        request.SourceId = opCard.FirstOrDefault().CardID;
                        request.Amount = Convert.ToDecimal(amount) + GetFeeForCharge(Convert.ToDecimal(amount), opCard.FirstOrDefault().CardID);
                        request.Currency = "MXN";
                        request.Description = "Recarga de Monedero Enpadi";
                        request.DeviceSessionId = deviceSessionId;
                        request.OrderId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);

                        Openpay.Entities.Charge charge = api.ChargeService.Create(opCustomer.FirstOrDefault().CustomerID, request);

                        if (charge.Status == "completed")
                        {
                            t.estatusID = 1;
                            t.tarjetaID = usuario.tarjetaID;
                            t.tarjeta = usuario.tcNum.Substring(usuario.tcNum.Length - 4);
                            t.transaccion = charge.OrderId;
                            t.descripcion = charge.Description;
                            t.referencia = charge.Id;
                            t.aprobacion = charge.Authorization;
                            t.monto = charge.Amount - GetFeeForCharge(Convert.ToDecimal(amount), opCard.FirstOrDefault().CardID);
                            t.abono1 = charge.Amount - GetFeeForCharge(Convert.ToDecimal(amount), opCard.FirstOrDefault().CardID);
                            t.abonoTotal = charge.Amount - GetFeeForCharge(Convert.ToDecimal(amount), opCard.FirstOrDefault().CardID);

                            //decimal dIva = (decimal)0.16;
                            string strBrand = string.Empty;
                            if (!string.IsNullOrEmpty(opCard.FirstOrDefault().Brand))
                                strBrand = opCard.FirstOrDefault().Brand.ToLower();

                            if (strBrand == "visa" || strBrand == "mastercard")
                            {
                                t.socioID = socio.SocioID.Visa;
                            }
                            else
                            {
                                t.socioID = socio.SocioID.Amex;
                            }

                            t.ip = "189.25.169.74";

                            db.transacciones.Add(t);

                            logTC envio = new logTC() { idTarjeta = usuario.tarjetaID, result = 1 };
                            db.logTCs.Add(envio);

                            db.SaveChanges();

                            TransactionFee transfee = new TransactionFee();

                            Enpadi_WebUI.Models.Fee fee = new Models.Fee();

                            if (opCard.FirstOrDefault().Brand == "visa")
                            {
                                fee = db.Fee.FirstOrDefault(x => x.Id == Models.Fee.Keys.VisaFeeId);
                            }
                            else if (opCard.FirstOrDefault().Brand == "mastercard")
                            {
                                fee = db.Fee.FirstOrDefault(x => x.Id == Models.Fee.Keys.masterCardFeeId);
                            }
                            else
                            {
                                fee = db.Fee.FirstOrDefault(x => x.Id == Models.Fee.Keys.AmexFeeId);
                            }

                            transfee.Id = Guid.NewGuid();
                            transfee.TransaccionesId = t.Id;
                            transfee.FeeId = fee.Id;
                            transfee.FixAmount = fee.FixFee;
                            transfee.VariableAmount = fee.VariableFee * Decimal.Parse(amount);
                            transfee.ChargedByPartnerAmount = 0;
                            transfee.PaidOut = false;
                            transfee.PaidOutAt = null;
                            transfee.AmountPaid = null;

                            db.TransactionFee.Add(transfee);
                            db.Entry(transfee).State = System.Data.Entity.EntityState.Added;

                            db.SaveChanges();

                            return RedirectToAction("CreditCardResponse", "website", new { estatus = "APROBADA", autorizacion = string.Format("AUTORIZACIÓN  {0}", t.aprobacion), total = string.Format("MONTO {0} MXN", t.monto.ToString("C")), Comision = GetFeeForCharge(Convert.ToDecimal(amount), opCard.FirstOrDefault().CardID) });
                        }
                        else
                        {
                            logTC envio = new logTC() { idTarjeta = usuario.tarjetaID, result = 0 };
                            db.logTCs.Add(envio);

                            db.SaveChanges();
                            return RedirectToAction("CreditCardResponse", "website", new { estatus = "DECLINADA", autorizacion = charge.Description, total = "" });
                        }

                    }
                    else
                    {
                        return RedirectToAction("CreditCardResponse", "website", new { estatus = "DECLINADA", autorizacion = "No se ha encontrado el Usuario Seleccionado" });
                    }
                }
                else
                {
                    return RedirectToAction("CreditCardResponse", "website", new { estatus = "DECLINADA", autorizacion = "No se ha encontrado la Tarjeta Seleccionada", total = "" });
                }
            }
            else
            {
                return View();
            }
        }

        public ActionResult CreditCardResponse(string estatus, string autorizacion, string total, Decimal comision, string skin)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            respuestaTC r = new respuestaTC() { Estatus = estatus, Autorizacion = autorizacion, Total = total, Comision = comision.ToString("C") };
            return View(r);
        }

        public ActionResult Recharge()
        {
            return View();
        }

        public ActionResult Buy()
        {
            return View();
        }

        public ActionResult Transfer()
        {
            return View();
        }

        public ActionResult Pay()
        {
            return View();
        }

        public ActionResult Rewards()
        {
            return View();
        }

        public ActionResult serviciosdeimpresion(string transid)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            Guid _transid = Guid.Parse(transid);

            pagoServicio p = db.pagoServicios.Where(x => x.transaccionesId == _transid).FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();

            if (p == null)
            {
                p = new pagoServicio();
                p.transaccionesId = Guid.Parse(transid);
                p = Tools.ServiciosImpresion(p, currentUserId, true);
            }

            p = Tools.ServiciosImpresion(p, currentUserId);
            return View("ServiciosImpresion", p);
        }

        public ActionResult ServiciosImpresion(Guid Id)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            pagoServicio p = db.pagoServicios.Where(x => x.Id == Id).FirstOrDefault();
            string currentUserId = User.Identity.GetUserId();

            if (p == null)
            {
                p = new pagoServicio();
                p.transaccionesId = Id;
                p = Tools.ServiciosImpresion(p, currentUserId, true);
            }

            p = Tools.ServiciosImpresion(p, currentUserId);
            return View(p);
        }

        [HttpPost]
        public ActionResult ServiciosImpresion(pagoServicio oPago, string skin)
        {
            pagoServicio result = new pagoServicio();
            string currentUserId = User.Identity.GetUserId();
            result = Tools.ServiciosImpresion(oPago, currentUserId);
            return View(result);
        }

        public ActionResult ServiciosImpresionInDlls(Guid Id)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            pagoServicio p = db.pagoServicios.Where(x => x.Id == Id).FirstOrDefault();
            string currentUserId = User.Identity.GetUserId();

            if (p == null)
            {
                p = new pagoServicio();
                p.transaccionesId = Id;
                p = Tools.ServiciosImpresion(p, currentUserId, true);
            }

            p = Tools.ServiciosImpresion(p, currentUserId);
            return View(p);
        }

        [HttpPost]
        public ActionResult ServiciosImpresionInDlls(pagoServicio oPago, string skin)
        {
            pagoServicio result = new pagoServicio();
            string currentUserId = User.Identity.GetUserId();
            result = Tools.ServiciosImpresion(oPago, currentUserId);
            return View(result);
        }

        #region services

        #region check services

        public ActionResult ProcessPay(string tipo)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            if (tipo == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            List<servicios> s = new List<servicios>();

            if (Coffee.GetPlatformFromRegion() == PlatformModel.Keys.WebUsa)
            {
                List<servicios> services = (db.servicios).Where(x => x.Tipo == tipo).ToList();

                foreach (servicios service in services)
                {
                    if (service.DoesAmountApply == true)
                    {
                        if (service.Amount >= 100.00M)
                        {
                            s.Add(service);
                        }
                    }
                    else
                    {
                        s.Add(service);
                    }
                }
            }
            else if (Coffee.GetPlatformFromRegion() == PlatformModel.Keys.Enpadi)
            {
                s = db.servicios.Where(x => x.Tipo == tipo).Where(x => x.Activo == true).Where(x => x.FechaBaja == null).ToList();
            }
            else
            {
                s = db.servicios.Where(x => x.Tipo == tipo).Where(x => x.Activo == true).Where(x => x.FechaBaja == null).ToList();
            }

            return View(s.OrderBy(x => x.SKU));

        }

        public ActionResult CheckService(string sku, string referencia)
        {
            object model = null;
            string currentUserId = User.Identity.GetUserId();
            Guid _currentUserId = Guid.Parse(currentUserId);

            try
            {
                //imss
                if (sku == servicios.keys.imss_new || sku == servicios.keys.imss_existing)
                {
                    model = Tools.CheckService_imss(sku, referencia, currentUserId, PlatformModel.Keys.WebUsa, 0);

                    return View("ProcessIMSS", model);
                }
                //cemex
                else if (
                  sku == servicios.keys.cemex_100 ||
                  sku == servicios.keys.cemex_200 ||
                  sku == servicios.keys.cemex_300 ||
                  sku == servicios.keys.cemex_400 ||
                  sku == servicios.keys.cemex_10 ||
                  sku == servicios.keys.cemex_5 ||
                  sku == servicios.keys.cemex_500)
                {
                    model = Tools.CheckService_cemex(sku, referencia, currentUserId, PlatformModel.Keys.WebUsa, 0);

                    return View("ProcessCemex", model);
                }
                //todos los demas
                else
                {
                    model = Tools.CheckService_diestel(sku, referencia, currentUserId, PlatformModel.Keys.WebUsa, 0);

                    return View("ProcessCheckService", model);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return View("ProcessCheckService", model);
            }
        }

        #endregion check services

        #region payment of services

        #region imss

        public ActionResult ProcessIMSSPayment(
            Guid? Id
          )
        {
            if (Id == null)
            {
                return RedirectToActionPermanent("UserIndex");
            }
            ApplicationDbContext db = new ApplicationDbContext();
            pagoServicio result = new pagoServicio();
            pagoServicio oPago = db.pagoServicios.Where(x => x.Id == Id).FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            result = Tools.ServiciosImpresion(oPago, currentUserId);

            return View("ProcessServiceInDlls", result);
        }

        [HttpPost]
        public ActionResult ProcessIMSSPayment(
            Guid Id,
            string opagoId,
            string sku,
            string name,
            string namelast,
            string namemothers,
            string imss_number,
            string phone_number,
            string cell_phone_number,
            string email,
            string buyer_name,
            string buyer_phone_number,
            string buyer_email
            )
        {
            ApplicationDbContext db = new ApplicationDbContext();
            pagoServicio oPago = db.pagoServicios.Where(x => x.Id == Id).FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            Guid _currentUserId = Guid.Parse(currentUserId);

            oPago = Tools.PayServiceIMSS(
                _currentUserId,
                sku,
                name,
                namelast,
                namemothers,
                imss_number,
                phone_number,
                cell_phone_number,
                email,
                buyer_name,
                buyer_phone_number,
                buyer_email,
                false,
                PlatformModel.Keys.WebUsa,
                oPago);

            return View("ProcessServiceInDlls", oPago);
        }

        #endregion imss

        #region cemex

        public ActionResult ProcessCemex()
        {
            return View(new Enpadi_WebUI.Models.ViewModelProcessCemex());
        }

        public ActionResult ProcessCemexPayment(
           Guid? Id
         )
        {
            if (Id == null)
            {
                return RedirectToActionPermanent("UserIndex");
            }
            ApplicationDbContext db = new ApplicationDbContext();
            pagoServicio result = new pagoServicio();
            pagoServicio oPago = db.pagoServicios.Where(x => x.Id == Id).FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            result = Tools.ServiciosImpresion(oPago, currentUserId);

            return View("ProcessServiceInDlls", result);
        }

        [HttpPost]
        public ActionResult ProcessCemexPayment(
            Guid Id,
            string sku,
            string name,
            string namelast,
            string namemothers,
            string imss_number,
            string phone_number,
            string cell_phone_number,
            string email,
            string buyer_name,
            string buyer_phone_number,
            string buyer_email)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            pagoServicio oPago = db.pagoServicios.Where(x => x.Id == Id).FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();

            oPago = Tools.PayServiceCemex(
                currentUserId,
                sku,
                name,
                namelast,
                namemothers,
                imss_number,
                phone_number,
                cell_phone_number,
                email,
                buyer_name,
                buyer_phone_number,
                buyer_email,
                false,
                PlatformModel.Keys.WebUsa,
                oPago);

            return View("ProcessServiceInDlls", oPago);
        }

        #endregion cemex

        #region services with diestel

        public ActionResult ProcessService(Guid? Id)
        {
            if (Id == null)
            {
                return RedirectToActionPermanent("UserIndex");
            }

            ApplicationDbContext db = new ApplicationDbContext();

            pagoServicio result = new pagoServicio();
            pagoServicio oPago = db.pagoServicios.Where(x => x.Id == Id).FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            result = Tools.ServiciosImpresion(oPago, currentUserId);

            return View(result);
        }

        [HttpPost]
        public ActionResult ProcessService(pagoServicio oPago)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            string currentUserId = User.Identity.GetUserId();
            Guid _currentUserId = Guid.Parse(currentUserId);
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
            servicios servicio = db.servicios.FirstOrDefault(x => x.SKU == oPago.sku);
            List<Enpadi_WebUI.Models.Fee> fees = Enpadi_WebUI.Models.Fee.GetFeeForService(ApplicationUser.Key.ConsulateId, servicio.Id, PlatformModel.Keys.WebUsa);
            //pagoServicio response = new pagoServicio();

            oPago = Tools.PayServiceWithDiestel(oPago, currentUserId, PlatformModel.Keys.WebUsa, false, fees, false);

            return View(oPago);

        }

        #endregion services with diestel

        #endregion payment of services

        #endregion services

        public ActionResult UserIndex(int? periodo = 0)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            periodo = periodo * -1;

            if (User.Identity.GetUserId() == null)
            {
                return RedirectToAction("Index");
            }

            ViewModelInicio vm = new ViewModelInicio();
            string strSQL = string.Empty;

            vm.periodo = (int)periodo;

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            if (usuario == null)
            {
                return RedirectToAction("LogOff", "website");
            }
            int tID = usuario.tarjetaID;

            if (User.IsInRole(SecurityRoles.Concentradora))
                vm.perfil = SecurityRoles.Concentradora;
            else
                vm.perfil = SecurityRoles.Usuario;

            vm = Tools.LoadChards(vm, periodo.GetValueOrDefault(), usuario);

            return View(vm);
        }

        // GET: PanelUsuarios
        public ActionResult RechargeCredit()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            string strSQL = String.Empty;

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            strSQL = string.Format("SELECT COUNT(*) AS total FROM opCustomers WHERE ExternalID = {0}", usuario.tarjetaID);
            IEnumerable<contador> custAddress = db.Database.SqlQuery<contador>(strSQL);
            int iCustAddress = custAddress.ToList().FirstOrDefault().total;

            strSQL = string.Format("SELECT COUNT(*) AS total FROM opCards WHERE ExternalID = {0}", usuario.tarjetaID);
            IEnumerable<contador> custCard = db.Database.SqlQuery<contador>(strSQL);
            int iCustCard = custCard.ToList().FirstOrDefault().total;

            if (iCustAddress <= 0)
                return RedirectToAction("AddCustomer");
            if (iCustCard <= 0)
                return RedirectToAction("AddCard");

            return RedirectToAction("AddCharge");
        }

        //TODO check this method, i thinks it has absolutly no use, Second thought, i think is the 20 buck to confirm creditcard
        [HttpPost]
        public ActionResult RechargeCredit(string token_id, string holder_name, string amount, string deviceSessionId)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            string strSQL = String.Empty;

            if (!ModelState.IsValid)
            {
                return View();
            }
            else
            {
                string currentUserId = User.Identity.GetUserId();
                tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
                trans t = new trans { estatusID = 0, origenID = 1, tipoID = 1, modoID = 1, socioID = 2, terminalID = 1, monedaID = 484, Concepto = "TC", geoCountryCode = 0, geoCountryName = String.Empty, geoRegion = String.Empty, geoCity = String.Empty, geoPostalCode = String.Empty };

                strSQL = string.Format("SELECT COUNT(*) As total FROM logTC WHERE idTarjeta = {0} AND fechaAlta >= DATEADD(DAY, -7, getdate())", usuario.tarjetaID);
                IEnumerable<contador> intentos = db.Database.SqlQuery<contador>(strSQL);
                int iIntentos = intentos.ToList().FirstOrDefault().total;

                strSQL = string.Format("SELECT COUNT(*) As total FROM logTC WHERE idTarjeta = {0} AND result = 1 AND fechaAlta >= DATEADD(DAY, -7, getdate())", usuario.tarjetaID);
                IEnumerable<contador> exitosos = db.Database.SqlQuery<contador>(strSQL);
                int iExitosos = exitosos.ToList().FirstOrDefault().total;

                if (iIntentos < 5 && iExitosos < 2)
                {
                    OpenpayAPI api = new OpenpayAPI(OPENPAY_PRIVATE_KEY, OPENPAY_ID);

                    Openpay.Entities.Customer cliente = new Openpay.Entities.Customer();
                    cliente.ExternalId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);
                    cliente.Name = holder_name;
                    cliente.Email = "amorales@truewisdom.co"; //TODO why was Marcos mail in here?
                    cliente.RequiresAccount = false;

                    ChargeRequest request = new ChargeRequest();
                    request.Method = OpenpayAPI.RequestMethod.card;
                    request.SourceId = token_id;
                    request.Amount = Convert.ToDecimal(amount);
                    request.Currency = "MXN";
                    request.Description = "Recarga de Monedero Enpadi";
                    request.DeviceSessionId = deviceSessionId;
                    request.Customer = cliente;
                    request.OrderId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);

                    Openpay.Entities.Charge charge = api.ChargeService.Create(request);

                    if (charge.Status == "completed")
                    {
                        t.estatusID = 1;
                        t.tarjetaID = usuario.tarjetaID;
                        t.tarjeta = usuario.tcNum.Substring(usuario.tcNum.Length - 4);
                        t.transaccion = charge.OrderId;
                        t.descripcion = charge.Description;
                        t.referencia = charge.Id;
                        t.aprobacion = charge.Authorization;
                        t.monto = charge.Amount;
                        t.abono1 = charge.Amount;
                        t.cargo1 = 0;
                        t.abono2 = 0;
                        t.cargo2 = 0;
                        t.abono3 = 0;
                        t.cargo3 = 0;
                        t.abonoTotal = charge.Amount;
                        t.cargoTotal = 0;
                        t.ip = "189.25.169.74";
                        trans.Save(t);
                        //db.transacciones.Add(t);

                        //recompensa r = new recompensa() { tarjetaID = usuario.tarjetaID, origenID = 1, socioID = 2, transaccion = charge.OrderId, concepto = "Recompensa Recarga", abono = charge.Amount * (decimal)0.01, cargo = 0 };
                        //db.recompensas.Add(r);

                        logTC envio = new logTC() { idTarjeta = usuario.tarjetaID, result = 1 };
                        db.logTCs.Add(envio);

                        db.SaveChanges();

                        return RedirectToAction("RespuestaTC", "PanelUsuarios", new { estatus = "APROBADA", autorizacion = string.Format("AUTORIZACIÓN  {0}", t.aprobacion), total = string.Format("MONTO  ${0} MXN", t.monto) });
                    }
                    else
                    {
                        logTC envio = new logTC() { idTarjeta = usuario.tarjetaID, result = 0 };
                        db.logTCs.Add(envio);

                        db.SaveChanges();
                        return RedirectToAction("RespuestaTC", "PanelUsuarios", new { estatus = "DECLINADA", autorizacion = charge.Description, total = "" });
                    }
                }
                else
                {
                    return RedirectToAction("RespuestaTC", "PanelUsuarios", new { estatus = "DECLINADA", autorizacion = "Has excedido el Limite de Recargas Autorizado por Semana", total = "" });
                }
            }
        }

        // GET: PanelUsuarios
        public ActionResult RespuestaTC(string estatus, string autorizacion, string total)
        {

            respuestaTC r = new respuestaTC() { Estatus = estatus, Autorizacion = autorizacion, Total = total };
            return View(r);
        }

        #endregion views user inner system

        #region enpadi contact

        public ActionResult EnpadiFriend()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            string strSQL = String.Empty;

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            strSQL = string.Format("SELECT * FROM TarjetasContactos WHERE tarjetaID = {0}", usuario.tarjetaID);
            IEnumerable<tarjetasContactos> tarjetasContactos = db.Database.SqlQuery<tarjetasContactos>(strSQL);
            return View(tarjetasContactos);

        }

        // GET: tarjetasContactos/Details/5
        public ActionResult EnpadiFriendDetail(int? id)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Empty;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tarjetasContactos tarjetasContactos = db.tarjetasContactos.Find(id);
            if (tarjetasContactos == null)
            {
                return HttpNotFound();
            }
            return View(tarjetasContactos);
        }

        // GET: tarjetasContactos/Create
        public ActionResult EnpadiFriendCreate()
        {
            return View();
        }

        // POST: tarjetasContactos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EnpadiFriendCreate([Bind(Include = "contactoID,tarjetaID,contactoTarjeta,contactoNumero,contactoAlias")] tarjetasContactos tarjetasContactos)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Empty;

            if (ModelState.IsValid)
            {
                string currentUserId = User.Identity.GetUserId();
                tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
                tarjetasContactos.tarjetaID = usuario.tarjetaID;

                strSQL = string.Format("SELECT Count(*) As total FROM Tarjetas WHERE tcNum = '{0}' AND tcAsignada = 1 AND tcActiva = 1 AND fechaBaja IS NULL", tarjetasContactos.contactoTarjeta);
                IEnumerable<contador> tarjetaExiste = db.Database.SqlQuery<contador>(strSQL);
                int iExiste = tarjetaExiste.ToList().FirstOrDefault().total;

                strSQL = string.Format("SELECT Count(*) As total FROM TarjetasContactos WHERE tarjetaID = '{0}' AND contactoTarjeta = {1} AND fechaBaja IS NULL", tarjetasContactos.tarjetaID, tarjetasContactos.contactoTarjeta);
                IEnumerable<contador> alreadyAdded = db.Database.SqlQuery<contador>(strSQL);
                int aAdded = alreadyAdded.ToList().FirstOrDefault().total;

                if (iExiste > 0)
                {
                    if (aAdded <= 0)
                    {
                        strSQL = string.Format("SELECT tarjetaID As total FROM Tarjetas WHERE tcNum = '{0}' AND tcAsignada = 1 AND tcActiva = 1 AND fechaBaja IS NULL", tarjetasContactos.contactoTarjeta);
                        IEnumerable<contador> contacto = db.Database.SqlQuery<contador>(strSQL);
                        tarjetasContactos.contactoNumero = contacto.ToList().FirstOrDefault().total;

                        if (tarjetasContactos.tarjetaID == tarjetasContactos.contactoNumero)
                        {
                            ModelState.AddModelError("", "<label class='lang' key='enpadifriendcreate.errorownaccount'></label>");
                            return View(tarjetasContactos);
                        }
                        else
                        {
                            db.tarjetasContactos.Add(tarjetasContactos);
                            db.SaveChanges();
                            return RedirectToAction("EnpadiFriend");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "<label class='lang' key='enpadifriendcreate.errorbadaccount'></label>");
                        return View(tarjetasContactos);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "<label class='lang' key='enpadifriendcreate.wrongaccountnumber'></label>");
                    return View(tarjetasContactos);
                }
            }
            return View(tarjetasContactos);
        }

        // GET: tarjetasContactos/Edit/5
        public ActionResult EnpadiFriendEdit(int? id)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Empty;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tarjetasContactos tarjetasContactos = db.tarjetasContactos.Find(id);
            if (tarjetasContactos == null)
            {
                return HttpNotFound();
            }
            return View(tarjetasContactos);
        }

        // POST: tarjetasContactos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EnpadiFriendEdit([Bind(Include = "contactoID,tarjetaID,contactoTarjeta,contactoNumero,contactoAlias")] tarjetasContactos tarjetasContactos)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Empty;

            if (ModelState.IsValid)
            {
                db.Entry(tarjetasContactos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("EnpadiFriend");
            }
            return View(tarjetasContactos);
        }

        // GET: tarjetasContactos/Delete/5
        public ActionResult EnpadiFriendDelete(int? id)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Empty;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tarjetasContactos tarjetasContactos = db.tarjetasContactos.Find(id);
            if (tarjetasContactos == null)
            {
                return HttpNotFound();
            }
            return View(tarjetasContactos);
        }

        // POST: tarjetasContactos/Delete/5
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Empty;

            tarjetasContactos tarjetasContactos = db.tarjetasContactos.Find(id);
            db.tarjetasContactos.Remove(tarjetasContactos);
            db.SaveChanges();
            return RedirectToAction("EnpadiFriend");
        }

        public ActionResult EnpadiFriendTransfer()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            string strSQL = String.Empty;

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            strSQL = string.Format("SELECT * FROM TarjetasContactos WHERE tarjetaID = {0}", usuario.tarjetaID);
            IEnumerable<tarjetasContactos> tarjetasContactos = db.Database.SqlQuery<tarjetasContactos>(strSQL);
            return View(tarjetasContactos);
        }

        [HttpPost]
        public ActionResult EnpadiFriendTransfer(int contacto, string monto)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            string strSQL = String.Empty;

            decimal dMonto = 0;
            bool dConvert = decimal.TryParse(monto, out dMonto);

            if (dMonto <= 0)
                dMonto = 0;

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
            string sTarjetaEnvia = usuario.tcNum.Substring(usuario.tcNum.Length - 4);

            tarjetas amigo = db.tarjetas.FirstOrDefault(x => x.tarjetaID == contacto);
            string sTarjetaRecibe = amigo.tcNum.Substring(amigo.tcNum.Length - 4);

            if (dConvert)
            {
                strSQL = String.Format("SELECT ISNULL(SUM(abonoTotal-cargoTotal),0) As Total FROM Transacciones WHERE tarjetaID = {0} AND tipoID IN (1,2)", usuario.tarjetaID);
                IEnumerable<total> saldoActual = db.Database.SqlQuery<total>(strSQL);

                if (saldoActual.ToList().FirstOrDefault().Total >= dMonto)
                {
                    string sTransaccion = DateTime.Now.ToString("yyyyMMddHHmmss");
                    string sAprobacion = "329875";

                    trans cargo = new trans()
                    {
                        Id = Guid.NewGuid(),
                        estatusID = 1,
                        origenID = 2,
                        tipoID = 2,
                        modoID = 1,
                        socioID = 0,
                        terminalID = 0,
                        monedaID = 484,
                        tarjetaID = usuario.tarjetaID,
                        tarjeta = sTarjetaRecibe,
                        transaccion = sTransaccion,
                        monto = dMonto,
                        abono1 = 0,
                        cargo1 = 0,
                        abono2 = 0,
                        cargo2 = 0,
                        abono3 = 0,
                        cargo3 = 0,
                        abonoTotal = 0,
                        cargoTotal = dMonto,
                        Concepto = "Transferencia",
                        descripcion = "Transferencia entre Cuentas",
                        referencia = amigo.tcNum,
                        aprobacion = sAprobacion,
                        ip = "189.25.169.74",
                        geoCountryCode = 0,
                        geoCountryName = string.Empty,
                        geoRegion = string.Empty,
                        geoCity = string.Empty,
                        geoPostalCode = string.Empty,
                        fechaRegistro = DateTime.Now
                    };

                    db.transacciones.Add(cargo);
                    db.Entry(cargo).State = System.Data.Entity.EntityState.Added;

                    trans abono = new trans()
                    {
                        Id = Guid.NewGuid(),
                        estatusID = 1,
                        origenID = 2,
                        tipoID = 1,
                        modoID = 1,
                        socioID = 0,
                        terminalID = 0,
                        monedaID = 484,
                        tarjetaID = contacto,
                        tarjeta = sTarjetaEnvia,
                        transaccion = sTransaccion,
                        monto = dMonto,
                        abono1 = 0,
                        cargo1 = 0,
                        abono2 = 0,
                        cargo2 = 0,
                        abono3 = 0,
                        cargo3 = 0,
                        abonoTotal = dMonto,
                        cargoTotal = 0,
                        Concepto = "Transferencia",
                        descripcion = "Transferencia entre Cuentas",
                        referencia = usuario.tcNum,
                        aprobacion = sAprobacion,
                        ip = "189.25.169.74",
                        geoCountryCode = 0,
                        geoCountryName = string.Empty,
                        geoRegion = string.Empty,
                        geoCity = string.Empty,
                        geoPostalCode = string.Empty,
                        fechaRegistro = DateTime.Now
                    };

                    db.transacciones.Add(abono);
                    db.Entry(abono).State = System.Data.Entity.EntityState.Added;

                    db.SaveChanges();

                    // Realiza el calculo de Comision a Aplicar
                    //strSQL = string.Format("SELECT ISNULL((SELECT TOP 1 socioID As total FROM Transacciones WHERE fechaBaja IS NULL AND socioID > 0 AND tipoID = 1 AND tarjetaID = {0} ORDER BY fechaRegistro DESC),0) As total", usuario.tarjetaID);
                    //IEnumerable<contador> ultimoDeposito = db.Database.SqlQuery<contador>(strSQL);
                    //int iSocio = ultimoDeposito.ToList().FirstOrDefault().total;

                    //decimal dIva = (decimal)0.16;

                    //switch (iSocio)
                    //{
                    //    case 1: //Visa-MasterCard
                    //        cargo.cargo3 = dMonto * (decimal)0.029;
                    //        cargo.cargo3iva = cargo.cargo3 * dIva;
                    //        cargo.cargo4 = (decimal)2.50;
                    //        cargo.cargo4iva = cargo.cargo4 * dIva;
                    //        cargo.cargoTotal += cargo.cargo3 + cargo.cargo3iva + cargo.cargo4 + cargo.cargo4iva;
                    //        break;
                    //    case 2: //AmericanExpress
                    //        cargo.cargo3 = dMonto * (decimal)0.049;
                    //        cargo.cargo3iva = cargo.cargo3 * dIva;
                    //        cargo.cargo4 = (decimal)2.50;
                    //        cargo.cargo4iva = cargo.cargo4 * dIva;
                    //        cargo.cargoTotal += cargo.cargo3 + cargo.cargo3iva + cargo.cargo4 + cargo.cargo4iva;
                    //        break;
                    //    case 3: //Banco
                    //        cargo.cargo4 = (decimal)8.00;
                    //        cargo.cargo4iva = cargo.cargo4 * dIva;
                    //        cargo.cargoTotal += cargo.cargo4 + cargo.cargo4iva;
                    //        break;
                    //    case 4: //Tienda
                    //        cargo.cargo3 = dMonto * (decimal)0.029;
                    //        cargo.cargo3iva = cargo.cargo3 * dIva;
                    //        cargo.cargo4 = (decimal)2.50;
                    //        cargo.cargo4iva = cargo.cargo4 * dIva;
                    //        cargo.cargoTotal += cargo.cargo3 + cargo.cargo3iva + cargo.cargo4 + cargo.cargo4iva;
                    //        break;
                    //}

                    //trans.Save(cargo);
                    //trans.Save(abono);
                    //db.transacciones.Add(cargo);
                    //db.transacciones.Add(abono);
                    db.SaveChanges();
                    //Procedimiento Valido
                    return RedirectToAction("UserIndex", "website");
                }
                else
                    ModelState.AddModelError("", "Saldo Insuficiente");
            }
            else
                ModelState.AddModelError("", "Proporcione el Monto a Transferir");

            strSQL = string.Format("SELECT * FROM TarjetasContactos WHERE tarjetaID = {0}", usuario.tarjetaID);
            IEnumerable<tarjetasContactos> tarjetasContactos = db.Database.SqlQuery<tarjetasContactos>(strSQL);
            return View(tarjetasContactos);
        }

        #endregion enpadi contact

        #region methods

        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "website");
            //RedirectPermanent("~/website/index");
        }

        private String GetProfilePicture()
        {
            string result = String.Empty;
            string currentUserId = User.Identity.GetUserId();
            var user = UserManager.FindById(currentUserId);

            string path = System.IO.Path.Combine(
                                           Server.MapPath("~/UpLoads/ProfilePictures/"), currentUserId + ".jpg");

            if (System.IO.File.Exists(path))
            {
                AspNetUsersModel tempUser = AspNetUsersModel.Get(Guid.Parse(currentUserId), user.Email);
                result = tempUser.ProfilePicUrl;
            }
            else
            {
                result = "Images/account_no_image.png";
            }

            return result;
        }

        [HttpPost]
        public ActionResult ProfilePictureUpload(HttpPostedFileBase file, String Email, String Name, String PhoneNumber)
        {
            string currentUserId = User.Identity.GetUserId();
            var user = UserManager.FindById(currentUserId);

            if (file != null)
            {
                if (file.ContentLength < 2048000)
                {
                    if (Path.GetExtension(file.FileName).ToString() != ".jpg")
                    {
                        ViewBag.ErrorMessage = "Solo se permiten imagenes de formato '.jpg', favor de subir una imagen con el formato correcto.";
                        return View("Profile", new ViewModelProfile
                        {
                            Name = Name,
                            Email = Email,
                            PhoneNumber = PhoneNumber,
                            currentUserId = currentUserId,
                            ProfilePicUrl = GetProfilePicture()
                        });

                    }

                    string localPath = "UpLoads/ProfilePictures/" + currentUserId + ".jpg";

                    string path = System.IO.Path.Combine(
                                           Server.MapPath("~/UpLoads/ProfilePictures"), currentUserId + ".jpg");

                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }

                    // file is uploaded
                    file.SaveAs(path);

                    AspNetUsersModel tempUser = AspNetUsersModel.Get(Guid.Parse(currentUserId), user.Email);
                    tempUser.ProfilePicUrl = localPath;
                    AspNetUsersModel.Save(tempUser);

                    // save the image path path to the database or you can send image 
                    // directly to database
                    // in-case if you want to store byte[] ie. for DB
                    //using (MemoryStream ms = new MemoryStream())
                    //{
                    //    file.InputStream.CopyTo(ms);
                    //    byte[] array = ms.GetBuffer();
                    //}
                }
                else
                {
                    ViewBag.ErrorMessage = "Tamaño de imagen exedido, seleccionar una imagen menor de 2MB.";
                    return View("Profile", new ViewModelProfile
                    {
                        Name = Name,
                        Email = Email,
                        PhoneNumber = PhoneNumber,
                        currentUserId = currentUserId,
                        ProfilePicUrl = GetProfilePicture()
                    });
                }
            }
            // after successfully uploading redirect the user
            return RedirectToAction("Profile");
        }

        public async Task<ActionResult> UpdateProfile(String Email, String Name, String Password, string currentPassword, String PhoneNumber)
        {
            var contextDB = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
            var storeDB = new UserStore<IdentityUser>(contextDB);
            var managerDB = new UserManager<IdentityUser>(storeDB);
            var userDB = managerDB.Users.Where(u => u.PhoneNumber == PhoneNumber).FirstOrDefault();
            ApplicationDbContext db = new ApplicationDbContext();

            if (userDB != null)
            {
                string currentUserId = User.Identity.GetUserId();
                tarjetas enpadiCard = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

                if (userDB.PhoneNumber == PhoneNumber)
                {
                    await ChangeUserProfileData(Email, Name, Password, currentPassword, PhoneNumber);

                    ViewBag.SuccessMessage = "Los datos se han actualizado.";
                }
                else
                {
                    ViewBag.ErrorMessage = "Ya existe una cuenta registrada con el mismo Número Movil.";
                }
            }
            else
            {

                await ChangeUserProfileData(Email, Name, Password, currentPassword, PhoneNumber);

                ViewBag.SuccessMessage = "Los datos se han actualizado.";
            }

            return View("Profile", new ViewModelProfile
            {
                Name = Name,
                Email = Email,
                PhoneNumber = PhoneNumber,
                ProfilePicUrl = GetProfilePicture()
            });
        }

        private async Task<Boolean> ChangeUserProfileData(String Email, String Name, String Password, string currentPassword, String PhoneNumber)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            try
            {
                string currentUserId = User.Identity.GetUserId();
                tarjetas enpadiCard = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

                enpadiCard.datNombre = Name;
                //enpadiCard.email = Email;
                enpadiCard.datTelefono = PhoneNumber;

                db.tarjetas.Add(enpadiCard);
                db.Entry(enpadiCard).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                //EnpadiCardModel.Save(enpadiCard);

                var user = await UserManager.FindByIdAsync(currentUserId);
                user.Name = Name;
                //user.Email = Email;
                user.PhoneNumber = PhoneNumber;

                await UserManager.UpdateAsync(user);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public ActionResult UpdatePassword(String Password, string currentPassword)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            string currentUserId = User.Identity.GetUserId();
            tarjetas enpadiCard = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            var result = UserManager.ChangePassword(currentUserId, currentPassword, Password);

            if (!result.Succeeded)
            {
                ViewBag.ErrorMessage = "La contraseña actual esta incorrecta.";
            }
            else
            {
                ViewBag.SuccessMessage = "Se cambio la contraseña exitosamente.";
            }
            return View("Profile", new ViewModelProfile
            {
                Name = enpadiCard.datNombre,
                Email = enpadiCard.email,
                PhoneNumber = enpadiCard.datTelefono,
                ProfilePicUrl = GetProfilePicture()
            });
        }

        public JsonResult LoadUser()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            var contextDB = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
            var storeDB = new UserStore<IdentityUser>(contextDB);
            var managerDB = new UserManager<IdentityUser>(storeDB);
            var userDB = managerDB.FindByEmail(usuario.email);

            Decimal accountBalance = Tools.GetUserBalance(currentUserId);

            KioskoIndexViewModel model = new KioskoIndexViewModel
            {
                enpadiCard = EnpadiCardModel.Get(usuario.tarjetaID),
                user = AspNetUsersModel.Get(null, usuario.email),
                AccountBalance = accountBalance
            };

            model.user.ProfilePicUrl = GetProfilePicture();

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadBalance(int? periodo = 0)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            periodo = periodo * -1;

            string strSQL = String.Empty;
            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
            int tID = usuario.tarjetaID;

            strSQL = " SELECT numero, CONVERT(VARCHAR, Anio) + '  ' + CASE Mes WHEN 1 THEN 'ENERO' WHEN 2 THEN 'FEBRERO' WHEN 3 THEN 'MARZO' WHEN 4 THEN 'ABRIL' WHEN 5 THEN 'MAYO' WHEN 6 THEN 'JUNIO' WHEN 7 THEN 'JULIO' "
                      + " WHEN 8 THEN 'AGOSTO' WHEN 9 THEN 'SEPTIEMBRE' WHEN 10 THEN 'OCTUBRE' WHEN 11 THEN 'NOVIEMBRE' WHEN 12 THEN 'DICIEMBRE' END As Mes "
                      + " FROM ( "
                      + " SELECT 0 As numero, DATEPART(MONTH, DATEADD(MONTH, 0, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, 0, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -1 As numero, DATEPART(MONTH, DATEADD(MONTH, -1, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -1, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -2 As numero, DATEPART(MONTH, DATEADD(MONTH, -2, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -2, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -3 As numero, DATEPART(MONTH, DATEADD(MONTH, -3, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -3, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -4 As numero, DATEPART(MONTH, DATEADD(MONTH, -4, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -4, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -5 As numero, DATEPART(MONTH, DATEADD(MONTH, -5, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -5, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -6 As numero, DATEPART(MONTH, DATEADD(MONTH, -6, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -6, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -7 As numero, DATEPART(MONTH, DATEADD(MONTH, -7, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -7, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -8 As numero, DATEPART(MONTH, DATEADD(MONTH, -8, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -8, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -9 As numero, DATEPART(MONTH, DATEADD(MONTH, -9, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -9, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -10 As numero, DATEPART(MONTH, DATEADD(MONTH, -10, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -10, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -11 As numero, DATEPART(MONTH, DATEADD(MONTH, -11, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -11, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -12 As numero, DATEPART(MONTH, DATEADD(MONTH, -12, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -12, GETDATE())) As Anio "
                      + " ) As Meses "
                      + " ORDER BY numero DESC ";
            IEnumerable<periodo> meses = db.Database.SqlQuery<periodo>(strSQL);
            List<periodo> rmeses = meses.ToList();

            strSQL = " SELECT CASE DATEPART(MONTH, DATEADD(MONTH, " + periodo.ToString() + ", GetDate())) "
                   + " WHEN 1 THEN 'ENERO' WHEN 2 THEN 'FEBRERO' WHEN 3 THEN 'MARZO' WHEN 4 THEN 'ABRIL' "
                   + " WHEN 5 THEN 'MAYO' WHEN 6 THEN 'JUNIO' WHEN 7 THEN 'JULIO' WHEN 8 THEN 'AGOSTO' "
                   + " WHEN 9 THEN 'SEPTIEMBRE' WHEN 10 THEN 'OCTUBRE' WHEN 11 THEN 'NOVIEMBRE' WHEN 12 THEN 'DICIEMBRE' "
                   + " END + ' ' + CONVERT(varchar(10), DATEPART(YEAR, DATEADD(MONTH, " + periodo.ToString() + ", GetDate()))) AS [desc] ";
            IEnumerable<descripcion> descrip = db.Database.SqlQuery<descripcion>(strSQL);
            string periodo_descripcion = descrip.ToList().FirstOrDefault().desc;

            strSQL = " SELECT ISNULL(SUM(abonoTotal-cargoTotal), 0) As Total "
                   + " FROM Transacciones "
                   + " WHERE tarjetaID = " + tID;
            IEnumerable<total> saldoInicial = db.Database.SqlQuery<total>(strSQL);
            decimal rsaldoInicial = saldoInicial.ToList().FirstOrDefault().Total;

            strSQL = " SELECT ISNULL(SUM(abonoTotal), 0) As Total "
                   + " FROM Transacciones "
                   + " WHERE tarjetaID = " + tID + " AND tipoID = 1 AND DateDiff(MONTH, GetDate(), FechaRegistro) = " + periodo.ToString();
            IEnumerable<total> abonos = db.Database.SqlQuery<total>(strSQL);
            decimal rabonos = abonos.ToList().FirstOrDefault().Total;

            strSQL = " SELECT ISNULL(SUM(cargoTotal), 0) As Total "
                   + " FROM Transacciones "
                   + " WHERE tarjetaID = " + tID + " AND tipoID = 2 AND DateDiff(MONTH, GetDate(), FechaRegistro) = " + periodo.ToString();
            IEnumerable<total> cargos = db.Database.SqlQuery<total>(strSQL);
            decimal rcargos = cargos.ToList().FirstOrDefault().Total;

            rsaldoInicial = rsaldoInicial - rabonos;
            rsaldoInicial = rsaldoInicial + rcargos;

            //strSQL = " SELECT ISNULL(SUM(abonoTotal-cargoTotal),0) As Total "
            //       + " FROM Transacciones "
            //       + " WHERE tarjetaID = " + tID + " AND DateDiff(MONTH, GetDate(), FechaRegistro) <= " + periodo.ToString();
            //IEnumerable<total> saldoFinal = db.Database.SqlQuery<total>(strSQL);
            decimal rsaldoFinal = rsaldoInicial - rcargos;

            var result = new
            {
                periodo_descripcion,
                rsaldoInicial,
                rabonos,
                rcargos,
                rsaldoFinal,
                rmeses
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadPendingOrders()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            string currentUserId = User.Identity.GetUserId();
            Guid _currentUserId = Guid.Parse(currentUserId);

            List<EnpadiOrder> orders = db.EnpadiOrder.Where(x => x.customerId == _currentUserId).ToList();
            foreach (EnpadiOrder order in orders)
            {

                ApplicationUser supplier = db.Users.Where(x => x.Email == order.client_email).FirstOrDefault();
                if (supplier != null)
                {
                    order.supplier = supplier.Name;
                }

                order.Status = "Pending";
            }

            return Json(orders, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadGeneralReport(String periodo)
        {
            string currentUserId = User.Identity.GetUserId();
            Guid _currentUserId = Guid.Parse(currentUserId);

            return Json(Tools.GetDataForBasicReportWebsites(_currentUserId, periodo, 1), JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #endregion methods
    }
}