﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Enpadi_WebUI.Models;

namespace Enpadi_WebUI.Controllers
{
    public class santiagoPredialTarjetasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: santiagoPredialTarjetas
        public ActionResult Index(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View(db.santiagoPredialTarjetas.ToList());
        }

        // GET: santiagoPredialTarjetas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            santiagoPredialTarjetas santiagoPredialTarjetas = db.santiagoPredialTarjetas.Find(id);
            if (santiagoPredialTarjetas == null)
            {
                return HttpNotFound();
            }
            return View(santiagoPredialTarjetas);
        }

        // GET: santiagoPredialTarjetas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: santiagoPredialTarjetas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idMov,expediente,ubicacionPredio,saldo,titular,tarjeta,expiracion,fechaAlta")] santiagoPredialTarjetas santiagoPredialTarjetas)
        {
            if (ModelState.IsValid)
            {
                db.santiagoPredialTarjetas.Add(santiagoPredialTarjetas);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(santiagoPredialTarjetas);
        }

        // GET: santiagoPredialTarjetas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            santiagoPredialTarjetas santiagoPredialTarjetas = db.santiagoPredialTarjetas.Find(id);
            if (santiagoPredialTarjetas == null)
            {
                return HttpNotFound();
            }
            return View(santiagoPredialTarjetas);
        }

        // POST: santiagoPredialTarjetas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idMov,expediente,ubicacionPredio,saldo,titular,tarjeta,expiracion,fechaAlta")] santiagoPredialTarjetas santiagoPredialTarjetas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(santiagoPredialTarjetas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(santiagoPredialTarjetas);
        }

        // GET: santiagoPredialTarjetas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            santiagoPredialTarjetas santiagoPredialTarjetas = db.santiagoPredialTarjetas.Find(id);
            if (santiagoPredialTarjetas == null)
            {
                return HttpNotFound();
            }
            return View(santiagoPredialTarjetas);
        }

        // POST: santiagoPredialTarjetas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            santiagoPredialTarjetas santiagoPredialTarjetas = db.santiagoPredialTarjetas.Find(id);
            db.santiagoPredialTarjetas.Remove(santiagoPredialTarjetas);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
