﻿using Enpadi_WebUI.Models;
using Enpadi_WebUI.Properties;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Xml;
using System.Xml.Serialization;
using EnpadiSpecialAttributes;

namespace Enpadi_WebUI.Controllers
{
    [Security(ControllerToRedirect = "KioskoAdmin", ActionToRedirect = "login")]
    public class KioskoAdminController : Controller
    {

        #region constructors and inicializers

        private static bool bModoProduccion = Convert.ToBoolean(ConfigurationManager.AppSettings["ModoProduccion"]);

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        #region constructors
        public KioskoAdminController()
        {
        }

        public KioskoAdminController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }
        #endregion constructors


        #endregion constructors and inicializers 

        #region views
        // GET: KioskoAdmin
        public ActionResult Index()
        {
            ApplicationDbContext db = new ApplicationDbContext();

            String userId = User.Identity.GetUserId();
            if (userId == null)
            {
                return RedirectToAction("LogIn");
            }

            String skin = "clean";
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        // GET: WebUsaAdmin
        [AllowAnonymous]
        public ActionResult LogIn(String errorMessage = "")
        {
            ApplicationDbContext db = new ApplicationDbContext();

            String skin = "clean";
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();
            ViewModelWebUsaLogIn model = new ViewModelWebUsaLogIn
            {
                Email = String.Empty,
                Password = String.Empty
            };

            ViewBag.ErrorMessage = errorMessage;

            return View(model);
        }

        #region printing services

        public ActionResult ServiciosImpresion(Guid Id)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            pagoServicio p = db.pagoServicios.Where(x => x.Id == Id).FirstOrDefault();
            string currentUserId = User.Identity.GetUserId();

            if (p == null)
            {
                p = new pagoServicio();
                p.transaccionesId = Id;
                p = Tools.ServiciosImpresion(p, currentUserId, true);
            }

            p = Tools.ServiciosImpresion(p, currentUserId);
            return View(p);
        }

        [HttpPost]
        public ActionResult ServiciosImpresion(pagoServicio oPago, string skin)
        {
            pagoServicio result = new pagoServicio();
            string currentUserId = User.Identity.GetUserId();
            result = Tools.ServiciosImpresion(oPago, currentUserId);
            return View(result);
        }

        public ActionResult serviciosdeimpresion(string transid)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            Guid _transid = Guid.Parse(transid);

            pagoServicio p = db.pagoServicios.Where(x => x.transaccionesId == _transid).FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();

            if (p == null)
            {
                p = new pagoServicio();
                p.transaccionesId = Guid.Parse(transid);
                p = Tools.ServiciosImpresion(p, currentUserId, true);
            }
            else
            {
                p = Tools.ServiciosImpresion(p, currentUserId);
            }


            return View("ServiciosImpresion", p);
        }

        #endregion printing services
        #endregion views

        #region methods
        public ActionResult LoadUser()
        {
            ApplicationDbContext db = new ApplicationDbContext();

            String userId = User.Identity.GetUserId();
            if (userId == null)
            {
                return RedirectToActionPermanent("LogIn");
            }

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            var contextDB = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
            var storeDB = new UserStore<IdentityUser>(contextDB);
            var managerDB = new UserManager<IdentityUser>(storeDB);
            var userDB = managerDB.FindByEmail(usuario.email);

            Decimal accountBalance = trans.GetAccountStatus(usuario.tarjetaID);

            KioskoIndexViewModel model = new KioskoIndexViewModel
            {
                enpadiCard = EnpadiCardModel.Get(usuario.tarjetaID),
                user = AspNetUsersModel.Get(null, usuario.email),
                AccountBalance = accountBalance
            };

            model.user.ProfilePicUrl = GetProfilePicture();

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public async Task<ActionResult> LogInValidation(String email, String password)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            var result = await SignInManager.PasswordSignInAsync(email, password, false, shouldLockout: false);

            String skin = "clean";
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();
            ViewModelWebUsaLogIn model = new ViewModelWebUsaLogIn
            {
                Email = String.Empty,
                Password = String.Empty
            };

            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToActionPermanent("Index", "KioskoAdmin");
                case SignInStatus.LockedOut:
                    ViewBag.ErrorMessage = "Por seguridad el usuario se encuentra restringido.";
                    return View("LogIn", model);
                case SignInStatus.Failure:
                default:
                    ViewBag.ErrorMessage = "Mal usuario o contraseña.";
                    return View("LogIn", model);
            }

        }

        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("LogIn", "KioskoAdmin");
        }

        public JsonResult LoadGeneralReport(String periodo)
        {
            string currentUserId = User.Identity.GetUserId();
            Guid _currentUserId = Guid.Parse(currentUserId);

            return Json(Tools.GetDataForBasicReportWebsites(_currentUserId, periodo, 1), JsonRequestBehavior.AllowGet);
        }


        #endregion methods

        #region tools 

        private String GetProfilePicture()
        {
            string result = String.Empty;
            string currentUserId = User.Identity.GetUserId();
            var user = UserManager.FindById(currentUserId);

            string path = System.IO.Path.Combine(
                                           Server.MapPath("~/UpLoads/ProfilePictures/"), currentUserId + ".jpg");

            if (System.IO.File.Exists(path))
            {
                AspNetUsersModel tempUser = AspNetUsersModel.Get(Guid.Parse(currentUserId), user.Email);
                result = tempUser.ProfilePicUrl;
            }
            else
            {
                result = "Images/account_no_image.png";
            }

            return result;
        }


        #endregion tools
    }
}