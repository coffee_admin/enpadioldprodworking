﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Enpadi_WebUI.Controllers
{
    public class AddCustomerController : Controller
    {
        // GET: AddCustomer
        public ActionResult AddCustomer()
        {


            return View();
        }

        public JsonResult LoadCountries() {

            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadStates()
        {

            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadCities()
        {

            return Json(new { }, JsonRequestBehavior.AllowGet);
        }
    }
}