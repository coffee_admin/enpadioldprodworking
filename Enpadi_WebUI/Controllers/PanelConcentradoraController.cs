﻿using Enpadi_WebUI.Models;
using Openpay;
using Openpay.Entities;
using Openpay.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Configuration;
using Enpadi_WebUI.Properties;
using System.Data.Entity;


namespace Enpadi_WebUI.Controllers
{
    public class PanelConcentradoraController : Controller
    {

        private ApplicationDbContext db = new ApplicationDbContext();

        private static bool bModoProduccion = Convert.ToBoolean(ConfigurationManager.AppSettings["ModoProduccion"]);

        private string OPENPAY_ID = (bModoProduccion) ? ConfigurationManager.AppSettings["Enpadi_OpenPay_Id"] : ConfigurationManager.AppSettings["Test_OpenPay_id"];
        private string OPENPAY_PUBLIC_KEY = (bModoProduccion) ? ConfigurationManager.AppSettings["Enpadi_OpenPay_PublicKey"] : ConfigurationManager.AppSettings["Test_OpenPay_PublicKey"];

        private string OPENPAY_PRIVATE_KEY = (bModoProduccion) ? Settings.Default.Enpadi_OpenPay_PrivateKey : Settings.Default.Test_OpenPay_PrivateKey;

        private string DIESTEL_URL = (bModoProduccion) ? Settings.Default.Enpadi_WebUI_WSDiestel_PxUniversal : Settings.Default.Test_WebUI_WSDiestel_PxUniversal;
        private int DIESTEL_GRUPO = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Grupo : Settings.Default.Test_WSDiestel_Grupo;
        private int DIESTEL_CADENA = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Cadena : Settings.Default.Test_WSDiestel_Cadena;
        private int DIESTEL_TIENDA = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Tienda : Settings.Default.Test_WSDiestel_Tienda;
        private string DIESTEL_USUARIO = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Usuario : Settings.Default.Test_WSDiestel_Usuario;
        private string DIESTEL_PASSWORD = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Password : Settings.Default.Test_WSDiestel_Password;

        public ActionResult SantiagoNL(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SantiagoNL(santiagoPredialConsulta consulta, string skin)
        {

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                consulta.fecha_registro = DateTime.Now;
                db.santiagoPredialConsultas.Add(consulta);
                db.SaveChanges();
                return RedirectToAction("SantiagoNLPredial", "PanelConcentradora", consulta);
            }

            return View(consulta);
        }

        public ActionResult SantiagoNLPredial(santiagoPredialConsulta consulta, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            santiagoPredial predial = db.santiagoPredials.FirstOrDefault(x => x.expediente == consulta.no_predial);

            return View(predial);
        }

        public ActionResult Mapa(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        public ActionResult Bancos(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        public ActionResult Tiendas(decimal monto, string expediente, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            OpenpayAPI api = new OpenpayAPI(OPENPAY_PRIVATE_KEY, OPENPAY_ID, bModoProduccion);

            Customer cliente = new Customer();
            cliente.ExternalId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);
            cliente.Name = String.Format("{0} {1} {2}", usuario.datNombre, usuario.datPaterno, usuario.datMaterno);
            cliente.Email = String.Format("{0}", usuario.email);
            cliente.RequiresAccount = false;

            ChargeRequest request = new ChargeRequest();
            request.Method = "store";
            request.Amount = (decimal)monto;
            request.Description = "Pago en Tienda";
            request.OrderId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);
            request.Customer = cliente;

            Charge charge = api.ChargeService.Create(request);

            pagoTienda ordenTienda = new pagoTienda() { id = charge.Id, description = charge.Description, error_message = charge.ErrorMessage, authorization = charge.Authorization, amount = charge.Amount, operation_type = charge.OperationType, type = charge.PaymentMethod.Type, reference = charge.PaymentMethod.Reference, barcode_url = charge.PaymentMethod.BarcodeURL, order_id = charge.OrderId, transaction_type = charge.TransactionType, creation_date = charge.CreationDate.ToString(), status = charge.Status, method = charge.Method };

            ViewBag.MontoRecarga = monto;
            return View(ordenTienda);
        }

        public ActionResult Caja(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        public ActionResult Tarjeta(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        [HttpPost]
        public ActionResult Tarjeta(string token_id, string holder_name, string amount, string deviceSessionId, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (!ModelState.IsValid)
            {
                return View();
            }
            else
            {
                string currentUserId = User.Identity.GetUserId();
                tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
                trans t = new trans { estatusID = 0, origenID = 1, tipoID = 1, modoID = 1, socioID = 2, terminalID = 1, monedaID = 484, Concepto = "TC", geoCountryCode = 0, geoCountryName = String.Empty, geoRegion = String.Empty, geoCity = String.Empty, geoPostalCode = String.Empty };

                strSQL = string.Format("SELECT COUNT(*) As total FROM logTC WHERE idTarjeta = {0} AND fechaAlta >= DATEADD(DAY, -7, getdate())", usuario.tarjetaID);
                IEnumerable<contador> intentos = db.Database.SqlQuery<contador>(strSQL);
                int iIntentos = intentos.ToList().FirstOrDefault().total;

                strSQL = string.Format("SELECT COUNT(*) As total FROM logTC WHERE idTarjeta = {0} AND result = 1 AND fechaAlta >= DATEADD(DAY, -7, getdate())", usuario.tarjetaID);
                IEnumerable<contador> exitosos = db.Database.SqlQuery<contador>(strSQL);
                int iExitosos = exitosos.ToList().FirstOrDefault().total;

                if (iIntentos < 5 && iExitosos < 2)
                {
                    OpenpayAPI api = new OpenpayAPI(OPENPAY_PRIVATE_KEY, OPENPAY_ID);

                    Customer cliente = new Customer();
                    cliente.ExternalId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);
                    cliente.Name = holder_name;
                    cliente.Email = "mramireza@hotmail.com";
                    cliente.RequiresAccount = false;

                    ChargeRequest request = new ChargeRequest();
                    request.Method = "card";
                    request.SourceId = token_id;
                    request.Amount = Convert.ToDecimal(amount);
                    request.Currency = "MXN";
                    request.Description = "Recarga de Monedero Enpadi";
                    request.DeviceSessionId = deviceSessionId;
                    request.Customer = cliente;
                    request.OrderId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);

                    Charge charge = api.ChargeService.Create(request);

                    if (charge.Status == "completed")
                    {
                        t.estatusID = 1;
                        t.tarjetaID = usuario.tarjetaID;
                        t.tarjeta = usuario.tcNum.Substring(usuario.tcNum.Length - 4);
                        t.transaccion = charge.OrderId;
                        t.descripcion = charge.Description;
                        t.referencia = charge.Id;
                        t.aprobacion = charge.Authorization;
                        t.monto = charge.Amount; t.abono1 = charge.Amount; t.cargo1 = 0; t.abono2 = 0; t.cargo2 = 0; t.abono3 = 0; t.cargo3 = 0; t.abonoTotal = charge.Amount; t.cargoTotal = 0;
                        t.ip = "189.25.169.74";
                        db.transacciones.Add(t);

                        recompensa r = new recompensa() { tarjetaID = usuario.tarjetaID, origenID = 1, socioID = 2, transaccion = charge.OrderId, concepto = "Recompensa Recarga", abono = charge.Amount * (decimal)0.01, cargo = 0 };
                        db.recompensas.Add(r);

                        logTC envio = new logTC() { idTarjeta = usuario.tarjetaID, result = 1 };
                        db.logTCs.Add(envio);

                        db.SaveChanges();

                        return RedirectToAction("TarjetaResp", "PanelConcentradora", new { estatus = "APROBADA", autorizacion = string.Format("AUTORIZACIÓN  {0}", t.aprobacion), total = string.Format("MONTO  ${0} MXN", t.monto) });
                    }
                    else
                    {
                        logTC envio = new logTC() { idTarjeta = usuario.tarjetaID, result = 0 };
                        db.logTCs.Add(envio);

                        db.SaveChanges();
                        return RedirectToAction("TarjetaResp", "PanelConcentradora", new { estatus = "DECLINADA", autorizacion = charge.Description, total = "" });
                    }
                }
                else
                {
                    return RedirectToAction("TarjetaResp", "PanelConcentradora", new { estatus = "DECLINADA", autorizacion = "Has excedido el Limite de Recargas Autorizado por Semana", total = "" });
                }
            }
        }


        // GET: PanelUsuarios
        public ActionResult TarjetaResp(string estatus, string autorizacion, string total, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            respuestaTC r = new respuestaTC() { Estatus = estatus, Autorizacion = autorizacion, Total = total };
            return View(r);
        }

        public ActionResult Movimientos(string skin, int? periodo)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            ViewModelEstadoDeCuenta edoCuenta = new ViewModelEstadoDeCuenta();

            edoCuenta.periodo = (int)periodo;

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
            int tID = usuario.tarjetaID;

            edoCuenta.usuario = usuario;
            strSQL = string.Empty;

            strSQL = " SELECT numero, CONVERT(VARCHAR, Anio) + '  ' + CASE Mes WHEN 1 THEN 'ENERO' WHEN 2 THEN 'FEBRERO' WHEN 3 THEN 'MARZO' WHEN 4 THEN 'ABRIL' WHEN 5 THEN 'MAYO' WHEN 6 THEN 'JUNIO' WHEN 7 THEN 'JULIO' "
                   + " WHEN 8 THEN 'AGOSTO' WHEN 9 THEN 'SEPTIEMBRE' WHEN 10 THEN 'OCTUBRE' WHEN 11 THEN 'NOVIEMBRE' WHEN 12 THEN 'DICIEMBRE' END As Mes "
                   + " FROM ( "
                   + " SELECT 0 As numero, DATEPART(MONTH, DATEADD(MONTH, 0, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, 0, GETDATE())) As Anio "
                   + " UNION "
                   + " SELECT -1 As numero, DATEPART(MONTH, DATEADD(MONTH, -1, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -1, GETDATE())) As Anio "
                   + " UNION "
                   + " SELECT -2 As numero, DATEPART(MONTH, DATEADD(MONTH, -2, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -2, GETDATE())) As Anio "
                   + " UNION "
                   + " SELECT -3 As numero, DATEPART(MONTH, DATEADD(MONTH, -3, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -3, GETDATE())) As Anio "
                   + " UNION "
                   + " SELECT -4 As numero, DATEPART(MONTH, DATEADD(MONTH, -4, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -4, GETDATE())) As Anio "
                   + " UNION "
                   + " SELECT -5 As numero, DATEPART(MONTH, DATEADD(MONTH, -5, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -5, GETDATE())) As Anio "
                   + " UNION "
                   + " SELECT -6 As numero, DATEPART(MONTH, DATEADD(MONTH, -6, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -6, GETDATE())) As Anio "
                   + " UNION "
                   + " SELECT -7 As numero, DATEPART(MONTH, DATEADD(MONTH, -7, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -7, GETDATE())) As Anio "
                   + " UNION "
                   + " SELECT -8 As numero, DATEPART(MONTH, DATEADD(MONTH, -8, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -8, GETDATE())) As Anio "
                   + " UNION "
                   + " SELECT -9 As numero, DATEPART(MONTH, DATEADD(MONTH, -9, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -9, GETDATE())) As Anio "
                   + " UNION "
                   + " SELECT -10 As numero, DATEPART(MONTH, DATEADD(MONTH, -10, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -10, GETDATE())) As Anio "
                   + " UNION "
                   + " SELECT -11 As numero, DATEPART(MONTH, DATEADD(MONTH, -11, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -11, GETDATE())) As Anio "
                   + " UNION "
                   + " SELECT -12 As numero, DATEPART(MONTH, DATEADD(MONTH, -12, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -12, GETDATE())) As Anio "
                   + " ) As Meses "
                   + " ORDER BY numero DESC ";
            IEnumerable<periodo> meses = db.Database.SqlQuery<periodo>(strSQL);
            edoCuenta.meses = meses.ToList();

            strSQL = " SELECT CASE DATEPART(MONTH, DATEADD(MONTH, " + periodo.ToString() + ", GetDate())) "
                   + " WHEN 1 THEN 'ENERO' WHEN 2 THEN 'FEBRERO' WHEN 3 THEN 'MARZO' WHEN 4 THEN 'ABRIL' "
                   + " WHEN 5 THEN 'MAYO' WHEN 6 THEN 'JUNIO' WHEN 7 THEN 'JULIO' WHEN 8 THEN 'AGOSTO' "
                   + " WHEN 9 THEN 'SEPTIEMBRE' WHEN 10 THEN 'OCTUBRE' WHEN 11 THEN 'NOVIEMBRE' WHEN 12 THEN 'DICIEMBRE' "
                   + " END + ' ' + CONVERT(varchar(10), DATEPART(YEAR, DATEADD(MONTH, " + periodo.ToString() + ", GetDate()))) AS [desc] ";
            IEnumerable<descripcion> descrip = db.Database.SqlQuery<descripcion>(strSQL);
            edoCuenta.periodo_descripcion = descrip.ToList().FirstOrDefault().desc;

            strSQL = " SELECT t.transID, o.descripcion AS origen, i.descripcion AS tipo, t.socioID, s.nombreComercial As socio, terminalID, "
                   + " m.codigo AS moneda, t.tarjetaID, t.tarjeta, t.monto, (t.cargo3 + t.cargo4) AS comision, (t.cargo3iva + t.cargo4iva) AS iva, t.transaccion, "
                   + " t.concepto, t.descripcion, t.referencia, e.descripcion As estatus, t.aprobacion, t.ip, t.fechaRegistro "
                   + " FROM Transacciones t "
                   + " LEFT JOIN Origen o ON t.origenID = o.origenID "
                   + " LEFT JOIN Tipo i ON t.tipoID = i.tipoID "
                   + " LEFT JOIN Socios s ON t.socioID = s.socioID "
                   + " LEFT JOIN Monedas m ON t.monedaID = m.monedaID "
                   + " LEFT JOIN Estatus e ON t.estatusID = e.estatusID "
                   + " WHERE t.tarjetaID = " + tID + " AND DateDiff(MONTH, GetDate(), t.FechaRegistro) = " + periodo.ToString()
                   + " ORDER BY t.fechaRegistro ";
            IEnumerable<movimientosListado> movs = db.Database.SqlQuery<movimientosListado>(strSQL);
            edoCuenta.movimientos = movs.ToList();

            strSQL = " SELECT ISNULL(SUM(abonoTotal-cargoTotal), 0) As Total "
                   + " FROM Transacciones "
                   + " WHERE tarjetaID = " + tID + " AND DateDiff(MONTH, GetDate(), FechaRegistro) < " + periodo.ToString();
            IEnumerable<total> saldoInicial = db.Database.SqlQuery<total>(strSQL);
            edoCuenta.saldoInicial = saldoInicial.ToList().FirstOrDefault().Total;

            strSQL = " SELECT ISNULL(SUM(abonoTotal), 0) As Total "
                   + " FROM Transacciones "
                   + " WHERE tarjetaID = " + tID + " AND tipoID = 1 AND DateDiff(MONTH, GetDate(), FechaRegistro) = " + periodo.ToString();
            IEnumerable<total> abonos = db.Database.SqlQuery<total>(strSQL);
            edoCuenta.abonos = abonos.ToList().FirstOrDefault().Total;

            strSQL = " SELECT ISNULL(SUM(cargoTotal), 0) As Total "
                   + " FROM Transacciones "
                   + " WHERE tarjetaID = " + tID + " AND tipoID = 2 AND DateDiff(MONTH, GetDate(), FechaRegistro) = " + periodo.ToString();
            IEnumerable<total> cargos = db.Database.SqlQuery<total>(strSQL);
            edoCuenta.cargos = cargos.ToList().FirstOrDefault().Total;

            strSQL = " SELECT ISNULL(SUM(abonoTotal-cargoTotal),0) As Total "
                   + " FROM Transacciones "
                   + " WHERE tarjetaID = " + tID + " AND DateDiff(MONTH, GetDate(), FechaRegistro) <= " + periodo.ToString();
            IEnumerable<total> saldoFinal = db.Database.SqlQuery<total>(strSQL);
            edoCuenta.saldoFinal = saldoFinal.ToList().FirstOrDefault().Total;

            return View(edoCuenta);
        }

        public ActionResult Migracion(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            strSQL = "SELECT * FROM SantiagoPredial";
            IEnumerable<santiagoPredial> santiago = db.Database.SqlQuery<santiagoPredial>(strSQL);

            foreach (santiagoPredial predio in santiago.ToList())
            {
                strSQL = String.Format("SELECT predialID As total FROM Predial WHERE FechaBaja IS NULL AND socioID = 7 AND expedienteNo = '{0}'", predio.expediente);
                int ID = db.Database.SqlQuery<contador>(strSQL).FirstOrDefault().total;
                if (ID > 0)
                {
                    string sTransaccion = String.Format("{0}{1}", DateTime.Now.ToString("yyMMddhhmm"), ID);
                    string sReferencia = "2017 ENERO";

                    if(predio.actual > 0) {
                        predialMovimiento actual = new predialMovimiento() { predialID = ID, conceptoID = 1, descripcion = "Predial", transaccion = sTransaccion, referencia = sReferencia, cargo1 = predio.actual, cargoTotal = predio.actual, fechaRegistro = DateTime.Now };
                        db.predialMovimientoes.Add(actual);
                    }

                    if (predio.rezago > 0) {
                        predialMovimiento rezago = new predialMovimiento() { predialID = ID, conceptoID = 2, descripcion = "Rezagos", transaccion = sTransaccion, referencia = sReferencia, cargo1 = predio.rezago, cargoTotal = predio.rezago, fechaRegistro = DateTime.Now };
                        db.predialMovimientoes.Add(rezago);
                    }

                    if (predio.recargoRezago > 0) {
                        predialMovimiento recargoRezago = new predialMovimiento() { predialID = ID, conceptoID = 3, descripcion = "Recargos Rezagos", transaccion = sTransaccion, referencia = sReferencia, cargo1 = predio.recargoRezago, cargoTotal = predio.recargoRezago, fechaRegistro = DateTime.Now };
                        db.predialMovimientoes.Add(recargoRezago);
                    }

                    if (predio.recargoActual > 0) {
                        predialMovimiento recargoActual = new predialMovimiento() { predialID = ID, conceptoID = 4, descripcion = "Recargos Actual", transaccion = sTransaccion, referencia = sReferencia, cargo1 = predio.recargoActual, cargoTotal = predio.recargoActual, fechaRegistro = DateTime.Now };
                        db.predialMovimientoes.Add(recargoActual);
                    }
                    
                    db.SaveChanges();
                }
            }

            return View();
        }

    }
}