﻿using Enpadi_WebUI.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Security.Cryptography;
using System.Xml;
using System.Configuration;
using Microsoft.Owin.Security;
using Enpadi_WebUI.Properties;
using System.Net;
using Enpadi_WebUI.WSDiestel;
using System.Data.Entity;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Enpadi_WebUI.Controllers
{
    public class Tools
    {

        #region global variables

        private static bool bModoProduccion = Convert.ToBoolean(ConfigurationManager.AppSettings["ModoProduccion"]);
        private static String region = ConfigurationManager.AppSettings["Region"];

        public const String localFileName = "Controllers/Tools";
        public const String userRoleAditionalIngo = "Default role assign: " + SecurityRoles.Usuario + " to user ";

        #region diestel

        private static string DIESTEL_URL = (bModoProduccion) ? Settings.Default.Enpadi_WebUI_WSDiestel_PxUniversal : Settings.Default.Test_WebUI_WSDiestel_PxUniversal;
        private static int DIESTEL_GRUPO = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Grupo : Settings.Default.Test_WSDiestel_Grupo;
        private static int DIESTEL_CADENA = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Cadena : Settings.Default.Test_WSDiestel_Cadena;
        private static int DIESTEL_TIENDA = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Tienda : Settings.Default.Test_WSDiestel_Tienda;
        private static string DIESTEL_USUARIO = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Usuario : Settings.Default.Test_WSDiestel_Usuario;
        private static string DIESTEL_PASSWORD = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Password : Settings.Default.Test_WSDiestel_Password;

        #endregion diestel

        #endregion global variables

        #region user data and exchange rate

        public static String GetUserRole(System.Security.Principal.IPrincipal user)
        {
            var context = new ApplicationDbContext();
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);
            String userId = user.Identity.GetUserId();

            //get user roles
            var userIdentity = (ClaimsIdentity)user.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            String role = string.Empty;

            //if roles count is 0 then assign default role
            if (roles.Count == 0)
            {
                string userEmail = user.Identity.GetUserName();

                userManager.AddToRole(userId, SecurityRoles.Usuario);

                context.Entry(user).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();

                //insert this in log and warning
                SystemErrorModel error = new SystemErrorModel
                {
                    Description = SystemErrorModel.ErrorList.UserErrorRoleNotFound,
                    AditionalInfo = userRoleAditionalIngo + userId + " with user " + userEmail,
                    FileName = localFileName,
                    ErrorFound = true,
                    ErrorNumber = SystemErrorModel.ErrorList.UserErrorRoleNotFoundNumber
                };

                SystemErrorModel.Save(error);

                role = SecurityRoles.Usuario;
            }

            return role;
        }

        public static Decimal GetPesoToDollarRate(Decimal addExtra = -1)
        {
            try
            {
                WSBanxico.DgieWSPortClient TipoDeCambio = new WSBanxico.DgieWSPortClient();
                String strTipoCambio = TipoDeCambio.tiposDeCambioBanxico();

                XmlDocument xml = new XmlDocument();
                xml.LoadXml(strTipoCambio);

                Decimal exchangeRate = Decimal.Parse(xml.ChildNodes[1].ChildNodes[1].ChildNodes[2].ChildNodes[0].Attributes[1].Value);
                exchangeRate += addExtra;

                return exchangeRate;
            }
            catch (Exception e)
            {
                return 19.90m + addExtra;
            }

        }

        public static Decimal GetUserBalance(Guid currentUserId)
        {
            return _getUserBalance(currentUserId);
        }

        public static Decimal GetUserBalance(String currentUserId)
        {
            return _getUserBalance(Guid.Parse(currentUserId));
        }

        private static Decimal _getUserBalance(Guid currentUserId)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId.ToString());
            try
            {
                Decimal balance = db.transacciones.Where(x => (x.PaidWithCredit == false || x.PaidWithCredit == null) && x.tarjetaID == usuario.tarjetaID && x.fechaBaja == null).Sum(x => x.abonoTotal - x.cargoTotal);
                return balance;
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public static Decimal GetUserCredit(Guid currentUserId)
        {
            return _getUserCredit(currentUserId);
        }

        public static Decimal GetUserCredit(String currentUserId)
        {
            return _getUserCredit(Guid.Parse(currentUserId));
        }

        private static Decimal _getUserCredit(Guid currentUserId)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            Decimal exchangeRateDown = Tools.GetPesoToDollarRate(-1);

            string strSQL = String.Empty;
            tarjetas userCard = GetUserEnpadiCard(currentUserId);

            Decimal AvailableCredit = 0;
            if (userCard.creditoDisponible != null)
            {
                if (userCard.creditoPorPagar == null)
                {
                    userCard.creditoPorPagar = 0;
                }
                AvailableCredit = (Decimal)userCard.creditoDisponible - (Decimal)userCard.creditoPorPagar;
            }

            decimal currentAvailableCredit = AvailableCredit * exchangeRateDown;

            return currentAvailableCredit;
        }

        public static tarjetas GetUserEnpadiCard(String currentUserId)
        {
            return _getUserEnpadiCard(Guid.Parse(currentUserId));
        }
        public static tarjetas GetUserEnpadiCard(Guid currentUserId)
        {
            return _getUserEnpadiCard(currentUserId);
        }

        public static tarjetas _getUserEnpadiCard(Guid currentUserId)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            return db.tarjetas.FirstOrDefault(x => x.password == currentUserId.ToString());
        }

        #endregion user data and exchange rate

        #region pay services

        /// <summary>
        /// Makes payment via Diestel services
        /// </summary>
        public static pagoServicio PayServiceWithDiestel(
            pagoServicio oPago,
            String currentUserId,
            Guid platformId,
            Boolean variableFeeForPartnerCharge,
            List<Fee> fees,
            Boolean useUserCredit)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            pagoServicio _paymentOrder = db.pagoServicios.Where(x => x.Id == oPago.Id).FirstOrDefault();

            if (_paymentOrder == null)
            {
                throw new ArgumentException(SystemErrorModel.ErrorList.PaymentOrderNotSaveInDataBase);
            }

            if (_paymentOrder.transaccionesId != null)
            {

                _paymentOrder = Tools.AddDisplayFields(_paymentOrder);

                return _paymentOrder;
            }

            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            if (usuario == null)
            {
                throw new ArgumentException(SystemErrorModel.ErrorList.UserEnpadiCardNotFound);
            }

            Enpadi_WebUI.WSDiestel.cCampo[] resp;

            Decimal feeChargedByPartner = _paymentOrder.supplierFeeInMXN.GetValueOrDefault();
            Decimal userCurrentBalance = Tools.GetUserBalance(currentUserId);
            Decimal userCurrentAvailableCredit = Tools.GetUserCredit(currentUserId);

            Decimal exchangeRateUp = _paymentOrder.exchageRateUp.GetValueOrDefault();
            Decimal exchangeRateDown = _paymentOrder.exchageRateDown.GetValueOrDefault();


            Decimal ComissionMX = _paymentOrder.comissionMXN.GetValueOrDefault();
            Decimal TotalAmountMX = _paymentOrder.totalMXN.GetValueOrDefault();
            Decimal ComissionUSD = _paymentOrder.comissionUSD.GetValueOrDefault();
            Decimal TotalAmountUSD = _paymentOrder.totalUSD.GetValueOrDefault();
            Decimal AmountForServiceMXN = _paymentOrder.serviceInMXN.GetValueOrDefault();
            Decimal AmountForServiceUSD = _paymentOrder.serviceInUSD.GetValueOrDefault();

            servicios servicio = db.servicios.FirstOrDefault(x => x.SKU == _paymentOrder.sku);

            if (useUserCredit == true)
            {
                if (TotalAmountMX > userCurrentAvailableCredit)
                {
                    _paymentOrder.estatus = 2;
                    _paymentOrder.codigoRespuesta = 908;
                    _paymentOrder.codigoRespuestaDescr = "Saldo Insuficiente. Recargue su Cuenta";

                    //pagoServicio rowPago = db.pagoServicios.Find(oPago.pagoID);
                    //oPago.estatus = 2;
                    //oPago.codigoRespuesta = 908;
                    //oPago.codigoRespuestaDescr = "Saldo Insuficiente. Recargue su Cuenta";
                    //db.Entry(rowPago).Property(x => x.estatus).IsModified = true;
                    //db.Entry(rowPago).Property(x => x.codigoRespuesta).IsModified = true;
                    //db.Entry(rowPago).Property(x => x.codigoRespuestaDescr).IsModified = true;
                    db.Entry(_paymentOrder).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                    return _paymentOrder;
                }
            }
            else if (TotalAmountMX > userCurrentBalance)
            {
                _paymentOrder.estatus = 2;
                _paymentOrder.codigoRespuesta = 908;
                _paymentOrder.codigoRespuestaDescr = "Saldo Insuficiente. Recargue su Cuenta";

                //pagoServicio rowPago = db.pagoServicios.Find(oPago.pagoID);
                //rowPago.estatus = 2;
                //rowPago.codigoRespuesta = 908;
                //rowPago.codigoRespuestaDescr = "Saldo Insuficiente. Recargue su Cuenta";
                //db.Entry(rowPago).Property(x => x.estatus).IsModified = true;
                //db.Entry(rowPago).Property(x => x.codigoRespuesta).IsModified = true;
                //db.Entry(rowPago).Property(x => x.codigoRespuestaDescr).IsModified = true;
                //db.SaveChanges();

                return _paymentOrder;

            }

            Enpadi_WebUI.WSDiestel.PxUniversal ws = new Enpadi_WebUI.WSDiestel.PxUniversal() { Url = DIESTEL_URL, Timeout = 45000 };
            NetworkCredential oCredenciales = new NetworkCredential() { UserName = DIESTEL_USUARIO, Password = DIESTEL_PASSWORD };

            Enpadi_WebUI.WSDiestel.cCampo[] req = new Enpadi_WebUI.WSDiestel.cCampo[17];
            req[0] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "IDGRUPO", sValor = DIESTEL_GRUPO };
            req[1] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "IDCADENA", sValor = DIESTEL_CADENA };
            req[2] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "IDTIENDA", sValor = DIESTEL_TIENDA };
            req[3] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "IDPOS", sValor = 1 };
            req[4] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "IDCAJERO", sValor = 1 };
            req[5] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "FECHALOCAL", sValor = _paymentOrder.fechaLocal };
            req[6] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "HORALOCAL", sValor = _paymentOrder.horaLocal };
            req[7] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "TRANSACCION", sValor = _paymentOrder.pagoID };
            req[8] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "SKU", sValor = _paymentOrder.sku };
            req[9] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "REFERENCIA", sValor = _paymentOrder.supplierSendReference };
            req[10] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "FECHACONTABLE", sValor = _paymentOrder.fechaContable };
            req[11] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "MONTO", sValor = _paymentOrder.supplierAmountForServiceInMXN.GetValueOrDefault() };
            req[12] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "TIPOPAGO", sValor = _paymentOrder.tipoPago };

            if (_paymentOrder.dv != null)
                req[13] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "DV", sValor = _paymentOrder.dv };
            if (_paymentOrder.comision != null)
                req[14] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "COMISION", sValor = _paymentOrder.supplierFeeInMXN.GetValueOrDefault() };
            if (_paymentOrder.token != null)
                req[15] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "TOKEN", sValor = _paymentOrder.token };
            if (_paymentOrder.referencia2 != null)
                req[16] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "REFERENCIA2", sValor = _paymentOrder.referencia2 };

            try
            {
                //add petition as it was send to partner to process
                Coffee.SaveEnpadiPetition(_paymentOrder, _paymentOrder.Id, _paymentOrder.pagoID);

                ws.Credentials = oCredenciales;
                resp = ws.Ejecuta(req);
                _paymentOrder.Campos = resp;

                //save petition as it was recived by partner to process
                foreach (Enpadi_WebUI.WSDiestel.cCampo campo in resp)
                {
                    Coffee.SaveEnpadiResponse(campo, _paymentOrder.Id, _paymentOrder.pagoID);
                }

                //pagoServicio rowPago = db.pagoServicios.Where(x => x.pagoID == _paymentOrder.pagoID).FirstOrDefault();

                foreach (var item in resp)
                {
                    switch (item.sCampo)
                    {
                        case "MONTO":
                            //_paymentOrder.monto = Decimal.Parse(item.sValor.ToString().Replace('$', ' '));
                            //_paymentOrder.monto = Decimal.Parse(item.sValor.ToString().Replace('$', ' '));
                            //db.Entry(_paymentOrder).Property(x => x.monto).IsModified = true;
                            break;
                        case "COMISION":

                            break;
                        case "CODIGORESPUESTA":
                            _paymentOrder.codigoRespuesta = Convert.ToInt32(item.sValor.ToString());
                            db.Entry(_paymentOrder).Property(x => x.codigoRespuesta).IsModified = true;

                            //Si la respuesta incluye un CODIGO RESPUESTA mayor a 0 se da por hecho que el pago fue RECHAZADO
                            //if (_paymentOrder.codigoRespuesta > 0)
                            //{
                            //    //Se actualiza la informacion adicional de la operacion en la DB
                            //    _paymentOrder.monto = _paymentOrder.monto;
                            //    db.Entry(_paymentOrder).Property(x => x.monto).IsModified = true;
                            //    _paymentOrder.comision = _paymentOrder.comision;
                            //    db.Entry(_paymentOrder).Property(x => x.comision).IsModified = true;
                            //}
                            break;
                        case "CODIGORESPUESTADESCR":
                            _paymentOrder.codigoRespuestaDescr = item.sValor.ToString();
                            db.Entry(_paymentOrder).Property(x => x.codigoRespuestaDescr).IsModified = true;
                            break;
                        case "AUTORIZACION":
                            _paymentOrder.autorizacion = item.sValor.ToString();
                            db.Entry(_paymentOrder).Property(x => x.autorizacion).IsModified = true;

                            //Si la respuesta incluye el campo AUTORIZACION se da por hecho que el pago fue ACEPTADO
                            _paymentOrder.estatus = Estatus.db_keys.payment_approved;


                            db.Entry(_paymentOrder).Property(x => x.estatus).IsModified = true;
                            //Se debita el Monto Pagado del Monedero
                            string sTransaccion = DateTime.Now.ToString("yyyyMMddHHmmss");

                            trans cargo = new trans()
                            {
                                Id = Guid.NewGuid(),
                                origenID = trans.TransOrigin.External,
                                tipoID = trans.TransType.Payment,
                                modoID = 1,
                                socioID = socio.SocioID.Pago_de_servicios,
                                terminalID = 1,
                                monedaID = moneda.currency.MexicanPesoIntId,
                                tarjetaID = usuario.tarjetaID,
                                monto = AmountForServiceMXN,
                                abono1 = 0,
                                cargo1 = AmountForServiceMXN,
                                abono2 = 0,
                                cargo2 = 0,
                                abono3 = 0,
                                cargo3 = 0,
                                abonoTotal = 0,
                                cargoTotal = TotalAmountMX,
                                tarjeta = usuario.tcNum.Substring(usuario.tcNum.Length - 4),
                                transaccion = sTransaccion,
                                Concepto = _paymentOrder.compañia,
                                descripcion = String.Format("{0} - {1} ({2})", _paymentOrder.sku, _paymentOrder.compañia, _paymentOrder.referencia),
                                referencia = _paymentOrder.pagoID.ToString(),
                                estatusID = 1,
                                aprobacion = _paymentOrder.autorizacion,
                                ip = "189.25.169.74",
                                geoCountryCode = 0,
                                geoCountryName = String.Empty,
                                geoRegion = String.Empty,
                                geoCity = String.Empty,
                                geoPostalCode = String.Empty,
                                fechaRegistro = DateTime.Now,

                                ComissionMX = ComissionMX,
                                ComissionUSD = ComissionUSD,
                                TotalMX = TotalAmountMX,
                                TotalUSD = TotalAmountUSD,
                                ExchangeRateDown = exchangeRateDown,
                                ExchangeRateUp = exchangeRateUp,
                                PaidWithCredit = useUserCredit,

                                ServiceInDlls = AmountForServiceUSD,
                                ServiceInMXN = AmountForServiceMXN
                            };

                            if (_paymentOrder.compañia == "IUSACELL" ||
                                _paymentOrder.compañia == "UNEFON" ||
                                _paymentOrder.compañia == "NEXTEL" ||
                                _paymentOrder.compañia == "MOVISTAR" ||
                                _paymentOrder.compañia == "VIRGIN" ||
                                _paymentOrder.compañia == "TELCEL")
                            {
                                cargo.socioID = socio.SocioID.Tiempo_aire;
                            }

                            db.transacciones.Add(cargo);
                            db.Entry(cargo).State = System.Data.Entity.EntityState.Added;
                            _paymentOrder.transaccionesId = cargo.Id;
                            db.Entry(_paymentOrder).Property(x => x.transaccionesId).IsModified = true;

                            db.SaveChanges();



                            foreach (Enpadi_WebUI.Models.Fee fee in fees)
                            {
                                TransactionFee transfee = new TransactionFee();
                                if (fee.CurrencyId == moneda.currency.MexicanPesoId)
                                {
                                    transfee.Id = Guid.NewGuid();
                                    transfee.TransaccionesId = cargo.Id;
                                    transfee.FeeId = fee.Id;
                                    transfee.FixAmount = fee.FixFee;
                                    transfee.TaxFeeAmount = fee.TaxFee;
                                    transfee.VariableAmount = fee.VariableFee * feeChargedByPartner;
                                    if (fee.PartnerToPayId == socio.SocioID.Enpadi)
                                    {
                                        transfee.ChargedByPartnerAmount = _paymentOrder.supplierFeeInMXN.GetValueOrDefault();
                                    }
                                    else
                                    {
                                        transfee.ChargedByPartnerAmount = 0;
                                    }
                                    transfee.PaidOut = false;
                                    transfee.PaidOutAt = null;
                                    transfee.AmountPaid = null;



                                    transfee.FixAmountDlls = fee.FixFee / exchangeRateDown;
                                    transfee.TaxFeeAmountDlls = fee.TaxFee / exchangeRateDown;
                                    transfee.VariableAmountDlls = (fee.VariableFee * feeChargedByPartner) / exchangeRateDown;
                                }
                                else
                                {
                                    transfee.Id = Guid.NewGuid();
                                    transfee.TransaccionesId = cargo.Id;
                                    transfee.FeeId = fee.Id;
                                    transfee.FixAmount = fee.FixFee * exchangeRateUp;
                                    transfee.VariableAmount = (fee.VariableFee * feeChargedByPartner) / exchangeRateDown;
                                    transfee.TaxFeeAmount = fee.TaxFee;

                                    if (fee.PartnerToPayId == socio.SocioID.Enpadi)
                                    {
                                        transfee.ChargedByPartnerAmount = _paymentOrder.supplierFeeInMXN.GetValueOrDefault();
                                    }
                                    else
                                    {
                                        transfee.ChargedByPartnerAmount = 0;
                                    }
                                    transfee.PaidOut = false;
                                    transfee.PaidOutAt = null;
                                    transfee.AmountPaid = null;

                                    transfee.FixAmountDlls = fee.FixFee;
                                    transfee.TaxFeeAmountDlls = fee.TaxFee;
                                    transfee.VariableAmountDlls = (fee.VariableFee * feeChargedByPartner);
                                }
                                //add diestel fee
                                if (fee.PartnerToPayId == socio.SocioID.Enpadi)
                                {
                                    transfee.ChargedByPartnerAmount = feeChargedByPartner;
                                }

                                db.TransactionFee.Add(transfee);
                                db.Entry(transfee).State = System.Data.Entity.EntityState.Added;


                            }

                            db.SaveChanges();

                            if (useUserCredit == true)
                            {
                                usuario.creditoPorPagar = (usuario.creditoPorPagar == null ? 0 : usuario.creditoPorPagar) + _paymentOrder.serviceInUSD;

                                EnpadiCardModel.Save(usuario);
                            }


                            break;
                        case "REFERENCIA2":
                            _paymentOrder.referencia2 = item.sValor.ToString();
                            db.Entry(_paymentOrder).Property(x => x.referencia2).IsModified = true;
                            break;
                        case "TOKEN":
                            _paymentOrder.token = item.sValor.ToString();
                            db.Entry(_paymentOrder).Property(x => x.token).IsModified = true;
                            break;
                        case "BRAND":
                            _paymentOrder.brand = item.sValor.ToString();
                            db.Entry(_paymentOrder).Property(x => x.brand).IsModified = true;
                            break;
                        case "PROVEEDOR":
                            _paymentOrder.proveedor = item.sValor.ToString();
                            db.Entry(_paymentOrder).Property(x => x.proveedor).IsModified = true;
                            break;
                        case "LEYENDA":
                            _paymentOrder.leyenda = item.sValor.ToString();
                            db.Entry(_paymentOrder).Property(x => x.leyenda).IsModified = true;
                            break;
                        case "LEYENDA1":
                            _paymentOrder.leyenda1 = item.sValor.ToString();
                            db.Entry(_paymentOrder).Property(x => x.leyenda1).IsModified = true;
                            break;
                        case "LEYENDA2":
                            _paymentOrder.leyenda2 = item.sValor.ToString();
                            db.Entry(_paymentOrder).Property(x => x.leyenda2).IsModified = true;
                            break;
                        default:
                            break;
                    }
                }

                db.SaveChanges();

                //Con este Codigo de Respuesta se Requiere el envio de Cancelación
                if (_paymentOrder.codigoRespuesta == 8 || _paymentOrder.codigoRespuesta == 71 || _paymentOrder.codigoRespuesta == 72)
                {
                    req[0] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "IDGRUPO", sValor = 7 };
                    req[1] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "IDCADENA", sValor = 1 };
                    req[2] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "IDTIENDA", sValor = 1 };
                    req[3] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "IDPOS", sValor = 1 };
                    req[4] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "IDCAJERO", sValor = 1 };
                    req[5] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "FECHALOCAL", sValor = _paymentOrder.fechaLocal };
                    req[6] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "HORALOCAL", sValor = _paymentOrder.horaLocal };
                    req[7] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "TRANSACCION", sValor = _paymentOrder.pagoID };
                    req[8] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "SKU", sValor = _paymentOrder.sku };
                    req[9] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "REFERENCIA", sValor = _paymentOrder.referencia };
                    req[10] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "FECHACONTABLE", sValor = _paymentOrder.fechaContable };
                    req[11] = new Enpadi_WebUI.WSDiestel.cCampo() { sCampo = "AUTORIZACION", sValor = _paymentOrder.autorizacion };
                    req[12] = null;
                    req[13] = null;
                    req[14] = null;
                    req[15] = null;
                    ws.Credentials = oCredenciales;
                    resp = ws.Reversa(req);

                    foreach (var item in resp)
                    {
                        switch (item.sCampo)
                        {
                            case "CODIGORESPUESTA":
                                if (Convert.ToInt32(item.sValor.ToString()) > 0)
                                {
                                    resp = ws.Reversa(req);
                                    foreach (var i in resp)
                                    {
                                        switch (i.sCampo)
                                        {
                                            case "CODIGORESPUESTA":
                                                if (Convert.ToInt32(item.sValor.ToString()) > 0)
                                                {
                                                    resp = ws.Reversa(req);
                                                }
                                                break;
                                        }
                                    }
                                }
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                if (ex.Message == "Se excedió el tiempo de espera de la operación")
                {
                    try
                    {
                        resp = ws.Reversa(req);
                        foreach (var item in resp)
                        {
                            switch (item.sCampo)
                            {
                                case "CODIGORESPUESTA":
                                    if (Convert.ToInt32(item.sValor.ToString()) > 0)
                                    {
                                        resp = ws.Reversa(req);
                                        foreach (var i in resp)
                                        {
                                            switch (i.sCampo)
                                            {
                                                case "CODIGORESPUESTA":
                                                    if (Convert.ToInt32(item.sValor.ToString()) > 0)
                                                    {
                                                        resp = ws.Reversa(req);
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                    catch (Exception ex1)
                    {
                        Console.WriteLine(ex1.Message);
                        if (ex.Message == "Se excedió el tiempo de espera de la operación")
                        {
                            try
                            {
                                resp = ws.Reversa(req);
                                foreach (var item in resp)
                                {
                                    switch (item.sCampo)
                                    {
                                        case "CODIGORESPUESTA":
                                            if (Convert.ToInt32(item.sValor.ToString()) > 0)
                                            {
                                                resp = ws.Reversa(req);
                                                foreach (var i in resp)
                                                {
                                                    switch (i.sCampo)
                                                    {
                                                        case "CODIGORESPUESTA":
                                                            if (Convert.ToInt32(item.sValor.ToString()) > 0)
                                                            {
                                                                resp = ws.Reversa(req);
                                                            }
                                                            break;
                                                    }
                                                }
                                            }
                                            break;
                                    }
                                }

                            }
                            catch (Exception ex2)
                            {
                                Console.WriteLine(ex2.Message);
                                if (ex.Message == "Se excedió el tiempo de espera de la operación")
                                {
                                    try
                                    {
                                        resp = ws.Reversa(req);
                                        foreach (var item in resp)
                                        {
                                            switch (item.sCampo)
                                            {
                                                case "CODIGORESPUESTA":
                                                    if (Convert.ToInt32(item.sValor.ToString()) > 0)
                                                    {
                                                        resp = ws.Reversa(req);
                                                        foreach (var i in resp)
                                                        {
                                                            switch (i.sCampo)
                                                            {
                                                                case "CODIGORESPUESTA":
                                                                    if (Convert.ToInt32(item.sValor.ToString()) > 0)
                                                                    {
                                                                        resp = ws.Reversa(req);
                                                                    }
                                                                    break;
                                                            }
                                                        }
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                    catch (Exception ex3)
                                    {
                                        Console.WriteLine(ex3.Message);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            _paymentOrder = Tools.AddDisplayFields(_paymentOrder);

            return _paymentOrder;
        }

        /// <summary>
        /// Makes payment to the empadi platform without sending data to third parties
        /// </summary>
        private static pagoServicio PayServiceWithEnpadi(
            List<Fee> fees,
            Decimal amountToChargeInMXN,
            string serviceSku,
            Guid currentUserId,
            pagoServicio oPago,
            decimal feeChargedByPartner,
            Guid platformId,
            String transaction_concept,
            String transaction_description,
            String transaction_folio,
            String transaction_reference,
            Boolean useUserCredit,
            Int32 socioIdForPayedService,
            string companyName,
            string instructions,
            string supplier
            )
        {
            ApplicationDbContext db = new ApplicationDbContext();
            pagoServicio _paymentOrder = db.pagoServicios.Where(x => x.Id == oPago.Id).FirstOrDefault();
            tarjetas usuario = Tools.GetUserEnpadiCard(currentUserId);

            if (_paymentOrder == null)
            {
                throw new ArgumentException(SystemErrorModel.ErrorList.PaymentOrderNotSaveInDataBase);
            }

            if (_paymentOrder.transaccionesId != null)
            {
                _paymentOrder = Tools.AddDisplayFields(_paymentOrder);

                return _paymentOrder;
            }

            if (usuario == null)
            {
                throw new ArgumentException(SystemErrorModel.ErrorList.UserEnpadiCardNotFound);
            }

            try
            {
                Decimal userCurrentBalance = Tools.GetUserBalance(currentUserId);
                Decimal userCurrentAvailableCredit = Tools.GetUserCredit(currentUserId);
                Decimal exchangeRateDown = oPago.exchageRateDown.GetValueOrDefault();
                Decimal exchangeRateUp = oPago.exchageRateUp.GetValueOrDefault();

                Decimal ComissionMX = oPago.comissionMXN.GetValueOrDefault();
                Decimal TotalAmountMX = oPago.totalMXN.GetValueOrDefault();
                Decimal ComissionUSD = oPago.comissionUSD.GetValueOrDefault();
                Decimal TotalAmountUSD = oPago.totalUSD.GetValueOrDefault();

                Decimal AmountForServiceMXN = oPago.serviceInMXN.GetValueOrDefault();
                Decimal AmountForServiceUSD = oPago.serviceInUSD.GetValueOrDefault();

                servicios servicio = db.servicios.FirstOrDefault(x => x.SKU == serviceSku);

                //ComissionMX = Enpadi_WebUI.Models.Fee.GetTotalFeeInMXN(fees, AmountForServiceMXN, exchangeRateUp, platformId, feeChargedByPartner, false);
                //ComissionUSD = Enpadi_WebUI.Models.Fee.GetTotalFeeInUSD(fees, AmountForServiceMXN, exchangeRateDown, platformId, feeChargedByPartner, false);

                ////get totals
                //TotalAmountMX = oPago.totalMXN.GetValueOrDefault();
                //TotalAmountUSD = oPago.totalUSD.GetValueOrDefault();

                if (useUserCredit == true)
                {
                    if (TotalAmountMX > userCurrentAvailableCredit)
                    {
                        _paymentOrder.estatus = 2;
                        _paymentOrder.codigoRespuesta = 908;
                        _paymentOrder.codigoRespuestaDescr = "Saldo Insuficiente. Recargue su Cuenta";

                        return _paymentOrder;
                    }
                }
                else if (TotalAmountMX > userCurrentBalance)
                {
                    _paymentOrder.estatus = 2;
                    _paymentOrder.codigoRespuesta = 908;
                    _paymentOrder.codigoRespuestaDescr = "Saldo Insuficiente. Recargue su Cuenta";

                    return _paymentOrder;
                }

                trans cargo = new trans()
                {
                    Id = Guid.NewGuid(),
                    origenID = trans.TransOrigin.Enpadi,
                    tipoID = trans.TransType.Payment,
                    modoID = 1,
                    socioID = socioIdForPayedService,
                    terminalID = 1,
                    monedaID = 484,
                    tarjetaID = usuario.tarjetaID,
                    monto = AmountForServiceMXN,
                    abono1 = 0,
                    cargo1 = 0,
                    abono2 = 0,
                    cargo2 = 0,
                    abono3 = 0,
                    cargo3 = 0,
                    abonoTotal = 0,
                    cargoTotal = TotalAmountMX,
                    tarjeta = usuario.tcNum.Substring(usuario.tcNum.Length - 4),
                    transaccion = transaction_folio,
                    Concepto = transaction_concept,
                    descripcion = transaction_description,
                    referencia = transaction_reference,
                    estatusID = 1,
                    aprobacion = "",
                    ip = "189.25.169.74",
                    geoCountryCode = 0,
                    geoCountryName = String.Empty,
                    geoRegion = String.Empty,
                    geoCity = String.Empty,
                    geoPostalCode = String.Empty,
                    fechaRegistro = DateTime.Now,

                    ComissionMX = ComissionMX,
                    ComissionUSD = ComissionUSD,
                    TotalMX = TotalAmountMX,
                    TotalUSD = TotalAmountUSD,
                    ExchangeRateDown = exchangeRateDown,
                    ExchangeRateUp = exchangeRateUp,
                    PaidWithCredit = useUserCredit,

                    ServiceInMXN = AmountForServiceMXN,
                    ServiceInDlls = AmountForServiceUSD
                };

                db.transacciones.Add(cargo);
                db.Entry(cargo).State = System.Data.Entity.EntityState.Added;
                _paymentOrder.transaccionesId = cargo.Id;
                db.Entry(_paymentOrder).Property(x => x.transaccionesId).IsModified = true;

                db.SaveChanges();

                foreach (Enpadi_WebUI.Models.Fee fee in fees)
                {
                    TransactionFee transfee = new TransactionFee();
                    if (fee.CurrencyId == moneda.currency.MexicanPesoId)
                    {
                        transfee.Id = Guid.NewGuid();
                        transfee.TransaccionesId = cargo.Id;
                        transfee.FeeId = fee.Id;
                        transfee.FixAmount = fee.FixFee;
                        transfee.TaxFeeAmount = fee.TaxFee;
                        transfee.VariableAmount = fee.VariableFee * 0;
                        transfee.ChargedByPartnerAmount = 0;
                        transfee.PaidOut = false;
                        transfee.PaidOutAt = null;
                        transfee.AmountPaid = null;


                        transfee.FixAmountDlls = fee.FixFee / exchangeRateUp;
                        transfee.TaxFeeAmountDlls = fee.TaxFee / exchangeRateUp;
                        transfee.VariableAmountDlls = fee.VariableFee * 0;
                    }
                    else
                    {
                        transfee.Id = Guid.NewGuid();
                        transfee.TransaccionesId = cargo.Id;
                        transfee.FeeId = fee.Id;
                        transfee.FixAmount = fee.FixFee * exchangeRateUp;
                        transfee.VariableAmount = (fee.VariableFee * 0) * exchangeRateDown;
                        transfee.ChargedByPartnerAmount = 0;
                        transfee.PaidOut = false;
                        transfee.PaidOutAt = null;
                        transfee.AmountPaid = null;

                        transfee.FixAmountDlls = fee.FixFee;
                        transfee.TaxFeeAmountDlls = fee.TaxFee;
                        transfee.VariableAmountDlls = fee.VariableFee * 0;

                    }
                    db.TransactionFee.Add(transfee);
                    db.Entry(transfee).State = System.Data.Entity.EntityState.Added;
                }

                _paymentOrder.estatus = 1;
                _paymentOrder.codigoRespuesta = 0;
                _paymentOrder.autorizacion = transaction_folio;
                _paymentOrder.referencia = transaction_reference;
                _paymentOrder.proveedor = supplier;
                db.Entry(_paymentOrder).State = System.Data.Entity.EntityState.Modified;

                db.SaveChanges();

                if (useUserCredit == true)
                {
                    //charge the user on credit
                    usuario.creditoPorPagar = (usuario.creditoPorPagar == null ? 0 : usuario.creditoPorPagar) + _paymentOrder.serviceInUSD;

                    EnpadiCardModel.Save(usuario);
                }

                _paymentOrder.monto = TotalAmountMX;
                _paymentOrder.comision = _paymentOrder.comissionMXN;
                _paymentOrder.autorizacion = transaction_folio;
                _paymentOrder.leyenda = instructions;
                _paymentOrder = Tools.AddDisplayFields(_paymentOrder);

                db.Entry(_paymentOrder).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                return _paymentOrder;
            }
            catch (Exception e)
            {
                _paymentOrder.estatus = 2;
                _paymentOrder.codigoRespuesta = 908;
                _paymentOrder.codigoRespuestaDescr = "Servicio temporalmente no disponible." + e.Message;

                return _paymentOrder;
            }
        }

        #region pay exclusive services

        public static pagoServicio PayServiceIMSS(
            Guid currentUserId,
            string sku,
            string name,
            string namelast,
            string namemothers,
            string imss_number,
            string phone_number,
            string cell_phone_number,
            string email,
            string buyer_name,
            string buyer_phone_number,
            string buyer_email,
            Boolean useUserCredit,
            Guid platformId,
            pagoServicio oPago
            )
        {
            try
            {
                ApplicationDbContext db = new ApplicationDbContext();
                Decimal userCurrentBalance = Tools.GetUserBalance(currentUserId);
                Decimal userCurrentAvailableCredit = Tools.GetUserCredit(currentUserId);

                pagoServicio _paymentOrder = db.pagoServicios.Where(x => x.Id == oPago.Id).FirstOrDefault();
                tarjetas usuario = Tools.GetUserEnpadiCard(currentUserId);

                decimal feeChargedByPartner = 0;
                servicios servicio = db.servicios.FirstOrDefault(x => x.SKU == sku);

                String transaction_concept = "Pago IMSS";
                String transaction_description = (sku == servicios.keys.imss_new ? "Pago por contratación I.M.S.S." : "Pago por renovación I.M.S.S.");
                String transaction_folio = "";
                String transaction_reference = "";
                String last_4_digit = usuario.tcNum.Substring(usuario.tcNum.Length - 4);
                DateTime transaction_date = DateTime.Now;

                List<Fee> fees = new List<Fee>();
                if (platformId == PlatformModel.Keys.WebUsa)
                {
                    fees = Fee.GetFeeForService(ApplicationUser.Key.ConsulateId, servicio.Id, platformId);
                }
                else
                {
                    fees = Fee.GetFeeForService(currentUserId, servicio.Id, platformId);
                }

                if (_paymentOrder == null)
                {
                    throw new ArgumentException("Payment order not found.");
                }

                if (_paymentOrder.transaccionesId != null)
                {
                    _paymentOrder = Tools.AddDisplayFields(_paymentOrder);

                    return _paymentOrder;
                }

                if (usuario == null)
                {
                    throw new ArgumentException(SystemErrorModel.ErrorList.UserEnpadiCardNotFound);
                }

                Decimal exchangeRateDown = _paymentOrder.exchageRateDown.GetValueOrDefault();
                Decimal exchangeRateUp = _paymentOrder.exchageRateUp.GetValueOrDefault();
                Decimal ComissionMX = _paymentOrder.comissionMXN.GetValueOrDefault();
                Decimal TotalAmountMX = _paymentOrder.totalMXN.GetValueOrDefault();
                Decimal ComissionUSD = _paymentOrder.comissionUSD.GetValueOrDefault();
                Decimal TotalAmountUSD = _paymentOrder.totalUSD.GetValueOrDefault();
                Decimal AmountForServiceMXN = _paymentOrder.serviceInMXN.GetValueOrDefault();
                Decimal AmountForServiceUSD = _paymentOrder.serviceInUSD.GetValueOrDefault();

                if (useUserCredit == true)
                {
                    if (TotalAmountMX > userCurrentAvailableCredit)
                    {
                        _paymentOrder.estatus = 2;
                        _paymentOrder.codigoRespuesta = 908;
                        _paymentOrder.codigoRespuestaDescr = "Saldo Insuficiente. Recargue su Cuenta";

                        db.Entry(_paymentOrder).Property(x => x.estatus).IsModified = true;
                        db.Entry(_paymentOrder).Property(x => x.codigoRespuesta).IsModified = true;
                        db.Entry(_paymentOrder).Property(x => x.codigoRespuestaDescr).IsModified = true;
                        db.SaveChanges();

                        return _paymentOrder;
                    }
                }
                else if (TotalAmountMX > userCurrentBalance)
                {
                    _paymentOrder.estatus = 2;
                    _paymentOrder.codigoRespuesta = 908;
                    _paymentOrder.codigoRespuestaDescr = "Saldo Insuficiente. Recargue su Cuenta";
                    db.Entry(_paymentOrder).Property(x => x.estatus).IsModified = true;
                    db.Entry(_paymentOrder).Property(x => x.codigoRespuesta).IsModified = true;
                    db.Entry(_paymentOrder).Property(x => x.codigoRespuestaDescr).IsModified = true;
                    db.SaveChanges();

                    return _paymentOrder;
                }

                ImssPaymentsModel imssPayment = new ImssPaymentsModel
                {
                    id = Guid.NewGuid(),
                    amout_pay = AmountForServiceMXN,
                    beneficiary_email_address = email,
                    beneficiary_imss_number = (imss_number == null ? "" : imss_number),
                    beneficiary_name = name,
                    beneficiary_last = namelast,
                    beneficiary_mothers = namemothers,
                    beneficiary_phone_number = phone_number,
                    beneficiary_cell_phone_number = cell_phone_number,
                    buyer_email = usuario.email,
                    buyer_enpadi_card = usuario.tcNum,
                    buyer_enpadi_card_ending = last_4_digit,
                    buyer_name = buyer_name,
                    buyer_phone_number = AspNetUsersModel.Get(null, usuario.email).PhoneNumber,
                    code_base = "P",
                    payment_date_at = transaction_date,
                    remitter_name = "Enpadi"
                };

                //first save to get the transaction reference
                db.ImssPaymentsModel.Add(imssPayment);
                db.Entry(imssPayment).State = System.Data.Entity.EntityState.Added;
                db.SaveChanges();



                //this is done to get the code_number
                Guid imssPaymentId = imssPayment.id;
                imssPayment = db.ImssPaymentsModel.FirstOrDefault(x => x.id == imssPaymentId);

                transaction_reference = imssPayment.code_base + imssPayment.code_number.ToString("D4") + last_4_digit + imssPayment.buyer_name;
                transaction_folio = imssPayment.code_number.ToString("D4");

                string companyName = "Seguro Popular";
                string instructions = "Favor de comunicarse al 01-800-IMSS-SEGURO CON EL FOLIO " + transaction_reference + " y su nombre para activar el seguro.";
                string supplier = "Seguro Popular";



                _paymentOrder = PayServiceWithEnpadi(
                    fees,
                    AmountForServiceMXN,
                    sku,
                    currentUserId,
                    oPago,
                    feeChargedByPartner,
                    platformId,
                    transaction_concept,
                    transaction_description,
                    transaction_folio,
                    transaction_reference,
                    useUserCredit,
                    socio.SocioID.Imss_int_id,
                    companyName,
                    instructions,
                    supplier);

                imssPayment.transaccionesId = _paymentOrder.transaccionesId;
                db.Entry(imssPayment).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                _paymentOrder = Tools.AddDisplayFields(_paymentOrder);

                return _paymentOrder;
            }
            catch (Exception e)
            {
                oPago.estatus = 2;
                oPago.codigoRespuesta = 908;
                oPago.codigoRespuestaDescr = "Servicio temporalmente no disponible." + e.Message;

                return oPago;
            }
        }

        public static pagoServicio PayServiceCemex(
           string currentUserId,
           string sku,
           string name,
           string namelast,
           string namemothers,
           string imss_number,
           string phone_number,
           string cell_phone_number,
           string email,
           string buyer_name,
           string buyer_phone_number,
           string buyer_email,
           Boolean useUserCredit,
           Guid platformId,
           pagoServicio oPago
           )
        {
            try
            {
                ApplicationDbContext db = new ApplicationDbContext();
                Guid _currentUserId = Guid.Parse(currentUserId);
                decimal feeChargedByPartner = 0;

                pagoServicio _paymentOrder = db.pagoServicios.Where(x => x.Id == oPago.Id).FirstOrDefault();
                tarjetas usuario = Tools.GetUserEnpadiCard(currentUserId);

                Decimal userCurrentBalance = Tools.GetUserBalance(currentUserId);
                Decimal userCurrentAvailableCredit = Tools.GetUserCredit(currentUserId);
                Decimal exchangeRateDown = Tools.GetPesoToDollarRate(-1);
                Decimal exchangeRateUp = Tools.GetPesoToDollarRate(1);
                String transaction_concept = "Cemex Card";
                String transaction_description = "Cemex Card";
                String transaction_folio = "";
                String transaction_reference = "";
                String last_4_digit = usuario.tcNum.Substring(usuario.tcNum.Length - 4);
                DateTime transaction_date = DateTime.Now;

                servicios servicio = db.servicios.FirstOrDefault(x => x.SKU == sku);

                List<Fee> fees = new List<Fee>();
                if (platformId == PlatformModel.Keys.WebUsa)
                {
                    fees = Fee.GetFeeForService(ApplicationUser.Key.ConsulateId, servicio.Id, platformId);
                }
                else
                {
                    fees = Fee.GetFeeForService(_currentUserId, servicio.Id, platformId);
                }

                if (_paymentOrder == null)
                {
                    throw new ArgumentException("Payment order not found.");
                }

                if (_paymentOrder.transaccionesId != null)
                {
                    _paymentOrder = Tools.AddDisplayFields(_paymentOrder);

                    return _paymentOrder;
                }

                if (usuario == null)
                {
                    throw new ArgumentException(SystemErrorModel.ErrorList.UserEnpadiCardNotFound);
                }

                Decimal ComissionMX = 0;
                Decimal TotalAmountMX = 0;
                Decimal ComissionUSD = 0;
                Decimal TotalAmountUSD = 0;
                Decimal AmountForServiceMXN = ((Decimal)servicio.Amount) * exchangeRateUp;
                Decimal AmountForServiceUSD = (Decimal)servicio.Amount;

                ComissionMX = Enpadi_WebUI.Models.Fee.GetTotalFeeInMXN(fees, AmountForServiceMXN, exchangeRateUp, platformId, feeChargedByPartner, false);
                ComissionUSD = Enpadi_WebUI.Models.Fee.GetTotalFeeInUSD(fees, AmountForServiceMXN, exchangeRateDown, platformId, feeChargedByPartner, false);

                //get totals
                TotalAmountMX = AmountForServiceMXN + ComissionMX;
                TotalAmountUSD = AmountForServiceUSD + ComissionUSD;

                if (useUserCredit == true)
                {
                    if (TotalAmountMX > userCurrentAvailableCredit)
                    {
                        _paymentOrder.estatus = 2;
                        _paymentOrder.codigoRespuesta = 908;
                        _paymentOrder.codigoRespuestaDescr = "Saldo Insuficiente. Recargue su Cuenta";

                        db.Entry(_paymentOrder).Property(x => x.estatus).IsModified = true;
                        db.Entry(_paymentOrder).Property(x => x.codigoRespuesta).IsModified = true;
                        db.Entry(_paymentOrder).Property(x => x.codigoRespuestaDescr).IsModified = true;
                        db.SaveChanges();

                        return _paymentOrder;
                    }
                }
                else if (TotalAmountMX > userCurrentBalance)
                {
                    _paymentOrder.estatus = 2;
                    _paymentOrder.codigoRespuesta = 908;
                    _paymentOrder.codigoRespuestaDescr = "Saldo Insuficiente. Recargue su Cuenta";

                    db.Entry(_paymentOrder).Property(x => x.estatus).IsModified = true;
                    db.Entry(_paymentOrder).Property(x => x.codigoRespuesta).IsModified = true;
                    db.Entry(_paymentOrder).Property(x => x.codigoRespuestaDescr).IsModified = true;
                    db.SaveChanges();

                    return _paymentOrder;
                }

                CemexPayments cemexPayment = new CemexPayments
                {
                    Id = Guid.NewGuid(),
                    amout_pay = TotalAmountMX,
                    beneficiary_email_address = email,
                    beneficiary_name = name,
                    beneficiary_last = namelast,
                    beneficiary_mothers = namemothers,
                    beneficiary_phone_number = phone_number,
                    beneficiary_cell_phone_number = cell_phone_number,
                    buyer_email = usuario.email,
                    buyer_enpadi_card = usuario.tcNum,
                    buyer_enpadi_card_ending = last_4_digit,
                    buyer_name = usuario.datNombre,
                    buyer_phone_number = AspNetUsersModel.Get(null, usuario.email).PhoneNumber,
                    code_base = "CEMEX",
                    payment_date_at = transaction_date,
                    remitter_name = "Enpadi"
                };


                db.CemexPayments.Add(cemexPayment);
                db.Entry(cemexPayment).State = System.Data.Entity.EntityState.Added;
                db.SaveChanges();

                Guid cemexPaymentId = cemexPayment.Id;
                cemexPayment = db.CemexPayments.FirstOrDefault(x => x.Id == cemexPaymentId);

                transaction_reference = cemexPayment.code_base + cemexPayment.code_number.ToString("D4") + last_4_digit + cemexPayment.payment_date_at.GetValueOrDefault().ToShortDateString();
                transaction_folio = cemexPayment.code_number.ToString("D4");

                string companyName = "Cemex";
                string instructions = "Favor de ir a la tienda Construrama con el Folio " + transaction_reference + " para hacer válida la tarjeta.";
                string supplier = "CEMEX";

                _paymentOrder = PayServiceWithEnpadi(
                    fees,
                    AmountForServiceMXN,
                    sku,
                    _currentUserId,
                    oPago,
                    feeChargedByPartner,
                    platformId,
                    transaction_concept,
                    transaction_description,
                    transaction_folio,
                    transaction_reference,
                    useUserCredit,
                    socio.SocioID.Cemex_int_id,
                    companyName,
                    instructions,
                    supplier);

                cemexPayment.transaccionesId = _paymentOrder.transaccionesId;
                db.Entry(cemexPayment).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                _paymentOrder = Tools.AddDisplayFields(_paymentOrder);

                return _paymentOrder;
            }
            catch (Exception e)
            {
                oPago.estatus = 2;
                oPago.codigoRespuesta = 908;
                oPago.codigoRespuestaDescr = "Servicio temporalmente no disponible." + e.Message;

                return oPago;
            }
        }

        #endregion pay exclusive services

        #endregion pay services

        #region check services indivudual process

        /// <summary>
        /// iclase on display attributes work for web usa = 2, general = 0 or kiosko =1 ticket
        /// </summary>
        /// <param name="sku"></param>
        /// <param name="referencia"></param>
        /// <param name="currentUserId"></param>
        /// <param name="platformId"></param>
        /// <param name="chargeByPartnerAmount"></param>
        /// <returns></returns>
        public static ViewModelProcessIMSS CheckService_imss(
            string sku,
            string referencia,
            string currentUserId,
            Guid platformId,
            decimal chargeByPartnerAmount)
        {

            ApplicationDbContext db = new ApplicationDbContext();
            Guid _currentUserId = Guid.Parse(currentUserId);

            Decimal exchangeRateUp = Tools.GetPesoToDollarRate(1);
            Decimal exchangeRateDown = Tools.GetPesoToDollarRate(-1);

            ViewModelProcessIMSS model = new ViewModelProcessIMSS();

            servicios servicio = db.servicios.FirstOrDefault(x => x.SKU == sku);
            List<Fee> fees = new List<Fee>();
            if (platformId == PlatformModel.Keys.WebUsa)
            {
                fees = Fee.GetFeeForService(ApplicationUser.Key.ConsulateId, servicio.Id, platformId);
            }
            else
            {
                fees = Fee.GetFeeForService(_currentUserId, servicio.Id, platformId);
            }

            String Reference = servicio.Descripcion;
            Decimal AmountForServiceMXN = ((Decimal)servicio.Amount) * exchangeRateUp;
            Decimal AmountForServiceUSD = (Decimal)servicio.Amount;

            Decimal ComissionMX = 0;
            Decimal TotalAmountMX = 0;
            Decimal ComissionUSD = 0;
            Decimal TotalAmountUSD = 0;

            //calculate fees
            ComissionMX = Fee.GetTotalFeeInMXN(fees, AmountForServiceMXN, exchangeRateUp, platformId, chargeByPartnerAmount, false);
            ComissionUSD = Fee.GetTotalFeeInUSD(fees, AmountForServiceMXN, exchangeRateDown, platformId, chargeByPartnerAmount, false);

            //get totals
            TotalAmountMX = AmountForServiceMXN + ComissionMX;
            TotalAmountUSD = AmountForServiceUSD + ComissionUSD;

            servicios service = db.servicios.Where(x => x.SKU == sku).FirstOrDefault();

            if (service == null)
            {
                throw new ArgumentException("Service not found");
            }

            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            if (usuario == null)
            {
                throw new ArgumentException(SystemErrorModel.ErrorList.UserEnpadiCardNotFound);
            }

            pagoServicio oPago = new pagoServicio()
            {
                tarjetaID = usuario.tarjetaID,
                titular = String.Format("{0} {1} {2}", usuario.datNombre, usuario.datPaterno, usuario.datMaterno),
                sku = sku,
                referencia = referencia,
                tipoPago = "MELE",
                nombre = service.Nombre,
                compañia = service.Compañia,
                imagenCompañia = service.imagenCompañia,
                tipo = service.Tipo,
                imagenTipo = service.imagenTipo,
                estatus = Estatus.db_keys.payment_pending
            };

            oPago.fechaLocal = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
            oPago.horaLocal = String.Format("{0:HH:mm:ss}", DateTime.Now);
            oPago.fechaContable = String.Format("{0:dd/MM/yyyy}", DateTime.Now);

            oPago.exchageRateUp = exchangeRateUp;
            oPago.exchageRateDown = exchangeRateDown;

            oPago.monto = AmountForServiceMXN;
            oPago.comision = ComissionMX;

            oPago.totalMXN = TotalAmountMX;
            oPago.totalUSD = TotalAmountUSD;
            oPago.comissionMXN = ComissionMX;
            oPago.comissionUSD = ComissionUSD;
            oPago.serviceInMXN = AmountForServiceMXN;
            oPago.serviceInUSD = AmountForServiceUSD;
            oPago.referencia = Reference;

            db.pagoServicios.Add(oPago);
            db.Entry(oPago).State = System.Data.Entity.EntityState.Added;
            db.SaveChanges();


            oPago = Tools.AddDisplayFields(oPago);

            model.DisplayFields = oPago.DisplayFields;

            if (sku == servicios.keys.imss_new)
            {
                model.new_registry = true;
                model.sku_service = servicios.keys.imss_new;
            }
            else
            {
                model.new_registry = false;
                model.sku_service = servicios.keys.imss_existing;
            }

            model.opagoId = oPago.Id;

            return model;
        }

        /// <summary>
        /// iclase on display attributes work for web usa = 2, general = 0 or kiosko =1 ticket
        /// </summary>
        /// <param name="sku"></param>
        /// <param name="referencia"></param>
        /// <param name="currentUserId"></param>
        /// <param name="platformId"></param>
        /// <param name="chargeByPartnerAmount"></param>
        /// <returns></returns>
        public static ViewModelProcessCemex CheckService_cemex(
            string sku,
            string referencia,
            string currentUserId,
            Guid platformId,
            decimal chargeByPartnerAmount)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            Guid _currentUserId = Guid.Parse(currentUserId);

            Decimal exchangeRateUp = Tools.GetPesoToDollarRate(1);
            Decimal exchangeRateDown = Tools.GetPesoToDollarRate(-1);

            ViewModelProcessCemex model = new ViewModelProcessCemex();
            servicios servicio = db.servicios.FirstOrDefault(x => x.SKU == sku);

            String Reference = servicio.Descripcion;
            Decimal AmountForServiceMXN = ((Decimal)servicio.Amount) * exchangeRateUp;
            Decimal AmountForServiceUSD = (Decimal)servicio.Amount;

            Decimal ComissionMX = 0;
            Decimal TotalAmountMX = 0;
            Decimal ComissionUSD = 0;
            Decimal TotalAmountUSD = 0;

            List<Fee> fees = new List<Fee>();
            if (platformId == PlatformModel.Keys.WebUsa)
            {
                fees = Fee.GetFeeForService(ApplicationUser.Key.ConsulateId, servicio.Id, platformId);
            }
            else
            {
                fees = Fee.GetFeeForService(_currentUserId, servicio.Id, platformId);
            }

            //calculate fees
            ComissionMX = Fee.GetTotalFeeInMXN(fees, AmountForServiceMXN, exchangeRateUp, platformId, chargeByPartnerAmount, false);
            ComissionUSD = Fee.GetTotalFeeInUSD(fees, AmountForServiceMXN, exchangeRateDown, platformId, chargeByPartnerAmount, false);

            //get totals
            TotalAmountMX = AmountForServiceMXN + ComissionMX;
            TotalAmountUSD = AmountForServiceUSD + ComissionUSD;

            servicios service = db.servicios.Where(x => x.SKU == sku).FirstOrDefault();

            if (service == null)
            {
                throw new ArgumentException("Service not found");
            }

            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            if (usuario == null)
            {
                throw new ArgumentException(SystemErrorModel.ErrorList.UserEnpadiCardNotFound);
            }

            pagoServicio oPago = new pagoServicio()
            {
                tarjetaID = usuario.tarjetaID,
                titular = String.Format("{0} {1} {2}", usuario.datNombre, usuario.datPaterno, usuario.datMaterno),
                sku = sku,
                referencia = referencia,
                tipoPago = "MELE",
                nombre = service.Nombre,
                compañia = service.Compañia,
                imagenCompañia = service.imagenCompañia,
                tipo = service.Tipo,
                imagenTipo = service.imagenTipo,
                estatus = Estatus.db_keys.payment_pending
            };

            oPago.fechaLocal = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
            oPago.horaLocal = String.Format("{0:HH:mm:ss}", DateTime.Now);
            oPago.fechaContable = String.Format("{0:dd/MM/yyyy}", DateTime.Now);

            oPago.exchageRateUp = exchangeRateUp;
            oPago.exchageRateDown = exchangeRateDown;

            oPago.monto = AmountForServiceMXN;
            oPago.comision = ComissionMX;

            oPago.totalMXN = TotalAmountMX;
            oPago.totalUSD = TotalAmountUSD;
            oPago.comissionMXN = ComissionMX;
            oPago.comissionUSD = ComissionUSD;
            oPago.serviceInMXN = AmountForServiceMXN;
            oPago.serviceInUSD = AmountForServiceUSD;
            oPago.referencia = Reference;

            db.pagoServicios.Add(oPago);
            db.Entry(oPago).State = System.Data.Entity.EntityState.Added;
            db.SaveChanges();

            oPago = Tools.AddDisplayFields(oPago);

            model.DisplayFields = oPago.DisplayFields;

            model.opagoId = oPago.Id;

            return model;
        }

        //don't confuse with payservicewithdiestel
        /// <summary>
        /// iclase on display attributes work for web usa = 2, general = 0 or kiosko =1 ticket
        /// </summary>
        /// <param name="sku"></param>
        /// <param name="referencia"></param>
        /// <param name="currentUserId"></param>
        /// <param name="platformId"></param>
        /// <param name="chargeByPartnerAmount"></param>
        /// <returns></returns>
        public static pagoServicio CheckService_diestel(
            string sku,
            string referencia,
            string currentUserId,
            Guid platformId,
            decimal chargeByPartnerAmount)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            Guid _currentUserId = Guid.Parse(currentUserId);

            Decimal exchangeRateUp = Tools.GetPesoToDollarRate(1);
            Decimal exchangeRateDown = Tools.GetPesoToDollarRate(-1);

            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            IEnumerable<servicios> s = db.servicios.Where(x => x.SKU == sku).Where(x => x.Activo == true).Where(x => x.FechaBaja == null).OrderBy(x => x.Nombre);

            pagoServicio oPago = new pagoServicio()
            {
                tarjetaID = usuario.tarjetaID,
                titular = String.Format("{0} {1} {2}", usuario.datNombre, usuario.datPaterno, usuario.datMaterno),
                sku = sku,
                referencia = referencia,
                tipoPago = "MELE",
                nombre = s.FirstOrDefault().Nombre,
                compañia = s.FirstOrDefault().Compañia,
                imagenCompañia = s.FirstOrDefault().imagenCompañia,
                tipo = s.FirstOrDefault().Tipo,
                imagenTipo = s.FirstOrDefault().imagenTipo,
                estatus = Estatus.db_keys.payment_pending
            };

            oPago.fechaLocal = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
            oPago.horaLocal = String.Format("{0:HH:mm:ss}", DateTime.Now);
            oPago.fechaContable = String.Format("{0:dd/MM/yyyy}", DateTime.Now);

            oPago.exchageRateUp = exchangeRateUp;
            oPago.exchageRateDown = exchangeRateDown;

            db.pagoServicios.Add(oPago);
            db.Entry(oPago).State = System.Data.Entity.EntityState.Added;
            db.SaveChanges();

            PxUniversal ws = new PxUniversal() { Url = DIESTEL_URL, Timeout = 45000 };
            NetworkCredential oCredenciales = new NetworkCredential() { UserName = DIESTEL_USUARIO, Password = DIESTEL_PASSWORD };

            cCampo[] req = new cCampo[11];
            req[0] = new cCampo() { sCampo = "IDGRUPO", sValor = DIESTEL_GRUPO };
            req[1] = new cCampo() { sCampo = "IDCADENA", sValor = DIESTEL_CADENA };
            req[2] = new cCampo() { sCampo = "IDTIENDA", sValor = DIESTEL_TIENDA };
            req[3] = new cCampo() { sCampo = "IDPOS", sValor = 1 };
            req[4] = new cCampo() { sCampo = "IDCAJERO", sValor = 1 };
            req[5] = new cCampo() { sCampo = "FECHALOCAL", sValor = oPago.fechaLocal };
            req[6] = new cCampo() { sCampo = "HORALOCAL", sValor = oPago.horaLocal };
            req[7] = new cCampo() { sCampo = "SKU", sValor = sku };
            req[8] = new cCampo() { sCampo = "FECHACONTABLE", sValor = oPago.fechaContable };
            req[9] = new cCampo() { sCampo = "REFERENCIA", sValor = referencia };

            cCampo[] resp;

            req[10] = new cCampo() { sCampo = "TRANSACCION", sValor = oPago.pagoID };

            ws.Credentials = oCredenciales;
            resp = ws.Info(req);

            //TODO: this is imporant, nedd to catch all codes and make a log in systems errors
            if (((Enpadi_WebUI.WSDiestel.cCampo[])resp)[0].sCampo.ToString() != "CODIGORESPUESTA")
            {
                Decimal dMonto = Decimal.Parse(resp.First(x => x.sCampo == "MONTO").sValor.ToString());

                String Reference = resp.First(x => x.sCampo == "REFERENCIA").sValor.ToString();
                Decimal AmountForServiceMXN = dMonto;
                Decimal AmountForServiceUSD = dMonto / exchangeRateDown;
                servicios servicio = db.servicios.FirstOrDefault(x => x.SKU == oPago.sku);

                Decimal ComissionMX = 0;
                Decimal TotalAmountMX = 0;
                Decimal ComissionUSD = 0;
                Decimal TotalAmountUSD = 0;


                //get fees
                List<Fee> fees = new List<Fee>();
                if (platformId == PlatformModel.Keys.WebUsa)
                {
                    fees = Fee.GetFeeForService(ApplicationUser.Key.ConsulateId, servicio.Id, platformId);
                }
                else
                {
                    fees = Fee.GetFeeForService(_currentUserId, servicio.Id, platformId);
                }
                //calculate fees
                ComissionMX = Fee.GetTotalFeeInMXN(fees, AmountForServiceMXN, exchangeRateUp, platformId, chargeByPartnerAmount, false);
                ComissionUSD = Fee.GetTotalFeeInUSD(fees, AmountForServiceMXN, exchangeRateDown, platformId, chargeByPartnerAmount, false);

                //calculate totals
                TotalAmountMX = AmountForServiceMXN + ComissionMX;
                TotalAmountUSD = (AmountForServiceMXN / exchangeRateDown) + ComissionUSD;

                //create registry and supply input from supplier and enpadi
                oPago.Campos = resp;
                oPago.monto = AmountForServiceMXN;
                oPago.comision = ComissionMX;

                oPago.supplierSendReference = referencia;
                oPago.supplierRecivedReference = resp.First(x => x.sCampo == "REFERENCIA").sValor.ToString();
                oPago.supplierFeeInMXN = Decimal.Parse(resp.First(x => x.sCampo == "COMISION").sValor.ToString());
                oPago.supplierAmountForServiceInMXN = Decimal.Parse(resp.First(x => x.sCampo == "MONTO").sValor.ToString());
                oPago.supplierTotal = oPago.supplierAmountForServiceInMXN + oPago.supplierFeeInMXN;


                oPago.totalMXN = TotalAmountMX;
                oPago.totalUSD = TotalAmountUSD;
                oPago.comissionMXN = ComissionMX;
                oPago.comissionUSD = ComissionUSD;
                oPago.serviceInMXN = AmountForServiceMXN;
                oPago.serviceInUSD = AmountForServiceUSD;

                oPago = Tools.AddDisplayFields(oPago);

                db.Entry(oPago).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                throw new ArgumentException("Servicio temporalmente suspendido");
            }

            return oPago;
        }

        #endregion check services indivudual process

        #region printing services

        private static cCampo[] GetDisplayField(pagoServicio oPago, trans transaction, Boolean legacy)
        {

            Decimal ComissionMX = 0;
            Decimal TotalAmountMX = 0;
            Decimal ComissionUSD = 0;
            Decimal TotalAmountUSD = 0;
            Decimal AmountForServiceMXN = 0;
            Decimal AmountForServiceUSD = 0;
            Decimal exchangeRateDown = 0;
            Decimal exchangeRateUp = 0;

            if (legacy == false)
            {
                ComissionMX = transaction.ComissionMX.GetValueOrDefault();
                TotalAmountMX = transaction.TotalMX.GetValueOrDefault();
                ComissionUSD = transaction.ComissionUSD.GetValueOrDefault();
                TotalAmountUSD = transaction.TotalUSD.GetValueOrDefault();
                AmountForServiceMXN = transaction.ServiceInMXN.GetValueOrDefault();
                AmountForServiceUSD = transaction.ServiceInDlls.GetValueOrDefault();
                exchangeRateDown = transaction.ExchangeRateDown.GetValueOrDefault();
                exchangeRateDown = transaction.ExchangeRateUp.GetValueOrDefault();
            }
            else
            {

                exchangeRateDown = Tools.GetPesoToDollarRate(-1);
                exchangeRateDown = Tools.GetPesoToDollarRate(+1);

                ComissionMX = transaction.cargo1 + transaction.cargo1iva + transaction.cargo2 + transaction.cargo2iva + transaction.cargo3 + transaction.cargo3iva + transaction.cargo4 + transaction.cargo4iva + transaction.cargo5 + transaction.cargo5iva + transaction.cargo5iva + transaction.cargo6 + transaction.cargo6iva;
                TotalAmountMX = (transaction.abonoTotal == 0 ? transaction.cargo1 + transaction.cargo1iva : transaction.abono1 + transaction.abono1iva);
                ComissionUSD = ComissionMX / exchangeRateDown;
                TotalAmountUSD = TotalAmountMX / exchangeRateDown;
                AmountForServiceMXN = TotalAmountMX + ComissionMX;
                AmountForServiceUSD = AmountForServiceMXN / exchangeRateDown;
            }


            cCampo[] displayAttributes = new cCampo[9];
            cCampo displayReference = new cCampo
            {
                sCampo = "<label class='control-label col-md-4 lang' key='processcheckservice.displayReference'></label>",
                sValor = oPago.referencia,
                iClase = 0,
            };

            cCampo displayAmountForServiceMX = new cCampo
            {
                sCampo = "<label class='control-label col-md-4 lang' key='processcheckservice.displayAmountForServiceMX'></label>",
                sValor = AmountForServiceMXN.ToString("c"),
                iClase = 2,
            };

            cCampo displayComissionMX = new cCampo
            {
                sCampo = "<label class='control-label col-md-4 lang' key='processcheckservice.displayComissionMX'></label>",
                sValor = ComissionMX.ToString("C"),
                iClase = 3,
            };

            cCampo displayTotalAmountMX = new cCampo
            {
                sCampo = "<label class='control-label col-md-4 lang' key='processcheckservice.displayTotalAmountMX'></label>",
                sValor = TotalAmountMX.ToString("C"),
                iClase = 2,
            };

            cCampo displayAmountForServiceDlls = new cCampo
            {
                sCampo = "<label class='control-label col-md-4 lang' key='processcheckservice.displayAmountForServiceDlls'></label>",
                sValor = AmountForServiceUSD.ToString("c"),
                iClase = 1,
            };

            cCampo displayComissionUSD = new cCampo
            {
                sCampo = "<label class='control-label col-md-4 lang' key='processcheckservice.displayComissionUSD'></label>",
                sValor = ComissionUSD.ToString("c"),
                iClase = 0,
            };

            cCampo displayTotalAmountUSD = new cCampo
            {
                sCampo = "<label class='control-label col-md-4 lang' key='processcheckservice.displayTotalAmountUSD'></label>",
                sValor = TotalAmountUSD.ToString("C"),
                iClase = 1,
            };

            cCampo exchangeRateToReportDown = new cCampo
            {
                sCampo = "<label class='control-label col-md-4 lang' key='processcheckservice.exchangeRateToReportDown'></label>",
                sValor = (exchangeRateDown).ToString("C"),
                iClase = 3,
            };
            cCampo exchangeRateToReportUp = new cCampo
            {
                sCampo = "<label class='control-label col-md-4 lang' key='processcheckservice.exchangeRateToReportUp'></label>",
                sValor = (exchangeRateUp).ToString("C"),
                iClase = 3,
            };
            displayAttributes[0] = displayReference;
            displayAttributes[1] = displayAmountForServiceMX;
            displayAttributes[2] = displayAmountForServiceDlls;
            displayAttributes[3] = displayComissionUSD;
            displayAttributes[4] = displayTotalAmountUSD;
            displayAttributes[5] = displayTotalAmountMX;
            displayAttributes[6] = displayComissionMX;
            displayAttributes[7] = exchangeRateToReportDown;
            displayAttributes[8] = exchangeRateToReportUp;

            return displayAttributes;
        }


        /// <summary>
        /// iclase on display attributes work for web usa = 2, general = 0 or kiosko =1 ticket   
        /// </summary>
        /// <param name="oPago"></param>
        /// <param name="currentUserId"></param>
        /// <param name="useSendPagoServicios"></param>
        /// <returns></returns>
        public static pagoServicio ServiciosImpresion(pagoServicio oPago, string currentUserId, Boolean useSendPagoServicios = false)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            Guid _currentUserId = Guid.Parse(currentUserId);
            pagoServicio _oPago = oPago;


            if (useSendPagoServicios == false)
            {
                _oPago = db.pagoServicios.Where(x => x.Id == oPago.Id).FirstOrDefault();

                _oPago = Tools.AddDisplayFields(_oPago);
            }
            else
            {
                trans transaction;
                transaction = db.transacciones.Where(x => x.Id == _oPago.transaccionesId).FirstOrDefault();
                if (transaction != null)
                {
                    _oPago.exchageRateDown = Tools.GetPesoToDollarRate(-1);
                    _oPago.exchageRateUp = Tools.GetPesoToDollarRate(+1);

                    _oPago.comissionMXN = transaction.cargo1 + transaction.cargo1iva + transaction.cargo2 + transaction.cargo2iva + transaction.cargo3 + transaction.cargo3iva + transaction.cargo4 + transaction.cargo4iva + transaction.cargo5 + transaction.cargo5iva + transaction.cargo5iva + transaction.cargo6 + transaction.cargo6iva;
                    _oPago.totalMXN = (transaction.abonoTotal == 0 ? transaction.cargo1 + transaction.cargo1iva : transaction.abono1 + transaction.abono1iva);
                    _oPago.comissionUSD = _oPago.comissionMXN / _oPago.exchageRateDown;
                    _oPago.totalUSD = _oPago.totalMXN / _oPago.exchageRateDown;
                    _oPago.serviceInMXN = _oPago.totalMXN + _oPago.comissionMXN;
                    _oPago.serviceInUSD = _oPago.serviceInMXN / _oPago.exchageRateDown;

                    _oPago = Tools.AddDisplayFields(_oPago);
                }
            }

            return _oPago;
        }

        /// <summary>
        /// 0 - attributes for all transactions
        /// 1 - dlls transactions
        /// 2 - mxn transactions (need to add always type 5 too)
        /// 3 - special atributes for calculation only
        /// 4 - imss and cemex attributes (need to add always type 5 too)
        /// 5 - attributes shared in imss, cemex and mxn transactions
        /// </summary>
        /// <param name="oPago"></param>
        /// <returns></returns>
        public static pagoServicio AddDisplayFields(pagoServicio oPago)
        {
            //add display information to opago for displaying ticket on enpado site
            ApplicationDbContext db = new ApplicationDbContext();
            string reference = (oPago.supplierRecivedReference == null ? oPago.referencia : oPago.supplierRecivedReference);
            Decimal amountToDisplayInMXN = (oPago.supplierAmountForServiceInMXN == null ? oPago.serviceInMXN.GetValueOrDefault() : oPago.supplierAmountForServiceInMXN.GetValueOrDefault());
            string _beneficiary = "N/A";
            string _buyer = "N/A";

            if (oPago.transaccionesId != null)
            {
                trans transaction = db.transacciones.FirstOrDefault(x => x.Id == oPago.transaccionesId);
                Guid _transactionId = transaction.Id;


                if (transaction.socioID == 11)
                {
                    ImssPaymentsModel imssPayment = db.ImssPaymentsModel.Where(x => x.transaccionesId == oPago.transaccionesId).FirstOrDefault();
                    if (imssPayment != null)
                    {
                        _beneficiary = imssPayment.beneficiary_name + " " + imssPayment.beneficiary_last + " " + imssPayment.beneficiary_mothers + " - " + imssPayment.beneficiary_phone_number;
                        _buyer = imssPayment.buyer_name + " - " + imssPayment.beneficiary_cell_phone_number;
                    }
                }
                else if (transaction.socioID == 12)
                {
                    CemexPayments cemexPayment = db.CemexPayments.Where(x => x.transaccionesId == oPago.transaccionesId).FirstOrDefault();
                    if (cemexPayment != null)
                    {
                        _beneficiary = cemexPayment.beneficiary_name + " " + cemexPayment.beneficiary_last + " " + cemexPayment.beneficiary_mothers + " - " + cemexPayment.beneficiary_phone_number;
                        _buyer = cemexPayment.buyer_name + " - " + cemexPayment.beneficiary_cell_phone_number;
                    }
                }
                else
                {
                    _beneficiary = oPago.referencia;
                }
            }

            cCampo[] displayAttributes = new cCampo[11];

            cCampo displayBeneficiary = new cCampo
            {
                sCampo = "<label class='control-label col-md-4 lang' key='processcheckservice.beneficiary'></label>",
                sValor = _beneficiary,
                iClase = 4,
            };
            cCampo displayBuyer = new cCampo
            {
                sCampo = "<label class='control-label col-md-4 lang' key='processcheckservice.buyer'></label>",
                sValor = _buyer,
                iClase = 4,
            };
            cCampo displayReference = new cCampo
            {
                sCampo = "<label class='control-label col-md-4 lang' key='processcheckservice.displayReference'></label>",
                sValor = oPago.referencia,
                iClase = 0,
            };

            cCampo displayAmountForServiceMX = new cCampo
            {
                sCampo = "<label class='control-label col-md-4 lang' key='processcheckservice.displayAmountForServiceMX'></label>",
                sValor = oPago.serviceInMXN.GetValueOrDefault().ToString("c"),
                iClase = 2,
            };

            cCampo displayComissionMX = new cCampo
            {
                sCampo = "<label class='control-label col-md-4 lang' key='processcheckservice.displayComissionMX'></label>",
                sValor = oPago.comissionMXN.GetValueOrDefault().ToString("C"),
                iClase = 3,
            };

            cCampo displayTotalAmountMX = new cCampo
            {
                sCampo = "<label class='control-label col-md-4 lang' key='processcheckservice.displayTotalAmountMX'></label>",
                sValor = oPago.totalMXN.GetValueOrDefault().ToString("C"),
                iClase = 5,
            };

            cCampo displayAmountForServiceDlls = new cCampo
            {
                sCampo = "<label class='control-label col-md-4 lang' key='processcheckservice.displayAmountForServiceDlls'></label>",
                sValor = oPago.serviceInUSD.GetValueOrDefault().ToString("c"),
                iClase = 1,
            };

            cCampo displayComissionUSD = new cCampo
            {
                sCampo = "<label class='control-label col-md-4 lang' key='processcheckservice.displayComissionUSD'></label>",
                sValor = oPago.comissionUSD.GetValueOrDefault().ToString("c"),
                iClase = 0,
            };

            cCampo displayTotalAmountUSD = new cCampo
            {
                sCampo = "<label class='control-label col-md-4 lang' key='processcheckservice.displayTotalAmountUSD'></label>",
                sValor = oPago.totalUSD.GetValueOrDefault().ToString("C"),
                iClase = 1,
            };

            cCampo exchangeRateToReportDown = new cCampo
            {
                sCampo = "<label class='control-label col-md-4 lang' key='processcheckservice.exchangeRateToReportDown'></label>",
                sValor = (oPago.exchageRateDown.GetValueOrDefault()).ToString("C"),
                iClase = 3,
            };
            cCampo exchangeRateToReportUp = new cCampo
            {
                sCampo = "<label class='control-label col-md-4 lang' key='processcheckservice.exchangeRateToReportUp'></label>",
                sValor = (oPago.exchageRateUp.GetValueOrDefault()).ToString("C"),
                iClase = 3,
            };

            displayAttributes[0] = displayReference;
            displayAttributes[1] = displayAmountForServiceMX;
            displayAttributes[2] = displayAmountForServiceDlls;
            displayAttributes[3] = displayComissionUSD;
            displayAttributes[4] = displayTotalAmountUSD;
            displayAttributes[5] = displayTotalAmountMX;
            displayAttributes[6] = displayComissionMX;
            displayAttributes[7] = exchangeRateToReportDown;
            displayAttributes[8] = exchangeRateToReportUp;
            displayAttributes[9] = displayBeneficiary;
            displayAttributes[10] = displayBuyer;

            oPago.DisplayFields = displayAttributes;

            cCampo[] opagoCampos = new cCampo[5];
            cCampo cctransaction = new cCampo
            {
                bEncriptado = false,
                iClase = 6,
                sCampo = "TRANSACCION",
                sValor = oPago.Id.ToString()
            };
            cCampo ccautorizacion = new cCampo
            {
                bEncriptado = false,
                iClase = 6,
                sCampo = "AUTORIZACION",
                sValor = oPago.autorizacion
            };
            cCampo ccreferencia = new cCampo
            {
                bEncriptado = false,
                iClase = 6,
                sCampo = "REFERENCIA",
                sValor = oPago.referencia
            };
            cCampo ccleyenda = new cCampo
            {
                bEncriptado = false,
                iClase = 6,
                sCampo = "LEYENDA",
                sValor = oPago.leyenda
            };

            cCampo ccprovedore = new cCampo
            {
                bEncriptado = false,
                iClase = 6,
                sCampo = "PROVEEDOR",
                sValor = oPago.proveedor
            };

            opagoCampos[0] = cctransaction;
            opagoCampos[1] = ccprovedore;
            opagoCampos[2] = ccautorizacion;
            opagoCampos[3] = ccreferencia;
            opagoCampos[4] = ccleyenda;

            oPago.Campos = opagoCampos;

            return oPago;
        }

        #endregion printing services

        #region load elements

        public static ViewModelInicio LoadChards(ViewModelInicio vm, Int32 periodo, tarjetas usuario)
        {

            ApplicationDbContext db = new ApplicationDbContext();
            string strSQL = string.Empty;
            int tID = usuario.tarjetaID;

            // MONTOS DIARIOS POR METODO DE PAGO | Visa/MasterCard
            strSQL = " SELECT Dia, Socio, DATEADD(DD, Dia-1, DATEADD(MM, DATEDIFF(MM, 0, GETDATE()), 0)) As Fecha, Mes, COALESCE(Periodo,Dia) As Periodo, COALESCE(Transacciones,0) As Transacciones, COALESCE(Abono,0) As Abono, COALESCE(Cargo,0) As Cargo ";
            strSQL += " FROM Dias d ";
            strSQL += " LEFT JOIN ( ";
            strSQL += "   SELECT s.nombreComercial As Socio, CAST(FLOOR(CAST(t.fechaRegistro as float)) as DATETIME) As Fecha, CONVERT(VARCHAR, DATEPART(MONTH, t.FechaRegistro)) As Mes, DATEPART(DAY, t.fechaRegistro) As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(t.abonoTotal),0) As Abono, COALESCE(SUM(t.cargoTotal * -1),0) As Cargo ";
            strSQL += "   FROM Socios s ";
            strSQL += "   LEFT JOIN Transacciones t ON s.socioID = t.socioID ";
            strSQL += "   WHERE t.fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), t.FechaRegistro) = " + vm.periodo.ToString() + " AND s.socioID = 1 AND t.tarjetaID = " + tID.ToString();
            strSQL += "   GROUP BY s.socioID, s.nombreComercial, CAST(FLOOR(CAST(t.fechaRegistro AS float)) as DATETIME), DATEPART(MONTH, t.FechaRegistro), DATEPART(DAY, t.fechaRegistro) ";
            strSQL += " ) As data ON d.dia = data.Periodo ";
            strSQL += " WHERE Dia <= DATEPART(DD, EOMONTH(DATEADD(MONTH," + vm.periodo.ToString() + ", GETDATE()))) ";
            strSQL += " ORDER BY Dia ";
            IEnumerable<resumen> visamc = db.Database.SqlQuery<resumen>(strSQL);
            vm.VisaMC = visamc.ToList();

            // MONTOS DIARIOS POR METODO DE PAGO | American Express
            strSQL = " SELECT Dia, Socio, DATEADD(DD, Dia-1, DATEADD(MM, DATEDIFF(MM, 0, GETDATE()), 0)) As Fecha, Mes, COALESCE(Periodo,Dia) As Periodo, COALESCE(Transacciones,0) As Transacciones, COALESCE(Abono,0) As Abono, COALESCE(Cargo,0) As Cargo ";
            strSQL += " FROM Dias d ";
            strSQL += " LEFT JOIN ( ";
            strSQL += "   SELECT s.nombreComercial As Socio, CAST(FLOOR(CAST(t.fechaRegistro as float)) as DATETIME) As Fecha, CONVERT(VARCHAR, DATEPART(MONTH, t.FechaRegistro)) As Mes, DATEPART(DAY, t.fechaRegistro) As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(t.abonoTotal),0) As Abono, COALESCE(SUM(t.cargoTotal * -1),0) As Cargo ";
            strSQL += " 	FROM Socios s ";
            strSQL += " 	LEFT JOIN Transacciones t ON s.socioID = t.socioID ";
            strSQL += " 	WHERE t.fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), t.FechaRegistro) = " + vm.periodo.ToString() + " AND s.socioID = 2 AND t.tarjetaID = " + tID.ToString();
            strSQL += " 	GROUP BY s.socioID, s.nombreComercial, CAST(FLOOR(CAST(t.fechaRegistro AS float)) as DATETIME), DATEPART(MONTH, t.FechaRegistro), DATEPART(DAY, t.fechaRegistro) ";
            strSQL += " ) As data ON d.dia = data.Periodo ";
            strSQL += " WHERE Dia <= DATEPART(DD, EOMONTH(DATEADD(MONTH," + vm.periodo.ToString() + ", GETDATE()))) ";
            strSQL += " ORDER BY Dia ";
            IEnumerable<resumen> amex = db.Database.SqlQuery<resumen>(strSQL);
            vm.Amex = amex.ToList();

            // MONTOS DIARIOS POR METODO DE PAGO | Banco
            strSQL = " SELECT Dia, Socio, DATEADD(DD, Dia-1, DATEADD(MM, DATEDIFF(MM, 0, GETDATE()), 0)) As Fecha, Mes, COALESCE(Periodo,Dia) As Periodo, COALESCE(Transacciones,0) As Transacciones, COALESCE(Abono,0) As Abono, COALESCE(Cargo,0) As Cargo ";
            strSQL += " FROM Dias d ";
            strSQL += " LEFT JOIN ( ";
            strSQL += "   SELECT s.nombreComercial As Socio, CAST(FLOOR(CAST(t.fechaRegistro as float)) as DATETIME) As Fecha, CONVERT(VARCHAR, DATEPART(MONTH, t.FechaRegistro)) As Mes, DATEPART(DAY, t.fechaRegistro) As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(t.abonoTotal),0) As Abono, COALESCE(SUM(t.cargoTotal * -1),0) As Cargo ";
            strSQL += "   FROM Socios s ";
            strSQL += "   LEFT JOIN Transacciones t ON s.socioID = t.socioID ";
            strSQL += "   WHERE t.fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), t.FechaRegistro) = " + vm.periodo.ToString() + " AND s.socioID = 3 AND t.tarjetaID = " + tID.ToString();
            strSQL += "   GROUP BY s.socioID, s.nombreComercial, CAST(FLOOR(CAST(t.fechaRegistro AS float)) as DATETIME), DATEPART(MONTH, t.FechaRegistro), DATEPART(DAY, t.fechaRegistro) ";
            strSQL += " ) As data ON d.dia = data.Periodo ";
            strSQL += " WHERE Dia <= DATEPART(DD, EOMONTH(DATEADD(MONTH," + vm.periodo.ToString() + ", GETDATE()))) ";
            strSQL += " ORDER BY Dia ";
            IEnumerable<resumen> banco = db.Database.SqlQuery<resumen>(strSQL);
            vm.Banco = banco.ToList();

            // MONTOS DIARIOS POR METODO DE PAGO | Tienda
            strSQL = " SELECT Dia, Socio, DATEADD(DD, Dia-1, DATEADD(MM, DATEDIFF(MM, 0, GETDATE()), 0)) As Fecha, Mes, COALESCE(Periodo,Dia) As Periodo, COALESCE(Transacciones,0) As Transacciones, COALESCE(Abono,0) As Abono, COALESCE(Cargo,0) As Cargo ";
            strSQL += " FROM Dias d ";
            strSQL += " LEFT JOIN ( ";
            strSQL += "   SELECT s.nombreComercial As Socio, CAST(FLOOR(CAST(t.fechaRegistro as float)) as DATETIME) As Fecha, CONVERT(VARCHAR, DATEPART(MONTH, t.FechaRegistro)) As Mes, DATEPART(DAY, t.fechaRegistro) As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(t.abonoTotal),0) As Abono, COALESCE(SUM(t.cargoTotal * -1),0) As Cargo ";
            strSQL += "   FROM Socios s ";
            strSQL += "   LEFT JOIN Transacciones t ON s.socioID = t.socioID ";
            strSQL += "   WHERE t.fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), t.FechaRegistro) = " + vm.periodo.ToString() + " AND s.socioID = 4 AND t.tarjetaID = " + tID.ToString();
            strSQL += "   GROUP BY s.socioID, s.nombreComercial, CAST(FLOOR(CAST(t.fechaRegistro AS float)) as DATETIME), DATEPART(MONTH, t.FechaRegistro), DATEPART(DAY, t.fechaRegistro) ";
            strSQL += " ) As data ON d.dia = data.Periodo ";
            strSQL += " WHERE Dia <= DATEPART(DD, EOMONTH(DATEADD(MONTH," + vm.periodo.ToString() + ", GETDATE()))) ";
            strSQL += " ORDER BY Dia ";
            IEnumerable<resumen> tienda = db.Database.SqlQuery<resumen>(strSQL);
            vm.Tienda = tienda.ToList();

            // MONTOS DIARIOS POR METODO DE PAGO | Tiempo Aire
            strSQL = " SELECT Dia, Socio, DATEADD(DD, Dia-1, DATEADD(MM, DATEDIFF(MM, 0, GETDATE()), 0)) As Fecha, Mes, COALESCE(Periodo,Dia) As Periodo, COALESCE(Transacciones,0) As Transacciones, COALESCE(Abono,0) As Abono, COALESCE(Cargo,0) As Cargo ";
            strSQL += " FROM Dias d ";
            strSQL += " LEFT JOIN ( ";
            strSQL += "   SELECT s.nombreComercial As Socio, CAST(FLOOR(CAST(t.fechaRegistro as float)) as DATETIME) As Fecha, CONVERT(VARCHAR, DATEPART(MONTH, t.FechaRegistro)) As Mes, DATEPART(DAY, t.fechaRegistro) As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(t.abonoTotal),0) As Abono, COALESCE(SUM(t.cargoTotal * -1),0) As Cargo ";
            strSQL += "   FROM Socios s ";
            strSQL += "   LEFT JOIN Transacciones t ON s.socioID = t.socioID ";
            strSQL += "   WHERE t.fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), t.FechaRegistro) = " + vm.periodo.ToString() + " AND s.socioID = 5 AND t.tarjetaID = " + tID.ToString();
            strSQL += "   GROUP BY s.socioID, s.nombreComercial, CAST(FLOOR(CAST(t.fechaRegistro AS float)) as DATETIME), DATEPART(MONTH, t.FechaRegistro), DATEPART(DAY, t.fechaRegistro) ";
            strSQL += " ) As data ON d.dia = data.Periodo ";
            strSQL += " WHERE Dia <= DATEPART(DD, EOMONTH(DATEADD(MONTH," + vm.periodo.ToString() + ", GETDATE()))) ";
            strSQL += " ORDER BY Dia ";
            IEnumerable<resumen> tae = db.Database.SqlQuery<resumen>(strSQL);
            vm.TAE = tae.ToList();

            //// MONTOS DIARIOS POR METODO DE PAGO | Pago de Servicios
            strSQL = " SELECT Dia, Socio, DATEADD(DD, Dia-1, DATEADD(MM, DATEDIFF(MM, 0, GETDATE()), 0)) As Fecha, Mes, COALESCE(Periodo,Dia) As Periodo, COALESCE(Transacciones,0) As Transacciones, COALESCE(Abono,0) As Abono, COALESCE(Cargo,0) As Cargo ";
            strSQL += " FROM Dias d ";
            strSQL += " LEFT JOIN ( ";
            strSQL += "   SELECT s.nombreComercial As Socio, CAST(FLOOR(CAST(t.fechaRegistro as float)) as DATETIME) As Fecha, CONVERT(VARCHAR, DATEPART(MONTH, t.FechaRegistro)) As Mes, DATEPART(DAY, t.fechaRegistro) As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(t.abonoTotal),0) As Abono, COALESCE(SUM(t.cargoTotal * -1),0) As Cargo ";
            strSQL += "   FROM Socios s ";
            strSQL += "   LEFT JOIN Transacciones t ON s.socioID = t.socioID ";
            strSQL += "   WHERE t.fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), t.FechaRegistro) = " + vm.periodo.ToString() + " AND (s.socioID = 6 OR s.socioID = 11 OR s.socioID =12 ) AND t.tarjetaID = " + tID.ToString();
            strSQL += "   GROUP BY s.socioID, s.nombreComercial, CAST(FLOOR(CAST(t.fechaRegistro AS float)) as DATETIME), DATEPART(MONTH, t.FechaRegistro), DATEPART(DAY, t.fechaRegistro) ";
            strSQL += " ) As data ON d.dia = data.Periodo ";
            strSQL += " WHERE Dia <= DATEPART(DD, EOMONTH(DATEADD(MONTH," + vm.periodo.ToString() + ", GETDATE()))) ";
            strSQL += " ORDER BY Dia ";
            IEnumerable<resumen> servicios = db.Database.SqlQuery<resumen>(strSQL);
            vm.Servicios = servicios.ToList();

            // MONTOS DIARIOS POR METODO DE PAGO | Transferencias
            strSQL = " SELECT Dia, Socio, DATEADD(DD, Dia-1, DATEADD(MM, DATEDIFF(MM, 0, GETDATE()), 0)) As Fecha, Mes, COALESCE(Periodo,Dia) As Periodo, COALESCE(Transacciones,0) As Transacciones, COALESCE(Abono,0) As Abono, COALESCE(Cargo,0) As Cargo "
                   + " FROM Dias d "
                   + " LEFT JOIN ( "
                   + " SELECT '' As Socio, CAST(FLOOR(CAST(t.fechaRegistro as float)) as DATETIME) As Fecha, CONVERT(VARCHAR, DATEPART(MONTH, t.FechaRegistro)) As Mes, DATEPART(DAY, t.fechaRegistro) As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(t.abono1),0) As Abono, COALESCE(SUM(t.cargo1 * -1),0) As Cargo "
                   + " FROM Transacciones t "
                   + " WHERE t.fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), t.FechaRegistro) = " + vm.periodo.ToString() + " AND t.socioID = 0 AND t.tarjetaID = " + tID.ToString()
                   + " GROUP BY socioID, CAST(FLOOR(CAST(t.fechaRegistro AS float)) as DATETIME), DATEPART(MONTH, t.FechaRegistro), DATEPART(DAY, t.fechaRegistro) "
                   + " ) As data ON d.dia = data.Periodo "
                   + " WHERE Dia <= DATEPART(DD, EOMONTH(DATEADD(MONTH," + vm.periodo.ToString() + ", GETDATE()))) "
                   + " ORDER BY Dia ";
            IEnumerable<resumen> transfer = db.Database.SqlQuery<resumen>(strSQL);
            vm.Transferencias = transfer.ToList();

            // TIEMPO AIRE | TELCEL
            strSQL = String.Format(" SELECT Periodo, Transacciones, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
            strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Abono, Cargo ");
            strSQL += String.Format(" FROM ( ");
            strSQL += String.Format(" 	SELECT -12 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -11 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -10 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -9 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -8 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -7 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -6 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -5 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -4 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -3 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -2 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -1 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT 0 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ", tID);
            strSQL += String.Format(" ) As Detalle ");
            IEnumerable<resumen> telcel = db.Database.SqlQuery<resumen>(strSQL);
            vm.Telcel = telcel.ToList();

            // TIEMPO AIRE | MOVISTAR
            strSQL = String.Format(" SELECT Periodo, Transacciones, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
            strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Abono, Cargo ");
            strSQL += String.Format(" FROM ( ");
            strSQL += String.Format(" 	SELECT -12 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -11 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -10 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -9 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -8 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -7 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -6 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -5 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -4 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -3 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -2 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -1 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT 0 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ", tID);
            strSQL += String.Format(" ) As Detalle ");
            IEnumerable<resumen> movistar = db.Database.SqlQuery<resumen>(strSQL);
            vm.Movistar = movistar.ToList();

            // TIEMPO AIRE | IUSACELL
            strSQL = String.Format(" SELECT Periodo, Transacciones, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
            strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Abono, Cargo ");
            strSQL += String.Format(" FROM ( ");
            strSQL += String.Format(" 	SELECT -12 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -11 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -10 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -9 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -8 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -7 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -6 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -5 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -4 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -3 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -2 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -1 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT 0 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ", tID);
            strSQL += String.Format(" ) As Detalle ");
            IEnumerable<resumen> iusacell = db.Database.SqlQuery<resumen>(strSQL);
            vm.Iusacell = iusacell.ToList();

            // TIEMPO AIRE | UNEFON
            strSQL = String.Format(" SELECT Periodo, Transacciones, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
            strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Abono, Cargo ");
            strSQL += String.Format(" FROM ( ");
            strSQL += String.Format(" 	SELECT -12 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -11 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -10 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -9 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -8 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -7 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -6 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -5 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -4 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -3 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -2 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -1 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT 0 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ", tID);
            strSQL += String.Format(" ) As Detalle ");
            IEnumerable<resumen> unefon = db.Database.SqlQuery<resumen>(strSQL);
            vm.Unefon = unefon.ToList();

            // TIEMPO AIRE | NEXTEL
            strSQL = String.Format(" SELECT Periodo, Transacciones, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
            strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Abono, Cargo ");
            strSQL += String.Format(" FROM ( ");
            strSQL += String.Format(" 	SELECT -12 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -11 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -10 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -9 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -8 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -7 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -6 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -5 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -4 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -3 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -2 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -1 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT 0 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ", tID);
            strSQL += String.Format(" ) As Detalle ");
            IEnumerable<resumen> nextel = db.Database.SqlQuery<resumen>(strSQL);
            vm.Nextel = nextel.ToList();

            // TIEMPO AIRE | VIRGIN
            strSQL = String.Format(" SELECT Periodo, Transacciones, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
            strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Abono, Cargo ");
            strSQL += String.Format(" FROM ( ");
            strSQL += String.Format(" 	SELECT -12 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -11 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -10 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -9 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -8 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -7 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -6 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -5 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -4 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -3 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -2 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -1 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT 0 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ", tID);
            strSQL += String.Format(" ) As Detalle ");
            IEnumerable<resumen> virgin = db.Database.SqlQuery<resumen>(strSQL);
            vm.Virgin = virgin.ToList();

            // PAGO DE SERVICIOS | Montos Procesados
            strSQL = String.Format(" SELECT Periodo, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
            strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Abono, Cargo ");
            strSQL += String.Format(" FROM ( ");
            strSQL += String.Format(" 	SELECT -12 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 )  AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -11 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -10 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -9 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -8 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -7 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -6 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -5 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -4 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -3 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -2 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -1 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT 0 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ", tID);
            strSQL += String.Format(" ) As Detalle ");
            IEnumerable<resumen> serviciosMonto = db.Database.SqlQuery<resumen>(strSQL);
            vm.ServiciosMonto = serviciosMonto.ToList();

            // PAGO DE SERVICIOS | Total de Transacciones
            strSQL = String.Format(" SELECT Periodo, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
            strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Total ");
            strSQL += String.Format(" FROM ( ");
            strSQL += String.Format(" 	SELECT -12 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -11 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -10 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -9 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -8 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -7 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -6 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -5 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -4 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -3 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -2 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT -1 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ", tID);
            strSQL += String.Format(" 	UNION ");
            strSQL += String.Format(" 	SELECT 0 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE (socioID = 6 OR socioID = 11 OR socioID =12 ) AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ", tID);
            strSQL += String.Format(" ) As Detalle ");
            IEnumerable<resumenUnitario> serviciosTransacciones = db.Database.SqlQuery<resumenUnitario>(strSQL);
            vm.ServiciosTransacciones = serviciosTransacciones.ToList();

            return vm;
        }

        #endregion load elements

        #region reports

        public static List<UserAccountReportModel> GetGeneralReport(Int32 tarjetaID, Int32 periodo)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            List<UserAccountReportModel> registries = new List<UserAccountReportModel>();

            registries = (
                from t in db.transacciones
                join o in db.origenes on t.origenID equals o.origenID
                join i in db.Tipo on t.tipoID equals i.tipoID
                join s in db.socios on t.socioID equals s.socioID
                join m in db.monedas on t.monedaID equals m.monedaID
                join e in db.Estatus on t.estatusID equals e.estatusID
                where
                t.tarjetaID == tarjetaID
                &&
                DbFunctions.DiffMonths(DateTime.Now, t.fechaRegistro) == periodo
                orderby t.fechaRegistro
                select new UserAccountReportModel
                {
                    transaccion_id = t.Id,
                    transaccion_transID = t.transID,
                    origin_descripcion = o.descripcion,
                    type_descripcion = i.descripcion,
                    transaccion_socioID = t.socioID,
                    partner_nombreComercial = s.nombreComercial,
                    transaccion_terminalID = t.terminalID,
                    currency_codigo = m.codigo,
                    transaccion_tarjetaID = t.tarjetaID,
                    transaccion_tarjeta = t.tarjeta,
                    transaccion_monto = t.monto,
                    transaccion_comision = (t.cargo2 + t.cargo3 + t.cargo4 + t.cargo5 + t.cargo6),
                    transaccion_iva = (t.cargo2iva + t.cargo3iva + t.cargo4iva + t.cargo5iva + t.cargo6iva),
                    transaccion_transaccion = t.transaccion,
                    transaccion_concepto = t.concepto,
                    transaccion_descripcion = t.descripcion,
                    transaccion_referencia = t.referencia,
                    status_descripcion = e.descripcion,
                    status_id = e.estatusID,
                    transaccion_aprobacion = t.aprobacion,
                    transaccion_ip = t.ip,
                    transaccion_fechaRegistro = t.fechaRegistro ?? DateTime.MinValue,
                    transaccion_monto_con_comission = (t.TotalMX != null ? (Decimal)t.TotalMX : 0)

                }).ToList();

            return registries;
        }

        public static List<UserAccountReportModel> GetDataForBasicReportWebsites(Guid currentUserId, String periodo, Int32 reportVersion)
        {

            ApplicationDbContext db = new ApplicationDbContext();
            periodo = (Int32.Parse(periodo) * -1).ToString();
            Int32 _intPeriod = Int32.Parse(periodo);

            if (periodo == String.Empty)
            {
                periodo = "0";
            }

            string _currentUserId = currentUserId.ToString();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == _currentUserId);

            List<UserAccountReportModel> items = new List<UserAccountReportModel>();
            items = Tools.GetGeneralReport(usuario.tarjetaID, _intPeriod);

            List<TransactionFee> trasnfee = db.TransactionFee.ToList();

            foreach (UserAccountReportModel item in items)
            {
                trans transaction = db.transacciones.FirstOrDefault(x => x.transID == item.transaccion_transID);
                Guid _transactionId = transaction.Id;

                string _beneficiary = "N/A";
                string _buyer = "N/A";

                //this is use to send a ticket via email
                pagoServicio oPago = db.pagoServicios.Where(x => x.transaccionesId == _transactionId).FirstOrDefault();
                if (oPago != null)
                {
                    if (oPago.Id != null)
                    {
                        item.GuidPagoServiciosId = oPago.Id;
                    }
                }

                if (item.transaccion_socioID == 11)
                {
                    ImssPaymentsModel imssPayment = db.ImssPaymentsModel.Where(x => x.transaccionesId == item.transaccion_id).FirstOrDefault();
                    if (imssPayment != null)
                    {
                        _beneficiary = imssPayment.beneficiary_name + " " + imssPayment.beneficiary_last + " " + imssPayment.beneficiary_mothers + " - " + imssPayment.beneficiary_phone_number;
                        _buyer = imssPayment.buyer_name + " - " + imssPayment.beneficiary_cell_phone_number;
                    }
                }
                else if (item.transaccion_socioID == 12)
                {
                    CemexPayments cemexPayment = db.CemexPayments.Where(x => x.transaccionesId == item.transaccion_id).FirstOrDefault();
                    if (cemexPayment != null)
                    {
                        _beneficiary = cemexPayment.beneficiary_name + " " + cemexPayment.beneficiary_last + " " + cemexPayment.beneficiary_mothers + " - " + cemexPayment.beneficiary_phone_number;
                        _buyer = cemexPayment.buyer_name + " - " + cemexPayment.beneficiary_cell_phone_number;
                    }
                }
                else
                {
                    if (oPago != null)
                    {
                        _beneficiary = oPago.referencia;
                    }
                }

                List<TransactionFee> transferFees = trasnfee.Where(x => x.TransaccionesId == _transactionId).ToList();
                Decimal comissionEnpadiMXN = 0;
                Decimal comissionPartnerMXN = 0;
                Decimal comissionEnpadiUSD = 0;
                Decimal comissionPartnerUSD = 0;

                foreach (TransactionFee transferFee in transferFees)
                {

                    Fee fee = db.Fee.Where(x => x.Id == transferFee.FeeId).FirstOrDefault();

                    if (fee.PartnerToPayId == socio.SocioID.Enpadi)
                    {
                        comissionEnpadiMXN += transferFee.FixAmount + transferFee.VariableAmount + transferFee.TaxFeeAmount + transferFee.ChargedByPartnerAmount;
                        comissionEnpadiUSD += transferFee.FixAmountDlls.GetValueOrDefault() + transferFee.VariableAmountDlls.GetValueOrDefault() + transferFee.TaxFeeAmountDlls.GetValueOrDefault() + transferFee.ChargedByPartnerAmount;
                    }
                    else
                    {
                        comissionPartnerMXN += transferFee.FixAmount + transferFee.VariableAmount + transferFee.TaxFeeAmount + transferFee.ChargedByPartnerAmount;
                        comissionPartnerUSD += transferFee.FixAmountDlls.GetValueOrDefault() + transferFee.VariableAmountDlls.GetValueOrDefault() + transferFee.TaxFeeAmountDlls.GetValueOrDefault() + transferFee.ChargedByPartnerAmount;
                    }
                }

                item.EnpadiFeesMXN = comissionEnpadiMXN;
                item.EnpadiFeesUSD = comissionEnpadiUSD;

                item.PartnerFeesMXN = comissionPartnerMXN;
                item.PartnerFeesUSD = comissionPartnerUSD;

                item.AmountForServiceMXN = transaction.ServiceInMXN;
                item.AmountForServiceUSD = transaction.ServiceInDlls;
                item.ComissionMX = transaction.ComissionMX;
                item.ComissionUSD = transaction.ComissionUSD;



                //separate comission from services payed in MXN and services payed in dlls
                switch (item.transaccion_socioID)
                {
                    case socio.SocioID.Cemex_int_id:
                        item.ComissionDllsServicesInUSD += transaction.ComissionUSD.GetValueOrDefault();
                        break;
                    case socio.SocioID.Imss_int_id:
                        item.ComissionDllsServicesInUSD += transaction.ComissionUSD.GetValueOrDefault();
                        break;
                    default:
                        item.ComissionMXServicesInMX += transaction.ComissionMX.GetValueOrDefault();
                        break;
                }


                item.TotalAmountMX = transaction.TotalMX;
                item.TotalAmountUSD = transaction.TotalUSD;

                item.transaccion_comision = transaction.ComissionUSD.GetValueOrDefault();
                item.transaccion_monto_con_comission = transaction.TotalUSD.GetValueOrDefault();

                item.ReprintReceipt = _transactionId.ToString();

                item.beneficiary = _beneficiary;

                switch (transaction.tipoID)
                {
                    case 1:
                        item.type_descripcion = "<label class='lang' style='font-weight: normal' key='tabledesc.deposit'></label>";
                        break;
                    case 2:
                        item.type_descripcion = "<label class='lang' style='font-weight: normal' key='tabledesc.charge'></label>";
                        break;
                    case 3:
                        item.type_descripcion = "<label class='lang' style='font-weight: normal' key='tabledesc.transfer'></label>";
                        break;
                    case 4:
                        item.type_descripcion = "<label class='lang' style='font-weight: normal' key='tabledesc.return'></label>";
                        break;
                    case 5:
                        item.type_descripcion = "<label class='lang' style='font-weight: normal' key='tabledesc.chargeback'></label>";
                        break;
                    case 6:
                        item.type_descripcion = "<label class='lang' style='font-weight: normal' key='tabledesc.remittance'></label>";
                        break;
                    case 7:
                        item.type_descripcion = "<label class='lang' style='font-weight: normal' key='tabledesc.reward'></label>";
                        break;
                    default:
                        item.type_descripcion = "N/A";
                        break;
                }

                switch (item.status_id)
                {
                    case 1:
                        item.status_descripcion = "<label class='lang' style='font-weight: normal' key='tabledesc.approved'></label>";
                        break;
                    case 2:
                        item.status_descripcion = "<label class='lang' style='font-weight: normal' key='tabledesc.rejected'></label>";
                        break;
                    case 3:
                        item.status_descripcion = "<label class='lang' style='font-weight: normal' key='tabledesc.pending'></label>";
                        break;
                    default:
                        item.status_descripcion = "N/A";
                        break;
                }

                item.buyer = _buyer;
            }

            return items.OrderByDescending(x => x.transaccion_transID).ToList();

        }

        #endregion reports

        #region send email

        private static string prepareEmailBodyMessage(
                                String greetings
                                , String timestap
                                , String title
                                , String serviceName
                                , String reference
                                , String referenceValue
                                , String serviceInDlls
                                , String serviceInDllsValue
                                , String feeInDlls
                                , String feeInDllsValue
                                , String totalInDlls
                                , String totalInDllsValue
                                , String beneficiary
                                , String beneficiaryValue
                                , String buyer
                                , String buyerValue
                                , String transaction
                                , String transactionValue
                                , String supplier
                                , String supplierValue
                                , String ticketReference
                                , String ticketReferenceValue
                                , String autorization
                                , String autorizationValue
                                , String additionalInfo
                                , String email
                                , String Message
            )
        {
            string bodyMessage = String.Empty;

            var body = "<p>{0}({1})</p><p>{2}</p><br />";

            /** 
             * ticket parameters
             * 0 date time stap of purchase
             * 1 title
             * 2 service name
             * 3 reference name
             * 4 reference value
             * 5 service in dlls amount name
             * 6 service in dlls amount value
             * 7 fee in dlls name
             * 8 fee in dlls value
             * 9 total in dlls name
             * 10 total in dlls value
             * 11 beneficiary name
             * 12 beneficiary value
             * 13 buyer name
             * 14 buyer value
             * 15 transaction name
             * 16 transaction value
             * 17 supplier name 
             * 18 supplier value
             * 19 reference name
             * 20 regerence value
             * 21 autorization name
             * 22 autorization value
             * 23 addition info
             * 
             * **/
            var ticket = @"<table style='margin: 0 auto'>
                        <tbody>
                            <tr>
                                <td colspan = '2' style = 'text-align:center'>
                                       <img src = 'https://www.enpadi.com/enpadi-logo.png' alt = 'Logo' style = 'width: 150px;'>
                                            <hr />
                                        </td>
                                    </tr>
                            <tr>
                                <td colspan = '2'>
                                     <h4 style = 'padding-top:10px; text-align:center;'> {0} </h4>
                                              <hr />
                                          </td>
                                      </tr>
                            <tr>
                                <td colspan = '2'>
                                     <div style = 'text-align:center;'>{1}</div>
                                          <hr />
                                      </td>
                                  </tr>
                                  <tr>
                                      <td colspan = '2'>
                                           <h5 style = 'font-weight:bold; text-align:center;'> {2} </h5>
                                                <hr />
                                            </td>
                                        </tr>
                            <tr>
                                <td>
                                    <label>
                                        {3}
                                    </label>
                                </td>
                                <td>
                                    <div style = 'border-bottom-style: inset;'>
                                         {4}
                                     </div>
                                 </td>
                             </tr>
                           <tr>
                               <td>
                                   <label>
                                       {5}
                                   </label>
                               </td>
                               <td>
                                   <div style = 'border-bottom-style: inset;'>
                                        {6}
                                   </div>
                               </td>
                           </tr>
                          <tr>
                              <td>
                                  <label>
                                      {7}
                                  </label>
                              </td>
                              <td>
                                  <div style = 'border-bottom-style: inset;'>
                                        {8}
                                  </div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <label>
                                      {9}
                                  </label>
                              </td>
                              <td>
                                  <div style = 'border-bottom-style: inset;'>
                                        {10}
                                  </div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <label>
                                      {11}
                                  </label>
                              </td>
                              <td>
                                  <div style = 'border-bottom-style: inset;'>
                                      {12}
                                  </div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <label>
                                      {13}
                                  </label>
                              </td>
                              <td>
                                  <div style = 'border-bottom-style: inset;'>
                                      {14}
                                  </div>
                              </td>
                          </tr>
                          <tr>
                              <td style = 'text-align:center' colspan = '2'>
                                  <hr />
                              </td>
                          </tr>
                          <tr>
                              <td style = 'text-align:left'>
                                  <label>{15}</label>
                              </td>
                              <td style = 'text-align:left'>
                                  <label> {16}</label>
                              </td>
                          </tr>
                          <tr>
                              <td style = 'text-align:left'>
                                  <label>
                                      {17}
                                  </label>
                              </td>
                              <td style = 'text-align:left'>
                                  <label>
                                      {18}
                                  </label>
                              </td>
                          </tr>
                          <tr>
                              <td style = 'text-align:left'>
                                  <label>
                                      {19}
                                  </label>
                              </td>
                              <td style = 'text-align:left'>
                                  <label>
                                      {20}
                                  </label>
                              </td>
                          </tr>
                          <tr>
                              <td style = 'text-align:left'>
                                  <label>
                                      {21}
                                  </label>
                              </td>
                              <td style = 'text-align:left'>
                                  <label>
                                      {22}
                                  </label>
                              </td>
                          </tr>
                          <tr>
                              <td style = 'text-align:center' colspan = '2'>
                                  <h5> {23} </h5>
                              </td>
                          </tr>
                      </tbody>
                  </table>";

            var processTicket = string.Format(ticket
                  , timestap
                  , title
                  , serviceName
                  , reference
                  , referenceValue
                  , serviceInDlls
                  , serviceInDllsValue
                  , feeInDlls
                  , feeInDllsValue
                  , totalInDlls
                  , totalInDllsValue
                  , beneficiary
                  , beneficiaryValue
                  , buyer
                  , buyerValue
                  , transaction
                  , transactionValue
                  , supplier
                  , supplierValue
                  , ticketReference
                  , ticketReferenceValue
                  , autorization
                  , autorizationValue
                  , additionalInfo);

            bodyMessage = string.Format(body + processTicket, greetings, email, Message);
            return bodyMessage;
        }

        public static async Task<Boolean> SendEmail(String email, String Message, Boolean spanish, Guid opagoId)
        {
            Boolean emailSend = false;

            ApplicationDbContext db = new ApplicationDbContext();


            try
            {

                var message = new MailMessage();
                message.To.Add(new MailAddress(email));  // replace with valid value 
                message.From = new MailAddress("notificaciones@enpadi.com");  // replace with valid value
                message.Subject = "Enpadi Ticket";
                message.IsBodyHtml = true;
                message.Headers.Add("Message-ID",
                         String.Format("<{0}@{1}>",
                         Guid.NewGuid().ToString(),
                        "notificaciones@enpadi.com"));
                message.Headers.Add("X-Company", "Enpadi");
                message.Headers.Add("X-Location", "Mexico");
                message.Headers.Add("Content-Type", "html");
                message.Headers.Add("X-Priority", "3");

                pagoServicio oPago = db.pagoServicios.Where(x => x.Id == opagoId).FirstOrDefault();
                oPago = Tools.AddDisplayFields(oPago);

                if (spanish == true)
                {
                    message.Body = Tools.prepareEmailBodyMessage(
                                    "Querido "
                                    , String.Format("{0} {1}", oPago.fechaLocal, oPago.horaLocal)
                                    , "TICKET DE VENTA"
                                    , oPago.nombre
                                    , "Referencia"
                                    , oPago.DisplayFields[0].sValor.ToString()
                                    , "Cantidad por el servicio (DLLS)"
                                    , oPago.DisplayFields[2].sValor.ToString()
                                    , "Comisión por el servicio (DLLS)"
                                    , oPago.DisplayFields[3].sValor.ToString()
                                    , "Total (DLLS)"
                                    , oPago.DisplayFields[4].sValor.ToString()
                                    , "Beneficiario"
                                    , oPago.DisplayFields[9].sValor.ToString()
                                    , "Comprador"
                                    , oPago.DisplayFields[10].sValor.ToString()
                                    , "TRANSACCION :"
                                    , oPago.Id.ToString()
                                    , " PROVEEDOR :"
                                    , oPago.proveedor
                                    , "REFERENCIA :"
                                    , oPago.referencia
                                    , "AUTORIZACION"
                                    , oPago.autorizacion
                                    , oPago.leyenda
                                    , email
                                    , Message
                                    );
                }
                else
                {
                    message.Body = Tools.prepareEmailBodyMessage(
                                    "Dear "
                                    , String.Format("{0} {1}", oPago.fechaLocal, oPago.horaLocal)
                                    , "SALE TICKET"
                                    , oPago.nombre
                                    , "Reference"
                                    , oPago.DisplayFields[0].sValor.ToString()
                                    , "Amount for service (DLLS)"
                                    , oPago.DisplayFields[2].sValor.ToString()
                                    , "Fee for service (DLLS)"
                                    , oPago.DisplayFields[3].sValor.ToString()
                                    , "Total (DLLS)"
                                    , oPago.DisplayFields[4].sValor.ToString()
                                    , "Beneficiary"
                                    , oPago.DisplayFields[9].sValor.ToString()
                                    , "Buyer"
                                    , oPago.DisplayFields[10].sValor.ToString()
                                    , "TRANSACTION :"
                                    , oPago.Id.ToString()
                                    , " SUPPLIER :"
                                    , oPago.proveedor
                                    , "REFERENCE :"
                                    , oPago.referencia
                                    , "AUTHORIZATION"
                                    , oPago.autorizacion
                                    , oPago.leyenda
                                    , email
                                    , Message
                                    );
                }

                using (var smtp = new SmtpClient())
                {
                    var credential = new NetworkCredential
                    {
                        UserName = "notificaciones@enpadi.com",  // replace with valid value
                        Password = "xANQ4TeJYJ6DvSWa#$"  // replace with valid value
                    };
                    smtp.Credentials = credential;
                    smtp.Host = "enpadi.com";
                    smtp.Port = 25;
                    smtp.EnableSsl = false;
                    await smtp.SendMailAsync(message);
                    emailSend = true;
                }

                return emailSend;
            }
            catch (Exception e)
            {
                string message = e.Message;
                return false;
            }
        }

        #endregion send email

    }
}