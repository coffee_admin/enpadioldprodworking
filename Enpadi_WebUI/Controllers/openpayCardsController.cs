﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Enpadi_WebUI.Models;

namespace Enpadi_WebUI.Controllers
{
    public class openpayCardsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: openpayCards
        public ActionResult Index()
        {
            return View(db.openpayCards.ToList());
        }

        // GET: openpayCards/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            openpayCard openpayCard = db.openpayCards.Find(id);
            if (openpayCard == null)
            {
                return HttpNotFound();
            }
            return View(openpayCard);
        }

        // GET: openpayCards/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: openpayCards/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,CardID,HolderName,CardNumber,CVV2,ExpirationMonth,ExpirationYear,Address1,Address2,Address3,City,State,Country,AllowCharges,AllowPayouts,Brand,Type,BankName,BankCode,CustomerId,PointsCard,CreationDate")] openpayCard openpayCard)
        {
            if (ModelState.IsValid)
            {
                db.openpayCards.Add(openpayCard);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(openpayCard);
        }

        // GET: openpayCards/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            openpayCard openpayCard = db.openpayCards.Find(id);
            if (openpayCard == null)
            {
                return HttpNotFound();
            }
            return View(openpayCard);
        }

        // POST: openpayCards/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,CardID,HolderName,CardNumber,CVV2,ExpirationMonth,ExpirationYear,Address1,Address2,Address3,City,State,Country,AllowCharges,AllowPayouts,Brand,Type,BankName,BankCode,CustomerId,PointsCard,CreationDate")] openpayCard openpayCard)
        {
            if (ModelState.IsValid)
            {
                db.Entry(openpayCard).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(openpayCard);
        }

        // GET: openpayCards/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            openpayCard openpayCard = db.openpayCards.Find(id);
            if (openpayCard == null)
            {
                return HttpNotFound();
            }
            return View(openpayCard);
        }

        // POST: openpayCards/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            openpayCard openpayCard = db.openpayCards.Find(id);
            db.openpayCards.Remove(openpayCard);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
