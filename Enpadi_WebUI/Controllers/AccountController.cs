﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Enpadi_WebUI.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using AutoMapper;
using Enpadi_WebUI.Models.ViewModel;
using System.ServiceModel.Web;
using System.IO;

namespace Enpadi_WebUI.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private ApplicationDbContext db = new ApplicationDbContext();

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [WebInvoke(
            Method = "POST",
            UriTemplate = "OxxoPayResponseConekta",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        [AllowAnonymous]
        public ActionResult ConektaTest(string returnUrlresult)
        {
            //TBBM: this is the template to confirm an email
            //var user = await UserManager.FindByNameAsync(Model.Email);
            //if (user != null) {
            //    if (!await UserManager.IsEmailConfirmedAsync(user.id)) {
            //        ViewBag.errorMessage = "You must have a confirmed email to log in";
            //        return View("Error");
            //    }
            //}


            return View();
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl, string skin, String cty)
        {
            //TBBM: this is the template to confirm an email
            //var user = await UserManager.FindByNameAsync(Model.Email);
            //if (user != null) {
            //    if (!await UserManager.IsEmailConfirmedAsync(user.id)) {
            //        ViewBag.errorMessage = "You must have a confirmed email to log in";
            //        return View("Error");
            //    }
            //}

            Session["country"] = cty;

            ViewBag.ReturnUrl = returnUrl;

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();


            Stream req = Request.InputStream;

            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl, string skin, string cty)
        {


            Session["country"] = cty;

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToAction("Index", "Home");
                case SignInStatus.LockedOut:
                    return RedirectToAction("LogIn", model);
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return RedirectToAction("LogIn", model);

                //case SignInStatus.Success:
                //    return RedirectToLocal(returnUrl);
                //case SignInStatus.LockedOut:
                //    return View("Lockout");
                //case SignInStatus.RequiresVerification:
                //    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                //case SignInStatus.Failure:
                //default:
                //    ModelState.AddModelError("", "Invalid login attempt.");
                //    return View(model);
            }
        }


        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe, string userId, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe, UserId = userId });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    strSQL = String.Format("UPDATE AspNetUsers SET PhoneNumberConfirmed = 1, TwoFactorEnabled = 0 WHERE Id = '{0}'", model.UserId);
                    db.Database.ExecuteSqlCommand(strSQL);
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                //strSQL = string.Format("SELECT COUNT(*) As total FROM AspNetUsers WHERE PhoneNumber = '{0}'", model.PhoneNumber);
                //IEnumerable<contador> registros = db.Database.SqlQuery<contador>(strSQL);
                //int iReg = registros.ToList().FirstOrDefault().total;

                var contextDB = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
                var storeDB = new UserStore<IdentityUser>(contextDB);
                var managerDB = new UserManager<IdentityUser>(storeDB);
                var userDB = managerDB.Users.Where(u => u.PhoneNumber == model.PhoneNumber).FirstOrDefault();

                if (userDB != null)
                {
                    ModelState.AddModelError("", "Ya existe una cuenta registrada con el mismo Número Movil");
                    return View(model);
                }
                else
                {
                    var user = new ApplicationUser {
                        UserName = model.Email,
                        Email = model.Email,
                        Name = model.Name,
                        PhoneNumber = model.PhoneNumber,
                        TwoFactorEnabled = false,
                        EmailConfirmed = false
                    };

                    var result = await UserManager.CreateAsync(user, model.Password);

                    var context = new ApplicationDbContext();
                    var roleStore = new RoleStore<IdentityRole>(context);
                    var roleManager = new RoleManager<IdentityRole>(roleStore);
                    var userStore = new UserStore<ApplicationUser>(context);
                    var userManager = new UserManager<ApplicationUser>(userStore);

                    if (result.Succeeded)
                    {
                        userManager.AddToRole(user.Id, SecurityRoles.Usuario);
                        tarjetas tc = db.tarjetas.Where(t => t.tcAsignada == false).FirstOrDefault();
                        tc.tcAsignada = true;
                        tc.datNombre = model.Name;
                        tc.email = model.Email;
                        tc.password = user.Id;
                        db.Entry(tc).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();

                        //Envio de Correo - Creación de Cuenta (Procedimiento Temporal)
                        //var email = new MailMessage() { From = new MailAddress("info@enpadi.com"), Subject = "Binvenido a Enpadi", IsBodyHtml = true };
                        //string sFilePath = string.Format("C:\\Proyectos\\Enpadi\\Enpadi_WebUI\\Images\\enpadi-logo.png");
                        //Attachment img = new Attachment(sFilePath);
                        //img.ContentId = "207465738";
                        //email.Attachments.Add(img);
                        //email.Body = EmailBody("Registro", tc.datNombre + ' ' + tc.datPaterno, tc.email, tc.tcNum, tc.tcNIP);
                        //email.To.Add(tc.email);
                        //using (var smtp = new SmtpClient())
                        //{
                        //    var credential = new NetworkCredential() { UserName = "info@enpadi.com", Password = "93Hj!vh9" };
                        //    smtp.Credentials = credential;
                        //    smtp.Host = "enpadi.com";
                        //    smtp.Port = 25;
                        //    await smtp.SendMailAsync(email);
                        //}


                        // Termina Procedimiento para Envio de Correo - Creación de Cuenta

                        //Logeo Automatico
                        //await SignInManager.SignInAsync(user, isPersistent:false, rememberBrowser:false);

                        // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                        // Send an email with this link
                        //string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        //var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                        //await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                        //var code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        //var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                        //string sBody = EmailBody("Registro", string.Format("{0} {1} {2}", tc.datNombre, tc.datPaterno, tc.datMaterno), tc.email, tc.tcNum, tc.tcNIP, callbackUrl);
                        //await UserManager.SendEmailAsync(user.Id, "Verificación de Cuenta", sBody);

                        //await UserManager.SendSmsAsync(user.Id, "Tu Codigo de Verificación es: " + code.ToString());
                        //ViewBag.Link = callbackUrl;



                        //TBBM: this works

                        // Plug in your email service here to send an email.
                        //var code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        //var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                        //string sBody = EmailBody("Registro", string.Format("{0} {1} {2}", tc.datNombre, tc.datPaterno, tc.datMaterno), tc.email, tc.tcNum, tc.tcNIP, callbackUrl);
                        //IdentityMessage message = new IdentityMessage();

                        //var mailMessage = new MailMessage("info@enpadi.com",
                        //    message.Destination = tc.email,
                        //    message.Subject = "Registro",
                        //    message.Body = sBody
                        //    );


                        //var credential = new NetworkCredential() { UserName = "info@enpadi.com", Password = "93Hj!vh9" };
                        //SmtpClient client = new SmtpClient("enpadi.com", 25) { Credentials = credential };
                        //await client.SendMailAsync(mailMessage);

                        await SendEmail(user, tc);

                        //TBBM: this works


                        LoginViewModel loginview = new LoginViewModel {
                            Email=model.Email,
                            Password = model.Password
                        };

                        //return View("PleaseConfirm");
                        return View("ThankYouForAccount", loginview);

                        //return RedirectToAction("SendCode", new { ReturnUrl = "Login", RememberMe = false });
                    }
                    else
                    {
                        AddErrors(result);
                        return View(model);
                    }
                }

            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public async Task<Boolean> SendEmail(ApplicationUser user, tarjetas tc)
        {
            var code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
            var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
            string sBody = EmailBody("Registro", string.Format("{0} {1} {2}", tc.datNombre, tc.datPaterno, tc.datMaterno), tc.email, tc.tcNum, tc.tcNIP, callbackUrl);

            IdentityMessage message = new IdentityMessage();

            var mailMessage = new MailMessage("info@enpadi.com",
                message.Destination = tc.email,
                message.Subject = "Registro - Enpadi.com",
                message.Body = sBody
                );


            var credential = new NetworkCredential() { UserName = "info@enpadi.com", Password = "93Hj!vh9" };
            SmtpClient client = new SmtpClient("enpadi.com", 25) { Credentials = credential };
            await client.SendMailAsync(mailMessage);

            return true;
        }

        public ActionResult PleaseConfirm(String skin)
        {

            var contextDB = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
            var storeDB = new UserStore<IdentityUser>(contextDB);
            var managerDB = new UserManager<IdentityUser>(storeDB);
            var userDB = managerDB.FindById(User.Identity.GetUserId());

            ViewModelPleaseConfirm viewModel = new ViewModelPleaseConfirm {
                UserEmail = userDB.Email,
            };

            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View(viewModel);
        }

        public async Task<ActionResult> ResentConfirmEmail()
        {

            var contextDB = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
            var storeDB = new UserStore<IdentityUser>(contextDB);
            var managerDB = new UserManager<IdentityUser>(storeDB);
            IdentityUser userDB = managerDB.FindById(User.Identity.GetUserId());

            ApplicationUser user = Mapper.Map<IdentityUser, ApplicationUser>(userDB);

            tarjetas tc = db.tarjetas.Where(t => t.email == user.Email).FirstOrDefault();

            var send = await SendEmail(user, tc);

            return Content("<p><b>Se ha enviado el correo electr&oacute;nico.</b></p>");

        }


        private string EmailBody(string tipo, string usuario, string email, string cuenta, string Nip, string callbackUrl)
        {
            string sBody = string.Empty;
            switch (tipo)
            {
                case "Registro":
                    sBody = "<html>"
                          + " <style type='text/css'> "
                          + " body { background-color:#FFFFFF; font-family:Verdana, Arial, Sans-Serif; color:#003366; font-size:14px; text-align:center; } "
                          + " </style> "
                          + " <body> "
                          + " <div id='content' align='center' style='width:600px;'> "
                          + " <div id='header' style='padding-top:25px; padding-bottom:50px;'> "
                          + " <img src='http://www.enpadi.com/enpadiApp/Images/enpadi-logo.png' width='250'> "
                          + " </div> "
                          + " <div id='info' align='center' style='margin-top:15px;'> "
                          + " <div>Estimado <strong>" + usuario + "</strong></div></br> "
                          + " <div>Tu cuenta <b>enpadi</b> ha sido configurada exitosamente</div></br> "
                          + " <div>Usuario: <strong>" + email + "</strong></div></br> "
                          + " <div>Cuenta: <strong>" + cuenta + "</strong></div> "
                          + " <div>NIP: <strong>" + Nip + "</strong></div></br> "
                          + " <div>Antes de comenzar a realizar pagos de manera segura, requerimos validar tu cuenta.</div>"
                          + " <div>¡Verificala en este momento haciendo click en el siguiente link:<div>"
                          + " <div><a href=\"" + callbackUrl + "\">Verificar Cuenta</a></div></br> "
                          + " <div>Cualquier duda o comentario que tengas, podras comunicarte al (812) 089-6071</div> "
                          + " <div>o enviar un correo a <a href='mailto:atencion@enpadi.com'>atencion@enpadi.com</a></div> "
                          + " </div> "
                          + " </div> "
                          + " </body> "
                          + " </html> ";
                    break;
            }
            return sBody;
        }




        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);

            if (result.Succeeded == true)
            {
                strSQL = String.Format("UPDATE AspNetUsers SET PhoneNumberConfirmed = 1, TwoFactorEnabled = 0 WHERE Id = '{0}'", userId);
                db.Database.ExecuteSqlCommand(strSQL);
            }

            return View(result.Succeeded ? "ConfirmEmail" : "PleaseConfirm");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                // return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe, UserId = userId });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe, UserId = model.UserId });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }


        //
        // GET: /Account/LogOff
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        //
        // POST: /Account/LogOff
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult LogOff()
        //{
        //    AuthenticationManager.SignOut();
        //    return RedirectToAction("Index", "Home");
        //}

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}