﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Enpadi_WebUI.Models;
using Enpadi_WebUI.Models.ViewModel;
using System.Collections.Generic;
using System.Net.Http;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using System.Text;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Enpadi_WebUI.Controllers
{
    public class TestController : Controller
    {
        #region main class views and methods (shared, index and general views)

        #region views

        public ActionResult Index()
        {
            ApplicationDbContext db = new ApplicationDbContext();

            String demoText = "This is the demo text en espa;ol tambien.";

            TestModelView model = new TestModelView();

            EncryptionModel encrypModel = new EncryptionModel();
            encrypModel.GenerateKeys();

            model.Text = demoText;
            model.Encripted = encrypModel.Encript(demoText);
            model.Decripted = encrypModel.DeEncrypt(model.Encripted);
            model.PrivateKey = encrypModel.Private_key;
            model.PublicKey = encrypModel.Public_key;

            return View(model);
        }

        #endregion views

        #endregion main class views and methods (shared, index and general views)

        #region api

        #region views

        public ActionResult htmlticket()
        {
            return View();
        }

        public ActionResult ApiMx()
        {
            OperationObject result = ApplicationUser.CreateEncryptionKeysForUser("amorales@truewisdom.co");

            return View();
        }

        public ActionResult ApiUsa()
        {
            return View();
        }

        #region ajax methods

        public JsonResult GetServices(String region, String email, String password)
        {
            object result = new object();
            var contentValue = "";

            //create request to send password
            HttpRequestMessage message = new HttpRequestMessage();
            var configuration = new System.Web.Http.HttpConfiguration();
            message.Properties[System.Web.Http.Hosting.HttpPropertyKeys.HttpConfigurationKey] = configuration;
            message.Content = new StringContent("{'email':'" + email + "','password':'" + password + "'}", Encoding.UTF8, "application/json");

            //send request and read results
            Enpadi_WebUI.Controllers.Api.EnpadiController api = new Enpadi_WebUI.Controllers.Api.EnpadiController();
            if (api.CheckServices(message).Result.TryGetContentValue(out contentValue) == true)
            {
                result = new Dictionary<String, Object>() {
                    { "data",contentValue}
                };
            }
            else
            {
                result = new Dictionary<String, Object>() {
                    { "Error:","Please check server connection with Enpadi."}
                };
            }

            //return json object with results
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetFeeForServices(String region, String email, String password, String sku)
        {
            object result = new object();

            var contentValue = "";

            //create request to send password
            HttpRequestMessage message = new HttpRequestMessage();
            var configuration = new System.Web.Http.HttpConfiguration();
            message.Properties[System.Web.Http.Hosting.HttpPropertyKeys.HttpConfigurationKey] = configuration;
            message.Content = new StringContent("{'email':'" + email + "','password':'" + password + "','sku':'" + sku + "'}", Encoding.UTF8, "application/json");

            //send request and read results
            Enpadi_WebUI.Controllers.Api.EnpadiController api = new Enpadi_WebUI.Controllers.Api.EnpadiController();
            if (api.CheckFeeForService(message).Result.TryGetContentValue(out contentValue) == true)
            {
                result = new Dictionary<String, Object>() {
                    { "data",contentValue}
                };
            }
            else
            {
                result = new Dictionary<String, Object>() {
                    { "Error:","Please check server connection with Enpadi."}
                };
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateOrderNoPaymentOption(String email, String password, String jsonObject)
        {
            object result = new object();
            var contentValue = "";

            try
            {
                HttpRequestMessage message = new HttpRequestMessage();
                var configuration = new System.Web.Http.HttpConfiguration();
                message.Properties[System.Web.Http.Hosting.HttpPropertyKeys.HttpConfigurationKey] = configuration;
                message.Content = new StringContent("{'email':'" + email + "','password':'" + password + "','jsonObject':'" + jsonObject + "'}", Encoding.UTF8, "application/json");

                //send request and read results
                Enpadi_WebUI.Controllers.Api.EnpadiController api = new Enpadi_WebUI.Controllers.Api.EnpadiController();
                if (api.CreateOrder(message).Result.TryGetContentValue(out contentValue) == true)
                {
                    result = new Dictionary<String, Object>() {
                    { "data",contentValue}
                };
                }
                else
                {
                    result = new Dictionary<String, Object>() {
                    { "Error:","Please check server connection with Enpadi."}
                };
                }

                //return json object with results
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {

                String exeptionMessage = e.Message;
                result = new Dictionary<String, Object>() {
                    { "Error:","Please check http request message call."}
                };

                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAllOrders(String email, String password) {

            object result = new object();
            var contentValue = "";

            //create request to send password
            HttpRequestMessage message = new HttpRequestMessage();
            var configuration = new System.Web.Http.HttpConfiguration();
            message.Properties[System.Web.Http.Hosting.HttpPropertyKeys.HttpConfigurationKey] = configuration;
            message.Content = new StringContent("{'email':'" + email + "','password':'" + password + "'}", Encoding.UTF8, "application/json");

            //send request and read results
            Enpadi_WebUI.Controllers.Api.EnpadiController api = new Enpadi_WebUI.Controllers.Api.EnpadiController();
            if (api.GetAllOrders(message).Result.TryGetContentValue(out contentValue) == true)
            {
                result = new Dictionary<String, Object>() {
                    { "data",contentValue}
                };
            }
            else
            {
                result = new Dictionary<String, Object>() {
                    { "Error:","Please check server connection with Enpadi."}
                };
            }

            //return json object with results
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult PayServiceWithCreditCard(String email, String password, String jsonObject)
        {
            object result = new object();
            var contentValue = "";

            try
            {

                //read the content of the httprequestmessage
                //string str = request.Content.ReadAsStringAsync().Result;
                //String jsonString = await request.Content.ReadAsStringAsync();

                //string email = ((JToken)JObject.Parse(jsonString))["email"].ToString();
                //string password = ((JToken)JObject.Parse(jsonString))["password"].ToString();

                //string jsonObject = ((JToken)JObject.Parse(jsonString))["jsonObject"].ToString();

                //create request to send password
                HttpRequestMessage message = new HttpRequestMessage();
                var configuration = new System.Web.Http.HttpConfiguration();
                message.Properties[System.Web.Http.Hosting.HttpPropertyKeys.HttpConfigurationKey] = configuration;
                message.Content = new StringContent("{'email':'" + email + "','password':'" + password + "','jsonObject':'" + jsonObject + "'}", Encoding.UTF8, "application/json");

                //send request and read results
                Enpadi_WebUI.Controllers.Api.EnpadiController api = new Enpadi_WebUI.Controllers.Api.EnpadiController();
                if (api.PayServiceWithCredit(message).Result.TryGetContentValue(out contentValue) == true)
                {
                    result = new Dictionary<String, Object>() {
                    { "data",contentValue}
                };
                }
                else
                {
                    result = new Dictionary<String, Object>() {
                    { "Error:","Please check server connection with Enpadi."}
                };
                }

                //return json object with results
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {

                String exeptionMessage = e.Message;
                result = new Dictionary<String, Object>() {
                    { "Error:","Please check http request message call."}
                };

                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion ajax methods

        #endregion views

        #endregion api

        #region methods

        private IdentityUser GetUser(String email, String password)
        {

            var contextDB = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
            var storeDB = new UserStore<IdentityUser>(contextDB);
            var managerDB = new UserManager<IdentityUser>(storeDB);
            IdentityUser user = managerDB.FindByEmail(email);

            if (managerDB.CheckPassword(user, password))
            {
                return user;
            }

            return null;
        }

        public ActionResult EncryptMessage(String email, String password, String message)
        {
            object result = new object();


            return Json(result, JsonRequestBehavior.AllowGet);
        }


        //public ActionResult GetServicesUSA(String email, String password)
        //{

        //    ApplicationDbContext db = new ApplicationDbContext();
        //    object result = new object();

        //    if (GetUser(email, password) != null)
        //    {
        //        result = db.servicios.ToList();
        //    }
        //    else
        //    {
        //        result = new Dictionary<string, string>() {
        //            { "Error", "check credentials" }
        //        };
        //    }

        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        //public ActionResult GetFeeForService(String email, String password, String SKU)
        //{

        //    ApplicationDbContext db = new ApplicationDbContext();
        //    object result = new object();

        //    if (GetUser(email, password) != null)
        //    {
        //        result = db.servicios.FirstOrDefault(x => x.SKU == SKU);
        //    }
        //    else
        //    {
        //        result = new Dictionary<string, string>() {
        //            { "Error", "check credentials" }
        //        };
        //    }

        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        #endregion methods

        #region encryption

        #region views

        public ActionResult Encryption()
        {
            ApplicationDbContext db = new ApplicationDbContext();

            String demoText = "This is the demo text of an encrypted message with the given keys.";

            TestModelView model = new TestModelView();

            EncryptionModel encrypModel = new EncryptionModel();
            encrypModel.GenerateKeys();


            model.Text = demoText;
            model.Encripted = encrypModel.Encript(demoText);
            model.Decripted = encrypModel.DeEncrypt(model.Encripted);
            model.PrivateKey = encrypModel.Private_key;
            model.PublicKey = encrypModel.Public_key;

            return View(model);
        }

        #endregion views

        #region post methods

        [HttpPost]
        public async Task<HttpResponseMessage> Post()
        {
            try
            {

                Stream req = Request.InputStream;
                req.Seek(0, System.IO.SeekOrigin.Begin);

                string json = new StreamReader(req).ReadToEnd();

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        #endregion post methods

        #region class methods

        /// <summary>
        /// this test encryption with keys
        /// </summary>
        public void TestFunctions()
        {
            Guid userId = Guid.Parse("19b816b4-557b-4021-9a22-be17473617d3");
            Guid serviceId = Guid.Parse("5AE94AA6-7999-4E03-ACFA-AB0D435CBE7C");

            //Fee.GetFeeForServiceByUser(userId, serviceId, PlatformModel.Keys.Enpadi);

            //Fee.AddFeesStandarUser(socio.SocioID.Enpadi, PlatformModel.Keys.EnpadiKiosko, userId);
            //Fee.AddFeesStandarUser(socio.SocioID.Enpadi, PlatformModel.Keys.Enpadi, userId);

            //Fee.AddFeesKiskoStandarUser(socio.SocioID.Enpadi, userId);

            //Fee.AddFeesEnpadiStandarUser(socio.SocioID.Enpadi, userId);

            //var resultKioskp = Fee.GetFeeForServiceByKioskoUser(userId, serviceId, PlatformModel.Keys.EnpadiKiosko);

            //var resultEnpadi = Fee.GetFeeForServiceByKioskoUser(userId, serviceId, PlatformModel.Keys.Enpadi);

            string test = "test";
        }
        #endregion class methods

        #endregion encryption

    }
}
