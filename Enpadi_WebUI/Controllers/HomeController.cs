﻿using Enpadi_WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System.Collections;
using System.Threading.Tasks;


namespace Enpadi_WebUI.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {

        private ApplicationDbContext db = new ApplicationDbContext();


        public ActionResult Index(int? periodo, string skin)
        {
            skin = "clean";

            if (User.Identity.GetUserId() != String.Empty && !User.IsInRole(SecurityRoles.Administrador))
            {

                var context = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
                var store = new UserStore<IdentityUser>(context);
                var manager = new UserManager<IdentityUser>(store);
                var user = manager.FindById<IdentityUser, String>(User.Identity.GetUserId());


                //TBBM: i need to confirm the email before login
                //if (user.EmailConfirmed != true)
                //{
                //    return RedirectToAction("PleaseConfirm", "Account", new { skin = skin });

                //}
            }

            ViewModelInicio vm = new ViewModelInicio();
            string strSQL = string.Empty;

            vm.periodo = 0;
            if (periodo != null)
                vm.periodo = (int)periodo;

            // Skin Aplicacion
            strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (User.IsInRole(SecurityRoles.Concentradora))
            {
                return RedirectToAction("Index", "SantiagoPredial");
            }

            if (User.IsInRole(SecurityRoles.Usuario) || User.IsInRole(SecurityRoles.Concentradora))
            {
                string currentUserId = User.Identity.GetUserId();
                tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
                int tID = usuario.tarjetaID;

                if (User.IsInRole(SecurityRoles.Concentradora))
                    vm.perfil = SecurityRoles.Concentradora;
                else
                    vm.perfil = SecurityRoles.Usuario;

                // MONTOS DIARIOS POR METODO DE PAGO | Visa/MasterCard
                strSQL = " SELECT Dia, Socio, DATEADD(DD, Dia-1, DATEADD(MM, DATEDIFF(MM, 0, GETDATE()), 0)) As Fecha, Mes, COALESCE(Periodo,Dia) As Periodo, COALESCE(Transacciones,0) As Transacciones, COALESCE(Abono,0) As Abono, COALESCE(Cargo,0) As Cargo ";
                strSQL += " FROM Dias d ";
                strSQL += " LEFT JOIN ( ";
                strSQL += "   SELECT s.nombreComercial As Socio, CAST(FLOOR(CAST(t.fechaRegistro as float)) as DATETIME) As Fecha, CONVERT(VARCHAR, DATEPART(MONTH, t.FechaRegistro)) As Mes, DATEPART(DAY, t.fechaRegistro) As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(t.abonoTotal),0) As Abono, COALESCE(SUM(t.cargoTotal * -1),0) As Cargo ";
                strSQL += "   FROM Socios s ";
                strSQL += "   LEFT JOIN Transacciones t ON s.socioID = t.socioID ";
                strSQL += "   WHERE t.fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), t.FechaRegistro) = " + vm.periodo.ToString() + " AND s.socioID = 1 AND t.tarjetaID = " + tID.ToString();
                strSQL += "   GROUP BY s.socioID, s.nombreComercial, CAST(FLOOR(CAST(t.fechaRegistro AS float)) as DATETIME), DATEPART(MONTH, t.FechaRegistro), DATEPART(DAY, t.fechaRegistro) ";
                strSQL += " ) As data ON d.dia = data.Periodo ";
                strSQL += " WHERE Dia <= DATEPART(DD, EOMONTH(DATEADD(MONTH," + vm.periodo.ToString() + ", GETDATE()))) ";
                strSQL += " ORDER BY Dia ";
                IEnumerable<resumen> visamc = db.Database.SqlQuery<resumen>(strSQL);
                vm.VisaMC = visamc.ToList();

                // MONTOS DIARIOS POR METODO DE PAGO | American Express
                strSQL = " SELECT Dia, Socio, DATEADD(DD, Dia-1, DATEADD(MM, DATEDIFF(MM, 0, GETDATE()), 0)) As Fecha, Mes, COALESCE(Periodo,Dia) As Periodo, COALESCE(Transacciones,0) As Transacciones, COALESCE(Abono,0) As Abono, COALESCE(Cargo,0) As Cargo ";
                strSQL += " FROM Dias d ";
                strSQL += " LEFT JOIN ( ";
                strSQL += "   SELECT s.nombreComercial As Socio, CAST(FLOOR(CAST(t.fechaRegistro as float)) as DATETIME) As Fecha, CONVERT(VARCHAR, DATEPART(MONTH, t.FechaRegistro)) As Mes, DATEPART(DAY, t.fechaRegistro) As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(t.abonoTotal),0) As Abono, COALESCE(SUM(t.cargoTotal * -1),0) As Cargo ";
                strSQL += " 	FROM Socios s ";
                strSQL += " 	LEFT JOIN Transacciones t ON s.socioID = t.socioID ";
                strSQL += " 	WHERE t.fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), t.FechaRegistro) = " + vm.periodo.ToString() + " AND s.socioID = 2 AND t.tarjetaID = " + tID.ToString();
                strSQL += " 	GROUP BY s.socioID, s.nombreComercial, CAST(FLOOR(CAST(t.fechaRegistro AS float)) as DATETIME), DATEPART(MONTH, t.FechaRegistro), DATEPART(DAY, t.fechaRegistro) ";
                strSQL += " ) As data ON d.dia = data.Periodo ";
                strSQL += " WHERE Dia <= DATEPART(DD, EOMONTH(DATEADD(MONTH," + vm.periodo.ToString() + ", GETDATE()))) ";
                strSQL += " ORDER BY Dia ";
                IEnumerable<resumen> amex = db.Database.SqlQuery<resumen>(strSQL);
                vm.Amex = amex.ToList();

                // MONTOS DIARIOS POR METODO DE PAGO | Banco
                strSQL = " SELECT Dia, Socio, DATEADD(DD, Dia-1, DATEADD(MM, DATEDIFF(MM, 0, GETDATE()), 0)) As Fecha, Mes, COALESCE(Periodo,Dia) As Periodo, COALESCE(Transacciones,0) As Transacciones, COALESCE(Abono,0) As Abono, COALESCE(Cargo,0) As Cargo ";
                strSQL += " FROM Dias d ";
                strSQL += " LEFT JOIN ( ";
                strSQL += "   SELECT s.nombreComercial As Socio, CAST(FLOOR(CAST(t.fechaRegistro as float)) as DATETIME) As Fecha, CONVERT(VARCHAR, DATEPART(MONTH, t.FechaRegistro)) As Mes, DATEPART(DAY, t.fechaRegistro) As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(t.abonoTotal),0) As Abono, COALESCE(SUM(t.cargoTotal * -1),0) As Cargo ";
                strSQL += "   FROM Socios s ";
                strSQL += "   LEFT JOIN Transacciones t ON s.socioID = t.socioID ";
                strSQL += "   WHERE t.fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), t.FechaRegistro) = " + vm.periodo.ToString() + " AND s.socioID = 3 AND t.tarjetaID = " + tID.ToString();
                strSQL += "   GROUP BY s.socioID, s.nombreComercial, CAST(FLOOR(CAST(t.fechaRegistro AS float)) as DATETIME), DATEPART(MONTH, t.FechaRegistro), DATEPART(DAY, t.fechaRegistro) ";
                strSQL += " ) As data ON d.dia = data.Periodo ";
                strSQL += " WHERE Dia <= DATEPART(DD, EOMONTH(DATEADD(MONTH," + vm.periodo.ToString() + ", GETDATE()))) ";
                strSQL += " ORDER BY Dia ";
                IEnumerable<resumen> banco = db.Database.SqlQuery<resumen>(strSQL);
                vm.Banco = banco.ToList();

                // MONTOS DIARIOS POR METODO DE PAGO | Tienda
                strSQL = " SELECT Dia, Socio, DATEADD(DD, Dia-1, DATEADD(MM, DATEDIFF(MM, 0, GETDATE()), 0)) As Fecha, Mes, COALESCE(Periodo,Dia) As Periodo, COALESCE(Transacciones,0) As Transacciones, COALESCE(Abono,0) As Abono, COALESCE(Cargo,0) As Cargo ";
                strSQL += " FROM Dias d ";
                strSQL += " LEFT JOIN ( ";
                strSQL += "   SELECT s.nombreComercial As Socio, CAST(FLOOR(CAST(t.fechaRegistro as float)) as DATETIME) As Fecha, CONVERT(VARCHAR, DATEPART(MONTH, t.FechaRegistro)) As Mes, DATEPART(DAY, t.fechaRegistro) As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(t.abonoTotal),0) As Abono, COALESCE(SUM(t.cargoTotal * -1),0) As Cargo ";
                strSQL += "   FROM Socios s ";
                strSQL += "   LEFT JOIN Transacciones t ON s.socioID = t.socioID ";
                strSQL += "   WHERE t.fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), t.FechaRegistro) = " + vm.periodo.ToString() + " AND s.socioID = 4 AND t.tarjetaID = " + tID.ToString();
                strSQL += "   GROUP BY s.socioID, s.nombreComercial, CAST(FLOOR(CAST(t.fechaRegistro AS float)) as DATETIME), DATEPART(MONTH, t.FechaRegistro), DATEPART(DAY, t.fechaRegistro) ";
                strSQL += " ) As data ON d.dia = data.Periodo ";
                strSQL += " WHERE Dia <= DATEPART(DD, EOMONTH(DATEADD(MONTH," + vm.periodo.ToString() + ", GETDATE()))) ";
                strSQL += " ORDER BY Dia ";
                IEnumerable<resumen> tienda = db.Database.SqlQuery<resumen>(strSQL);
                vm.Tienda = tienda.ToList();

                // MONTOS DIARIOS POR METODO DE PAGO | Tiempo Aire
                strSQL = " SELECT Dia, Socio, DATEADD(DD, Dia-1, DATEADD(MM, DATEDIFF(MM, 0, GETDATE()), 0)) As Fecha, Mes, COALESCE(Periodo,Dia) As Periodo, COALESCE(Transacciones,0) As Transacciones, COALESCE(Abono,0) As Abono, COALESCE(Cargo,0) As Cargo ";
                strSQL += " FROM Dias d ";
                strSQL += " LEFT JOIN ( ";
                strSQL += "   SELECT s.nombreComercial As Socio, CAST(FLOOR(CAST(t.fechaRegistro as float)) as DATETIME) As Fecha, CONVERT(VARCHAR, DATEPART(MONTH, t.FechaRegistro)) As Mes, DATEPART(DAY, t.fechaRegistro) As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(t.abonoTotal),0) As Abono, COALESCE(SUM(t.cargoTotal * -1),0) As Cargo ";
                strSQL += "   FROM Socios s ";
                strSQL += "   LEFT JOIN Transacciones t ON s.socioID = t.socioID ";
                strSQL += "   WHERE t.fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), t.FechaRegistro) = " + vm.periodo.ToString() + " AND s.socioID = 5 AND t.tarjetaID = " + tID.ToString();
                strSQL += "   GROUP BY s.socioID, s.nombreComercial, CAST(FLOOR(CAST(t.fechaRegistro AS float)) as DATETIME), DATEPART(MONTH, t.FechaRegistro), DATEPART(DAY, t.fechaRegistro) ";
                strSQL += " ) As data ON d.dia = data.Periodo ";
                strSQL += " WHERE Dia <= DATEPART(DD, EOMONTH(DATEADD(MONTH," + vm.periodo.ToString() + ", GETDATE()))) ";
                strSQL += " ORDER BY Dia ";
                IEnumerable<resumen> tae = db.Database.SqlQuery<resumen>(strSQL);
                vm.TAE = tae.ToList();

                //// MONTOS DIARIOS POR METODO DE PAGO | Pago de Servicios
                strSQL = " SELECT Dia, Socio, DATEADD(DD, Dia-1, DATEADD(MM, DATEDIFF(MM, 0, GETDATE()), 0)) As Fecha, Mes, COALESCE(Periodo,Dia) As Periodo, COALESCE(Transacciones,0) As Transacciones, COALESCE(Abono,0) As Abono, COALESCE(Cargo,0) As Cargo ";
                strSQL += " FROM Dias d ";
                strSQL += " LEFT JOIN ( ";
                strSQL += "   SELECT s.nombreComercial As Socio, CAST(FLOOR(CAST(t.fechaRegistro as float)) as DATETIME) As Fecha, CONVERT(VARCHAR, DATEPART(MONTH, t.FechaRegistro)) As Mes, DATEPART(DAY, t.fechaRegistro) As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(t.abonoTotal),0) As Abono, COALESCE(SUM(t.cargoTotal * -1),0) As Cargo ";
                strSQL += "   FROM Socios s ";
                strSQL += "   LEFT JOIN Transacciones t ON s.socioID = t.socioID ";
                strSQL += "   WHERE t.fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), t.FechaRegistro) = " + vm.periodo.ToString() + " AND s.socioID = 6 AND t.tarjetaID = " + tID.ToString();
                strSQL += "   GROUP BY s.socioID, s.nombreComercial, CAST(FLOOR(CAST(t.fechaRegistro AS float)) as DATETIME), DATEPART(MONTH, t.FechaRegistro), DATEPART(DAY, t.fechaRegistro) ";
                strSQL += " ) As data ON d.dia = data.Periodo ";
                strSQL += " WHERE Dia <= DATEPART(DD, EOMONTH(DATEADD(MONTH," + vm.periodo.ToString() + ", GETDATE()))) ";
                strSQL += " ORDER BY Dia ";
                IEnumerable<resumen> servicios = db.Database.SqlQuery<resumen>(strSQL);
                vm.Servicios = servicios.ToList();

                // MONTOS DIARIOS POR METODO DE PAGO | Transferencias
                strSQL = " SELECT Dia, Socio, DATEADD(DD, Dia-1, DATEADD(MM, DATEDIFF(MM, 0, GETDATE()), 0)) As Fecha, Mes, COALESCE(Periodo,Dia) As Periodo, COALESCE(Transacciones,0) As Transacciones, COALESCE(Abono,0) As Abono, COALESCE(Cargo,0) As Cargo "
                       + " FROM Dias d "
                       + " LEFT JOIN ( "
                       + " SELECT '' As Socio, CAST(FLOOR(CAST(t.fechaRegistro as float)) as DATETIME) As Fecha, CONVERT(VARCHAR, DATEPART(MONTH, t.FechaRegistro)) As Mes, DATEPART(DAY, t.fechaRegistro) As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(t.abono1),0) As Abono, COALESCE(SUM(t.cargo1 * -1),0) As Cargo "
                       + " FROM Transacciones t "
                       + " WHERE t.fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), t.FechaRegistro) = " + vm.periodo.ToString() + " AND t.socioID = 0 AND t.tarjetaID = " + tID.ToString()
                       + " GROUP BY socioID, CAST(FLOOR(CAST(t.fechaRegistro AS float)) as DATETIME), DATEPART(MONTH, t.FechaRegistro), DATEPART(DAY, t.fechaRegistro) "
                       + " ) As data ON d.dia = data.Periodo "
                       + " WHERE Dia <= DATEPART(DD, EOMONTH(DATEADD(MONTH," + vm.periodo.ToString() + ", GETDATE()))) "
                       + " ORDER BY Dia ";
                IEnumerable<resumen> transfer = db.Database.SqlQuery<resumen>(strSQL);
                vm.Transferencias = transfer.ToList();

                // TIEMPO AIRE | TELCEL
                strSQL = String.Format(" SELECT Periodo, Transacciones, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
                strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Abono, Cargo ");
                strSQL += String.Format(" FROM ( ");
                strSQL += String.Format(" 	SELECT -12 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -11 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -10 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -9 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -8 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -7 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -6 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -5 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -4 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -3 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -2 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -1 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT 0 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ", tID);
                strSQL += String.Format(" ) As Detalle ");
                IEnumerable<resumen> telcel = db.Database.SqlQuery<resumen>(strSQL);
                vm.Telcel = telcel.ToList();

                // TIEMPO AIRE | MOVISTAR
                strSQL = String.Format(" SELECT Periodo, Transacciones, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
                strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Abono, Cargo ");
                strSQL += String.Format(" FROM ( ");
                strSQL += String.Format(" 	SELECT -12 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -11 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -10 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -9 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -8 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -7 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -6 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -5 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -4 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -3 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -2 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -1 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT 0 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ", tID);
                strSQL += String.Format(" ) As Detalle ");
                IEnumerable<resumen> movistar = db.Database.SqlQuery<resumen>(strSQL);
                vm.Movistar = movistar.ToList();

                // TIEMPO AIRE | IUSACELL
                strSQL = String.Format(" SELECT Periodo, Transacciones, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
                strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Abono, Cargo ");
                strSQL += String.Format(" FROM ( ");
                strSQL += String.Format(" 	SELECT -12 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -11 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -10 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -9 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -8 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -7 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -6 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -5 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -4 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -3 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -2 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -1 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT 0 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ", tID);
                strSQL += String.Format(" ) As Detalle ");
                IEnumerable<resumen> iusacell = db.Database.SqlQuery<resumen>(strSQL);
                vm.Iusacell = iusacell.ToList();

                // TIEMPO AIRE | UNEFON
                strSQL = String.Format(" SELECT Periodo, Transacciones, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
                strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Abono, Cargo ");
                strSQL += String.Format(" FROM ( ");
                strSQL += String.Format(" 	SELECT -12 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -11 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -10 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -9 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -8 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -7 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -6 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -5 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -4 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -3 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -2 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -1 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT 0 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ", tID);
                strSQL += String.Format(" ) As Detalle ");
                IEnumerable<resumen> unefon = db.Database.SqlQuery<resumen>(strSQL);
                vm.Unefon = unefon.ToList();

                // TIEMPO AIRE | NEXTEL
                strSQL = String.Format(" SELECT Periodo, Transacciones, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
                strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Abono, Cargo ");
                strSQL += String.Format(" FROM ( ");
                strSQL += String.Format(" 	SELECT -12 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -11 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -10 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -9 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -8 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -7 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -6 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -5 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -4 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -3 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -2 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -1 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT 0 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ", tID);
                strSQL += String.Format(" ) As Detalle ");
                IEnumerable<resumen> nextel = db.Database.SqlQuery<resumen>(strSQL);
                vm.Nextel = nextel.ToList();

                // TIEMPO AIRE | VIRGIN
                strSQL = String.Format(" SELECT Periodo, Transacciones, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
                strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Abono, Cargo ");
                strSQL += String.Format(" FROM ( ");
                strSQL += String.Format(" 	SELECT -12 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -11 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -10 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -9 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -8 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -7 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -6 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -5 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -4 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -3 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -2 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -1 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT 0 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ", tID);
                strSQL += String.Format(" ) As Detalle ");
                IEnumerable<resumen> virgin = db.Database.SqlQuery<resumen>(strSQL);
                vm.Virgin = virgin.ToList();

                // PAGO DE SERVICIOS | Montos Procesados
                strSQL = String.Format(" SELECT Periodo, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
                strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Abono, Cargo ");
                strSQL += String.Format(" FROM ( ");
                strSQL += String.Format(" 	SELECT -12 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -11 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -10 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -9 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -8 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -7 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -6 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -5 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -4 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -3 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -2 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -1 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT 0 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ", tID);
                strSQL += String.Format(" ) As Detalle ");
                IEnumerable<resumen> serviciosMonto = db.Database.SqlQuery<resumen>(strSQL);
                vm.ServiciosMonto = serviciosMonto.ToList();

                // PAGO DE SERVICIOS | Total de Transacciones
                strSQL = String.Format(" SELECT Periodo, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
                strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Total ");
                strSQL += String.Format(" FROM ( ");
                strSQL += String.Format(" 	SELECT -12 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -11 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -10 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -9 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -8 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -7 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -6 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -5 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -4 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -3 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -2 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -1 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ", tID);
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT 0 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND tarjetaID = {0} AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ", tID);
                strSQL += String.Format(" ) As Detalle ");
                IEnumerable<resumenUnitario> serviciosTransacciones = db.Database.SqlQuery<resumenUnitario>(strSQL);
                vm.ServiciosTransacciones = serviciosTransacciones.ToList();
            }

            if (User.IsInRole(SecurityRoles.Administrador))
            {
                vm.perfil = SecurityRoles.Administrador;

                // Numero de Usuarios
                strSQL = String.Format("SELECT COALESCE(COUNT(*),0) AS total FROM Tarjetas WHERE fechaBaja IS NULL AND tcAsignada = 1 AND tcActiva = 1");
                IEnumerable<contador> totales = db.Database.SqlQuery<contador>(strSQL);
                vm.usuariosTotales = totales.ToList().FirstOrDefault().total;

                // Usuarios Activos
                strSQL = String.Format("SELECT COALESCE(COUNT(DISTINCT(tarjetaID)),0) AS total FROM Transacciones  WHERE fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) <= -2");
                IEnumerable<contador> activos = db.Database.SqlQuery<contador>(strSQL);
                vm.usuariosActivos = activos.ToList().FirstOrDefault().total;

                // Usuarios Inactivos
                vm.usuariosInactivos = vm.usuariosTotales - vm.usuariosActivos;

                //RESUMEN DE ENTRADAS Y SALIDAS | Visa/MasterCard
                strSQL = " SELECT Dia, Socio, DATEADD(DD, Dia-1, DATEADD(MM, DATEDIFF(MM, 0, GETDATE()), 0)) As Fecha, Mes, COALESCE(Periodo,Dia) As Periodo, COALESCE(Transacciones,0) As Transacciones, COALESCE(Abono,0) As Abono, COALESCE(Cargo,0) As Cargo ";
                strSQL += " FROM Dias d ";
                strSQL += " LEFT JOIN ( ";
                strSQL += " 	SELECT s.nombreComercial As Socio, CAST(FLOOR(CAST(t.fechaRegistro as float)) as DATETIME) As Fecha, CONVERT(VARCHAR, DATEPART(MONTH, t.FechaRegistro)) As Mes, DATEPART(DAY, t.fechaRegistro) As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(t.abonoTotal),0) As Abono, COALESCE(SUM(t.cargoTotal * -1),0) As Cargo ";
                strSQL += " 	FROM Socios s ";
                strSQL += " 	LEFT JOIN Transacciones t ON s.socioID = t.socioID ";
                strSQL += " 	WHERE t.fechaBaja IS NULL AND t.tarjetaID > 0 AND DATEDIFF(MONTH, GETDATE(), t.FechaRegistro) = 0 AND s.socioID = 1 ";
                strSQL += " 	GROUP BY s.socioID, s.nombreComercial, CAST(FLOOR(CAST(t.fechaRegistro AS float)) as DATETIME), DATEPART(MONTH, t.FechaRegistro), DATEPART(DAY, t.fechaRegistro) ";
                strSQL += " ) As data ON d.dia = data.Periodo ";
                strSQL += " WHERE Dia <= DATEPART(DD, GETDATE()) ";
                strSQL += " ORDER BY Dia ";
                IEnumerable<resumen> visamc = db.Database.SqlQuery<resumen>(strSQL);
                vm.VisaMC = visamc.ToList();

                //RESUMEN DE ENTRADAS Y SALIDAS | American Express
                strSQL = " SELECT Dia, Socio, DATEADD(DD, Dia-1, DATEADD(MM, DATEDIFF(MM, 0, GETDATE()), 0)) As Fecha, Mes, COALESCE(Periodo,Dia) As Periodo, COALESCE(Transacciones,0) As Transacciones, COALESCE(Abono,0) As Abono, COALESCE(Cargo,0) As Cargo ";
                strSQL += " FROM Dias d ";
                strSQL += " LEFT JOIN ( ";
                strSQL += " 	SELECT s.nombreComercial As Socio, CAST(FLOOR(CAST(t.fechaRegistro as float)) as DATETIME) As Fecha, CONVERT(VARCHAR, DATEPART(MONTH, t.FechaRegistro)) As Mes, DATEPART(DAY, t.fechaRegistro) As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(t.abonoTotal),0) As Abono, COALESCE(SUM(t.cargoTotal * -1),0) As Cargo ";
                strSQL += " 	FROM Socios s ";
                strSQL += " 	LEFT JOIN Transacciones t ON s.socioID = t.socioID ";
                strSQL += " 	WHERE t.fechaBaja IS NULL AND t.tarjetaID > 0 AND DATEDIFF(MONTH, GETDATE(), t.FechaRegistro) = 0 AND s.socioID = 2 ";
                strSQL += " 	GROUP BY s.socioID, s.nombreComercial, CAST(FLOOR(CAST(t.fechaRegistro AS float)) as DATETIME), DATEPART(MONTH, t.FechaRegistro), DATEPART(DAY, t.fechaRegistro) ";
                strSQL += " ) As data ON d.dia = data.Periodo ";
                strSQL += " WHERE Dia <= DATEPART(DD, GETDATE()) ";
                strSQL += " ORDER BY Dia ";
                IEnumerable<resumen> amex = db.Database.SqlQuery<resumen>(strSQL);
                vm.Amex = amex.ToList();

                //RESUMEN DE ENTRADAS Y SALIDAS | Banco
                strSQL = " SELECT Dia, Socio, DATEADD(DD, Dia-1, DATEADD(MM, DATEDIFF(MM, 0, GETDATE()), 0)) As Fecha, Mes, COALESCE(Periodo,Dia) As Periodo, COALESCE(Transacciones,0) As Transacciones, COALESCE(Abono,0) As Abono, COALESCE(Cargo,0) As Cargo ";
                strSQL += " FROM Dias d ";
                strSQL += " LEFT JOIN ( ";
                strSQL += " 	SELECT s.nombreComercial As Socio, CAST(FLOOR(CAST(t.fechaRegistro as float)) as DATETIME) As Fecha, CONVERT(VARCHAR, DATEPART(MONTH, t.FechaRegistro)) As Mes, DATEPART(DAY, t.fechaRegistro) As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(t.abonoTotal),0) As Abono, COALESCE(SUM(t.cargoTotal * -1),0) As Cargo ";
                strSQL += " 	FROM Socios s ";
                strSQL += " 	LEFT JOIN Transacciones t ON s.socioID = t.socioID ";
                strSQL += " 	WHERE t.fechaBaja IS NULL AND t.tarjetaID > 0 AND DATEDIFF(MONTH, GETDATE(), t.FechaRegistro) = 0 AND s.socioID = 3 ";
                strSQL += " 	GROUP BY s.socioID, s.nombreComercial, CAST(FLOOR(CAST(t.fechaRegistro AS float)) as DATETIME), DATEPART(MONTH, t.FechaRegistro), DATEPART(DAY, t.fechaRegistro) ";
                strSQL += " ) As data ON d.dia = data.Periodo ";
                strSQL += " WHERE Dia <= DATEPART(DD, GETDATE()) ";
                strSQL += " ORDER BY Dia ";
                IEnumerable<resumen> banco = db.Database.SqlQuery<resumen>(strSQL);
                vm.Banco = banco.ToList();

                //RESUMEN DE ENTRADAS Y SALIDAS | Tienda
                strSQL = " SELECT Dia, Socio, DATEADD(DD, Dia-1, DATEADD(MM, DATEDIFF(MM, 0, GETDATE()), 0)) As Fecha, Mes, COALESCE(Periodo,Dia) As Periodo, COALESCE(Transacciones,0) As Transacciones, COALESCE(Abono,0) As Abono, COALESCE(Cargo,0) As Cargo ";
                strSQL += " FROM Dias d ";
                strSQL += " LEFT JOIN ( ";
                strSQL += " 	SELECT s.nombreComercial As Socio, CAST(FLOOR(CAST(t.fechaRegistro as float)) as DATETIME) As Fecha, CONVERT(VARCHAR, DATEPART(MONTH, t.FechaRegistro)) As Mes, DATEPART(DAY, t.fechaRegistro) As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(t.abonoTotal),0) As Abono, COALESCE(SUM(t.cargoTotal * -1),0) As Cargo ";
                strSQL += " 	FROM Socios s ";
                strSQL += " 	LEFT JOIN Transacciones t ON s.socioID = t.socioID ";
                strSQL += " 	WHERE t.fechaBaja IS NULL AND t.tarjetaID > 0 AND DATEDIFF(MONTH, GETDATE(), t.FechaRegistro) = 0 AND s.socioID = 4 ";
                strSQL += " 	GROUP BY s.socioID, s.nombreComercial, CAST(FLOOR(CAST(t.fechaRegistro AS float)) as DATETIME), DATEPART(MONTH, t.FechaRegistro), DATEPART(DAY, t.fechaRegistro) ";
                strSQL += " ) As data ON d.dia = data.Periodo ";
                strSQL += " WHERE Dia <= DATEPART(DD, GETDATE()) ";
                strSQL += " ORDER BY Dia ";
                IEnumerable<resumen> tienda = db.Database.SqlQuery<resumen>(strSQL);
                vm.Tienda = tienda.ToList();

                //RESUMEN DE ENTRADAS Y SALIDAS | Tiempo Aire
                strSQL = " SELECT Dia, Socio, DATEADD(DD, Dia-1, DATEADD(MM, DATEDIFF(MM, 0, GETDATE()), 0)) As Fecha, Mes, COALESCE(Periodo,Dia) As Periodo, COALESCE(Transacciones,0) As Transacciones, COALESCE(Abono,0) As Abono, COALESCE(Cargo,0) As Cargo ";
                strSQL += " FROM Dias d ";
                strSQL += " LEFT JOIN ( ";
                strSQL += " 	SELECT s.nombreComercial As Socio, CAST(FLOOR(CAST(t.fechaRegistro as float)) as DATETIME) As Fecha, CONVERT(VARCHAR, DATEPART(MONTH, t.FechaRegistro)) As Mes, DATEPART(DAY, t.fechaRegistro) As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(t.abonoTotal),0) As Abono, COALESCE(SUM(t.cargoTotal * -1),0) As Cargo ";
                strSQL += " 	FROM Socios s ";
                strSQL += " 	LEFT JOIN Transacciones t ON s.socioID = t.socioID ";
                strSQL += " 	WHERE t.fechaBaja IS NULL AND t.tarjetaID > 0 AND DATEDIFF(MONTH, GETDATE(), t.FechaRegistro) = 0 AND s.socioID = 5 ";
                strSQL += " 	GROUP BY s.socioID, s.nombreComercial, CAST(FLOOR(CAST(t.fechaRegistro AS float)) as DATETIME), DATEPART(MONTH, t.FechaRegistro), DATEPART(DAY, t.fechaRegistro) ";
                strSQL += " ) As data ON d.dia = data.Periodo ";
                strSQL += " WHERE Dia <= DATEPART(DD, GETDATE()) ";
                strSQL += " ORDER BY Dia ";
                IEnumerable<resumen> tae = db.Database.SqlQuery<resumen>(strSQL);
                vm.TAE = tae.ToList();

                //RESUMEN DE ENTRADAS Y SALIDAS | Pago de Servicios
                strSQL = " SELECT Dia, Socio, DATEADD(DD, Dia-1, DATEADD(MM, DATEDIFF(MM, 0, GETDATE()), 0)) As Fecha, Mes, COALESCE(Periodo,Dia) As Periodo, COALESCE(Transacciones,0) As Transacciones, COALESCE(Abono,0) As Abono, COALESCE(Cargo,0) As Cargo ";
                strSQL += " FROM Dias d ";
                strSQL += " LEFT JOIN ( ";
                strSQL += " 	SELECT s.nombreComercial As Socio, CAST(FLOOR(CAST(t.fechaRegistro as float)) as DATETIME) As Fecha, CONVERT(VARCHAR, DATEPART(MONTH, t.FechaRegistro)) As Mes, DATEPART(DAY, t.fechaRegistro) As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(t.abonoTotal),0) As Abono, COALESCE(SUM(t.cargoTotal * -1),0) As Cargo ";
                strSQL += " 	FROM Socios s ";
                strSQL += " 	LEFT JOIN Transacciones t ON s.socioID = t.socioID ";
                strSQL += " 	WHERE t.fechaBaja IS NULL AND t.tarjetaID > 0 AND DATEDIFF(MONTH, GETDATE(), t.FechaRegistro) = 0 AND s.socioID = 6 ";
                strSQL += " 	GROUP BY s.socioID, s.nombreComercial, CAST(FLOOR(CAST(t.fechaRegistro AS float)) as DATETIME), DATEPART(MONTH, t.FechaRegistro), DATEPART(DAY, t.fechaRegistro) ";
                strSQL += " ) As data ON d.dia = data.Periodo ";
                strSQL += " WHERE Dia <= DATEPART(DD, GETDATE()) ";
                strSQL += " ORDER BY Dia ";
                IEnumerable<resumen> servicios = db.Database.SqlQuery<resumen>(strSQL);
                vm.Servicios = servicios.ToList();

                // MONTOS DIARIOS POR METODO DE PAGO | Transferencias
                strSQL = " SELECT Dia, Socio, DATEADD(DD, Dia-1, DATEADD(MM, DATEDIFF(MM, 0, GETDATE()), 0)) As Fecha, Mes, COALESCE(Periodo,Dia) As Periodo, COALESCE(Transacciones,0) As Transacciones, COALESCE(Abono,0) As Abono, COALESCE(Cargo,0) As Cargo "
                       + " FROM Dias d "
                       + " LEFT JOIN ( "
                       + " SELECT '' As Socio, CAST(FLOOR(CAST(t.fechaRegistro as float)) as DATETIME) As Fecha, CONVERT(VARCHAR, DATEPART(MONTH, t.FechaRegistro)) As Mes, DATEPART(DAY, t.fechaRegistro) As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(t.abono1),0) As Abono, COALESCE(SUM(t.cargo1 * -1),0) As Cargo "
                       + " FROM Transacciones t "
                       + " WHERE t.fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), t.FechaRegistro) = 0 AND t.socioID = 0 "
                       + " GROUP BY socioID, CAST(FLOOR(CAST(t.fechaRegistro AS float)) as DATETIME), DATEPART(MONTH, t.FechaRegistro), DATEPART(DAY, t.fechaRegistro) "
                       + " ) As data ON d.dia = data.Periodo "
                       + " WHERE Dia <= DATEPART(DD, GETDATE()) "
                       + " ORDER BY Dia ";
                IEnumerable<resumen> transfer = db.Database.SqlQuery<resumen>(strSQL);
                vm.Transferencias = transfer.ToList();

                // TIEMPO AIRE | TELCEL
                strSQL = String.Format(" SELECT Periodo, Transacciones, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
                strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Abono, Cargo ");
                strSQL += String.Format(" FROM ( ");
                strSQL += String.Format(" 	SELECT -12 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -11 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -10 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -9 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -8 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -7 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -6 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -5 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -4 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -3 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -2 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -1 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT 0 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'TELCEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ");
                strSQL += String.Format(" ) As Detalle ");
                IEnumerable<resumen> telcel = db.Database.SqlQuery<resumen>(strSQL);
                vm.Telcel = telcel.ToList();

                // TIEMPO AIRE | MOVISTAR
                strSQL = String.Format(" SELECT Periodo, Transacciones, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
                strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Abono, Cargo ");
                strSQL += String.Format(" FROM ( ");
                strSQL += String.Format(" 	SELECT -12 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -11 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -10 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -9 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -8 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -7 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -6 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -5 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -4 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -3 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -2 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -1 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT 0 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'MOVISTAR' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ");
                strSQL += String.Format(" ) As Detalle ");
                IEnumerable<resumen> movistar = db.Database.SqlQuery<resumen>(strSQL);
                vm.Movistar = movistar.ToList();

                // TIEMPO AIRE | IUSACELL
                strSQL = String.Format(" SELECT Periodo, Transacciones, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
                strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Abono, Cargo ");
                strSQL += String.Format(" FROM ( ");
                strSQL += String.Format(" 	SELECT -12 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -11 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -10 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -9 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -8 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -7 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -6 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -5 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -4 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -3 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -2 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -1 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT 0 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'IUSACELL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ");
                strSQL += String.Format(" ) As Detalle ");
                IEnumerable<resumen> iusacell = db.Database.SqlQuery<resumen>(strSQL);
                vm.Iusacell = iusacell.ToList();

                // TIEMPO AIRE | UNEFON
                strSQL = String.Format(" SELECT Periodo, Transacciones, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
                strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Abono, Cargo ");
                strSQL += String.Format(" FROM ( ");
                strSQL += String.Format(" 	SELECT -12 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -11 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -10 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -9 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -8 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -7 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -6 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -5 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -4 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -3 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -2 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -1 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT 0 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'UNEFON' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ");
                strSQL += String.Format(" ) As Detalle ");
                IEnumerable<resumen> unefon = db.Database.SqlQuery<resumen>(strSQL);
                vm.Unefon = unefon.ToList();

                // TIEMPO AIRE | NEXTEL
                strSQL = String.Format(" SELECT Periodo, Transacciones, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
                strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Abono, Cargo ");
                strSQL += String.Format(" FROM ( ");
                strSQL += String.Format(" 	SELECT -12 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -11 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -10 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -9 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -8 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -7 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -6 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -5 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -4 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -3 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -2 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -1 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT 0 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'NEXTEL' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ");
                strSQL += String.Format(" ) As Detalle ");
                IEnumerable<resumen> nextel = db.Database.SqlQuery<resumen>(strSQL);
                vm.Nextel = nextel.ToList();

                // TIEMPO AIRE | VIRGIN
                strSQL = String.Format(" SELECT Periodo, Transacciones, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
                strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Abono, Cargo ");
                strSQL += String.Format(" FROM ( ");
                strSQL += String.Format(" 	SELECT -12 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -11 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -10 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -9 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -8 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -7 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -6 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -5 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -4 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -3 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -2 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -1 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT 0 As Periodo, COUNT(*) As Transacciones, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND concepto = 'VIRGIN' AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ");
                strSQL += String.Format(" ) As Detalle ");
                IEnumerable<resumen> virgin = db.Database.SqlQuery<resumen>(strSQL);
                vm.Virgin = virgin.ToList();


                // PAGO DE SERVICIOS | Montos Procesados
                strSQL = String.Format(" SELECT Periodo, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
                strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Abono, Cargo ");
                strSQL += String.Format(" FROM ( ");
                strSQL += String.Format(" 	SELECT -12 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -11 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -10 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -9 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -8 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -7 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -6 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -5 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -4 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -3 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -2 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -1 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT 0 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ");
                strSQL += String.Format(" ) As Detalle ");
                IEnumerable<resumen> serviciosMonto = db.Database.SqlQuery<resumen>(strSQL);
                vm.ServiciosMonto = serviciosMonto.ToList();

                // PAGO DE SERVICIOS | Total de Transacciones
                strSQL = String.Format(" SELECT Periodo, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
                strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Total ");
                strSQL += String.Format(" FROM ( ");
                strSQL += String.Format(" 	SELECT -12 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -11 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -10 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -9 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -8 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -7 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -6 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -5 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -4 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -3 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -2 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -1 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT 0 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 6 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ");
                strSQL += String.Format(" ) As Detalle ");
                IEnumerable<resumenUnitario> serviciosTransacciones = db.Database.SqlQuery<resumenUnitario>(strSQL);
                vm.ServiciosTransacciones = serviciosTransacciones.ToList();

                // TIEMPO AIRE | Montos Procesados
                strSQL = String.Format(" SELECT Periodo, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
                strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Abono, Cargo ");
                strSQL += String.Format(" FROM ( ");
                strSQL += String.Format(" 	SELECT -12 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -11 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -10 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -9 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -8 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -7 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -6 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -5 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -4 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -3 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -2 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -1 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT 0 As Periodo, COALESCE(SUM(abonoTotal),0) As Abono, COALESCE(SUM(cargoTotal),0) As Cargo FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ");
                strSQL += String.Format(" ) As Detalle ");
                IEnumerable<resumen> taeMonto = db.Database.SqlQuery<resumen>(strSQL);
                vm.TaeMonto = taeMonto.ToList();

                // TIEMPO AIRE | Total de Transacciones
                strSQL = String.Format(" SELECT Periodo, CASE MONTH(DATEADD(MONTH, Periodo, GETDATE())) WHEN 1 THEN 'ENE' WHEN 2 THEN 'FEB' WHEN 3 THEN 'MZO' WHEN 4 THEN 'ABR' WHEN 5 THEN 'MAY' WHEN 6 THEN 'JUN' WHEN 7 THEN 'JUL' ");
                strSQL += String.Format(" 	WHEN 8 THEN 'AGO' WHEN 9 THEN 'SEP' WHEN 10 THEN 'OCT' WHEN 11 THEN 'NOV' WHEN 12 THEN 'DIC' END + RIGHT(YEAR(DATEADD(MONTH, Periodo, GETDATE())), 2) As Mes, Total ");
                strSQL += String.Format(" FROM ( ");
                strSQL += String.Format(" 	SELECT -12 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -12 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -11 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -11 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -10 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -10 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -9 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -9 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -8 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -8 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -7 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -7 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -6 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -6 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -5 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -5 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -4 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -4 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -3 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -3 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -2 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -2 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT -1 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = -1 ");
                strSQL += String.Format(" 	UNION ");
                strSQL += String.Format(" 	SELECT 0 As Periodo, COALESCE(COUNT(*),0) As Total FROM Transacciones WHERE socioID = 5 AND tipoID = 2 AND terminalID = 1 AND fechaBaja IS NULL AND DATEDIFF(MONTH, GETDATE(), fechaRegistro) = 0 ");
                strSQL += String.Format(" ) As Detalle ");
                IEnumerable<resumenUnitario> taeTransacciones = db.Database.SqlQuery<resumenUnitario>(strSQL);
                vm.TaeTransacciones = taeTransacciones.ToList();

            }


            return View(vm);
        }

        [ChildActionOnly]
        public ActionResult EnpadiHome(int? periodo)
        {

            //var context = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
            //var store = new UserStore<IdentityUser>(context);
            //var manager = new UserManager<IdentityUser>(store);
            //var user = manager.FindById<IdentityUser, String>(User.Identity.GetUserId());


            ////TBBM: i need to confirm the email before login
            //if (manager.IsEmailConfirmed(user.Id) && !User.IsInRole(SecurityRoles.Administrador))
            //{

            if (User.IsInRole(SecurityRoles.Administrador))
            {
                ViewModelAdministrador viewAdmin = new ViewModelAdministrador();

                string strSQL = String.Format("SELECT COALESCE(SUM(abonoTotal),0) AS Total FROM Transacciones WHERE fechaBaja IS NULL AND origenID = 1 AND tipoID = 1");
                IEnumerable<total> historicoIngreso = db.Database.SqlQuery<total>(strSQL);
                viewAdmin.HistoricoIngreso = historicoIngreso.ToList().FirstOrDefault().Total;

                return PartialView("_IndexAdministrador", viewAdmin);
            }


            if (User.IsInRole(SecurityRoles.Socio))
                return PartialView("_IndexSocio");

            if (User.IsInRole(SecurityRoles.Usuario) || User.IsInRole(SecurityRoles.Concentradora))
            {
                ViewModelEstadoDeCuenta edoCuenta = new ViewModelEstadoDeCuenta();

                edoCuenta.periodo = (int)periodo;

                string currentUserId = User.Identity.GetUserId();
                tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
                int tID = usuario.tarjetaID;

                edoCuenta.usuario = usuario;
                string strSQL = string.Empty;

                strSQL = " SELECT numero, CONVERT(VARCHAR, Anio) + '  ' + CASE Mes WHEN 1 THEN 'ENERO' WHEN 2 THEN 'FEBRERO' WHEN 3 THEN 'MARZO' WHEN 4 THEN 'ABRIL' WHEN 5 THEN 'MAYO' WHEN 6 THEN 'JUNIO' WHEN 7 THEN 'JULIO' "
                       + " WHEN 8 THEN 'AGOSTO' WHEN 9 THEN 'SEPTIEMBRE' WHEN 10 THEN 'OCTUBRE' WHEN 11 THEN 'NOVIEMBRE' WHEN 12 THEN 'DICIEMBRE' END As Mes "
                       + " FROM ( "
                       + " SELECT 0 As numero, DATEPART(MONTH, DATEADD(MONTH, 0, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, 0, GETDATE())) As Anio "
                       + " UNION "
                       + " SELECT -1 As numero, DATEPART(MONTH, DATEADD(MONTH, -1, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -1, GETDATE())) As Anio "
                       + " UNION "
                       + " SELECT -2 As numero, DATEPART(MONTH, DATEADD(MONTH, -2, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -2, GETDATE())) As Anio "
                       + " UNION "
                       + " SELECT -3 As numero, DATEPART(MONTH, DATEADD(MONTH, -3, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -3, GETDATE())) As Anio "
                       + " UNION "
                       + " SELECT -4 As numero, DATEPART(MONTH, DATEADD(MONTH, -4, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -4, GETDATE())) As Anio "
                       + " UNION "
                       + " SELECT -5 As numero, DATEPART(MONTH, DATEADD(MONTH, -5, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -5, GETDATE())) As Anio "
                       + " UNION "
                       + " SELECT -6 As numero, DATEPART(MONTH, DATEADD(MONTH, -6, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -6, GETDATE())) As Anio "
                       + " UNION "
                       + " SELECT -7 As numero, DATEPART(MONTH, DATEADD(MONTH, -7, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -7, GETDATE())) As Anio "
                       + " UNION "
                       + " SELECT -8 As numero, DATEPART(MONTH, DATEADD(MONTH, -8, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -8, GETDATE())) As Anio "
                       + " UNION "
                       + " SELECT -9 As numero, DATEPART(MONTH, DATEADD(MONTH, -9, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -9, GETDATE())) As Anio "
                       + " UNION "
                       + " SELECT -10 As numero, DATEPART(MONTH, DATEADD(MONTH, -10, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -10, GETDATE())) As Anio "
                       + " UNION "
                       + " SELECT -11 As numero, DATEPART(MONTH, DATEADD(MONTH, -11, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -11, GETDATE())) As Anio "
                       + " UNION "
                       + " SELECT -12 As numero, DATEPART(MONTH, DATEADD(MONTH, -12, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -12, GETDATE())) As Anio "
                       + " ) As Meses "
                       + " ORDER BY numero DESC ";
                IEnumerable<periodo> meses = db.Database.SqlQuery<periodo>(strSQL);
                edoCuenta.meses = meses.ToList();

                strSQL = " SELECT CASE DATEPART(MONTH, DATEADD(MONTH, " + periodo.ToString() + ", GetDate())) "
                       + " WHEN 1 THEN 'ENERO' WHEN 2 THEN 'FEBRERO' WHEN 3 THEN 'MARZO' WHEN 4 THEN 'ABRIL' "
                       + " WHEN 5 THEN 'MAYO' WHEN 6 THEN 'JUNIO' WHEN 7 THEN 'JULIO' WHEN 8 THEN 'AGOSTO' "
                       + " WHEN 9 THEN 'SEPTIEMBRE' WHEN 10 THEN 'OCTUBRE' WHEN 11 THEN 'NOVIEMBRE' WHEN 12 THEN 'DICIEMBRE' "
                       + " END + ' ' + CONVERT(varchar(10), DATEPART(YEAR, DATEADD(MONTH, " + periodo.ToString() + ", GetDate()))) AS [desc] ";
                IEnumerable<descripcion> descrip = db.Database.SqlQuery<descripcion>(strSQL);
                edoCuenta.periodo_descripcion = descrip.ToList().FirstOrDefault().desc;

                strSQL = " SELECT t.transID, o.descripcion AS origen, i.descripcion AS tipo, t.socioID, s.nombreComercial As socio, terminalID, "
                       + " m.codigo AS moneda, t.tarjetaID, t.tarjeta, t.monto, (t.cargo2 + t.cargo3 + t.cargo4 + t.cargo5 + t.cargo6) AS comision,"
                       + " (t.cargo2iva + t.cargo3iva + t.cargo4iva + t.cargo5iva + t.cargo6iva) AS iva, t.transaccion, "
                       + " t.concepto, t.descripcion, t.referencia, e.descripcion As estatus, t.aprobacion, t.ip, t.fechaRegistro "
                       + " FROM Transacciones t "
                       + " LEFT JOIN Origen o ON t.origenID = o.origenID "
                       + " LEFT JOIN Tipo i ON t.tipoID = i.tipoID "
                       + " LEFT JOIN Socios s ON t.socioID = s.socioID "
                       + " LEFT JOIN Monedas m ON t.monedaID = m.monedaID "
                       + " LEFT JOIN Estatus e ON t.estatusID = e.estatusID "
                       + " WHERE t.tarjetaID = " + tID + " AND DateDiff(MONTH, GetDate(), t.FechaRegistro) = " + periodo.ToString()
                       + " ORDER BY t.fechaRegistro ";
                IEnumerable<movimientosListado> movs = db.Database.SqlQuery<movimientosListado>(strSQL);
                edoCuenta.movimientos = movs.ToList();

                strSQL = " SELECT ISNULL(SUM(abonoTotal-cargoTotal), 0) As Total "
                       + " FROM Transacciones "
                       + " WHERE tarjetaID = " + tID + " AND DateDiff(MONTH, GetDate(), FechaRegistro) < " + periodo.ToString();
                IEnumerable<total> saldoInicial = db.Database.SqlQuery<total>(strSQL);
                edoCuenta.saldoInicial = saldoInicial.ToList().FirstOrDefault().Total;

                strSQL = " SELECT ISNULL(SUM(abonoTotal), 0) As Total "
                       + " FROM Transacciones "
                       + " WHERE tarjetaID = " + tID + " AND tipoID = 1 AND DateDiff(MONTH, GetDate(), FechaRegistro) = " + periodo.ToString();
                IEnumerable<total> abonos = db.Database.SqlQuery<total>(strSQL);
                edoCuenta.abonos = abonos.ToList().FirstOrDefault().Total;

                strSQL = " SELECT ISNULL(SUM(cargoTotal), 0) As Total "
                       + " FROM Transacciones "
                       + " WHERE tarjetaID = " + tID + " AND tipoID = 2 AND DateDiff(MONTH, GetDate(), FechaRegistro) = " + periodo.ToString();
                IEnumerable<total> cargos = db.Database.SqlQuery<total>(strSQL);
                edoCuenta.cargos = cargos.ToList().FirstOrDefault().Total;

                strSQL = " SELECT ISNULL(SUM(abonoTotal-cargoTotal),0) As Total "
                       + " FROM Transacciones "
                       + " WHERE tarjetaID = " + tID + " AND DateDiff(MONTH, GetDate(), FechaRegistro) <= " + periodo.ToString();
                IEnumerable<total> saldoFinal = db.Database.SqlQuery<total>(strSQL);
                edoCuenta.saldoFinal = saldoFinal.ToList().FirstOrDefault().Total;

                return PartialView("_IndexUsuario", edoCuenta);
            }

            if (User.IsInRole(SecurityRoles.Remesas))
            {
                return PartialView("_IndexRemesas");
            }
            //}

            return new EmptyResult();

        }

        //public ActionResult About()
        //{
        //    ViewBag.Message = "Your application description page.";

        //    return View();
        //}

        //public ActionResult Contact()
        //{
        //    ViewBag.Message = "Your contact page.";

        //    return View();
        //}
    }
}