﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Enpadi_WebUI.Models;

namespace Enpadi_WebUI.Controllers
{
    public class tarjetasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        public ActionResult Resumen(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            //strSQL = " if exists(SELECT * FROM sys.views WHERE name = 'saldos_view' AND schema_id = SCHEMA_ID('dbo')) DROP VIEW dbo.saldos_view ";

            db.Database.ExecuteSqlCommand(strSQL);

            strSQL = " alter VIEW saldos_view AS "
                   + " SELECT tarjetaID, Saldo, UltimaTransaccion, DATEDIFF(DAY, UltimaTransaccion, GETDATE()) As Dias "
                   + " FROM ( "
                   + "   SELECT tarjetaID, Saldo, (SELECT TOP 1 t.FechaRegistro FROM Transacciones t WHERE t.tarjetaID = d1.tarjetaID ORDER BY FechaRegistro DESC) As UltimaTransaccion "
                   + "     FROM ( "
                   + "       SELECT DISTINCT(tarjetaID) As tarjetaID, SUM(abonoTotal - cargoTotal) As Saldo "
                   + "       FROM Transacciones "
                   + "       WHERE tarjetaID > 0 "
                   + "       GROUP BY tarjetaID "
                   + "     ) As d1 "
                   + " )d2;";
            db.Database.ExecuteSqlCommand(strSQL);

            strSQL = " SELECT 1 As Orden, 'Activas' As Periodo, COALESCE(COUNT(*),0) As Cuentas, COALESCE(SUM(Saldo),0) As Saldo FROM saldos_view WHERE Dias <= 29 "
                   + " UNION "
                   + " SELECT 2 As Orden, '30 Dias' As Periodo, COALESCE(COUNT(*),0) As Cuentas, COALESCE(SUM(Saldo),0) As Saldo FROM saldos_view WHERE Dias BETWEEN 30 AND 59 "
                   + " UNION "
                   + " SELECT 3 As Orden, '60 Dias' As Periodo, COALESCE(COUNT(*),0) As Cuentas, COALESCE(SUM(Saldo),0) As Saldo FROM saldos_view WHERE Dias BETWEEN 60 AND 89 "
                   + " UNION "
                   + " SELECT 4 As Orden, '90 Dias' As Periodo, COALESCE(COUNT(*),0) As Cuentas, COALESCE(SUM(Saldo),0) As Saldo FROM saldos_view WHERE Dias BETWEEN 90 AND 119 "
                   + " UNION "
                   + " SELECT 5 As Orden, '120 Dias' As Periodo, COALESCE(COUNT(*),0) As Cuentas, COALESCE(SUM(Saldo),0) As Saldo FROM saldos_view WHERE Dias BETWEEN 120 AND 149 "
                   + " UNION "
                   + " SELECT 6 As Orden, '150 Dias' As Periodo, COALESCE(COUNT(*),0) As Cuentas, COALESCE(SUM(Saldo),0) As Saldo FROM saldos_view WHERE Dias BETWEEN 150 AND 179 "
                   + " UNION "
                   + " SELECT 7 As Orden, '180 Dias' As Periodo, COALESCE(COUNT(*),0) As Cuentas, COALESCE(SUM(Saldo),0) As Saldo FROM saldos_view WHERE Dias >= 180 ";
            IEnumerable<resumenSaldos> saldos = db.Database.SqlQuery<resumenSaldos>(strSQL);

            return View(saldos);
        }


        // GET: tarjetas
        public ActionResult Index(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            strSQL  = " SELECT t.tarjetaID, t.productoID, p.nombre AS producto, m.codigo, t.tcNum, t.datNombre, t.datPaterno, t.datMaterno, t.tcAsignada, t.tcActiva, ";
            strSQL += " (SELECT COALESCE(SUM(abonoTotal-cargoTotal),0) As Saldo FROM Transacciones WHERE tarjetaID = t.tarjetaID) As Saldo ";
            strSQL += " FROM Tarjetas t ";
            strSQL += " LEFT JOIN Productos p ON t.productoID = p.productoID ";
            strSQL += " LEFT JOIN Monedas m ON t.monedaID = m.monedaID ";
            strSQL += " WHERE t.FechaBaja IS NULL AND t.tcActiva = 1 AND t.tcAsignada = 1 ";
            strSQL += " ORDER BY Saldo DESC, t.tarjetaID ";

            IEnumerable<tarjetasListado> tarjetasList = db.Database.SqlQuery<tarjetasListado>(strSQL);
            
            return View(tarjetasList.ToList());
        }

        // GET: tarjetas/Details/5
        public ActionResult Details(int? id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tarjetas tarjetas = db.tarjetas.Find(id);
            if (tarjetas == null)
            {
                return HttpNotFound();
            }
            return View(tarjetas);
        }

        // GET: tarjetas/Create
        public ActionResult Create(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            ViewBag.cdID = new SelectList(db.ciudades, "cdID", "nombre");
            ViewBag.edoID = new SelectList(db.estados, "edoID", "nombre");
            ViewBag.monedaID = new SelectList(db.monedas, "monedaID", "codigo");
            ViewBag.paisID = new SelectList(db.paises, "paisID", "nombre");
            ViewBag.productoID = new SelectList(db.productos, "productoID", "nombre");
            return View();
        }

        // POST: tarjetas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "tarjetaID,productoID,monedaID,tcNum,tcMD5,tcMes,tcAnio,tcDigitos,tcNIP,tcAsignada,tcActiva,limActivacion,limDepMin,limDepMax,limDepDia,limDepSem,limDepMes,limRetMin,limRetMax,limRetDia,limRetSem,limRetMes,limTransMin,limTransMax,limTransDia,limTransSem,limTransMes,titulo,datNombre,datPaterno,datMaterno,genero,datNacimiento,datDir1,datDir2,datColonia,paisID,edoID,cdID,datCP,datTelefono,datFax,datCelular,datNextel,datBlackPin,identificacionID,identificacionInfo,identificacionFile,identificacionValida,email,password")] tarjetas tarjetas, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                db.tarjetas.Add(tarjetas);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.cdID = new SelectList(db.ciudades, "cdID", "nombre", tarjetas.cdID);
            ViewBag.edoID = new SelectList(db.estados, "edoID", "nombre", tarjetas.edoID);
            ViewBag.monedaID = new SelectList(db.monedas, "monedaID", "codigo", tarjetas.monedaID);
            ViewBag.paisID = new SelectList(db.paises, "paisID", "nombre", tarjetas.paisID);
            ViewBag.productoID = new SelectList(db.productos, "productoID", "nombre", tarjetas.productoID);
            return View(tarjetas);
        }

        // GET: tarjetas/Edit/5
        public ActionResult Edit(int? id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tarjetas tarjetas = db.tarjetas.Find(id);
            if (tarjetas == null)
            {
                return HttpNotFound();
            }
            ViewBag.cdID = new SelectList(db.ciudades, "cdID", "nombre", tarjetas.cdID);
            ViewBag.edoID = new SelectList(db.estados, "edoID", "nombre", tarjetas.edoID);
            ViewBag.monedaID = new SelectList(db.monedas, "monedaID", "codigo", tarjetas.monedaID);
            ViewBag.paisID = new SelectList(db.paises, "paisID", "nombre", tarjetas.paisID);
            ViewBag.productoID = new SelectList(db.productos, "productoID", "nombre", tarjetas.productoID);
            return View(tarjetas);
        }

        // POST: tarjetas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "tarjetaID,productoID,monedaID,tcNum,tcMD5,tcMes,tcAnio,tcDigitos,tcNIP,tcAsignada,tcActiva,limActivacion,limDepMin,limDepMax,limDepDia,limDepSem,limDepMes,limRetMin,limRetMax,limRetDia,limRetSem,limRetMes,limTransMin,limTransMax,limTransDia,limTransSem,limTransMes,titulo,datNombre,datPaterno,datMaterno,genero,datNacimiento,datDir1,datDir2,datColonia,paisID,edoID,cdID,datCP,datTelefono,datFax,datCelular,datNextel,datBlackPin,identificacionID,identificacionInfo,identificacionFile,identificacionValida,email,password")] tarjetas tarjetas, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                db.Entry(tarjetas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.cdID = new SelectList(db.ciudades, "cdID", "nombre", tarjetas.cdID);
            ViewBag.edoID = new SelectList(db.estados, "edoID", "nombre", tarjetas.edoID);
            ViewBag.monedaID = new SelectList(db.monedas, "monedaID", "codigo", tarjetas.monedaID);
            ViewBag.paisID = new SelectList(db.paises, "paisID", "nombre", tarjetas.paisID);
            ViewBag.productoID = new SelectList(db.productos, "productoID", "nombre", tarjetas.productoID);
            return View(tarjetas);
        }

        // GET: tarjetas/Delete/5
        public ActionResult Delete(int? id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tarjetas tarjetas = db.tarjetas.Find(id);
            if (tarjetas == null)
            {
                return HttpNotFound();
            }
            return View(tarjetas);
        }

        // POST: tarjetas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            tarjetas tarjetas = db.tarjetas.Find(id);
            db.tarjetas.Remove(tarjetas);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
