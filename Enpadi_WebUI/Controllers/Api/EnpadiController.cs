﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel.Web;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using Enpadi_WebUI.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System.Web.Script.Serialization;
using System.Text;
using System.Collections.Specialized;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using System.Web.Http.Cors;
using System.Configuration;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace Enpadi_WebUI.Controllers.Api
{
    public class EnpadiController : ApiController
    {
        #region varibales
        private Boolean write_to_file = true;

        #endregion variables

        #region web methods

        //this works with http://localhost:45093/api/enpadi

        [HttpPost]
        [Route("api/enpadi")]
        public async Task<HttpResponseMessage> Post(HttpRequestMessage request)
        {
            try
            {

                //read response from webhook
                string str = request.Content.ReadAsStringAsync().Result;

                //convert response to string
                String jsonString = await request.Content.ReadAsStringAsync();

                //to return the proper object on the response
                object objToReturn = new object();

                //to read some propeties of the jsonstring
                string type = ((JToken)JObject.Parse(jsonString))["type"].ToString();
                JObject job = JObject.Parse(jsonString);

                switch (type)
                {
                    case EnpadiOrderModel.EnpadiOrderEstatus.Create:
                        //transforms jsonstring to charge and order object
                        EnpadiPostOrder response = (EnpadiPostOrder)EnpadiPostOrder.JsonStringToModel(jsonString);

                        //asign object to return
                        //objToReturn = response;

                        //save empadi order
                        SaveEnpadiOrderPost(response.order);

                        //save charge order
                        SaveEnpadiChargePost(response.charge);

                        //Make payment
                        String payment_method_type = job["chargedata"]["payment_method"]["payment_method_type"].ToString();
                        objToReturn = MakePayment(payment_method_type, response);


                        break;
                    case EnpadiOrderModel.EnpadiOrderEstatus.Paid:
                        break;
                    case EnpadiOrderModel.EnpadiOrderEstatus.Created:

                        break;
                    case EnpadiOrderModel.EnpadiOrderEstatus.Canceled:
                        break;
                    case EnpadiOrderModel.EnpadiOrderEstatus.Pending:
                        break;
                    case EnpadiChargeModel.ChargeEstatus.charge_paid:
                        break;
                    case EnpadiChargeModel.ChargeEstatus.charge_created:
                        break;
                    case EnpadiChargeModel.ChargeEstatus.charge_pending:
                        break;
                    case EnpadiChargeModel.ChargeEstatus.charge_cancel:
                        break;
                    default:
                        break;
                }

                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(objToReturn));
                //return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception e)
            {

                //string json1 = HostingEnvironment.MapPath(@"~/App_Data/EnpadiPostPostError.txt");
                //File.AppendAllText(json1, "Test on " + DateTime.Now.ToString() + "-----------------------" + Environment.NewLine);
                //File.AppendAllText(json1, e.Message);

                return Request.CreateResponse(HttpStatusCode.BadRequest, SystemErrorModel.ErrorList.GenericError + e.Message);
            }
        }

        #region services

        //TODO
        [HttpPost]
        [Route("api/enpadi/CheckFeeForService")]
        public async Task<HttpResponseMessage> CheckFeeForService(HttpRequestMessage request)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            try
            {
                //read the content of the httprequestmessage
                string str = request.Content.ReadAsStringAsync().Result;
                String jsonString = await request.Content.ReadAsStringAsync();

                string email = ((JToken)JObject.Parse(jsonString))["email"].ToString();
                string password = ((JToken)JObject.Parse(jsonString))["password"].ToString();
                string sku = ((JToken)JObject.Parse(jsonString))["sku"].ToString();
                object objToReturn = new object();

                if (email != String.Empty && password != String.Empty)
                {
                    var contextDB = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
                    var storeDB = new UserStore<IdentityUser>(contextDB);
                    var managerDB = new UserManager<IdentityUser>(storeDB);
                    var user = managerDB.FindByEmail(email);

                    if (managerDB.CheckPassword(user, password))
                    {
                        servicios service = db.servicios.FirstOrDefault(x => x.SKU == sku);

                        if (service != null)
                        {

                            //get mx or usa fees 

                            List<Fee> fees = Fee.GetPlatformUserFeesForSeviceCategory(Guid.Parse(user.Id), service.ServiceCategoryId);
                            //List<Fee> fees = Fee.GetUsaApiServiceFees(Guid.Parse(user.Id), service.ServiceCategoryId);
                            if (fees != null)
                            {
                                objToReturn = new Dictionary<String, object>() {
                                    {"Service: ", service },
                                    {"Fees: ", fees }
                                };
                            }
                        }
                        else
                        {
                            objToReturn = service;
                        }
                    }
                    else
                    {
                        return request.CreateResponse(HttpStatusCode.BadRequest, "Bad credentials.");
                    }
                }
                else
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, "Check request for password and email.");
                }

                return request.CreateResponse(HttpStatusCode.OK, new JavaScriptSerializer().Serialize(objToReturn));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, SystemErrorModel.ErrorList.GenericError + e.Message);
            }
        }

        /// <summary>
        /// This return all the serviceson the database
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/enpadi/checkservices")]
        public async Task<HttpResponseMessage> CheckServices(HttpRequestMessage request)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            try
            {
                //read the content of the httprequestmessage
                string str = request.Content.ReadAsStringAsync().Result;
                String jsonString = await request.Content.ReadAsStringAsync();

                string email = ((JToken)JObject.Parse(jsonString))["email"].ToString();
                string password = ((JToken)JObject.Parse(jsonString))["password"].ToString();
                object objToReturn = new object();

                if (email != String.Empty && password != String.Empty)
                {
                    var contextDB = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
                    var storeDB = new UserStore<IdentityUser>(contextDB);
                    var managerDB = new UserManager<IdentityUser>(storeDB);
                    var user = managerDB.FindByEmail(email);

                    if (managerDB.CheckPassword(user, password))
                    {
                        List<servicios> services = db.servicios.ToList();

                        objToReturn = services;
                    }
                    else
                    {
                        return request.CreateResponse(HttpStatusCode.BadRequest, "Bad credentials.");
                    }
                }
                else
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, "Check request for password and email.");
                }

                return request.CreateResponse(HttpStatusCode.OK, new JavaScriptSerializer().Serialize(objToReturn));
            }
            catch (Exception e)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, SystemErrorModel.ErrorList.GenericError + e.Message);
            }
        }

        [HttpPost]
        [Route("api/enpadi/GetAllOrders")]
        public async Task<HttpResponseMessage> GetAllOrders(HttpRequestMessage request)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            try
            {
                //read the content of the httprequestmessage
                string str = request.Content.ReadAsStringAsync().Result;
                String jsonString = await request.Content.ReadAsStringAsync();

                string email = ((JToken)JObject.Parse(jsonString))["email"].ToString();
                string password = ((JToken)JObject.Parse(jsonString))["password"].ToString();
                object objToReturn = new object();

                if (email != String.Empty && password != String.Empty)
                {
                    var contextDB = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
                    var storeDB = new UserStore<IdentityUser>(contextDB);
                    var managerDB = new UserManager<IdentityUser>(storeDB);
                    var user = managerDB.FindByEmail(email);

                    if (managerDB.CheckPassword(user, password))
                    {
                        List<EnpadiOrder> orders = db.EnpadiOrder.Where(x=>x.client_email == email).ToList();
                        foreach (EnpadiOrder order in orders) {

                            order.line_items = db.EnpadiLineItem.Where(x => x.orderId == order.id).ToList();
                        }

                        objToReturn = orders;
                    }
                    else
                    {
                        return request.CreateResponse(HttpStatusCode.BadRequest, "Bad credentials.");
                    }
                }
                else
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, "Check request for password and email.");
                }

                return request.CreateResponse(HttpStatusCode.OK, new JavaScriptSerializer().Serialize(objToReturn));
            }
            catch (Exception e)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, SystemErrorModel.ErrorList.GenericError + e.Message);
            }
        }


        [HttpPost]
        [Route("api/enpadi/CreateOrder")]
        public async Task<HttpResponseMessage> CreateOrder(HttpRequestMessage request)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            try
            {
                //read the content of the httprequestmessage
                string str = request.Content.ReadAsStringAsync().Result;
                String jsonString = await request.Content.ReadAsStringAsync();

                string email = ((JToken)JObject.Parse(jsonString))["email"].ToString();
                string password = ((JToken)JObject.Parse(jsonString))["password"].ToString();

                string jsonObject = ((JToken)JObject.Parse(jsonString))["jsonObject"].ToString();
                JObject job = JObject.Parse(jsonObject);

                object objToReturn = new object();

                if (email != String.Empty && password != String.Empty)
                {
                    var contextDB = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
                    var storeDB = new UserStore<IdentityUser>(contextDB);
                    var managerDB = new UserManager<IdentityUser>(storeDB);
                    var user = managerDB.FindByEmail(email);

                    if (managerDB.CheckPassword(user, password))
                    {
                        //in here starts the process of reading the json order and start processing it
                        EnpadiPostOrder response = (EnpadiPostOrder)EnpadiPostOrder.JsonStringToModel(jsonObject);
                        objToReturn = response;
                    }
                    else
                    {
                        return request.CreateResponse(HttpStatusCode.BadRequest, "Bad credentials.");
                    }
                }
                else
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, "Check request for password and email.");
                }

                return request.CreateResponse(HttpStatusCode.OK, new JavaScriptSerializer().Serialize(objToReturn));
            }
            catch (Exception e)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, SystemErrorModel.ErrorList.GenericError + e.Message);
            }
        }

        private class storeResponse
        {
            public EnpadiPostOrder enpadiOrder;
            public pagoTienda opago;
        };

        [HttpPost]
        [Route("api/enpadi/PayServiceWithCredit")]
        public async Task<HttpResponseMessage> PayServiceWithCredit(HttpRequestMessage request)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            try
            {
                //read the content of the httprequestmessage
                string str = request.Content.ReadAsStringAsync().Result;
                String jsonString = await request.Content.ReadAsStringAsync();

                string email = ((JToken)JObject.Parse(jsonString))["email"].ToString();
                string password = ((JToken)JObject.Parse(jsonString))["password"].ToString();

                string jsonObject = ((JToken)JObject.Parse(jsonString))["jsonObject"].ToString();
                JObject job = JObject.Parse(jsonObject);



                object objToReturn = new object();

                if (email != String.Empty && password != String.Empty)
                {
                    var contextDB = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
                    var storeDB = new UserStore<IdentityUser>(contextDB);
                    var managerDB = new UserManager<IdentityUser>(storeDB);
                    var user = managerDB.FindByEmail(email);

                    if (managerDB.CheckPassword(user, password))
                    {
                        //in here starts the process of reading the json order and start processing it
                        EnpadiPostOrder response = (EnpadiPostOrder)EnpadiPostOrder.JsonStringToModel(jsonObject);


                        //check this, this was a quick fix for sending pago tienda
                        storeResponse proceesResponse = new storeResponse();
                        proceesResponse.enpadiOrder = response;

                        pagoTienda paystubStore = new pagoTienda();

                        paystubStore = OpenPayModel.ProcessPaymentStore(response);

                        objToReturn = new
                        {
                            paystubStore.id,
                            paystubStore.description,
                            paystubStore.error_message,
                            paystubStore.authorization,
                            paystubStore.amount,
                            paystubStore.operation_type,
                            paystubStore.payment_method,
                            paystubStore.type,
                            paystubStore.reference,
                            paystubStore.walmart_reference,
                            paystubStore.barcode_url,
                            paystubStore.barcode_walmart_url,
                            paystubStore.order_id,
                            paystubStore.transaction_type,
                            paystubStore.creation_date,
                            paystubStore.currency,
                            paystubStore.status,
                            paystubStore.method
                        };

                        proceesResponse.opago = paystubStore;

                        objToReturn = proceesResponse;
                    }
                    else
                    {
                        return request.CreateResponse(HttpStatusCode.BadRequest, "Bad credentials.");
                    }
                }
                else
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, "Check request for password and email.");
                }

                return request.CreateResponse(HttpStatusCode.OK, new JavaScriptSerializer().Serialize(objToReturn));
            }
            catch (Exception e)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, SystemErrorModel.ErrorList.GenericError + e.Message);
            }
        }

        #endregion services

        #endregion web methods

        #region private functions

        /// <summary>
        /// The amount param most be format to de proper int, call FormatOxxoResponseAmount()
        /// </summary>
        /// <param name="paymentType"></param>
        /// <param name="amount"></param>
        /// <param name="clientEmail"></param>
        /// <returns></returns>
        private Object MakePayment(String paymentType, EnpadiPostOrder response)
        {
            Object objToReturn = new object();
            try
            {
                switch (paymentType)
                {
                    case "oxxo_cash":


                        //TODO: remove the oxxo response fee
                        //prepare paystup from oxxo
                        ConektaModel.OxxoOrderResponseForPaySub paystub = new ConektaModel.OxxoOrderResponseForPaySub();

                        paystub = ConektaModel.RecargarTiendaOxxo(ConektaModel.FormatOxxoResponseAmount(response.charge.data_object.amount), response.order.data_object.line_items, response.order.data_object.client_email);

                        objToReturn = new
                        {
                            paystub.Oxxo_responseAmount,
                            paystub.Oxxo_responseDescription,
                            paystub.Oxxo_responseOrderCreationDate,
                            paystub.Oxxo_responseOrderId,
                            paystub.Oxxo_responseOrderPaymentStatus,
                            paystub.Oxxo_responsePaymentMethod,
                            paystub.Oxxo_responseReference

                        };

                        //send paystup to client
                        WebHookModel.SendWebHookNotification(
                            objToReturn,
                            response.order.data_object.client_email,
                            response.charge.data_object.id,
                            response.order.data_object.livemode
                            );

                        break;
                    case "card":

                        //create the paystub
                        trans paystub_credit_card = new trans();

                        OpenPayModel.CheckCreditCardUser(response);

                        paystub_credit_card = OpenPayModel.ProcessPaymentCreditCard(response);

                        objToReturn = new
                        {
                            paystub_credit_card.transID,
                            paystub_credit_card.monedaID,
                            paystub_credit_card.tarjetaID,
                            paystub_credit_card.tarjeta,
                            paystub_credit_card.abonoTotal,
                            paystub_credit_card.transaccion,
                            paystub_credit_card.concepto,
                            paystub_credit_card.descripcion,
                            paystub_credit_card.referencia,
                            paystub_credit_card.aprobacion
                        };

                        WebHookModel.SendWebHookNotification(
                           objToReturn,
                           response.order.data_object.client_email,
                           response.charge.data_object.id,
                           response.order.data_object.livemode
                           );
                        break;
                    case "bank_account":

                        pagoBanco paystubBank = new pagoBanco();

                        paystubBank = OpenPayModel.ProcessPaymentBank(response);

                        WebHookModel.SendWebHookNotification(paystubBank, response.order.data_object.client_email, response.charge.data_object.id, response.order.data_object.livemode);

                        objToReturn = paystubBank;

                        break;
                    case "store":
                    case "7eleven":

                        pagoTienda paystubStore = new pagoTienda();

                        paystubStore = OpenPayModel.ProcessPaymentStore(response);

                        objToReturn = new
                        {
                            paystubStore.id,
                            paystubStore.description,
                            paystubStore.error_message,
                            paystubStore.authorization,
                            paystubStore.amount,
                            paystubStore.operation_type,
                            paystubStore.payment_method,
                            paystubStore.type,
                            paystubStore.reference,
                            paystubStore.walmart_reference,
                            paystubStore.barcode_url,
                            paystubStore.barcode_walmart_url,
                            paystubStore.order_id,
                            paystubStore.transaction_type,
                            paystubStore.creation_date,
                            paystubStore.currency,
                            paystubStore.status,
                            paystubStore.method
                        };

                        WebHookModel.SendWebHookNotification(objToReturn, response.order.data_object.client_email, response.charge.data_object.id, response.order.data_object.livemode);

                        break;
                    case "enpadi_login":

                        //TODO: Generate enpadi login form and payment options

                        //open a login from with the product information and the user login


                        //if successfull send the customer to payment options and process payment

                        //send paystub or receipt to client

                        //rediret customer to client webpage

                        break;
                    default:
                        throw new ArgumentException(SystemErrorModel.ErrorList.PaymentMethodNotRecognized);
                }
            }
            catch (Exception e)
            {
                throw new ArgumentException(SystemErrorModel.ErrorList.GenericError + e.Message);
                //TODO create a log and a system error entry
            }
            return objToReturn;
        }

        private void SaveStringToLocalFile(String jsonString, Boolean write_to_file)
        {
            if (write_to_file)
            {

                string json1 = HostingEnvironment.MapPath(@"~/App_Data/EnpadiPost.txt");
                File.AppendAllText(json1, "Test on " + DateTime.Now.ToString() + "-----------------------" + Environment.NewLine);
                File.AppendAllText(json1, jsonString);
            }

        }

        private void SaveEnpadiChargePost(EnpadiChargeModel response)
        {
            try
            {
                EnpadiChargeModel.Save(response);
            }
            catch (Exception e)
            {
                string json1 = HostingEnvironment.MapPath(@"~/App_Data/EnpadiPostSaveModelError.txt");
                File.AppendAllText(json1, "Test on " + DateTime.Now.ToString() + "-----------------------" + Environment.NewLine);
                File.AppendAllText(json1, e.Message);
            }
        }

        private void SaveEnpadiOrderPost(EnpadiOrderModel response)
        {
            try
            {
                EnpadiOrderModel.Save(response);
                EnpadiLineItemModel.Save(response.data_object.line_items);
            }
            catch (Exception e)
            {
                string json1 = HostingEnvironment.MapPath(@"~/App_Data/EnpadiPostSaveModelError.txt");
                File.AppendAllText(json1, "Test on " + DateTime.Now.ToString() + "-----------------------" + Environment.NewLine);
                File.AppendAllText(json1, e.Message);
            }
        }

        //private void SaveTransaction(EnpadiOrderModel response)
        //{
        //    try
        //    {

        //        ////create a basic transaccion
        //        //trans t = new trans
        //        //{
        //        //    estatusID = 0,
        //        //    origenID = trans.TransOrigin.Exteranl,
        //        //    tipoID = trans.TransType.Deposit,
        //        //    modoID = 1,
        //        //    socioID = socio.SocioID.Tienda,
        //        //    terminalID = 1,
        //        //    monedaID = 484,
        //        //    Concepto = "Pago de orden Enpadi",
        //        //    geoCountryCode = 0,
        //        //    geoCountryName = String.Empty,
        //        //    geoRegion = String.Empty,
        //        //    geoCity = String.Empty,
        //        //    geoPostalCode = String.Empty
        //        //};

        //        ////get the user
        //        //var contextDB = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
        //        //var storeDB = new UserStore<IdentityUser>(contextDB);
        //        //var managerDB = new UserManager<IdentityUser>(storeDB);
        //        //var userDB = managerDB.FindByEmail(response.customer_info.email);
        //        //tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.email == userDB.Email);


        //        //string json1 = HostingEnvironment.MapPath(@"~/App_Data/EnpadiPostSaveTransaction.txt");
        //        //File.AppendAllText(json1, "Test on " + DateTime.Now.ToString() + "-----------------------" + Environment.NewLine);
        //        //File.AppendAllText(json1, "tcDigitos " + usuario.tcDigitos + " tarjeta numero " + usuario.tcNum);

        //        ////complement transaccion with response
        //        //t.estatusID = 1;
        //        //t.tarjetaID = usuario.tarjetaID;
        //        //t.tarjeta = usuario.tcNum.Substring(usuario.tcNum.Length - 4);
        //        //t.transaccion = response.data_object.id.ToString();
        //        //t.descripcion = response.data_object.object_description;
        //        //t.referencia = response.payment_method_reference;
        //        //t.aprobacion = response.charges_data_id;
        //        //t.monto = Decimal.Parse(response.data_object_amount) / 100;
        //        //t.abono1 = Decimal.Parse(response.data_object_amount) / 100;
        //        //t.abonoTotal = Decimal.Parse(response.data_object_amount) / 100;

        //        //decimal dIva = (decimal)0.16;

        //        ////ejemplo visa
        //        //t.cargo2 = t.monto * (decimal)(0.035); //3.5% of transaccion charge
        //        //t.cargo2iva = t.cargo2 * dIva;
        //        ////t.cargo5 = (decimal)2.5; no fix charge yet
        //        //t.cargo5iva = t.cargo5 * dIva;

        //        //t.ip = "";

        //        //trans.Save(t);

        //    }
        //    catch (Exception e)
        //    {

        //        string json1 = HostingEnvironment.MapPath(@"~/App_Data/EnpadiPostSaveTransactionError.txt");
        //        File.AppendAllText(json1, "Test on " + DateTime.Now.ToString() + "-----------------------" + Environment.NewLine);
        //        File.AppendAllText(json1, e.Message);
        //    }
        //}

        #endregion private functions
    }
}
