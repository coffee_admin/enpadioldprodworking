﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel.Web;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using Enpadi_WebUI.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System.Web.Http.Cors;

namespace Enpadi_WebUI.Controllers.Api
{
    public class ConektaController : ApiController
    {

        #region varibales

        private ApplicationDbContext db = new ApplicationDbContext();
        private Boolean write_to_file = true;

        #endregion variables

        #region web methods

        [HttpPost]
        public async Task<HttpResponseMessage> Post(HttpRequestMessage request)
        {
            try
            {

                //read response from webhoo
                string str = request.Content.ReadAsStringAsync().Result;

                //convert response to string
                String jsonString = await request.Content.ReadAsStringAsync();

                //for testing
                SaveStringToLocalFile(jsonString, write_to_file);

                string type = ((JToken)JObject.Parse(jsonString))["type"].ToString();


                switch (type)
                {

                    case "order.paid":
                        OxxoResponseModel response = JsonStringToOxxoResponseModel(jsonString);

                        //saving response
                        SaveOxxoResponseFromJson(response);

                        //saveing transaction
                        SaveTransaction(response);
                        break;
                    case "order.pending_payment":
                        break;
                    case "order.created":
                        break;
                    case "charge.created":
                        break;
                    case "charge.paid":
                        break;
                    default:
                        break;
                }

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception e)
            {

                string json1 = HostingEnvironment.MapPath(@"~/App_Data/PostPost.txt");
                File.AppendAllText(json1, "Test on " + DateTime.Now.ToString() + "-----------------------" + Environment.NewLine);
                File.AppendAllText(json1, e.Message);

                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        #endregion web methods

        #region private functions

        private void SaveStringToLocalFile(String jsonString, Boolean write_to_file)
        {
            if (write_to_file)
            {

                string json1 = HostingEnvironment.MapPath(@"~/App_Data/Post.txt");
                File.AppendAllText(json1, "Test on " + DateTime.Now.ToString() + "-----------------------" + Environment.NewLine);
                File.AppendAllText(json1, jsonString);
            }

        }

        private OxxoResponseModel JsonStringToOxxoResponseModel(String jsonString)
        {

            try
            {
                JObject job = JObject.Parse(jsonString);
                OxxoResponseModel response = new OxxoResponseModel();

                string Oxxotype = ((JToken)JObject.Parse(jsonString))["type"].ToString();

                if (Oxxotype == "order.paid")
                {
                    JToken type = job["type"];

                    JToken data_object_id = job["data"]["object"]["id"];
                    JToken data_object_livemode = job["data"]["object"]["livemode"];
                    JToken data_object_created_at = job["data"]["object"]["created_at"];
                    JToken data_object_currency = job["data"]["object"]["currency"];
                    JToken data_object_amount = job["data"]["object"]["amount"];
                    JToken data_object_payment_status = job["data"]["object"]["payment_status"];

                    JToken customer_email = job["data"]["object"]["customer_info"]["email"];
                    JToken customer_phone = job["data"]["object"]["customer_info"]["phone"];
                    JToken customer_name = job["data"]["object"]["customer_info"]["name"];

                    JToken line_item_price = job["data"]["object"]["line_items"]["data"][0]["unit_price"];
                    JToken line_item_id = job["data"]["object"]["line_items"]["data"][0]["id"];
                    JToken line_item_parent_id = job["data"]["object"]["line_items"]["data"][0]["parent_id"];

                    JToken charges_data_id = job["data"]["object"]["charges"]["data"][0]["id"];
                    JToken charges_data_livemode = job["data"]["object"]["charges"]["data"][0]["livemode"];
                    JToken charges_data_created_at = job["data"]["object"]["charges"]["data"][0]["created_at"];
                    JToken charges_data_currency = job["data"]["object"]["charges"]["data"][0]["currency"];
                    JToken charges_data_description = job["data"]["object"]["charges"]["data"][0]["description"];
                    JToken charges_data_status = job["data"]["object"]["charges"]["data"][0]["status"];
                    JToken charges_data_amount = job["data"]["object"]["charges"]["data"][0]["amount"];
                    JToken charges_data_paid_at = job["data"]["object"]["charges"]["data"][0]["paid_at"];
                    JToken charges_data_fee = job["data"]["object"]["charges"]["data"][0]["fee"];
                    JToken charges_data_order_id = job["data"]["object"]["charges"]["data"][0]["order_id"];

                    JToken payment_method_service_name = job["data"]["object"]["charges"]["data"][0]["payment_method"]["service_name"];
                    JToken payment_method_type = job["data"]["object"]["charges"]["data"][0]["payment_method"]["type"];
                    JToken payment_method_expires_at = job["data"]["object"]["charges"]["data"][0]["payment_method"]["expires_at"];
                    JToken payment_method_reference = job["data"]["object"]["charges"]["data"][0]["payment_method"]["reference"];

                    JToken livemode = job["livemode"];
                    JToken id = job["id"];



                    response.data_object_id = data_object_id.ToString();
                    response.data_object_livemode = data_object_livemode.ToString();
                    response.data_object_created_at = data_object_created_at.ToString();
                    response.data_object_currency = data_object_currency.ToString();
                    response.data_object_amount = data_object_amount.ToString();
                    response.data_object_payment_status = data_object_payment_status.ToString();

                    response.customer_email = customer_email.ToString();
                    response.customer_phone = customer_phone.ToString();
                    response.customer_name = customer_name.ToString();

                    response.line_item_price = line_item_price.ToString();
                    response.line_item_id = line_item_id.ToString();
                    response.line_item_parent_id = line_item_parent_id.ToString();

                    response.charges_data_id = charges_data_id.ToString();
                    response.charges_data_livemode = charges_data_livemode.ToString();
                    response.charges_data_created_at = charges_data_created_at.ToString();
                    response.charges_data_currency = charges_data_currency.ToString();
                    response.charges_data_description = charges_data_description.ToString();
                    response.charges_data_status = charges_data_status.ToString();
                    response.charges_data_amount = charges_data_amount.ToString();
                    response.charges_data_paid_at = charges_data_paid_at.ToString();
                    response.charges_data_fee = charges_data_fee.ToString();
                    response.charges_data_order_id = charges_data_order_id.ToString();

                    response.payment_method_service_name = payment_method_service_name.ToString();
                    response.payment_method_type = payment_method_type.ToString();
                    response.payment_method_expires_at = payment_method_expires_at.ToString();
                    response.payment_method_reference = payment_method_reference.ToString();

                    response.livemode = livemode.ToString();
                    response.id = id.ToString();
                    response.type = type.ToString();
                }
                return response;
            }
            catch (Exception e)
            {
                string json1 = HostingEnvironment.MapPath(@"~/App_Data/PostJsonStringToOxxoResponseModel.txt");
                File.AppendAllText(json1, "Test on " + DateTime.Now.ToString() + "-----------------------" + Environment.NewLine);
                File.AppendAllText(json1, e.Message);

                return new OxxoResponseModel { charges_data_description = e.Message };
            }

        }


        private void SaveOxxoResponseFromJson(OxxoResponseModel response)
        {
            try
            {
                if (response.type == "order.paid")
                {
                    OxxoResponseModel.Save(response);
                }
            }
            catch (Exception e)
            {
                string json1 = HostingEnvironment.MapPath(@"~/App_Data/PostSaveOxxoResponseFromJson.txt");
                File.AppendAllText(json1, "Test on " + DateTime.Now.ToString() + "-----------------------" + Environment.NewLine);
                File.AppendAllText(json1, e.Message);
            }
        }

        private void SaveTransaction(OxxoResponseModel response)
        {
            try
            {
                if (response.type == "order.paid")
                {
                    //create a basic transaccion
                    trans t = new trans
                    {
                        Id = Guid.NewGuid(),
                        estatusID = 1,
                        origenID = trans.TransOrigin.External,
                        tipoID = trans.TransType.Deposit,
                        modoID = 1,
                        socioID = socio.SocioID.Tienda,
                        terminalID = 1,
                        monedaID = moneda.currency.MexicanPesoIntId,
                        Concepto = "Deposito - OxxoPay",
                        geoCountryCode = 0,
                        geoCountryName = String.Empty,
                        geoRegion = String.Empty,
                        geoCity = String.Empty,
                        geoPostalCode = String.Empty,

                        fechaRegistro = DateTime.Now
                    };

                    //get the user
                    var contextDB = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
                    var storeDB = new UserStore<IdentityUser>(contextDB);
                    var managerDB = new UserManager<IdentityUser>(storeDB);
                    var userDB = managerDB.FindByEmail(response.customer_email);
                    tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.email == userDB.Email);


                    string json1 = HostingEnvironment.MapPath(@"~/App_Data/PostSaveTransaction.txt");
                    File.AppendAllText(json1, "Test on " + DateTime.Now.ToString() + "-----------------------" + Environment.NewLine);
                    File.AppendAllText(json1, "tcDigitos " + usuario.tcDigitos + " tarjeta numero " + usuario.tcNum);

                    //complement transaccion with response
                    t.tarjetaID = usuario.tarjetaID;
                    t.tarjeta = usuario.tcNum.Substring(usuario.tcNum.Length - 4);
                    t.transaccion = response.charges_data_order_id;
                    t.descripcion = response.charges_data_description;
                    t.referencia = response.payment_method_reference;
                    t.aprobacion = response.charges_data_id;
                    t.monto = Decimal.Parse(response.data_object_amount) / 100;
                    t.abono1 = Decimal.Parse(response.data_object_amount) / 100;
                    t.abonoTotal = Decimal.Parse(response.data_object_amount) / 100;

                    decimal exchangeRateDown = Tools.GetPesoToDollarRate(-1);
                    decimal exchangeRateUp = Tools.GetPesoToDollarRate(1);
                    decimal TotalAmountUSD = (t.monto / exchangeRateDown);
                    Decimal TotalAmountMX = t.monto;

                    t.ComissionMX = 0;
                    t.ComissionUSD = 0;
                    t.TotalMX = TotalAmountMX;
                    t.TotalUSD = TotalAmountUSD;
                    t.ExchangeRateDown = exchangeRateDown;
                    t.ExchangeRateUp = exchangeRateUp;
                    t.PaidWithCredit = false;

                    t.ServiceInDlls = TotalAmountUSD + t.ComissionUSD;
                    t.ServiceInMXN = TotalAmountMX + t.ComissionMX;

                    decimal dIva = (decimal)0.16;

                    //ejemplo visa
                    t.cargo2 = t.monto * (decimal)(0.035); //3.5% of transaccion charge
                    t.cargo2iva = t.cargo2 * dIva;
                    t.cargo5 = (decimal)2.9;
                    t.cargo5iva = t.cargo5 * dIva;

                    t.ip = "";

                    db.transacciones.Add(t);
                    db.Entry(t).State = System.Data.Entity.EntityState.Added;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {

                string json1 = HostingEnvironment.MapPath(@"~/App_Data/PostSaveTransaction.txt");
                File.AppendAllText(json1, "Test on " + DateTime.Now.ToString() + "-----------------------" + Environment.NewLine);
                File.AppendAllText(json1, e.Message);
            }

        }

        #endregion private functions
    }

}
