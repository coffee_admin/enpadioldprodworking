﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Enpadi_WebUI.Controllers.Api
{
    public class TestConnectionController : ApiController
    {
        //[WebInvoke(
        //  Method = "POST",
        //  UriTemplate = "testconektaconection",
        //  RequestFormat = WebMessageFormat.Json,
        //  ResponseFormat = WebMessageFormat.Json,
        //  BodyStyle = WebMessageBodyStyle.Wrapped)]
        [HttpPost]
        public String TestConektaConection()
        {
            return "Connection to Enpadi via Post successful.";
        }

        //[WebInvoke(
        //  Method = "GET",
        //  UriTemplate = "testconektaconection2",
        //  RequestFormat = WebMessageFormat.Json,
        //  ResponseFormat = WebMessageFormat.Json,
        //  BodyStyle = WebMessageBodyStyle.Wrapped)]
        [HttpGet]
        public String TestConektaConection2()
        {
            return "Connection to Enpadi via Get successful.";
        }
    }
}
