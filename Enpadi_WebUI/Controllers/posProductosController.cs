﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Enpadi_WebUI.Models;

namespace Enpadi_WebUI.Controllers
{
    public class posProductosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: posProductos
        public ActionResult Index(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            var posProductoes = db.posProductoes.Include(p => p.moneda).Include(p => p.posCategoria).Include(p => p.posTienda).Include(p => p.posUnidad);
            return View(posProductoes.ToList());
        }

        // GET: posProductos/Details/5
        public ActionResult Details(int? id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            posProducto posProducto = db.posProductoes.Find(id);
            if (posProducto == null)
            {
                return HttpNotFound();
            }
            return View(posProducto);
        }

        // GET: posProductos/Create
        public ActionResult Create(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            ViewBag.monedaID = new SelectList(db.monedas, "monedaID", "codigo");
            ViewBag.categoriaID = new SelectList(db.posCategorias, "categoriaID", "categoria");
            ViewBag.tiendaID = new SelectList(db.posTiendas, "tiendaID", "nombre");
            ViewBag.unidadID = new SelectList(db.posUnidads, "unidadID", "unidad");
            return View();
        }

        // POST: posProductos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "productoID,tiendaID,categoriaID,producto,descripcion,imagen,unidadID,monto,monedaID")] posProducto posProducto, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                db.posProductoes.Add(posProducto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.monedaID = new SelectList(db.monedas, "monedaID", "codigo", posProducto.monedaID);
            ViewBag.categoriaID = new SelectList(db.posCategorias, "categoriaID", "categoria", posProducto.categoriaID);
            ViewBag.tiendaID = new SelectList(db.posTiendas, "tiendaID", "nombre", posProducto.tiendaID);
            ViewBag.unidadID = new SelectList(db.posUnidads, "unidadID", "unidad", posProducto.unidadID);
            return View(posProducto);
        }

        // GET: posProductos/Edit/5
        public ActionResult Edit(int? id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            posProducto posProducto = db.posProductoes.Find(id);
            if (posProducto == null)
            {
                return HttpNotFound();
            }
            ViewBag.monedaID = new SelectList(db.monedas, "monedaID", "codigo", posProducto.monedaID);
            ViewBag.categoriaID = new SelectList(db.posCategorias, "categoriaID", "categoria", posProducto.categoriaID);
            ViewBag.tiendaID = new SelectList(db.posTiendas, "tiendaID", "nombre", posProducto.tiendaID);
            ViewBag.unidadID = new SelectList(db.posUnidads, "unidadID", "unidad", posProducto.unidadID);
            return View(posProducto);
        }

        // POST: posProductos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "productoID,tiendaID,categoriaID,producto,descripcion,imagen,unidadID,monto,monedaID")] posProducto posProducto, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                db.Entry(posProducto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.monedaID = new SelectList(db.monedas, "monedaID", "codigo", posProducto.monedaID);
            ViewBag.categoriaID = new SelectList(db.posCategorias, "categoriaID", "categoria", posProducto.categoriaID);
            ViewBag.tiendaID = new SelectList(db.posTiendas, "tiendaID", "nombre", posProducto.tiendaID);
            ViewBag.unidadID = new SelectList(db.posUnidads, "unidadID", "unidad", posProducto.unidadID);
            return View(posProducto);
        }

        // GET: posProductos/Delete/5
        public ActionResult Delete(int? id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            posProducto posProducto = db.posProductoes.Find(id);
            if (posProducto == null)
            {
                return HttpNotFound();
            }
            return View(posProducto);
        }

        // POST: posProductos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            posProducto posProducto = db.posProductoes.Find(id);
            db.posProductoes.Remove(posProducto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
