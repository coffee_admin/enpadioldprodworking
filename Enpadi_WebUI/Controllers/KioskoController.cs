﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Enpadi_WebUI.Models;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using System.Net;
using Enpadi_WebUI.WSDiestel;
using System.Configuration;
using Enpadi_WebUI.Properties;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using EnpadiSpecialAttributes;

namespace Enpadi_WebUI.Controllers
{
    [Security(ControllerToRedirect = "Kiosko", ActionToRedirect = "login")]
    public class KioskoController : Controller
    {
        #region privae variables, constructors and properties

        private static bool bModoProduccion = Convert.ToBoolean(ConfigurationManager.AppSettings["ModoProduccion"]);
        private static string region = ConfigurationManager.AppSettings["Region"];

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        //private ApplicationDbContext db = new ApplicationDbContext();

        public KioskoController()
        {
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public KioskoController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        #region diestel

        private string DIESTEL_URL = (bModoProduccion) ? Settings.Default.Enpadi_WebUI_WSDiestel_PxUniversal : Settings.Default.Test_WebUI_WSDiestel_PxUniversal;
        private int DIESTEL_GRUPO = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Grupo : Settings.Default.Test_WSDiestel_Grupo;
        private int DIESTEL_CADENA = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Cadena : Settings.Default.Test_WSDiestel_Cadena;
        private int DIESTEL_TIENDA = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Tienda : Settings.Default.Test_WSDiestel_Tienda;
        private string DIESTEL_USUARIO = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Usuario : Settings.Default.Test_WSDiestel_Usuario;
        private string DIESTEL_PASSWORD = (bModoProduccion) ? Settings.Default.Enpadi_WSDiestel_Password : Settings.Default.Test_WSDiestel_Password;

        #endregion diestel

        #endregion privae variables, constructors and properties

        #region view

        public ActionResult Pay(string tipo)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            if (tipo == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            List<servicios> s = db.servicios.Where(x => x.Tipo == tipo).Where(x => x.Activo == true).Where(x => x.FechaBaja == null).ToList();

            return View("_Pay", s.OrderBy(x => x.SKU));
        }

        public ActionResult ProcessCemex()
        {
            return View(new Enpadi_WebUI.Models.ViewModelProcessCemex());
        }

        // GET: Kiosko
        public ActionResult Index()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            if (usuario != null && (usuario.email != null && usuario.email != String.Empty))
            {
                return View();
            }
            else
            {
                return View("LogIn");
            }
        }

        // GET: Kiosko
        [AllowAnonymous]
        public ActionResult LogIn()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            String skin = "Clean";
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            Enpadi_WebUI.Models.LoginViewModel model = new LoginViewModel();

            return View(model);
        }

        // GET: PanelUsuarios
        public ActionResult CheckService(string sku, string referencia)
        {
            object model = null;
            string currentUserId = User.Identity.GetUserId();
            Guid _currentUserId = Guid.Parse(currentUserId);

            try
            {
                //imss
                if (sku == servicios.keys.imss_new || sku == servicios.keys.imss_existing)
                {
                    model = Tools.CheckService_imss(sku, referencia, currentUserId, PlatformModel.Keys.EnpadiKiosko, 0);

                    return View("ProcessIMSS", model);
                }
                //cemex
                else if (
                  sku == servicios.keys.cemex_100 ||
                  sku == servicios.keys.cemex_200 ||
                  sku == servicios.keys.cemex_300 ||
                  sku == servicios.keys.cemex_400 ||
                  sku == servicios.keys.cemex_10 ||
                  sku == servicios.keys.cemex_5 ||
                  sku == servicios.keys.cemex_500)
                {
                    model = Tools.CheckService_cemex(sku, referencia, currentUserId, PlatformModel.Keys.EnpadiKiosko, 0);

                    return View("ProcessCemex", model);
                }
                //todos los demas
                else
                {
                    model = Tools.CheckService_diestel(sku, referencia, currentUserId, PlatformModel.Keys.EnpadiKiosko, 0);

                    return View("_CheckService", model);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return View("_CheckService", model);
            }
        }

        public ActionResult ProcessService(Guid? Id)
        {
            if (Id == null)
            {
                return RedirectToActionPermanent("Index");
            }

            ApplicationDbContext db = new ApplicationDbContext();

            pagoServicio result = new pagoServicio();
            pagoServicio oPago = db.pagoServicios.Where(x => x.Id == Id).FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            result = Tools.ServiciosImpresion(oPago, currentUserId);

            return View("_ProcessService", result);
        }

        [HttpPost]
        public ActionResult ProcessService(pagoServicio oPago)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            string currentUserId = User.Identity.GetUserId();
            Guid _currentUserId = Guid.Parse(currentUserId);
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
            servicios servicio = db.servicios.FirstOrDefault(x => x.SKU == oPago.sku);
            List<Fee> fees = Fee.GetFeeForServiceByKioskoUser(_currentUserId, servicio.Id, PlatformModel.Keys.EnpadiKiosko);

            oPago = Tools.PayServiceWithDiestel(oPago, currentUserId, PlatformModel.Keys.EnpadiKiosko, false, fees, true);

            return View("_ProcessService", oPago);
        }

        public ActionResult ProcessPay(string tipo)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            string strSQL = String.Empty;


            if (tipo == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            IEnumerable<servicios> s = db.servicios.Where(x => x.Tipo == tipo).Where(x => x.Activo == true).Where(x => x.FechaBaja == null).OrderBy(x => x.Nombre);

            return View(s);

        }

        #region printing services

        public ActionResult ServiciosImpresion(Guid Id)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            pagoServicio p = db.pagoServicios.Where(x => x.Id == Id).FirstOrDefault();
            string currentUserId = User.Identity.GetUserId();

            if (p == null)
            {
                p = new pagoServicio();
                p.transaccionesId = Id;
                p = Tools.ServiciosImpresion(p, currentUserId, true);
            }

            p = Tools.ServiciosImpresion(p, currentUserId);
            return View(p);
        }

        [HttpPost]
        public ActionResult ServiciosImpresion(pagoServicio oPago, string skin)
        {
            pagoServicio result = new pagoServicio();
            string currentUserId = User.Identity.GetUserId();
            result = Tools.ServiciosImpresion(oPago, currentUserId);
            return View(result);
        }

        public ActionResult serviciosdeimpresion(string transid)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            Guid _transid = Guid.Parse(transid);

            pagoServicio p = db.pagoServicios.Where(x => x.transaccionesId == _transid).FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();

            if (p == null)
            {
                p = new pagoServicio();
                p.transaccionesId = Guid.Parse(transid);
                p = Tools.ServiciosImpresion(p, currentUserId, true);
            }
            else
            {
                p = Tools.ServiciosImpresion(p, currentUserId);
            }


            return View("ServiciosImpresion", p);
        }

        #endregion printing services

        #region imss

        public ActionResult ProcessIMSSPayment(
           Guid? Id
            )
        {
            if (Id == null)
            {
                return RedirectToActionPermanent("Index");
            }
            ApplicationDbContext db = new ApplicationDbContext();
            pagoServicio result = new pagoServicio();
            pagoServicio oPago = db.pagoServicios.Where(x => x.Id == Id).FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            result = Tools.ServiciosImpresion(oPago, currentUserId);

            return View("_ProcessService", result);
        }

        [HttpPost]
        public ActionResult ProcessIMSSPayment(
            Guid Id,
            string sku,
            string name,
            string namelast,
            string namemothers,
            string imss_number,
            string phone_number,
            string cell_phone_number,
            string email,
            string buyer_name,
            string buyer_phone_number,
            string buyer_email
            )
        {
            ApplicationDbContext db = new ApplicationDbContext();
            pagoServicio oPago = db.pagoServicios.Where(x => x.Id == Id).FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            Guid _currentUserId = Guid.Parse(currentUserId);

            oPago = Tools.PayServiceIMSS(
                _currentUserId,
                sku,
                name,
                namelast,
                namemothers,
                imss_number,
                phone_number,
                cell_phone_number,
                email,
                buyer_name,
                buyer_phone_number,
                buyer_email,
                true,
                PlatformModel.Keys.EnpadiKiosko,
                oPago);

            return View("_ProcessService", oPago);
        }

        #endregion imss

        #region cemex

        public ActionResult ProcessCemexPayment(
          Guid? Id
           )
        {
            if (Id == null)
            {
                return RedirectToActionPermanent("Index");
            }
            ApplicationDbContext db = new ApplicationDbContext();
            pagoServicio result = new pagoServicio();
            pagoServicio oPago = db.pagoServicios.Where(x => x.Id == Id).FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            result = Tools.ServiciosImpresion(oPago, currentUserId);

            return View("_ProcessService", result);
        }

        [HttpPost]
        public ActionResult ProcessCemexPayment(
            Guid Id,
            string sku,
            string name,
            string namelast,
            string namemothers,
            string imss_number,
            string phone_number,
            string cell_phone_number,
            string email,
            string buyer_name,
            string buyer_phone_number,
            string buyer_email)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            pagoServicio oPago = db.pagoServicios.Where(x => x.Id == Id).FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();

            oPago = Tools.PayServiceCemex(
                currentUserId,
                sku,
                name,
                namelast,
                namemothers,
                imss_number,
                phone_number,
                cell_phone_number,
                email,
                buyer_name,
                buyer_phone_number,
                buyer_email,
                true,
                PlatformModel.Keys.EnpadiKiosko,
                oPago);

            return View("_ProcessService", oPago);
        }

        #endregion cemex

        #endregion views

        #region methods

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> SendEmail(String email, String message, Boolean spanish, string opagoId) {

            Guid _opagoId = Guid.Parse(opagoId);

            Boolean response = await Tools.SendEmail(email, message, spanish, _opagoId);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadBalance(int? periodo = 0)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            periodo = periodo * -1;

            string strSQL = String.Empty;
            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
            int tID = usuario.tarjetaID;

            strSQL = " SELECT numero, CONVERT(VARCHAR, Anio) + '  ' + CASE Mes WHEN 1 THEN 'ENERO' WHEN 2 THEN 'FEBRERO' WHEN 3 THEN 'MARZO' WHEN 4 THEN 'ABRIL' WHEN 5 THEN 'MAYO' WHEN 6 THEN 'JUNIO' WHEN 7 THEN 'JULIO' "
                      + " WHEN 8 THEN 'AGOSTO' WHEN 9 THEN 'SEPTIEMBRE' WHEN 10 THEN 'OCTUBRE' WHEN 11 THEN 'NOVIEMBRE' WHEN 12 THEN 'DICIEMBRE' END As Mes "
                      + " FROM ( "
                      + " SELECT 0 As numero, DATEPART(MONTH, DATEADD(MONTH, 0, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, 0, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -1 As numero, DATEPART(MONTH, DATEADD(MONTH, -1, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -1, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -2 As numero, DATEPART(MONTH, DATEADD(MONTH, -2, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -2, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -3 As numero, DATEPART(MONTH, DATEADD(MONTH, -3, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -3, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -4 As numero, DATEPART(MONTH, DATEADD(MONTH, -4, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -4, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -5 As numero, DATEPART(MONTH, DATEADD(MONTH, -5, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -5, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -6 As numero, DATEPART(MONTH, DATEADD(MONTH, -6, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -6, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -7 As numero, DATEPART(MONTH, DATEADD(MONTH, -7, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -7, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -8 As numero, DATEPART(MONTH, DATEADD(MONTH, -8, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -8, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -9 As numero, DATEPART(MONTH, DATEADD(MONTH, -9, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -9, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -10 As numero, DATEPART(MONTH, DATEADD(MONTH, -10, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -10, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -11 As numero, DATEPART(MONTH, DATEADD(MONTH, -11, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -11, GETDATE())) As Anio "
                      + " UNION "
                      + " SELECT -12 As numero, DATEPART(MONTH, DATEADD(MONTH, -12, GETDATE())) As Mes, DATEPART(YEAR, DATEADD(MONTH, -12, GETDATE())) As Anio "
                      + " ) As Meses "
                      + " ORDER BY numero DESC ";
            IEnumerable<periodo> meses = db.Database.SqlQuery<periodo>(strSQL);
            List<periodo> rmeses = meses.ToList();

            strSQL = " SELECT CASE DATEPART(MONTH, DATEADD(MONTH, " + periodo.ToString() + ", GetDate())) "
                   + " WHEN 1 THEN 'ENERO' WHEN 2 THEN 'FEBRERO' WHEN 3 THEN 'MARZO' WHEN 4 THEN 'ABRIL' "
                   + " WHEN 5 THEN 'MAYO' WHEN 6 THEN 'JUNIO' WHEN 7 THEN 'JULIO' WHEN 8 THEN 'AGOSTO' "
                   + " WHEN 9 THEN 'SEPTIEMBRE' WHEN 10 THEN 'OCTUBRE' WHEN 11 THEN 'NOVIEMBRE' WHEN 12 THEN 'DICIEMBRE' "
                   + " END + ' ' + CONVERT(varchar(10), DATEPART(YEAR, DATEADD(MONTH, " + periodo.ToString() + ", GetDate()))) AS [desc] ";
            IEnumerable<descripcion> descrip = db.Database.SqlQuery<descripcion>(strSQL);
            string periodo_descripcion = descrip.ToList().FirstOrDefault().desc;


            decimal rsaldoInicial = usuario.creditoDisponible.GetValueOrDefault();

            strSQL = " SELECT ISNULL(SUM(abonoTotal), 0) As Total "
                   + " FROM Transacciones "
                   + " WHERE tarjetaID = " + tID + " AND tipoID = 1 AND DateDiff(MONTH, GetDate(), FechaRegistro) = " + periodo.ToString();
            IEnumerable<total> abonos = db.Database.SqlQuery<total>(strSQL);
            decimal rabonos = abonos.ToList().FirstOrDefault().Total;

            strSQL = " SELECT ISNULL(SUM(ServiceInDlls), 0) As Total "
                   + " FROM Transacciones "
                   + " WHERE tarjetaID = " + tID + " AND tipoID = 2 AND DateDiff(MONTH, GetDate(), FechaRegistro) = " + periodo.ToString();
            IEnumerable<total> cargos = db.Database.SqlQuery<total>(strSQL);
            decimal rcargos = cargos.ToList().FirstOrDefault().Total;

            strSQL = " SELECT ISNULL(SUM(ServiceInDlls), 0) As Total "
           + " FROM Transacciones "
           + " WHERE tarjetaID = " + tID + " AND tipoID = 2 AND DateDiff(MONTH, GetDate(), FechaRegistro) = " + (periodo - 1).ToString();
            IEnumerable<total> cargosMesAnterior = db.Database.SqlQuery<total>(strSQL);
            decimal rcargosMesAnterior = cargosMesAnterior.ToList().FirstOrDefault().Total;


            decimal rsaldoFinal = usuario.creditoDisponible.GetValueOrDefault() - usuario.creditoPorPagar.GetValueOrDefault();

            rsaldoInicial = rsaldoInicial - rcargosMesAnterior;

            var result = new
            {
                periodo_descripcion,
                rsaldoInicial,
                rabonos,
                rcargos,
                rsaldoFinal,
                rmeses
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadGeneralReport(String periodo)
        {
            string currentUserId = User.Identity.GetUserId();
            Guid _currentUserId = Guid.Parse(currentUserId);

            return Json(Tools.GetDataForBasicReportWebsites(_currentUserId, periodo, 1), JsonRequestBehavior.AllowGet);
        }

        // GET: Kiosko
        public ActionResult Payments(int? periodo = 0)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            periodo = periodo * -1;

            if (User.Identity.GetUserId() == null)
            {
                return RedirectToAction("Index");
            }

            ViewModelInicio vm = new ViewModelInicio();
            string strSQL = string.Empty;

            vm.periodo = (int)periodo;

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
            int tID = usuario.tarjetaID;

            if (User.IsInRole(SecurityRoles.Concentradora))
                vm.perfil = SecurityRoles.Concentradora;
            else
                vm.perfil = SecurityRoles.Usuario;

            vm = Tools.LoadChards(vm, periodo.GetValueOrDefault(), usuario);

            return View("_Payments", vm);
        }

        [AllowAnonymous]
        public async Task<ActionResult> LogInValidation(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToActionPermanent("Index", "Kiosko");
                case SignInStatus.LockedOut:
                    return RedirectToActionPermanent("LogIn", model);
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return RedirectToActionPermanent("LogIn", model);
            }
        }

        public ActionResult LogOff()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            // Skin Aplicacion
            String skin = "Clean";
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            Enpadi_WebUI.Models.LoginViewModel model = new LoginViewModel();

            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Kiosko", model);
        }

        private String GetProfilePicture()
        {
            string result = String.Empty;
            string currentUserId = User.Identity.GetUserId();
            var user = UserManager.FindById(currentUserId);

            string path = System.IO.Path.Combine(
                                           Server.MapPath("~/UpLoads/ProfilePictures/"), currentUserId + ".jpg");

            if (System.IO.File.Exists(path))
            {
                AspNetUsersModel tempUser = AspNetUsersModel.Get(Guid.Parse(currentUserId), user.Email);
                result = tempUser.ProfilePicUrl;
            }
            else
            {
                result = "Images/account_no_image.png";
            }

            return result;
        }

        public JsonResult LoadUser()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            var contextDB = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
            var storeDB = new UserStore<IdentityUser>(contextDB);
            var managerDB = new UserManager<IdentityUser>(storeDB);
            var userDB = managerDB.FindByEmail(usuario.email);

            Decimal accountBalance = trans.GetAccountStatus(usuario.tarjetaID);
            Decimal exchangeRateUp = Tools.GetPesoToDollarRate(1);

            KioskoIndexViewModel model = new KioskoIndexViewModel
            {
                enpadiCard = EnpadiCardModel.Get(usuario.tarjetaID),
                user = AspNetUsersModel.Get(null, usuario.email),
                AccountBalance = accountBalance
            };

            model.enpadiCard.creditoDisponible = model.enpadiCard.creditoDisponible;
            model.enpadiCard.creditoPorPagar = model.enpadiCard.creditoPorPagar;
            model.AccountBalance = model.AccountBalance;


            model.user.ProfilePicUrl = GetProfilePicture();

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        #endregion methods

    }
}