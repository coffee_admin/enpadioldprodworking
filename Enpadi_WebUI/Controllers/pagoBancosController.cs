﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Enpadi_WebUI.Models;

namespace Enpadi_WebUI.Controllers
{
    public class pagoBancosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: pagoBancos
        public ActionResult Index()
        {
            return View(db.pagoBancoes.ToList());
        }

        // GET: pagoBancos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pagoBanco pagoBanco = db.pagoBancoes.Find(id);
            if (pagoBanco == null)
            {
                return HttpNotFound();
            }
            return View(pagoBanco);
        }

        // GET: pagoBancos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: pagoBancos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pagoId,id,description,error_message,authorization,amount,operation_type,payment_method,clabe,bank,type,name,order_id,transaction_type,creation_date,currency,status,method")] pagoBanco pagoBanco)
        {
            if (ModelState.IsValid)
            {
                db.pagoBancoes.Add(pagoBanco);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pagoBanco);
        }

        // GET: pagoBancos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pagoBanco pagoBanco = db.pagoBancoes.Find(id);
            if (pagoBanco == null)
            {
                return HttpNotFound();
            }
            return View(pagoBanco);
        }

        // POST: pagoBancos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "pagoId,id,description,error_message,authorization,amount,operation_type,payment_method,clabe,bank,type,name,order_id,transaction_type,creation_date,currency,status,method")] pagoBanco pagoBanco)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pagoBanco).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pagoBanco);
        }

        // GET: pagoBancos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pagoBanco pagoBanco = db.pagoBancoes.Find(id);
            if (pagoBanco == null)
            {
                return HttpNotFound();
            }
            return View(pagoBanco);
        }

        // POST: pagoBancos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            pagoBanco pagoBanco = db.pagoBancoes.Find(id);
            db.pagoBancoes.Remove(pagoBanco);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
