﻿using Enpadi_WebUI.Models;
using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Enpadi_WebUI.Controllers
{
    [Authorize]
    public class ModCityController : Controller
    {

        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ModCity
        public ActionResult Index(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        public ActionResult Payment()
        {
            return View();
        }
        public ActionResult Services(string tipo)
        {
            if (tipo == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            string strSQL = String.Format("SELECT SKU, Nombre, Compañia, Tipo, Activo FROM Servicios WHERE Tipo = '{0}' ORDER BY SKU", tipo);
            IEnumerable<servicios> s = db.Database.SqlQuery<servicios>(strSQL);
            return View(s);
        }

        [HttpPost]
        public ActionResult Services(string sku, string referencia, string monto)
        {
            decimal dMonto = 0;
            bool dConvert = decimal.TryParse(monto, out dMonto);

            if (sku.Length != 13)
                ModelState.AddModelError("", "Seleccione el Servicio a Pagar");
            if (referencia.Length != 28)
                ModelState.AddModelError("", "Proporcione un Número de Referencia Valido");
            if (dConvert == false)
                ModelState.AddModelError("", "Verifique el Monto a Pagar");

            if (ModelState.IsValid)
            {
                string currentUserId = User.Identity.GetUserId();
                tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

                string strSQL = String.Format("SELECT ISNULL(SUM(abonoTotal-cargoTotal),0) As Total FROM Transacciones WHERE tarjetaID = {0} AND tipoID IN (1,2)", usuario.tarjetaID);
                IEnumerable<total> saldoActual = db.Database.SqlQuery<total>(strSQL);

                if (saldoActual.ToList().FirstOrDefault().Total >= dMonto)
                {
                    string sTransaccion = DateTime.Now.ToString("yyyyMMddHHmmss");
                    trans cargo = new trans() { origenID = 1, tipoID = 2, modoID = 1, socioID = 3, terminalID = 1, monedaID = 484, tarjetaID = usuario.tarjetaID, monto = dMonto, abono1 = 0, cargo1 = dMonto, abono2 = 0, cargo2 = 0, abono3 = 0, cargo3 = 0, abonoTotal = 0, cargoTotal = dMonto, tarjeta = usuario.tcNum.Substring(usuario.tcNum.Length - 4), transaccion = sTransaccion, Concepto = "Cargo", descripcion = "Pago de Servicios", referencia = "32424258", estatusID = 1, aprobacion = "549832", ip = "189.25.169.74", geoCountryCode = 0, geoCountryName = String.Empty, geoRegion = String.Empty, geoCity = String.Empty, geoPostalCode = String.Empty };
                    db.transacciones.Add(cargo);

                    recompensa r = new recompensa() { tarjetaID = usuario.tarjetaID, origenID = 1, socioID = 3, transaccion = sTransaccion, concepto = "Recompensa Pago de Servicios", abono = dMonto * (decimal)0.01, cargo = 0 };
                    db.recompensas.Add(r);

                    db.SaveChanges();

                    return RedirectToAction("ServiciosRespuesta", "PanelUsuarios", new { sku = sku, referencia = referencia, autorizacion = string.Format("AUTORIZACIÓN  {0}", 56235987), total = string.Format("MONTO  ${0} MXN", dMonto) });

                }
                else
                {
                    ModelState.AddModelError("", "Saldo Insuficiente. Recargue su Cuenta");
                    strSQL = String.Format("SELECT SKU, Nombre, Compañia, Tipo, Activo FROM Servicios WHERE Tipo = (SELECT Tipo FROM Servicios WHERE SKU = '{0}') ORDER BY SKU", sku);
                    IEnumerable<servicios> s = db.Database.SqlQuery<servicios>(strSQL);
                    return View(s);
                }
            }
            else
            {
                string strSQL = String.Format("SELECT SKU, Nombre, Compañia, Tipo, Activo FROM Servicios WHERE Tipo = (SELECT Tipo FROM Servicios WHERE SKU = '{0}') ORDER BY SKU", sku);
                IEnumerable<servicios> s = db.Database.SqlQuery<servicios>(strSQL);
                return View(s);
            }

        }


    }
}