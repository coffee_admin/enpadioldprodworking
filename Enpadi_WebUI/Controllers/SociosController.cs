﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Enpadi_WebUI.Models;

namespace Enpadi_WebUI.Controllers
{
    [Authorize]
    public class SociosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Clientes
        public ActionResult Index(string skin)
        {
            skin = "clean";
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            var socios = db.socios.Include(c => c.ciudad).Include(c => c.estado).Include(c => c.pais);
            return View(socios.ToList());
        }

        // GET: Clientes/Details/5
        public ActionResult Details(int? id, string skin)
        {
            skin = "clean";
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            socio cliente = db.socios.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            return View(cliente);
        }

        // GET: Clientes/Create
        public ActionResult Create(string skin)
        {
            skin = "clean";
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            ViewBag.cdID = new SelectList(db.ciudades, "cdID", "nombre");
            ViewBag.edoID = new SelectList(db.estados, "edoID", "nombre");
            ViewBag.paisID = new SelectList(db.paises, "paisID", "nombre");
            return View();
        }

        // POST: Clientes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "cteID,nombreComercial,razonSocial,rfc,datNombre,datDir1,datDir2,datColonia,paisID,edoID,cdID,datCP,datTelefono,datEmail,datContacto,activo,admnUSR,admPASS,ventas,recargas,remesas,recompensas")] socio cliente, string skin)
        {
            skin = "clean";
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                db.socios.Add(cliente);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.cdID = new SelectList(db.ciudades, "cdID", "nombre", cliente.cdID);
            ViewBag.edoID = new SelectList(db.estados, "edoID", "nombre", cliente.edoID);
            ViewBag.paisID = new SelectList(db.paises, "paisID", "nombre", cliente.paisID);
            return View(cliente);
        }

        // GET: Clientes/Edit/5
        public ActionResult Edit(int? id, string skin)
        {
            skin = "clean";
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            socio cliente = db.socios.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            ViewBag.cdID = new SelectList(db.ciudades, "cdID", "nombre", cliente.cdID);
            ViewBag.edoID = new SelectList(db.estados, "edoID", "nombre", cliente.edoID);
            ViewBag.paisID = new SelectList(db.paises, "paisID", "nombre", cliente.paisID);
            return View(cliente);
        }

        // POST: Clientes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "cteID,nombreComercial,razonSocial,rfc,datNombre,datDir1,datDir2,datColonia,paisID,edoID,cdID,datCP,datTelefono,datEmail,datContacto,activo,admnUSR,admPASS,ventas,recargas,remesas,recompensas")] socio cliente, string skin)
        {
            skin = "clean";
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                db.Entry(cliente).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.cdID = new SelectList(db.ciudades, "cdID", "nombre", cliente.cdID);
            ViewBag.edoID = new SelectList(db.estados, "edoID", "nombre", cliente.edoID);
            ViewBag.paisID = new SelectList(db.paises, "paisID", "nombre", cliente.paisID);
            return View(cliente);
        }

        // GET: Clientes/Delete/5
        public ActionResult Delete(int? id, string skin)
        {
            skin = "clean";
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            socio cliente = db.socios.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            return View(cliente);
        }

        // POST: Clientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string skin)
        {
            skin = "clean";
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            socio cliente = db.socios.Find(id);
            db.socios.Remove(cliente);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
