﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Enpadi_WebUI.Models;
using PagedList;

namespace Enpadi_WebUI.Controllers
{
    public class SantiagoPredialController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: SantiagoPredial
        public ActionResult Index(string skin, string sortOrder, string currentFilter, string searchString, int? page)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            strSQL = String.Format("SELECT * FROM SantiagoPredial WHERE fechaBaja IS NULL");
            IEnumerable<santiagoPredial> predial = db.Database.SqlQuery<santiagoPredial>(strSQL);

            int pageSize = 50;
            int pageNumber = (page ?? 1);

            return View(predial.ToPagedList(pageNumber, pageSize));
        }

        // GET: SantiagoPredial/Details/5
        public ActionResult Details(int? id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            santiagoPredial santiagoPredial = db.santiagoPredials.Find(id);
            if (santiagoPredial == null)
            {
                return HttpNotFound();
            }
            return View(santiagoPredial);
        }

        // GET: SantiagoPredial/Create
        public ActionResult Create(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        // POST: SantiagoPredial/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "expediente,ubicacionPredio,rezago,actual,recargoRezago,descuento,recargoActual,total,saldo,fechaInicio,fechaVencimiento,codigoDeBarras,referenciaBancomer,numReg")] santiagoPredial santiagoPredial, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                db.santiagoPredials.Add(santiagoPredial);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(santiagoPredial);
        }

        // GET: SantiagoPredial/Edit/5
        public ActionResult Edit(int? id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            santiagoPredial santiagoPredial = db.santiagoPredials.Find(id);
            if (santiagoPredial == null)
            {
                return HttpNotFound();
            }
            return View(santiagoPredial);
        }

        // POST: SantiagoPredial/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "expediente,ubicacionPredio,rezago,actual,recargoRezago,descuento,recargoActual,total,saldo,fechaInicio,fechaVencimiento,codigoDeBarras,referenciaBancomer,numReg")] santiagoPredial santiagoPredial, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                db.Entry(santiagoPredial).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(santiagoPredial);
        }

        // GET: SantiagoPredial/Delete/5
        public ActionResult Delete(int? id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            santiagoPredial santiagoPredial = db.santiagoPredials.Find(id);
            if (santiagoPredial == null)
            {
                return HttpNotFound();
            }
            return View(santiagoPredial);
        }

        // POST: SantiagoPredial/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            santiagoPredial santiagoPredial = db.santiagoPredials.Find(id);
            db.santiagoPredials.Remove(santiagoPredial);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
