﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Enpadi_WebUI.Models;
using Microsoft.AspNet.Identity;

namespace Enpadi_WebUI.Controllers
{
    public class tarjetasContactosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: tarjetasContactos
        public ActionResult Index(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            string currentUserId = User.Identity.GetUserId();
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);

            strSQL = string.Format("SELECT * FROM TarjetasContactos WHERE tarjetaID = {0}", usuario.tarjetaID);
            IEnumerable<tarjetasContactos> tarjetasContactos = db.Database.SqlQuery<tarjetasContactos>(strSQL);
            return View(tarjetasContactos);
        }

        // GET: tarjetasContactos/Details/5
        public ActionResult Details(string skin, int? id)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tarjetasContactos tarjetasContactos = db.tarjetasContactos.Find(id);
            if (tarjetasContactos == null)
            {
                return HttpNotFound();
            }
            return View(tarjetasContactos);
        }

        // GET: tarjetasContactos/Create
        public ActionResult Create(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View();
        }

        // POST: tarjetasContactos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(string skin, [Bind(Include = "contactoID,tarjetaID,contactoTarjeta,contactoNumero,contactoAlias")] tarjetasContactos tarjetasContactos)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                string currentUserId = User.Identity.GetUserId();
                tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
                tarjetasContactos.tarjetaID = usuario.tarjetaID;

                strSQL = string.Format("SELECT Count(*) As total FROM Tarjetas WHERE tcNum = '{0}' AND tcAsignada = 1 AND tcActiva = 1 AND fechaBaja IS NULL", tarjetasContactos.contactoTarjeta);
                IEnumerable<contador> tarjetaExiste = db.Database.SqlQuery<contador>(strSQL);
                int iExiste = tarjetaExiste.ToList().FirstOrDefault().total;

                if(iExiste > 0)
                {
                    strSQL = string.Format("SELECT tarjetaID As total FROM Tarjetas WHERE tcNum = '{0}' AND tcAsignada = 1 AND tcActiva = 1 AND fechaBaja IS NULL", tarjetasContactos.contactoTarjeta);
                    IEnumerable<contador> contacto = db.Database.SqlQuery<contador>(strSQL);
                    tarjetasContactos.contactoNumero = contacto.ToList().FirstOrDefault().total;

                    if (tarjetasContactos.tarjetaID == tarjetasContactos.contactoNumero)
                    {
                        ModelState.AddModelError("", "No puede agregar su propia cuenta como Contacto");
                        return View(tarjetasContactos);
                    }
                    else
                    {
                        db.tarjetasContactos.Add(tarjetasContactos);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Numero de Cuenta Invalido");
                    return View(tarjetasContactos);
                }
            }
            return View(tarjetasContactos);
        }

        // GET: tarjetasContactos/Edit/5
        public ActionResult Edit(string skin, int? id)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tarjetasContactos tarjetasContactos = db.tarjetasContactos.Find(id);
            if (tarjetasContactos == null)
            {
                return HttpNotFound();
            }
            return View(tarjetasContactos);
        }

        // POST: tarjetasContactos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string skin, [Bind(Include = "contactoID,tarjetaID,contactoTarjeta,contactoNumero,contactoAlias")] tarjetasContactos tarjetasContactos)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                db.Entry(tarjetasContactos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tarjetasContactos);
        }

        // GET: tarjetasContactos/Delete/5
        public ActionResult Delete(string skin, int? id)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tarjetasContactos tarjetasContactos = db.tarjetasContactos.Find(id);
            if (tarjetasContactos == null)
            {
                return HttpNotFound();
            }
            return View(tarjetasContactos);
        }

        // POST: tarjetasContactos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string skin, int id)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            tarjetasContactos tarjetasContactos = db.tarjetasContactos.Find(id);
            db.tarjetasContactos.Remove(tarjetasContactos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
