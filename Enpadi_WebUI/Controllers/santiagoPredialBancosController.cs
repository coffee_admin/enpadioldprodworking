﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Enpadi_WebUI.Models;

namespace Enpadi_WebUI.Controllers
{
    public class santiagoPredialBancosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: santiagoPredialBancos
        public ActionResult Index(string skin)
        {
            // Skin Aplicacion
            string strSQL = String.Format("SELECT * FROM Skins WHERE name = '{0}'", skin);
            IEnumerable<skin> skinAplicacion = db.Database.SqlQuery<skin>(strSQL);
            ViewBag.skinApp = skinAplicacion.ToList().FirstOrDefault();

            return View(db.santiagoPredialBancos.ToList());
        }

        // GET: santiagoPredialBancos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            santiagoPredialBancos santiagoPredialBancos = db.santiagoPredialBancos.Find(id);
            if (santiagoPredialBancos == null)
            {
                return HttpNotFound();
            }
            return View(santiagoPredialBancos);
        }

        // GET: santiagoPredialBancos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: santiagoPredialBancos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idRef,expediente,ubicacionPredio,saldo,fechaAlta")] santiagoPredialBancos santiagoPredialBancos)
        {
            if (ModelState.IsValid)
            {
                db.santiagoPredialBancos.Add(santiagoPredialBancos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(santiagoPredialBancos);
        }

        // GET: santiagoPredialBancos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            santiagoPredialBancos santiagoPredialBancos = db.santiagoPredialBancos.Find(id);
            if (santiagoPredialBancos == null)
            {
                return HttpNotFound();
            }
            return View(santiagoPredialBancos);
        }

        // POST: santiagoPredialBancos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idRef,expediente,ubicacionPredio,saldo,fechaAlta")] santiagoPredialBancos santiagoPredialBancos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(santiagoPredialBancos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(santiagoPredialBancos);
        }

        // GET: santiagoPredialBancos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            santiagoPredialBancos santiagoPredialBancos = db.santiagoPredialBancos.Find(id);
            if (santiagoPredialBancos == null)
            {
                return HttpNotFound();
            }
            return View(santiagoPredialBancos);
        }

        // POST: santiagoPredialBancos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            santiagoPredialBancos santiagoPredialBancos = db.santiagoPredialBancos.Find(id);
            db.santiagoPredialBancos.Remove(santiagoPredialBancos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
