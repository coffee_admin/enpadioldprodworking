﻿using System.Web.Mvc;
using Enpadi_WebUI.Models;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Enpadi_WebUI.Controllers
{
    [Authorize]
    public class ModulosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Modulos
        public ActionResult RecompensasA1()
        {
            venta v = new venta();
            v.cuenta = "6517863215648975";
            return View(v);
        }

        [HttpPost]
        public ActionResult RecompensasA1(venta v)
        {
            var id = db.tarjetas.Where(t => t.tcNum == v.cuenta).Select(t => t.tarjetaID);

            if(id.Count() > 0) {
                recompensa r = new recompensa();
                r.tarjetaID = id.First();
                r.origenID = 1;
                r.socioID = 1;
                r.transaccion = DateTime.Now.ToString("yyyyMMddHHmmss");
                r.abono = v.puntos;
                r.cargo = 0;
                r.concepto = "Puntos";
                db.recompensas.Add(r);
                db.SaveChanges();
            }
            else
            {
                ModelState.AddModelError("dfsfsf", "sdfsfsfs");
            }

            if(!ModelState.IsValid)
            {
                return View();
            }

            //Procedimiento Valido
            TempData["v"] = v;
            return RedirectToAction("RecompensasA2");
        }

        // GET: Modulos
        public ActionResult RecompensasA2()
        {
            venta v = (venta)TempData["v"];
            return View(v);
        }


        // GET: Modulos
        public ActionResult RecompensasC1()
        {
            venta v = new venta();
            v.cuenta = "6517863215648975";
            return View(v);
        }

        [HttpPost]
        public ActionResult RecompensasC1(venta v)
        {
            if(!ModelState.IsValid)
            {
                return View();
            }

            //Procedimiento Valido
            return RedirectToAction("FarmaciaC2");
        }

        public ActionResult RecompensasC2()
        {
            return View();
        }

        // GET: Modulos
        public ActionResult Remesas()
        {
            ViewModelServices s = new ViewModelServices();

            string strSQL = "SELECT * FROM Paises WHERE activo = 1";
            IEnumerable<pais> paises = db.Database.SqlQuery<pais>(strSQL);
            s.paises = paises.ToList();
            return View(s);
        }

        [HttpPost]
        public ActionResult Remesas(ViewModelServices s)
        {
            if(!ModelState.IsValid)
            {
                string strSQL = "SELECT * FROM Paises WHERE activo = 1";
                IEnumerable<pais> paises = db.Database.SqlQuery<pais>(strSQL);
                s.paises = paises.ToList();
                return View(s);
              
            }
            //Procedimiento Valido
            return RedirectToAction("ServicesUSA1", "Modulos");
        }

        public JsonResult GetStates(int? id)
        {
            string strSQL = string.Format("SELECT * FROM Estados WHERE paisID = {0}", id);
            IEnumerable<estado> estados = db.Database.SqlQuery<estado>(strSQL);
            return Json(estados.ToList());
        }

        public JsonResult GetCities(int? id)
        {
            string strSQL = string.Format("SELECT * FROM Ciudades WHERE edoID = {0}", id);
            IEnumerable<ciudad> ciudades = db.Database.SqlQuery<ciudad>(strSQL);
            return Json(ciudades.ToList());
        }

    }
}