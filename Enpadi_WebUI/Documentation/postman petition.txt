﻿http://localhost:45093/api/enpadi

{
	"orderdata": {
		"livemode": false,
		"amount": 20000,
		"currency": "MXN",
		"amount_refunded": 0,
		"customer_info": {
			"email": "customer14@truewisdom.co",
			"phoneNumber": "+52818185256",
			"name": "Adrian",
			"last_name": "Morales",
			"externalId": "",
			"object_description": "customer.info",
			"address_line1":"street",
			"address_line2":"optional",
			"address_line3":"optional",
			"address_city":"Monterrey",
			"address_state":"Nuevo Leon",
			"address_country_code":"MX",
			"address_postal_code":"66220"
		},
		"external_id": "external id",
		"external_description": " external description",
		"external_category": " external category",
		"object_description": "order",
		"line_items": [{
			"unit_price": 10000,
			"quantity": 2,
			"name": "product name",
			"description": "product description"
		}],
		"client_email": "amorales@coffee-systems.com",
		"client_trans_password": "enpadiclientpassword"
	},
	"chargedata": {
		"livemode": false,
		"amount": 20000,
		"currency": "MXN",
		"payment_method": {
			"service_name": "Pago de servicio",
			"object_description": "Pago a CFE",
			"payment_method_type": "oxxo_cash",
			"deviceSessionId":"",
			"source_id":"",
			"expires_at": "1513036800",
			"reference": "",
			"name_on_card": "",
			"credit_card_type": "",
			"card_number": "",
			"credit_card_expiration_month": "",
			"credit_card_expiration_year": "",
			"credit_card_security_code": "",
			"credit_card_maestro_switch": ""
		},
		"customer_email": "customer14@truewisdom.co"
	},
	"type": "order.create"
}



http://localhost:45093/api/conekta

{
"data":{
	"object":{
		"livemode":false,
		"amount":200000,
		"currency":"MXN",
		"payment_status":"paid",
		"amount_refunded":0,
		"customer_info":{
			"email":"acontreras@truewisdom.co",
			"phone":"0005455455545",
			"name":"testtest  ",
			"object":"customer_info"
			},
		"object":"order",
		"id":"ord_2gf9LAdunfNSFnoqJ",
		"metadata":{},
		"created_at":1496947071,
		"updated_at":1496947105,
		"line_items":{
				"object":"list",
				"has_more":false,
				"total":1,
				"data":[{
					"name":"Recarga en Oxxo",
					"unit_price":200000,
					"quantity":1,
					"object":"line_item",
					"id":"line_item_2gf9LAdunfNSFnoqG",
					"parent_id":"ord_2gf9LAdunfNSFnoqJ",
					"metadata":{},
					"antifraud_info":{}}
					]
		},
		"charges":{
			"object":"list",
			"has_more":false,
			"total":1,
			"data":[{
					"id":"5939997fedbb6e32380a4336",
					"livemode":false,
					"created_at":1496947071,
					"currency":"MXN",
					"payment_method":{
							"service_name":"Card",
							"deviceSessionId":"Obo91dBOiSBxKBDn2b1f7FF1hfJGXxvG",
							"source_id":"kctworhuugtgpiggxzdt",
							"object":"cash_payment",
							"type":"oxxo",
							"expires_at":1513036800,
							"store_name":"OXXO",
							"reference":"93345678901234"
							},
					"object":"charge",
					"description":"Payment from order",
					"status":"paid",
					"amount":20000,
					"paid_at":1496947105,
					"fee":812,
					"customer_id":"",
					"order_id":"ord_2gf9LAdunfNSFnoqJ"}]
		}
	},"previous_attributes":{}
},
"livemode":false,
"webhook_status":"pending",
"webhook_logs":[{
	"id":"webhl_2gf9LcnXuVncMrUPv",
	"url":"http://enpadi.com/EnpadiWebTest/api/conekta",
	"failed_attempts":0,
	"last_http_response_status":-1,
	"object":"webhook_log",
	"last_attempted_at":1496947109
}],
"id":"593999a25906e70f5064ff04",
"object":"event",
"type":"order.paid",
"created_at":1496947106
}