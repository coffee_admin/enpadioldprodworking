﻿
var coffeetools = {
    //help clean a form
    clearForm: function clearForm() {

        $('#employeeForm').each(function () {

            //Clear the fields
            $(this).find("input:not([type=submit], [type=checkbox]), textarea").not("[noclear]").val("");
            $(this).find("input[type=checkbox]").not("[noclear]").prop("checked", false);
            $(this).find("img").not("[noclear]").attr("src", "/img/no-image.jpg");
            $(this).find("select").not("[noclear]").val("0");

            //Clear the TinyMCE editors.
            if (typeof tinyMCE !== 'undefined') {
                for (i = 0; i < tinyMCE.editors.length; i++) {
                    tinyMCE.editors[i].setContent("");
                    tinyMCE.editors[i].save();
                }
            }

            //Clear the multiselects.
            $(this).find("select[multiple]").each(function () {
                $(this).zmultiselect('uncheckall');
            });

            //$(this).find('#tbl_related_to').DataTable().clear().draw();
            //$(this).find('#tbl_prices').DataTable().clear().draw();
            $(this).find('.cs_element').remove();
        });
    },

    //change c# datetime to javascript datetime
    ToJavaScriptDate: function ToJavaScriptDate(value) {
        try {
            var pattern = /Date\(([^)]+)\)/;
            var results = pattern.exec(value);
            var dt = new Date(parseFloat(results[1]));

            var dd = dt.getDate();
            var mm = dt.getMonth() + 1; //January is 0!
            if (dd < 10) {
                dd = '0' + dd
            }
            if (mm < 10) {
                mm = '0' + mm
            }


            return mm + "/" + dd + "/" + dt.getFullYear();
        } catch (e) {
            return "no date";
            //layoutErrorMessage('Error JavaScript: ' + 'Request Status: ' + e.status + ' Status Text: ' + e.statusText + ' ' + e.responseText + '-----------------')
        }
    },

    //change c# datetime to javascript datetime
    ToJavaScriptDateTime: function ToJavaScriptDateTime(value) {
        try {
            var pattern = /Date\(([^)]+)\)/;
            var results = pattern.exec(value);
            var dt = new Date(parseFloat(results[1]));

            var dd = dt.getDate();
            var mm = dt.getMonth() + 1; //January is 0!

            var hours = dt.getHours();
            var minutes = dt.getMinutes();
            var tt = "AM";


            if (dd < 10) {
                dd = '0' + dd
            }
            if (mm < 10) {
                mm = '0' + mm
            }


            if (hours > 12) {
                hours -= 12;
                tt = "PM";
            } else if (hours === 0) {
                hours = 12;
                tt = "AM";
            }

            if (minutes < 9) {
                minutes = "0" + minutes;
            }


            return mm + "/" + dd + "/" + dt.getFullYear() + " " + hours + ":" + minutes + " " + tt;
        } catch (e) {
            return "no date";
            //layoutErrorMessage('Error JavaScript: ' + 'Request Status: ' + e.status + ' Status Text: ' + e.statusText + ' ' + e.responseText + '-----------------')
        }
    },

    //change c# datetime to javascript datetime
    ToJavaScriptDateTime24: function ToJavaScriptDateTime24(value) {
        try {
            var pattern = /Date\(([^)]+)\)/;
            var results = pattern.exec(value);
            var dt = new Date(parseFloat(results[1]));

            var dd = dt.getDate();
            var mm = dt.getMonth() + 1; //January is 0!

            var hours = dt.getHours();
            var minutes = dt.getMinutes();

            if (dd < 10) {
                dd = '0' + dd
            }
            if (mm < 10) {
                mm = '0' + mm
            }

            if (minutes < 9) {
                minutes = "0" + minutes;
            }


            return mm + "/" + dd + "/" + dt.getFullYear().toString().substr(-2) + " " + hours + ":" + minutes + " ";
        } catch (e) {
            return "no date";
            //layoutErrorMessage('Error JavaScript: ' + 'Request Status: ' + e.status + ' Status Text: ' + e.statusText + ' ' + e.responseText + '-----------------')
        }
    },

    //change c# datetime to javascript datetime
    ToJavaScriptTime: function ToJavaScriptTime(value) {
        try {
            var pattern = /Date\(([^)]+)\)/;
            var results = pattern.exec(value);
            var dt = new Date(parseFloat(results[1]));

            var hours = dt.getHours();
            var minutes = dt.getMinutes();
            var tt = "AM";

            if (hours > 12) {
                hours -= 12;
                tt = "PM";
            } else if (hours === 0) {
                hours = 12;
                tt = "AM";
            }

            if (minutes < 9) {
                minutes = "0" + minutes;
            }

            return hours + ":" + minutes + " " + tt;

        } catch (e) {
            return "no date";
            //layoutErrorMessage('Error JavaScript: ' + 'Request Status: ' + e.status + ' Status Text: ' + e.statusText + ' ' + e.responseText + '-----------------')
        }
    },

    //returns DateTime.now
    DateTimeNow: function DateTimeNow(extraDays) {
        try {
            //add or substract days
            var dat = new Date();
            dat.setDate(dat.getDate() + extraDays);

            //get the date in a usefull format
            var today = dat;
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd
            }

            if (mm < 10) {
                mm = '0' + mm
            }

            today = mm + '/' + dd + '/' + yyyy;

            return today;
        } catch (e) {
            return "no date";
            //layoutErrorMessage('Error JavaScript: ' + 'Request Status: ' + e.status + ' Status Text: ' + e.statusText + ' ' + e.responseText + '-----------------')
        }
    },

    formatMoney: function (number, c, d, t) {
        var n = number,
            c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },

    getRelativeUrl: function (url, region) {

        var relativeUrl = "";
        switch (region) {
            case "usa":
                relativeUrl = "/usa/";
                break;
            case "mx":
                relativeUrl = "/mx/";
                break;
            case "dev":
                relativeUrl = "";
                break;
            default:
                break;
        }

        return relativeUrl + url.toString().trim();
    },

    changeMonthLanguage: function changeMonthLanguage(monthArray) {

        var result = [];

        if (sessionStorage.lang == 'en') {
            monthArray.forEach(function (element) {

                switch (element.substring(0, 3)) {
                    case "ENE":
                        result.push("JAN" + element.substring(3, 6));
                        break;
                    case "FEB":
                        result.push("FEB" + element.substring(3, 6));
                        break;
                    case "MZO":
                        result.push("MAR" + element.substring(3, 6));
                        break;
                    case "ABR":
                        result.push("APR" + element.substring(3, 6));
                        break;
                    case "MAY":
                        result.push("MAY" + element.substring(3, 6));
                        break;
                    case "JUN":
                        result.push("JUN" + element.substring(3, 6));
                        break;
                    case "JUL":
                        result.push("JUL" + element.substring(3, 6));
                        break;
                    case "AGO":
                        result.push("AUG" + element.substring(3, 6));
                        break;
                    case "SEP":
                        result.push("SEP" + element.substring(3, 6));
                        break;
                    case "OCT":
                        result.push("OCT" + element.substring(3, 6));
                        break;
                    case "NOV":
                        result.push("NOV" + element.substring(3, 6));
                        break;
                    case "DIC":
                        result.push("DEC" + element.substring(3, 6));
                        break;
                }

            });
        } else {
            result = monthArray;
        }

        return result;
    },

    format: function format(d) {
        var amountForServiceLabel = "$" + coffeetools.formatMoney(d.AmountForServiceMXN, 2, '.', ',') + " MXN ";
        var totalAmount = "$" + coffeetools.formatMoney(d.TotalAmountMX, 2, '.', ',') + " MXN ";

        if (d.transaccion_socioID === 11) {
            amountForServiceLabel = "$" + coffeetools.formatMoney(d.AmountForServiceUSD, 2, '.', ',') + " USD ";
            totalAmount = "$" + coffeetools.formatMoney(d.TotalAmountUSD, 2, '.', ',') + " USD ";
        }
        else if (d.transaccion_socioID === 12) {
            amountForServiceLabel = "$" + coffeetools.formatMoney(d.AmountForServiceUSD, 2, '.', ',') + " USD ";
            totalAmount = "$" + coffeetools.formatMoney(d.TotalAmountUSD, 2, '.', ',') + " USD ";
        }

        // `d` is the original data object for the row
        var result = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +

            '<tr>' +
            '<td class="lang" key="table.movementtype">Tipo de Movimiento:</td>' +
            '<td>' + d.type_descripcion + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.foliotransaction">Folio transacci&oacute;n:</td>' +
            '<td>' + d.transaccion_transaccion + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.description">Descripci&oacute;n:</td>' +
            '<td>' + d.transaccion_descripcion + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.reference">Referencia:</td>' +
            '<td>' + d.transaccion_referencia + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.enpadicard">Tarjeta:</td>' +
            '<td>' + d.transaccion_tarjeta + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.service">Servicio:</td>' +
            '<td>' + amountForServiceLabel + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.fee">Comisi&oacute;n:</td>' +
            '<td>' + "$" + coffeetools.formatMoney(d.transaccion_comision, 2, '.', ',') + " USD " + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.total">Total:</td>' +
            '<td>' + totalAmount + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.electronicpursetotalcharge">Total debitado en MXN:</td>' +
            '<td>' + "$" + coffeetools.formatMoney(d.TotalAmountMX, 2, '.', ',') + " MXN " + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.beneficiary">Beneficiario:</td>' +
            '<td>' + d.beneficiary + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.buyer">Comprador:</td>' +
            '<td>' + d.buyer + '</td>' +
            '</tr>' +
            '</table>';

        return result;
    },
    loadtableWebUsa: function loadtableWebUsa(result) {

        var path = window.location.protocol + "//" + window.location.host;

        var langFileUrl = "/lang/Spanish.json";

        if (sessionStorage.lang === "en") {
            langFileUrl = "/lang/English.json";
        }

        $('#date').text();
        $('#start_balance').text();
        $('#deposits').text();
        $('#charges').text();
        $('#final_balance').text();

        //config variables
        var firstColumn = 0;
        var lastColumn = 8;
        //console.log(result);

        //configure search boxes
        var table_search = $('#tbl_payment thead th').each(function (key, value) {
            var title = $(this).attr('key');
            var text = $(this).text();

            if (key != firstColumn && key != lastColumn) {
                //here!! finaly the answer!
                $(this).html('<label class="lang" key="' + title.trim() + '">' + text.trim() + '</label>' + '<input style="width: 85px;" type="text" />');
            }
        });

        //create table
        var table = $('#tbl_payment').DataTable({
            destroy: true,
            oLanguage: {
                "sUrl": langFileUrl
            },
            dom: 'Bfrtip',
            buttons: [{
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL', exportOptions: {
                    columns: [1, 2, 3, 4, 5, 7, 9, 11, 14, 16, 17, 18]
                },
                customize: function (doc) {
                    doc.styles['td:nth-child(2)'] = {
                        width: '100px',
                        'max-width': '100px'
                    }
                }
            },
            {
                extend: 'copy',
                text: '<div class="lang" key="general.copy"></div>',
            },
            {
                extend: 'csv',
                text: 'CSV'
            },
            {
                extend: 'excel',
                text: 'Excel'
            },
            {
                extend: 'print',
                text: '<div class="lang" key="general.print"></div>'
            },
            ],
            language: {
                buttons: {
                    copyTitle: 'Tabla copiada',
                    copySuccess: {
                        _: '%d lineas copiadas',
                        1: '1 linea copiada'
                    }
                }
            },
            "processing": true,
            data: result,

            columns: [
                {
                    className: 'details-control',
                    orderable: false,
                    data: null,
                    defaultContent: '',
                    render: function () {
                        return '<i class="fa fa-plus-square" aria-hidden="true"></i>';
                    },
                    width: "15px"
                },
                {
                    data: "transaccion_transID"
                },
                {
                    data: "transaccion_monto_con_comission",
                    render: function (data, type, row) {
                        return "$" + coffeetools.formatMoney(data, 2, '.', ',') + " USD ";
                    }
                },
                {
                    data: "transaccion_concepto"
                },
                {

                    data: "status_descripcion"
                },
                {
                    data: "transaccion_fechaRegistro",
                    render: function (data, type, row) {
                        return coffeetools.ToJavaScriptDateTime24(data);
                    }
                },
                {
                    data: "ReprintReceipt",
                    render: function (data, type, row) {
                        return "<a href='serviciosdeimpresion?transid=" + data + "' class='lang' key='table.receipt'>RECIBO</a>";
                    }
                },
                {

                    data: "transaccion_transaccion",
                    visible: false
                },
                {

                    data: "transaccion_descripcion",
                    visible: false
                },
                {

                    data: "transaccion_referencia",
                    visible: false
                },
                {

                    data: "transaccion_tarjeta",
                    visible: false
                },
                {

                    data: "type_descripcion",
                    visible: false
                },
                {

                    data: "AmountForServiceMXN",
                    visible: false
                },
                {

                    data: "transaccion_comision",
                    visible: false
                },
                {

                    data: "transaccion_iva",
                    visible: false
                },
                {

                    data: "currency_codigo",
                    visible: false
                },
                {

                    data: "beneficiary",
                    visible: false
                },
                {
                    data: "buyer",
                    visible: false
                }
            ]
        });

        // Add event listener for opening and closing details
        $('#tbl_payment tbody').off('click', 'td.details-control');
        $('#tbl_payment tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var tdi = tr.find("i.fa");
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
                tdi.first().removeClass('fa-minus-square');
                tdi.first().addClass('fa-plus-square');
            }
            else {
                // Open this row
                row.child(coffeetools.format(row.data())).show();
                tr.addClass('shown');
                tdi.first().removeClass('fa-plus-square');
                tdi.first().addClass('fa-minus-square');

                ChangeDivLanguages();
            }
        });

        table.on("user-select", function (e, dt, type, cell, originalEvent) {
            if ($(cell.node()).hasClass("details-control")) {
                e.preventDefault();
            }
        });

        // Apply the search
        table.columns().every(function (key, value) {

            if (key != firstColumn && key != lastColumn) {
                var that = this;
                $('input', this.header()).on('keyup change', function () {
                    if (that.search() !== this.value) {
                        that
                            .search(this.value)
                            .draw();
                    }
                });
            }
        });

        var sum = table.column(1).data().eq(0).reduce(function (a, b) {
            return a + b;
        }, 0);

        ChangeDivLanguages();


    },
    loadTableDetailWebUsaAdmin: function format(d) {
        var amountForServiceLabel = "$" + coffeetools.formatMoney(d.AmountForServiceMXN, 2, '.', ',') + " MXN ";
        var totalAmount = "$" + coffeetools.formatMoney(d.TotalAmountMX, 2, '.', ',') + " MXN ";

        if (d.transaccion_socioID === 11) {
            amountForServiceLabel = "$" + coffeetools.formatMoney(d.AmountForServiceUSD, 2, '.', ',') + " USD ";
            totalAmount = "$" + coffeetools.formatMoney(d.TotalAmountUSD, 2, '.', ',') + " USD ";
        }
        else if (d.transaccion_socioID === 12) {
            amountForServiceLabel = "$" + coffeetools.formatMoney(d.AmountForServiceUSD, 2, '.', ',') + " USD ";
            totalAmount = "$" + coffeetools.formatMoney(d.TotalAmountUSD, 2, '.', ',') + " USD ";
        }

        // `d` is the original data object for the row
        var result = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +

            '<tr>' +
            '<td class="lang" key="table.movementtype">Tipo de Movimiento:</td>' +
            '<td>' + d.type_descripcion + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.foliotransaction">Folio transacci&oacute;n:</td>' +
            '<td>' + d.transaccion_transaccion + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.description">Descripci&oacute;n:</td>' +
            '<td>' + d.transaccion_descripcion + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.reference">Referencia:</td>' +
            '<td>' + d.transaccion_referencia + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.enpadicard">Tarjeta:</td>' +
            '<td>' + d.transaccion_tarjeta + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.service">Servicio:</td>' +
            '<td>' + amountForServiceLabel + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.fee">Comisi&oacute;n:</td>' +
            '<td>' + "$" + coffeetools.formatMoney(d.transaccion_comision, 2, '.', ',') + " USD " + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.date">Fecha:</td>' +
            '<td>' + coffeetools.ToJavaScriptDateTime24(d.transaccion_fechaRegistro); + '</td>' +
                '</tr>' +

                '<tr>' +
                '<td class="lang" key="table.electronicpursetotalcharge">Total debitado en MXN:</td>' +
                '<td>' + "$" + coffeetools.formatMoney(d.TotalAmountMX, 2, '.', ',') + " MXN " + '</td>' +
                '</tr>' +

                '<tr>' +
                '<td class="lang" key="table.beneficiary">Beneficiario:</td>' +
                '<td>' + d.beneficiary + '</td>' +
                '</tr>' +

                '<tr>' +
                '<td class="lang" key="table.buyer">Comprador:</td>' +
                '<td>' + d.buyer + '</td>' +
                '</tr>' +
                '</table>';

        return result;
    },
    loadtableWebUsaAdmin: function loadtableWebUsa(result) {

        var path = window.location.protocol + "//" + window.location.host;

        var langFileUrl = "/lang/Spanish.json";

        if (sessionStorage.lang === "en") {
            langFileUrl = "/lang/English.json";
        }

        $('#date').text();
        $('#start_balance').text();
        $('#deposits').text();
        $('#charges').text();
        $('#final_balance').text();

        //config variables
        var firstColumn = 0;
        var lastColumn = 8;
        //console.log(result);

        //configure search boxes
        var table_search = $('#tbl_payment thead th').each(function (key, value) {
            var title = $(this).attr('key');
            var text = $(this).text();

            if (key != firstColumn && key != lastColumn) {
                //here!! finaly the answer!
                $(this).html('<label class="lang" key="' + title.trim() + '">' + text.trim() + '</label>' + '<input style="width: 85px;" type="text" />');
            }
        });

        //create table
        var table = $('#tbl_payment').DataTable({
            destroy: true,
            oLanguage: {
                "sUrl": langFileUrl
            },
            dom: 'Bfrtip',
            buttons: [{
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL', exportOptions: {
                    columns: [1, 2, 3, 4, 5, 7, 9, 11, 14, 16, 17, 18]
                },
                customize: function (doc) {
                    doc.styles['td:nth-child(2)'] = {
                        width: '100px',
                        'max-width': '100px'
                    }
                }
            },
            {
                extend: 'copy',
                text: '<div class="lang" key="general.copy"></div>',
            },
            {
                extend: 'csv',
                text: 'CSV'
            },
            {
                extend: 'excel',
                text: 'Excel'
            },
            {
                extend: 'print',
                text: '<div class="lang" key="general.print"></div>'
            },
            ],
            language: {
                buttons: {
                    copyTitle: 'Tabla copiada',
                    copySuccess: {
                        _: '%d lineas copiadas',
                        1: '1 linea copiada'
                    }
                }
            },
            "processing": true,
            data: result,

            columns: [
                {
                    className: 'details-control',
                    orderable: false,
                    data: null,
                    defaultContent: '',
                    render: function () {
                        return '<i class="fa fa-plus-square" aria-hidden="true"></i>';
                    },
                    width: "15px"
                },
                {
                    data: "transaccion_transID"
                },
                {
                    data: "transaccion_concepto"
                },
                {
                    data: "AmountToShowInWebUsaAdminReport",
                }, {
                    data: "ComissionUSD",
                    render: function (data, type, row) {
                        return "$" + coffeetools.formatMoney(data, 2, '.', ',') + " USD ";
                    }
                },
                {
                    data: "status_descripcion"
                },
                {
                    data: "transactionUserEmail"
                }

            ]
        });

        // Add event listener for opening and closing details
        $('#tbl_payment tbody').off('click', 'td.details-control');
        $('#tbl_payment tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var tdi = tr.find("i.fa");
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
                tdi.first().removeClass('fa-minus-square');
                tdi.first().addClass('fa-plus-square');
            }
            else {
                // Open this row
                row.child(coffeetools.loadTableDetailWebUsaAdmin(row.data())).show();
                tr.addClass('shown');
                tdi.first().removeClass('fa-plus-square');
                tdi.first().addClass('fa-minus-square');

                ChangeDivLanguages();
            }
        });

        table.on("user-select", function (e, dt, type, cell, originalEvent) {
            if ($(cell.node()).hasClass("details-control")) {
                e.preventDefault();
            }
        });

        // Apply the search
        table.columns().every(function (key, value) {

            if (key != firstColumn && key != lastColumn) {
                var that = this;
                $('input', this.header()).on('keyup change', function () {
                    if (that.search() !== this.value) {
                        that
                            .search(this.value)
                            .draw();
                    }
                });
            }
        });

        var sum = table.column(1).data().eq(0).reduce(function (a, b) {
            return a + b;
        }, 0);

        ChangeDivLanguages();


    },
    loadtableKioskoAdmin: function loadtableKioskoAdmin(result) {

        var path = window.location.protocol + "//" + window.location.host;

        var langFileUrl = "/lang/Spanish.json";

        if (sessionStorage.lang === "en") {
            langFileUrl = "/lang/English.json";
        }

        $('#date').text();
        $('#start_balance').text();
        $('#deposits').text();
        $('#charges').text();
        $('#final_balance').text();

        //config variables
        var firstColumn = 0;
        var lastColumn = 8;
        //console.log(result);

        //configure search boxes
        var table_search = $('#tbl_payment thead th').each(function (key, value) {
            var title = $(this).attr('key');
            var text = $(this).text();

            if (key != firstColumn && key != lastColumn) {
                //here!! finaly the answer!
                $(this).html('<label class="lang" key="' + title.trim() + '">' + text.trim() + '</label>' + '<input style="width: 85px;" type="text" />');
            }
        });

        //create table
        var table = $('#tbl_payment').DataTable({
            destroy: true,
            oLanguage: {
                "sUrl": langFileUrl
            },
            dom: 'Bfrtip',
            buttons: [{
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL', exportOptions: {
                    columns: [1, 2, 3, 4, 5, 7, 9, 11, 14, 16, 17, 18]
                },
                customize: function (doc) {
                    doc.styles['td:nth-child(2)'] = {
                        width: '100px',
                        'max-width': '100px'
                    }
                }
            },
            {
                extend: 'copy',
                text: '<div class="lang" key="general.copy"></div>',
            },
            {
                extend: 'csv',
                text: 'CSV'
            },
            {
                extend: 'excel',
                text: 'Excel'
            },
            {
                extend: 'print',
                text: '<div class="lang" key="general.print"></div>'
            },
            ],
            language: {
                buttons: {
                    copyTitle: 'Tabla copiada',
                    copySuccess: {
                        _: '%d lineas copiadas',
                        1: '1 linea copiada'
                    }
                }
            },
            "processing": true,
            data: result,

            columns: [
                {
                    className: 'details-control',
                    orderable: false,
                    data: null,
                    defaultContent: '',
                    render: function () {
                        return '<i class="fa fa-plus-square" aria-hidden="true"></i>';
                    },
                    width: "15px"
                },
                {
                    data: "transaccion_transID"
                },
                {
                    data: "transaccion_monto_con_comission",
                    render: function (data, type, row) {
                        return "$" + coffeetools.formatMoney(data, 2, '.', ',') + " dlls ";
                    }
                },
                {
                    data: "transaccion_concepto"
                },
                {

                    data: "status_descripcion"
                },
                {
                    data: "transaccion_fechaRegistro",
                    render: function (data, type, row) {
                        return coffeetools.ToJavaScriptDateTime24(data);
                    }
                },
                {
                    data: "ReprintReceipt",
                    render: function (data, type, row) {
                        return "<a href='serviciosdeimpresion?transid=" + data + "' class='lang' key='table.receipt'>RECIBO</a>";
                    }
                },
                {

                    data: "transaccion_transaccion",
                    visible: false
                },
                {

                    data: "transaccion_descripcion",
                    visible: false
                },
                {

                    data: "transaccion_referencia",
                    visible: false
                },
                {

                    data: "transaccion_tarjeta",
                    visible: false
                },
                {

                    data: "type_descripcion",
                    visible: false
                },
                {

                    data: "AmountForServiceMXN",
                    visible: false
                },
                {

                    data: "transaccion_comision",
                    visible: false
                },
                {

                    data: "transaccion_iva",
                    visible: false
                },
                {

                    data: "currency_codigo",
                    visible: false
                },
                {

                    data: "beneficiary",
                    visible: false
                },
                {
                    data: "buyer",
                    visible: false
                }
            ]
        });

        // Add event listener for opening and closing details
        $('#tbl_payment tbody').off('click', 'td.details-control');
        $('#tbl_payment tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var tdi = tr.find("i.fa");
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
                tdi.first().removeClass('fa-minus-square');
                tdi.first().addClass('fa-plus-square');
            }
            else {
                // Open this row
                row.child(coffeetools.loadDetailsForKioskoAdmin(row.data())).show();
                tr.addClass('shown');
                tdi.first().removeClass('fa-plus-square');
                tdi.first().addClass('fa-minus-square');

                ChangeDivLanguages();
            }
        });

        table.on("user-select", function (e, dt, type, cell, originalEvent) {
            if ($(cell.node()).hasClass("details-control")) {
                e.preventDefault();
            }
        });

        // Apply the search
        table.columns().every(function (key, value) {

            if (key != firstColumn && key != lastColumn) {
                var that = this;
                $('input', this.header()).on('keyup change', function () {
                    if (that.search() !== this.value) {
                        that
                            .search(this.value)
                            .draw();
                    }
                });
            }
        });

        var sum = table.column(1).data().eq(0).reduce(function (a, b) {
            return a + b;
        }, 0);

        ChangeDivLanguages();


    },
    loadDetailsForKioskoAdmin: function loadDetailsForKioskoAdmin(d) {
        // `d` is the original data object for the row
        var result = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +

            '<tr>' +
            '<td class="lang" key="table.movementtype">Tipo de Movimiento:</td>' +
            '<td>' + d.type_descripcion + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.foliotransaction">Folio transacci&oacute;n:</td>' +
            '<td>' + d.transaccion_transaccion + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.description">Descripci&oacute;n:</td>' +
            '<td>' + d.transaccion_descripcion + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.reference">Referencia:</td>' +
            '<td>' + d.transaccion_referencia + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.enpadicard">Tarjeta:</td>' +
            '<td>' + d.transaccion_tarjeta + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.totalforserviceusd">Servicio USD:</td>' +
            '<td>' + "$" + coffeetools.formatMoney(d.AmountForServiceUSD, 2, '.', ',') + " USD " + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.fee">Comisi&oacute;n:</td>' +
            '<td>' + "$" + coffeetools.formatMoney(d.transaccion_comision, 2, '.', ',') + " USD " + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.totalforserviceusd">Total:</td>' +
            '<td>' + "$" + coffeetools.formatMoney(d.TotalAmountUSD, 2, '.', ',') + " USD " + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.beneficiary">Beneficiario:</td>' +
            '<td>' + d.beneficiary + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.buyer">Comprador:</td>' +
            '<td>' + d.buyer + '</td>' +
            '</tr>' +
            '</table>';

        return result;
    },
    loadtableKioskoFront: function loadtableKioskoAdmin(result) {

        var path = window.location.protocol + "//" + window.location.host;

        var langFileUrl = "/lang/Spanish.json";

        if (sessionStorage.lang === "en") {
            langFileUrl = "/lang/English.json";
        }

        $('#date').text();
        $('#start_balance').text();
        $('#deposits').text();
        $('#charges').text();
        $('#final_balance').text();

        //config variables
        var firstColumn = 0;
        var lastColumn = 8;
        //console.log(result);

        //configure search boxes
        var table_search = $('#tbl_payment thead th').each(function (key, value) {
            var title = $(this).attr('key');
            var text = $(this).text();

            if (key != firstColumn && key != lastColumn) {
                //here!! finaly the answer!
                $(this).html('<label class="lang" key="' + title.trim() + '">' + text.trim() + '</label>' + '<input style="width: 85px;" type="text" />');
            }
        });

        //create table
        var table = $('#tbl_payment').DataTable({
            destroy: true,
            oLanguage: {
                "sUrl": langFileUrl
            },
            dom: 'Bfrtip',
            buttons: [{
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL', exportOptions: {
                    columns: [1, 2, 3, 4, 5, 7, 9, 11, 14, 16, 17, 18]
                },
                customize: function (doc) {
                    doc.styles['td:nth-child(2)'] = {
                        width: '100px',
                        'max-width': '100px'
                    }
                }
            },
            {
                extend: 'copy',
                text: '<div class="lang" key="general.copy"></div>',
            },
            {
                extend: 'csv',
                text: 'CSV'
            },
            {
                extend: 'excel',
                text: 'Excel'
            },
            {
                extend: 'print',
                text: '<div class="lang" key="general.print"></div>'
            },
            ],
            language: {
                buttons: {
                    copyTitle: 'Tabla copiada',
                    copySuccess: {
                        _: '%d lineas copiadas',
                        1: '1 linea copiada'
                    }
                }
            },
            "processing": true,
            data: result,

            columns: [
                {
                    className: 'details-control',
                    orderable: false,
                    data: null,
                    defaultContent: '',
                    render: function () {
                        return '<i class="fa fa-plus-square" aria-hidden="true"></i>';
                    },
                    width: "15px"
                },
                {
                    data: "transaccion_transID"
                },
                {
                    data: "transaccion_monto_con_comission",
                    render: function (data, type, row) {
                        return "$" + coffeetools.formatMoney(data, 2, '.', ',') + " dlls ";
                    }
                },
                {
                    data: "transaccion_concepto"
                },
                {

                    data: "status_descripcion"
                },
                {
                    data: "transaccion_fechaRegistro",
                    render: function (data, type, row) {
                        return coffeetools.ToJavaScriptDateTime24(data);
                    }
                },
                {
                    data: "ReprintReceipt",
                    render: function (data, type, row) {
                        return "<a href='serviciosdeimpresion?transid=" + data + "' class='lang' key='table.receipt'>RECIBO</a>";
                    }
                },
                {

                    data: "transaccion_transaccion",
                    visible: false
                },
                {

                    data: "transaccion_descripcion",
                    visible: false
                },
                {

                    data: "transaccion_referencia",
                    visible: false
                },
                {

                    data: "transaccion_tarjeta",
                    visible: false
                },
                {

                    data: "type_descripcion",
                    visible: false
                },
                {

                    data: "AmountForServiceMXN",
                    visible: false
                },
                {

                    data: "transaccion_comision",
                    visible: false
                },
                {

                    data: "transaccion_iva",
                    visible: false
                },
                {

                    data: "currency_codigo",
                    visible: false
                },
                {

                    data: "beneficiary",
                    visible: false
                },
                {
                    data: "buyer",
                    visible: false
                }
            ]
        });

        // Add event listener for opening and closing details
        $('#tbl_payment tbody').off('click', 'td.details-control');
        $('#tbl_payment tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var tdi = tr.find("i.fa");
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
                tdi.first().removeClass('fa-minus-square');
                tdi.first().addClass('fa-plus-square');
            }
            else {
                // Open this row
                row.child(coffeetools.loadDetailsForKioskoFront(row.data())).show();
                tr.addClass('shown');
                tdi.first().removeClass('fa-plus-square');
                tdi.first().addClass('fa-minus-square');

                ChangeDivLanguages();
            }
        });

        table.on("user-select", function (e, dt, type, cell, originalEvent) {
            if ($(cell.node()).hasClass("details-control")) {
                e.preventDefault();
            }
        });

        // Apply the search
        table.columns().every(function (key, value) {

            if (key != firstColumn && key != lastColumn) {
                var that = this;
                $('input', this.header()).on('keyup change', function () {
                    if (that.search() !== this.value) {
                        that
                            .search(this.value)
                            .draw();
                    }
                });
            }
        });

        var sum = table.column(1).data().eq(0).reduce(function (a, b) {
            return a + b;
        }, 0);

        ChangeDivLanguages();


    },
    loadDetailsForKioskoFront: function loadDetailsForKioskoAdmin(d) {

        var serviceString = "$" + coffeetools.formatMoney(d.AmountForServiceMXN, 2, '.', ',') + " MXN ";

        if (d.transaccion_socioID === 11) {
            serviceString = "$" + coffeetools.formatMoney(d.AmountForServiceUSD, 2, '.', ',') + " USD ";
        }
        else if (d.transaccion_socioID === 12) {
            serviceString = "$" + coffeetools.formatMoney(d.AmountForServiceUSD, 2, '.', ',') + " USD ";
        }

        // `d` is the original data object for the row
        var result = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +

            '<tr>' +
            '<td class="lang" key="table.movementtype">Tipo de Movimiento:</td>' +
            '<td>' + d.type_descripcion + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.foliotransaction">Folio transacci&oacute;n:</td>' +
            '<td>' + d.transaccion_transaccion + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.description">Descripci&oacute;n:</td>' +
            '<td>' + d.transaccion_descripcion + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.reference">Referencia:</td>' +
            '<td>' + d.transaccion_referencia + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.enpadicard">Tarjeta:</td>' +
            '<td>' + d.transaccion_tarjeta + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.totalforserviceusd">Servicio USD:</td>' +
            '<td>' + serviceString + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.fee">Comisi&oacute;n:</td>' +
            '<td>' + "$" + coffeetools.formatMoney(d.transaccion_comision, 2, '.', ',') + " USD " + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.totalforserviceusd">Servicio:</td>' +
            '<td>' + "$" + coffeetools.formatMoney(d.TotalAmountUSD, 2, '.', ',') + " USD " + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.beneficiary">Beneficiario:</td>' +
            '<td>' + d.beneficiary + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.buyer">Comprador:</td>' +
            '<td>' + d.buyer + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.buyer">Comprador:</td>' +
            '<td>' + "<input type='button' id='sendEmail' onclick='OpenSendEmailModalKiosko( \"" + d.GuidPagoServiciosId + "\");' value='Enviar por correo' key='email.btnsendemail' class='btn btn-primary lang-button' />" + '</td>' +
            '</tr>' +
            '</table>';

        return result;
    },
    loadtableBasicAmountWitoutFee: function loadtableBasicAmountWitoutFee(result) {

        var path = window.location.protocol + "//" + window.location.host;

        var langFileUrl = "/lang/Spanish.json";

        if (sessionStorage.lang === "en") {
            langFileUrl = "/lang/English.json";
        }

        $('#date').text();
        $('#start_balance').text();
        $('#deposits').text();
        $('#charges').text();
        $('#final_balance').text();

        //config variables
        var firstColumn = 0;
        var lastColumn = 8;
        //console.log(result);

        //configure search boxes
        var table_search = $('#tbl_payment thead th').each(function (key, value) {
            var title = $(this).attr('key');
            var text = $(this).text();

            if (key != firstColumn && key != lastColumn) {
                //here!! finaly the answer!
                $(this).html('<label class="lang" key="' + title.trim() + '">' + text.trim() + '</label>' + '<input style="width: 85px;" type="text" />');
            }
        });

        //create table
        var table = $('#tbl_payment').DataTable({
            destroy: true,
            oLanguage: {
                "sUrl": langFileUrl
            },
            dom: 'Bfrtip',
            buttons: [{
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL', exportOptions: {
                    columns: [1, 2, 3, 4, 5, 7, 9, 11, 14, 16, 17, 18]
                },
                customize: function (doc) {
                    doc.styles['td:nth-child(2)'] = {
                        width: '100px',
                        'max-width': '100px'
                    }
                }
            },
            {
                extend: 'copy',
                text: '<div class="lang" key="general.copy"></div>',
            },
            {
                extend: 'csv',
                text: 'CSV'
            },
            {
                extend: 'excel',
                text: 'Excel'
            },
            {
                extend: 'print',
                text: '<div class="lang" key="general.print"></div>'
            },
            ],
            language: {
                buttons: {
                    copyTitle: 'Tabla copiada',
                    copySuccess: {
                        _: '%d lineas copiadas',
                        1: '1 linea copiada'
                    }
                }
            },
            "processing": true,
            data: result,

            columns: [
                {
                    className: 'details-control',
                    orderable: false,
                    data: null,
                    defaultContent: '',
                    render: function () {
                        return '<i class="fa fa-plus-square" aria-hidden="true"></i>';
                    },
                    width: "15px"
                },
                {
                    data: "transaccion_transID"
                },
                {
                    data: "transaccion_monto_con_comission",
                    render: function (data, type, row) {
                        return "$" + coffeetools.formatMoney(data, 2, '.', ',') + " dlls ";
                    }
                },
                {
                    data: "transaccion_concepto"
                },
                {

                    data: "status_descripcion"
                },
                {
                    data: "transaccion_fechaRegistro",
                    render: function (data, type, row) {
                        return coffeetools.ToJavaScriptDateTime24(data);
                    }
                },
                {
                    data: "ReprintReceipt",
                    render: function (data, type, row) {
                        return "<a href='serviciosdeimpresion?transid=" + data + "' class='lang' key='table.receipt'>RECIBO</a>";
                    }
                },
                {

                    data: "transaccion_transaccion",
                    visible: false
                },
                {

                    data: "transaccion_descripcion",
                    visible: false
                },
                {

                    data: "transaccion_referencia",
                    visible: false
                },
                {

                    data: "transaccion_tarjeta",
                    visible: false
                },
                {

                    data: "type_descripcion",
                    visible: false
                },
                {

                    data: "AmountForServiceMXN",
                    visible: false
                },
                {

                    data: "transaccion_comision",
                    visible: false
                },
                {

                    data: "transaccion_iva",
                    visible: false
                },
                {

                    data: "currency_codigo",
                    visible: false
                },
                {

                    data: "beneficiary",
                    visible: false
                },
                {
                    data: "buyer",
                    visible: false
                }
            ]
        });

        // Add event listener for opening and closing details
        $('#tbl_payment tbody').off('click', 'td.details-control');
        $('#tbl_payment tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var tdi = tr.find("i.fa");
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
                tdi.first().removeClass('fa-minus-square');
                tdi.first().addClass('fa-plus-square');
            }
            else {
                // Open this row
                row.child(coffeetools.format(row.data())).show();
                tr.addClass('shown');
                tdi.first().removeClass('fa-plus-square');
                tdi.first().addClass('fa-minus-square');

                ChangeDivLanguages();
            }
        });

        table.on("user-select", function (e, dt, type, cell, originalEvent) {
            if ($(cell.node()).hasClass("details-control")) {
                e.preventDefault();
            }
        });

        // Apply the search
        table.columns().every(function (key, value) {

            if (key != firstColumn && key != lastColumn) {
                var that = this;
                $('input', this.header()).on('keyup change', function () {
                    if (that.search() !== this.value) {
                        that
                            .search(this.value)
                            .draw();
                    }
                });
            }
        });

        var sum = table.column(1).data().eq(0).reduce(function (a, b) {
            return a + b;
        }, 0);

        ChangeDivLanguages();


    },
    loadtablePendingOrders: function loadtablePendingOrders(result) {

        var path = window.location.protocol + "//" + window.location.host;

        var langFileUrl = "/lang/Spanish.json";

        if (sessionStorage.lang === "en") {
            langFileUrl = "/lang/English.json";
        }

        $('#date').text();
        $('#start_balance').text();
        $('#deposits').text();
        $('#charges').text();
        $('#final_balance').text();

        //config variables
        var firstColumn = 0;
        var lastColumn = 7;
        //console.log(result);

        //configure search boxes
        var table_search = $('#tbl_pending_orders thead th').each(function (key, value) {
            var title = $(this).attr('key');
            var text = $(this).text();

            if (key != firstColumn && key != lastColumn) {
                //here!! finaly the answer!
                $(this).html('<label class="lang" key="' + title.trim() + '">' + text.trim() + '</label>' + '<input style="width: 85px;" type="text" />');
            }
        });

        //create table
        var table = $('#tbl_pending_orders').DataTable({
            destroy: true,
            oLanguage: {
                "sUrl": langFileUrl
            },
            dom: 'Bfrtip',
            buttons: [{
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL', exportOptions: {
                    columns: [1, 2, 3, 4, 5, 7, 9, 11, 14, 16, 17, 18]
                },
                customize: function (doc) {
                    doc.styles['td:nth-child(2)'] = {
                        width: '100px',
                        'max-width': '100px'
                    }
                }
            },
            {
                extend: 'copy',
                text: '<div class="lang" key="general.copy"></div>',
            },
            {
                extend: 'csv',
                text: 'CSV'
            },
            {
                extend: 'excel',
                text: 'Excel'
            },
            {
                extend: 'print',
                text: '<div class="lang" key="general.print"></div>'
            },
            ],
            language: {
                buttons: {
                    copyTitle: 'Tabla copiada',
                    copySuccess: {
                        _: '%d lineas copiadas',
                        1: '1 linea copiada'
                    }
                }
            },
            "processing": true,
            data: result,

            columns: [
                {
                    className: 'details-control',
                    orderable: false,
                    data: null,
                    defaultContent: '',
                    render: function () {
                        return '<i class="fa fa-plus-square" aria-hidden="true"></i>';
                    },
                    width: "15px"
                },
                {
                    data: "id"
                },
                {
                    data: "amount",
                    render: function (data, type, row) {
                        return "$" + coffeetools.formatMoney(data, 2, '.', ',') + " dlls ";
                    }
                },
                {
                    data: "supplier"
                },
                {

                    data: "Status"
                },
                {
                    data: "receivedAt",
                    render: function (data, type, row) {
                        return coffeetools.ToJavaScriptDateTime24(data);
                    }
                },
                {
                    data: "client_email",
                    render: function (data, type, row) {
                        return "<a href='serviciosdeimpresion?transid=" + data + "' class='lang' key='general.pay'>Pagar</a>";
                    }
                }
            ]
        });

        // Add event listener for opening and closing details
        $('#tbl_pending_orders tbody').off('click', 'td.details-control');
        $('#tbl_pending_orders tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var tdi = tr.find("i.fa");
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
                tdi.first().removeClass('fa-minus-square');
                tdi.first().addClass('fa-plus-square');
            }
            else {
                // Open this row
                row.child(coffeetools.loadDetailsPendingOrder(row.data())).show();
                tr.addClass('shown');
                tdi.first().removeClass('fa-plus-square');
                tdi.first().addClass('fa-minus-square');

                ChangeDivLanguages();
            }
        });

        table.on("user-select", function (e, dt, type, cell, originalEvent) {
            if ($(cell.node()).hasClass("details-control")) {
                e.preventDefault();
            }
        });

        // Apply the search
        table.columns().every(function (key, value) {

            if (key != firstColumn && key != lastColumn) {
                var that = this;
                $('input', this.header()).on('keyup change', function () {
                    if (that.search() !== this.value) {
                        that
                            .search(this.value)
                            .draw();
                    }
                });
            }
        });

        ChangeDivLanguages();
    },
    loadDetailsPendingOrder: function loadDetailsForKioskoAdmin(d) {

        var serviceString = "$" + coffeetools.formatMoney(d.amount, 2, '.', ',') + " MXN ";

        // `d` is the original data object for the row
        var result = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +

            '<tr>' +
            '<td class="lang" key="table.movementtype">Tipo de Movimiento:</td>' +
            '<td>' + 'Charge' + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.description">Descripci&oacute;n:</td>' +
            '<td>' + d.external_description + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.fee">Comisi&oacute;n:</td>' +
            '<td>' + "$" + coffeetools.formatMoney(d.orderFee, 2, '.', ',') + " USD " + '</td>' +
            '</tr>' +

            '<tr>' +
            '<td class="lang" key="table.totalforserviceusd">Servicio:</td>' +
            '<td>' + "$" + coffeetools.formatMoney(d.amount, 2, '.', ',') + " USD " + '</td>' +
            '</tr>' +

            '</table>';

        return result;
    },
    changeMonthTextWithKey: function changeMonthTextWithKey(monthArray) {

        var result = [];

        if (sessionStorage.lang == 'en') {
            monthArray.forEach(function (element) {
                switch (element.mes.replace(/\d/g, '').replace(/\s/g, '')) {
                    case "ENERO":
                        result.push({ "numero": element.numero, "mes": element.mes.replace(/\bENERO\b/, "JANUARY") });
                        break;
                    case "FEBRERO":
                        result.push({ "numero": element.numero, "mes": element.mes.replace(/\bFEBRERO\b/, "FEBRUARY") });
                        break;
                    case "MARZO":
                        result.push({ "numero": element.numero, "mes": element.mes.replace(/\bMARZO\b/, "MARCH") });
                        break;
                    case "ABRIL":
                        result.push({ "numero": element.numero, "mes": element.mes.replace(/\bABRIL\b/, "APRIL") });
                        break;
                    case "MAYO":
                        result.push({ "numero": element.numero, "mes": element.mes.replace(/\bMAYO\b/, "MAY") });
                        break;
                    case "JUNIO":
                        result.push({ "numero": element.numero, "mes": element.mes.replace(/\bJUNIO\b/, "JUNE") });
                        break;
                    case "JULIO":
                        result.push({ "numero": element.numero, "mes": element.mes.replace(/\bJULIO\b/, "JULY") });
                        break;
                    case "AGOSTO":
                        result.push({ "numero": element.numero, "mes": element.mes.replace(/\bAGOSTO\b/, "AUGUST") });
                        break;
                    case "SEPTIEMBRE":
                        result.push({ "numero": element.numero, "mes": element.mes.replace(/\bSEPTIEMBRE\b/, "SEPTEMBER") });
                        break;
                    case "OCTUBRE":
                        result.push({ "numero": element.numero, "mes": element.mes.replace(/\bOCTUBRE\b/, "OCTOBER") });
                        break;
                    case "NOVIEMBRE":
                        result.push({ "numero": element.numero, "mes": element.mes.replace(/\bNOVIEMBRE\b/, "NOVEMBER") });
                        break;
                    case "DICIEMBRE":
                        result.push({ "numero": element.numero, "mes": element.mes.replace(/\bDICIEMBRE\b/, "DECEMBER") });
                        break;
                }
            });
        } else {
            result = monthArray;
        }

        return result;
    },
    changeMonthFullText: function changeMonthFullText(monthFullText) {

        var result = null;

        if (sessionStorage.lang == 'en') {

            switch (monthFullText.replace(/\d/g, '').replace(/\s/g, '')) {
                case "ENERO":
                    result = monthFullText.replace(/\bENERO\b/, "JANUARY");
                    break;
                case "FEBRERO":
                    result = monthFullText.replace(/\bFEBRERO\b/, "FEBRUARY");
                    break;
                case "MARZO":
                    result = monthFullText.replace(/\bMARZO\b/, "MARCH");
                    break;
                case "ABRIL":
                    result = monthFullText.replace(/\bABRIL\b/, "APRIL");
                    break;
                case "MAYO":
                    result = monthFullText.replace(/\bMAYO\b/, "MAY");
                    break;
                case "JUNIO":
                    result = monthFullText.replace(/\bJUNIO\b/, "JUNE");
                    break;
                case "JULIO":
                    result = monthFullText.replace(/\bJULIO\b/, "JULY");
                    break;
                case "AGOSTO":
                    result = monthFullText.replace(/\bAGOSTO\b/, "AUGUST");
                    break;
                case "SEPTIEMBRE":
                    result = monthFullText.replace(/\bSEPTIEMBRE\b/, "SEPTEMBER");
                    break;
                case "OCTUBRE":
                    result = monthFullText.replace(/\bOCTUBRE\b/, "OCTOBER");
                    break;
                case "NOVIEMBRE":
                    result = monthFullText.replace(/\bNOVIEMBRE\b/, "NOVEMBER");
                    break;
                case "DICIEMBRE":
                    result = monthFullText.replace(/\bDICIEMBRE\b/, "DECEMBER");
                    break;
            }

        } else {
            result = monthFullText;
        }

        return result;
    },
    createPointForGraph: function createPointsForGraph(xPoints, yPoints, aditionalInfo) {

        var result = [];

        var pointsObject = {
            y: yPoints,
            x: xPoints,
            info: aditionalInfo
        };

        for (var i = 0, len = pointsObject.x.length; i < len; i++) {
            var info = "";
            if (aditionalInfo != undefined) {
                if (aditionalInfo[i] != undefined) {
                    info = aditionalInfo[i];
                }
            }
            result.push(
                {
                    x: pointsObject.x[i],
                    y: pointsObject.y[i],
                    info: info
                })

                ;
        }

        return result;
    },
    dalilyAmountsByMethodChart: function dalilyAmountsByMethodChart(
        chartJsCanvas,
        lang,
        showStores,
        rsmVisaMC,
        rsmAmex,
        rsmBanco,
        rsmTienda,
        rsmTiempoAire,
        rsmServicios,
        rsmTransferencias
    ) {

        var graphResult;

        var strbanco = "Bancos";
        var strtienda = "Tiendas";
        var strairtime = "Tiempo Aire";
        var strservicepayment = "Pago de Servicios";
        var strtransfer = "Transferencias";
        var strservice = "Servicios";

        if (lang == 'en') {
            strbanco = "Banks";
            strtienda = "Shops";
            strairtime = "Tops Ups";
            strservicepayment = "Bill Payments";
            strtransfer = "Transfers";
            strservice = "Services";
        }

        var resumenData = {};

        if (showStores === true) {
            resumenData = {
                datasets: [
                    {
                        type: 'scatter',
                        label: 'Visa/MasterCard',
                        backgroundColor: 'rgba(255,17,5,0.5)',
                        data: rsmVisaMC,
                        borderColor: 'rgba(255,17,5,0.5)',
                        borderWidth: 2
                    }, {
                        type: 'scatter',
                        label: 'American Express',
                        backgroundColor: 'rgba(15,161,220,0.5)',
                        data: rsmAmex,
                        borderColor: 'rgba(15,161,220,0.7)',
                        borderWidth: 2
                    }, {
                        type: 'scatter',
                        label: strbanco,
                        backgroundColor: 'rgba(82,37,153,0.5)',
                        data: rsmBanco,
                        borderColor: 'rgba(82,37,153,0.5)',
                        borderWidth: 2
                    }, {
                        type: 'scatter',
                        label: strtienda,
                        backgroundColor: 'rgba(0,128,97,0.5)',
                        data: rsmTienda,
                        borderColor: 'rgba(0,128,97,0.7)',
                        borderWidth: 2
                    },
                    {
                        type: 'scatter',
                        label: strairtime,
                        backgroundColor: 'rgba(255,224,2,0.5)',
                        data: rsmTiempoAire,
                        borderColor: 'rgba(255,224,2,0.7)',
                        borderWidth: 2
                    }, {
                        type: 'scatter',
                        label: strservicepayment,
                        backgroundColor: 'rgba(68,65,42,0.5)',
                        data: rsmServicios,
                        borderColor: 'rgba(68,65,42,0.7)',
                        borderWidth: 2
                    }, {
                        type: 'scatter',
                        label: strtransfer,
                        backgroundColor: 'rgba(250,118,255,0.5)',
                        data: rsmTransferencias,
                        borderColor: 'rgba(250,118,255,0.7)',
                        borderWidth: 2
                    }]
            };
        }
        else {
            resumenData = {
                datasets: [
                    {
                        type: 'scatter',
                        label: 'Visa/MasterCard',
                        backgroundColor: 'rgba(255,17,5,0.5)',
                        data: rsmVisaMC,
                        borderColor: 'rgba(255,17,5,0.5)',
                        borderWidth: 2
                    }, {
                        type: 'scatter',
                        label: 'American Express',
                        backgroundColor: 'rgba(15,161,220,0.5)',
                        data: rsmAmex,
                        borderColor: 'rgba(15,161,220,0.7)',
                        borderWidth: 2
                    }, {
                        type: 'scatter',
                        label: strbanco,
                        backgroundColor: 'rgba(82,37,153,0.5)',
                        data: rsmBanco,
                        borderColor: 'rgba(82,37,153,0.5)',
                        borderWidth: 2
                    },
                    {
                        type: 'scatter',
                        label: strairtime,
                        backgroundColor: 'rgba(255,224,2,0.5)',
                        data: rsmTiempoAire,
                        borderColor: 'rgba(255,224,2,0.7)',
                        borderWidth: 2
                    }, {
                        type: 'scatter',
                        label: strservicepayment,
                        backgroundColor: 'rgba(68,65,42,0.5)',
                        data: rsmServicios,
                        borderColor: 'rgba(68,65,42,0.7)',
                        borderWidth: 2
                    }, {
                        type: 'scatter',
                        label: strtransfer,
                        backgroundColor: 'rgba(250,118,255,0.5)',
                        data: rsmTransferencias,
                        borderColor: 'rgba(250,118,255,0.7)',
                        borderWidth: 2
                    }]
            };
        }

        graphResult = new Chart(chartJsCanvas, {
            type: 'scatter',
            data: resumenData,
            options: {
                legend: {
                    display: true,
                },
                tooltips: {
                    enabled: true,
                    callbacks: {
                        label: function (tooltipItem, data) {

                            var tooltip = [];
                            var i = tooltipItem.datasetIndex;
                            if (data.datasets[i] !== undefined) {
                                var total = 0;
                                var infoLabel = data.datasets[i].label
                                if (data.datasets[i].data[tooltipItem.index].info !== "") {
                                    infoLabel = data.datasets[i].data[tooltipItem.index].info;
                                }
                                total = data.datasets[i].data[tooltipItem.index].y
                                tooltip.push(infoLabel + ":" + total);
                            }
                            return tooltip;
                        }
                    }
                },
                responsive: true,
                title: {
                    display: false,
                    text: " "
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            callback: function (value, index, values) {
                                if (value > -1 && value < 1)
                                    return null;
                                value = value.toString();
                                value = value.split(/(?=(?:...)*$)/);
                                value = value.join(',');
                                return '$' + value;
                            }
                        }
                    }],
                    xAxes: [{
                        stacked: false,
                        beginAtZero: true,
                        scaleLabel: {
                            labelString: 'Month'
                        },
                        ticks: {
                            stepSize: 1,
                            min: 0,
                            autoSkip: false
                        }
                    }]
                },
                animation: {
                    onComplete: function () {
                        //var chartInstance = this.chart;
                        //var ctx = chartInstance.chartJsCanvas;

                        //ctx.textAlign = "center";

                        //Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {

                        //    var meta = chartInstance.controller.getDatasetMeta(i);

                        //    Chart.helpers.each(meta.data.forEach(function (bar, index) {
                        //        //in here you can print every point with informarion
                        //        //if (i == 0)
                        //        //    ctx.fillText(dataset.data[index], bar._model.x, bar._model.y - 15);
                        //        //if (i == 2)
                        //        //    ctx.fillText(dataset.data[index], bar._model.x, bar._model.y + 15);
                        //    }), this)
                        //}), this);
                    }
                }
            }
        });

        return graphResult;
    },
    dalilyOfTransactionsChart: function dalilyOfTransactionsChart(
        chartJsCanvas,
        lang,
        showStores,
        rsmTransVisaMC,
        rsmTransAmex,
        rsmTransBanco,
        rsmTransTienda,
        rsmTransTiempoAire,
        rsmTransServicios,
        rsmTransTransfer
    ) {

        var graphResult;

        var strbanco = "Bancos";
        var strtienda = "Tiendas";
        var strairtime = "Tiempo Aire";
        var strservicepayment = "Pago de Servicios";
        var strtransfer = "Transferencias";
        var strservice = "Servicios";

        if (lang == 'en') {
            strbanco = "Banks";
            strtienda = "Shops";
            strairtime = "Tops Ups";
            strservicepayment = "Bill Payments";
            strtransfer = "Transfers";
            strservice = "Services";
        }

        var resumenData = {};

        if (showStores === true) {
            resumenData = {
                datasets: [
                    {
                        type: 'scatter',
                        label: 'Visa/MasterCard',
                        backgroundColor: 'rgba(255,17,5,0.5)',
                        data: rsmTransVisaMC,
                        borderColor: 'rgba(255,17,5,0.5)',
                        borderWidth: 2
                    }, {
                        type: 'scatter',
                        label: 'American Express',
                        backgroundColor: 'rgba(15,161,220,0.5)',
                        data: rsmTransAmex,
                        borderColor: 'rgba(15,161,220,0.7)',
                        borderWidth: 2
                    }, {
                        type: 'scatter',
                        label: strbanco,
                        backgroundColor: 'rgba(82,37,153,0.5)',
                        data: rsmTransBanco,
                        borderColor: 'rgba(82,37,153,0.5)',
                        borderWidth: 2
                    }, {
                        type: 'scatter',
                        label: strtienda,
                        backgroundColor: 'rgba(0,128,97,0.5)',
                        data: rsmTransTienda,
                        borderColor: 'rgba(0,128,97,0.7)',
                        borderWidth: 2
                    },
                    {
                        type: 'scatter',
                        label: strairtime,
                        backgroundColor: 'rgba(255,224,2,0.5)',
                        data: rsmTransTiempoAire,
                        borderColor: 'rgba(255,224,2,0.7)',
                        borderWidth: 2
                    }, {
                        type: 'scatter',
                        label: strservicepayment,
                        backgroundColor: 'rgba(68,65,42,0.5)',
                        data: rsmTransServicios,
                        borderColor: 'rgba(68,65,42,0.7)',
                        borderWidth: 2
                    }, {
                        type: 'scatter',
                        label: strtransfer,
                        backgroundColor: 'rgba(250,118,255,0.5)',
                        data: rsmTransTransfer,
                        borderColor: 'rgba(250,118,255,0.7)',
                        borderWidth: 2
                    }]
            };
        }
        else {
            resumenData = {
                datasets: [
                    {
                        type: 'scatter',
                        label: 'Visa/MasterCard',
                        backgroundColor: 'rgba(255,17,5,0.5)',
                        data: rsmTransVisaMC,
                        borderColor: 'rgba(255,17,5,0.5)',
                        borderWidth: 2
                    }, {
                        type: 'scatter',
                        label: 'American Express',
                        backgroundColor: 'rgba(15,161,220,0.5)',
                        data: rsmTransAmex,
                        borderColor: 'rgba(15,161,220,0.7)',
                        borderWidth: 2
                    }, {
                        type: 'scatter',
                        label: strbanco,
                        backgroundColor: 'rgba(82,37,153,0.5)',
                        data: rsmTransBanco,
                        borderColor: 'rgba(82,37,153,0.5)',
                        borderWidth: 2
                    },
                    {
                        type: 'scatter',
                        label: strairtime,
                        backgroundColor: 'rgba(255,224,2,0.5)',
                        data: rsmTransTiempoAire,
                        borderColor: 'rgba(255,224,2,0.7)',
                        borderWidth: 2
                    }, {
                        type: 'scatter',
                        label: strservicepayment,
                        backgroundColor: 'rgba(68,65,42,0.5)',
                        data: rsmTransServicios,
                        borderColor: 'rgba(68,65,42,0.7)',
                        borderWidth: 2
                    }, {
                        type: 'scatter',
                        label: strtransfer,
                        backgroundColor: 'rgba(250,118,255,0.5)',
                        data: rsmTransTransfer,
                        borderColor: 'rgba(250,118,255,0.7)',
                        borderWidth: 2
                    }]
            };
        }

        graphResult = new Chart(chartJsCanvas, {
            type: 'scatter',
            data: resumenData,
            options: {
                legend: {
                    display: true,
                },
                tooltips: {
                    enabled: true,
                    callbacks: {
                        label: function (tooltipItem, data) {
                            var tooltip = [];
                            var i = tooltipItem.datasetIndex;
                            if (data.datasets[i] !== undefined) {
                                var total = 0;
                                var infoLabel = data.datasets[i].label
                                if (data.datasets[i].data[tooltipItem.index].info !== "") {
                                    infoLabel = data.datasets[i].data[tooltipItem.index].info;
                                }
                                total = data.datasets[i].data[tooltipItem.index].y
                                tooltip.push(infoLabel + ":" + total);
                            }
                            return tooltip;
                        }
                    }
                },
                responsive: true,
                title: {
                    display: false,
                    text: " "
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            stepSize: 1,
                            min: 0,
                            autoSkip: false
                        }
                    }],
                    xAxes: [{
                        stacked: false,
                        beginAtZero: true,
                        scaleLabel: {
                            labelString: 'Month'
                        },
                        ticks: {
                            stepSize: 1,
                            beginAtZero: true,
                            autoSkip: false
                        }
                    }]
                },
                animation: {
                    onComplete: function () {
                        //var chartInstance = this.chart;
                        //var ctx = chartInstance.chartJsCanvas;

                        //ctx.textAlign = "center";

                        //Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {

                        //    var meta = chartInstance.controller.getDatasetMeta(i);

                        //    Chart.helpers.each(meta.data.forEach(function (bar, index) {
                        //        //in here you can print every point with informarion
                        //        //if (i == 0)
                        //        //    ctx.fillText(dataset.data[index], bar._model.x, bar._model.y - 15);
                        //        //if (i == 2)
                        //        //    ctx.fillText(dataset.data[index], bar._model.x, bar._model.y + 15);
                        //    }), this)
                        //}), this);
                    }
                }
            }
        });

        return graphResult;
    },
    topsUpsChart: function topsUpsChart(
        chartJsCanvas,
        lang,
        taeLabel,
        taeTelcel,
        taeMovistar,
        taeIusacell,
        taeUnefon,
        taeNextel,
        taeVirgin
    ) {
        var graphResult;

        var strbanco = "Bancos";
        var strtienda = "Tiendas";
        var strairtime = "Tiempo Aire";
        var strservicepayment = "Pago de Servicios";
        var strtransfer = "Transferencias";
        var strservice = "Servicios";

        if (lang == 'en') {
            strbanco = "Banks";
            strtienda = "Shops";
            strairtime = "Tops Ups";
            strservicepayment = "Bill Payments";
            strtransfer = "Transfers";
            strservice = "Services";
        }

        taeLabel = coffeetools.changeMonthLanguage(taeLabel);

        var taeData = {
            labels: taeLabel,
            datasets: [{
                label: 'Telcel',
                backgroundColor: 'rgba(0,37,151,0.7)',
                data: taeTelcel
            }, {
                label: 'Movistar',
                backgroundColor: 'rgba(135,196,56,0.5)',
                data: taeMovistar
            }, {
                label: 'Iusacell',
                backgroundColor: 'rgba(46,159,215,0.5)',
                data: taeIusacell
            }, {
                label: 'Unefon',
                backgroundColor: 'rgba(255,228,9,0.5)',
                data: taeUnefon
            }, {
                label: 'Nextel',
                backgroundColor: 'rgba(255,81,0,0.5)',
                data: taeNextel
            }, {
                label: 'Virgin',
                backgroundColor: 'rgba(204,0,1,0.5)',
                data: taeVirgin
            }]
        }

        graphResult = new Chart(chartJsCanvas, {
            type: 'bar',
            data: taeData,
            options: {
                title: {
                    display: true,
                    text: strairtime
                },
                legend: {
                    display: true,
                },
                tooltips: {
                    enabled: true,
                    callbacks: {
                        label: function (tooltipItem, data) {

                            var tooltip = [];
                            var i = tooltipItem.datasetIndex;
                            if (data.datasets[i] !== undefined) {
                                var total = 0;
                                total = data.datasets[i].data[tooltipItem.index]
                                tooltip.push(data.datasets[i].label + ":" + total);
                            }
                            return tooltip;
                        }
                    }
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            beginAtZero: true,
                            autoSkip: false,
                            callback: function (value, index, values) {
                                value = value.toString();
                                value = value.split(/(?=(?:...)*$)/);
                                value = value.join(',');
                                return '$' + value;
                            }
                        }
                    }]
                },
            }
        });

        return graphResult;
    },
    billPaymentsChart: function billPaymentsChart(
        chartJsCanvas,
        lang,
        serviciosLabel,
        serviciosData
    ) {
        var graphResult;

        var strbanco = "Bancos";
        var strtienda = "Tiendas";
        var strairtime = "Tiempo Aire";
        var strservicepayment = "Pago de Servicios";
        var strtransfer = "Transferencias";
        var strservice = "Servicios";

        if (lang == 'en') {
            strbanco = "Banks";
            strtienda = "Shops";
            strairtime = "Tops Ups";
            strservicepayment = "Bill Payments";
            strtransfer = "Transfers";
            strservice = "Services";
        }

        serviciosLabel = coffeetools.changeMonthLanguage(serviciosLabel);

        var serviciosData = {
            labels: serviciosLabel,
            datasets: [{
                label: strservicepayment,
                backgroundColor: 'rgba(151,187,205,0.5)',
                data: serviciosData
            }]
        }

        graphResult = new Chart(chartJsCanvas, {
            type: 'bar',
            data: serviciosData,
            options: {
                title: {
                    display: true,
                    text: strservicepayment
                },
                legend: {
                    display: true,
                },
                tooltips: {
                    enabled: true,
                    callbacks: {
                        label: function (tooltipItem, data) {
                            var tooltip = [];
                            var i = tooltipItem.datasetIndex;
                            if (data.datasets[i] !== undefined) {
                                var total = 0;
                                total = data.datasets[i].data[tooltipItem.index]
                                tooltip.push(data.datasets[i].label + ":" + total);
                            }

                            return tooltip;
                        }
                    }
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true
                    }],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            autoSkip: false,
                            callback: function (value, index, values) {
                                value = value.toString();
                                value = value.split(/(?=(?:...)*$)/);
                                value = value.join(',');
                                return '$' + value;
                            }
                        }
                    }]
                }
            }
        });

        return graphResult;
    },
    topUpsTransactionsChart: function topUpsTransactionsChart(
        chartJsCanvas,
        taeTransLabel,
        taeTransTelcel,
        taeTransMovistar,
        taeTransIusacell,
        taeTransUnefon,
        taeTransNextel,
        taeTransVirgin
    ) {

        taeTransLabel = coffeetools.changeMonthLanguage(taeTransLabel);

        var graphResult = new Chart(chartJsCanvas, {
            type: 'line',
            data: {
                labels: taeTransLabel,
                datasets: [{
                    label: 'Telcel',
                    data: taeTransTelcel,
                    fill: false,
                    borderColor: 'rgba(0,37,151,0.7)',
                    borderDash: [1, 1]
                }, {
                    label: 'Movistar',
                    backgroundColor: 'rgba(135,196,56,0.5)',
                    fill: false,
                    data: taeTransMovistar
                }, {
                    label: 'Iusacell',
                    backgroundColor: 'rgba(46,159,215,0.5)',
                    fill: false,
                    data: taeTransIusacell
                }, {
                    label: 'Unefon',
                    backgroundColor: 'rgba(255,228,9,0.5)',
                    fill: false,
                    data: taeTransUnefon
                }, {
                    label: 'Nextel',
                    backgroundColor: 'rgba(255,81,0,0.5)',
                    fill: false,
                    data: taeTransNextel
                }, {
                    label: 'Virgin',
                    backgroundColor: 'rgba(204,0,1,0.5)',
                    fill: false,
                    data: taeTransVirgin
                }]
            },
            options: {
                responsive: true,
                title: {
                    display: false,
                    text: 'Transacciones de Tiempo Aire'
                },
                legend: {
                    display: true,
                },
                tooltips: {
                    enabled: true,
                    callbacks: {
                        label: function (tooltipItem, data) {

                            var tooltip = [];
                            var i = tooltipItem.datasetIndex;
                            if (data.datasets[i] !== undefined) {
                                var total = 0;
                                total = data.datasets[i].data[tooltipItem.index]
                                tooltip.push(data.datasets[i].label + ":" + total);
                            }
                            //console.log("tooltip");
                            //console.log(tooltipItem);
                            //console.log("----");
                            //console.log(data);
                            return tooltip;
                        }
                    }
                },
                hover: {
                    mode: 'dataset'
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            show: true,
                            labelString: 'Mes'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            show: true,
                            labelString: 'Valor'
                        },
                        ticks: {
                            beginAtZero: true,
                            autoSkip: false
                        }
                    }]
                },
                animation: {
                    onComplete: function () {
                        //var chartInstance = this.chart;
                        //var chartJsCanvas = chartInstance.chartJsCanvas;

                        //chartJsCanvas.textAlign = "center";

                        //Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
                        //    var meta = chartInstance.controller.getDatasetMeta(i);
                        //    Chart.helpers.each(meta.data.forEach(function (bar, index) {
                        //        chartJsCanvas.fillText(dataset.data[index], bar._model.x, bar._model.y - 25);
                        //    }), this)
                        //}), this);
                    }
                }
            }
        }
        );

        return graphResult;
    },
    billPaymentsTransactionChart: function billPaymentsTransactionChart(
        chartJsCanvas,
        lang,
        serviciosTransLabel,
        serviciosTransData
    ) {
        var strbanco = "Bancos";
        var strtienda = "Tiendas";
        var strairtime = "Tiempo Aire";
        var strservicepayment = "Pago de Servicios";
        var strtransfer = "Transferencias";
        var strservice = "Servicios";

        if (lang == 'en') {
            strbanco = "Banks";
            strtienda = "Shops";
            strairtime = "Tops Ups";
            strservicepayment = "Bill Payments";
            strtransfer = "Transfers";
            strservice = "Services";
        }

        serviciosTransLabel = coffeetools.changeMonthLanguage(serviciosTransLabel);

        var graphResult = new Chart(chartJsCanvas, {
            type: 'line',
            data: {
                labels: serviciosTransLabel,
                datasets: [{
                    label: strservice,
                    data: serviciosTransData,
                    fill: false,
                    borderColor: 'rgba(0, 37, 151, 0.7)',
                    borderDash: [1, 1]
                }]
            },
            options: {
                responsive: true,
                title: {
                    display: false,
                    text: 'Transacciones de Tiempo Aire'
                },
                legend: {
                    display: true,
                },
                tooltips: {
                    enabled: true,
                    callbacks: {
                        label: function (tooltipItem, data) {

                            var tooltip = [];
                            var i = tooltipItem.datasetIndex;
                            if (data.datasets[i] !== undefined) {
                                var total = 0;
                                total = data.datasets[i].data[tooltipItem.index]
                                tooltip.push(data.datasets[i].label + ":" + total);
                            }
                            //console.log("tooltip");
                            //console.log(tooltipItem);
                            //console.log("----");
                            //console.log(data);
                            return tooltip;
                        }
                    }
                },
                hover: {
                    mode: 'datasets'
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            show: true,
                            labelString: 'Mes'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            show: true,
                            labelString: 'Valor'
                        },
                        ticks: {

                            beginAtZero: true,
                            autoSkip: false
                        }
                    }]
                },
                animation: {
                    onComplete: function () {
                        //var chartInstance = this.chart;
                        //var chartJsCanvas = chartInstance.chartJsCanvas;

                        //chartJsCanvas.textAlign = "center";

                        //Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
                        //    var meta = chartInstance.controller.getDatasetMeta(i);
                        //    Chart.helpers.each(meta.data.forEach(function (bar, index) {
                        //        chartJsCanvas.fillText(dataset.data[index], bar._model.x, bar._model.y - 25);
                        //    }), this)
                        //}), this);
                    }
                }
            }
        });

        return graphResult;
    },

}