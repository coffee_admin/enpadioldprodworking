﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Enpadi_WebUI.Models;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Enpadi_WebUI.DAO
{
    public class AspNetUsers
    {
        #region variables

        #region for saving asp user

        private static ApplicationSignInManager _signInManager;
        private static ApplicationUserManager _userManager;
        private static ApplicationDbContext db = new ApplicationDbContext();

        #endregion for saving asp user

        #region sp name

        private const String SaveStoreProcedureName = "AspNetUsersInsert";
        private const String GetStoreProcedureName = "AspNetUsersGet";

        #endregion sp name

        #region parameters

        private const String Param_Id = "@Id";
        private const String Param_Email = "@Email";
        private const String Param_EmailConfirmed = "@EmailConfirmed";
        private const String Param_PasswordHash = "@PasswordHash";

        private const String Param_SecurityStamp = "@SecurityStamp";
        private const String Param_PhoneNumber = "@PhoneNumber";
        private const String Param_PhoneNumberConfirmed = "@PhoneNumberConfirmed";
        private const String Param_TwoFactorEnabled = "@TwoFactorEnabled";

        private const String Param_LockoutEndDateUtc = "@LockoutEndDateUtc";
        private const String Param_LockoutEnabled = "@LockoutEnabled";
        private const String Param_AccessFailedCount = "@AccessFailedCount";
        private const String Param_UserName = "@UserName";

        private const String Param_Name = "@Name";
        private const String Param_ProfilePicUrl = "@ProfilePicUrl";

        #endregion parameters

        #region members

        private const String Member_Id = "Id";
        private const String Member_Email = "Email";
        private const String Member_EmailConfirmed = "EmailConfirmed";
        private const String Member_PasswordHash = "PasswordHash";

        private const String Member_SecurityStamp = "SecurityStamp";
        private const String Member_PhoneNumber = "PhoneNumber";
        private const String Member_PhoneNumberConfirmed = "PhoneNumberConfirmed";
        private const String Member_TwoFactorEnabled = "TwoFactorEnabled";

        private const String Member_LockoutEndDateUtc = "LockoutEndDateUtc";
        private const String Member_LockoutEnabled = "LockoutEnabled";
        private const String Member_AccessFailedCount = "AccessFailedCount";
        private const String Member_UserName = "UserName";

        private const String Member_Name = "Name";
        private const String Member_ProfilePicUrl = "ProfilePicUrl";

        #endregion members

        #endregion variables

        #region methods

        #region save
        /// <summary>
        /// DO NOT USE THIS UNLESS YOU KNOW WHAT YOU ARE DOING, USE SaveApplicationUser INSTED
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="Email"></param>
        /// <param name="EmailConfirmed"></param>
        /// <param name="PasswordHash"></param>
        /// <param name="SecurityStamp"></param>
        /// <param name="PhoneNumber"></param>
        /// <param name="PhoneNumberConfirmed"></param>
        /// <param name="TwoFactorEnabled"></param>
        /// <param name="LockoutEndDateUtc"></param>
        /// <param name="LockoutEnabled"></param>
        /// <param name="AccessFailedCount"></param>
        /// <param name="UserName"></param>
        /// <param name="Name"></param>
        public static void Save(
            String Id,
            String Email,
            Boolean EmailConfirmed,
            String PasswordHash,

            String SecurityStamp,
            String PhoneNumber,
            Boolean PhoneNumberConfirmed,
            Boolean TwoFactorEnabled,

            DateTime LockoutEndDateUtc,
            Boolean LockoutEnabled,
            Int32 AccessFailedCount,
            String UserName,

            String Name,
            String ProfilePicUrl
        )
        {
            SqlParameter[] parameters = {
                new SqlParameter(Param_Id, Id),
                new SqlParameter(Param_Email, Email),
                new SqlParameter(Param_EmailConfirmed, EmailConfirmed),
                //new SqlParameter(Param_PasswordHash, PasswordHash),

                //new SqlParameter(Param_SecurityStamp,SecurityStamp),
                new SqlParameter(Param_PhoneNumber,PhoneNumber),
                new SqlParameter(Param_PhoneNumberConfirmed,PhoneNumberConfirmed),
                //new SqlParameter(Param_TwoFactorEnabled,TwoFactorEnabled),

                //new SqlParameter(Param_LockoutEndDateUtc,LockoutEndDateUtc),
                //new SqlParameter(Param_LockoutEnabled,LockoutEnabled),
                //new SqlParameter(Param_AccessFailedCount,AccessFailedCount),
                new SqlParameter(Param_UserName,UserName),

                new SqlParameter(Param_Name,Name),
                new SqlParameter(Param_ProfilePicUrl,ProfilePicUrl)
            };

            DataBaseConnector.ExecuteStoreProcedure(SaveStoreProcedureName, parameters);
        }

        /// <summary>
        /// DO NOT USE THIS UNLESS YOU KNOW WHAT YOU ARE DOING, USE SaveApplicationUser INSTED
        /// </summary>
        /// <param name="item"></param>
        public static void Save(
            AspNetUsersModel item
        )
        {
            Save(
                item.Id,
                item.Email,
                item.EmailConfirmed,
                item.PasswordHash,

                item.SecurityStamp,
                item.PhoneNumber,
                item.PhoneNumberConfirmed,
                item.TwoFactorEnabled,

                item.LockoutEndDateUtc,
                item.LockoutEnabled,
                item.AccessFailedCount,
                item.UserName,

                item.Name,
                item.ProfilePicUrl
           );
        }

        public static UserManager<ApplicationUser> UserManager { get; private set; }
        /// <summary>
        /// Use this to create a user on ENPADI
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static async Task<ApplicationUser> SaveApplicationUser(AspNetUsersModel model)
        {
            var user = new ApplicationUser
            {
                UserName = model.Email,
                Email = model.Email,
                Name = model.Name,
                PhoneNumber = model.PhoneNumber,
                TwoFactorEnabled = false,
                EmailConfirmed = false
            };

            var result = await UserManager.CreateAsync(user, model.Password);

            var context = new ApplicationDbContext();
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            if (result.Succeeded)
            {
                userManager.AddToRole(user.Id, SecurityRoles.Usuario);
                tarjetas tc = db.tarjetas.Where(t => t.tcAsignada == false).FirstOrDefault();
                tc.tcAsignada = true;
                tc.datNombre = model.Name;
                tc.email = model.Email;
                tc.password = user.Id;
                db.Entry(tc).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }

            return user;
        }

        #endregion save

        #region get

        public static AspNetUsersModel Get(Guid? Id, String email)
        {
            DataTable result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_Id, Id),
                new SqlParameter(Param_Email, (email == String.Empty? null: email))

            };

            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName, parameters);

            return PorcessResult(result).FirstOrDefault();
        }

        public static List<AspNetUsersModel> GetAll()
        {
            DataTable result = new DataTable();
            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName);

            return PorcessResult(result);
        }

        #endregion get

        #region datatable to model

        private static List<AspNetUsersModel> PorcessResult(DataTable items)
        {

            List<AspNetUsersModel> result_items = new List<AspNetUsersModel>();

            foreach (DataRow row in items.Rows)
            {
                result_items.Add(new AspNetUsersModel
                {
                    Id = ToolsBO.ProcessResultString(Member_Id,row),
                    Email = ToolsBO.ProcessResultString(Member_Email, row),
                    EmailConfirmed = ToolsBO.ProcessResultBoolean(Member_EmailConfirmed, row),
                    PasswordHash = ToolsBO.ProcessResultString(Member_PasswordHash, row),

                    SecurityStamp = ToolsBO.ProcessResultString(Member_SecurityStamp, row),
                    PhoneNumber = ToolsBO.ProcessResultString(Member_PhoneNumber, row),
                    PhoneNumberConfirmed = ToolsBO.ProcessResultBoolean(Member_PhoneNumberConfirmed, row),
                    TwoFactorEnabled = ToolsBO.ProcessResultBoolean(Member_TwoFactorEnabled, row),

                    LockoutEndDateUtc = ToolsBO.ProcessResultDateTime(Member_LockoutEndDateUtc, row),
                    LockoutEnabled = ToolsBO.ProcessResultBoolean(Member_LockoutEnabled, row),
                    AccessFailedCount = ToolsBO.ProcessResultInt(Member_AccessFailedCount, row),
                    UserName = ToolsBO.ProcessResultString(Member_UserName, row),

                    Name = ToolsBO.ProcessResultString(Member_Name, row),
                    ProfilePicUrl = ToolsBO.ProcessResultString(Member_ProfilePicUrl, row)
                });
            }

            return result_items;
        }


        #endregion datatable to model

        #endregion methods

    }
}