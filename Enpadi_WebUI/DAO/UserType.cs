﻿using Enpadi_WebUI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.DAO
{
    public class UserType
    {
        #region constants

        #region sp names

        private const String SaveStoreProcedureName = "UserTypeInsert";
        private const String GetStoreProcedureName = "UserTypeGet";

        #endregion sp names

        #region parameters

        private const String Param_id = "@id";
        private const String Param_name = "@name";
        private const String Param_description = "@description";

        #endregion parameters

        #region parameters

        private const String Member_id = "id";
        private const String Member_name = "name";
        private const String Member_description = "description";

        #endregion parameters

        #endregion constants

        #region methods

        #region save

        public static Int32 Save(
         Guid id,
         string name,
         string description
)
        {
            DataTable operation_result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_id, id),
                new SqlParameter(Param_name,name),
                new SqlParameter(Param_description,description)
            };

            operation_result = DataBaseConnector.ExecuteStoreProcedure(SaveStoreProcedureName, parameters);

            return Int32.Parse(operation_result.Rows[0].ItemArray[0].ToString());
        }

        public static UserTypeModel Save(
             UserTypeModel item
        )
        {
            Save(
                item.id,
                item.name,
                item.description
             );

            return item;
        }

        public static void Save(
            List<UserTypeModel> items
        )
        {
            foreach (UserTypeModel item in items)
            {
                Save(item);
            }
        }

        #endregion save

        #region get

        public static UserTypeModel Get(Guid id)
        {
            DataTable result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_id, id)

            };

            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName, parameters);

            return PorcessResult(result).FirstOrDefault();
        }

        public static List<UserTypeModel> GetAll()
        {
            DataTable result = new DataTable();
            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName);

            return PorcessResult(result);
        }

        #endregion get

        #region datatable to model

        private static List<UserTypeModel> PorcessResult(DataTable items)
        {

            List<UserTypeModel> result_items = new List<UserTypeModel>();

            foreach (DataRow row in items.Rows)
            {
                result_items.Add(new UserTypeModel
                {
                    id = ToolsBO.ProcessResultGuid(Member_id, row),
                    name = ToolsBO.ProcessResultString(Member_name, row),
                    description = ToolsBO.ProcessResultString(Member_description, row)
                });
            }

            return result_items;
        }


        #endregion datatable to model

        #endregion methods
    }
}