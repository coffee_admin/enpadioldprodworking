﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enpadi_WebUI.Models;
using System.Data;
using System.Data.SqlClient;

namespace Enpadi_WebUI.DAO
{
    public class PagoServicios
    {
        #region constants


        #region sp names

        private const String SaveStoreProcedureName = "PagoServiciosInsert";
        private const String GetStoreProcedureName = "PagoServiciosGet";


        #endregion sp names

        #region parameters

        private const String Param_pagoID = "@pagoID";
        private const String Param_tarjetaID = "@tarjetaID";
        private const String Param_sku = "@sku";
        private const String Param_nombre = "@nombre";

        private const String Param_compañia = "@compañia";
        private const String Param_referencia = "@referencia";
        private const String Param_referencia2 = "@referencia2";
        private const String Param_monto = "@monto";

        private const String Param_comision = "@comision";
        private const String Param_fechaLocal = "@fechaLocal";
        private const String Param_horaLocal = "@horaLocal";
        private const String Param_fechaContable = "@fechaContable";

        private const String Param_codigoRespuesta = "@codigoRespuesta";
        private const String Param_codigoRespuestaDescr = "@codigoRespuestaDescr";
        private const String Param_tipoPago = "@tipoPago";
        private const String Param_autorizacion = "@autorizacion";

        private const String Param_brand = "@brand";
        private const String Param_leyenda = "@leyenda";
        private const String Param_leyenda1 = "@leyenda1";
        private const String Param_leyenda2 = "@leyenda2";

        private const String Param_dv = "@dv";
        private const String Param_token = "@token";
        private const String Param_proveedor = "@proveedor";
        private const String Param_estatus = "@estatus";

        private const String Param_fechaRegistro = "@fechaRegistro";
        private const String Param_fechaBaja = "@fechaBaja";


        #endregion parameters


        #region parameters

        private const String Member_pagoID = "pagoID";
        private const String Member_tarjetaID = "tarjetaID";
        private const String Member_sku = "sku";
        private const String Member_nombre = "nombre";

        private const String Member_compañia = "compañia";
        private const String Member_referencia = "referencia";
        private const String Member_referencia2 = "referencia2";
        private const String Member_monto = "monto";

        private const String Member_comision = "comision";
        private const String Member_fechaLocal = "fechaLocal";
        private const String Member_horaLocal = "horaLocal";
        private const String Member_fechaContable = "fechaContable";

        private const String Member_codigoRespuesta = "codigoRespuesta";
        private const String Member_codigoRespuestaDescr = "codigoRespuestaDescr";
        private const String Member_tipoPago = "tipoPago";
        private const String Member_autorizacion = "autorizacion";

        private const String Member_brand = "brand";
        private const String Member_leyenda = "leyenda";
        private const String Member_leyenda1 = "leyenda1";
        private const String Member_leyenda2 = "leyenda2";

        private const String Member_dv = "dv";
        private const String Member_token = "token";
        private const String Member_proveedor = "proveedor";
        private const String Member_estatus = "estatus";

        private const String Member_fechaRegistro = "fechaRegistro";
        private const String Member_fechaBaja = "fechaBaja";

        #endregion parameters

        #endregion constants

        #region methods

        #region save

        public static Int32 Save(
            Int32 pagoID,
            Int32 tarjetaID,
            String sku,
            String nombre,

            String compañia,
            String referencia,
            String referencia2,
            Decimal? monto,

            Decimal? comision,
            String fechaLocal,
            String horaLocal,
            String fechaContable,

            Int32? codigoRespuesta,
            String codigoRespuestaDescr,
            String tipoPago,
            String autorizacion,

            String brand,
            String leyenda,
            String leyenda1,
            String leyenda2,

            String dv,
            String token,
            String proveedor,
            Int32? estatus,

            DateTime? fechaRegistro,
            DateTime? fechaBaja
        )
        {
            DataTable operation_result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_pagoID, pagoID),
                new SqlParameter(Param_tarjetaID,tarjetaID),
                new SqlParameter(Param_sku,sku),
                new SqlParameter(Param_nombre,nombre),

                new SqlParameter(Param_compañia, compañia),
                new SqlParameter(Param_referencia,referencia),
                new SqlParameter(Param_referencia2,referencia2),
                new SqlParameter(Param_monto, (monto == 0 ? null : monto)),

                new SqlParameter(Param_comision, (comision == 0 ? null : comision)),
                new SqlParameter(Param_fechaLocal,fechaLocal),
                new SqlParameter(Param_horaLocal,horaLocal),
                new SqlParameter(Param_fechaContable,fechaContable),

                new SqlParameter(Param_codigoRespuesta, codigoRespuesta),
                new SqlParameter(Param_codigoRespuestaDescr,codigoRespuestaDescr),
                new SqlParameter(Param_tipoPago,tipoPago),
                new SqlParameter(Param_autorizacion,autorizacion),

                new SqlParameter(Param_brand, brand),
                new SqlParameter(Param_leyenda,leyenda),
                new SqlParameter(Param_leyenda1,leyenda1),
                new SqlParameter(Param_leyenda2,leyenda2),

                new SqlParameter(Param_dv, dv),
                new SqlParameter(Param_token,token),
                new SqlParameter(Param_proveedor,proveedor),
                new SqlParameter(Param_estatus,estatus),

                new SqlParameter(Param_fechaRegistro, fechaRegistro),
                new SqlParameter(Param_fechaBaja,fechaBaja)

            };

            operation_result = DataBaseConnector.ExecuteStoreProcedure(SaveStoreProcedureName, parameters);

            return Int32.Parse(operation_result.Rows[0].ItemArray[0].ToString());
        }

        public static PagoServiciosModel Save(
             PagoServiciosModel item
        )
        {
            item.pagoID = Save(
                item.pagoID,
                item.tarjetaID,
                item.sku,
                item.nombre,
                item.compañia,
                item.referencia,
                item.referencia2,
                item.monto,
                item.comision,
                item.fechaLocal,
                item.horaLocal,
                item.fechaContable,
                item.codigoRespuesta,
                item.codigoRespuestaDescr,
                item.tipoPago,
                item.autorizacion,
                item.brand,
                item.leyenda,
                item.leyenda1,
                item.leyenda2,
                item.dv,
                item.token,
                item.proveedor,
                item.estatus,
                item.fechaRegistro,
                item.fechaBaja
             );

            return item;
        }

        public static Enpadi_WebUI.Models.pagoServicio Save(
    Enpadi_WebUI.Models.pagoServicio item
)
        {
            item.pagoID = Save(
                item.pagoID,
                item.tarjetaID,
                item.sku,
                item.nombre,
                item.compañia,
                item.referencia,
                item.referencia2,
                (item.monto != null ? item.monto : 0),
                (item.comision != null ? item.comision : 0),
                item.fechaLocal,
                item.horaLocal,
                item.fechaContable,
                item.codigoRespuesta,
                item.codigoRespuestaDescr,
                item.tipoPago,
                item.autorizacion,
                item.brand,
                item.leyenda,
                item.leyenda1,
                item.leyenda2,
                item.dv,
                item.token,
                item.proveedor,
                item.estatus,
                DateTime.Now,
                null
             );

            return item;
        }


        public static void Save(
            List<PagoServiciosModel> items
        )
        {
            foreach (PagoServiciosModel item in items)
            {
                Save(item);
            }
        }

        #endregion save

        #region get

        public static PagoServiciosModel Get(Int32 id)
        {
            DataTable result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_pagoID, id)

            };

            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName, parameters);

            return PorcessResult(result).FirstOrDefault();
        }

        public static List<PagoServiciosModel> GetAll()
        {
            DataTable result = new DataTable();
            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName);

            return PorcessResult(result);
        }

        #endregion get

        #region datatable to model

        private static List<PagoServiciosModel> PorcessResult(DataTable items)
        {

            List<PagoServiciosModel> result_items = new List<PagoServiciosModel>();

            foreach (DataRow row in items.Rows)
            {
                result_items.Add(new PagoServiciosModel
                {
                    pagoID = ToolsBO.ProcessResultInt(Member_pagoID, row),
                    tarjetaID = ToolsBO.ProcessResultInt(Member_tarjetaID, row),
                    sku = ToolsBO.ProcessResultString(Member_sku, row),
                    nombre = ToolsBO.ProcessResultString(Member_nombre, row),
                    compañia = ToolsBO.ProcessResultString(Member_compañia, row),
                    referencia = ToolsBO.ProcessResultString(Member_referencia, row),
                    referencia2 = ToolsBO.ProcessResultString(Member_referencia2, row),
                    monto = ToolsBO.ProcessResultDecimal(Member_monto, row),
                    comision = ToolsBO.ProcessResultDecimal(Member_comision, row),
                    fechaLocal = ToolsBO.ProcessResultString(Member_fechaLocal, row),
                    horaLocal = ToolsBO.ProcessResultString(Member_horaLocal, row),
                    fechaContable = ToolsBO.ProcessResultString(Member_fechaContable, row),
                    codigoRespuesta = ToolsBO.ProcessResultInt(Member_codigoRespuesta, row),
                    codigoRespuestaDescr = ToolsBO.ProcessResultString(Member_codigoRespuestaDescr, row),
                    tipoPago = ToolsBO.ProcessResultString(Member_tipoPago, row),
                    autorizacion = ToolsBO.ProcessResultString(Member_autorizacion, row),
                    brand = ToolsBO.ProcessResultString(Member_brand, row),
                    leyenda = ToolsBO.ProcessResultString(Member_leyenda, row),
                    leyenda1 = ToolsBO.ProcessResultString(Member_leyenda1, row),
                    leyenda2 = ToolsBO.ProcessResultString(Member_leyenda2, row),
                    dv = ToolsBO.ProcessResultString(Member_dv, row),
                    token = ToolsBO.ProcessResultString(Member_token, row),
                    proveedor = ToolsBO.ProcessResultString(Member_proveedor, row),
                    estatus = ToolsBO.ProcessResultInt(Member_estatus, row),
                    fechaRegistro = ToolsBO.ProcessResultDateTime(Member_fechaRegistro, row),
                    fechaBaja = ToolsBO.ProcessResultDateTime(Member_fechaBaja, row)
                });
            }

            return result_items;
        }


        #endregion datatable to model


        #endregion methods
    }
}