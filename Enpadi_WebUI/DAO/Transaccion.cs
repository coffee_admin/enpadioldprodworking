﻿using Enpadi_WebUI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.DAO
{
    public class Transaccion
    {

        #region variables

        #region sp name

        private const String SaveStoreProcedureName = "TransactionInsert";
        private const String GetStoreProcedureName = "TransactionGet";
        private const String GetUserAccountStatus = "UserAccountStatus";

        #endregion sp name

        #region parameters

        private const String Param_transID = "@transID";
        private const String Param_origenID = "@origenID";
        private const String Param_tipoID = "@tipoID";
        private const String Param_modoID = "@modoID";
        private const String Param_socioID = "@socioID";
        private const String Param_terminalID = "@terminalID";

        private const String Param_monedaID = "@monedaID";
        private const String Param_tarjetaID = "@tarjetaID";
        private const String Param_tarjeta = "@tarjeta";
        private const String Param_monto = "@monto";
        private const String Param_abono1 = "@abono1";
        private const String Param_abono1iva = "@abono1iva";

        private const String Param_cargo1 = "@cargo1";
        private const String Param_cargo1iva = "@cargo1iva";
        private const String Param_abono2 = "@abono2";
        private const String Param_abono2iva = "@abono2iva";
        private const String Param_cargo2 = "@cargo2";
        private const String Param_cargo2iva = "@cargo2iva";

        private const String Param_abono3 = "@abono3";
        private const String Param_abono3iva = "@abono3iva";
        private const String Param_cargo3 = "@cargo3";
        private const String Param_cargo3iva = "@cargo3iva";
        private const String Param_abono4 = "@abono4";
        private const String Param_abono4iva = "@abono4iva";

        private const String Param_cargo4 = "@cargo4";
        private const String Param_cargo4iva = "@cargo4iva";
        private const String Param_abono5 = "@abono5";
        private const String Param_abono5iva = "@abono5iva";
        private const String Param_cargo5 = "@cargo5";
        private const String Param_cargo5iva = "@cargo5iva";

        private const String Param_abono6 = "@abono6";
        private const String Param_abono6iva = "@abono6iva";
        private const String Param_cargo6 = "@cargo6";
        private const String Param_cargo6iva = "@cargo6iva";
        private const String Param_abonoTotal = "@abonoTotal";
        private const String Param_cargoTotal = "@cargoTotal";

        private const String Param_facturable1 = "@facturable1";
        private const String Param_facturable1iva = "@facturable1iva";
        private const String Param_pagado1 = "@pagado1";
        private const String Param_transaccion = "@transaccion";
        private const String Param_concepto = "@concepto";
        private const String Param_descripcion = "@descripcion";

        private const String Param_referencia = "@referencia";
        private const String Param_estatusID = "@estatusID";
        private const String Param_aprobacion = "@aprobacion";
        private const String Param_ip = "@ip";
        private const String Param_geoCountryCode = "@geoCountryCode";
        private const String Param_geoCountryName = "@geoCountryName";

        private const String Param_geoRegion = "@geoRegion";
        private const String Param_geoCity = "@geoCity";
        private const String Param_geoPostalCode = "@geoPostalCode";

        private const String Param_fechaRegistro = "@fechaRegistro";

        #endregion parameters

        #region members

        private const String Member_transID = "transID";
        private const String Member_origenID = "origenID";
        private const String Member_tipoID = "tipoID";
        private const String Member_modoID = "modoID";
        private const String Member_socioID = "socioID";
        private const String Member_terminalID = "terminalID";

        private const String Member_monedaID = "monedaID";
        private const String Member_tarjetaID = "tarjetaID";
        private const String Member_tarjeta = "tarjeta";
        private const String Member_monto = "monto";
        private const String Member_abono1 = "abono1";
        private const String Member_abono1iva = "abono1iva";

        private const String Member_cargo1 = "cargo1";
        private const String Member_cargo1iva = "cargo1iva";
        private const String Member_abono2 = "abono2";
        private const String Member_abono2iva = "abono2iva";
        private const String Member_cargo2 = "cargo2";
        private const String Member_cargo2iva = "cargo2iva";

        private const String Member_abono3 = "abono3";
        private const String Member_abono3iva = "abono3iva";
        private const String Member_cargo3 = "cargo3";
        private const String Member_cargo3iva = "cargo3iva";
        private const String Member_abono4 = "abono4";
        private const String Member_abono4iva = "abono4iva";

        private const String Member_cargo4 = "cargo4";
        private const String Member_cargo4iva = "cargo4iva";
        private const String Member_abono5 = "abono5";
        private const String Member_abono5iva = "abono5iva";
        private const String Member_cargo5 = "cargo5";
        private const String Member_cargo5iva = "cargo5iva";

        private const String Member_abono6 = "abono6";
        private const String Member_abono6iva = "abono6iva";
        private const String Member_cargo6 = "cargo6";
        private const String Member_cargo6iva = "cargo6iva";
        private const String Member_abonoTotal = "abonoTotal";
        private const String Member_cargoTotal = "cargoTotal";

        private const String Member_facturable1 = "facturable1";
        private const String Member_facturable1iva = "facturable1iva";
        private const String Member_pagado1 = "pagado1";
        private const String Member_transaccion = "transaccion";
        private const String Member_concepto = "concepto";
        private const String Member_descripcion = "descripcion";

        private const String Member_referencia = "referencia";
        private const String Member_estatusID = "estatusID";
        private const String Member_aprobacion = "aprobacion";
        private const String Member_ip = "ip";
        private const String Member_geoCountryCode = "geoCountryCode";
        private const String Member_geoCountryName = "geoCountryName";

        private const String Member_geoRegion = "geoRegion";
        private const String Member_geoCity = "geoCity";
        private const String Member_geoPostalCode = "geoPostalCode";

        private const String Member_fechaRegistro = "fechaRegistro";

        #endregion members

        #endregion variables

        #region methods

        #region save

        public static Int32 Save(
                 int origenID,
                 int tipoID,
                 int modoID,
                 int socioID,
                 int terminalID,

                 int monedaID,
                 int tarjetaID,
                 string tarjeta,
                 decimal monto,
                 decimal abono1,
                 decimal abono1iva,

                 decimal cargo1,
                 decimal cargo1iva,
                 decimal abono2,
                 decimal abono2iva,
                 decimal cargo2,
                 decimal cargo2iva,

                 decimal abono3,
                 decimal abono3iva,
                 decimal cargo3,
                 decimal cargo3iva,
                 decimal abono4,
                 decimal abono4iva,

                 decimal cargo4,
                 decimal cargo4iva,
                 decimal abono5,
                 decimal abono5iva,
                 decimal cargo5,
                 decimal cargo5iva,

                 decimal abono6,
                 decimal abono6iva,
                 decimal cargo6,
                 decimal cargo6iva,
                 decimal abonoTotal,
                 decimal cargoTotal,

                 decimal facturable1,
                 decimal facturable1iva,
                 bool pagado1,
                 string transaccion,
                 string concepto,
                 string descripcion,

                 string referencia,
                 int estatusID,
                 string aprobacion,
                 string ip,
                 int geoCountryCode,
                 string geoCountryName,

                 string geoRegion,
                 string geoCity,
                 string geoPostalCode,

                 DateTime fechaRegistro,
                 DateTime fechaBaja
            )
        {
            SqlParameter[] parameters = {
                new SqlParameter(Param_origenID,origenID),
                new SqlParameter(Param_tipoID,tipoID),
                new SqlParameter(Param_modoID,modoID),
                new SqlParameter(Param_socioID,socioID),
                new SqlParameter(Param_terminalID,terminalID),

                new SqlParameter(Param_monedaID,monedaID),
                new SqlParameter(Param_tarjetaID,tarjetaID),
                new SqlParameter(Param_tarjeta,tarjeta),
                new SqlParameter(Param_monto,monto),
                new SqlParameter(Param_abono1,abono1),
                new SqlParameter(Param_abono1iva,abono1iva),

                new SqlParameter(Param_cargo1,cargo1),
                new SqlParameter(Param_cargo1iva,cargo1iva),
                new SqlParameter(Param_abono2,abono2),
                new SqlParameter(Param_abono2iva,abono2iva),
                new SqlParameter(Param_cargo2,cargo2),
                new SqlParameter(Param_cargo2iva,cargo2iva),

                new SqlParameter(Param_abono3,abono3),
                new SqlParameter(Param_abono3iva,abono3iva),
                new SqlParameter(Param_cargo3,cargo3),
                new SqlParameter(Param_cargo3iva,cargo3iva),
                new SqlParameter(Param_abono4,abono4),
                new SqlParameter(Param_abono4iva,abono4iva),

                new SqlParameter(Param_cargo4,cargo4),
                new SqlParameter(Param_cargo4iva,cargo4iva),
                new SqlParameter(Param_abono5,abono5),
                new SqlParameter(Param_abono5iva,abono5iva),
                new SqlParameter(Param_cargo5,cargo5),
                new SqlParameter(Param_cargo5iva,cargo5iva),

                new SqlParameter(Param_abono6,abono6),
                new SqlParameter(Param_abono6iva,abono6iva),
                new SqlParameter(Param_cargo6,cargo6),
                new SqlParameter(Param_cargo6iva,cargo6iva),
                new SqlParameter(Param_abonoTotal,abonoTotal),
                new SqlParameter(Param_cargoTotal,cargoTotal),

                new SqlParameter(Param_facturable1,facturable1),
                new SqlParameter(Param_facturable1iva,facturable1iva),
                new SqlParameter(Param_pagado1,pagado1),
                new SqlParameter(Param_transaccion,transaccion),
                new SqlParameter(Param_concepto,concepto),
                new SqlParameter(Param_descripcion,descripcion),

                new SqlParameter(Param_referencia,referencia),
                new SqlParameter(Param_estatusID,estatusID),
                new SqlParameter(Param_aprobacion,aprobacion),
                new SqlParameter(Param_ip,ip),
                new SqlParameter(Param_geoCountryCode,geoCountryCode),
                new SqlParameter(Param_geoCountryName,geoCountryName),

                new SqlParameter(Param_geoRegion,geoRegion),
                new SqlParameter(Param_geoCity,geoCity),
                new SqlParameter(Param_geoPostalCode,geoPostalCode),

                new SqlParameter(Param_fechaRegistro,fechaRegistro)
            };

            DataTable operation_result = new DataTable();

            operation_result = DataBaseConnector.ExecuteStoreProcedure(SaveStoreProcedureName, parameters);
            
            return Int32.Parse(operation_result.Rows[0].ItemArray[0].ToString());
        }

        public static Int32 Save(
            trans response
         )
        {
            response.transID = Save(
                response.origenID,
                response.tipoID,
                response.modoID,
                response.socioID,
                response.terminalID,

                response.monedaID,
                response.tarjetaID,
                response.tarjeta,
                response.monto,
                response.abono1,
                response.abono1iva,

                response.cargo1,
                response.cargo1iva,
                response.abono2,
                response.abono2iva,
                response.cargo2,
                response.cargo2iva,

                response.abono3,
                response.abono3iva,
                response.cargo3,
                response.cargo3iva,
                response.abono4,
                response.abono4iva,

                response.cargo4,
                response.cargo4iva,
                response.abono5,
                response.abono5iva,
                response.cargo5,
                response.cargo5iva,

                response.abono6,
                response.abono6iva,
                response.cargo6,
                response.cargo6iva,
                response.abonoTotal,
                response.cargoTotal,

                response.facturable1,
                response.facturable1iva,
                response.pagado1,
                response.transaccion,
                response.Concepto,
                response.descripcion,

                response.referencia,
                response.estatusID,
                response.aprobacion,
                response.ip,
                response.geoCountryCode,
                response.geoCountryName,

                response.geoRegion,
                response.geoCity,
                response.geoPostalCode,

                DateTime.Now,
                DateTime.MinValue

                );

            return response.transID;
        }

        #endregion save

        #region get

        public static trans Get(Int64 id)
        {
            DataTable result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_transID, id)

            };

            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName, parameters);

            return PorcessResult(result).FirstOrDefault();
        }

        public static List<trans> GetAll()
        {
            DataTable result = new DataTable();
            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName);

            return PorcessResult(result);
        }

        public static Decimal GetAccountStatus(Int32 tarjetaID)
        {
            DataTable result = new DataTable();
            SqlParameter[] parameters = {
                new SqlParameter("tarjetaID", tarjetaID)

            };
            result = DataBaseConnector.ExecuteStoreProcedure(GetUserAccountStatus, parameters);

            return ToolsBO.ProcessResultDecimal("accountBalance", result.Rows[0]);
        }

        #endregion get

        #region datatable to model

        private static List<trans> PorcessResult(DataTable items)
        {

            List<trans> result_items = new List<trans>();

            foreach (DataRow row in items.Rows)
            {
                result_items.Add(new trans
                {

                    origenID = ToolsBO.ProcessResultInt(Member_origenID, row),
                    tipoID = ToolsBO.ProcessResultInt(Member_tipoID, row),
                    modoID = ToolsBO.ProcessResultInt(Member_modoID, row),
                    socioID = ToolsBO.ProcessResultInt(Member_socioID, row),
                    terminalID = ToolsBO.ProcessResultInt(Member_terminalID, row),

                    monedaID = ToolsBO.ProcessResultInt(Member_monedaID, row),
                    tarjetaID = ToolsBO.ProcessResultInt(Member_tarjetaID, row),
                    tarjeta = ToolsBO.ProcessResultString(Member_tarjeta, row),
                    monto = ToolsBO.ProcessResultUInt64(Member_monto, row),
                    abono1 = ToolsBO.ProcessResultUInt64(Member_abono1, row),
                    abono1iva = ToolsBO.ProcessResultUInt64(Member_abono1iva, row),

                    cargo1 = ToolsBO.ProcessResultUInt64(Member_cargo1, row),
                    cargo1iva = ToolsBO.ProcessResultUInt64(Member_cargo1iva, row),
                    abono2 = ToolsBO.ProcessResultUInt64(Member_abono2, row),
                    abono2iva = ToolsBO.ProcessResultUInt64(Member_abono2iva, row),
                    cargo2 = ToolsBO.ProcessResultUInt64(Member_cargo2, row),
                    cargo2iva = ToolsBO.ProcessResultUInt64(Member_cargo2iva, row),

                    abono3 = ToolsBO.ProcessResultUInt64(Member_abono3, row),
                    abono3iva = ToolsBO.ProcessResultUInt64(Member_abono3iva, row),
                    cargo3 = ToolsBO.ProcessResultUInt64(Member_cargo3, row),
                    cargo3iva = ToolsBO.ProcessResultUInt64(Member_cargo3iva, row),
                    abono4 = ToolsBO.ProcessResultUInt64(Member_abono4, row),
                    abono4iva = ToolsBO.ProcessResultUInt64(Member_abono4iva, row),

                    cargo4 = ToolsBO.ProcessResultUInt64(Member_cargo4, row),
                    cargo4iva = ToolsBO.ProcessResultUInt64(Member_cargo4iva, row),
                    abono5 = ToolsBO.ProcessResultUInt64(Member_abono5, row),
                    abono5iva = ToolsBO.ProcessResultUInt64(Member_abono5iva, row),
                    cargo5 = ToolsBO.ProcessResultUInt64(Member_cargo5, row),
                    cargo5iva = ToolsBO.ProcessResultUInt64(Member_cargo5iva, row),

                    abono6 = ToolsBO.ProcessResultUInt64(Member_abono6, row),
                    abono6iva = ToolsBO.ProcessResultUInt64(Member_abono6iva, row),
                    cargo6 = ToolsBO.ProcessResultUInt64(Member_cargo6, row),
                    cargo6iva = ToolsBO.ProcessResultUInt64(Member_cargo6iva, row),
                    abonoTotal = ToolsBO.ProcessResultUInt64(Member_abonoTotal, row),
                    cargoTotal = ToolsBO.ProcessResultUInt64(Member_cargoTotal, row),

                    facturable1 = ToolsBO.ProcessResultUInt64(Member_facturable1, row),
                    facturable1iva = ToolsBO.ProcessResultUInt64(Member_facturable1iva, row),
                    pagado1 = ToolsBO.ProcessResultBoolean(Member_pagado1, row),
                    transaccion = ToolsBO.ProcessResultString(Member_transaccion, row),
                    Concepto = ToolsBO.ProcessResultString(Member_concepto, row),
                    descripcion = ToolsBO.ProcessResultString(Member_descripcion, row),

                    referencia = ToolsBO.ProcessResultString(Member_referencia, row),
                    estatusID = ToolsBO.ProcessResultInt(Member_estatusID, row),
                    aprobacion = ToolsBO.ProcessResultString(Member_aprobacion, row),
                    ip = ToolsBO.ProcessResultString(Member_ip, row),
                    geoCountryCode = ToolsBO.ProcessResultInt(Member_geoCountryCode, row),
                    geoCountryName = ToolsBO.ProcessResultString(Member_geoCountryName, row),

                    geoRegion = ToolsBO.ProcessResultString(Member_geoRegion, row),
                    geoCity = ToolsBO.ProcessResultString(Member_geoCity, row),
                    geoPostalCode = ToolsBO.ProcessResultString(Member_geoPostalCode, row),

                    fechaRegistro = ToolsBO.ProcessResultDateTime(Member_fechaRegistro, row)

                });
            }

            return result_items;
        }


        #endregion datatable to model

        #endregion methods
    }
}