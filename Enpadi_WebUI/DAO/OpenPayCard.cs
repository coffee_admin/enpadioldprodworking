﻿using Enpadi_WebUI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


namespace Enpadi_WebUI.DAO
{
    public class OpenPayCard
    {

        #region constants


        #region sp names

        private const String SaveStoreProcedureName = "OpenPayCardInsert";
        private const String GetStoreProcedureName = "OpenPayCardGet";


        #endregion sp names

        #region parameters

        private const String Param_id = "@id";
        private const String Param_CardID = "@CardID";
        private const String Param_ExternalID = "@ExternalID";
        private const String Param_HolderName = "@HolderName";

        private const String Param_CardNumber = "@CardNumber";
        private const String Param_CVV2 = "@CVV2";
        private const String Param_ExpirationMonth = "@ExpirationMonth";
        private const String Param_ExpirationYear = "@ExpirationYear";

        private const String Param_Address1 = "@Address1";

        private const String Param_Address2 = "@Address2";
        private const String Param_Address3 = "@Address3";
        private const String Param_City = "@City";
        private const String Param_State = "@State";
        private const String Param_Country = "@Country";
        private const String Param_PostalCode = "@PostalCode";
        private const String Param_AllowCharges = "@AllowCharges";
        private const String Param_AllowPayouts = "@AllowPayouts";
        private const String Param_Brand = "@Brand";

        private const String Param_Type = "@Type";
        private const String Param_BankName = "@BankName";
        private const String Param_BankCode = "@BankCode";
        private const String Param_CustomerId = "@CustomerId";
        private const String Param_PointsCard = "@PointsCard";
        private const String Param_CreationDate = "@CreationDate";
        private const String Param_DeleteDate = "@DeleteDate";

        #endregion parameters


        #region members

        private const String Member_id = "id";
        private const String Member_CardID = "CardID";
        private const String Member_ExternalID = "ExternalID";
        private const String Member_HolderName = "HolderName";

        private const String Member_CardNumber = "CardNumber";
        private const String Member_CVV2 = "CVV2";
        private const String Member_ExpirationMonth = "ExpirationMonth";
        private const String Member_ExpirationYear = "ExpirationYear";

        private const String Member_Address1 = "Address1";

        private const String Member_Address2 = "Address2";
        private const String Member_Address3 = "Address3";
        private const String Member_City = "City";
        private const String Member_State = "State";
        private const String Member_Country = "Country";
        private const String Member_PostalCode = "PostalCode";
        private const String Member_AllowCharges = "AllowCharges";
        private const String Member_AllowPayouts = "AllowPayouts";
        private const String Member_Brand = "Brand";

        private const String Member_Type = "Type";
        private const String Member_BankName = "BankName";
        private const String Member_BankCode = "BankCode";
        private const String Member_CustomerId = "CustomerId";
        private const String Member_PointsCard = "PointsCard";
        private const String Member_CreationDate = "CreationDate";
        private const String Member_DeleteDate = "DeleteDate";


        #endregion members

        #endregion constants

        #region methods

        #region save

        public static Int32 Save(
            Int32? id,
            String CardID,
            Int32 ExternalID,
            String HolderName,

            String CardNumber,
            String CVV2,
            String ExpirationMonth,
            String ExpirationYear,

            String Address1,
            String Address2,
            String Address3,
            String City,

            String State,
            String Country,
            String PostalCode,
            Boolean AllowCharges,

            Boolean AllowPayouts,
            String Brand,
            String Type,
            String BankName,

            String BankCode,
            String CustomerId,
            Boolean PointsCard,
            DateTime CreationDate
        )
        {
            DataTable result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_id, id),
                new SqlParameter(Param_CardID,CardID),
                new SqlParameter(Param_ExternalID,ExternalID),
                new SqlParameter(Param_HolderName,HolderName),

                new SqlParameter(Param_CardNumber,CardNumber),
                new SqlParameter(Param_CVV2,CVV2),
                new SqlParameter(Param_ExpirationMonth,ExpirationMonth),
                new SqlParameter(Param_ExpirationYear,ExpirationYear),

                new SqlParameter(Param_Address1,Address1),
                new SqlParameter(Param_Address2,Address2),
                new SqlParameter(Param_Address3,Address3),
                new SqlParameter(Param_City,City),

                new SqlParameter(Param_State,State),
                new SqlParameter(Param_Country,Country),
                new SqlParameter(Param_PostalCode,PostalCode),
                new SqlParameter(Param_AllowCharges,AllowCharges),

                new SqlParameter(Param_AllowPayouts,AllowPayouts),
                new SqlParameter(Param_Brand,Brand),
                new SqlParameter(Param_Type,Type),
                new SqlParameter(Param_BankName,BankName),

                new SqlParameter(Param_BankCode,BankCode),
                new SqlParameter(Param_CustomerId,CustomerId),
                new SqlParameter(Param_PointsCard,PointsCard),
                new SqlParameter(Param_CreationDate,DateTime.Now)

            };

            result = DataBaseConnector.ExecuteStoreProcedure(SaveStoreProcedureName, parameters);

            return Int32.Parse(result.Rows[0].ItemArray[0].ToString());
        }

        public static OpenPayCardModel Save(
            OpenPayCardModel item
        )
        {
            item.id = Save(
                item.id,
                item.CardID,
                item.ExternalID,
                item.HolderName,

                item.CardNumber,
                item.CVV2,
                item.ExpirationMonth,
                item.ExpirationYear,

                item.Address1,
                item.Address2,
                item.Address3,
                item.City,

                item.State,
                item.Country,
                item.PostalCode,
                item.AllowCharges,

                item.AllowPayouts,
                item.Brand,
                item.Type,
                item.BankName,

                item.BankCode,
                item.CustomerId,
                item.PointsCard,
                DateTime.Now
             );

            return item;
        }

        public static openpayCard Save(
            openpayCard item
        )
        {
            item.id = Save(
                item.id,
                item.CardID,
                item.ExternalID,
                item.HolderName,

                item.CardNumber,
                item.CVV2,
                item.ExpirationMonth,
                item.ExpirationYear,

                item.Address1,
                item.Address2,
                item.Address3,
                item.City,

                item.State,
                item.Country,
                item.PostalCode,
                item.AllowCharges,

                item.AllowPayouts,
                item.Brand,
                item.Type,
                item.BankName,

                item.BankCode,
                item.CustomerId,
                item.PointsCard,
                DateTime.Now
             );

            return item;
        }

        public static void Save(
            List<OpenPayCardModel> items
        )
        {
            foreach (OpenPayCardModel item in items)
            {
                Save(item);
            }
        }

        #endregion save

        #region get

        public static OpenPayCardModel Get(Int32 id)
        {
            DataTable result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_id, id)

            };

            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName, parameters);

            return PorcessResult(result).FirstOrDefault();
        }

        public static List<OpenPayCardModel> GetAll()
        {
            DataTable result = new DataTable();
            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName);

            return PorcessResult(result);
        }

        #endregion get

        #region datatable to model

        private static List<OpenPayCardModel> PorcessResult(DataTable items)
        {

            List<OpenPayCardModel> result_items = new List<OpenPayCardModel>();

            foreach (DataRow row in items.Rows)
            {
                result_items.Add(new OpenPayCardModel
                {
                    id = ToolsBO.ProcessResultInt(Member_id , row), 
                    CardID = ToolsBO.ProcessResultString(Member_CardID, row),
                    ExternalID = ToolsBO.ProcessResultInt(Member_ExternalID, row),
                    HolderName = ToolsBO.ProcessResultString(Member_HolderName, row),
                    CardNumber = ToolsBO.ProcessResultString(Member_CardNumber, row),
                    CVV2 = ToolsBO.ProcessResultString(Member_CVV2, row),
                    ExpirationMonth = ToolsBO.ProcessResultString(Member_ExpirationMonth, row),
                    ExpirationYear = ToolsBO.ProcessResultString(Member_ExpirationYear, row),
                    Address1 = ToolsBO.ProcessResultString(Member_Address1, row),
                    Address2 = ToolsBO.ProcessResultString(Member_Address2, row),
                    Address3 = ToolsBO.ProcessResultString(Member_Address3, row),
                    City = ToolsBO.ProcessResultString(Member_City, row),
                    State = ToolsBO.ProcessResultString(Member_State, row),
                    Country = ToolsBO.ProcessResultString(Member_Country, row),
                    PostalCode = ToolsBO.ProcessResultString(Member_PostalCode, row),
                    AllowCharges = ToolsBO.ProcessResultBoolean(Member_AllowCharges, row),
                    AllowPayouts = ToolsBO.ProcessResultBoolean(Member_AllowPayouts, row),
                    Brand = ToolsBO.ProcessResultString(Member_Brand, row),
                    Type = ToolsBO.ProcessResultString(Member_Type, row),
                    BankName = ToolsBO.ProcessResultString(Member_BankName, row),
                    BankCode = ToolsBO.ProcessResultString(Member_BankCode, row),
                    CustomerId = ToolsBO.ProcessResultString(Member_CustomerId, row),
                    PointsCard = ToolsBO.ProcessResultBoolean(Member_PointsCard, row),
                    CreationDate = ToolsBO.ProcessResultDateTime(Member_CreationDate, row)
                });
            }

            return result_items;
        }


        #endregion datatable to model


        #endregion methods

    }
}

