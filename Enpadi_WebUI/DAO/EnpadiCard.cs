﻿using Enpadi_WebUI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.DAO
{
    public class EnpadiCard
    {
        #region constants

        #region sp name

        private const String SaveStoreProcedureName = "TarjetasInsert";
        private const String GetStoreProcedureName = "TarjetasGet";

        #endregion sp name

        #region parameters
        private const String Param_tarjetaID = "@tarjetaID";

        private const String Param_productoID = "@productoID";
        private const String Param_monedaID = "@monedaID";
        private const String Param_tcNum = "@tcNum";
        private const String Param_tcMD5 = "@tcMD5";

        private const String Param_tcMes = "@tcMes";
        private const String Param_tcAnio = "@tcAnio";
        private const String Param_tcDigitos = "@tcDigitos";
        private const String Param_tcNIP = "@tcNIP";

        private const String Param_tcAsignada = "@tcAsignada";
        private const String Param_tcActiva = "@tcActiva";
        private const String Param_limActivacion = "@limActivacion";
        private const String Param_limDepMin = "@limDepMin";

        private const String Param_limDepMax = "@limDepMax";
        private const String Param_limDepDia = "@limDepDia";
        private const String Param_limDepSem = "@limDepSem";
        private const String Param_limDepMes = "@limDepMes";

        private const String Param_limRetMin = "@limRetMin";
        private const String Param_limRetMax = "@limRetMax";
        private const String Param_limRetDia = "@limRetDia";
        private const String Param_limRetSem = "@limRetSem";

        private const String Param_limRetMes = "@limRetMes";
        private const String Param_limTransMin = "@limTransMin";
        private const String Param_limTransMax = "@limTransMax";
        private const String Param_limTransDia = "@limTransDia";

        private const String Param_limTransSem = "@limTransSem";
        private const String Param_limTransMes = "@limTransMes";
        private const String Param_titulo = "@titulo";
        private const String Param_datNombre = "@datNombre";

        private const String Param_datPaterno = "@datPaterno";
        private const String Param_datMaterno = "@datMaterno";
        private const String Param_genero = "@genero";
        private const String Param_datNacimiento = "@datNacimiento";

        private const String Param_datDir1 = "@datDir1";
        private const String Param_datDir2 = "@datDir2";
        private const String Param_datColonia = "@datColonia";
        private const String Param_paisID = "@paisID";

        private const String Param_edoID = "@edoID";
        private const String Param_cdID = "@cdID";
        private const String Param_datCP = "@datCP";
        private const String Param_datTelefono = "@datTelefono";

        private const String Param_datFax = "@datFax";
        private const String Param_datCelular = "@datCelular";
        private const String Param_datNextel = "@datNextel";
        private const String Param_datBlackPin = "@datBlackPin";

        private const String Param_identificacionID = "@identificacionID";
        private const String Param_identificacionInfo = "@identificacionInfo";
        private const String Param_identificacionValida = "@identificacionValida";
        private const String Param_email = "@email";

        private const String Param_password = "@password";

        private const String Param_fechaAlta = "@fechaAlta";
        private const String Param_fechaBaja = "@fechaBaja";
        private const String Param_creditoDisponible = "@creditoDisponible";
        private const String Param_creditoPorPagar = "@creditoPorPagar";

        #endregion parameters

        #region members
        private const String Member_tarjetaID = "tarjetaID";

        private const String Member_productoID = "productoID";
        private const String Member_monedaID = "monedaID";
        private const String Member_tcNum = "tcNum";
        private const String Member_tcMD5 = "tcMD5";

        private const String Member_tcMes = "tcMes";
        private const String Member_tcAnio = "tcAnio";
        private const String Member_tcDigitos = "tcDigitos";
        private const String Member_tcNIP = "tcNIP";

        private const String Member_tcAsignada = "tcAsignada";
        private const String Member_tcActiva = "tcActiva";
        private const String Member_limActivacion = "limActivacion";
        private const String Member_limDepMin = "limDepMin";

        private const String Member_limDepMax = "limDepMax";
        private const String Member_limDepDia = "limDepDia";
        private const String Member_limDepSem = "limDepSem";
        private const String Member_limDepMes = "limDepMes";

        private const String Member_limRetMin = "limRetMin";
        private const String Member_limRetMax = "limRetMax";
        private const String Member_limRetDia = "limRetDia";
        private const String Member_limRetSem = "limRetSem";

        private const String Member_limRetMes = "limRetMes";
        private const String Member_limTransMin = "limTransMin";
        private const String Member_limTransMax = "limTransMax";
        private const String Member_limTransDia = "limTransDia";

        private const String Member_limTransSem = "limTransSem";
        private const String Member_limTransMes = "limTransMes";
        private const String Member_titulo = "titulo";
        private const String Member_datNombre = "datNombre";

        private const String Member_datPaterno = "datPaterno";
        private const String Member_datMaterno = "datMaterno";
        private const String Member_genero = "genero";
        private const String Member_datNacimiento = "datNacimiento";

        private const String Member_datDir1 = "datDir1";
        private const String Member_datDir2 = "datDir2";
        private const String Member_datColonia = "datColonia";
        private const String Member_paisID = "paisID";

        private const String Member_edoID = "edoID";
        private const String Member_cdID = "cdID";
        private const String Member_datCP = "datCP";
        private const String Member_datTelefono = "datTelefono";

        private const String Member_datFax = "datFax";
        private const String Member_datCelular = "datCelular";
        private const String Member_datNextel = "datNextel";
        private const String Member_datBlackPin = "datBlackPin";

        private const String Member_identificacionID = "identificacionID";
        private const String Member_identificacionInfo = "identificacionInfo";
        private const String Member_identificacionValida = "identificacionValida";
        private const String Member_email = "email";

        private const String Member_password = "password";

        private const String Member_fechaAlta = "fechaAlta";
        private const String Member_fechaBaja = "fechaBaja";
        private const String Member_creditoDisponible = "creditoDisponible";
        private const String Member_creditoPorPagar = "creditoPorPagar";

        #endregion members

        #endregion constants

        #region methods

        #region save

        public static Int32 Save(
            int tarjetaID,

            int productoID,
            int monedaID,
            string tcNum,
            string tcMD5,

            string tcMes,
            string tcAnio,
            string tcDigitos,
            string tcNIP,

            bool tcAsignada,
            bool tcActiva,
            decimal limActivacion,
            decimal limDepMin,

            decimal limDepMax,
            decimal limDepDia,
            decimal limDepSem,
            decimal limDepMes,

            decimal limRetMin,
            decimal limRetMax,
            decimal limRetDia,
            decimal limRetSem,

            decimal limRetMes,
            decimal limTransMin,
            decimal limTransMax,
            decimal limTransDia,

            decimal limTransSem,
            decimal limTransMes,
            string titulo,
            string datNombre,

            string datPaterno,
            string datMaterno,
            int genero,
            DateTime datNacimiento,

            string datDir1,
            string datDir2,
            string datColonia,
            int paisID,

            int edoID,
            int cdID,
            string datCP,
            string datTelefono,

            string datFax,
            string datCelular,
            string datNextel,
            string datBlackPin,

            int? identificacionID,
            string identificacionInfo,
            bool identificacionValida,
            string email,

            string password,

            DateTime? fechaAlta,
            DateTime? fechaBaja,
            Decimal? creditoDisponible,
            Decimal? creditoPorPagar
    )
        {
            SqlParameter[] parameters = {
                new SqlParameter(Param_tarjetaID,tarjetaID),

                new SqlParameter(Param_productoID,productoID),
                new SqlParameter(Param_monedaID,monedaID),
                new SqlParameter(Param_tcNum,tcNum),
                new SqlParameter(Param_tcMD5,tcMD5),

                new SqlParameter(Param_tcMes,tcMes),
                new SqlParameter(Param_tcAnio,tcAnio),
                new SqlParameter(Param_tcDigitos,tcDigitos),
                new SqlParameter(Param_tcNIP,tcNIP),

                new SqlParameter(Param_tcAsignada,tcAsignada),
                new SqlParameter(Param_tcActiva,tcActiva),
                new SqlParameter(Param_limActivacion,limActivacion),
                new SqlParameter(Param_limDepMin,limDepMin),

                new SqlParameter(Param_limDepMax,limDepMax),
                new SqlParameter(Param_limDepDia,limDepDia),
                new SqlParameter(Param_limDepSem,limDepSem),
                new SqlParameter(Param_limDepMes,limDepMes),

                new SqlParameter(Param_limRetMin,limRetMin),
                new SqlParameter(Param_limRetMax,limRetMax),
                new SqlParameter(Param_limRetDia,limRetDia),
                new SqlParameter(Param_limRetSem,limRetSem),

                new SqlParameter(Param_limRetMes,limRetMes),
                new SqlParameter(Param_limTransMin,limTransMin),
                new SqlParameter(Param_limTransMax,limTransMax),
                new SqlParameter(Param_limTransDia,limTransDia),

                new SqlParameter(Param_limTransSem,limTransSem),
                new SqlParameter(Param_limTransMes,limTransMes),
                new SqlParameter(Param_titulo,titulo),
                new SqlParameter(Param_datNombre,datNombre),

                new SqlParameter(Param_datPaterno,datPaterno),
                new SqlParameter(Param_datMaterno,datMaterno),
                new SqlParameter(Param_genero,genero),
                new SqlParameter(Param_datNacimiento,(datNacimiento == DateTime.MinValue ? DateTime.Parse("1754-01-01"): datNacimiento)),

                new SqlParameter(Param_datDir1,datDir1),
                new SqlParameter(Param_datDir2,datDir2),
                new SqlParameter(Param_datColonia,datColonia),
                new SqlParameter(Param_paisID,paisID),

                new SqlParameter(Param_edoID,edoID),
                new SqlParameter(Param_cdID,cdID),
                new SqlParameter(Param_datCP,datCP),
                new SqlParameter(Param_datTelefono,datTelefono),

                new SqlParameter(Param_datFax,datFax),
                new SqlParameter(Param_datCelular,datCelular),
                new SqlParameter(Param_datNextel,datNextel),
                new SqlParameter(Param_datBlackPin,datBlackPin),

                new SqlParameter(Param_identificacionID,identificacionID),
                new SqlParameter(Param_identificacionInfo,identificacionInfo),
                new SqlParameter(Param_identificacionValida,identificacionValida),
                new SqlParameter(Param_email,email),

                new SqlParameter(Param_password,password),

                new SqlParameter(Param_fechaAlta,fechaAlta),
                new SqlParameter(Param_fechaBaja,fechaBaja),
                new SqlParameter(Param_creditoDisponible,creditoDisponible),
                new SqlParameter(Param_creditoPorPagar,creditoPorPagar)

            };

            DataTable operation_result = new DataTable();
            operation_result = DataBaseConnector.ExecuteStoreProcedure(SaveStoreProcedureName, parameters);

            return Int32.Parse(operation_result.Rows[0].ItemArray[0].ToString());
        }

        public static Int32 Save(
            EnpadiCardModel item
)
        {
            return Save(
                item.tarjetaID,

                item.productoID,
                item.monedaID,
                item.tcNum,
                item.tcMD5,

                item.tcMes,
                item.tcAnio,
                item.tcDigitos,
                item.tcNIP,

                item.tcAsignada,
                item.tcActiva,
                item.limActivacion,
                item.limDepMin,

                item.limDepMax,
                item.limDepDia,
                item.limDepSem,
                item.limDepMes,

                item.limRetMin,
                item.limRetMax,
                item.limRetDia,
                item.limRetSem,

                item.limRetMes,
                item.limTransMin,
                item.limTransMax,
                item.limTransDia,

                item.limTransSem,
                item.limTransMes,
                item.titulo,
                item.datNombre,

                item.datPaterno,
                item.datMaterno,
                item.genero,
                item.datNacimiento,

                item.datDir1,
                item.datDir2,
                item.datColonia,
                item.paisID,

                item.edoID,
                item.cdID,
                item.datCP,
                item.datTelefono,

                item.datFax,
                item.datCelular,
                item.datNextel,
                item.datBlackPin,

                item.identificacionID,
                item.identificacionInfo,
                item.identificacionValida,
                item.email,

                item.password,

                item.fechaAlta,
                item.fechaBaja,
                item.creditoDisponible,
                item.creditoPorPagar
                );
        }

        public static Int32 Save(
     tarjetas item
)
        {
            return Save(
                item.tarjetaID,

                item.productoID,
                item.monedaID,
                item.tcNum,
                item.tcMD5,

                item.tcMes,
                item.tcAnio,
                item.tcDigitos,
                item.tcNIP,

                item.tcAsignada,
                item.tcActiva,
                item.limActivacion,
                item.limDepMin,

                item.limDepMax,
                item.limDepDia,
                item.limDepSem,
                item.limDepMes,

                item.limRetMin,
                item.limRetMax,
                item.limRetDia,
                item.limRetSem,

                item.limRetMes,
                item.limTransMin,
                item.limTransMax,
                item.limTransDia,

                item.limTransSem,
                item.limTransMes,
                item.titulo,
                item.datNombre,

                item.datPaterno,
                item.datMaterno,
                item.genero,
                item.datNacimiento,

                item.datDir1,
                item.datDir2,
                item.datColonia,
                item.paisID,

                item.edoID,
                item.cdID,
                item.datCP,
                item.datTelefono,

                item.datFax,
                item.datCelular,
                item.datNextel,
                item.datBlackPin,

                item.identificacionID,
                item.identificacionInfo,
                item.identificacionValida,
                item.email,

                item.password,

                item.fechaAlta,
                item.fechaBaja,
                item.creditoDisponible,
                item.creditoPorPagar
                );
        }

        #endregion save

        #region get

        public static EnpadiCardModel Get(Int32 tarjetaID)
        {
            DataTable result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_tarjetaID, tarjetaID)
            };

            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName, parameters);

            return PorcessResult(result).FirstOrDefault();
        }

        public static List<EnpadiCardModel> GetAll()
        {
            DataTable result = new DataTable();
            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName);

            return PorcessResult(result);
        }

        #endregion get

        #region datatable to model

        private static List<EnpadiCardModel> PorcessResult(DataTable items)
        {

            List<EnpadiCardModel> result_items = new List<EnpadiCardModel>();

            foreach (DataRow row in items.Rows)
            {
                result_items.Add(new EnpadiCardModel
                {
                    tarjetaID = ToolsBO.ProcessResultInt(Member_tarjetaID , row),

                    productoID = ToolsBO.ProcessResultInt(Member_productoID, row),
                    monedaID = ToolsBO.ProcessResultInt(Member_monedaID, row),
                    tcNum = ToolsBO.ProcessResultString(Member_tcNum, row),
                    tcMD5 = ToolsBO.ProcessResultString(Member_tcMD5, row),

                    tcMes = ToolsBO.ProcessResultString(Member_tcMes, row),
                    tcAnio = ToolsBO.ProcessResultString(Member_tcAnio, row),
                    tcDigitos = ToolsBO.ProcessResultString(Member_tcDigitos, row),
                    tcNIP = ToolsBO.ProcessResultString(Member_tcNIP, row),

                    tcAsignada = ToolsBO.ProcessResultBoolean(Member_tcAsignada, row),
                    tcActiva = ToolsBO.ProcessResultBoolean(Member_tcActiva, row),
                    limActivacion = ToolsBO.ProcessResultDecimal(Member_limActivacion, row),
                    limDepMin = ToolsBO.ProcessResultDecimal(Member_limDepMin, row),

                    limDepMax = ToolsBO.ProcessResultDecimal(Member_limDepMax, row),
                    limDepDia = ToolsBO.ProcessResultDecimal(Member_limDepDia, row),
                    limDepSem = ToolsBO.ProcessResultDecimal(Member_limDepSem, row),
                    limDepMes = ToolsBO.ProcessResultDecimal(Member_limDepMes, row),

                    limRetMin = ToolsBO.ProcessResultDecimal(Member_limRetMin, row),
                    limRetMax = ToolsBO.ProcessResultDecimal(Member_limRetMax, row),
                    limRetDia = ToolsBO.ProcessResultDecimal(Member_limRetDia, row),
                    limRetSem = ToolsBO.ProcessResultDecimal(Member_limRetSem, row),

                    limRetMes = ToolsBO.ProcessResultDecimal(Member_limRetMes, row),
                    limTransMin = ToolsBO.ProcessResultDecimal(Member_limTransMin, row),
                    limTransMax = ToolsBO.ProcessResultDecimal(Member_limTransMax, row),
                    limTransDia = ToolsBO.ProcessResultDecimal(Member_limTransDia, row),

                    limTransSem = ToolsBO.ProcessResultDecimal(Member_limTransSem, row),
                    limTransMes = ToolsBO.ProcessResultDecimal(Member_limTransMes, row),
                    titulo = ToolsBO.ProcessResultString(Member_titulo, row),
                    datNombre = ToolsBO.ProcessResultString(Member_datNombre, row),

                    datPaterno = ToolsBO.ProcessResultString(Member_datPaterno, row),
                    datMaterno = ToolsBO.ProcessResultString(Member_datMaterno, row),
                    genero = ToolsBO.ProcessResultInt(Member_genero, row),
                    datNacimiento = ToolsBO.ProcessResultDateTime(Member_datNacimiento, row),

                    datDir1 = ToolsBO.ProcessResultString(Member_datDir1, row),
                    datDir2 = ToolsBO.ProcessResultString(Member_datDir2, row),
                    datColonia = ToolsBO.ProcessResultString(Member_datColonia, row),
                    paisID = ToolsBO.ProcessResultInt(Member_paisID, row),

                    edoID = ToolsBO.ProcessResultInt(Member_edoID, row),
                    cdID = ToolsBO.ProcessResultInt(Member_cdID, row),
                    datCP = ToolsBO.ProcessResultString(Member_datCP, row),
                    datTelefono = ToolsBO.ProcessResultString(Member_datTelefono, row),

                    datFax = ToolsBO.ProcessResultString(Member_datFax, row),
                    datCelular = ToolsBO.ProcessResultString(Member_datCelular, row),
                    datNextel = ToolsBO.ProcessResultString(Member_datNextel, row),
                    datBlackPin = ToolsBO.ProcessResultString(Member_datBlackPin, row),

                    identificacionID = ToolsBO.ProcessResultInt(Member_identificacionID, row),
                    identificacionInfo = ToolsBO.ProcessResultString(Member_identificacionInfo, row),
                    identificacionValida = ToolsBO.ProcessResultBoolean(Member_identificacionValida, row),
                    email = ToolsBO.ProcessResultString(Member_email, row),

                    password = ToolsBO.ProcessResultString(Member_password , row),

                    fechaAlta = ToolsBO.ProcessResultDateTime(Member_fechaAlta, row),
                    fechaBaja = ToolsBO.ProcessResultDateTime(Member_fechaBaja, row),
                    creditoDisponible = ToolsBO.ProcessResultDecimal(Member_creditoDisponible, row),
                    creditoPorPagar = ToolsBO.ProcessResultDecimal(Member_creditoPorPagar, row)
                });
            }

            return result_items;
        }


        #endregion datatable to model

        #endregion methods
    }
}