﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Enpadi_WebUI.Models;

namespace Enpadi_WebUI.DAO
{
    public class OxxoResponse
    {
        #region sp names

        private const String SaveStoreProcedureName = "OxxoResponseInsert";
        private const String GetStoreProcedureName = "";


        #endregion sp names

        #region constants

        #region parameters

        private const String Param_data_object_id = "@data_object_id";
        private const String Param_data_object_livemode = "@data_object_livemode";
        private const String Param_data_object_created_at = "@data_object_created_at";
        private const String Param_data_object_currency = "@data_object_currency";
        private const String Param_data_object_amount = "@data_object_amount";
        private const String Param_data_object_payment_status = "@data_object_payment_status";

        private const String Param_customer_email = "@customer_email";
        private const String Param_customer_phone = "@customer_phone";
        private const String Param_customer_name = "@customer_name";

        private const String Param_line_item_price = "@line_item_price";
        private const String Param_line_item_id = "@line_item_id";
        private const String Param_line_item_parent_id = "@line_item_parent_id";

        private const String Param_charges_data_id = "@charges_data_id";
        private const String Param_charges_data_livemode = "@charges_data_livemode";
        private const String Param_charges_data_created_at = "@charges_data_created_at";
        private const String Param_charges_data_currency = "@charges_data_currency";
        private const String Param_charges_data_description = "@charges_data_description";
        private const String Param_charges_data_status = "@charges_data_status";
        private const String Param_charges_data_amount = "@charges_data_amount";
        private const String Param_charges_data_pay_at = "@charges_data_pay_at";
        private const String Param_charges_data_fee = "@charges_data_fee";
        private const String Param_charges_data_order_id = "@charges_data_order_id";

        private const String Param_payment_method_service_name = "@payment_method_service_name";
        private const String Param_payment_method_type = "@payment_method_type";
        private const String Param_payment_method_expires_at = "@payment_method_expires_at";
        private const String Param_payment_method_reference = "@payment_method_reference";

        private const String Param_livemode = "@livemode";
        private const String Param_id = "@id";
        private const String Param_type = "@type";
        private const String Param_received_locally_at = "@received_locally_at";

        #endregion parameters

        #region members

        private const String Member_data_object_id = "data_object_id";
        private const String Member_data_object_livemode = "data_object_livemode";
        private const String Member_data_object_created_at = "data_object_created_at";
        private const String Member_data_object_currency = "data_object_currency";
        private const String Member_data_object_amount = "data_object_amount";
        private const String Member_data_object_payment_status = "data_object_payment_status";

        private const String Member_customer_email = "customer_email";
        private const String Member_customer_phone = "customer_phone";
        private const String Member_customer_name = "customer_name";

        private const String Member_line_item_price = "line_item_price";
        private const String Member_line_item_id = "line_item_id";
        private const String Member_line_item_parent_id = "line_item_parent_id";

        private const String Member_charges_data_id = "charges_data_id";
        private const String Member_charges_data_livemode = "charges_data_livemode";
        private const String Member_charges_data_created_at = "charges_data_created_at";
        private const String Member_charges_data_currency = "charges_data_currency";
        private const String Member_charges_data_description = "charges_data_description";
        private const String Member_charges_data_status = "charges_data_status";
        private const String Member_charges_data_amount = "charges_data_amount";
        private const String Member_charges_data_pay_at = "charges_data_pay_at";
        private const String Member_charges_data_fee = "charges_data_fee";
        private const String Member_charges_data_order_id = "charges_data_order_id";

        private const String Member_payment_method_service_name = "payment_method_service_name";
        private const String Member_payment_method_type = "payment_method_type";
        private const String Member_payment_method_expires_at = "payment_method_expires_at";
        private const String Member_payment_method_reference = "payment_method_reference";

        private const String Member_livemode = "livemode";
        private const String Member_id = "id";
        private const String Member_type = "type";
        private const String Member_received_locally_at = "received_locally_at";


        #endregion members

        #endregion constants

        #region methods

        #region save

        public static void Save(
                String data_object_id,
                String data_object_livemode,
                String data_object_created_at,
                String data_object_currency,
                String data_object_amount,
                String data_object_payment_status,

                String customer_email,
                String customer_phone,
                String customer_name,

                String line_item_price,
                String line_item_id,
                String line_item_parent_id,

                String charges_data_id,
                String charges_data_livemode,
                String charges_data_created_at,
                String charges_data_currency,
                String charges_data_description,
                String charges_data_status,
                String charges_data_amount,
                String charges_data_pay_at,
                String charges_data_fee,
                String charges_data_order_id,

                String payment_method_service_name,
                String payment_method_type,
                String payment_method_expires_at,
                String payment_method_reference,

                String livemode,
                String id,
                String type,
                DateTime received_locally_at
            )
        {
            SqlParameter[] parameters = {
                new SqlParameter(Param_data_object_id,data_object_id),
                new SqlParameter(Param_data_object_livemode,data_object_livemode),
                new SqlParameter(Param_data_object_created_at,data_object_created_at),
                new SqlParameter(Param_data_object_currency,data_object_currency),
                new SqlParameter(Param_data_object_amount,data_object_amount),
                new SqlParameter(Param_data_object_payment_status,data_object_payment_status),

                new SqlParameter(Param_customer_email,customer_email),
                new SqlParameter(Param_customer_phone,customer_phone),
                new SqlParameter(Param_customer_name,customer_name),

                new SqlParameter(Param_line_item_price,line_item_price),
                new SqlParameter(Param_line_item_id,line_item_id),
                new SqlParameter(Param_line_item_parent_id,line_item_parent_id),

                new SqlParameter(Param_charges_data_id,charges_data_id),
                new SqlParameter(Param_charges_data_livemode,charges_data_livemode),
                new SqlParameter(Param_charges_data_created_at,charges_data_created_at),
                new SqlParameter(Param_charges_data_currency,charges_data_currency),
                new SqlParameter(Param_charges_data_description,charges_data_description),
                new SqlParameter(Param_charges_data_status,charges_data_status),
                new SqlParameter(Param_charges_data_amount,charges_data_amount),
                new SqlParameter(Param_charges_data_pay_at,charges_data_pay_at),
                new SqlParameter(Param_charges_data_fee,charges_data_fee),
                new SqlParameter(Param_charges_data_order_id,charges_data_order_id),


                new SqlParameter(Param_payment_method_service_name,payment_method_service_name),
                new SqlParameter(Param_payment_method_type,payment_method_type),
                new SqlParameter(Param_payment_method_expires_at,payment_method_expires_at),
                new SqlParameter(Param_payment_method_reference,payment_method_reference),

                new SqlParameter(Param_livemode,livemode),
                new SqlParameter(Param_id,id),
                new SqlParameter(Param_type,type),
                new SqlParameter(Param_received_locally_at,received_locally_at)
            };

            DataBaseConnector.ExecuteStoreProcedure(SaveStoreProcedureName, parameters);
        }

        public static void Save(
            OxxoResponseModel response
         )
        {
            Save(
                response.data_object_id,
                response.data_object_livemode,
                response.data_object_created_at,
                response.data_object_currency,
                response.data_object_amount,
                response.data_object_payment_status,

                response.customer_email,
                response.customer_phone,
                response.customer_name,

                response.line_item_price,
                response.line_item_id,
                response.line_item_parent_id,

                response.charges_data_id,
                response.charges_data_livemode,
                response.charges_data_created_at,
                response.charges_data_currency,
                response.charges_data_description,
                response.charges_data_status,
                response.charges_data_amount,
                response.charges_data_paid_at,
                response.charges_data_fee,
                response.charges_data_order_id,


                response.payment_method_service_name,
                response.payment_method_type,
                response.payment_method_expires_at,
                response.payment_method_reference,

                response.livemode,
                response.id,
                response.type,
                response.received_locally_at
                );
        }

        #endregion save

        #region get



        #endregion get

        #endregion methods

    }
}