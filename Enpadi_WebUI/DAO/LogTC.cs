﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enpadi_WebUI.Models;
using System.Data.SqlClient;
using Enpadi_WebUI.Models;
using System.Data;

namespace Enpadi_WebUI.DAO
{
    public class LogTC
    {

        #region constants


        #region sp names

        private const String SaveStoreProcedureName = "LogTCInsert";
        private const String GetStoreProcedureName = "LogTCGet";


        #endregion sp names

        #region parameters

        private const String Param_idLog = "@idLog";
        private const String Param_idTarjeta = "@idTarjeta";
        private const String Param_result = "@result";
        private const String Param_fechaAlta = "@fechaAlta";

        private const String Param_fechaBaja = "@fechaBaja";

        #endregion parameters


        #region parameters

        private const String Member_idLog = "idLog";
        private const String Member_idTarjeta = "idTarjeta";
        private const String Member_result = "result";
        private const String Member_fechaAlta = "fechaAlta";

        private const String Member_fechaBaja = "fechaBaja";
        
        #endregion parameters

        #endregion constants

        #region methods

        #region save

        public static Int32 Save(
            Int32? idLog,
            Int32? idTarjeta,
            Int32? result,
            DateTime fechaAlta,

            DateTime? fechaBaja
        )
        {
            DataTable operation_result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_idLog, idLog),
                new SqlParameter(Param_idTarjeta,idTarjeta),
                new SqlParameter(Param_result,result),
                new SqlParameter(Param_fechaAlta,fechaAlta),

                new SqlParameter(Param_fechaBaja,fechaBaja)

            };

            operation_result = DataBaseConnector.ExecuteStoreProcedure(SaveStoreProcedureName, parameters);

            return Int32.Parse(operation_result.Rows[0].ItemArray[0].ToString());
        }

        public static LogTCModel Save(
             LogTCModel item
        )
        {
            item.idLog = Save(
                item.idLog,
                item.idTarjeta,
                item.result,
                item.fechaAlta,
                item.fechaBaja
             );

            return item;
        }

        public static void Save(
            List<LogTCModel> items
        )
        {
            foreach (LogTCModel item in items)
            {
                Save(item);
            }
        }

        #endregion save

        #region get

        public static LogTCModel Get(Int32 id)
        {
            DataTable result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_idLog, id)

            };

            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName, parameters);

            return PorcessResult(result).FirstOrDefault();
        }

        public static List<LogTCModel> GetAll()
        {
            DataTable result = new DataTable();
            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName);

            return PorcessResult(result);
        }

        #endregion get

        #region datatable to model

        private static List<LogTCModel> PorcessResult(DataTable items)
        {

            List<LogTCModel> result_items = new List<LogTCModel>();

            foreach (DataRow row in items.Rows)
            {
                result_items.Add(new LogTCModel
                {
                    idLog = ToolsBO.ProcessResultInt(Member_idLog, row),
                    idTarjeta = ToolsBO.ProcessResultInt(Member_idTarjeta, row),
                    result = ToolsBO.ProcessResultInt(Member_result, row),
                    fechaAlta = ToolsBO.ProcessResultDateTime(Member_fechaAlta, row),
                    fechaBaja = ToolsBO.ProcessResultDateTime(Member_fechaBaja, row)
                });
            }

            return result_items;
        }


        #endregion datatable to model


        #endregion methods
    }
}