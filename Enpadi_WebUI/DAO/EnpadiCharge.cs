﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Enpadi_WebUI.Models;
using System.Data;

namespace Enpadi_WebUI.DAO
{
    public class EnpadiCharge
    {
        #region constants

        #region sp name

        private const String SaveStoreProcedureName = "EnpadiChargeInsert";
        private const String GetStoreProcedureName = "EnpadiChargeGet";

        #endregion sp name

        #region parameters
        private const String Param_id = "@id";

        private const String Param_livemode = "@livemode";
        private const String Param_amount = "@amount";
        private const String Param_currency = "@currency";
        private const String Param_object_description = "@object_description";

        private const String Param_fee = "@fee";
        private const String Param_customer_id = "@customer_id";
        private const String Param_order_id = "@order_id";
        private const String Param_customer_email = "@customer_email";

        private const String Param_service_name = "@service_name";
        private const String Param_pay_met_obj_desc = "@pay_met_obj_desc";
        private const String Param_payment_method_type = "@payment_method_type";
        private const String Param_charge_type = "@charge_type";
        private const String Param_expires_at = "@expires_at";
        private const String Param_reference = "@reference";

        private const String Param_name_on_card = "@name_on_card";
        private const String Param_credit_card_type = "@credit_card_type";
        private const String Param_card_number = "@card_number";
        private const String Param_credit_card_expiration_month = "@credit_card_expiration_month";
        private const String Param_credit_card_expiration_year = "@credit_card_expiration_year";
        private const String Param_credit_card_security_code = "@credit_card_security_code";
        private const String Param_credit_card_maestro_switch = "@credit_card_maestro_switch";

        #endregion parameters

        #region members

        private const String Member_id = "id";

        private const String Member_livemode = "livemode";
        private const String Member_amount = "amount";
        private const String Member_currency = "currency";
        private const String Member_object_description = "object_description";

        private const String Member_fee = "fee";
        private const String Member_customer_id = "customer_id";
        private const String Member_order_id = "order_id";
        private const String Member_customer_email = "customer_email";

        private const String Member_service_name = "service_name";
        private const String Member_pay_met_obj_desc = "pay_met_obj_desc";
        private const String Member_payment_method_type = "payment_method_type";
        private const String Member_charge_type = "charge_type";
        private const String Member_expires_at = "expires_at";
        private const String Member_reference = "reference";

        private const String Member_name_on_card = "name_on_card";
        private const String Member_credit_card_type = "credit_card_type";
        private const String Member_card_number = "card_number";
        private const String Member_credit_card_expiration_month = "credit_card_expiration_month";
        private const String Member_credit_card_expiration_year = "credit_card_expiration_year";
        private const String Member_credit_card_security_code = "credit_card_security_code";
        private const String Member_credit_card_maestro_switch = "credit_card_maestro_switch";

        #endregion members

        #endregion constants

        #region methods

        #region save

        public static void Save(
            Guid id,
            Boolean livemode,
            Decimal amount,
            String currency,
            String object_description,

            Decimal fee,
            String customer_id,
            String order_id,
            String customer_email,

            String service_name,
            String pay_met_obj_desc,
            String payment_method_type,
            String expires_at,
            String reference,
            String payment_charge_type
    )
        {
            SqlParameter[] parameters = {
                new SqlParameter(Param_id,id),
                new SqlParameter(Param_livemode,livemode),
                new SqlParameter(Param_amount,amount),
                new SqlParameter(Param_currency,currency),

                new SqlParameter(Param_fee,fee),
                new SqlParameter(Param_customer_id,customer_id),
                new SqlParameter(Param_order_id,order_id),
                new SqlParameter(Param_customer_email,customer_email),


                new SqlParameter(Param_object_description,object_description),
                new SqlParameter(Param_service_name,service_name),
                new SqlParameter(Param_pay_met_obj_desc,pay_met_obj_desc),
                new SqlParameter(Param_payment_method_type,payment_method_type),
                new SqlParameter(Param_expires_at,expires_at),
                new SqlParameter(Param_reference,reference),

                new SqlParameter(Param_charge_type,payment_charge_type)

            };

            DataBaseConnector.ExecuteStoreProcedure(SaveStoreProcedureName, parameters);
        }

        public static void Save(
            EnpadiChargeModel item
)
        {
            SqlParameter[] parameters = {
                new SqlParameter(Param_id,item.data_object.id),
                new SqlParameter(Param_livemode, item.data_object.livemode),
                new SqlParameter(Param_amount,item.data_object.amount),
                new SqlParameter(Param_currency,item.data_object.currency),

                new SqlParameter(Param_fee,item.data_object.fee),
                new SqlParameter(Param_customer_id,item.data_object.customer_id),
                new SqlParameter(Param_order_id,item.data_object.order_id),
                new SqlParameter(Param_customer_email,item.data_object.customer_email),


                new SqlParameter(Param_object_description, (item.data_object.object_description ==null ? "Pending" : item.data_object.object_description)),
                new SqlParameter(Param_service_name, (item.data_object.payment_method.service_name ==null ? "Pending" : item.data_object.payment_method.service_name )),
                new SqlParameter(Param_pay_met_obj_desc, (item.data_object.payment_method.object_description) ==null ? "Pending" : item.data_object.payment_method.object_description),
                new SqlParameter(Param_payment_method_type, (item.data_object.payment_method.payment_method_type ==null ? "Pending" : item.data_object.payment_method.payment_method_type)),
                new SqlParameter(Param_expires_at, (item.data_object.payment_method.expires_at ==null ? "Pending" : item.data_object.payment_method.expires_at)),
                new SqlParameter(Param_reference, (item.data_object.payment_method.reference ==null ? "Pending" : item.data_object.payment_method.reference)),

                new SqlParameter(Param_charge_type,item.data_object.charge_type)

            };

            DataBaseConnector.ExecuteStoreProcedure(SaveStoreProcedureName, parameters);
        }

        /// <summary>
        /// If this method is used the payment method class needs to be the one inside the charge, ths 2 objects are part of the same Enpadi Charge
        /// </summary>
        /// <param name="data"></param>
        /// <param name="payment_method"></param>
        //public static void Save(
        //    EnpadiChargeModel.data data,
        //    EnpadiChargeModel.PaymentMethod payment_method
        //)
        //{
        //    SqlParameter[] parameters = {
        //        new SqlParameter(Param_livemode, data.livemode),
        //        new SqlParameter(Param_amount, data.amount),
        //        new SqlParameter(Param_currency, data.currency),
        //        new SqlParameter(Param_object_description, data.object_description),

        //        new SqlParameter(Param_fee,data.fee),
        //        new SqlParameter(Param_customer_id,data.customer_id),
        //        new SqlParameter(Param_order_id,data.order_id),
        //        new SqlParameter(Param_customer_email,data.customer_email),

        //        new SqlParameter(Param_service_name,payment_method.service_name),
        //        new SqlParameter(Param_pay_met_obj_desc,payment_method.object_description),
        //        new SqlParameter(Param_type,payment_method.type),
        //        new SqlParameter(Param_expires_at,payment_method.expires_at),
        //        new SqlParameter(Param_reference,payment_method.reference),

        //    };

        //    DataBaseConnector.ExecuteStoreProcedure(SaveStoreProcedureName, parameters);
        //}

        #endregion save

        #region get

        public static EnpadiChargeModel Get(Guid id)
        {
            DataTable result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_id, id)

            };

            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName, parameters);

            return PorcessResult(result).FirstOrDefault();
        }

        public static List<EnpadiChargeModel> GetAll()
        {
            DataTable result = new DataTable();
            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName);

            return PorcessResult(result);
        }

        #endregion get

        #region datatable to model

        private static List<EnpadiChargeModel> PorcessResult(DataTable items)
        {

            List<EnpadiChargeModel> result_items = new List<EnpadiChargeModel>();

            foreach (DataRow row in items.Rows)
            {
                result_items.Add(new EnpadiChargeModel
                {
                    data_object = new EnpadiChargeModel.data
                    {
                        id = ToolsBO.ProcessResultGuid(Member_id, row),
                        livemode = ToolsBO.ProcessResultBoolean(Member_livemode, row),
                        amount = ToolsBO.ProcessResultDecimal(Member_amount, row),
                        currency = ToolsBO.ProcessResultString(Member_currency, row),
                        object_description = ToolsBO.ProcessResultString(Member_object_description, row),
                        fee = ToolsBO.ProcessResultDecimal(Member_fee, row),
                        customer_id = ToolsBO.ProcessResultString(Member_customer_id, row),
                        customer_email = ToolsBO.ProcessResultString(Member_customer_email, row),
                        order_id = ToolsBO.ProcessResultGuid(Member_order_id, row),
                        charge_type = ToolsBO.ProcessResultString(Member_charge_type, row),
                        payment_method = new EnpadiChargeModel.PaymentMethod
                        {
                            service_name = ToolsBO.ProcessResultString(Member_service_name, row),
                            object_description = ToolsBO.ProcessResultString(Member_pay_met_obj_desc, row),
                            payment_method_type = ToolsBO.ProcessResultString(Member_payment_method_type, row),
                            expires_at = ToolsBO.ProcessResultString(Member_expires_at, row),
                            reference = ToolsBO.ProcessResultString(Member_reference, row),
                            card_number = ToolsBO.ProcessResultString(Member_card_number, row),
                            credit_card_type = ToolsBO.ProcessResultInt(Member_charge_type, row),
                            name_on_card = ToolsBO.ProcessResultString(Member_name_on_card, row),
                            credit_card_expiration_month = ToolsBO.ProcessResultInt(Member_credit_card_expiration_month, row),
                            credit_card_expiration_year = ToolsBO.ProcessResultInt(Member_credit_card_expiration_year, row),
                            credit_card_security_code = ToolsBO.ProcessResultInt(Member_credit_card_security_code, row),
                            credit_card_maestro_switch = ToolsBO.ProcessResultInt(Member_credit_card_maestro_switch, row)
                        }
                    },

                });
            }

            return result_items;
        }


        #endregion datatable to model

        #endregion methods
    }
}