﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Enpadi_WebUI.Models;

namespace Enpadi_WebUI.DAO
{
    public class PagoBancos
    {
        #region constants

        #region sp names

        private const String SaveStoreProcedureName = "PagoBancosInsert";
        private const String GetStoreProcedureName = "PagoBancosGet";

        #endregion sp names

        #region parameters

        private const String Param_pagoId = "@pagoId";
        private const String Param_id = "@id";
        private const String Param_description = "@description";
        private const String Param_error_message = "@error_message";
        private const String Param_authorization = "@authorization";
        private const String Param_amount = "@amount";
        private const String Param_operation_type = "@operation_type";
        private const String Param_payment_method = "@payment_method";
        private const String Param_clabe = "@clabe";
        private const String Param_bank = "@bank";
        private const String Param_type = "@type";
        private const String Param_name = "@name";
        private const String Param_order_id = "@order_id";
        private const String Param_transaction_type = "@transaction_type";
        private const String Param_creation_date = "@creation_date";
        private const String Param_currency = "@currency";
        private const String Param_status = "@status";
        private const String Param_method = "@method";

        #endregion parameters

        #region parameters

        private const String Member_pagoId = "pagoId";
        private const String Member_id = "id";
        private const String Member_description = "description";
        private const String Member_error_message = "error_message";
        private const String Member_authorization = "authorization";
        private const String Member_amount = "amount";
        private const String Member_operation_type = "operation_type";
        private const String Member_payment_method = "payment_method";
        private const String Member_clabe = "clabe";
        private const String Member_bank = "bank";
        private const String Member_type = "type";
        private const String Member_name = "name";
        private const String Member_order_id = "order_id";
        private const String Member_transaction_type = "transaction_type";
        private const String Member_creation_date = "creation_date";
        private const String Member_currency = "currency";
        private const String Member_status = "status";
        private const String Member_method = "method";

        #endregion parameters

        #endregion constants

        #region methods

        #region save

        public static Int32 Save(
         int pagoId,
         string id,
         string description,
         string error_message,

         string authorization,
         decimal amount,
         string operation_type,
         string payment_method,

         string clabe,
         string bank,
         string type,
         string name,

         string order_id,
         string transaction_type,
         string creation_date,
         string currency,

         string status,
         string method
)
        {
            DataTable operation_result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_pagoId, pagoId),
                new SqlParameter(Param_id,id),
                new SqlParameter(Param_description,description),
                new SqlParameter(Param_error_message,error_message),

                new SqlParameter(Param_authorization, authorization),
                new SqlParameter(Param_amount,amount),
                new SqlParameter(Param_operation_type,operation_type),
                new SqlParameter(Param_payment_method,payment_method),

                new SqlParameter(Param_clabe, clabe),
                new SqlParameter(Param_bank,bank),
                new SqlParameter(Param_type,type),
                new SqlParameter(Param_name,name),

                new SqlParameter(Param_order_id, order_id),
                new SqlParameter(Param_transaction_type,transaction_type),
                new SqlParameter(Param_creation_date,creation_date),
                new SqlParameter(Param_currency,currency),

                new SqlParameter(Param_status, status),
                new SqlParameter(Param_method, method)

            };

            operation_result = DataBaseConnector.ExecuteStoreProcedure(SaveStoreProcedureName, parameters);

            return Int32.Parse(operation_result.Rows[0].ItemArray[0].ToString());
        }

        public static PagoBancosModel Save(
             PagoBancosModel item
        )
        {
            item.pagoId = Save(
        item.pagoId,
            item.id,
            item.description,
            item.error_message,
            item.authorization,
            item.amount,
            item.operation_type,
            item.payment_method,
            item.clabe,
            item.bank,
            item.type,
            item.name,
            item.order_id,
            item.transaction_type,
            item.creation_date,
            item.currency,
            item.status,
            item.method
             );

            return item;
        }

        public static pagoBanco Save(
           pagoBanco item
       )
        {
            item.pagoId = Save(
            item.pagoId,
            item.id,
            item.description,
            item.error_message,
            item.authorization,
            item.amount,
            item.operation_type,
            item.payment_method,
            item.clabe,
            item.bank,
            item.type,
            item.name,
            item.order_id,
            item.transaction_type,
            item.creation_date,
            item.currency,
            item.status,
            item.method
             );

            return item;
        }

        public static void Save(
            List<PagoBancosModel> items
        )
        {
            foreach (PagoBancosModel item in items)
            {
                Save(item);
            }
        }

        #endregion save

        #region get

        public static PagoBancosModel Get(Int32 id)
        {
            DataTable result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_pagoId, id)

            };

            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName, parameters);

            return PorcessResult(result).FirstOrDefault();
        }

        public static List<PagoBancosModel> GetAll()
        {
            DataTable result = new DataTable();
            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName);

            return PorcessResult(result);
        }

        #endregion get

        #region datatable to model

        private static List<PagoBancosModel> PorcessResult(DataTable items)
        {

            List<PagoBancosModel> result_items = new List<PagoBancosModel>();

            foreach (DataRow row in items.Rows)
            {
                result_items.Add(new PagoBancosModel
                {
                    pagoId = ToolsBO.ProcessResultInt(Member_pagoId, row),
                    id = ToolsBO.ProcessResultString(Member_id, row),
                    description = ToolsBO.ProcessResultString(Member_description, row),
                    error_message = ToolsBO.ProcessResultString(Member_error_message, row),
                    authorization = ToolsBO.ProcessResultString(Member_authorization, row),
                    amount = ToolsBO.ProcessResultDecimal(Member_amount, row),
                    operation_type = ToolsBO.ProcessResultString(Member_operation_type, row),
                    payment_method = ToolsBO.ProcessResultString(Member_payment_method, row),
                    clabe = ToolsBO.ProcessResultString(Member_clabe, row),
                    bank = ToolsBO.ProcessResultString(Member_bank, row),
                    type = ToolsBO.ProcessResultString(Member_type, row),
                    name = ToolsBO.ProcessResultString(Member_name, row),
                    order_id = ToolsBO.ProcessResultString(Member_order_id, row),
                    transaction_type = ToolsBO.ProcessResultString(Member_transaction_type, row),
                    creation_date = ToolsBO.ProcessResultString(Member_creation_date, row),
                    currency = ToolsBO.ProcessResultString(Member_currency, row),
                    status = ToolsBO.ProcessResultString(Member_status, row),
                    method = ToolsBO.ProcessResultString(Member_method, row)
                });
            }

            return result_items;
        }


        #endregion datatable to model

        #endregion methods
    }
}