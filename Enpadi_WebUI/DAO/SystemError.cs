﻿using Enpadi_WebUI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.DAO
{
    //TODO create system error dao
    public class SystemError
    {
        #region constants


        #region sp names

        private const String SaveStoreProcedureName = "SystemErrorInsert";
        private const String GetStoreProcedureName = "SystemErrorGet";


        #endregion sp names

        #region parameters

        private const String Param_Id = "@Id";
        private const String Param_Description = "@Description";
        private const String Param_ErrorNumber = "@ErrorNumber";
        private const String Param_SpName = "@SpName";

        private const String Param_AditionalInfo = "@AditionalInfo";
        private const String Param_FileName = "@FileName";
        private const String Param_ErrorFound = "@ErrorFound";
        private const String Param_DateCreated = "@DateCreated";

        #endregion parameters


        #region parameters

        private const String Member_Id = "Id";
        private const String Member_Description = "Description";
        private const String Member_ErrorNumber = "ErrorNumber";
        private const String Member_SpName = "SpName";

        private const String Member_AditionalInfo = "AditionalInfo";
        private const String Member_FileName = "FileName";
        private const String Member_ErrorFound = "ErrorFound";
        private const String Member_DateCreated = "DateCreated";

        #endregion parameters

        #endregion constants

        #region methods

        #region save

        public static void Save(
            Guid Id,
            String Description,
            String ErrorNumber,
            String SpName,

            String AditionalInfo,
            String FileName,
            Boolean ErrorFound,
            DateTime DateCreated
        )
        {
            DataTable result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_Id, Id),
                new SqlParameter(Param_Description,Description),
                new SqlParameter(Param_ErrorNumber,ErrorNumber),
                new SqlParameter(Param_SpName,SpName),

                new SqlParameter(Param_AditionalInfo,AditionalInfo),
                new SqlParameter(Param_FileName,FileName),
                new SqlParameter(Param_ErrorFound,ErrorFound),
                new SqlParameter(Param_DateCreated,DateCreated),
            };

            result = DataBaseConnector.ExecuteStoreProcedure(SaveStoreProcedureName, parameters);
        }

        public static SystemErrorModel Save(
            SystemErrorModel item
        )
        {
            Save(
                item.Id,
                item.Description,
                item.ErrorNumber,
                item.SpName,

                item.AditionalInfo,
                item.FileName,
                item.ErrorFound,
                item.DateCreated
             );

            return item;
        }


        public static void Save(
            List<SystemErrorModel> items
        )
        {
            foreach (SystemErrorModel item in items)
            {
                Save(item);
            }
        }

        #endregion save

        #region get

        public static SystemErrorModel Get(Guid id)
        {
            DataTable result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_Id, id)

            };

            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName, parameters);

            return PorcessResult(result).FirstOrDefault();
        }

        public static List<SystemErrorModel> GetAll()
        {
            DataTable result = new DataTable();
            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName);

            return PorcessResult(result);
        }

        #endregion get

        #region datatable to model

        private static List<SystemErrorModel> PorcessResult(DataTable items)
        {

            List<SystemErrorModel> result_items = new List<SystemErrorModel>();

            foreach (DataRow row in items.Rows)
            {
                result_items.Add(new SystemErrorModel
                {
                    Id = ToolsBO.ProcessResultGuid(Member_Id, row),
                    Description = ToolsBO.ProcessResultString(Member_Description, row),
                    AditionalInfo = ToolsBO.ProcessResultString(Member_AditionalInfo, row),
                    FileName = ToolsBO.ProcessResultString(Member_FileName, row),

                    ErrorNumber = ToolsBO.ProcessResultString(Member_ErrorNumber, row),
                    ErrorFound = ToolsBO.ProcessResultBoolean(Member_ErrorFound, row),
                    SpName = ToolsBO.ProcessResultString(Member_SpName, row),
                    DateCreated = ToolsBO.ProcessResultDateTime(Member_DateCreated, row),
                    
                });
            }

            return result_items;
        }


        #endregion datatable to model


        #endregion methods
    }
}