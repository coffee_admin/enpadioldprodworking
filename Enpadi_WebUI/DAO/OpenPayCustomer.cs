﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Enpadi_WebUI.Models;
using System.Data;

namespace Enpadi_WebUI.DAO
{
    public class OpenPayCustomer
    {
        #region constants


        #region sp names

        private const String SaveStoreProcedureName = "OpenPayCustomerInsert";
        private const String GetStoreProcedureName = "OpenPayCustomerGet";


        #endregion sp names

        #region parameters

        private const String Param_id = "@id";
        private const String Param_CustomerID = "@CustomerID";
        private const String Param_ExternalID = "@ExternalID";
        private const String Param_Name = "@Name";

        private const String Param_LastName = "@LastName";
        private const String Param_Email = "@Email";
        private const String Param_PhoneNumber = "@PhoneNumber";
        private const String Param_RequiresAccount = "@RequiresAccount";

        private const String Param_Address1 = "@Address1";

        private const String Param_Address2 = "@Address2";
        private const String Param_Address3 = "@Address3";
        private const String Param_City = "@City";
        private const String Param_State = "@State";
        private const String Param_CountryCode = "@CountryCode";
        private const String Param_PostalCode = "@PostalCode";
        private const String Param_CLABE = "@CLABE";
        private const String Param_DateAdd = "@DateAdd";
        private const String Param_DateDelete = "@DateDelete";

        #endregion parameters


        #region parameters

        private const String Member_id = "id";
        private const String Member_CustomerID = "CustomerID";
        private const String Member_ExternalID = "ExternalID";
        private const String Member_Name = "Name";

        private const String Member_LastName = "LastName";
        private const String Member_Email = "Email";
        private const String Member_PhoneNumber = "PhoneNumber";
        private const String Member_RequiresAccount = "RequiresAccount";

        private const String Member_Address1 = "Address1";

        private const String Member_Address2 = "Address2";
        private const String Member_Address3 = "Address3";
        private const String Member_City = "City";
        private const String Member_State = "State";
        private const String Member_CountryCode = "CountryCode";
        private const String Member_PostalCode = "PostalCode";
        private const String Member_CLABE = "CLABE";
        private const String Member_DateAdd = "DateAdd";
        private const String Member_DateDelete = "DateDelete";

        #endregion parameters

        #endregion constants

        #region methods

        #region save

        /// <summary>
        /// returns id of the generated registry
        /// </summary>
        /// <param name="CustomerID"></param>
        /// <param name="ExternalID"></param>
        /// <param name="Name"></param>
        /// <param name="LastName"></param>
        /// <param name="Email"></param>
        /// <param name="PhoneNumber"></param>
        /// <param name="RequiresAccount"></param>
        /// <param name="Address1"></param>
        /// <param name="Address2"></param>
        /// <param name="Address3"></param>
        /// <param name="City"></param>
        /// <param name="State"></param>
        /// <param name="CountryCode"></param>
        /// <param name="PostalCode"></param>
        /// <param name="CLABE"></param>
        /// <param name="DateAdd"></param>
        /// <returns></returns>
        public static Int32 Save(
            String CustomerID,
            Int32 ExternalID,
            String Name,

            String LastName,
            String Email,
            String PhoneNumber,
            Boolean RequiresAccount,

            String Address1,
            String Address2,
            String Address3,
            String City,

            String State,
            String CountryCode,
            String PostalCode,
            String CLABE,

            DateTime DateAdd
        )
        {
            DataTable result = new DataTable();

            SqlParameter[] parameters = {

                new SqlParameter(Param_CustomerID,CustomerID),
                new SqlParameter(Param_ExternalID,ExternalID),
                new SqlParameter(Param_Name,Name),

                new SqlParameter(Param_LastName,LastName),
                new SqlParameter(Param_Email,Email),
                new SqlParameter(Param_PhoneNumber,PhoneNumber),
                new SqlParameter(Param_RequiresAccount,RequiresAccount),

                new SqlParameter(Param_Address1,Address1),
                new SqlParameter(Param_Address2,Address2),
                new SqlParameter(Param_Address3,Address3),
                new SqlParameter(Param_City,City),

                new SqlParameter(Param_State,State),
                new SqlParameter(Param_CountryCode,CountryCode),
                new SqlParameter(Param_PostalCode,PostalCode),
                new SqlParameter(Param_CLABE,CLABE),

                new SqlParameter(Param_DateAdd,DateAdd)

            };

            result = DataBaseConnector.ExecuteStoreProcedure(SaveStoreProcedureName, parameters);

            return Int32.Parse(result.Rows[0].ItemArray[0].ToString());
        }

        /// <summary>
        /// returns the same object with the id set
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static OpenPayCustomerModel Save(
            OpenPayCustomerModel item
        )
        {
            item.id = Save(
                item.CustomerID,
                item.ExternalID,
                item.Name,

                item.LastName,
                item.Email,
                item.PhoneNumber,
                item.RequiresAccount,

                item.Address1,
                item.Address2,
                item.Address3,
                item.City,

                item.State,
                item.CountryCode,
                item.PostalCode,
                item.CLABE,

                item.DateAdd
             );

            return item;
        }

        /// <summary>
        /// returns the same object with the id set
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static openpayCustomer Save(
            openpayCustomer item
        )
        {
            item.id = Save(
                item.CustomerID,
                item.ExternalID,
                item.Name,

                item.LastName,
                item.Email,
                item.PhoneNumber,
                item.RequiresAccount,

                item.Address1,
                item.Address2,
                item.Address3,
                item.City,

                item.State,
                item.CountryCode,
                item.PostalCode,
                item.CLABE,

                DateTime.Now
             );

            return item;
        }

        public static void Save(
            List<OpenPayCustomerModel> items
        )
        {
            foreach (OpenPayCustomerModel item in items)
            {
                Save(item);
            }
        }

        #endregion save

        #region get

        public static OpenPayCustomerModel Get(Int32 id)
        {
            DataTable result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_id, id)

            };

            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName, parameters);

            return PorcessResult(result).FirstOrDefault();
        }

        public static List<OpenPayCustomerModel> GetAll()
        {
            DataTable result = new DataTable();
            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName);

            return PorcessResult(result);
        }

        #endregion get

        #region datatable to model

        private static List<OpenPayCustomerModel> PorcessResult(DataTable items)
        {

            List<OpenPayCustomerModel> result_items = new List<OpenPayCustomerModel>();

            foreach (DataRow row in items.Rows)
            {
                result_items.Add(new OpenPayCustomerModel
                {
                    id = ToolsBO.ProcessResultInt(Member_id, row),
                    CustomerID = ToolsBO.ProcessResultString(Member_CustomerID, row),
                    ExternalID = ToolsBO.ProcessResultInt(Member_ExternalID, row),
                    Name = ToolsBO.ProcessResultString(Member_Name, row),

                    LastName = ToolsBO.ProcessResultString(Member_LastName, row),
                    Email = ToolsBO.ProcessResultString(Member_Email, row),
                    PhoneNumber = ToolsBO.ProcessResultString(Member_PhoneNumber, row),
                    RequiresAccount = ToolsBO.ProcessResultBoolean(Member_RequiresAccount, row),

                    Address1 = ToolsBO.ProcessResultString(Member_Address1, row),
                    Address2 = ToolsBO.ProcessResultString(Member_Address2, row),
                    Address3 = ToolsBO.ProcessResultString(Member_Address3, row),
                    City = ToolsBO.ProcessResultString(Member_City, row),

                    State = ToolsBO.ProcessResultString(Member_State, row),
                    CountryCode = ToolsBO.ProcessResultString(Member_CountryCode, row),
                    PostalCode = ToolsBO.ProcessResultString(Member_PostalCode, row),
                    CLABE = ToolsBO.ProcessResultString(Member_CLABE, row),

                    DateAdd = ToolsBO.ProcessResultDateTime(Member_DateAdd, row),
                    DateDelete = ToolsBO.ProcessResultDateTime(Member_DateDelete, row)

                });
            }

            return result_items;
        }


        #endregion datatable to model


        #endregion methods
    }
}