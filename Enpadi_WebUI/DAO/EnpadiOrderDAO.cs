﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enpadi_WebUI.Models;
using System.Data;
using System.Data.SqlClient;

namespace Enpadi_WebUI.DAO
{
    public class EnpadiOrderDAO
    {

        #region variables

        #region sp name

        private const String SaveStoreProcedureName = "EnpadiOrderInsert";
        private const String GetStoreProcedureName = "EnpadiOrderGet";

        #endregion sp name

        #region parameters

        private const String Param_id = "@id";
        private const String Param_livemode = "@livemode";
        private const String Param_amount = "@amount";
        private const String Param_currency = "@currency";
        private const String Param_amount_refunded = "@amount_refunded";
        private const String Param_external_id = "@external_id";

        private const String Param_external_description = "@external_description";
        private const String Param_external_category = "@external_category";
        private const String Param_object_description = "@object_description";
        private const String Param_createdAt = "@createdAt";
        private const String Param_updatedAt = "@updatedAt";

        private const String Param_customer_email = "@customer_email";
        private const String Param_customer_phone_number = "@customer_phone_number";
        private const String Param_customer_name = "@customer_name";
        private const String Param_customer_last_name = "@customer_last_name";
        private const String Param_customer_external_id = "@customer_external_id";
        private const String Param_customer_object_description = "@customer_object_description";

        private const String Param_address_line1 = "@address_line1";
        private const String Param_address_line2 = "@address_line2";
        private const String Param_address_line3 = "@address_line3";

        private const String Param_address_city = "@address_city";
        private const String Param_address_state = "@address_state";

        private const String Param_address_country_code = "@address_country_code";
        private const String Param_address_postal_code = "@address_postal_code";

        private const String Param_localUserEmail = "@client_email";
        private const String Param_localUserId = "@localUserId";
        private const String Param_type = "@type";
        private const String Param_client_trans_password = "@client_trans_password";

        #endregion parameters

        #region members

        private const String Member_id = "id";
        private const String Member_livemode = "livemode";
        private const String Member_amount = "amount";
        private const String Member_currency = "currency";
        private const String Member_amount_refunded = "amount_refunded";
        private const String Member_external_id = "external_id";

        private const String Member_external_description = "external_description";
        private const String Member_external_category = "external_category";
        private const String Member_object_description = "object_description";
        private const String Member_createdAt = "createdAt";
        private const String Member_updatedAt = "updatedAt";

        private const String Member_customer_email = "customer_email";
        private const String Member_customer_phone_number = "customer_phone_number";
        private const String Member_customer_name = "customer_name";
        private const String Member_customer_last_name = "customer_last_name";
        private const String Member_customer_external_id = "customer_external_id";
        private const String Member_customer_object_description = "customer_object_description";

        private const String Member_address_line1 = "address_line1";
        private const String Member_address_line2 = "address_line2";
        private const String Member_address_line3 = "address_line3";

        private const String Member_address_city = "address_city";
        private const String Member_address_state = "address_state";

        private const String Member_address_country_code = "address_country_code";
        private const String Member_address_postal_code = "address_postal_code";

        private const String Member_localUserEmail = "client_email";
        private const String Member_localUserId = "localUserId";
        private const String Member_type = "type";
        private const String Member_client_trans_password = "client_trans_password";

        #endregion members

        #endregion variables

        #region methods

        #region save

        public static void Save(
            Guid id,
            Boolean livemode,
            Decimal amount,
            String currency,
            Decimal amount_refunded,
            String external_id,

            String external_description,
            String external_category,
            String object_description,
            String createdAt,
            String updatedAt,

            String customer_email,
            String customer_phone_number,
            String customer_name,
            String customer_last_name,
            String customer_external_id,
            String customer_object_description,
           
            String localUserEmail,
            String localUserId,
            String type,
            String client_trans_password,
            
            String address_line1,
            String address_line2,
            String address_line3,

            String address_city,
            String address_state,
            String address_country_code,

            String address_postal_code
        )
        {
            SqlParameter[] parameters = {
                new SqlParameter(Param_id, id),
                new SqlParameter(Param_livemode, livemode),
                new SqlParameter(Param_amount, amount),
                new SqlParameter(Param_currency, currency),
                new SqlParameter(Param_amount_refunded,amount_refunded),
                new SqlParameter(Param_external_id,external_id),

                new SqlParameter(Param_external_description,external_description),
                new SqlParameter(Param_external_category,external_category),
                new SqlParameter(Param_object_description,object_description),
                new SqlParameter(Param_createdAt,createdAt),
                new SqlParameter(Param_updatedAt,updatedAt),

                new SqlParameter(Param_customer_email,customer_email),
                new SqlParameter(Param_customer_phone_number,customer_phone_number),
                new SqlParameter(Param_customer_name,customer_name),
                new SqlParameter(Param_customer_last_name,customer_last_name),
                new SqlParameter(Param_customer_external_id,customer_external_id),
                new SqlParameter(Param_customer_object_description,customer_object_description),

                new SqlParameter(Param_localUserEmail,localUserEmail),
                new SqlParameter(Param_localUserId,localUserId),
                new SqlParameter(Param_type,type),
                new SqlParameter(Param_client_trans_password,client_trans_password),

                new SqlParameter(Param_address_line1,address_line1),
                new SqlParameter(Param_address_line2,address_line2),
                new SqlParameter(Param_address_line3,address_line3),

                new SqlParameter(Param_address_city,address_city),
                new SqlParameter(Param_address_state,address_state),
                new SqlParameter(Param_address_country_code,address_country_code),

                new SqlParameter(Param_address_postal_code,address_postal_code)
            };

            DataBaseConnector.ExecuteStoreProcedure(SaveStoreProcedureName, parameters);
        }

        public static void Save(
            EnpadiOrderModel item
        )
        {
            Save(
                 item.data_object.id,
                 item.data_object.livemode,
                 item.data_object.amount,
                 item.data_object.currency,
                 item.data_object.amount_refunded,
                 item.data_object.external_id,

                 item.data_object.external_description,
                 item.data_object.external_category,
                 item.data_object.object_description,
                 item.data_object.createdAt,
                 item.data_object.updatedAt,

                 item.data_object.customer_info.email,
                 item.data_object.customer_info.phoneNumber,
                 item.data_object.customer_info.name,
                  item.data_object.customer_info.last_name,
                 item.data_object.customer_info.externalId,
                 item.data_object.customer_info.object_description,

                 item.data_object.client_email,
                 item.data_object.localUserId.ToString(),
                 item.data_object.type,
                 item.data_object.client_trans_password,

                 item.data_object.customer_info.address_line1,
                 item.data_object.customer_info.address_line2,
                 item.data_object.customer_info.address_line3,

                 item.data_object.customer_info.address_city,
                 item.data_object.customer_info.address_state,
                 item.data_object.customer_info.address_country_code,

                 item.data_object.customer_info.address_postal_code

           );
        }

        #endregion save

        #region get

        public static EnpadiOrderModel Get(Int64 id)
        {
            DataTable result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_id, id)

            };

            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName, parameters);

            return PorcessResult(result).FirstOrDefault();
        }

        public static List<EnpadiOrderModel> GetAll()
        {
            DataTable result = new DataTable();
            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName);

            return PorcessResult(result);
        }

        #endregion get

        #region datatable to model

        private static List<EnpadiOrderModel> PorcessResult(DataTable items)
        {

            List<EnpadiOrderModel> result_items = new List<EnpadiOrderModel>();

            foreach (DataRow row in items.Rows)
            {
                result_items.Add(new EnpadiOrderModel
                {
                    data_object = new EnpadiOrderModel.data {
                        id = ToolsBO.ProcessResultGuid(Member_id,row),
                        livemode = ToolsBO.ProcessResultBoolean(Member_livemode, row),
                        amount = ToolsBO.ProcessResultDecimal(Member_amount,row),
                        currency = ToolsBO.ProcessResultString(Member_currency,row),
                        amount_refunded = ToolsBO.ProcessResultDecimal(Member_amount_refunded,row),
                        external_id = ToolsBO.ProcessResultString(Member_external_id,row),

                        external_description = ToolsBO.ProcessResultString(Member_external_description, row),
                        external_category = ToolsBO.ProcessResultString(Member_external_category, row),
                        object_description = ToolsBO.ProcessResultString(Member_object_description, row),
                        createdAt = ToolsBO.ProcessResultString(Member_createdAt,row),
                        updatedAt = ToolsBO.ProcessResultString(Member_updatedAt, row),

                        customer_info = new EnpadiOrderModel.customer_info {
                            email = ToolsBO.ProcessResultString(Member_customer_email, row),
                            phoneNumber = ToolsBO.ProcessResultString(Member_customer_phone_number, row),
                            name = ToolsBO.ProcessResultString(Member_customer_name, row),
                            externalId = ToolsBO.ProcessResultString(Member_external_id, row),
                             object_description = ToolsBO.ProcessResultString(Member_customer_object_description, row)
                        },

                        client_email = ToolsBO.ProcessResultString(Member_customer_email,row),
                        localUserId = ToolsBO.ProcessResultGuid(Member_localUserId, row),
                        type = ToolsBO.ProcessResultString(Member_type, row),
                        client_trans_password = ToolsBO.ProcessResultString(Member_client_trans_password, row),
                    }
                });
            }

            return result_items;
        }


        #endregion datatable to model

        #endregion methods

    }
}