﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enpadi_WebUI.Models;
using System.Data.SqlClient;
using System.Data;

namespace Enpadi_WebUI.DAO
{
    public class EnpadiEvent
    {
        #region constants


        #region sp names

        private const String SaveStoreProcedureName = "EnpadiEventInsert";
        private const String GetStoreProcedureName = "EnpadiEventGet";


        #endregion sp names

        #region parameters

        private const String Param_id = "@id";
        private const String Param_livemode = "@livemode";
        private const String Param_webhook_status = "@webhook_status";

        private const String Param_object_description = "@object_description";
        private const String Param_type = "@type";
        private const String Param_data_id = "@data_id";
        private const String Param_created_at = "@created_at";

        #endregion parameters

        #region members


        #region parameters

        private const String Member_id = "@id";
        private const String Member_livemode = "@livemode";
        private const String Member_webhook_status = "@webhook_status";

        private const String Member_object_description = "@object_description";
        private const String Member_type = "@type";
        private const String Member_data_id = "@data_id";
        private const String Member_created_at = "@created_at";


        #endregion parameters

        #endregion members

        #endregion constants

        #region methods

        #region save

        /// <summary>
        /// Save an Enpadi Event with independent attributes
        /// </summary>
        /// <param name="id"></param>
        /// <param name="livemode"></param>
        /// <param name="webhook_status"></param>
        /// <param name="object_description"></param>
        /// <param name="type"></param>
        /// <param name="data_id"></param>
        public static void Save(
         Guid id,
         Guid data_id,
         Boolean livemode,
         String webhook_status,
         String object_description,
         String type
        )
        {
            SqlParameter[] parameters = {
                new SqlParameter(Param_id,id),
                new SqlParameter(Param_livemode,livemode),
                new SqlParameter(Param_webhook_status,webhook_status),
                new SqlParameter(Param_object_description,object_description),

                new SqlParameter(Param_type,type),
                new SqlParameter(Param_data_id,data_id),
                new SqlParameter(Param_created_at,ToolsBO.DateTimeToTimeStamp(DateTime.Now))

            };

            DataBaseConnector.ExecuteStoreProcedure(SaveStoreProcedureName, parameters);
        }


        /// <summary>
        /// Save an Enpadi Event
        /// </summary>
        /// <param name="item"></param>
        public static void Save(
            EnpadiEventModel item
        )
        {
            Save(
                item.id,
                item.data_id,
                item.livemode,
                item.webhook_status,
                item.object_description,
                item.type
                );
        }

        /// <summary>
        /// Save a list of Enpadi Event 
        /// </summary>
        /// <param name="items"></param>
        public static void Save(
            List<EnpadiEventModel> items
        )
        {

            foreach (EnpadiEventModel item in items)
            {
                Save(item);
            }
        }

        #endregion save

        #region get

        public static EnpadiEventModel Get(Guid id)
        {
            DataTable result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_id, id)

            };

            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName, parameters);

            return PorcessResult(result).FirstOrDefault();
        }

        public static List<EnpadiEventModel> GetAll()
        {
            DataTable result = new DataTable();
            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName);

            return PorcessResult(result);
        }

        #endregion get

        #region datatable to model

        private static List<EnpadiEventModel> PorcessResult(DataTable items)
        {

            List<EnpadiEventModel> result_items = new List<EnpadiEventModel>();

            foreach (DataRow row in items.Rows)
            {
                result_items.Add(new EnpadiEventModel
                {
                    id = ToolsBO.ProcessResultGuid(Member_id, row),
                    data_id = ToolsBO.ProcessResultGuid(Member_data_id, row),
                    livemode = ToolsBO.ProcessResultBoolean(Member_livemode, row),
                    object_description = ToolsBO.ProcessResultString(Member_object_description, row),
                    type = ToolsBO.ProcessResultString(Member_type, row),
                    webhook_status = ToolsBO.ProcessResultString(Member_webhook_status, row),
                    created_at = ToolsBO.ProcessResultString(Member_created_at, row)
                });
            }

            return result_items;
        }

        #endregion datatable to model

        #endregion methods
    }
}