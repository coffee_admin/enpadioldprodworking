﻿using Enpadi_WebUI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.DAO
{
    public class paystub
    {
        #region constants

        #region sp names

        private const String SaveStoreProcedureName = "paystubInsert";
        private const String GetStoreProcedureName = "paystubGet";

        #endregion sp names

        #region parameters

        private const String Param_id = "@id";
        private const String Param_paystub_type_id = "@paystub_type_id";
        private const String Param_created = "@date_created";
        private const String Param_modified = "@date_modified";
        private const String Param_created_by = "@created_by";
        private const String Param_open_pay_charge_id = "@open_pay_charge_id";
        private const String Param_open_pay_order_id = "@open_pay_order_id";

        #endregion parameters

        #region parameters

        private const String Member_id = "id";
        private const String Member_paystub_type_id = "paystub_type_id";
        private const String Member_created = "date_created";
        private const String Member_modified = "date_modified";
        private const String Member_created_by = "created_by";
        private const String Member_open_pay_charge_id = "open_pay_charge_id";
        private const String Member_open_pay_order_id = "open_pay_order_id";

        #endregion parameters

        #endregion constants

        #region methods

        #region save

        public static void Save(
            Guid id,
            Guid paystub_type_id,
            DateTime date_created,
            DateTime? date_modified,
            String created_by,
            String open_pay_charge_id,
            String open_pay_order_id
)
        {
            DataTable operation_result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_id, id),
                new SqlParameter(Param_paystub_type_id, paystub_type_id),
                new SqlParameter(Param_created, date_created),
                new SqlParameter(Param_modified , (date_modified == DateTime.MinValue ? null: date_modified)),
                new SqlParameter(Param_created_by, created_by),
                new SqlParameter(Param_open_pay_charge_id, open_pay_charge_id),
                new SqlParameter(Param_open_pay_order_id, open_pay_order_id)

            };

            operation_result = DataBaseConnector.ExecuteStoreProcedure(SaveStoreProcedureName, parameters);
        }

        public static paystubModel Save(
             paystubModel item
        )
        {
            Save(
                item.Id,
                item.paystub_type_id,
                item.date_created,
                item.date_modified,
                item.created_by_email,
                item.open_pay_charge_id,
                item.open_pay_order_id
             );

            return item;
        }

        public static void Save(
            List<paystubModel> items
        )
        {
            foreach (paystubModel item in items)
            {
                Save(item);
            }
        }

        #endregion save

        #region get

        public static paystubModel Get(String id)
        {
            DataTable result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_id, id)

            };

            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName, parameters);

            return PorcessResult(result).FirstOrDefault();
        }

        public static List<paystubModel> GetAll()
        {
            DataTable result = new DataTable();
            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName);

            return PorcessResult(result);
        }

        #endregion get

        #region datatable to model

        private static List<paystubModel> PorcessResult(DataTable items)
        {

            List<paystubModel> result_items = new List<paystubModel>();

            foreach (DataRow row in items.Rows)
            {
                result_items.Add(new paystubModel
                {
                    Id = ToolsBO.ProcessResultGuid(Param_id, row),
                    paystub_type_id = ToolsBO.ProcessResultGuid(Param_paystub_type_id, row),
                    date_created = ToolsBO.ProcessResultDateTime(Param_created, row),
                    date_modified = ToolsBO.ProcessResultDateTime(Param_modified, row),
                    created_by_email = ToolsBO.ProcessResultString(Param_created_by, row),
                    open_pay_charge_id = ToolsBO.ProcessResultString(Param_open_pay_charge_id, row),
                    open_pay_order_id = ToolsBO.ProcessResultString(Param_open_pay_order_id, row)
                });
            }

            return result_items;
        }


        #endregion datatable to model


        #endregion methods
    }
}