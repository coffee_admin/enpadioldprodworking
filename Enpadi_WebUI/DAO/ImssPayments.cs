﻿using Enpadi_WebUI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.DAO
{
    public class ImssPayments
    {
        #region constants


        #region sp names

        private const String SaveStoreProcedureName = "ImssPaymentInsert";
        private const String GetStoreProcedureName = "ImssPaymentGet";


        #endregion sp names

        #region parameters

        private const String Param_id = "@id";
        private const String Param_beneficiary_name = "@beneficiary_name";
        private const String Param_beneficiary_phone_number = "@beneficiary_phone_number";
        private const String Param_beneficiary_email_address = "@beneficiary_email_address";
        private const String Param_beneficiary_imss_number = "@beneficiary_imss_number";

        private const String Param_buyer_name = "@buyer_name";
        private const String Param_buyer_phone_number = "@buyer_phone_number";
        private const String Param_beneficiary_cell_phone_number = "@beneficiary_cell_phone_number";
        private const String Param_buyer_email = "@buyer_email";
        private const String Param_buyer_enpadi_card = "@buyer_enpadi_card";
        private const String Param_buyer_enpadi_card_ending = "@buyer_enpadi_card_ending";

        private const String Param_code_base = "@code_base";
        private const String Param_code_number = "@code_number";
        private const String Param_remitter_name = "@remitter_name";
        private const String Param_amout_pay = "@amout_pay";
        private const String Param_payment_date_at = "@payment_date_at";

        #endregion parameters


        #region members

        private const String Member_id = "id";
        private const String Member_beneficiary_name = "beneficiary_name";
        private const String Member_beneficiary_phone_number = "beneficiary_phone_number";
        private const String Member_beneficiary_email_address = "beneficiary_email_address";
        private const String Member_beneficiary_imss_number = "beneficiary_imss_number";

        private const String Member_buyer_name = "buyer_name";
        private const String Member_buyer_phone_number = "buyer_phone_number";
        private const String Member_beneficiary_cell_phone_number = "@beneficiary_cell_phone_number";
        private const String Member_buyer_email = "buyer_email";
        private const String Member_buyer_enpadi_card = "buyer_enpadi_card";
        private const String Member_buyer_enpadi_card_ending = "buyer_enpadi_card_ending";

        private const String Member_code_base = "code_base";
        private const String Member_code_number = "code_number";
        private const String Member_remitter_name = "remitter_name";
        private const String Member_amout_pay = "amout_pay";
        private const String Member_payment_date_at = "payment_date_at";


        #endregion members

        #endregion constants

        #region methods

        #region save

        public static Int32 Save(
            Guid id,
            string beneficiary_name,
            string beneficiary_phone_number,
            string beneficiary_cell_phone_number,
            string beneficiary_email_address,
            string beneficiary_imss_number,

            string buyer_name,
            string buyer_phone_number,

            string buyer_email,
            string buyer_enpadi_card,
            string buyer_enpadi_card_ending,

            string code_base,
            string remitter_name,
            decimal amout_pay,
            DateTime? payment_date_at
        )
        {

            DataTable operation_result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_id, id),
                new SqlParameter(Param_beneficiary_name,beneficiary_name),
                new SqlParameter(Param_beneficiary_phone_number,beneficiary_phone_number),
                new SqlParameter(Param_beneficiary_cell_phone_number,beneficiary_cell_phone_number),
                new SqlParameter(Param_beneficiary_email_address,beneficiary_email_address),
                new SqlParameter(Param_beneficiary_imss_number,beneficiary_imss_number),

                new SqlParameter(Param_buyer_name, buyer_name),
                new SqlParameter(Param_buyer_phone_number,buyer_phone_number),
                new SqlParameter(Param_buyer_email,buyer_email),
                new SqlParameter(Param_buyer_enpadi_card,buyer_enpadi_card),
                new SqlParameter(Param_buyer_enpadi_card_ending,buyer_enpadi_card_ending),

                new SqlParameter(Param_code_base, code_base),
                new SqlParameter(Param_remitter_name,remitter_name),
                new SqlParameter(Param_amout_pay,amout_pay),
                new SqlParameter(Param_payment_date_at,payment_date_at)
            };

            operation_result = DataBaseConnector.ExecuteStoreProcedure(SaveStoreProcedureName, parameters);

            return Int32.Parse(operation_result.Rows[0].ItemArray[0].ToString());
        }

        public static Int32 Save(
            ImssPaymentsModel item
        )
        {
           item.code_number = Save(
                item.id,
                item.beneficiary_name,
                item.beneficiary_phone_number,
                item.beneficiary_cell_phone_number,
                item.beneficiary_email_address,
                item.beneficiary_imss_number,

                item.buyer_name,
                item.buyer_phone_number,
                item.buyer_email,
                item.buyer_enpadi_card,
                item.buyer_enpadi_card_ending,

                item.code_base,
                item.remitter_name,
                item.amout_pay,
                item.payment_date_at
             );

            return item.code_number;
        }

        public static void Save(
            List<ImssPaymentsModel> items
        )
        {
            foreach (ImssPaymentsModel item in items)
            {
                Save(item);
            }
        }

        #endregion save

        #region get

        public static ImssPaymentsModel Get(Guid id)
        {
            DataTable result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_id, id)

            };

            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName, parameters);

            return PorcessResult(result).FirstOrDefault();
        }

        public static List<ImssPaymentsModel> GetAll()
        {
            DataTable result = new DataTable();
            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName);

            return PorcessResult(result);
        }

        #endregion get

        #region datatable to model

        private static List<ImssPaymentsModel> PorcessResult(DataTable items)
        {

            List<ImssPaymentsModel> result_items = new List<ImssPaymentsModel>();

            foreach (DataRow row in items.Rows)
            {
                result_items.Add(new ImssPaymentsModel
                {
                    id = ToolsBO.ProcessResultGuid(Member_id, row),
                    beneficiary_name = ToolsBO.ProcessResultString(Member_beneficiary_name, row),
                    beneficiary_phone_number = ToolsBO.ProcessResultString(Member_beneficiary_phone_number, row),
                    beneficiary_email_address = ToolsBO.ProcessResultString(Member_beneficiary_email_address, row),
                    beneficiary_imss_number = ToolsBO.ProcessResultString(Member_beneficiary_imss_number, row),

                    buyer_name = ToolsBO.ProcessResultString(Member_buyer_name, row),
                    buyer_phone_number = ToolsBO.ProcessResultString(Member_buyer_phone_number, row),
                    buyer_email = ToolsBO.ProcessResultString(Member_buyer_email, row),
                    buyer_enpadi_card = ToolsBO.ProcessResultString(Member_buyer_enpadi_card, row),
                    buyer_enpadi_card_ending = ToolsBO.ProcessResultString(Member_buyer_enpadi_card_ending, row),

                    code_base = ToolsBO.ProcessResultString(Member_code_base, row),
                    code_number = ToolsBO.ProcessResultInt(Member_code_number, row),
                    remitter_name = ToolsBO.ProcessResultString(Member_remitter_name, row),
                    amout_pay = ToolsBO.ProcessResultDecimal(Member_amout_pay, row),
                    payment_date_at = ToolsBO.ProcessResultDateTime(Member_payment_date_at, row)
                });
            }

            return result_items;
        }


        #endregion datatable to model


        #endregion methods

    }
}