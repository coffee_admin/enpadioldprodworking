﻿using Enpadi_WebUI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.DAO
{
    public class paystubType
    {
        #region constants

        #region sp names

        private const String SaveStoreProcedureName = "paystubtypeInsert";
        private const String GetStoreProcedureName = "paystubtypeGet";

        #endregion sp names

        #region parameters

        private const String Param_id = "@id";
        private const String Param_name = "@name";
        private const String Param_description = "@description";

        #endregion parameters

        #region parameters

        private const String Member_id = "id";
        private const String Member_name = "name";
        private const String Member_description = "description";

        #endregion parameters

        #endregion constants

        #region methods

        #region save

        public static void Save(
            Guid Id,
            String name,
            String description
)
        {
            DataTable operation_result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_id, Id),
                new SqlParameter(Param_name, name),
                new SqlParameter(Param_description, description)

            };

            operation_result = DataBaseConnector.ExecuteStoreProcedure(SaveStoreProcedureName, parameters);
        }

        public static paystubTypeModel Save(
             paystubTypeModel item
        )
        {
            Save(
                item.id,
                item.name,
                item.description
             );

            return item;
        }

        public static void Save(
            List<paystubTypeModel> items
        )
        {
            foreach (paystubTypeModel item in items)
            {
                Save(item);
            }
        }

        #endregion save

        #region get

        public static paystubTypeModel Get(String id)
        {
            DataTable result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_id, id)

            };

            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName, parameters);

            return PorcessResult(result).FirstOrDefault();
        }

        public static List<paystubTypeModel> GetAll()
        {
            DataTable result = new DataTable();
            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName);

            return PorcessResult(result);
        }

        #endregion get

        #region datatable to model

        private static List<paystubTypeModel> PorcessResult(DataTable items)
        {

            List<paystubTypeModel> result_items = new List<paystubTypeModel>();

            foreach (DataRow row in items.Rows)
            {
                result_items.Add(new paystubTypeModel
                {
                    id = ToolsBO.ProcessResultGuid(Param_id, row),
                    name = ToolsBO.ProcessResultString(Param_name, row),
                    description = ToolsBO.ProcessResultString(Param_description, row)

                });
            }

            return result_items;
        }


        #endregion datatable to model


        #endregion methods
    }
}