﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Enpadi_WebUI.Models;
using System.Data;

namespace Enpadi_WebUI.DAO
{
    public class ClientEncryptionInfo
    {
        #region sp names

        private const String SaveStoreProcedureName = "ClientEncryptionInsert";
        private const String GetStoreProcedureName = "ClientEncryptionGet";

        #endregion sp names

        #region parameters
        public const String Param_Id = "@Id";
        public const String Param_Name = "@Name";
        public const String Param_Comercial_Name = "@Comercial_Name";
        public const String Param_RFC = "@RFC";

        public const String Param_WebHook = "@WebHook";
        public const String Param_Contact_name = "@Contact_name";
        public const String Param_Contact_email = "@Contact_email";
        public const String Param_Contact_phone_number = "@Contact_phone_number";

        public const String Param_Acepted_at = "@Acepted_at";
        public const String Param_Created_at = "@Created_at";
        public const String Param_Deleted_at = "@Deleted_at";
        public const String Param_Blocked = "@Blocked";
        public const String Param_ChargeRate = "@ChargeRate";

        public const String Param_Charge = "@Charge";
        public const String Param_UserId = "@UserId";
        public const String Param_Private_Key = "@Private_Key";
        public const String Param_Public_Key = "@Public_Key";

        #endregion parameters

        #region members

        public const String Member_Id = "Id";
        public const String Member_Name = "Name";
        public const String Member_Comercial_Name = "Comercial_Name";
        public const String Member_RFC = "RFC";

        public const String Member_WebHook = "WebHook";
        public const String Member_Contact_name = "Contact_name";
        public const String Member_Contact_email = "Contact_email";
        public const String Member_Contact_phone_number = "Contact_phone_number";

        public const String Member_Acepted_at = "Acepted_at";
        public const String Member_Created_at = "Created_at";
        public const String Member_Deleted_at = "Deleted_at";
        public const String Member_Blocked = "Blocked";
        public const String Member_ChargeRate = "ChargeRate";

        public const String Member_Charge = "Charge";
        public const String Member_UserId = "UserId";
        public const String Member_Private_Key = "Private_Key";
        public const String Member_Public_Key = "Public_Key";

        #endregion members

        #region methods

        #region save
        public static Guid Save(
            String Name,
            String Comercial_Name,
            String RFC,

            String WebHook,

            String Contact_name,
            String Contact_email,
            String Contact_phone_number,

            DateTime Acepted_at,
            DateTime Created_at,
            Boolean Blocked,

            Decimal ChargeRate,
            Decimal Charge,
            Guid UserId,

            String Private_Key,
            String Public_Key
           )
        {
            SqlParameter[] parameters = {
                new SqlParameter(Param_Name,Name),
                new SqlParameter(Param_Comercial_Name,Comercial_Name),
                new SqlParameter(Param_RFC,RFC),
                new SqlParameter(Param_WebHook,WebHook),
                new SqlParameter(Param_Contact_name,Contact_name),

                new SqlParameter(Param_Contact_email,Contact_email),
                new SqlParameter(Param_Contact_phone_number,Contact_phone_number),
                new SqlParameter(Param_Acepted_at,Acepted_at),

                new SqlParameter(Param_Created_at,Created_at),
                new SqlParameter(Param_Blocked,Blocked),
                new SqlParameter(Param_ChargeRate,ChargeRate),
                new SqlParameter(Param_Charge,Charge),

                new SqlParameter(Param_UserId,UserId),
                new SqlParameter(Param_Private_Key,Private_Key),
                new SqlParameter(Param_Public_Key,Public_Key)
            };

            DataTable result = DataBaseConnector.ExecuteStoreProcedure(SaveStoreProcedureName, parameters);

            return Guid.Parse(result.Rows[0].ItemArray[0].ToString());
        }

        public static void Save(
            ClientEncryptionInfoModel item
        )
        {
            item.Id = Save(
            item.Name,
            item.Comercial_Name,
            item.RFC,

            item.WebHook,

            item.Contact_name,
            item.Contact_email,
            item.Contact_phone_number,

            item.Acepted_at,
            item.Created_at,
            item.Blocked,

            item.ChargeRate,
            item.Charge,
            item.UserId,

            item.Private_Key,
            item.Public_Key);
        }
        #endregion save

        #region get

        public static ClientEncryptionInfoModel Get(Guid id)
        {
            DataTable result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_Id, id)

            };

            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName, parameters);

            return PorcessResult(result).FirstOrDefault();
        }

        public static List<ClientEncryptionInfoModel> GetAll()
        {
            DataTable result = new DataTable();
            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName);

            return PorcessResult(result);
        }

        #endregion get

        #region datatable to model

        private static List<ClientEncryptionInfoModel> PorcessResult(DataTable items)
        {

            List<ClientEncryptionInfoModel> result_items = new List<ClientEncryptionInfoModel>();

            foreach (DataRow row in items.Rows)
            {
                result_items.Add(new ClientEncryptionInfoModel
                {
                    Id = ToolsBO.ProcessResultGuid(Member_Id, row),
                    Name = ToolsBO.ProcessResultString(Member_Name, row),
                    Comercial_Name = ToolsBO.ProcessResultString(Member_Comercial_Name, row),
                    RFC = ToolsBO.ProcessResultString(Member_RFC, row),

                    WebHook = ToolsBO.ProcessResultString(Member_WebHook, row),

                    Contact_name = ToolsBO.ProcessResultString(Member_Contact_name, row),
                    Contact_email = ToolsBO.ProcessResultString(Member_Contact_email, row),
                    Contact_phone_number = ToolsBO.ProcessResultString(Member_Contact_phone_number, row),

                    Acepted_at = ToolsBO.ProcessResultDateTime(Member_Acepted_at, row),
                    Created_at = ToolsBO.ProcessResultDateTime(Member_Created_at, row),
                    Blocked = ToolsBO.ProcessResultBoolean(Member_Blocked, row),

                    ChargeRate = ToolsBO.ProcessResultDecimal(Member_ChargeRate, row),
                    Charge = ToolsBO.ProcessResultDecimal(Member_Charge, row),
                    UserId = ToolsBO.ProcessResultGuid(Member_UserId, row),

                    Private_Key = ToolsBO.ProcessResultString(Member_Private_Key, row),
                    Public_Key = ToolsBO.ProcessResultString(Member_Public_Key, row),
                    Deleted_at = ToolsBO.ProcessResultDateTime(Member_Deleted_at, row)
                    
                });
            }

            return result_items;
        }

        #endregion datatable to model

        #endregion methods


    }
}