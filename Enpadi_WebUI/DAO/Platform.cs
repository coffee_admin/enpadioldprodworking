﻿using Enpadi_WebUI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.DAO
{
    public class Platform
    {

        #region constants

        #region sp names

        private const String SaveStoreProcedureName = "PlatformInsert";
        private const String GetStoreProcedureName = "PlatformGet";

        #endregion sp names

        #region parameters

        private const String Param_id = "@id";
        private const String Param_name = "@name";
        private const String Param_description = "@description";

        #endregion parameters

        #region parameters

        private const String Member_id = "id";
        private const String Member_name = "name";
        private const String Member_description = "description";

        #endregion parameters

        #endregion constants

        #region methods

        #region save

        public static Int32 Save(
         Guid id,
         string name,
         string description
)
        {
            DataTable operation_result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_id, id),
                new SqlParameter(Param_name,name),
                new SqlParameter(Param_description,description)
            };

            operation_result = DataBaseConnector.ExecuteStoreProcedure(SaveStoreProcedureName, parameters);

            return Int32.Parse(operation_result.Rows[0].ItemArray[0].ToString());
        }

        public static PlatformModel Save(
             PlatformModel item
        )
        {
            Save(
                item.Id,
                item.Name,
                item.Description
             );

            return item;
        }

        public static void Save(
            List<PlatformModel> items
        )
        {
            foreach (PlatformModel item in items)
            {
                Save(item);
            }
        }

        #endregion save

        #region get

        public static PlatformModel Get(Guid id)
        {
            DataTable result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_id, id)

            };

            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName, parameters);

            return PorcessResult(result).FirstOrDefault();
        }

        public static List<PlatformModel> GetAll()
        {
            DataTable result = new DataTable();
            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName);

            return PorcessResult(result);
        }

        #endregion get

        #region datatable to model

        private static List<PlatformModel> PorcessResult(DataTable items)
        {

            List<PlatformModel> result_items = new List<PlatformModel>();

            foreach (DataRow row in items.Rows)
            {
                result_items.Add(new PlatformModel
                {
                    Id = ToolsBO.ProcessResultGuid(Member_id, row),
                    Name = ToolsBO.ProcessResultString(Member_name, row),
                    Description = ToolsBO.ProcessResultString(Member_description, row)
                });
            }

            return result_items;
        }


        #endregion datatable to model

        #endregion methods

    }
}