﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Enpadi_WebUI.Models;

namespace Enpadi_WebUI.DAO
{
    public class OpenPayCharge
    {
        #region constants

        #region sp names

        private const String SaveStoreProcedureName = "OpenPayChargeInsert";
        private const String GetStoreProcedureName = "OpenPayChargeGet";

        #endregion sp names

        #region parameters

        private const String Param_Id = "@Id";
        private const String Param_CreationDate = "@CreationDate";
        private const String Param_Amount = "@Amount";
        private const String Param_Status = "@Status";
        private const String Param_Description = "@Description";
        private const String Param_TransactionType = "@TransactionType";
        private const String Param_OperationType = "@OperationType";
        private const String Param_Method = "@Method";
        private const String Param_ErrorMessage = "@ErrorMessage";
        private const String Param_Authorization = "@Authorization";
        private const String Param_OrderId = "@OrderId";
        private const String Param_CustomerId = "@CustomerId";
        private const String Param_Conciliated = "@Conciliated";
        private const String Param_Type = "@PaymentMethodType";
        private const String Param_BankName = "@PaymentMethodBankName";
        private const String Param_CLABE = "@PaymentMethodCLABE";
        private const String Param_Name = "@PaymentMethodName";
        private const String Param_Reference = "@PaymentMethodReference";
        private const String Param_BarcodeURL = "@PaymentMethodBarcodeURL";
        private const String Param_PaymentAddress = "@PaymentMethodPaymentAddress";
        private const String Param_PaymentUrlBip21 = "@PaymentMethodPaymentUrlBip21";
        private const String Param_AmountBitcoins = "@PaymentMethodAmountBitcoins";

        #endregion parameters

        #region parameters

        private const String Member_Id = "@Id";
        private const String Member_CreationDate = "CreationDate";
        private const String Member_Amount = "Amount";
        private const String Member_Status = "Status";
        private const String Member_Description = "Description";
        private const String Member_TransactionType = "TransactionType";
        private const String Member_OperationType = "OperationType";
        private const String Member_Method = "Method";
        private const String Member_ErrorMessage = "ErrorMessage";
        private const String Member_Authorization = "Authorization";
        private const String Member_OrderId = "OrderId";
        private const String Member_CustomerId = "CustomerId";
        private const String Member_Conciliated = "Conciliated";
        private const String Member_Type = "Type";
        private const String Member_BankName = "BankName";
        private const String Member_CLABE = "CLABE";
        private const String Member_Name = "Name";
        private const String Member_Reference = "Reference";
        private const String Member_BarcodeURL = "BarcodeURL";
        private const String Member_PaymentAddress = "PaymentAddress";
        private const String Member_PaymentUrlBip21 = "PaymentUrlBip21";
        private const String Member_AmountBitcoins = "AmountBitcoins";

        #endregion parameters

        #endregion constants

        #region methods

        #region save

        public static void Save(
            String Id,
            DateTime? CreationDate,
            Decimal Amount,
            String Status,
            String Description,
            String TransactionType,
            String OperationType,
            String Method,
            String ErrorMessage,
            String Authorization,
            String OrderId,
            String CustomerId,
            Boolean Conciliated,
            String Type,
            String BankName,
            String CLABE,
            String Name,
            String Reference,
            String BarcodeURL,
            String PaymentAddress,
            String PaymentUrlBip21,
            Decimal AmountBitcoins
)
        {
            DataTable operation_result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_Id, Id),
                new SqlParameter(Param_CreationDate, CreationDate),
                new SqlParameter(Param_Amount, Amount),
                new SqlParameter(Param_Status , Status),
                new SqlParameter(Param_Description, Description),
                new SqlParameter(Param_TransactionType , TransactionType),
                new SqlParameter(Param_OperationType, OperationType),
                new SqlParameter(Param_Method , Method),
                new SqlParameter(Param_ErrorMessage , ErrorMessage),
                new SqlParameter(Param_Authorization , Authorization),
                new SqlParameter(Param_OrderId , OrderId),
                new SqlParameter(Param_CustomerId , CustomerId),
                new SqlParameter(Param_Conciliated , Conciliated),
                new SqlParameter(Param_Type , Type),
                new SqlParameter(Param_BankName , BankName),
                new SqlParameter(Param_CLABE , CLABE),
                new SqlParameter(Param_Name , Name),
                new SqlParameter(Param_Reference , Reference),
                new SqlParameter(Param_BarcodeURL , BarcodeURL),
                new SqlParameter(Param_PaymentAddress , PaymentAddress),
                new SqlParameter(Param_PaymentUrlBip21 , PaymentUrlBip21),
                new SqlParameter(Param_AmountBitcoins , AmountBitcoins),

            };

            operation_result = DataBaseConnector.ExecuteStoreProcedure(SaveStoreProcedureName, parameters);
        }

        public static OpenPayChargeModel Save(
             OpenPayChargeModel item
        )
        {
            Save(
                item.Id,
                item.CreationDate,
                item.Amount,
                item.Status,
                item.Description,
                item.TransactionType,
                item.OperationType,
                item.Method,
                item.ErrorMessage,
                item.Authorization,
                item.OrderId,
                item.CustomerId,
                item.Conciliated,
                item.Type,
                item.BankName,
                item.CLABE,
                item.Name,
                item.Reference,
                item.BarcodeURL,
                item.PaymentAddress,
                item.PaymentUrlBip21,
                item.AmountBitcoins
             );

            return item;
        }

        public static Openpay.Entities.Charge Save(
           Openpay.Entities.Charge item
       )
        {
            Save(
                item.Id,
                item.CreationDate,
                item.Amount,
                item.Status,
                item.Description,
                item.TransactionType,
                item.OperationType,
                item.Method,
                item.ErrorMessage,
                item.Authorization,
                item.OrderId,
                item.CustomerId,
                item.Conciliated,
                item.PaymentMethod.Type,
                item.PaymentMethod.BankName,
                item.PaymentMethod.CLABE,
                item.PaymentMethod.Name,
                item.PaymentMethod.Reference,
                item.PaymentMethod.BarcodeURL,
                item.PaymentMethod.PaymentAddress,
                item.PaymentMethod.PaymentUrlBip21,
                item.PaymentMethod.AmountBitcoins
             );

            return item;
        }

        public static void Save(
            List<OpenPayChargeModel> items
        )
        {
            foreach (OpenPayChargeModel item in items)
            {
                Save(item);
            }
        }

        #endregion save

        #region get

        public static OpenPayChargeModel Get(String id)
        {
            DataTable result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_Id, id)

            };

            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName, parameters);

            return PorcessResult(result).FirstOrDefault();
        }

        public static List<OpenPayChargeModel> GetAll()
        {
            DataTable result = new DataTable();
            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName);

            return PorcessResult(result);
        }

        #endregion get

        #region datatable to model

        private static List<OpenPayChargeModel> PorcessResult(DataTable items)
        {

            List<OpenPayChargeModel> result_items = new List<OpenPayChargeModel>();

            foreach (DataRow row in items.Rows)
            {
                result_items.Add(new OpenPayChargeModel
                {
                   Id = ToolsBO.ProcessResultString(Param_Id, row),
                   CreationDate = ToolsBO.ProcessResultDateTime(Param_CreationDate, row),
                   Amount = ToolsBO.ProcessResultDecimal(Param_Amount, row),
                   Status = ToolsBO.ProcessResultString(Param_Status, row),
                   Description = ToolsBO.ProcessResultString(Param_Description, row),
                   TransactionType = ToolsBO.ProcessResultString(Param_TransactionType, row),
                   OperationType = ToolsBO.ProcessResultString(Param_OperationType, row),
                   Method = ToolsBO.ProcessResultString(Param_Method, row),
                   ErrorMessage = ToolsBO.ProcessResultString(Param_ErrorMessage, row),
                   Authorization = ToolsBO.ProcessResultString(Param_Authorization, row),
                   OrderId = ToolsBO.ProcessResultString(Param_OrderId, row),
                   CustomerId = ToolsBO.ProcessResultString(Param_CustomerId, row),
                   Conciliated = ToolsBO.ProcessResultBoolean(Param_Conciliated, row),
                   Type = ToolsBO.ProcessResultString(Param_Type, row),
                   BankName = ToolsBO.ProcessResultString(Param_BankName, row),
                   CLABE = ToolsBO.ProcessResultString(Param_CLABE, row),
                   Name = ToolsBO.ProcessResultString(Param_Name, row),
                   Reference = ToolsBO.ProcessResultString(Param_Reference, row),
                   BarcodeURL = ToolsBO.ProcessResultString(Param_BarcodeURL, row),
                   PaymentAddress = ToolsBO.ProcessResultString(Param_PaymentAddress, row),
                   PaymentUrlBip21 = ToolsBO.ProcessResultString(Param_PaymentUrlBip21, row),
                   AmountBitcoins = ToolsBO.ProcessResultDecimal(Param_AmountBitcoins, row)
                });
            }

            return result_items;
        }


        #endregion datatable to model


        #endregion methods
    }
}