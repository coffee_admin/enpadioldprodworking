﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Enpadi_WebUI.Models;

namespace Enpadi_WebUI.DAO
{
    public class DataBaseConnector
    {

        private SqlConnection _connection = new SqlConnection();
        private String _connectionString = (Boolean.Parse(ConfigurationManager.AppSettings["ModoProduccion"]) == true ? ConfigurationManager.ConnectionStrings["Enpadi_DB_Connection"].ToString() : ConfigurationManager.ConnectionStrings["Test_DB_Connection"].ToString());

        public DataBaseConnector() { }

        public void OpenConnection()
        {

            try
            {

                _connection = new SqlConnection(_connectionString);
                _connection.Open();

            }
            catch (SqlException e)
            {

            }
        }

        public void CloseConnection()
        {

            try
            {

                if (_connection != null)
                {
                    _connection.Close();
                }
            }
            catch (SqlException e) { }
        }

        private DataTable ExecuteStoredProcedure(String spName)
        {
            try
            {
                SqlCommand command = new SqlCommand(spName, _connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter reader = new SqlDataAdapter(command);
                DataTable results = new DataTable();

                reader.Fill(results);
                reader.Dispose();

                return results;
            }
            catch (Exception e)
            {
                throw new ArgumentException(SystemErrorModel.ErrorList.ErrorSPappInconsistency + e.Message);
            }
        }

        private DataTable ExecuteStoredProcedure(String spName, SqlParameter[] parameters)
        {
            try
            {
                SqlCommand command = new SqlCommand(spName, _connection);
                command.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }

                SqlDataAdapter reader = new SqlDataAdapter(command);
                DataTable results = new DataTable();

                reader.Fill(results);
                reader.Dispose();

                return results;
            }
            catch (Exception e)
            {
                throw new ArgumentException(SystemErrorModel.ErrorList.ErrorSPappInconsistency + e.Message);
            }
        }

        public static DataTable ExecuteStoreProcedure(string spName, SqlParameter[] parameters = null)
        {
            try
            {
                DataTable result = new DataTable();
                DataBaseConnector connector = new DataBaseConnector();
                connector.OpenConnection();

                if (parameters == null)
                {
                    result = connector.ExecuteStoredProcedure(spName);
                }
                else
                {
                    result = connector.ExecuteStoredProcedure(spName, parameters);
                }

                connector.CloseConnection();

                return result;
            }
            catch (Exception e)
            {
                throw new ArgumentException(SystemErrorModel.ErrorList.ErrorSPappInconsistency + e.Message);
            }
        }
    }
}