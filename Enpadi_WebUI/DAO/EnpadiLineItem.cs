﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Enpadi_WebUI.Models;
using System.Data;

namespace Enpadi_WebUI.DAO
{
    public class EnpadiLineItem
    {

        #region constants


        #region sp names

        private const String SaveStoreProcedureName = "EnpadiLineItemInsert";
        private const String GetStoreProcedureName = "EnpadiLineItemGet";


        #endregion sp names

        #region parameters

        private const String Param_id = "@id";
        private const String Param_orderId = "@orderId";
        private const String Param_unit_price = "@unit_price";
        private const String Param_quantity = "@quantity";

        private const String Param_name = "@name";
        private const String Param_description = "@description";
        private const String Param_customer_id = "@customer_id";
        private const String Param_customer_email = "@customer_email";

        private const String Param_created_at = "@created_at";

        #endregion parameters

        #region members


        #region parameters

        private const String Member_id = "id";
        private const String Member_orderId = "orderId";
        private const String Member_unit_price = "unit_price";
        private const String Member_quantity = "quantity";

        private const String Member_name = "name";
        private const String Member_description = "description";
        private const String Member_customer_id = "customer_id";
        private const String Member_customer_email = "customer_email";

        private const String Member_created_at = "created_at";


        #endregion parameters

        #endregion members

        #endregion constants

        #region methods

        #region save

        public static void Save(
            Guid id,
            Guid OrderId,
            Decimal Unit_price,
            Int32 Quantity,
            String Name,
            String Description,
            long customer_id,
            String customer_email
        )
        {
            SqlParameter[] parameters = {
                new SqlParameter(Param_id,id),
                new SqlParameter(Param_orderId,OrderId),
                new SqlParameter(Param_unit_price,Unit_price),
                new SqlParameter(Param_quantity,Quantity),
                new SqlParameter(Param_name,Name),

                new SqlParameter(Param_description,Description),
                new SqlParameter(Param_customer_id,customer_id),
                new SqlParameter(Param_customer_email,customer_email),
                new SqlParameter(Param_created_at,ToolsBO.DateTimeToTimeStamp(DateTime.Now.AddDays(5)))

            };

            DataBaseConnector.ExecuteStoreProcedure(SaveStoreProcedureName, parameters);
        }

        public static void Save(
            EnpadiLineItemModel item
        )
        {
            Save(
             item.id,
             item.orderId.GetValueOrDefault(),
             item.unit_price.GetValueOrDefault(),
             item.quantity.GetValueOrDefault(),
             item.name,
             item.description,
             item.customer_id.GetValueOrDefault(),
             item.customer_email
             );
        }

        public static void Save(
            List<EnpadiLineItemModel> items
        )
        {
            foreach (EnpadiLineItemModel item in items)
            {
                Save(item);
            }
        }

        #endregion save

        #region get

        public static EnpadiLineItemModel Get(Int64 id)
        {
            DataTable result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_id, id)

            };

            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName, parameters);

            return PorcessResult(result).FirstOrDefault();
        }

        public static List<EnpadiLineItemModel> GetAll()
        {
            DataTable result = new DataTable();
            result = DataBaseConnector.ExecuteStoreProcedure(GetStoreProcedureName);

            return PorcessResult(result);
        }

        #endregion get

        #region datatable to model

        private static List<EnpadiLineItemModel> PorcessResult(DataTable items)
        {

            List<EnpadiLineItemModel> result_items = new List<EnpadiLineItemModel>();

            foreach (DataRow row in items.Rows)
            {
                result_items.Add(new EnpadiLineItemModel
                {
                    id = ToolsBO.ProcessResultGuid(Member_id, row),
                    orderId = ToolsBO.ProcessResultGuid(Member_orderId, row),
                    unit_price = ToolsBO.ProcessResultDecimal(Member_unit_price, row),
                    quantity = ToolsBO.ProcessResultInt(Member_quantity, row),
                    name = ToolsBO.ProcessResultString(Member_name, row),
                    description = ToolsBO.ProcessResultString(Member_description, row),
                    customer_email = ToolsBO.ProcessResultString(Member_customer_email, row),
                    customer_id = ToolsBO.ProcessResultInt(Member_customer_id, row),
                    created_at = ToolsBO.ProcessResultTimeStampToDateTime2(Member_created_at, row)

                });
            }

            return result_items;
        }


        #endregion datatable to model


        #endregion methods
    }
}