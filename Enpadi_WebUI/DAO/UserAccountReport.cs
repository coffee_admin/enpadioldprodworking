﻿using Enpadi_WebUI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.DAO
{
    public class UserAccountReport
    {

        #region constants

        #region sp names

        private const String UserAccountGeneralReportProcedureName = "UserAccountReport";

        #endregion sp names

        #region parameters

        private const String Param_tarjetaID = "@tarjetaID";
        private const String Param_periodo = "@periodo";
   
        #endregion parameters

        #region members

        private const String Member_transaccion_transID = "transaccion_transID";
        private const String Member_origin_descripcion = "origin_descripcion";
        private const String Member_type_descripcion = "type_descripcion";
        private const String Member_transaccion_socioID = "transaccion_socioID";
        private const String Member_partner_nombreComercial = "partner_nombreComercial";
        private const String Member_transaccion_terminalID = "transaccion_terminalID";
        private const String Member_currency_codigo = "currency_codigo";
        private const String Member_transaccion_tarjetaID = "transaccion_tarjetaID";
        private const String Member_transaccion_tarjeta = "transaccion_tarjeta";
        private const String Member_transaccion_monto = "transaccion_monto";
        private const String Member_transaccion_comision = "transaccion_comision";
        private const String Member_transaccion_iva = "transaccion_iva";
        private const String Member_transaccion_transaccion = "transaccion_transaccion";
        private const String Member_transaccion_concepto = "transaccion_concepto";
        private const String Member_transaccion_descripcion = "transaccion_descripcion";
        private const String Member_transaccion_referencia = "transaccion_referencia";
        private const String Member_status_descripcion = "status_descripcion";
        private const String Member_transaccion_aprobacion = "transaccion_aprobacion";
        private const String Member_transaccion_ip = "transaccion_ip";
        private const String Member_transaccion_fechaRegistro = "transaccion_fechaRegistro";

        #endregion members

        #endregion constants

        #region methods

        #region get

        public static List<UserAccountReportModel> GetUserAccountGeneralReport(Int32 tarjetaID, String periodo)
        {
            DataTable result = new DataTable();

            SqlParameter[] parameters = {
                new SqlParameter(Param_tarjetaID, tarjetaID),
                new SqlParameter(Param_periodo, periodo)

            };

            result = DataBaseConnector.ExecuteStoreProcedure(UserAccountGeneralReportProcedureName, parameters);

            return PorcessResult(result);
        }

        #endregion get

        #region datatable to model

        private static List<UserAccountReportModel> PorcessResult(DataTable items)
        {

            List<UserAccountReportModel> result_items = new List<UserAccountReportModel>();

            foreach (DataRow row in items.Rows)
            {
                result_items.Add(new UserAccountReportModel
                {
                    transaccion_transID= ToolsBO.ProcessResultInt(Member_transaccion_transID, row),
                    origin_descripcion= ToolsBO.ProcessResultString(Member_origin_descripcion, row),
                    type_descripcion= ToolsBO.ProcessResultString(Member_type_descripcion, row),
                    transaccion_socioID= ToolsBO.ProcessResultInt(Member_transaccion_socioID, row),
                    partner_nombreComercial= ToolsBO.ProcessResultString(Member_partner_nombreComercial, row),
                    transaccion_terminalID= ToolsBO.ProcessResultInt(Member_transaccion_terminalID, row),
                    currency_codigo= ToolsBO.ProcessResultString(Member_currency_codigo, row),
                    transaccion_tarjetaID= ToolsBO.ProcessResultInt(Member_transaccion_tarjetaID, row),
                    transaccion_tarjeta= ToolsBO.ProcessResultString(Member_transaccion_tarjeta, row),
                    transaccion_monto= ToolsBO.ProcessResultDecimal(Member_transaccion_monto, row),
                    transaccion_comision= ToolsBO.ProcessResultDecimal(Member_transaccion_comision, row),
                    transaccion_iva= ToolsBO.ProcessResultDecimal(Member_transaccion_iva, row),
                    transaccion_transaccion= ToolsBO.ProcessResultString(Member_transaccion_transaccion, row),
                    transaccion_concepto= ToolsBO.ProcessResultString(Member_transaccion_concepto, row),
                    transaccion_descripcion= ToolsBO.ProcessResultString(Member_transaccion_descripcion, row),
                    transaccion_referencia= ToolsBO.ProcessResultString(Member_transaccion_referencia, row),
                    status_descripcion= ToolsBO.ProcessResultString(Member_status_descripcion, row),
                    transaccion_aprobacion= ToolsBO.ProcessResultString(Member_transaccion_aprobacion, row),
                    transaccion_ip= ToolsBO.ProcessResultString(Member_transaccion_ip, row),
                    transaccion_fechaRegistro= ToolsBO.ProcessResultDateTime(Member_transaccion_fechaRegistro, row)
                });
            }

            return result_items;
        }

        #endregion datatable to model

        #endregion methods
    }
}