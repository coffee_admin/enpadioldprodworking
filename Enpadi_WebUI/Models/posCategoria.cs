﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enpadi_WebUI.Models
{
    [Table("posCategorias")]
    public class posCategoria
    {
        [Key]
        public int categoriaID { get; set; }

        [Display(Name="Tienda")]
        public int tiendaID { get; set; }
        public virtual posTienda posTienda { get; set; }

        [Display(Name="Categoria")]
        public string categoria { get; set; }

        [Display(Name="Descripción")]
        public string descripcion { get; set; }

        [Display(Name="Imagen")]
        public string imagen { get; set; }
    }
}