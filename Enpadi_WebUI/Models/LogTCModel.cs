﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enpadi_WebUI.DAO;

namespace Enpadi_WebUI.Models
{
    public class LogTCModel : LogTC
    {
        #region properties

        public int idLog { get; set; }
        public int idTarjeta { get; set; }
        public int result { get; set; }
        public DateTime fechaAlta { get; set; }
        public DateTime? fechaBaja { get; set; }

        #endregion properties

    }
}