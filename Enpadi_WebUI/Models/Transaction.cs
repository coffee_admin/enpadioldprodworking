﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    [Table("Transaction")]
    public class Transaction
    {
        [Key]
        public Guid Id { get; set; }
        public Guid PartnerId { get; set; }
        public virtual socio Partner { get; set; }
        public Guid CurrencyId { get; set; }
        public virtual moneda Currency { get; set; }
        public Guid TransactionTypeId { get; set; }
        public virtual TransactionTypeModel TransactionType { get; set; }
        public Guid EnpadiCardId { get; set; }
        //public virtual tarjetas EnpadiCard { get; set; }

        public Guid PlatformId { get; set; }
        public virtual PlatformModel Platform { get; set; }
        public Guid UserTypeId { get; set; }
        public virtual UserTypeModel UserType { get; set; }
        public Guid UserBuyerId { get; set; }
        public Guid StatusId { get; set; }
        public virtual StatusModel Status { get; set; }

        public Int32 TerminalNumber { get; set; }

        public String Concept { get; set; }
        public String Description { get; set; }
        public String Reference { get; set; }
        public String ExternalTransaccionId { get; set; }
        public String ApprovalIdentifier { get; set; }
        public String UserIpAddress { get; set; }

        public Decimal Amount { get; set; }
        public Decimal TaxAmount { get; set; }
        public Decimal? AmountPaid { get; set; }

        public Boolean LiveMode { get; set; }
        public Boolean PaitOut { get; set; }

        public DateTime? PaitOutAt { get; set; }
        public DateTime? DeletedAt { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}