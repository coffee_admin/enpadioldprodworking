﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    public class paystubTypeModel
    {
        public Guid id { get; set; }
        public String name { get; set; }
        public String description { get; set; }
    }
}