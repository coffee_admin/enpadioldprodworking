﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enpadi_WebUI.DAO;

namespace Enpadi_WebUI.Models
{
    public class SystemErrorModel : SystemError
    {

        #region consts

        public static class ErrorList
        {

            private const String Error = "Error Id ";

            /**
             * Error range 1000-1999 sp and database related
             * Error range 2000-2999 payment with partner fail
             * Error range 3000-3999 process enpadi order
             * Error range 4000-4999 webhook, post and connection related
             * Error range 5000-5999 user related
             * */

            #region generic error 0-999

            public const String GenericError = Error + "0: Generic error, description ";

            #endregion generic error

            #region sp and database 1000-1999

            public const String ErrorSPappInconsistency = Error + "1000: store procedure mismatch with model";

            #endregion sp and database

            #region payment 2000 - 2999

            public const String PaymentWithBankFail = Error + "2000: Payment with bank fail with. ";
            public const String PaymentWithStoreFail = Error + "2001: Payment with store fail with. ";
            public const String PaymentWithCreditCardFail = Error + "2002: Payment with credit card fail with. ";
            public const String PaymentMethodNotRecognized = Error + "2003: Payment method not recognized or not available. ";
            public const String PaymentTryLimitExceeded = Error + "2004: Has excedido el Limite de Recargas Autorizado por Semana. ";
            public const String PaymentCreditCardDeclined = Error + "2005: Declinada. ";
            public const String PaymentUserNotFound = Error + "2006: No se ha encontrado el Usuario Seleccionado. ";
            public const String PaymentCreditCardNotFound = Error + "2007: No se ha encontrado la Tarjeta Seleccionada. ";
            public const String PaymentOrderNotSaveInDataBase = Error + "2008: Order not found on database. ";
            public const String PaymentCustomerNotFoundOnEnpadi = Error + "2009: Customer nor found on Enpadi";

            #endregion payment

            #region process enpadi order 3000-3999

            public const String AmountInChargeAndOrderMismatch = Error + "3000: Charge and order amount most match. ";

            #endregion process enpadi order 

            #region webhook, post and connection related 4000-4999

            public const String WebHookGenericError = Error + "4000: Webhook generic error with message ";

            #endregion webhook, post and connection related

            #region user errors 5000-5999

            public const String UserErrorRoleNotFoundNumber = "5000";
            public const String UserErrorRoleNotFound = Error + UserErrorRoleNotFoundNumber + " User role nor found ";
            public const String UserEnpadiCardNotFound = Error + "5001: user Enpadi card not found";

            #endregion user errors
        }

        #endregion const

        #region constructors

        public SystemErrorModel() { }

        public SystemErrorModel(String ErrorNumber, String SpName, String Description, String AditionalInfo, Boolean ErrorFound)
        {
            this.ErrorNumber = ErrorNumber;
            this.SpName = SpName;
            this.Description = Description;
            this.AditionalInfo = AditionalInfo;
            this.ErrorFound = ErrorFound;
        }

        #endregion constructors

        #region members

        Guid _id = Guid.NewGuid();
        String _description = String.Empty;
        String _errorNumber = String.Empty;
        String _spName = "No StoreProcedure";
        String _aditionalInfo = String.Empty;
        String _fileName = String.Empty;
        Boolean _errorFound = false;
        DateTime _dateCreated = DateTime.Now;

        #endregion members

        #region properties

        public Guid Id
        {
            get { return _id; }
            set
            {
                _id = value;
            }
        }


        public String FileName
        {
            get { return _fileName; }
            set
            {
                if (value == String.Empty)
                {
                    _fileName = value;
                }
                else
                {
                    _fileName += " " + value;
                }
            }
        }

        public String Description
        {
            get { return _description; }
            set
            {
                if (value == String.Empty)
                {
                    _description = value;
                }
                else
                {
                    _description += " " + value;
                }
            }
        }
        public String ErrorNumber
        {
            get { return _errorNumber; }
            set
            {
                if (value == String.Empty)
                {
                    _errorNumber = value;
                }
                else
                {
                    _errorNumber += " " + value;
                }
            }
        }

        /// <summary>
        /// Do not assign anything if you want the default value "No StoreProcedure" to be assigned
        /// </summary>
        public String SpName
        {
            get { return _spName; }
            set
            {
                if (value == String.Empty)
                {
                    _spName = value;
                }
                else
                {
                    _spName += " " + value;
                }
            }
        }
        public String AditionalInfo
        {
            get { return _aditionalInfo; }
            set
            {
                if (value == String.Empty)
                {
                    _aditionalInfo = value;
                }
                else
                {
                    _aditionalInfo += " " + value;
                }
            }
        }

        public Boolean ErrorFound
        {
            get { return _errorFound; }
            set { _errorFound = value; }
        }

        public DateTime DateCreated
        {
            get { return _dateCreated; }
            set { _dateCreated = value; }
        }

        #endregion properties
    }
}