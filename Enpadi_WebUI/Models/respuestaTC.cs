﻿namespace Enpadi_WebUI.Models
{
    public class respuestaTC
    {
        public string Estatus { get; set; }
        public string Autorizacion { get; set; }
        public string Total { get; set; }

        public string Comision { get; set; }
    }
}