﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models.ViewModel
{
    public class TestModelView
    {

        #region constuctors

        #endregion constructors

        #region encryption test

        public String Text { get; set; }
        public String Encripted { get; set; }
        public String Decripted { get; set; }
        public String PrivateKey { get; set; }
        public String PublicKey { get; set; }

        #endregion encryption test

    }
}