﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    [Table("ChargeOption")]
    public class ChargeOption
    {
        [NotMapped]
        public static class Keys {
            public static readonly Guid NewToIMSS = Guid.Parse("0FC5DACA-E7B6-4B58-B251-2C4CC55D19F9");
            public static readonly Guid RenewalToIMSS = Guid.Parse("9B74F735-C68F-4CED-94BC-905C29DC4B9F");
        }

        [Key]
        public Guid Id { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public Decimal Amount { get; set; }
        public Guid CurrencyId { get; set; }
        public virtual moneda Currency { get; set; }
    }
}