﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Text;
using conekta;

namespace Enpadi_WebUI.Models
{
    public class EncryptionModel
    {
        #region private variables

        //This is the public key
        private string _public_key = String.Empty;

        //This is the private and public key.
        private String _private_key = String.Empty;

        //keysize for generating keys
        private const int keySize = 2048;

        #endregion private variables

        #region getts and setters

        public string Public_key { get => _public_key; set => _public_key = value; }

        public string Private_key { get => _private_key; set => _private_key = value; }

        #endregion gettes and setters

        #region methods

        /// <summary>
        /// Generates private and public keys for this object
        /// </summary>
        public void GenerateKeys()
        {
            using (var provider = new RSACryptoServiceProvider(keySize))
            {
                _public_key = provider.ToXmlString(false);
                _private_key = provider.ToXmlString(true);
            }
        }

        /// <summary>
        /// Encryption of message
        /// </summary>
        /// <param name="unEncrypted">Un encrypted message</param>
        /// <returns></returns>
        public String Encript(String unEncrypted)
        {
            RSACryptoServiceProvider cipher = null;
            cipher = new RSACryptoServiceProvider();
            cipher.FromXmlString(_public_key);
            String resutl = String.Empty;

            byte[] data = Encoding.UTF8.GetBytes(unEncrypted);
            byte[] cipherText = cipher.Encrypt(data, false);
            resutl = Convert.ToBase64String(cipherText);

            return resutl;
        }
        //    decryptText();

        /// <summary>
        /// Decription with a private key, first need to call GenerateKeys method for the object 
        /// or set the public and private keys as string
        /// </summary>
        /// <param name="encrypted"> encrypted message</param>
        /// <returns></returns>
        public String DeEncrypt(String encrypted)
        {
            RSACryptoServiceProvider cipher = null;
            cipher = new RSACryptoServiceProvider();
            cipher.FromXmlString(_private_key);
            String result = String.Empty;

            byte[] ciphterText = Convert.FromBase64String(encrypted);
            byte[] plainText = cipher.Decrypt(ciphterText, false);
            result = Encoding.UTF8.GetString(plainText);

            return result;
        }

        #endregion methods
    }
}