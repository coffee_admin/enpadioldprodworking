﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    public class WebHookModel
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objToSend"></param>
        /// <param name="clientEmail"></param>
        /// <param name="data_id">the id of the order or related object that is been send, required to store it in database and keep record</param>
        /// <returns></returns>
        public static String SendWebHookNotification(object objToSend, string clientEmail, Guid data_id, Boolean livemode)
        {
            try
            {
                var contextDB = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
                var storeDB = new UserStore<IdentityUser>(contextDB);
                var managerDB = new UserManager<IdentityUser>(storeDB);
                var userDB = managerDB.FindByEmail(clientEmail);


                ClientEncryptionInfoModel clientEncryptionInfo = new ClientEncryptionInfoModel();
                clientEncryptionInfo = ClientEncryptionInfoModel.Get(Guid.Parse(userDB.Id));

                string url = "http://localhost:45093/Test/Post";
                //String jsonObject = JsonConvert.SerializeObject(
                //           new
                //           {
                //               jsonCreditApplication = objToSend
                //           });
                //var dict = JObject.Parse(jsonObject);

                EnpadiEventModel new_event = new EnpadiEventModel();
                new_event.data = objToSend;
                new_event.data_id = data_id;
                new_event.object_description = "Order Post send to " + clientEmail + " via " + " " + url;
                new_event.webhook_status = EnpadiEventModel.Status.Pending;
                new_event.livemode = livemode;

                String jsonObject = JsonConvert.SerializeObject(
                           new
                           {
                               jsonCreditApplication = new_event
                           });

                EnpadiEventModel.Save(new_event);

                /******************************************************/

                //// Create a request using a URL that can receive a post. 
                //WebRequest request = WebRequest.Create(url);
                //// Set the Method property of the request to POST.
                //request.Method = "POST";
                //// Create POST data and convert it to a byte array.
                ////string postData = "This is a test that posts this string to a Web server.";
                //byte[] byteArray = Encoding.UTF8.GetBytes(jsonObject);
                //// Set the ContentType property of the WebRequest.
                //request.ContentType = "text/xml";
                //// Set the ContentLength property of the WebRequest.
                //request.ContentLength = byteArray.Length;
                //// Get the request stream.
                //Stream dataStream = request.GetRequestStream();
                //// Write the data to the request stream.
                //dataStream.Write(byteArray, 0, byteArray.Length);
                //// Close the Stream object.
                //dataStream.Close();
                //// Get the response.
                //WebResponse response = request.GetResponse();
                //// Display the status.
                //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                //// Get the stream containing content returned by the server.
                //dataStream = response.GetResponseStream();
                //// Open the stream using a StreamReader for easy access.
                //StreamReader reader = new StreamReader(dataStream);
                //// Read the content.
                //string responseFromServer = reader.ReadToEnd();
                //// Display the content.
                //Console.WriteLine(responseFromServer);
                //// Clean up the streams.
                //reader.Close();
                //dataStream.Close();
                //response.Close();



                var webRequest = System.Net.WebRequest.Create(url);
                if (webRequest != null)
                {
                    webRequest.Method = "POST";
                    webRequest.Timeout = 20000;
                    webRequest.ContentType = "application/json";

                    using (System.IO.Stream s = webRequest.GetRequestStream())
                    {
                        using (System.IO.StreamWriter sw = new System.IO.StreamWriter(s))
                            sw.Write(jsonObject);
                    }

                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                        {
                            var jsonResponse = sr.ReadToEnd();
                            System.Diagnostics.Debug.WriteLine(String.Format("Response: {0}", jsonResponse));
                        }
                    }
                }

                new_event.webhook_status = EnpadiEventModel.Status.Acepted;
                EnpadiEventModel.Save(new_event);
                /****************************************************************/

                return jsonObject;
            }
            catch (Exception e)
            {
                throw new ArgumentException(SystemErrorModel.ErrorList.WebHookGenericError + e.Message);
            }
        }
    }
}