﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enpadi_WebUI.Models
{
    [Table("Productos")]
    public class producto
    {
        [Key]
        [Display(Name="Producto")]
        public int productoID { get; set; }

        [Display(Name="Producto")]
        public string nombre { get; set; }

        [Display(Name="Descripcion")]
        public string descripcion { get; set; }

        [Display(Name="BIN")]
        [DisplayFormat(DataFormatString="{0:D6}", ApplyFormatInEditMode=true)]
        [Range(100000, 999999)]
        public int bin { get; set; }

        [Display(Name="Producto")]
        [DisplayFormat(DataFormatString="{0:D4}", ApplyFormatInEditMode=true)]
        [Range(1,9999)]
        public int identificador { get; set; }
        
        [Display(Name="Inicia")]
        [DisplayFormat(DataFormatString="{0:D5}", ApplyFormatInEditMode=true)]
        [Range(1,99999)]
        public int inicial { get; set; }
        
        [Display(Name="Termina")]
        [DisplayFormat(DataFormatString="{0:D5}", ApplyFormatInEditMode=true)]
        [Range(1,99999)]
        public int final { get; set; }

        [Display(Name="Activo")]
        public bool activo { get; set; }
    }
}