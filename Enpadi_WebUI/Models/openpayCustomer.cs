﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enpadi_WebUI.Models
{
    [Table("opCustomers")]
    public class openpayCustomer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [ScaffoldColumn(false)]
        public string CustomerID { get; set; }

        [Display(Name="Cuenta")]
        public int ExternalID { get; set; }

        [Required(ErrorMessage = "El nombre es requerido.")]
        [Display(Name = "Nombre(s)")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Los apellidos son requeridos.")]
        [Display(Name = "Apellido(s)")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "El correo eletrónico es requerido.")]
        [EmailAddress]
        [Display(Name = "Correo Electronico")]
        public string Email { get; set; }

        [Required(ErrorMessage = "El teléfono es requerido.")]
        [MaxLength(10, ErrorMessage = "El Número de Telefono debe constar de 10 Digitos")]
        [MinLength(10, ErrorMessage = "El Número de Telefono debe constar de 10 Digitos")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Número de Telefono Incorrecto")]
        [Display(Name = "Telefono")]
        public string PhoneNumber { get; set; }

        [ScaffoldColumn(false)]
        public bool RequiresAccount { get; set; }

        [Required(ErrorMessage = "La direccion es requerida.")]
        [Display(Name = "Dirección")]
        public string Address1 { get; set; }

        [Display(Name = " ")]
        public string Address2 { get; set; }

        [Display(Name = " ")]
        public string Address3 { get; set; }

        [Required(ErrorMessage = "La ciudad es requerida.")]
        [Display(Name = "Ciudad")]
        public string City { get; set; }

        [Required(ErrorMessage = "El estado es requerido.")]
        [Display(Name = "Estado")]
        public string State { get; set; }

        [Required(ErrorMessage = "El país es requerido.")]
        [Display(Name = "Pais")]
        public string CountryCode { get; set; }

        [Required(ErrorMessage = "El código postal es requerido.")]
        [MaxLength(5, ErrorMessage = "El Codigo Postal debe estar compuesto de 5 Digitos")]
        [MinLength(5, ErrorMessage = "El Codigo Postal debe estar compuesto de 5 Digitos")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Codigo Postal Incorrecto")]
        [Display(Name = "Codigo Postal")]
        public string PostalCode { get; set; }

        [ScaffoldColumn(false)]
        public string CLABE { get; set; }
    }
}