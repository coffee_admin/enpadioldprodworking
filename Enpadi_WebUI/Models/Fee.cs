﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    [Table("Fee")]
    public class Fee
    {

        #region notmapped

        //static ApplicationDbContext db = new ApplicationDbContext();

        [NotMapped]
        public static readonly String strNameUserBasic = "Enpadi basic kiosko user";
        [NotMapped]
        public static readonly String strNameUserPremium = "Enpadi premium kiosko user";
        [NotMapped]
        public static readonly String strNameUserSuperPremium = "Enpadi super premium kiosko user";

        [NotMapped]
        public static readonly String strDescriptionUserBasic = "Basic fee foo an Enpadi kiosko user";
        [NotMapped]
        public static readonly String strDescriptionUserPremium = "Premium fee for an Enpadi kiosko user";
        [NotMapped]
        public static readonly String strDescriptionUserSuperPremium = "Super premium fee for an Enpadi kiosko user";

        [NotMapped]
        public static readonly String strNameEnpadiBasic = "Enpadi basic";
        [NotMapped]
        public static readonly String strNameEnpadiPremium = "Enpadi premium";
        [NotMapped]
        public static readonly String strNameEnpadiSuperPremium = "Enpadi super premium";

        [NotMapped]
        public static readonly String strDescriptionEnpadiBasic = "Fee for a basic service";
        [NotMapped]
        public static readonly String strDescriptionEnpadiPremium = "Fee for a premium service";
        [NotMapped]
        public static readonly String strDescriptionEnpadiSuperPremium = "Fee for a super premium service";

        [NotMapped]
        public class Keys
        {
            public static readonly Guid EnpadiBasic = Guid.Parse("C4999511-65FE-49E4-8E86-9B5A5A39E431");
            public static readonly Guid EnpadiPremium = Guid.Parse("B1CA4960-EC16-42AE-8167-CBFBDA452121");
            public static readonly Guid EnpadiSuperPremium = Guid.Parse("ED59B139-CC94-4DDE-9B52-BDAD83D544CB");

            public static readonly Guid UserBasic = Guid.Parse("80D76458-357A-4FDF-83E5-33BEEEFD9F31");
            public static readonly Guid UserPremium = Guid.Parse("80D08BBC-8E8C-47A6-8406-92712392B518");
            public static readonly Guid UserSuperPremium = Guid.Parse("D84955AB-C1C1-465F-85E2-05B10A23EC30");

            public static readonly Guid masterCardFeeId = Guid.Parse("BBB6916D-DDF1-4144-8CDB-38C616CAA7D6");
            public static readonly Guid VisaFeeId = Guid.Parse("B103AD41-0B42-4575-83A8-39D6106C5609");
            public static readonly Guid AmexFeeId = Guid.Parse("7436722B-6B5C-4855-BA7A-97332AE17CF8");

            public static readonly Guid ChargeForService = Guid.Parse("E012634B-BC17-42AA-A1C4-510ED78207EA");
        }

        [NotMapped]
        public String PartnerName { get; set; }
        [NotMapped]
        public String UserToPayEmail { get; set; }
        [NotMapped]
        public String CurrencyCode { get; set; }
        [NotMapped]
        public String PlatformName { get; set; }
        [NotMapped]
        public String ServiceCategoryName { get; set; }

        #endregion notmapped

        #region constructors

        public Fee()
        {
            this.Id = Guid.NewGuid();
        }

        #endregion constructors
        [Key]
        public Guid Id { get; set; }


        public String Name { get; set; }
        public String Description { get; set; }
        public Decimal VariableFee { get; set; }
        [Display(Name = "Fix fee")]
        public Decimal FixFee { get; set; }

        [Display(Name = "Tax fee")]
        public Decimal TaxFee { get; set; }

        [Display(Name = "Upper restriction")]
        public Decimal UpperRestriction { get; set; }

        [Display(Name = "Lower restriction")]
        public Decimal LowerRestriction { get; set; }

        public Guid? PartnerToPayId { get; set; }

        public Boolean AllowDelete { get; set; }
        public Guid CreatedByUserId { get; set; }
        public Guid CurrencyId { get; set; }
        //public virtual moneda Currency { get; set; }

        public Guid? UserToPayId { get; set; }

        //public Guid? UserToChargeId { get; set; }

        public Guid ServiceCategoryId { get; set; }

        public Guid PlatformId { get; set; }

        #region methods

        #region get fees

        #region get platform fees

        /// <summary>
        /// Automatically get the region of the web config file and get the selected user fees
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="serviceCategoryId"></param>
        /// <returns></returns>
        public static List<Fee> GetPlatformUserFeesForSeviceCategory(Guid? userId, Guid? serviceCategoryId)
        {

            List<Fee> userFeesForServiceCategory = null;

            String region = PlatformRegion.GetPlatformRegion();

            switch (region)
            {
                case PlatformRegion.Regions.dev:
                    break;
                case PlatformRegion.Regions.usa:
                    break;
                case PlatformRegion.Regions.kiosko:
                    break;
                case PlatformRegion.Regions.usaAPI:
                    userFeesForServiceCategory = Fee.GetUsaApiServiceFees((Guid)userId, (Guid)serviceCategoryId);
                    break;
                case PlatformRegion.Regions.mxAPI:
                    userFeesForServiceCategory = Fee.GetMxApiServiceFees((Guid)userId, (Guid)serviceCategoryId);
                    break;
                case PlatformRegion.Regions.mx:
                    break;
                default:
                    break;
            }

            return userFeesForServiceCategory;
        }

        //public static List<Fee> GetPlatformServiceFees(Guid? userId, Guid? serviceCategoryId)
        //{

        //    List<Fee> serviceFees = new List<Fee>();

        //    String region = PlatformRegion.GetPlatformRegion();

        //    switch (region)
        //    {
        //        case PlatformRegion.Regions.dev:
        //            break;
        //        case PlatformRegion.Regions.usa:

        //            break;
        //        case PlatformRegion.Regions.kiosko:
        //            break;
        //        case PlatformRegion.Regions.usaAPI:
        //            break;
        //        case PlatformRegion.Regions.mxAPI:
        //            break;
        //        case PlatformRegion.Regions.mx:
        //            break;
        //        default:
        //            break;
        //    }
        //}

        /// <summary>
        /// Get the fee for the user of a service category (basic, premium or super premium)
        /// it's a list on purpose, the user my have and extra fee for some reason (2 basic fees for example)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="serviceCategoryId"></param>
        /// <returns></returns>
        public static List<Fee> GetMxApiServiceFees(Guid userId, Guid serviceCategoryId)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            List<Fee> serviceFees = new List<Fee>();
            serviceFees = (from item in db.Fee
                           join servFeePlat in db.ServiciosFeePlatfom on item.Id equals servFeePlat.FeeId
                           where
                           servFeePlat.UserId == userId
                           &&
                           item.PlatformId == PlatformModel.Keys.MxApi
                           &&
                           item.ServiceCategoryId == serviceCategoryId
                           select item).ToList();

            if (serviceFees.Count() <= 0)
            {
                Fee.AddFeesMxApiUser(userId);

                serviceFees = (from item in db.Fee
                               join servFeePlat in db.ServiciosFeePlatfom on item.Id equals servFeePlat.FeeId
                               where
                               servFeePlat.UserId == userId
                               &&
                               item.PlatformId == PlatformModel.Keys.MxApi
                               &&
                               item.ServiceCategoryId == serviceCategoryId
                               select item).ToList();
            }

            return serviceFees;
        }

        /// <summary>
        /// Get the fee for the user of a service category (basic, premium or super premium)
        /// it's a list on purpose, the user my have and extra fee for some reason (2 basic fees for example)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="serviceCategoryId"></param>
        /// <returns></returns>
        public static List<Fee> GetUsaApiServiceFees(Guid userId, Guid serviceCategoryId)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            List<Fee> serviceFees = new List<Fee>();
            serviceFees = (from item in db.Fee
                           join servFeePlat in db.ServiciosFeePlatfom on item.Id equals servFeePlat.FeeId
                           where
                           servFeePlat.UserId == userId
                           &&
                           item.PlatformId == PlatformModel.Keys.UsaApi
                           &&
                           item.ServiceCategoryId == serviceCategoryId
                           select item).ToList();

            if (serviceFees.Count() <= 0)
            {
                Fee.AddFeesUsaApiUser(userId);

                serviceFees = (from item in db.Fee
                               join servFeePlat in db.ServiciosFeePlatfom on item.Id equals servFeePlat.FeeId
                               where
                               servFeePlat.UserId == userId
                               &&
                               item.PlatformId == PlatformModel.Keys.UsaApi
                               &&
                               item.ServiceCategoryId == serviceCategoryId
                               select item).ToList();
            }

            return serviceFees;
        }

        public static List<Fee> GetUsaWebUserFees()
        {
            ApplicationDbContext db = new ApplicationDbContext();

            //List<Fee> UserFees = new List<Fee>();
            List<Fee> UserFees = (from item in db.Fee
                                  where
                                  item.PlatformId == PlatformModel.Keys.WebUsa
                                  select item).ToList();
            return UserFees;
        }

        /// <summary>
        /// Get the specific user fees for the selected platform
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="platformId"></param>
        /// <returns></returns>
        public static List<Fee> GetPlatformUserFees(Guid userId, Guid platformId)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            //List<Fee> UserFees = new List<Fee>();
            List<Fee> UserFees = (from item in db.Fee
                                  where
                                  item.UserToPayId == userId
                                  &&
                                  item.PlatformId == platformId
                                  select item).ToList();

            if (UserFees.Count <= 0)
            {

                Fee basicServiceFee = new Fee
                {
                    Id = Guid.NewGuid(),
                    Name = strNameUserBasic,
                    Description = strDescriptionUserBasic,
                    VariableFee = 0,
                    FixFee = 2,
                    TaxFee = 0,
                    UpperRestriction = 0,
                    LowerRestriction = 0,
                    PartnerToPayId = null,
                    CreatedByUserId = ApplicationUser.Key.AdminUserId,
                    CurrencyId = moneda.currency.UsdId,
                    UserToPayId = userId,
                    ServiceCategoryId = ServiceCategory.Keys.Basic,
                    PlatformId = platformId
                };

                UserFees.Add(basicServiceFee);
                db.Fee.Add(basicServiceFee);
                db.Entry(basicServiceFee).State = System.Data.Entity.EntityState.Added;

                Fee premiumServiceFee = new Fee
                {
                    Id = Guid.NewGuid(),
                    Name = strNameUserPremium,
                    Description = strDescriptionUserPremium,
                    VariableFee = 0,
                    FixFee = 6,
                    TaxFee = 0,
                    UpperRestriction = 0,
                    LowerRestriction = 0,
                    PartnerToPayId = null,
                    CreatedByUserId = ApplicationUser.Key.AdminUserId,
                    CurrencyId = moneda.currency.UsdId,
                    UserToPayId = userId,
                    ServiceCategoryId = ServiceCategory.Keys.Premium,
                    PlatformId = platformId
                };

                UserFees.Add(premiumServiceFee);
                db.Fee.Add(premiumServiceFee);
                db.Entry(premiumServiceFee).State = System.Data.Entity.EntityState.Added;

                Fee superPremiumServiceFee = new Fee
                {
                    Id = Guid.NewGuid(),
                    Name = strNameUserSuperPremium,
                    Description = strDescriptionUserSuperPremium,
                    VariableFee = 0,
                    FixFee = 10,
                    TaxFee = 0,
                    UpperRestriction = 0,
                    LowerRestriction = 0,
                    PartnerToPayId = null,
                    CreatedByUserId = ApplicationUser.Key.AdminUserId,
                    CurrencyId = moneda.currency.UsdId,
                    UserToPayId = userId,
                    ServiceCategoryId = ServiceCategory.Keys.SuperPremium,
                    PlatformId = platformId
                };

                UserFees.Add(superPremiumServiceFee);
                db.Fee.Add(superPremiumServiceFee);
                db.Entry(superPremiumServiceFee).State = System.Data.Entity.EntityState.Added;

                db.SaveChanges();
            }

            return UserFees;
        }

        /// <summary>
        /// Get the specific user fees for the Enpadi Kiosko Platform
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static List<Fee> GetKiskoUserFees(Guid userId)
        {
            List<Fee> UserFees = Fee.GetPlatformUserFees(userId, PlatformModel.Keys.EnpadiKiosko);

            return UserFees;
        }

        /// <summary>
        /// Get enpadi fees for a selected partener and selected platform
        /// a partner is Enpadi, openpay, diestel, conketa, etc
        /// </summary>
        /// <param name="partnerToPayId"></param>
        /// <param name="platformId"></param>
        /// <returns></returns>
        public static List<Fee> GetPlatformFees(Guid partnerToPayId, Guid platformId)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            List<Fee> platformFees = (from item in db.Fee
                                      where
                                      item.PartnerToPayId == partnerToPayId
                                      &&
                                      item.PlatformId == platformId
                                      select item).ToList();
            if (platformFees.Count() <= 0)
            {

                PlatformModel platfom = db.Platform.FirstOrDefault(x => x.Id == platformId);

                //then add the standar fees (Enpadi basic - premium - super premium), this is a security measure, just in case no fees exist
                Enpadi_WebUI.Models.Fee feeBasic = db.Fee.FirstOrDefault(x => x.Id == Enpadi_WebUI.Models.Fee.Keys.EnpadiBasic);
                Enpadi_WebUI.Models.Fee newFeeBasic = new Fee
                {
                    AllowDelete = feeBasic.AllowDelete,
                    CreatedByUserId = feeBasic.CreatedByUserId,
                    CurrencyCode = feeBasic.CurrencyCode,
                    CurrencyId = feeBasic.CurrencyId,
                    Description = "Fee for a basic service in USA API",
                    FixFee = feeBasic.FixFee,
                    LowerRestriction = feeBasic.LowerRestriction,
                    Name = "Basic fee USA API",
                    PartnerName = feeBasic.PartnerName,
                    PartnerToPayId = partnerToPayId,
                    PlatformId = platformId,
                    PlatformName = platfom.Name,
                    ServiceCategoryId = feeBasic.ServiceCategoryId,
                    ServiceCategoryName = feeBasic.ServiceCategoryName,
                    TaxFee = feeBasic.TaxFee,
                    UpperRestriction = feeBasic.UpperRestriction,
                    UserToPayEmail = feeBasic.UserToPayEmail,
                    UserToPayId = feeBasic.UserToPayId,
                    VariableFee = feeBasic.VariableFee
                };

                db.Fee.Add(newFeeBasic);
                db.Entry(newFeeBasic).State = System.Data.Entity.EntityState.Added;

                Enpadi_WebUI.Models.Fee feePremium = db.Fee.FirstOrDefault(x => x.Id == Enpadi_WebUI.Models.Fee.Keys.EnpadiPremium);
                Enpadi_WebUI.Models.Fee newfeePremium = new Fee
                {
                    AllowDelete = feePremium.AllowDelete,
                    CreatedByUserId = feePremium.CreatedByUserId,
                    CurrencyCode = feePremium.CurrencyCode,
                    CurrencyId = feePremium.CurrencyId,
                    Description = "Fee for a premium service in USA API",
                    FixFee = feePremium.FixFee,
                    LowerRestriction = feePremium.LowerRestriction,
                    Name = "Premium fee USA API",
                    PartnerName = feePremium.PartnerName,
                    PartnerToPayId = partnerToPayId,
                    PlatformId = platformId,
                    PlatformName = platfom.Name,
                    ServiceCategoryId = feePremium.ServiceCategoryId,
                    ServiceCategoryName = feePremium.ServiceCategoryName,
                    TaxFee = feePremium.TaxFee,
                    UpperRestriction = feePremium.UpperRestriction,
                    UserToPayEmail = feePremium.UserToPayEmail,
                    UserToPayId = feePremium.UserToPayId,
                    VariableFee = feePremium.VariableFee
                };

                db.Fee.Add(newfeePremium);
                db.Entry(newfeePremium).State = System.Data.Entity.EntityState.Added;

                Enpadi_WebUI.Models.Fee feeSuperPremium = db.Fee.FirstOrDefault(x => x.Id == Enpadi_WebUI.Models.Fee.Keys.EnpadiSuperPremium);
                Enpadi_WebUI.Models.Fee newfeeSuperPremium = new Fee
                {
                    AllowDelete = feeSuperPremium.AllowDelete,
                    CreatedByUserId = feeSuperPremium.CreatedByUserId,
                    CurrencyCode = feeSuperPremium.CurrencyCode,
                    CurrencyId = feeSuperPremium.CurrencyId,
                    Description = "Fee for a super premium service in USA API",
                    FixFee = feeSuperPremium.FixFee,
                    LowerRestriction = feeSuperPremium.LowerRestriction,
                    Name = "Super premium fee USA API",
                    PartnerName = feeSuperPremium.PartnerName,
                    PartnerToPayId = partnerToPayId,
                    PlatformId = platformId,
                    PlatformName = platfom.Name,
                    ServiceCategoryId = feeSuperPremium.ServiceCategoryId,
                    ServiceCategoryName = feeSuperPremium.ServiceCategoryName,
                    TaxFee = feeSuperPremium.TaxFee,
                    UpperRestriction = feeSuperPremium.UpperRestriction,
                    UserToPayEmail = feeSuperPremium.UserToPayEmail,
                    UserToPayId = feeSuperPremium.UserToPayId,
                    VariableFee = feeSuperPremium.VariableFee
                }; ;


                db.Fee.Add(newfeeSuperPremium);
                db.Entry(newfeeSuperPremium).State = System.Data.Entity.EntityState.Added;

                db.SaveChanges();

                platformFees.Add(newFeeBasic);
                platformFees.Add(newfeePremium);
                platformFees.Add(newfeeSuperPremium);
            }

            return platformFees;
        }

        #endregion get platfomr fees

        #region get fee for a service
        /// <summary>
        /// Send the userId that has the fees, for kiosko is currentUserId for WebUsa is consulate id on de model
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="serviceId"></param>
        /// <param name="platformId"></param>
        /// <returns></returns>
        public static List<Fee> GetFeeForService(Guid userId, Guid serviceId, Guid platformId)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            List<Fee> serviceFees = new List<Fee>();

            try
            {
                servicios service = db.servicios.FirstOrDefault(x => x.Id == serviceId);

                serviceFees = (from item in db.Fee
                               join servFeePlat in db.ServiciosFeePlatfom on item.Id equals servFeePlat.FeeId
                               where
                               servFeePlat.UserId == userId
                               &&
                               item.PlatformId == platformId
                               &&
                               item.ServiceCategoryId == service.ServiceCategoryId
                               select item).ToList();

                if (serviceFees.Count() <= 0)
                {
                    if (platformId == PlatformModel.Keys.WebUsa)
                    {
                        Fee.AddFeesWebUsaUser(userId);
                    }
                    else
                    {
                        Fee.AddFeesKiskoStandarUser(socio.SocioID.Enpadi, userId);
                    }

                    serviceFees = (from item in db.Fee
                                   join servFeePlat in db.ServiciosFeePlatfom on item.Id equals servFeePlat.FeeId
                                   where
                                   servFeePlat.UserId == userId
                                   &&
                                   item.PlatformId == platformId
                                   &&
                                   item.ServiceCategoryId == service.ServiceCategoryId
                                   select item).ToList();
                }
            }
            catch (Exception e)
            {
                throw new ArgumentException("Service not found.");
            }

            return serviceFees;

        }

        /// <summary>
        /// return all applicable fees for the user on the specific service, deprecated for GetFeeForService
        /// </summary>
        /// <returns></returns>
        public static List<Fee> GetFeeForServiceByKioskoUser(Guid userId, Guid serviceId, Guid platformId)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            List<Fee> serviceFees = new List<Fee>();

            try
            {
                servicios service = db.servicios.FirstOrDefault(x => x.Id == serviceId);

                serviceFees = (from item in db.Fee
                               join servFeePlat in db.ServiciosFeePlatfom on item.Id equals servFeePlat.FeeId
                               where
                               servFeePlat.UserId == userId
                               &&
                               item.PlatformId == platformId
                               &&
                               item.ServiceCategoryId == service.ServiceCategoryId
                               select item).ToList();

                if (serviceFees.Count() <= 0)
                {
                    Fee.AddFeesKiskoStandarUser(socio.SocioID.Enpadi, userId);

                    serviceFees = (from item in db.Fee
                                   join servFeePlat in db.ServiciosFeePlatfom on item.Id equals servFeePlat.FeeId
                                   where
                                   servFeePlat.UserId == userId
                                   &&
                                   item.PlatformId == platformId
                                   &&
                                   item.ServiceCategoryId == service.ServiceCategoryId
                                   select item).ToList();
                }
            }
            catch (Exception e)
            {
                throw new ArgumentException("Service not found.");
            }

            return serviceFees;

        }

        /// <summary>
        /// , deprecated for GetFeeForService
        /// </summary>
        /// <param name="serviceId"></param>
        /// <returns></returns>
        public static List<Fee> GetFeeForServiceForWebUsa(Guid serviceId)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            List<Fee> serviceFees = new List<Fee>();

            try
            {
                servicios service = db.servicios.FirstOrDefault(x => x.Id == serviceId);

                serviceFees = (from item in db.Fee
                               join servFeePlat in db.ServiciosFeePlatfom on item.Id equals servFeePlat.FeeId
                               where
                               servFeePlat.UserId == ApplicationUser.Key.ConsulateId
                               &&
                               item.PlatformId == PlatformModel.Keys.WebUsa
                               &&
                               item.ServiceCategoryId == service.ServiceCategoryId
                               select item).ToList();

                if (serviceFees.Count() <= 0)
                {
                    Fee.AddFeesWebUsaUser(ApplicationUser.Key.ConsulateId);

                    serviceFees = (from item in db.Fee
                                   join servFeePlat in db.ServiciosFeePlatfom on item.Id equals servFeePlat.FeeId
                                   where
                                   servFeePlat.UserId == ApplicationUser.Key.ConsulateId
                                   &&
                                   item.PlatformId == PlatformModel.Keys.WebUsa
                                   &&
                                   item.ServiceCategoryId == service.ServiceCategoryId
                                   select item).ToList();
                }
            }
            catch (Exception e)
            {
                throw new ArgumentException("Service not found.");
            }

            return serviceFees;
        }

        public static List<Fee> GetFeeForServiceByEnpadiUser(Guid userId, Guid serviceId, Guid platformId)
        {
            List<Fee> serviceFees = new List<Fee>();
            ApplicationDbContext db = new ApplicationDbContext();
            try
            {
                servicios service = db.servicios.FirstOrDefault(x => x.Id == serviceId);

                serviceFees = (from item in db.Fee
                               join servFeePlat in db.ServiciosFeePlatfom on item.Id equals servFeePlat.FeeId
                               where
                               servFeePlat.UserId == userId
                               &&
                               item.PlatformId == platformId
                               &&
                               item.ServiceCategoryId == ServiceCategory.Keys.serviceChargeId
                               select item).ToList();

                if (serviceFees.Count() <= 0)
                {
                    Fee.AddFeesEnpadiStandarUser(socio.SocioID.Enpadi, userId);

                    serviceFees = (from item in db.Fee
                                   join servFeePlat in db.ServiciosFeePlatfom on item.Id equals servFeePlat.FeeId
                                   where
                                   servFeePlat.UserId == userId
                                   &&
                                   item.PlatformId == platformId
                                   &&
                                   item.ServiceCategoryId == ServiceCategory.Keys.serviceChargeId
                                   select item).ToList();
                }
            }
            catch (Exception e)
            {
                throw new ArgumentException("Service not found.");
            }

            return serviceFees;

        }

        #endregion get fee for a service

        #endregion get fees

        #region add fees

        /// <summary>
        /// add enpadi fees for using the platform
        /// </summary>
        public static void AddFeesMxApiUser(Guid UserId)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            //get all enpadiFees
            List<Fee> platformFees = Enpadi_WebUI.Models.Fee.GetPlatformFees(socio.SocioID.Enpadi, PlatformModel.Keys.MxApi);

            ServiciosFeePlatfom feeService = null;
            //add enpadi fee
            foreach (Fee itemEnpadiFee in platformFees)
            {
                feeService = new ServiciosFeePlatfom
                {
                    Id = Guid.NewGuid(),
                    FeeId = itemEnpadiFee.Id,
                    UserId = UserId
                };
                db.ServiciosFeePlatfom.Add(feeService);
                db.Entry(feeService).State = System.Data.Entity.EntityState.Added;
            }

            db.SaveChanges();
        }

        /// <summary>
        /// add enpadi fees for using the USA platform
        /// </summary>
        public static void AddFeesUsaApiUser(Guid UserId)
        {
            try
            {
                ApplicationDbContext db = new ApplicationDbContext();

                //get all enpadiFees
                List<Fee> platformFees = Enpadi_WebUI.Models.Fee.GetPlatformFees(socio.SocioID.Enpadi, PlatformModel.Keys.UsaApi);

                ServiciosFeePlatfom feeService = null;
                //add enpadi fee
                foreach (Fee itemEnpadiFee in platformFees)
                {
                    feeService = new ServiciosFeePlatfom
                    {
                        Id = Guid.NewGuid(),
                        FeeId = itemEnpadiFee.Id,
                        UserId = UserId
                    };
                    db.ServiciosFeePlatfom.Add(feeService);
                    db.Entry(feeService).State = System.Data.Entity.EntityState.Added;


                }

                db.SaveChanges();
            }
            catch (Exception e)
            {
                throw new ArgumentException("Error on adding fees");
            }
        }

        public static void AddFeesWebUsaUser(Guid userid)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            //get all enpadiFees
            List<Fee> platformFees = Enpadi_WebUI.Models.Fee.GetPlatformFees(socio.SocioID.Enpadi, PlatformModel.Keys.WebUsa);

            //get all enpadi web usa
            List<Fee> userWebUsa = Enpadi_WebUI.Models.Fee.GetPlatformUserFees(ApplicationUser.Key.ConsulateId, PlatformModel.Keys.WebUsa);

            //add enpadi fee
            foreach (Fee itemEnpadiFee in platformFees)
            {
                ServiciosFeePlatfom feeServiceCurrent = db.ServiciosFeePlatfom.Where(x => x.FeeId == itemEnpadiFee.Id && x.UserId == userid).FirstOrDefault();

                if (feeServiceCurrent == null)
                {
                    ServiciosFeePlatfom feeService = new ServiciosFeePlatfom
                    {
                        Id = Guid.NewGuid(),
                        FeeId = itemEnpadiFee.Id,
                        UserId = userid
                    };
                    db.ServiciosFeePlatfom.Add(feeService);
                    db.Entry(feeService).State = System.Data.Entity.EntityState.Added;
                }
            }

            //add user fee
            foreach (Fee itemUserFee in userWebUsa)
            {
                ServiciosFeePlatfom feeServiceCurrent = db.ServiciosFeePlatfom.Where(x => x.FeeId == itemUserFee.Id && x.UserId == userid).FirstOrDefault();

                if (feeServiceCurrent == null)
                {

                    ServiciosFeePlatfom feeService = new ServiciosFeePlatfom
                    {
                        Id = Guid.NewGuid(),
                        FeeId = itemUserFee.Id,
                        UserId = userid
                    };
                    db.ServiciosFeePlatfom.Add(feeService);
                    db.Entry(feeService).State = System.Data.Entity.EntityState.Added;
                }
                db.SaveChanges();
            }
        }

        public static void AddFeesKiskoStandarUser(Guid partnerToPayId, Guid userId)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            //get all enpadiFees
            List<Fee> platformFees = Enpadi_WebUI.Models.Fee.GetPlatformFees(partnerToPayId, PlatformModel.Keys.EnpadiKiosko);

            //get all kiosko userfees
            List<Fee> userKioskoFees = Enpadi_WebUI.Models.Fee.GetKiskoUserFees(userId);


            //add enpadi fee
            foreach (Fee itemEnpadiFee in platformFees)
            {

                ServiciosFeePlatfom feeService = new ServiciosFeePlatfom
                {
                    Id = Guid.NewGuid(),
                    FeeId = itemEnpadiFee.Id,
                    UserId = userId
                };
                db.ServiciosFeePlatfom.Add(feeService);
                db.Entry(feeService).State = System.Data.Entity.EntityState.Added;

            }

            //add user fee
            foreach (Fee itemUserFee in userKioskoFees)
            {

                ServiciosFeePlatfom feeService = new ServiciosFeePlatfom
                {
                    Id = Guid.NewGuid(),
                    FeeId = itemUserFee.Id,
                    UserId = userId
                };

                db.ServiciosFeePlatfom.Add(feeService);
                db.Entry(feeService).State = System.Data.Entity.EntityState.Added;

            }


            db.SaveChanges();

        }

        public static void AddFeesEnpadiStandarUser(Guid partnerToPayId, Guid userId)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            //get all enpadiFees
            List<Fee> platformFees = Enpadi_WebUI.Models.Fee.GetPlatformFees(partnerToPayId, PlatformModel.Keys.Enpadi);


            foreach (Fee itemEnpadiFee in platformFees)
            {
                //if (itemEnpadiFee.ServiceCategoryId == itemPayService.ServiceCategoryId)
                //{
                ServiciosFeePlatfom feeService = new ServiciosFeePlatfom
                {
                    Id = Guid.NewGuid(),
                    FeeId = itemEnpadiFee.Id,
                    UserId = userId
                };
                db.ServiciosFeePlatfom.Add(feeService);
                db.Entry(feeService).State = System.Data.Entity.EntityState.Added;
                //}
            }

            db.SaveChanges();

        }

        #endregion add fees

        #region calculate fees
        /// <summary>
        /// Normaly this exchange rate is +2 from the exchange rate retrive from webservice
        /// </summary>
        /// <param name="fees"></param>
        /// <param name="AmountInMXN"></param>
        /// <param name="exchangeRate"></param>
        /// <param name="platformId"></param>
        /// <param name="chargeByPartnerAmount"></param>
        /// <param name="variableFeeFromPartnerCharge"></param>
        /// <returns></returns>
        public static Decimal GetTotalFeeInMXN(List<Enpadi_WebUI.Models.Fee> fees, Decimal AmountInMXN, Decimal exchangeRateUp, Guid platformId, decimal chargeByPartnerAmount, Boolean variableFeeFromPartnerCharge)
        {
            decimal totalFeeToReportInMXN = 0;

            //for usa and kiosko the fee for the service is 0, there is no variable charge, just fix charged
            //if (platformId == PlatformModel.Keys.WebUsa || platformId == PlatformModel.Keys.EnpadiKiosko)
            //{
            //    AmountInMXN = 0;
            //}

            foreach (Enpadi_WebUI.Models.Fee fee in fees)
            {

                if (fee.CurrencyId == moneda.currency.MexicanPesoId)
                {
                    if (variableFeeFromPartnerCharge == false)
                    {
                        totalFeeToReportInMXN += (fee.FixFee) + (fee.VariableFee * AmountInMXN) + (fee.TaxFee * AmountInMXN);
                    }
                    else
                    {
                        totalFeeToReportInMXN += (fee.FixFee) + (fee.VariableFee * chargeByPartnerAmount) + (fee.TaxFee * AmountInMXN);
                    }
                }
                else
                {
                    if (variableFeeFromPartnerCharge == false)
                    {
                        totalFeeToReportInMXN += (fee.FixFee * exchangeRateUp) + ((fee.VariableFee * AmountInMXN) * exchangeRateUp) + ((fee.TaxFee * AmountInMXN) * exchangeRateUp);
                    }
                    else
                    {
                        totalFeeToReportInMXN += (fee.FixFee * exchangeRateUp) + ((fee.VariableFee * chargeByPartnerAmount) * exchangeRateUp) + ((fee.TaxFee * AmountInMXN) * exchangeRateUp);
                    }
                }
            }

            return totalFeeToReportInMXN;

        }

        /// <summary>
        /// Normaly this exchange rate is -2 from the exchange rate retrive from webservice
        /// </summary>
        /// <param name="fees"></param>
        /// <param name="AmountInMXN"></param>
        /// <param name="exchangeRate"></param>
        /// <param name="platformId"></param>
        /// <param name="chargeByPartnerAmount"></param>
        /// <param name="variableFeeFromPartnerCharge"></param>
        /// <returns></returns>
        public static Decimal GetTotalFeeInUSD(List<Enpadi_WebUI.Models.Fee> fees, Decimal AmountInMXN, Decimal exchangeRateDown, Guid platformId, decimal chargeByPartnerAmount, Boolean variableFeeFromPartnerCharge)
        {
            decimal totalFeeToReportInDLLS = 0;

            //for usa and kiosko the fee for the service is 0, there is no variable charge, just fix charged
            //if (platformId == PlatformModel.Keys.WebUsa || platformId == PlatformModel.Keys.EnpadiKiosko)
            //{
            //    AmountInMXN = 0;
            //}

            foreach (Enpadi_WebUI.Models.Fee fee in fees)
            {
                if (fee.CurrencyId == moneda.currency.MexicanPesoId)
                {
                    if (variableFeeFromPartnerCharge == false)
                    {
                        totalFeeToReportInDLLS += ((fee.FixFee) + (fee.VariableFee * AmountInMXN) + (fee.TaxFee * AmountInMXN)) / exchangeRateDown;
                    }
                    else
                    {
                        totalFeeToReportInDLLS += ((fee.FixFee) + (fee.VariableFee * chargeByPartnerAmount) + (fee.TaxFee * AmountInMXN) / exchangeRateDown);
                    }
                }
                else
                {
                    if (variableFeeFromPartnerCharge == false)
                    {
                        totalFeeToReportInDLLS += (fee.FixFee + (fee.VariableFee * (AmountInMXN / exchangeRateDown)) + (fee.TaxFee * (AmountInMXN / exchangeRateDown)));
                    }
                    else
                    {
                        totalFeeToReportInDLLS += (fee.FixFee + (fee.VariableFee * (chargeByPartnerAmount / exchangeRateDown)) + (fee.TaxFee * (chargeByPartnerAmount / exchangeRateDown)));
                    }
                }
            }

            return totalFeeToReportInDLLS;

        }

        #endregion calculate fees

        #endregion methods
    }
}