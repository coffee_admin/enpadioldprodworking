﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enpadi_WebUI.Models
{
    [Table("logTC")]
    public class logTC
    {
        [Key]
        public int idLog { get; set; }
        public int idTarjeta { get; set; }
        public int result { get; set; }
    }
}