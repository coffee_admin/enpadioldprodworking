﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    [Table("Estatus")]
    public class Estatus
    {
        /// <summary>
        /// Keys for generating status
        /// </summary>
        public static class db_keys
        {
            public static readonly Int32 payment_approved = 1;
            public static readonly Int32 payment_rejected = 2;
            public static readonly Int32 payment_pending = 3;
        }

        [Key]
        public int estatusID { get; set; }
        public String descripcion { get; set; }
        public DateTime? fechaAlta { get; set; }
        public DateTime? fechaBaja { get; set; }
    }
}