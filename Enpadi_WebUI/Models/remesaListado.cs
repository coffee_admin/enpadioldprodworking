﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    public class remesaListado
    {
        public int remesaID { get; set; }
        public string socio { get; set; }
        public string concepto { get; set; }
        public string transaccion { get; set; }
        public string referencia { get; set; }
        public decimal localAmount { get; set; }
        public decimal foreignAmount { get; set; }
        public decimal exchangeRate { get; set; }
        public string fromName { get; set; }
        public string fromPhone { get; set; }
        public string fromEmail { get; set; }
        public string toName { get; set; }
        public string toPhone { get; set; }
        public string toEmail { get; set; }
        public string estatus { get; set; }
        public DateTime fechaRegistro { get; set; }
    }
}