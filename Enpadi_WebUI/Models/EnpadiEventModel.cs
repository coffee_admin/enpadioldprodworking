﻿using Enpadi_WebUI.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    public class EnpadiEventModel : EnpadiEvent
    {

        #region keys
        public static class Status
        {
            public const string Pending = "Pending";
            public const string Acepted = "Acepted";
            public const string Fail = "Fail";
            public const string Retrying = "Retrying";
            public const string FailDoError = "Fail do error";
        }



        #endregion keys

        #region constructors

        public EnpadiEventModel()
        {
            id = Guid.NewGuid();
            created_at = ToolsBO.DateTimeToTimeStamp(DateTime.Now);
        }

        #endregion constructors

        #region members

        private String _type = "event";

        [Key]
        public Guid id { get; set; }

        public object data { get; set; }

        public Boolean livemode { get; set; }

        public String webhook_status { get; set; }

        public String object_description { get; set; }

        public String type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
            }
        }

        public String created_at { get; set; }

        public Guid data_id { get; set; }

        #endregion members
    }
}