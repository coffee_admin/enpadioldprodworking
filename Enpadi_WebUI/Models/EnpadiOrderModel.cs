﻿using Enpadi_WebUI.DAO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    public class EnpadiOrderModel : EnpadiOrderDAO
    {
        #region constructos
        [Key]
        public Guid id { get; set; }

        public EnpadiOrderModel()
        {
            this.id = Guid.NewGuid();
            this.data_object = new data();
        }

        #endregion constructos

        #region members

        public data data_object = new data();

        #endregion members

        #region keys

        [NotMapped]
        public static class EnpadiOrderEstatus
        {
            [NotMapped]
            public const String Paid = "order.paid";
            [NotMapped]
            public const String Pending = "order.pending_payment";
            [NotMapped]
            public const String Created = "order.created";
            [NotMapped]
            public const String Create = "order.create";
            [NotMapped]
            public const String Canceled = "order.canceled";
        }

        #endregion keys

        #region local class

        [NotMapped]
        public class customer_info
        {

            private String _object_description = "customer.info";

            [NotMapped]
            public String email { get; set; }
            [NotMapped]
            public String phoneNumber { get; set; }
            [NotMapped]
            public String name { get; set; }
            [NotMapped]
            public String last_name { get; set; }
            [NotMapped]
            public String externalId { get; set; }
            [NotMapped]
            public String object_description
            {
                get
                {
                    return _object_description;
                }
                set
                {
                    _object_description = value;
                }
            }

            public String address_line1 { get; set; }
            public String address_line2 { get; set; }
            public String address_line3 { get; set; }

            public String address_city { get; set; }
            public String address_state { get; set; }

            public String address_country_code { get; set; }
            public String address_postal_code { get; set; }
        }

        #region data class for event
        /// <summary>
        /// This class is build to be inserted in the enpadi event
        /// </summary>
        public class data
        {

            public data()
            {
                id = Guid.NewGuid();
                customer_info = new customer_info();
                line_items = new List<EnpadiLineItemModel>();
            }

            private String _object_description = "order";

            [Key]
            public Guid id { get; set; }
            public Boolean livemode { get; set; }
            public Decimal amount { get; set; }
            public String currency { get; set; }
            public Decimal amount_refunded { get; set; }
            public customer_info customer_info { get; set; }
            public String external_id { get; set; }
            public String external_description { get; set; }
            public String external_category { get; set; }
            public String object_description
            {
                get
                {
                    return _object_description;
                }
                set
                {
                    _object_description = value;
                }
            }
            public List<EnpadiLineItemModel> line_items { get; set; }
            public String createdAt { get; set; }
            public String updatedAt { get; set; }

            public String type { get; set; }

            #region for filtering and validation

            public String client_email { get; set; }
            public Guid localUserId { get; set; }
            public String client_trans_password { get; set; }

            #endregion for filtering and validation

            #region for consult and reference

            public String client_name { set; get; }


            #endregion for consult and reference
        }

        #endregion data class for event

        #endregion local class


    }
}