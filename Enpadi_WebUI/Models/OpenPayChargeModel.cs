﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enpadi_WebUI.DAO;

namespace Enpadi_WebUI.Models
{
    public class OpenPayChargeModel : OpenPayCharge
    {
        public String Id { get; set; }
        public DateTime? CreationDate { get; set; }
        public Decimal Amount { get; set; }
        public String Status { get; set; }
        public String Description { get; set; }
        public String TransactionType { get; set; }
        public String OperationType { get; set; }
        public String Method { get; set; }
        public String ErrorMessage { get; set; }
        public String Authorization { get; set; }
        public String OrderId { get; set; }
        public String CustomerId { get; set; }
        public Boolean Conciliated { get; set; }
        public String Type { get; set; }
        public String BankName { get; set; }
        public String CLABE { get; set; }
        public String Name { get; set; }
        public String Reference { get; set; }
        public String BarcodeURL { get; set; }
        public String PaymentAddress { get; set; }
        public String PaymentUrlBip21 { get; set; }
        public Decimal AmountBitcoins { get; set; }

    }
}