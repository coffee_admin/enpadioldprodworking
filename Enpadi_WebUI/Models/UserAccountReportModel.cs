﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enpadi_WebUI.DAO;

namespace Enpadi_WebUI.Models
{
    public class UserAccountReportModel : UserAccountReport
    {

        #region properties

        public Guid transaccion_id { get; set; }

        public String ReprintReceipt { get; set; }

        public Int32 transaccion_transID { get; set; }
        public String origin_descripcion { get; set; }
        public String type_descripcion { get; set; }
        public Int32 transaccion_socioID { get; set; }
        public String partner_nombreComercial { get; set; }
        public Int32 transaccion_terminalID { get; set; }
        public String currency_codigo { get; set; }
        public Int32 transaccion_tarjetaID { get; set; }
        public String transaccion_tarjeta { get; set; }
        public Decimal transaccion_monto { get; set; }
        public Decimal transaccion_comision { get; set; }
        public Decimal transaccion_iva { get; set; }
        public String transaccion_transaccion { get; set; }
        public String transaccion_concepto { get; set; }
        public String transaccion_descripcion { get; set; }
        public String transaccion_referencia { get; set; }
        public String status_descripcion { get; set; }
        public String transaccion_aprobacion { get; set; }
        public String transaccion_ip { get; set; }
        public DateTime transaccion_fechaRegistro { get; set; }

        public Decimal transaccion_monto_con_comission { get; set; }

        public Decimal? ComissionMX { get; set; }
        public Decimal? TotalAmountMX { get; set; }
        public Decimal? ComissionUSD { get; set; }
        public Decimal? TotalAmountUSD { get; set; }
        public Decimal? AmountForServiceMXN { get; set; }
        public Decimal? AmountForServiceUSD { get; set; }

        public Guid transactionUserId { get; set; }
        public String transactionUserEmail { get; set; }

        public String platform { get; set; }

        public String beneficiary { get; set; }
        public String buyer { get; set; }

        public Int32 status_id { get; set; }

        public Decimal exchangeRateDown { get; set; }
        public Decimal exchangeRateUp { get; set; }

        public Decimal EnpadiFeesMXN { get; set; }
        public Decimal PartnerFeesMXN { get; set; }

        public Decimal EnpadiFeesUSD { get; set; }
        public Decimal PartnerFeesUSD { get; set; }

        public String AmountToShowInWebUsaAdminReport { get; set; }

        public Decimal ComissionMXServicesInMX { get; set; }
        public Decimal ComissionDllsServicesInUSD { get; set; }

        public Decimal TransactionMXServicesInMX { get; set; }
        public Decimal TransactionDllsServicesInUSD { get; set; }

        public Guid GuidPagoServiciosId { get; set; }

        #endregion properties
    }
}