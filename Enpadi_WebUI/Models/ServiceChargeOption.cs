﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    [Table("ServiceChargeOption")]
    public class ServiceChargeOption
    {
        [Key]
        public Guid Id { get; set; }
        public Guid ServiceId { get; set; }
        public Guid ChargeOptionId { get; set; }

        public virtual ChargeOption ChargeOption { get; set; }
    }
}