﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    public class ViewModelEnpadiAdmin
    {
        public decimal DiestelPhoneTime { get; set; }
        public decimal DiestelServiceTime { get; set; }
    }
}