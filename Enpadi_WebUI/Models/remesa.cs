﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Enpadi_WebUI.Models
{
    public class remesa
    {
        public int remesaID { get; set; }
        public int origenID { get; set; }
        public int tipoID { get; set; }
        public int modoID { get; set; }
        public int socioID { get; set; }
        public int terminalID { get; set; }
        public string transaccion { get; set; }
        public string concepto { get; set; }
        public string descripcion { get; set; }
        public string referencia { get; set; }
        public decimal localAmount { get; set; }
        public decimal foreignAmount { get; set; }
        public decimal exchangeRate { get; set; }
        public decimal fee1 { get; set; }
        public decimal fee2 { get; set; }
        public decimal fee3 { get; set; }
        public string receiptCode { get; set; }
        public string remitanceCode { get; set; }
        public string confirmationCode { get; set; }
        
        [Required]
        [Display(Name="First Name", Prompt="First Name")]
        public string fromFirstName { get; set; }

        [Required]
        [Display(Name="Last Name", Prompt="Last Name")]
        public string fromLastName { get; set; }

        [Required]
        [Display(Name = "Address 1", Prompt = "Address 1")]
        public string fromAddress1 { get; set; }

        [Required]
        [Display(Name = "Address 2", Prompt = "Address 2")]
        public string fromAddress2 { get; set; }
        public string fromNeighborhood { get; set; }
        public int fromCity { get; set; }
        public int fromState { get; set; }

        [Required]
        [Range(1,99999)]
        [Display(Name = "Country", Prompt = "Country")]
        public int fromCountry { get; set; }
        public string fromZip { get; set; }

        [Required]
        [Display(Name = "Phone", Prompt = "Phone")]
        public string fromPhone { get; set; }

        [Required]
        [Display(Name = "Email", Prompt = "Email")]
        public string fromEmail { get; set; }

        [Required]
        [Display(Name = "First Name", Prompt = "First Name")]
        public string toFirstName { get; set; }

        [Required]
        [Display(Name = "Last Name", Prompt = "Last Name")]
        public string toLastName { get; set; }

        [Required]
        [Display(Name = "Address 1", Prompt = "Address 1")]
        public string toAddress1 { get; set; }

        [Required]
        [Display(Name = "Address 2", Prompt = "Address 2")]
        public string toAddress2 { get; set; }
        public string toNeighborhood { get; set; }
        public int toCity { get; set; }
        public int toState { get; set; }

        public int toCountry { get; set; }
        public string toZip { get; set; }

        [Required]
        [Display(Name = "Phone", Prompt = "Phone")]
        public string toPhone { get; set; }

        [Required]
        [Display(Name = "Email", Prompt = "Email")]
        public string toEmail { get; set; }
        public int estatus { get; set; }
        public DateTime fechaRegistro { get; set; }
    }
}