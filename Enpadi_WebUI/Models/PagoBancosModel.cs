﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enpadi_WebUI.DAO;

namespace Enpadi_WebUI.Models
{
    public class PagoBancosModel : PagoBancos
    {
        public int pagoId { get; set; }
        public string id { get; set; }
        public string description { get; set; }
        public string error_message { get; set; }
        public string authorization { get; set; }
        public decimal amount { get; set; }
        public string operation_type { get; set; }
        public string payment_method { get; set; }
        public string clabe { get; set; }
        public string bank { get; set; }
        public string type { get; set; }
        public string name { get; set; }
        public string order_id { get; set; }
        public string transaction_type { get; set; }
        public string creation_date { get; set; }
        public string currency { get; set; }
        public string status { get; set; }
        public string method { get; set; }
    }
}