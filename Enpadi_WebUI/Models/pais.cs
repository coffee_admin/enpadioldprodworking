﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Enpadi_WebUI.Models
{
    [Table("Paises")]
    public class pais
    {
        [Key]
        public int paisID { get; set; }

        [Display(Name="Pais")]
        public string nombre { get; set; }

        [Display(Name="Codigo")]
        public string codigo { get; set; }

        [Display(Name="ISO 2")]
        public string ISO2 { get; set; }

        [Display(Name="ISO 3")]
        public string ISO3 { get; set; }

        [Display(Name="Activo")]
        public bool activo { get; set; }
    }
}