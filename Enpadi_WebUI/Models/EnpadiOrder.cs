﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    [Table("EnpadiOrder")]
    public class EnpadiOrder
    {

        public EnpadiOrder()
        {
            id = Guid.NewGuid();
            line_items = new List<EnpadiLineItemModel>();
        }

        private String _object_description = "order";
        public Guid? customerId { get; set; }

        public Guid id { get; set; }
        public Boolean? livemode { get; set; }
        public Decimal? amount { get; set; }
        public String currency { get; set; }
        public Decimal? amount_refunded { get; set; }
        public String external_id { get; set; }
        public String external_description { get; set; }
        public String external_category { get; set; }
        public String object_description { get; set; }

        public String createdAt { get; set; }
        public String updatedAt { get; set; }

        public String customer_email { get; set; }
        public String customer_phone_number { get; set; }
        public String customer_name { get; set; }
        public String customer_last_name { get; set; }
        public String customer_external_id { get; set; }
        public String customer_object_description { get; set; }
        public String client_email { get; set; }
        public String localUserId { get; set; }

        public String type { get; set; }

        public String client_trans_password { get; set; }
        public String address_line1 { get; set; }
        public String address_line2 { get; set; }
        public String address_line3 { get; set; }
        public String address_city { get; set; }
        public String address_state { get; set; }
        public String address_country_code { get; set; }
        public String address_postal_code { get; set; }
        public Guid? estatusId { get; set; }
        public DateTime? receivedAt { get; set; }

        [NotMapped]
        public String Status { get; set; }
        [NotMapped]
        public String supplier { get; set; }
        [NotMapped]
        public Decimal orderFee { get; set; }



        #region static methods

        //public static Boolean Save(
        //    Boolean livemode,
        //    Decimal amount,
        //    String currency,
        //    Decimal amount_refunded,
        //    String external_id,
        //    String external_description,
        //    String external_category,
        //    String object_description,

        //    String createdAt,
        //    String updatedAt,

        //    String customer_email,
        //    String customer_phone_number,
        //    String customer_name,
        //    String customer_last_name,
        //    String customer_external_id,

        //    String customer_object_description,
        //    String client_email,
        //    String localUserId,
        //    String type,
        //    String client_trans_password,
        //    String address_line1,
        //    String address_line2,
        //    String address_line3,
        //    String address_city,
        //    String address_state,
        //    String address_country_code,
        //    String address_postal_code,
        //    Guid? estatusId,
        //    DateTime? receivedAt
        //    )
        //{
        //    Boolean success = true;


        //    return success;
        //}

        #endregion static methods

        #region not mapped 

        [NotMapped]
        public List<EnpadiLineItemModel> line_items { get; set; }

        #endregion not mapped 
    }
}