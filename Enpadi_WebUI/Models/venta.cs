﻿using System.ComponentModel.DataAnnotations;

namespace Enpadi_WebUI.Models
{
    public class venta
    {
        [Required(ErrorMessage = "El Numero de Cuenta debe constar de 16 Digitos")]
        [MinLength(16, ErrorMessage = "El Numero de Cuenta debe constar de 16 Digitos")]
        [MaxLength(16, ErrorMessage = "El Numero de Cuenta debe constar de 16 Digitos")]
        [DataType(DataType.CreditCard)]
        [Display(Name = "Cuenta")]
        public string cuenta { get; set; }

        public string nombre { get; set; }

        [Required()]
        [DataType(DataType.Currency)]
        [Range(0.1, double.MaxValue, ErrorMessage = "El Monto debe ser mayor a $0.00")]
        [DisplayFormat(DataFormatString = "{0:F2}", ApplyFormatInEditMode = true)]
        [Display(Name = "Venta")]
        public decimal monto { get; set; }
        public int puntos { get; set; }
        public int saldo { get; set; }
    }
}