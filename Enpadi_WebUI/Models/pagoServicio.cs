﻿using Enpadi_WebUI.WSDiestel;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
namespace Enpadi_WebUI.Models
{
    [Table("PagoServicios")]
    public class pagoServicio
    {
        public pagoServicio()
        {
            Id = Guid.NewGuid();
        }

        string _monto;

        [Key]
        public Guid Id { get; set; }

        //Parametros de Informacion del Pago
        [Display(Name = "ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int pagoID { get; set; }

        [Display(Name = "Cuenta")]
        public int tarjetaID { get; set; }

        [Display(Name = "SKU")]
        public string sku { get; set; }

        [Display(Name = "Servicio")]
        public string nombre { get; set; }

        [Display(Name = "Compañia")]
        public string compañia { get; set; }

        [Display(Name = "Referencia")]
        public string referencia { get; set; }

        [Display(Name = "Referencia 2")]
        public string referencia2 { get; set; }

        [Display(Name = "Monto")]
        public decimal? monto { get; set; }

        [Display(Name = "Comisión")]
        public decimal? comision { get; set; }

        [Display(Name = "Fecha")]
        public string fechaLocal { get; set; }

        [Display(Name = "Hora")]
        public string horaLocal { get; set; }

        [Display(Name = "Fecha Corte")]
        public string fechaContable { get; set; }

        [Display(Name = "Codigo Respuesta")]
        public int codigoRespuesta { get; set; }

        [Display(Name = "Respuesta")]
        public string codigoRespuestaDescr { get; set; }

        [Display(Name = "Tipo de Pago")]
        public string tipoPago { get; set; }

        [Display(Name = "Autorización")]
        public string autorizacion { get; set; }

        [Display(Name = "Brand")]
        public string brand { get; set; }

        [Display(Name = "Leyenda")]
        public string leyenda { get; set; }

        [Display(Name = "Leyenda 1")]
        public string leyenda1 { get; set; }

        [Display(Name = "Leyenda 2")]
        public string leyenda2 { get; set; }

        [Display(Name = "Digito Validador")]
        public string dv { get; set; }

        [Display(Name = "Token")]
        public string token { get; set; }

        [Display(Name = "Proveedor")]
        public string proveedor { get; set; }

        [Display(Name = "Estatus")]
        public int estatus { get; set; }

        public Guid? transaccionesId { get; set; }

        public Decimal? supplierFeeInMXN { get; set; }

        public Decimal? supplierAmountForServiceInMXN { get; set; }

        public Decimal? supplierTotal { get; set; }

        public String supplierSendReference { get; set; }

        public String supplierRecivedReference { get; set; }

        public Decimal? exchageRateUp { get; set; }

        public Decimal? exchageRateDown { get; set; }

        public Decimal? totalMXN { get; set; }

        public Decimal? totalUSD { get; set; }

        public Decimal? serviceInMXN { get; set; }

        public Decimal? serviceInUSD { get; set; }

        public Decimal? comissionMXN { get; set; }

        public Decimal? comissionUSD { get; set; }
        
        //Parametros de SKIN

        [NotMapped]
        [ScaffoldColumn(false)]
        public string imagenCompañia { get; set; }


        //Parametros Auxiliares

        [NotMapped]
        [ScaffoldColumn(false)]
        public string titular { get; set; }

        [NotMapped]
        [ScaffoldColumn(false)]
        public string cuenta { get; set; }

        [NotMapped]
        [ScaffoldColumn(false)]
        public decimal saldo { get; set; }

        [NotMapped]
        [ScaffoldColumn(false)]
        public string tipo { get; set; }

        [NotMapped]
        [ScaffoldColumn(false)]
        public string descripcion { get; set; }

        [NotMapped]
        [ScaffoldColumn(false)]
        public string referenciaTipo { get; set; }

        [NotMapped]
        [ScaffoldColumn(false)]
        public int referenciaLongitud { get; set; }

        [NotMapped]
        [ScaffoldColumn(false)]
        public string instrucciones { get; set; }

        [NotMapped]
        [ScaffoldColumn(false)]
        public string imagenTipo { get; set; }

        //[NotMapped]
        //[ScaffoldColumn(false)]
        //public decimal amountInDllsWithFees { get; set; }

        //[NotMapped]
        //[ScaffoldColumn(false)]
        //public decimal exchangeRate { get; set; }

        //Arreglo de Campos para la Mensajeria Universal
        [NotMapped]
        [ScaffoldColumn(false)]
        public cCampo[] Campos { get; set; }

        //Arreglo de Campos para la Mensajeria Universal
        [NotMapped]
        [ScaffoldColumn(false)]
        public cCampo[] DisplayFields { get; set; }

        //[NotMapped]
        //[ScaffoldColumn(false)]
        //public string DiestelSendReference { get; set; }

        //[NotMapped]
        //[ScaffoldColumn(false)]
        //public string DiestelRecivedReference { get; set; }

        //[NotMapped]
        //[ScaffoldColumn(false)]
        //public decimal DiestelComission { get; set; }

        //[NotMapped]
        //[ScaffoldColumn(false)]
        //public decimal DiestelAmountToCharge { get; set; }

    }
}