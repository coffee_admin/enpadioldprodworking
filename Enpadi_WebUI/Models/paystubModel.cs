﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enpadi_WebUI.DAO;

namespace Enpadi_WebUI.Models
{
    public class paystubModel : paystub
    {
        public class keys {
            public const String PagoBanco = "9F275E39-745C-4015-9EEB-0746F8D2FF7B";
            public const String PagoTienda = "9F275E39-745C-4015-9EEB-0746F8D2FF7B";
        }

        public paystubModel() {
            Id = Guid.NewGuid();
            date_created = DateTime.Now;
        }

        public Guid Id { get; set; }
        public Guid paystub_type_id { get; set; }
        public DateTime date_created { get; set; }
        public DateTime date_modified { get; set; }
        public String created_by_email { get; set; }
        public String open_pay_charge_id { get; set; }
        public String open_pay_order_id { get; set; }
    }
}