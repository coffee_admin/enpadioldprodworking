﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enpadi_WebUI.Models
{
    [Table("Estados")]
    public class estado
    {
        [Key]
        public int edoID { get; set; }

        [Display(Name="Estado")]
        public string nombre { get; set; }

        [Display(Name="Abreviación")]
        public string abreviacion { get; set; }

        [Display(Name="Pais")]
        public int paisID { get; set; }
        public virtual pais paises { get; set; }
    }
}