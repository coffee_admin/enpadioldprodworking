﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enpadi_WebUI.Models
{
    [Table("posFotos")]
    public class posFoto
    {
        [Key]
        public int fotoID { get; set; }

        [Display(Name="Producto")]
        public int productoID { get; set; }

        public virtual posProducto posProducto { get; set; }

        [Display(Name="Imagen")]
        public string imagen { get; set; }

        [Display(Name="Descripción")]
        public string descripcion { get; set; }
    }
}