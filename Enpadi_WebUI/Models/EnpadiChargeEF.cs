﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    [Table("EnpadiCharge")]
    public class EnpadiChargeEF
    {
        public EnpadiChargeEF()
        {
            id = Guid.NewGuid();
        }

        [Key]
        public Guid id { get; set; }
        public Boolean livemode { get; set; }
        public Decimal amount { get; set; }
        public String currency { get; set; }
        public String object_description { get; set; }
        public Decimal fee { get; set; }
        public String customer_id { get; set; }
        public Guid order_id { get; set; }
        public String customer_email { get; set; }

        public String service_name { get; set; }
        public String pay_met_obj_desc { get; set; }
        public String payment_method_type { get; set; }
        public String expires_at { get; set; }
        public String reference { get; set; }
        public String charge_type { get; set; }

        
        public String deviceSessionId { get; set; }
        public String source_id { get; set; }
        public String name_on_card { get; set; }
        public Int32 credit_card_type { get; set; }
        public String card_number { get; set; }
        public Int32 credit_card_expiration_month { get; set; }
        public Int32 credit_card_expiration_year { get; set; }
        public Int32 credit_card_security_code { get; set; }
        public Int32 credit_card_maestro_switch { get; set; }

    }
}