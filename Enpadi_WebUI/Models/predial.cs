﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enpadi_WebUI.Models
{
    [Table("Predial")]
    public class predial
    {
        [Key]
        public int predialID { get; set; }

        [Display(Name="Socio")]
        public int socioID { get; set; }

        [Display(Name = "Expediente")]
        public string expedienteNo { get; set; }

        [Display(Name = "Calle")]
        public string calle { get; set; }

        [Display(Name = "Num Ext")]
        public string numExterior { get; set; }

        [Display(Name = "Num Int")]
        public string numInterior { get; set; }

        [Display(Name = "Entre Calles")]
        public string referencia { get; set; }

        [Display(Name = "Colonia")]
        public string colonia { get; set; }

        [Display(Name = "Ciudad")]
        public int cdID { get; set; }
        public virtual ciudad ciudad { get; set; }

        [Display(Name = "Estado")]
        public int edoID { get; set; }
        public virtual estado estado { get; set; }

        [Display(Name = "Pais")]
        public int paisID { get; set; }
        public virtual pais pais { get; set; }

        [Display(Name = "CP")]
        public string cp { get; set; }

        [Display(Name = "Latitud")]
        public double latitud { get; set; }

        [Display(Name = "Longitud")]
        public double longitud { get; set; }

        [Display(Name = "Zoom")]
        public int zoom { get; set; }

        [Display(Name = "Nombre")]
        public string nombre { get; set; }

        [Display(Name = "Apellido Paterno")]
        public string aPaterno { get; set; }

        [Display(Name = "Apellido Materno")]
        public string aMaterno { get; set; }

        [Display(Name = "Direccion")]
        public string direccion { get; set; }

        [Display(Name = "Telefono")]
        public string telefono { get; set; }

        [Display(Name = "Celular")]
        public string celular { get; set; }

        [Display(Name = "Correo Electronico")]
        public string email { get; set; }

        [Display(Name = "Fecha Alta")]
        public DateTime fechaAlta { get; set; }

        [Display(Name = "Fecha Actualizacion")]
        public DateTime fechaActual { get; set; }
    }
}