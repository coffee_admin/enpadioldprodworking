﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    [Table("ServiciosFeePlatfom")]
    public class ServiciosFeePlatfom
    {
        [Key]
        public Guid Id { get; set; }

        public Guid FeeId { get; set; }

        //public Guid ServiceCategoryId { get; set; }

        //public Guid PlatformId { get; set; }

        public Guid UserId { get; set; }

    }
}