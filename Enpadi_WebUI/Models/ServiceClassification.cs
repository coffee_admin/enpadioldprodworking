﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    [Table("ServiceClassification")]
    public class ServiceClassification
    {

        public static class Keys
        {
            public static readonly Guid Water = Guid.Parse("C3F65E6C-A246-4CFD-B80C-A53FEB0C71F9");
            public static readonly Guid Light = Guid.Parse("1B07CBCF-4982-4517-8925-C86841AB3358");
            public static readonly Guid TV = Guid.Parse("9190436E-6BE8-47B7-BE10-FD050E09DC55");
            public static readonly Guid Gas = Guid.Parse("CE1C8C2E-441E-47A9-B3E9-B6296190E427");
            public static readonly Guid Government = Guid.Parse("1C200D98-AD46-4A6C-B915-6B97DBC14A26");
            public static readonly Guid Phone = Guid.Parse("E6449C7B-692D-4949-934B-31503FE65812");
            public static readonly Guid Insurance = Guid.Parse("BBD6DAB6-5B46-4D49-AD83-7BA5B18B8F4C");
            public static readonly Guid Housing = Guid.Parse("D1DDEFC0-4F85-4366-A826-BE30AD51E95F");
        }

        [Key]
        public Guid Id { get; set; }

        public String Name { get; set; }

        public String Description { get; set; }
    }
}