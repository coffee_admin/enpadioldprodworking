﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enpadi_WebUI.Models
{
    [Table("Tarjetas")]
    public class tarjetas
    {
        public Guid EnpadiCardId { get; set; }

        [Key]
        public int tarjetaID { get; set; }

        [Display(Name = "Producto")]
        public int productoID { get; set; }
        public virtual producto productos { get; set; }

        [Display(Name = "Moneda")]
        public int monedaID { get; set; }
        //public virtual moneda monedas { get; set; }

        [Display(Name = "Cuenta")]
        public string tcNum { get; set; }

        [Display(Name = "MD5")]
        public string tcMD5 { get; set; }

        [Display(Name = "Mes")]
        public string tcMes { get; set; }

        [Display(Name = "Año")]
        public string tcAnio { get; set; }

        [Display(Name = "CCV/CVV")]
        public string tcDigitos { get; set; }

        [Display(Name = "NIP")]
        public string tcNIP { get; set; }

        [Display(Name = "Asignada")]
        public bool tcAsignada { get; set; }

        [Display(Name = "Activa")]
        public bool tcActiva { get; set; }

        [Display(Name = "Limite de Activacion")]
        public decimal limActivacion { get; set; }

        [Display(Name = "Deposito Minimo")]
        public decimal limDepMin { get; set; }

        [Display(Name = "Deposito Maximo")]
        public decimal limDepMax { get; set; }

        [Display(Name = "Limite Deposito Diario")]
        public decimal limDepDia { get; set; }

        [Display(Name = "Limite Deposito Semanal")]
        public decimal limDepSem { get; set; }

        [Display(Name = "Limite Deposito Mensual")]
        public decimal limDepMes { get; set; }

        [Display(Name = "Limite Retiro Minimo")]
        public decimal limRetMin { get; set; }

        [Display(Name = "Limite Retiro Maximo")]
        public decimal limRetMax { get; set; }

        [Display(Name = "Limite Retiro Diario")]
        public decimal limRetDia { get; set; }

        [Display(Name = "Limite Retiro Semanal")]
        public decimal limRetSem { get; set; }

        [Display(Name = "Limite Retiro Mensual")]
        public decimal limRetMes { get; set; }

        [Display(Name = "Transferencia Minima")]
        public decimal limTransMin { get; set; }

        [Display(Name = "Transferencia Maxima")]
        public decimal limTransMax { get; set; }

        [Display(Name = "Limite Transferencia Diario")]
        public decimal limTransDia { get; set; }

        [Display(Name = "Limite Transferencia Semanal")]
        public decimal limTransSem { get; set; }

        [Display(Name = "Limite Transferencia Mensual")]
        public decimal limTransMes { get; set; }

        [Display(Name = "Titulo")]
        public string titulo { get; set; }

        [Display(Name = "Nombre(s)")]
        public string datNombre { get; set; }

        [Display(Name = "A.Paterno")]
        public string datPaterno { get; set; }

        [Display(Name = "A.Materno")]
        public string datMaterno { get; set; }

        [Display(Name = "Genero")]
        public int genero { get; set; }

        [Display(Name = "Fecha de Nacimiento")]
        public DateTime datNacimiento { get; set; }

        [Display(Name = "Dirección")]
        public string datDir1 { get; set; }

        [Display(Name = "Direccion 2")]
        public string datDir2 { get; set; }

        [Display(Name = "Colonia")]
        public string datColonia { get; set; }

        [Display(Name = "Pais")]
        public int paisID { get; set; }
        public virtual pais paises { get; set; }

        [Display(Name = "Estado")]
        public int edoID { get; set; }
        public virtual estado estados { get; set; }

        [Display(Name = "Ciudad")]
        public int cdID { get; set; }
        public virtual ciudad ciudades { get; set; }

        [Display(Name = "Codigo Postal")]
        public string datCP { get; set; }

        [Display(Name = "Telefono")]
        public string datTelefono { get; set; }

        [Display(Name = "Fax")]
        public string datFax { get; set; }

        [Display(Name = "Celular")]
        public string datCelular { get; set; }

        [Display(Name = "Nextel")]
        public string datNextel { get; set; }

        [Display(Name = "Blackberry PIN")]
        public string datBlackPin { get; set; }

        [Display(Name = "Identificacion Oficial")]
        public int? identificacionID { get; set; }

        public string identificacionInfo { get; set; }
        public Byte[] identificacionFile { get; set; }
        public bool identificacionValida { get; set; }

        [Display(Name = "Email")]
        public string email { get; set; }

        [Display(Name = "Password")]
        public string password { get; set; }




        public DateTime? fechaAlta { get; set; }
        public DateTime? fechaBaja { get; set; }
        public decimal? creditoDisponible
        {
            get;
            set;
        }
        public decimal? creditoPorPagar
        {
            get;
            set;
        }
    }
}