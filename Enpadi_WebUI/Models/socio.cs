﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{

    [Table("Socios")]
    public class socio
    {

        #region keys
        [NotMapped]
        public static class SocioID
        {

            public const string VisaStr = "visa";
            public const string MasterCardStr = "mastercard";
            public const string AmericanExpressStr = "amex";

            public const int Visa = 1;
            public const int Amex = 2;
            public const int Banco = 3;
            public const int Tienda = 4;
            public const int Tiempo_aire = 5;
            public const int Pago_de_servicios = 6;
            public const int Santiago_municipio = 7;
            public const int Cloud_transfer = 8;
            public const int Enpadi_int_id = 9;
            public const int Consulate_int_id = 10;
            public const int Imss_int_id = 11;
            public const int Cemex_int_id = 12;

            public static readonly Guid Visa_guid_id = Guid.Parse("AF4C3043-F8B0-4DC6-A9D0-83A0136EEAEB");
            public static readonly Guid Amex_guid_id = Guid.Parse("F63B5309-26A5-43A5-928B-E57728CA9543");
            public static readonly Guid Banco_guid_id = Guid.Parse("03E89F13-72E8-4E7E-A1E9-7D99C4953157");
            public static readonly Guid Tienda_guid_id = Guid.Parse("511043D5-3474-47C1-8870-69ED9E64C6AF");
            public static readonly Guid Tiempo_aire_guid_id = Guid.Parse("2F34BF9D-92CA-4B45-BEF2-E17E8EA1303E");
            public static readonly Guid Pago_de_servicios_guid_id = Guid.Parse("2D72B3E5-BAFD-48B7-A264-E781AD748280");
            public static readonly Guid Santiago_municipio_guid_id = Guid.Parse("0C56D599-2DFC-4944-BB64-7AD4B1E3E547");
            public static readonly Guid Cloud_transfer_guid_id = Guid.Parse("17B298E5-77AC-44EA-BBA0-4F581611FFE9");
            public static readonly Guid Imss_guid_id = Guid.Parse("09021502-6CAC-4FBF-81F6-0EFD9579FFC2");
            public static readonly Guid Cemex_guid_id = Guid.Parse("DF389155-399D-4B61-9005-CACB3A5878AD");


            public static readonly Guid Enpadi = Guid.Parse("FDDDEA56-2AB9-46E8-9FAA-D04D0452A72C");
            public static readonly Guid Consulate = Guid.Parse("7E8337F8-F6E3-4204-AA8A-7F36798BAC51");
        }

        #endregion keys
        [Key]
        public Guid PartnerId { get; set; }
        //public virtual socio Partner { get; set; }

       
        public int socioID { get; set; }

        [Display(Name="Asociado")]
        public string nombreComercial { get; set; }

        [Display(Name="Razón Social")]
        public string razonSocial { get; set; }
        public string rfc { get; set; }
        public string datNombre { get; set; }
        public string datDir1 { get; set; }
        public string datDir2 { get; set; }
        public string datColonia { get; set; }

        public int? paisID { get; set; }
        public virtual pais pais { get; set; }

        public int? edoID { get; set; }
        public virtual estado estado { get; set; }

        public int? cdID { get; set; }
        public virtual ciudad ciudad { get; set; }

        public string datCP { get; set; }
        public string datTelefono { get; set; }
        public string datEmail { get; set; }

        [Display(Name="Contacto")]
        public string datContacto { get; set; }

        [Display(Name="Activo")]
        public bool? activo { get; set; }
        public string admnUSR { get; set; }
        public string admnPASS { get; set; }

        [Display(Name="Ventas")]
        public bool? ventas { get; set; }

        [Display(Name="Recargas")]
        public bool? recargas { get; set; }

        [Display(Name="Remesas")]
        public bool? remesas { get; set; }

        [Display(Name="Recompensas")]
        public bool? recompensas { get; set; }

        public DateTime? fechaAlta { get; set; }

        public DateTime? fechaBaja { get; set; }
    }
}