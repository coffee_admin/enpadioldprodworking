﻿using System;
using System.Collections.Generic;

namespace Enpadi_WebUI.Models
{
    public class ViewModelPredial
    {
        public string folioRecibo { get; set; }
        public DateTime fecha { get; set; }
        public predial predial { get; set; }
        public List<predialMovimiento> movimientos {get; set;}
        public decimal saldoTotal { get; set; }
        public string folioRifa { get; set; }
    }
}