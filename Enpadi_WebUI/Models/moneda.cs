﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enpadi_WebUI.Models
{
    [Table("Monedas")]
    public class moneda
    {

        [NotMapped]
        public class currency
        {
            [NotMapped]
            public const String MexicanPeso = "MXN";
            [NotMapped]
            public const String USD = "USD";

            public static readonly Guid MexicanPesoId = Guid.Parse("897DA134-BB4D-4222-83B0-FAB99B0B33EE");
            public const Int32 MexicanPesoIntId = 484;

            public static readonly Guid UsdId = Guid.Parse("F9D262C4-24A0-4D51-B6AF-1E165F868A62");
        }

        [Key]
        public Guid CurrencyId { get; set; }

        public int monedaID { get; set; }

        [Display(Name = "Codigo")]
        public string codigo { get; set; }

        [Display(Name = "Moneda")]
        public string descripcion { get; set; }

        [Display(Name = "Activa")]
        public bool? activa { get; set; }

        public DateTime? fechaAlta { get; set; }

        public DateTime? fechaBaja { get; set; }
    }
}