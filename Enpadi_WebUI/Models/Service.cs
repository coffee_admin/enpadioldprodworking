﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    [Table("Service")]
    public class Service
    {

        static ApplicationDbContext db = new ApplicationDbContext();

        [NotMapped]
        public static class Keys
        {
            public static readonly Guid IMSS = Guid.Parse("CD309442-0226-4848-88F9-B1C3B3EFF757");
        }

        [Key]
        public Guid Id { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 SKU { get; set; }

        public String Name { get; set; }

        public String Description { get; set; }

        public Int32? ReferenceLength { get; set; }

        public String PaymentInstruction { get; set; }

        public String Company { get; set; }

        public String LogoUrl { get; set; }

        public Boolean Active { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? DeletedAt { get; set; }

        public string Location { get; set; }

        public Int32? Order { get; set; }

        public String Command { get; set; }

        public Guid ServiceTypeId { get; set; }

        public virtual ServiceType ServiceType { get; set; }

        public Guid ServiceCategoryId { get; set; }

        public virtual ServiceCategory ServiceCategory { get; set; }

        public Guid? ServiceClassificationId { get; set; }

        public virtual ServiceClassification ServiceClassification { get; set; }

        #region methods

     

        [NotMapped]
        public String StringSKU
        {
            get
            {
                return this.SKU.ToString("D6");
            }
        }

        #endregion methods
    }
}