﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    public class ViewModelPayService
    {
        public Service Service { get; set; }
        public List<ServiceChargeOption> ServiceChargeOption { get; set; }
    }
}