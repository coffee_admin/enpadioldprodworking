﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Enpadi_WebUI.Models
{
    [Table("Ciudades")]
    public class ciudad
    {
        [Key]
        public int cdID { get; set; }

        [Display(Name="Ciudad")]
        public string nombre { get; set; }

        [Display(Name="Estado")]
        public int edoID { get; set; }
        public virtual estado estados { get; set; }
    }
}