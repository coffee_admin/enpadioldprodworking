﻿using System.Collections.Generic;

namespace Enpadi_WebUI.Models
{
    public class ViewModelProducto
    {
        public producto producto { get; set; }
        public List<tarjetasListado> tarjetas { get; set; }
        public int Rango { get; set; }
        public int Configuradas { get; set; }
        public int Asignadas { get; set; }
        public int Disponibles { get; set; }
        public int Pendientes { get; set; }
        public int Inicial { get; set; }
        public int Final { get; set; }
    }
}