﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    public class skin
    {
        public int skinID { get; set; }
        public int productoID { get; set; }
        public string name { get; set; }
        public string title { get; set; }
        public string image { get; set; }
        public string loginImage { get; set; }
        public string loginMessage { get; set; }
        public string homeImage { get; set; }
        public string homeColor { get; set; }
        public string colorTitle { get; set; }
        public string colorNavBtn { get; set; }
        public string colorNavBar { get; set; }
        public string colorRecargar { get; set; }
        public string colorPagar { get; set; }
        public string colorTransferir { get; set; }
        public string mobileNavBtn { get; set; }
        public string mobileNavBtnHover { get; set; }
        public string mobileNavBackground { get; set; }
        public string childImage { get; set; }
    }
}