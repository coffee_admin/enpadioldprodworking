﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Enpadi_WebUI.Models
{
    public class ViewModelFeeEditor
    {
        public ViewModelFeeEditor()
        {
            Partner = new List<SelectListItem>();
            Users = new List<SelectListItem>();
            Currencies = new List<SelectListItem>();
            Platforms = new List<SelectListItem>();
            ServiceCategories = new List<SelectListItem>();
        }

        public Fee Fee { get; set; }

        //for drop down value
        public Guid PartnerId { get; set; }
        public Guid UsersId { get; set; }
        public Guid CurrenciesId { get; set; }
        public Guid PlatformsId { get; set; }
        public Guid ServiceCategoriesId { get; set; }

        //for drop down options
        public List<SelectListItem> Partner { get; set; }
        public List<SelectListItem> Users { get; set; }
        public List<SelectListItem> Currencies { get; set; }
        public List<SelectListItem> Platforms { get; set; }
        public List<SelectListItem> ServiceCategories { get; set; }
    }
}