﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enpadi_WebUI.Models
{
    [Table("Recompensas")]
    public class recompensa
    {
        [Key]
        public int recompensaID {get; set;}
        public int tarjetaID { get; set; }
        public virtual tarjetas tarjetas { get; set; }
        public int origenID { get; set; }
        public virtual origen origenes { get; set; }
        public virtual socio socios { get; set; }
        public string transaccion{ get; set; }
        public string concepto { get; set; }
        public decimal abono { get; set; }
        public decimal cargo { get; set; }

        public Guid PartnerId { get; set; }
        public int socioID { get; set; }
    }
}