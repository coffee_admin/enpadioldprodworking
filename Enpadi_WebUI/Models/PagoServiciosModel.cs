﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enpadi_WebUI.DAO;

namespace Enpadi_WebUI.Models
{
    public class PagoServiciosModel : PagoServicios
    {
        public Int32 pagoID { get; set; }
        public Int32 tarjetaID { get; set; }
        public String sku { get; set; }
        public String nombre { get; set; }
        public String compañia { get; set; }
        public String referencia { get; set; }
        public String referencia2 { get; set; }
        public Decimal? monto { get; set; }
        public Decimal? comision { get; set; }
        public String fechaLocal{ get; set; }
        public String horaLocal { get; set; }
        public String fechaContable { get; set; }
        public Int32? codigoRespuesta { get; set; }
        public String codigoRespuestaDescr { get; set; }
        public String tipoPago { get; set; }
        public String autorizacion { get; set; }
        public String brand { get; set; }
        public String leyenda { get; set; }
        public String leyenda1 { get; set; }
        public String leyenda2 { get; set; }
        public String dv { get; set; }
        public String token { get; set; }
        public String proveedor { get; set; }
        public Int32? estatus { get; set; }
        public DateTime? fechaRegistro { get; set; }
        public DateTime? fechaBaja { get; set; }


    }
}