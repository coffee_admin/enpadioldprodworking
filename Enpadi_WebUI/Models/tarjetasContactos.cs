﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enpadi_WebUI.Models
{
    [Table("TarjetasContactos")]
    public class tarjetasContactos
    {
        [Key]
        public int contactoID { get; set; }

        [Display(Name="Titular")]
        public int tarjetaID { get; set; }

        [Display(Name="Cuenta")]
        [Required]
        public string contactoTarjeta { get; set; }
        
        [Display(Name="Id Contacto")]
        public int contactoNumero { get; set; }

        [Display(Name="Contacto")]
        [Required]
        public string contactoAlias { get; set; }
    }
}