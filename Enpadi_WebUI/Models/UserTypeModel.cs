﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enpadi_WebUI.DAO;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Enpadi_WebUI.Models
{
    [Table("UserType")]
    public class UserTypeModel : UserType
    {

        [NotMapped]
        public static class Keys {
            public static readonly Guid ExternalSystem = Guid.Parse("853F07A2-60BB-4A77-A187-44EB039EECCE");
            public static readonly Guid Partner = Guid.Parse("542E91BC-4D86-4DC6-ABD8-773DB6A00EE9");
            public static readonly Guid System = Guid.Parse("7FEBB56C-DBA1-4EEA-B55C-7C47C3118AD0");
            public static readonly Guid AdminUser = Guid.Parse("1C33E231-718A-4986-A5D2-C8E3EC3D2B12");
            public static readonly Guid PlatformUser = Guid.Parse("F3EC4132-2228-4BAD-983C-E868A29751F5");
            public static readonly Guid DevUser = Guid.Parse("7E64E4EE-98F2-48DD-BBAB-F71F60E2FBAC");
        }

        [Key]
        public Guid id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }
}