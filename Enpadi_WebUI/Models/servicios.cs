﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Enpadi_WebUI.Models
{
    [Table("Servicios")]
    public class servicios
    {
        static ApplicationDbContext db = new ApplicationDbContext();

        public servicios()
        {
            ServiceCategory = new List<SelectListItem>();
            ServiceType = new List<SelectListItem>();
        }

        [NotMapped]
        static public class keys
        {
            public const string imss_new = "9398350960002";
            public const string imss_existing = "9398350960001";

            public const string cemex_100 = "000000000007";
            public const string cemex_200 = "000000000008";
            public const string cemex_300 = "000000000009";
            public const string cemex_400 = "000000000010";
            public const string cemex_500 = "000000000011";
            public const string cemex_10 = "000000000013";
            public const string cemex_5 = "000000000012";

            public static readonly Guid guidExternalPayment = Guid.Parse("0279BA61-9B3D-4682-98EF-B7E73A06F0F0");
            public const string externalPayment = "000000000014";
        }

        [Key]
        public Guid Id { get; set; }

        [Display(Name = "Monto")]
        public Decimal? Amount { get; set; }

        [Display(Name = "Aplica un monto fijo")]
        public Boolean DoesAmountApply { get; set; }

        [Display(Name = "SKU")]
        public string SKU { get; set; }

        [Display(Name = "Servicio")]
        public string Nombre { get; set; }

        [Display(Name = "Descripcion")]
        public string Descripcion { get; set; }

        [Display(Name = "Tipo de Referencia")]
        public string ReferenciaTipo { get; set; }

        [Display(Name = "Longitud de Referencia")]
        public int? ReferenciaLongitud { get; set; }

        [Display(Name = "Instrucciones")]
        public string Instrucciones { get; set; }

        [Display(Name = "Compañia")]
        public string Compañia { get; set; }

        [Display(Name = "Imagen Compañia")]
        public string imagenCompañia { get; set; }

        [Display(Name = "Tipo")]
        public string Tipo { get; set; }

        [Display(Name = "Imagen Tipo")]
        public string imagenTipo { get; set; }

        [Display(Name = "Activo")]
        public bool Activo { get; set; }

        [Display(Name = "Fecha Alta")]
        public DateTime? FechaAlta { get; set; }

        public DateTime? FechaBaja { get; set; }

        public Guid ServiceCategoryId { get; set; }

        public Guid ServiceTypeId { get; set; }

        [NotMapped]
        [Display(Name = "Categoria de Servicio")]
        public List<SelectListItem> ServiceCategory { get; set; }

        [NotMapped]
        [Display(Name = "Tipo de Servicio")]
        public List<SelectListItem> ServiceType { get; set; }

        [NotMapped]
        [Display(Name = "Categoria de Servicio")]
        public String ServiceCategoryName { get; set; }

        [NotMapped]
        [Display(Name = "Tipo de Servicio")]
        public String ServiceTypeName { get; set; }

        public static List<servicios> GetPayServices()
        {

            List<servicios> payServices = (from item in db.servicios
                                           where item.ServiceTypeId == Enpadi_WebUI.Models.ServiceType.Keys.Pay
                                           select item).ToList();

            return payServices;
        }
    }
}