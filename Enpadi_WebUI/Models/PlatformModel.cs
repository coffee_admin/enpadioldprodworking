﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enpadi_WebUI.DAO;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Enpadi_WebUI.Models
{
    [Table("Platform")]
    public class PlatformModel
    {
        [NotMapped]
        public class Keys {
            public static readonly Guid AllPlatforms = Guid.Parse("30CE45E3-B07F-4F95-9F7A-A4165DC048EA");
            public static readonly Guid Enpadi = Guid.Parse("42EA595C-4F8D-429F-BF96-652BF8D44D59");
            public static readonly Guid EnpadiKiosko = Guid.Parse("15638E4A-9312-45A3-B6BC-E38A4B782F03");
            public static readonly Guid IguanaPay = Guid.Parse("ECBB4EF2-3F82-4A71-B4C1-BC08EB8B5AFE");

            public static readonly Guid WebUsa = Guid.Parse("9E491986-6CC9-4F1D-8A05-18F214F1A647");

            public static readonly Guid UsaApi = Guid.Parse("F918B180-04BC-4A2A-B3C3-AE5E94357FD2");

            public static readonly Guid MxApi = Guid.Parse("0D7FA55E-D772-4289-B377-7112316DAF7F");
        }

        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

    }
}