﻿namespace Enpadi_WebUI.Models
{
    public class resumenUnitario
    {
        public int Periodo { get; set; }
        public string Mes { get; set; }
        public int Total { get; set; }
    }
}