﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enpadi_WebUI.Models;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Configuration;

namespace Enpadi_WebUI.Models
{
    public class EnpadiPostOrder
    {

        public EnpadiOrderModel order = new EnpadiOrderModel();
        public EnpadiChargeModel charge = new EnpadiChargeModel();

        #region methods

        public static List<Fee> GetFeeForService(Guid userId, Guid serviceId)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            Enpadi_WebUI.Models.servicios service = db.servicios.FirstOrDefault(x => x.Id == serviceId);

            String region = ConfigurationManager.AppSettings["Region"].ToString();

            List<Fee> fees = new List<Fee>();

            if (region == "usaapi")
            {
                fees = Enpadi_WebUI.Models.Fee.GetUsaApiServiceFees(userId, service.ServiceCategoryId);
            }
            else if (region == "mxapi")
            {
                fees = Enpadi_WebUI.Models.Fee.GetMxApiServiceFees(userId, service.ServiceCategoryId);
            }
            else
            {
                new ArgumentException("wrong url please verify the api you are trying to access.");
            }


            return fees;

        }

        /// <summary>
        /// deprecated use JsonStringToModelEF
        /// </summary>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        public static EnpadiPostOrder JsonStringToModel(String jsonString)
        {

            ApplicationDbContext db = new ApplicationDbContext();

            try
            {
                JObject job = JObject.Parse(jsonString);
                EnpadiOrderModel order = new EnpadiOrderModel();
                EnpadiOrder orderEF = new EnpadiOrder();
                EnpadiChargeModel charge = new EnpadiChargeModel();
                EnpadiChargeEF chargeEF = new EnpadiChargeEF();

                string postType = ((JToken)JObject.Parse(jsonString))["type"].ToString();

                EnpadiPostOrder response = new EnpadiPostOrder();

                if (postType == "order.create")
                {
                    db.EnpadiOrder.Add(orderEF);
                    db.Entry(orderEF).State = System.Data.Entity.EntityState.Added;
                    db.SaveChanges();

                    order.data_object.id = orderEF.id;

                    JToken type = job["type"];

                    JToken orderdata_livemode = job["orderdata"]["livemode"];
                    JToken orderdata_amount = job["orderdata"]["amount"];
                    JToken orderdata_currency = job["orderdata"]["currency"];
                    JToken orderdata_amount_refunded = job["orderdata"]["amount_refunded"];
                    JToken orderdata_external_id = job["orderdata"]["external_id"];
                    JToken orderdata_external_description = job["orderdata"]["external_description"];
                    JToken orderdata_external_category = job["orderdata"]["external_category"];
                    JToken orderdata_object_description = job["orderdata"]["object_description"];
                    JToken orderdata_client_email = job["orderdata"]["client_email"];
                    JToken orderdata_client_trans_password = job["orderdata"]["client_trans_password"];
                    JToken orderdata_customer_email = job["orderdata"]["customer_info"]["email"];
                    JToken orderdata_customer_phoneNumber = job["orderdata"]["customer_info"]["phoneNumber"];
                    JToken orderdata_customer_name = job["orderdata"]["customer_info"]["name"];
                    JToken orderdata_customer_last_name = job["orderdata"]["customer_info"]["last_name"];
                    JToken orderdata_customer_externalId = job["orderdata"]["customer_info"]["externalId"];
                    JToken orderdata_customer_object_description = job["orderdata"]["customer_info"]["object_description"];

                    JToken orderdata_customer_address_line1 = job["orderdata"]["customer_info"]["address_line1"];
                    JToken orderdata_customer_address_line2 = job["orderdata"]["customer_info"]["address_line2"];
                    JToken orderdata_customer_address_line3 = job["orderdata"]["customer_info"]["address_line3"];
                    JToken orderdata_customer_address_city = job["orderdata"]["customer_info"]["address_city"];
                    JToken orderdata_customer_address_state = job["orderdata"]["customer_info"]["address_state"];
                    JToken orderdata_customer_address_country_code = job["orderdata"]["customer_info"]["address_country_code"];
                    JToken orderdata_customer_address_postal_code = job["orderdata"]["customer_info"]["address_postal_code"];

                    dynamic jsonObj = JsonConvert.DeserializeObject(jsonString);
                    List<EnpadiLineItemModel> items = new List<EnpadiLineItemModel>();

                    foreach (var obj in jsonObj.orderdata.line_items)
                    {

                        //TODO: check the service, check the fee and if is not correct throw an exception
                        JToken service_sku = obj["service_sku"];

                        string emailEnpadiAccountPayingTo = orderdata_client_email.ToString().Trim();

                        ApplicationUser user = db.Users.FirstOrDefault(x => x.Email == emailEnpadiAccountPayingTo);

                        if (user == null)
                        {
                            throw new ArgumentException(SystemErrorModel.ErrorList.GenericError + "User not found on Enpadi, check client email");
                        }

                        if (service_sku == null)
                        {
                            throw new ArgumentException(SystemErrorModel.ErrorList.GenericError + "Service SKU empty, please check the available services.");
                        }

                        string serviceSku = service_sku.ToString().Trim();

                        servicios service = db.servicios.FirstOrDefault(x => x.SKU == serviceSku);

                        if (service == null)
                        {
                            throw new ArgumentException(SystemErrorModel.ErrorList.GenericError + "Service not found on Enpadi, check SKU");
                        }

                        Guid _guidUserId = Guid.Parse(user.Id);

                        List<Fee> fees = Enpadi_WebUI.Models.EnpadiPostOrder.GetFeeForService(_guidUserId, service.Id);

                        String region = ConfigurationManager.AppSettings["Region"].ToString();
                        decimal totalFeesInMXN = 0;
                        if (region == "usaapi")
                        {
                            totalFeesInMXN = Enpadi_WebUI.Models.Fee.GetTotalFeeInMXN(fees, Decimal.Parse(orderdata_amount.ToString()), Controllers.Tools.GetPesoToDollarRate(), Enpadi_WebUI.Models.PlatformModel.Keys.UsaApi, 0, false);
                        }
                        else if (region == "mxapi")
                        {

                            //TODO check fees from diestel and change the 0 value
                            decimal diestelFee = 0;
                            //diestel check service sku and fees
                            totalFeesInMXN = Enpadi_WebUI.Models.Fee.GetTotalFeeInMXN(fees, Decimal.Parse(orderdata_amount.ToString()), Controllers.Tools.GetPesoToDollarRate(), Enpadi_WebUI.Models.PlatformModel.Keys.UsaApi, diestelFee, false);
                        }

                        //TODO: if amount does not match with fees
                        //throw new argumentexception with "fees does not match with the total amount send"

                        JToken unit_price = obj["unit_price"];
                        JToken quantity = obj["quantity"];
                        JToken name = obj["name"];
                        JToken description = obj["description"];

                        items.Add(new EnpadiLineItemModel
                        {
                            unit_price = Decimal.Parse(unit_price.ToString()) / 100,
                            quantity = Int32.Parse(quantity.ToString()),
                            name = name.ToString(),
                            description = description.ToString(),
                            orderId = order.data_object.id,
                            created_at = DateTime.Now.ToString(),
                            customer_email = orderdata_customer_email.ToString(),
                        });
                    }

                    order.data_object.line_items = items;
                    order.data_object.livemode = Boolean.Parse(orderdata_livemode.ToString());
                    order.data_object.amount = Decimal.Parse(orderdata_amount.ToString()) / 100;
                    order.data_object.currency = orderdata_currency.ToString();
                    order.data_object.amount_refunded = (orderdata_amount_refunded.ToString() != "0" ? Decimal.Parse(orderdata_amount_refunded.ToString()) : 0m);
                    order.data_object.external_id = orderdata_customer_externalId.ToString();
                    order.data_object.external_description = orderdata_external_description.ToString();
                    order.data_object.external_category = orderdata_external_category.ToString();

                    //if order comes whit a charge create charge
                    if (job["chargedata"] != null)
                    {
                        JToken chargedata_livemode = job["chargedata"]["livemode"];
                        JToken chargedata_amount = job["chargedata"]["amount"];
                        JToken chargedata_currency = job["chargedata"]["currency"];

                        if (job["chargedata"]["payment_method"] != null)
                        {
                            JToken chargedata_service_name = job["chargedata"]["payment_method"]["service_name"];
                            JToken chargedata_object_description = job["chargedata"]["payment_method"]["object_description"];
                            JToken chargedata_payment_method_type = job["chargedata"]["payment_method"]["payment_method_type"];
                            JToken chargedata_expires_at = job["chargedata"]["payment_method"]["expires_at"];
                            JToken chargedata_reference = job["chargedata"]["payment_method"]["reference"];


                            if (chargedata_payment_method_type.ToString().ToLower() == "card")
                            {
                                JToken chargedata_deviceSessionId = job["chargedata"]["payment_method"]["deviceSessionId"];
                                JToken chargedata_source_id = job["chargedata"]["payment_method"]["source_id"];

                                charge.data_object.payment_method.deviceSessionId = chargedata_deviceSessionId.ToString();
                                charge.data_object.payment_method.source_id = chargedata_source_id.ToString();
                            }

                            charge.data_object.payment_method.service_name = chargedata_service_name.ToString();
                            charge.data_object.payment_method.object_description = chargedata_object_description.ToString();
                            charge.data_object.payment_method.payment_method_type = chargedata_payment_method_type.ToString();
                            charge.data_object.payment_method.expires_at = chargedata_expires_at.ToString();
                            charge.data_object.payment_method.reference = chargedata_reference.ToString();

                            if (chargedata_payment_method_type.ToString().Trim().ToLower() == "card"
                                )
                            {
                                JToken chargedata_name_on_card = job["chargedata"]["payment_method"]["name_on_card"];
                                JToken chargedata_credit_card_type = chargedata_payment_method_type.ToString();
                                JToken chargedata_card_number = job["chargedata"]["payment_method"]["card_number"];
                                JToken chargedata_credit_card_expiration_month = job["chargedata"]["payment_method"]["credit_card_expiration_month"];
                                JToken chargedata_credit_card_expiration_year = job["chargedata"]["payment_method"]["credit_card_expiration_year"];
                                JToken chargedata_credit_card_security_code = job["chargedata"]["payment_method"]["credit_card_security_code"];
                                JToken chargedata_credit_card_maestro_switch = job["chargedata"]["payment_method"]["credit_card_maestro_switch"];

                                charge.data_object.payment_method.name_on_card = chargedata_name_on_card.ToString();
                                charge.data_object.payment_method.credit_card_type = EnpadiChargeModel.GetPartnerId(chargedata_credit_card_type.ToString());
                                charge.data_object.payment_method.card_number = chargedata_card_number.ToString();
                                charge.data_object.payment_method.credit_card_expiration_month = Int32.Parse(chargedata_credit_card_expiration_month.ToString());
                                charge.data_object.payment_method.credit_card_expiration_year = Int32.Parse(chargedata_credit_card_expiration_year.ToString());
                                charge.data_object.payment_method.credit_card_security_code = Int32.Parse(chargedata_credit_card_security_code.ToString());
                                //charge.data_object.payment_method.credit_card_maestro_switch = Int32.Parse(chargedata_credit_card_maestro_switch.ToString());
                            }

                            charge.data_object.object_description = chargedata_object_description.ToString();
                            charge.data_object.fee = EnpadiChargeModel.GetCharge(chargedata_payment_method_type.ToString(), charge.data_object.amount);
                        }

                        charge.data_object.livemode = Boolean.Parse(chargedata_livemode.ToString());
                        charge.data_object.amount = Decimal.Parse(chargedata_amount.ToString()) / 100;
                        charge.data_object.currency = chargedata_currency.ToString();
                        charge.data_object.customer_id = String.Empty; //TODO: need to search on the databas to set customer id
                        charge.data_object.customer_email = orderdata_customer_email.ToString();

                        charge.data_object.order_id = order.data_object.id;
                        charge.data_object.charge_type = EnpadiChargeModel.ChargeEstatus.charge_created;

                        if (order.data_object.amount != charge.data_object.amount)
                        {
                            throw new ArgumentException(SystemErrorModel.ErrorList.AmountInChargeAndOrderMismatch);
                        }
                    }

                    order.data_object.object_description = orderdata_object_description.ToString();
                    order.data_object.createdAt = ToolsBO.DateTimeToTimeStamp(DateTime.Now);
                    order.data_object.updatedAt = String.Empty;

                    order.data_object.client_email = orderdata_client_email.ToString();
                    order.data_object.client_trans_password = orderdata_client_trans_password.ToString();
                    order.data_object.type = type.ToString();

                    order.data_object.customer_info.email = orderdata_customer_email.ToString();
                    order.data_object.customer_info.phoneNumber = orderdata_customer_phoneNumber.ToString();
                    order.data_object.customer_info.name = orderdata_customer_name.ToString();
                    order.data_object.customer_info.last_name = orderdata_customer_last_name.ToString();
                    order.data_object.customer_info.externalId = orderdata_customer_externalId.ToString();
                    order.data_object.customer_info.object_description = orderdata_customer_object_description.ToString();


                    order.data_object.customer_info.address_line1 = orderdata_customer_address_line1.ToString();
                    order.data_object.customer_info.address_line2 = orderdata_customer_address_line2.ToString();
                    order.data_object.customer_info.address_line3 = orderdata_customer_address_line3.ToString();
                    order.data_object.customer_info.address_city = orderdata_customer_address_city.ToString();
                    order.data_object.customer_info.address_state = orderdata_customer_address_state.ToString();
                    order.data_object.customer_info.address_country_code = orderdata_customer_address_country_code.ToString();
                    order.data_object.customer_info.address_postal_code = orderdata_customer_address_postal_code.ToString();



                    //Save the new order on EF
                    orderEF.line_items = items;
                    orderEF.livemode = Boolean.Parse(orderdata_livemode.ToString());
                    orderEF.amount = Decimal.Parse(orderdata_amount.ToString()) / 100;
                    orderEF.currency = orderdata_currency.ToString();
                    orderEF.amount_refunded = (orderdata_amount_refunded.ToString() != "0" ? Decimal.Parse(orderdata_amount_refunded.ToString()) : 0m);
                    orderEF.external_id = orderdata_customer_externalId.ToString();
                    orderEF.external_description = orderdata_external_description.ToString();
                    orderEF.external_category = orderdata_external_category.ToString();

                    orderEF.object_description = orderdata_object_description.ToString();
                    orderEF.createdAt = ToolsBO.DateTimeToTimeStamp(DateTime.Now);
                    orderEF.updatedAt = String.Empty;

                    orderEF.client_email = orderdata_client_email.ToString();
                    orderEF.client_trans_password = orderdata_client_trans_password.ToString();
                    orderEF.type = type.ToString();

                    orderEF.customer_email = orderdata_customer_email.ToString();
                    orderEF.customer_phone_number = orderdata_customer_phoneNumber.ToString();
                    orderEF.customer_name = orderdata_customer_name.ToString();
                    orderEF.customer_last_name = orderdata_customer_last_name.ToString();
                    orderEF.customer_external_id = orderdata_customer_externalId.ToString();
                    orderEF.customer_object_description = orderdata_customer_object_description.ToString();


                    orderEF.address_line1 = orderdata_customer_address_line1.ToString();
                    orderEF.address_line2 = orderdata_customer_address_line2.ToString();
                    orderEF.address_line3 = orderdata_customer_address_line3.ToString();
                    orderEF.address_city = orderdata_customer_address_city.ToString();
                    orderEF.address_state = orderdata_customer_address_state.ToString();
                    orderEF.address_country_code = orderdata_customer_address_country_code.ToString();
                    orderEF.address_postal_code = orderdata_customer_address_postal_code.ToString();
                    orderEF.receivedAt = DateTime.Now;

                    ApplicationUser customer = db.Users.Where(x => x.Email == orderEF.customer_email).FirstOrDefault();
                    if (customer != null)
                    {
                        orderEF.customerId = Guid.Parse(customer.Id);
                        order.data_object.localUserId = (Guid) orderEF.customerId;
                        charge.data_object.customer_id = order.data_object.localUserId.ToString();
                    }
                    else {
                        throw new ArgumentException(SystemErrorModel.ErrorList.PaymentCustomerNotFoundOnEnpadi);
                    }

                   
                    chargeEF.livemode = charge.data_object.livemode;
                    chargeEF.amount = charge.data_object.amount;
                    chargeEF.currency = charge.data_object.currency;
                    chargeEF.object_description = charge.data_object.object_description;
                    chargeEF.fee = charge.data_object.fee;
                    chargeEF.customer_id = charge.data_object.customer_id;
                    chargeEF.order_id = charge.data_object.order_id;
                    chargeEF.customer_email = charge.data_object.customer_email;
                    chargeEF.service_name = charge.data_object.payment_method.service_name;
                    chargeEF.pay_met_obj_desc = charge.data_object.payment_method.payment_method_type;
                    chargeEF.payment_method_type = charge.data_object.payment_method.payment_method_type;
                    chargeEF.expires_at = charge.data_object.payment_method.expires_at;
                    chargeEF.reference = charge.data_object.payment_method.reference;
                    chargeEF.charge_type = charge.data_object.charge_type;
                    chargeEF.deviceSessionId = charge.data_object.payment_method.deviceSessionId;
                    chargeEF.source_id = charge.data_object.payment_method.source_id;
                    chargeEF.name_on_card = charge.data_object.payment_method.name_on_card;
                    chargeEF.credit_card_type = charge.data_object.payment_method.credit_card_type;
                    chargeEF.card_number = charge.data_object.payment_method.card_number;
                    chargeEF.credit_card_expiration_month = charge.data_object.payment_method.credit_card_expiration_month;
                    chargeEF.credit_card_expiration_year = charge.data_object.payment_method.credit_card_expiration_year;
                    chargeEF.credit_card_security_code = charge.data_object.payment_method.credit_card_security_code;
                    chargeEF.credit_card_maestro_switch = charge.data_object.payment_method.credit_card_maestro_switch;

                    db.EnpadiCharge.Add(chargeEF);
                    db.Entry(chargeEF).State = System.Data.Entity.EntityState.Added;

                    db.EnpadiOrder.Add(orderEF);
                    db.Entry(orderEF).State = System.Data.Entity.EntityState.Modified;

                    foreach (EnpadiLineItemModel lineItem in order.data_object.line_items) {

                        db.EnpadiLineItem.Add(lineItem);
                        db.Entry(lineItem).State = System.Data.Entity.EntityState.Added;
                    }

                    db.SaveChanges();

                    order.id = orderEF.id;
                    charge.data_object.id = chargeEF.id;

                    response.order = order;
                    response.charge = charge;
                }
                return response;
            }
            catch (Exception e)
            {
                //string json1 = HostingEnvironment.MapPath(@"~/App_Data/EnpadiPostJsonStringModelError.txt");
                //File.AppendAllText(json1, "Test on " + DateTime.Now.ToString() + "-----------------------" + Environment.NewLine);
                //File.AppendAllText(json1, e.Message);

                //TODO need to add the log and error on db
                throw new ArgumentException(SystemErrorModel.ErrorList.GenericError + e.Message);
            }

        }

        public static void SaveEnpadiChargePost(EnpadiChargeModel response)
        {
            try
            {
                EnpadiChargeModel.Save(response);
            }
            catch (Exception e)
            {
                throw new ArgumentException(SystemErrorModel.ErrorList.WebHookGenericError + e.Message);
            }
        }

        public static void SaveEnpadiOrderPost(EnpadiOrderModel response)
        {
            try
            {
                EnpadiOrderModel.Save(response);
                EnpadiLineItemModel.Save(response.data_object.line_items);
            }
            catch (Exception e)
            {
                throw new ArgumentException(SystemErrorModel.ErrorList.WebHookGenericError + e.Message);
            }
        }

        #endregion methods

    }
}