﻿
using System.ComponentModel.DataAnnotations;

namespace Enpadi_WebUI.Models
{
    public class cargoTC
    {
        public string cuenta { get; set; }

        [Required(ErrorMessage="Nombre del Titular tal como aparece en la Tarjeta")]
        [Display(Name="Nombre del Titular")]
        public string Titular { get; set; }

        [Required(ErrorMessage="Número de Tarjeta de Crédito/Débito")]
        [Display(Name="Número de Tarjeta")]
        public string Tarjeta { get; set; }

        [Required(ErrorMessage="CCV")]
        [Display(Name="Código de Seguridad")]
        public string Digitos { get; set; }

        [Required(ErrorMessage="MM/YY")]
        [Display(Name="Fecha de Vencimiento")]
        public string Vencimiento { get; set; }

        [Required(ErrorMessage="Indique la cantidad a Cargar")]
        [Display(Name="Monto de Recarga")]
        public decimal Monto { get; set; }

        public string token_id { get; set; }
    }
}