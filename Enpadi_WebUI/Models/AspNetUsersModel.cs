﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enpadi_WebUI.DAO;

namespace Enpadi_WebUI.Models
{
    public class AspNetUsersModel : AspNetUsers
    {
        public String Id { get; set; }
        public String Email { get; set; }
        public Boolean EmailConfirmed { get; set; }
        public String PasswordHash { get; set; }
        public String SecurityStamp { get; set; }
        public String PhoneNumber { get; set; }
        public Boolean PhoneNumberConfirmed { get; set; }
        public Boolean TwoFactorEnabled { get; set; }
        public DateTime LockoutEndDateUtc { get; set; }
        public Boolean LockoutEnabled { get; set; }
        public Int32 AccessFailedCount { get; set; }
        public String UserName { get; set; }
        public String Name { get; set; }
        public String ProfilePicUrl { get; set; }

        #region for reference only, not on database

        public String Password { get; set; }

        #endregion for reference only, not on database

    }
}