﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enpadi_WebUI.DAO;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enpadi_WebUI.Models
{
    [Table("Tarjetas")]
    public class EnpadiCardModel : EnpadiCard
    {
        [Key]
        public Guid EnpadiCardId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int tarjetaID { get; set; }
        public int productoID { get; set; }
        public virtual producto productos { get; set; }
        public int monedaID { get; set; }
        //public virtual moneda monedas { get; set; }
        public string tcNum { get; set; }
        public string tcMD5 { get; set; }
        public string tcMes { get; set; }
        public string tcAnio { get; set; }
        public string tcDigitos { get; set; }
        public string tcNIP { get; set; }
        public bool tcAsignada { get; set; }
        public bool tcActiva { get; set; }
        public decimal limActivacion { get; set; }
        public decimal limDepMin { get; set; }
        public decimal limDepMax { get; set; }
        public decimal limDepDia { get; set; }
        public decimal limDepSem { get; set; }
        public decimal limDepMes { get; set; }
        public decimal limRetMin { get; set; }
        public decimal limRetMax { get; set; }
        public decimal limRetDia { get; set; }
        public decimal limRetSem { get; set; }
        public decimal limRetMes { get; set; }
        public decimal limTransMin { get; set; }
        public decimal limTransMax { get; set; }
        public decimal limTransDia { get; set; }
        public decimal limTransSem { get; set; }
        public decimal limTransMes { get; set; }
        public string titulo { get; set; }
        public string datNombre { get; set; }
        public string datPaterno { get; set; }
        public string datMaterno { get; set; }
        public int genero { get; set; }
        public DateTime datNacimiento { get; set; }
        public string datDir1 { get; set; }
        public string datDir2 { get; set; }
        public string datColonia { get; set; }
        public int paisID { get; set; }
        public virtual pais paises { get; set; }
        public int edoID { get; set; }
        public virtual estado estados { get; set; }
        public int cdID { get; set; }
        public virtual ciudad ciudades { get; set; }
        public string datCP { get; set; }
        public string datTelefono { get; set; }
        public string datFax { get; set; }
        public string datCelular { get; set; }
        public string datNextel { get; set; }
        public string datBlackPin { get; set; }
        public int? identificacionID { get; set; }
        public string identificacionInfo { get; set; }
        public Byte[] identificacionFile { get; set; }
        public bool identificacionValida { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public DateTime fechaAlta { get; set; }
        public DateTime fechaBaja { get; set; }
        public decimal creditoDisponible { get; set; }
        public decimal creditoPorPagar { get; set; }
    }
}