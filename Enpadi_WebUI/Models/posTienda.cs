﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enpadi_WebUI.Models
{
    [Table("posTiendas")]
    public class posTienda
    {
        [Key]
        public int tiendaID { get; set; }

        [Display(Name="Socio")]
        public int socioID { get; set; }
        public virtual socio socio { get; set; }

        [Display(Name="Tienda")]
        public string nombre { get; set; }

        [Display(Name="Descripción")]
        public string descripcion { get; set; }

        [Display(Name="Imagen")]
        public string imagen { get; set; }
    }
}