﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{

    public class LogModel
    {
        #region keys

        /*KEYS FOR LOG ENTRIES*/
        public static Int32 idLogTypeNew = 1;
        public static Int32 idLogTypeDelete = 2;
        public static Int32 idLogTypeModification = 3;
        public static Int32 idLogTypeLogIn = 4;
        public static Int32 idLogTypeError = 5;

        #endregion keys

        #region constructors

        public LogModel() { }

        public LogModel(
                Int32 id,
                Int32 idLogType,
                Int32 totalRows,
                String module,
                String description,
                String userId,
                String ipAddress,
                DateTime dateCreated,
                Boolean modified
            )
        {

            _id = id;
            _idLogType = idLogType;
            _totalRows = totalRows;
            _module = module;
            _description = description;
            _userId = userId;
            _ipAddress = ipAddress;
            _dateCreated = dateCreated;
            _modified = modified;
        }

        public LogModel(
                Int32 idLogType,
                String module,
                String description,
                String userId,
                String ipAddress,
                DateTime dateCreated
            )
        {
            _idLogType = idLogType;
            _module = module;
            _description = description;
            _userId = userId;
            _ipAddress = ipAddress;
            _dateCreated = dateCreated;
        }

        #endregion constructors

        #region members

        private int _id = 0;
        private int _idLogType = 0;
        private Int32 _totalRows = 0;
        private String _module = String.Empty;
        private String _description = String.Empty;
        private String _userId = String.Empty;
        private String _ipAddress = String.Empty;
        private DateTime _dateCreated = DateTime.MinValue;
        private Boolean _modified = false;

        #region standarMembers

        #endregion standarMembers

        #region referenceMembers

        private String _logType = String.Empty;

        #endregion referenceMembers

        #endregion members

        #region methods

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int IdLogType
        {
            get { return _idLogType; }
            set { _idLogType = value; }
        }

        public Int32 TotalRows
        {
            get { return _totalRows; }
            set { _totalRows = value; }
        }

        public String Module
        {
            get { return _module; }
            set { _module = value; }
        }

        public String Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public String userId
        {
            get { return _userId; }
            set { _userId = value; }
        }

        public String IpAddress
        {
            get { return _ipAddress; }
            set { _ipAddress = value; }
        }

        public DateTime DateCreated
        {
            get { return _dateCreated; }
            set { _dateCreated = value; }
        }

        public Boolean Modified
        {
            get { return _modified; }
            set { _modified = value; }
        }

        public String LogType
        {
            get { return _logType; }
            set { _logType = value; }
        }

        #endregion methods
    }
}