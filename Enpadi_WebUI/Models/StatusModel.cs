﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    [Table("Status")]
    public class StatusModel
    {
        [NotMapped]
        public static class Keys {

            public static readonly Guid Rejected = Guid.Parse("C0074C00-F5E6-44E9-94FF-387C752347C5");
            public static readonly Guid Pending = Guid.Parse("C104D47E-2B09-4F05-89AD-BB10F7A1AF7C");
            public static readonly Guid Canceled = Guid.Parse("735CD8F2-C362-4198-A27F-D6F205C9E112");
            public static readonly Guid Acepted = Guid.Parse("292685D0-4D0D-4706-A01F-ED64E49DAEE9");
        }

        [Key]
        public Guid id { get; set; }

        public string name { get; set; }

        public string description { get; set; }
    }
}