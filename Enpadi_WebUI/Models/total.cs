﻿using System.ComponentModel.DataAnnotations;

namespace Enpadi_WebUI.Models
{
    public class total
    {
        [DisplayFormat(DataFormatString = "{0:c}")]
        public decimal Total { get; set; }
    }
}