﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enpadi_WebUI.Models
{
    [Table("PredialMovimientos")]
    public class predialMovimiento
    {
        [Key]
        public int movID { get; set; }

        [Display(Name="Predial")]
        public int predialID { get; set; }

        [Display(Name = "Transacción")]
        public string transaccion { get; set; }

        [Display(Name = "Concepto")]
        public int conceptoID { get; set; }
        public virtual predialConcepto predialConcepto { get; set; }

        [Display(Name = "Descripción")]
        public string descripcion { get; set; }

        [Display(Name = "Referencia")]
        public string referencia { get; set; }

        [Display(Name = "Abono")]
        [DisplayFormat(DataFormatString = "{0:c}")]
        public decimal abono1 { get; set; }

        [Display(Name = "Iva")]
        [DisplayFormat(DataFormatString = "{0:c}")]
        public decimal abono1iva { get; set; }

        [Display(Name = "Cargo")]
        [DisplayFormat(DataFormatString = "{0:c}")]
        public decimal cargo1 { get; set; }

        [Display(Name = "Iva")]
        [DisplayFormat(DataFormatString = "{0:c}")]
        public decimal cargo1iva { get; set; }

        [Display(Name = "Abono")]
        [DisplayFormat(DataFormatString = "{0:c}")]
        public decimal abono2 { get; set; }

        [Display(Name = "Iva")]
        [DisplayFormat(DataFormatString = "{0:c}")]
        public decimal abono2iva { get; set; }

        [Display(Name = "Cargo")]
        [DisplayFormat(DataFormatString = "{0:c}")]
        public decimal cargo2 { get; set; }

        [Display(Name = "Iva")]
        [DisplayFormat(DataFormatString = "{0:c}")]
        public decimal cargo2iva { get; set; }

        [Display(Name = "Abono")]
        [DisplayFormat(DataFormatString = "{0:c}")]
        public decimal abono3 { get; set; }

        [Display(Name = "Iva")]
        [DisplayFormat(DataFormatString = "{0:c}")]
        public decimal abono3iva { get; set; }

        [Display(Name = "Cargo")]
        [DisplayFormat(DataFormatString = "{0:c}")]
        public decimal cargo3 { get; set; }

        [Display(Name = "Iva")]
        [DisplayFormat(DataFormatString = "{0:c}")]
        public decimal cargo3iva { get; set; }

        [Display(Name = "Abono Total")]
        [DisplayFormat(DataFormatString = "{0:c}")]
        public decimal abonoTotal { get; set; }

        [Display(Name = "Cargo Total")]
        public decimal cargoTotal { get; set; }

        [Display(Name = "Estatus")]
        public int estatusID { get; set; }

        [Display(Name = "Aprobación")]
        public string aprobacion { get; set; }

        [Display(Name = "Usuario")]
        public int userID { get; set; }

        [Display(Name = "IP")]
        public string ip { get; set; }

        [Display(Name = "Fecha")]
        public DateTime fechaRegistro { get; set; }

    }
}