﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enpadi_WebUI.DAO;

namespace Enpadi_WebUI.Models
{
    public class OpenPayCardModel : OpenPayCard
    {

        #region members

        public int id { get; set; }

        public string CardID { get; set; }

        public int ExternalID { get; set; }

        public string HolderName { get; set; }

        public string CardNumber { get; set; }

        public string CVV2 { get; set; }

        public string ExpirationMonth { get; set; }

        public string ExpirationYear { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Country { get; set; }

        public string PostalCode { get; set; }

        public bool AllowCharges { get; set; }

        public bool AllowPayouts { get; set; }

        public string Brand { get; set; }

        public string Type { get; set; }

        public string BankName { get; set; }

        public string BankCode { get; set; }

        public string CustomerId { get; set; }

        public bool PointsCard { get; set; }

        public DateTime CreationDate { get; set; }

        #endregion members
    }
}