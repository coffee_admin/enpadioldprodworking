﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    public class CemexPayments
    {
        public CemexPayments()
        {
            Id = Guid.NewGuid();
        }

        [Key]
        public Guid Id { get; set; }

        public String beneficiary_name { get; set; }
        public string beneficiary_last { get; set; }
        public string beneficiary_mothers { get; set; }
        public String beneficiary_phone_number { get; set; }
        public String beneficiary_cell_phone_number { get; set; }
        public String beneficiary_email_address { get; set; }

        public String buyer_name { get; set; }
        public String buyer_phone_number { get; set; }
        public String buyer_email { get; set; }
        public String buyer_enpadi_card { get; set; }
        public String buyer_enpadi_card_ending { get; set; }

        public String code_base { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int code_number { get; set; }

        public String remitter_name { get; set; }
        public Decimal amout_pay { get; set; }
        public DateTime? payment_date_at { get; set; }

        public Guid? transaccionesId { get; set; }
    }
}