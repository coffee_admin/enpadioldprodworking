﻿using System.Collections.Generic;

namespace Enpadi_WebUI.Models
{
    public class ViewModelInicio
    {
        #region properties

        private List<resumen> _visaMC = new List<resumen>();
        private List<resumen> _amex = new List<resumen>();
        private List<resumen> _banco = new List<resumen>();
        private List<resumen> _tienda = new List<resumen>();
        private List<resumen> _tAE = new List<resumen>();
        private List<resumen> _servicios = new List<resumen>();
        private List<resumen> _transferencias = new List<resumen>();
        private List<resumen> _ingresos = new List<resumen>();
        private List<resumen> _saldos = new List<resumen>();
        private List<resumen> _telcel = new List<resumen>();
        private List<resumen> _movistar = new List<resumen>();
        private List<resumen> _iusacell = new List<resumen>();
        private List<resumen> _unefon = new List<resumen>();
        private List<resumen> _nextel = new List<resumen>();
        private List<resumen> _virgin = new List<resumen>();
        private List<resumen> _serviciosMonto = new List<resumen>();
        private List<resumen> _taeMonto = new List<resumen>();
        private List<resumen> _remesas = new List<resumen>();

        private List<resumenUnitario> _taeTransacciones = new List<resumenUnitario>();
        private List<resumenUnitario> _serviciosTransacciones = new List<resumenUnitario>();
        private List<resumenUnitario> _ingresoTransacciones = new List<resumenUnitario>();
        private List<resumenUnitario> _egresoTransacciones = new List<resumenUnitario>();

        private string _perfil = string.Empty;

        private int _periodo = 0;
        private int _usuariosActivos = 0;
        private int _usuariosInactivos = 0;
        private int _usuariosTotales = 0;

        private decimal _montoIngresado = 0.0m;
        private decimal _montoEgresado = 0.0m;
        private decimal _saldoActual = 0.0m;
        private decimal _mesRemesas = 0.0m;
        private decimal _mesTransferencias = 0.0m;

        #endregion properties

        #region constructors

        public ViewModelInicio()
        {
            _visaMC = new List<resumen>();
            _amex = new List<resumen>();
            _banco = new List<resumen>();
            _tienda = new List<resumen>();
            _tAE = new List<resumen>();
            _servicios = new List<resumen>();
            _transferencias = new List<resumen>();
            _ingresos = new List<resumen>();
            _saldos = new List<resumen>();
            _telcel = new List<resumen>();
            _movistar = new List<resumen>();
            _iusacell = new List<resumen>();
            _unefon = new List<resumen>();
            _nextel = new List<resumen>();
            _virgin = new List<resumen>();
            _serviciosMonto = new List<resumen>();
            _taeMonto = new List<resumen>();
            _remesas = new List<resumen>();
            _taeTransacciones = new List<resumenUnitario>();
            _serviciosTransacciones = new List<resumenUnitario>();
            _ingresoTransacciones = new List<resumenUnitario>();
            _egresoTransacciones = new List<resumenUnitario>();
            _perfil = string.Empty;
            _periodo = 0;
            _usuariosActivos = 0;
            _usuariosInactivos = 0;
            _usuariosTotales = 0;
            _montoIngresado = 0.0m;
            _montoEgresado = 0.0m;
            _saldoActual = 0.0m;
            _mesRemesas = 0.0m;
            _mesTransferencias = 0.0m;
        }

        public ViewModelInicio(List<resumen> visaMC, List<resumen> amex, List<resumen> banco, List<resumen> tienda, List<resumen> tAE, List<resumen> servicios, List<resumen> transferencias, List<resumen> ingresos, List<resumen> saldos, List<resumen> telcel, List<resumen> movistar, List<resumen> iusacell, List<resumen> unefon, List<resumen> nextel, List<resumen> virgin, List<resumen> serviciosMonto, List<resumen> taeMonto, List<resumen> remesas, List<resumenUnitario> taeTransacciones, List<resumenUnitario> serviciosTransacciones, List<resumenUnitario> ingresoTransacciones, List<resumenUnitario> egresoTransacciones, string perfil, int periodo, int usuariosActivos, int usuariosInactivos, int usuariosTotales, decimal montoIngresado, decimal montoEgresado, decimal saldoActual, decimal mesRemesas, decimal mesTransferencias)
        {
            _visaMC = visaMC;
            _amex = amex;
            _banco = banco;
            _tienda = tienda;
            _tAE = tAE;
            _servicios = servicios;
            _transferencias = transferencias;
            _ingresos = ingresos;
            _saldos = saldos;
            _telcel = telcel;
            _movistar = movistar;
            _iusacell = iusacell;
            _unefon = unefon;
            _nextel = nextel;
            _virgin = virgin;
            _serviciosMonto = serviciosMonto;
            _taeMonto = taeMonto;
            _remesas = remesas;
            _taeTransacciones = taeTransacciones;
            _serviciosTransacciones = serviciosTransacciones;
            _ingresoTransacciones = ingresoTransacciones;
            _egresoTransacciones = egresoTransacciones;
            _perfil = perfil;
            _periodo = periodo;
            _usuariosActivos = usuariosActivos;
            _usuariosInactivos = usuariosInactivos;
            _usuariosTotales = usuariosTotales;
            _montoIngresado = montoIngresado;
            _montoEgresado = montoEgresado;
            _saldoActual = saldoActual;
            _mesRemesas = mesRemesas;
            _mesTransferencias = mesTransferencias;
        }

        #endregion constructors

        public List<resumen> VisaMC { get => _visaMC; set => _visaMC = value; }
        public List<resumen> Amex { get => _amex; set => _amex = value; }
        public List<resumen> Banco { get => _banco; set => _banco = value; }
        public List<resumen> Tienda { get => _tienda; set => _tienda = value; }
        public List<resumen> TAE { get => _tAE; set => _tAE = value; }
        public List<resumen> Servicios { get => _servicios; set => _servicios = value; }
        public List<resumen> Transferencias { get => _transferencias; set => _transferencias = value; }
        public List<resumen> Ingresos { get => _ingresos; set => _ingresos = value; }
        public List<resumen> Saldos { get => _saldos; set => _saldos = value; }
        public List<resumen> Telcel { get => _telcel; set => _telcel = value; }
        public List<resumen> Movistar { get => _movistar; set => _movistar = value; }
        public List<resumen> Iusacell { get => _iusacell; set => _iusacell = value; }
        public List<resumen> Unefon { get => _unefon; set => _unefon = value; }
        public List<resumen> Nextel { get => _nextel; set => _nextel = value; }
        public List<resumen> Virgin { get => _virgin; set => _virgin = value; }
        public List<resumen> ServiciosMonto { get => _serviciosMonto; set => _serviciosMonto = value; }
        public List<resumen> TaeMonto { get => _taeMonto; set => _taeMonto = value; }
        public List<resumen> Remesas { get => _remesas; set => _remesas = value; }
        public List<resumenUnitario> TaeTransacciones { get => _taeTransacciones; set => _taeTransacciones = value; }
        public List<resumenUnitario> ServiciosTransacciones { get => _serviciosTransacciones; set => _serviciosTransacciones = value; }
        public List<resumenUnitario> IngresoTransacciones { get => _ingresoTransacciones; set => _ingresoTransacciones = value; }
        public List<resumenUnitario> EgresoTransacciones { get => _egresoTransacciones; set => _egresoTransacciones = value; }
        public string perfil { get => _perfil; set => _perfil = value; }
        public int periodo { get => _periodo; set => _periodo = value; }
        public int usuariosActivos { get => _usuariosActivos; set => _usuariosActivos = value; }
        public int usuariosInactivos { get => _usuariosInactivos; set => _usuariosInactivos = value; }
        public int usuariosTotales { get => _usuariosTotales; set => _usuariosTotales = value; }
        public decimal montoIngresado { get => _montoIngresado; set => _montoIngresado = value; }
        public decimal montoEgresado { get => _montoEgresado; set => _montoEgresado = value; }
        public decimal saldoActual { get => _saldoActual; set => _saldoActual = value; }
        public decimal mesRemesas { get => _mesRemesas; set => _mesRemesas = value; }
        public decimal mesTransferencias { get => _mesTransferencias; set => _mesTransferencias = value; }




        //public string perfil { get; set; }
        //public int usuariosActivos { get; set; }
        //public int usuariosInactivos { get; set; }
        //public int usuariosTotales { get; set; }
        //public decimal montoIngresado { get; set; }
        //public decimal montoEgresado { get; set; }
        //public decimal saldoActual { get; set; }
        //public decimal mesRemesas { get; set; }
        //public decimal mesTransferencias { get; set; }

        //public List<resumen> VisaMC { get; set; }
        //public List<resumen> Amex { get; set; }
        //public List<resumen> Banco { get; set; }
        //public List<resumen> Tienda { get; set; }
        //public List<resumen> TAE { get; set; }
        //public List<resumen> Servicios { get; set; }
        //public List<resumen> Transferencias { get; set; }

        //public List<resumen> Ingresos { get; set; }
        //public List<resumen> Saldos { get; set; }
        //public List<resumenUnitario> IngresoTransacciones { get; set; }
        //public List<resumenUnitario> EgresoTransacciones { get; set; }
        //public List<resumen> Telcel { get; set; }
        //public List<resumen> Movistar { get; set; }
        //public List<resumen> Iusacell { get; set; }
        //public List<resumen> Unefon { get; set; }
        //public List<resumen> Nextel { get; set; }
        //public List<resumen> Virgin { get; set; }
        //public List<resumen> ServiciosMonto { get; set; }
        //public List<resumenUnitario> ServiciosTransacciones { get; set; }
        //public List<resumen> TaeMonto { get; set; }
        //public List<resumenUnitario> TaeTransacciones { get; set; }
        //public List<resumen> Remesas { get; set; }
        //public int periodo { get; set; }

    }
}