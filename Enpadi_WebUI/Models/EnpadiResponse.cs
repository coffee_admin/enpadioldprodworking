﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    [Table("EnpadiResponse")]
    public class EnpadiResponse
    {
        public EnpadiResponse() {
            Id = Guid.NewGuid();
        }

        [Key]
        public Guid Id { get; set; }

        public Int32 ObjIntId { get; set; }

        public Guid ObjId { get; set; }

        public String Name { get; set; }

        public String Value { get; set; }

        public Int32 Order { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}