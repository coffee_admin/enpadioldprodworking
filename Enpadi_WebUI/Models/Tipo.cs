﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    [Table("Tipo")]
    public class Tipo
    {
        [NotMapped]
        public static class Keys
        {
            [NotMapped]
            public const Int32 Deposit = 1;
            [NotMapped]
            public const Int32 Payment = 2;
            [NotMapped]
            public const Int32 Transfer = 3;
            [NotMapped]
            public const Int32 Return = 4;
            [NotMapped]
            public const Int32 Contracargo = 5;
            [NotMapped]
            public const Int32 Remesa = 6;
            [NotMapped]
            public const Int32 Recompensa = 7;

        }

        [Key]
        public Int32 tipoID { get; set; }
        public String descripcion { get; set; }
        public DateTime? fechaAlta { get; set; }
        public DateTime? fechaBaja { get; set; }
    }
}