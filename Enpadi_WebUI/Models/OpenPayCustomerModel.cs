﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enpadi_WebUI.DAO;

namespace Enpadi_WebUI.Models
{
    public class OpenPayCustomerModel : OpenPayCustomer
    {

        #region members 

        public int id { get; set; }

        public string CustomerID { get; set; }

        public int ExternalID { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public bool RequiresAccount { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string CountryCode { get; set; }

        public string PostalCode { get; set; }

        public string CLABE { get; set; }

        public DateTime DateAdd { get; set; }

        public DateTime DateDelete { get; set; }

        //internal static void Save(object opCustomer)
        //{
        //    throw new NotImplementedException();
        //}

        #endregion membres

    }
}