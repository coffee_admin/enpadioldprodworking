﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    [Table("ServiceType")]
    public class ServiceType
    {
        [NotMapped]
        public class Keys {

            public static Guid Recharge = Guid.Parse("CD9FCEEF-5E22-48DF-B837-04E6500E37A6");
            public static Guid Pay = Guid.Parse("50C65557-F724-44EB-BC13-920F09866308");
            public static Guid ExternalPayment = Guid.Parse("8F181577-3585-477F-81D9-DBFB2B554EE2");
        }

        [Key]
        public Guid Id { get; set; }
        public int SKU { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
    }
}