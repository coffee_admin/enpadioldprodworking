﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    public class ViewModelProfile
    {
        public String Name { get; set; }
        public String Email { get; set; }
        public String PhoneNumber { get; set; }
        public String currentUserId { get; set; }
        public String ProfilePicUrl { get; set; }
    }
}