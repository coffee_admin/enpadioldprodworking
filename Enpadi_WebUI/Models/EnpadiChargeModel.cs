﻿using Enpadi_WebUI.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    public class EnpadiChargeModel : EnpadiCharge
    {
        #region constructors

        public EnpadiChargeModel()
        {
            this.data_object = new data();
        }

        #endregion constructors

        #region keys

        [NotMapped]
        public class CreditCardType
        {
            public const Int32 NoPartner = 0;
            public const Int32 Visa = 1;
            public const Int32 MasterCard = 1;
            public const Int32 AMEX = 2;
            public const Int32 MaestroCard = 9;
            public const Int32 CashDiestel = 4;
            public const Int32 CashOxxo = 9;
            public const Int32 Banco = 3;

            public const Int32 TiempoAire = 5;
            public const Int32 PagoDeServicios = 6;
            public const Int32 Santiago = 7;
            public const Int32 CloudTransfer = 8;
        }

        [NotMapped]
        public class Partners {
            public const String Visa = "visa";
            public const String MasterCard = "mastercard";
            public const String Amex = "amex";
            public const String MaestroCard = "maestro";
            public const String CashDiestel = "tienda";
            public const String CashOxxo = "oxxopay";
            public const String Banco = "banco";

            public const String TiempoAire = "tiempoaire";
            public const String PagoDeServicios = "pagodeservicios";
            public const String Santiago = "santiago";
            public const String CloudTransfer = "cloudtransfer";
        }

        [NotMapped]
        public class ChargeEstatus
        {
            public const String charge_created = "charge.created";
            public const String charge_paid = "charge.paid";
            public const String charge_cancel = "charge.cancel";
            public const String charge_pending = "charge.pending";
        }

        [NotMapped]
        public class PaymentMethodType
        {
            public const String cash = "cash";
            public const String credit_card = "credit.card";

        }
        
        #endregion keys

        #region properties

        public data data_object { get; set; }

        #endregion properties

        #region local classes

        /// <summary>
        /// This class if for the data object
        /// </summary>
        public class PaymentMethod
        {

            public String service_name { get; set; }
            public String object_description { get; set; }
            public String payment_method_type { get; set; }
            public String expires_at { get; set; }
            public String reference { get; set; }
            public String deviceSessionId { get; set; }
            public String source_id { get; set; }

            #region credit card, this is not store in database

            public String name_on_card { get; set; }
            public Int32 credit_card_type { get; set; }
            public String card_number { get; set; }
            public Int32 credit_card_expiration_month { get; set; }
            public Int32 credit_card_expiration_year { get; set; }
            public Int32 credit_card_security_code { get; set; }
            public Int32 credit_card_maestro_switch { get; set; }

            #endregion credit card, this is not store in database

        }

        #region data class for event

        /// <summary>
        /// this class is built to be inserted in a Enpadi Event
        /// </summary>
        public class data
        {

            public data()
            {
                id = Guid.NewGuid();
                payment_method = new PaymentMethod();
            }

            [Key]
            public Guid id { get; set; }
            public Boolean livemode { get; set; }
            public Decimal amount { get; set; }
            public String currency { get; set; }
            public PaymentMethod payment_method { get; set; }
            public String object_description { get; set; }
            public Decimal fee { get; set; }
            public String customer_id { get; set; }
            public Guid order_id { get; set; }
            public String customer_email { get; set; }
            public String charge_type { get; set; }

        }

        #endregion data class for event

        #endregion local classes

        #region methods
        /// <summary>
        /// Gets a total fee for a charge
        /// </summary>
        /// <param name="partner">Partner name</param>
        /// <param name="chargeAmount">Total charge amount</param>
        /// <returns></returns>
        public static Decimal GetCharge(String partner, Decimal chargeAmount)
        {
            //its 4 to not lose any money if something goes wrong
            Decimal result = 4m;

            switch (partner.Trim().ToLower())
            {
                case Partners.Visa:
                    result = (chargeAmount * 0.029m)+2.5m;
                    break;
                case Partners.MasterCard:
                    result = (chargeAmount * 0.029m) + 2.5m;
                    break;
                case Partners.Amex:
                    result = (chargeAmount * 0.049m) + 2.5m;
                    break;
                case Partners.Banco:
                    result = (chargeAmount * 0.0m) + 8m;
                    break;
                case Partners.MaestroCard:
                    result = (chargeAmount * 0.5m) + 8m; ;
                    break;
                case Partners.CashDiestel:
                    result = (chargeAmount * 0.029m) + 2.5m;
                    break;
                case Partners.CashOxxo:
                    result = (chargeAmount * 0.035m) + 0m; ;
                    break;
                default:
                    result = 100m;
                    break;
            }

            //comission on trans look like this
            //t.cargo2 = t.monto * (decimal)(2.9 / 100);
            //t.cargo2iva = t.cargo2 * dIva;
            //t.cargo5 = (decimal)2.5;
            //t.cargo5iva = t.cargo5 * dIva;

            return result;
        }

        /// <summary>
        /// Gets a partner id from a partner string visa, amex, mastercard, banco, maestrocard, cashdiestel, cashoxxo
        /// </summary>
        /// <param name="partner">Partner name</param>
        /// <returns></returns>
        public static Int32 GetPartnerId(String partner)
        {
            Int32 result = 0;

            switch (partner.Trim().ToLower())
            {
                case Partners.Visa:
                    result = CreditCardType.Visa;
                    break;
                case Partners.MasterCard:
                    result = CreditCardType.MasterCard;
                    break;
                case Partners.Amex:
                    result = CreditCardType.AMEX;
                    break;
                case Partners.Banco:
                    result = CreditCardType.Banco;
                    break;
                case Partners.MaestroCard:
                    result = CreditCardType.MaestroCard;
                    break;
                case Partners.CashDiestel:
                    result = CreditCardType.CashDiestel;
                    break;
                case Partners.CashOxxo:
                    result = CreditCardType.CashOxxo;
                    break;
                default:
                    result = CreditCardType.NoPartner;
                    break;
            }


            return result;
        }

        #endregion methods

    }
}