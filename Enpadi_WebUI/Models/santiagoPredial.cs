﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enpadi_WebUI.Models
{
    [Table("SantiagoPredial")]
    public class santiagoPredial
    {
        [Key]
        [Display(Name="Expediente Catastral")]
        public string expediente { get; set; }

        [Display(Name="Propietario")]
        public string propietario { get; set; }

        [Display(Name="Ubicación del Predio")]
        public string ubicacionPredio { get; set; }

        [NotMapped]
        [ScaffoldColumn(false)]
        [Display(Name="Referencia")]
        public string referenciaPredio { get; set; }

        [Display(Name="Rezago")]
        public decimal rezago { get; set; }

        [Display(Name="Actual")]
        public decimal actual { get; set; }

        [Display(Name="Recargo Rezago")]
        public decimal recargoRezago { get; set; }

        [Display(Name="Descuento")]
        public decimal descuento { get; set; }

        [Display(Name="Recargo Actual")]
        public decimal recargoActual { get; set; }

        [Display(Name="Total")]
        public decimal total { get; set; }

        [Display(Name="Saldo Predial Actual")]
        [DisplayFormat(DataFormatString="{0:C2}")]
        [DataType(DataType.Currency)]
        public decimal saldo { get; set; }

        [Display(Name="Fecha de Inicio")]
        public DateTime fechaInicio { get; set; }

        [Display(Name="Fecha de Vencimiento")]
        public DateTime fechaVencimiento { get; set; }

        [Display(Name="Codigo de Barras")]
        public string codigoDeBarras { get; set; }

        [Display(Name="Referencia")]
        public string referenciaBancomer { get; set; }

        [Display(Name="Numero de Registro")]
        public int numReg { get; set; }

        [NotMapped]
        [ScaffoldColumn(false)]
        public string Transaccion { get; set; }

        [NotMapped]
        [ScaffoldColumn(false)]
        public int estatus { get; set; }

        [NotMapped]
        [ScaffoldColumn(false)]
        public int codigoRespuesta { get; set; }

        [NotMapped]
        [ScaffoldColumn(false)]
        public string codigoRespuestaDescr { get; set; }

        [NotMapped]
        [ScaffoldColumn(false)]
        public string email { get; set; }
    }
}