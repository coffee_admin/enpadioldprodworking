﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enpadi_WebUI.Models
{
    [Table("SantiagoPredialBancos")]
    public class santiagoPredialBancos
    {
        [Key]
        public int idRef { get; set; }

        [Display(Name="Expediente")]
        public string expediente { get; set; }

        [Display(Name="Propietario")]
        public string propietario { get; set; }

        [Display(Name="Ubicación Predio")]
        public string ubicacionPredio { get; set; }

        [Display(Name="Saldo")]
        [DisplayFormat(DataFormatString="{0:C2}")]
        public decimal saldo { get; set; }

        [Display(Name="Nombre")]
        public string nombre { get; set; }

        [Display(Name="A.Paterno")]
        public string paterno { get; set; }

        [Display(Name="A.Materno")]
        public string materno { get; set; }

        [Display(Name="Fecha Impresión")]
        public DateTime fechaAlta { get; set; }
    }
}