﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Configuration;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace Enpadi_WebUI.Models
{

    #region const

    /// <summary>
    /// Const for the roles hardcoded in db
    /// </summary>
    public static class SecurityRoles
    {
        public const String Usuario = "Usuario";
        public const String Concentradora = "Concentradora";
        public const String Administrador = "Administrador";
        public const String Socio = "Socio";
        public const String Remesas = "Remesas";
        public const String Recompensas = "Recompensas";
    }

    /// <summary>
    /// Const for confirm email and sms
    /// </summary>
    public static class UserValidation
    {
        public const String Confirm_email = "Confirm_email";
        public const String Confirm_sms = "Confirm_sms";
    }

    #endregion const

    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {

        public static class Key
        {
            public static readonly Guid AdminUserId = Guid.Parse("A5F83EF1-4CFE-4111-BF9A-F00F492C97E3");

            public static readonly Guid ConsulateId = Guid.Parse("e1f817a5-21c0-4501-af91-3cb0289efdc6");
        }

        //extra columns on aspnte user
        public string Name { get; set; }
        public string ProfilePicUrl { get; set; }
        public string UserTypeId { get; set; }
        public string PrivateKey { get; set; }
        public String PublicKey { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public static OperationObject CreateEncryptionKeysForUser(String userEmail) {

            OperationObject response = new OperationObject();
            ApplicationDbContext db = new ApplicationDbContext();
            ApplicationUser userContext = new ApplicationUser();

            var contextDB = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
            var storeDB = new UserStore<IdentityUser>(contextDB);
            var managerDB = new UserManager<IdentityUser>(storeDB);
            var user = managerDB.FindByEmail(userEmail);


            if (user != null)
            {
                response.Success = false;
                response.Property.Add("error", "User not found");
                response.Property.Add("error1", "User not found");
                response.Property.Add("error2", "User not found");
            }
            else {
                response.Success = false;
                response.Property.Add("error","User not found");
                response.Property.Add("error1", "User not found");
                response.Property.Add("error2", "User not found");
            }

            EncryptionModel encrypModel = new EncryptionModel();
            encrypModel.GenerateKeys();

            return response;

        }
    }


    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        private static bool bModoProduccion = Convert.ToBoolean(ConfigurationManager.AppSettings["ModoProduccion"]);
        private static string sNameConnection = (bModoProduccion) ? "Enpadi_DB_Connection" : "Test_DB_Connection";

        public ApplicationDbContext()
            : base(sNameConnection, throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Tipo> Tipo { get; set; }
        public DbSet<producto> productos { get; set; }
        public DbSet<pais> paises { get; set; }
        public DbSet<estado> estados { get; set; }
        public DbSet<ciudad> ciudades { get; set; }
        public DbSet<moneda> monedas { get; set; }
        public DbSet<tarjetas> tarjetas { get; set; }
        public DbSet<socio> socios { get; set; }
        public DbSet<remesa> remesas { get; set; }
        public DbSet<recompensa> recompensas { get; set; }
        public DbSet<origen> origenes { get; set; }
        public DbSet<trans> transacciones { get; set; }
        public DbSet<servicios> servicios { get; set; }
        public DbSet<pagoServicio> pagoServicios { get; set; }
        public DbSet<tarjetasContactos> tarjetasContactos { get; set; }
        public DbSet<santiagoPredial> santiagoPredials { get; set; }
        public DbSet<santiagoPredialConsulta> santiagoPredialConsultas { get; set; }
        public DbSet<logTC> logTCs { get; set; }
        public DbSet<openpayCustomer> openpayCustomers { get; set; }
        public DbSet<openpayCard> openpayCards { get; set; }

        public DbSet<pagoBanco> pagoBancoes { get; set; }
        public DbSet<posTienda> posTiendas { get; set; }
        public DbSet<posCategoria> posCategorias { get; set; }
        public DbSet<posProducto> posProductoes { get; set; }
        public DbSet<posUnidad> posUnidads { get; set; }
        public DbSet<predial> predials { get; set; }
        public DbSet<predialConcepto> predialConceptoes { get; set; }
        public DbSet<predialMovimiento> predialMovimientoes { get; set; }
        public DbSet<santiagoPredialBancos> santiagoPredialBancos { get; set; }
        public DbSet<santiagoPredialTarjetas> santiagoPredialTarjetas { get; set; }

        public DbSet<EnpadiOrderModel> EnpadiOrderModel { get; set; }
        public DbSet<EnpadiOrder> EnpadiOrder { get; set; }

        public DbSet<Estatus> Estatus { get; set; }

        //public DbSet<StatusModel> Status { get; set; }
        public DbSet<UserTypeModel> UserType { get; set; }
        public DbSet<TransactionTypeModel> TransactionType { get; set; }
        public DbSet<PlatformModel> Platform { get; set; }
        //public DbSet<Transaction> Transaction { get; set; }

        //public DbSet<ChargeOption> ChargeOption { get; set; }
        public DbSet<Fee> Fee { get; set; }
        public DbSet<FeeService> FeeService { get; set; }
        //public DbSet<ServiceChargeOption> ServiceChargeOption { get; set; }
        public DbSet<TransactionFee> TransactionFee { get; set; }

        //public DbSet<Service> Service { get; set; }

        public DbSet<ServiceCategory> ServiceCategory { get; set; }
        public DbSet<ServiceType> ServiceType { get; set; }

        public DbSet<ImssPaymentsModel> ImssPaymentsModel { get; set; }

        public DbSet<ServiciosFeePlatfom> ServiciosFeePlatfom { get; set; }

        public DbSet<EnpadiPetition> EnpadiPetition { get; set; }

        public DbSet<EnpadiResponse> EnpadiResponse { get; set; }

        public DbSet<CemexPayments> CemexPayments { get; set; }

        public DbSet<EnpadiChargeEF> EnpadiCharge { get; set; }

        public DbSet<EnpadiLineItemModel> EnpadiLineItem { get; set; }
    }
}