﻿using Enpadi_WebUI.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    [Table("Transacciones")]
    public class trans : Transaccion
    {
        #region constructor

        public trans()
        {
            Id = Guid.NewGuid();
        }

        #endregion constructor

        #region keys
        [NotMapped]
        public static class TransOrigin
        {
            [NotMapped]
            public const Int32 External = 1;
            [NotMapped]
            public const Int32 Enpadi = 2;

        }

        [NotMapped]
        public static class TransType
        {
            [NotMapped]
            public const Int32 Deposit = 1;
            [NotMapped]
            public const Int32 Payment = 2;

        }

        #endregion keys

        #region variables

        private String _concepto;

        #endregion variables

        [Key]
        public Guid Id { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 transID { get; set; }

        public int origenID { get; set; }
        public int tipoID { get; set; }
        public int modoID { get; set; }
        public int socioID { get; set; }
        public int terminalID { get; set; }
        public int monedaID { get; set; }
        public int tarjetaID { get; set; }
        public string tarjeta { get; set; }
        public decimal monto { get; set; }
        public decimal abono1 { get; set; }
        public decimal abono1iva { get; set; }
        public decimal cargo1 { get; set; }
        public decimal cargo1iva { get; set; }
        public decimal abono2 { get; set; }
        public decimal abono2iva { get; set; }
        public decimal cargo2 { get; set; }
        public decimal cargo2iva { get; set; }
        public decimal abono3 { get; set; }
        public decimal abono3iva { get; set; }
        public decimal cargo3 { get; set; }
        public decimal cargo3iva { get; set; }
        public decimal abono4 { get; set; }
        public decimal abono4iva { get; set; }
        public decimal cargo4 { get; set; }
        public decimal cargo4iva { get; set; }
        public decimal abono5 { get; set; }
        public decimal abono5iva { get; set; }
        public decimal cargo5 { get; set; }
        public decimal cargo5iva { get; set; }
        public decimal abono6 { get; set; }
        public decimal abono6iva { get; set; }
        public decimal cargo6 { get; set; }
        public decimal cargo6iva { get; set; }
        public decimal abonoTotal { get; set; }
        public decimal cargoTotal { get; set; }
        public decimal facturable1 { get; set; }
        public decimal facturable1iva { get; set; }
        public bool pagado1 { get; set; }
        public string transaccion { get; set; }
        [NotMapped]
        public string Concepto
        {
            get
            {
                return _concepto;
            }
            set
            {
                _concepto = value;
            }
        }


        public string concepto
        {
            get
            {
                return _concepto;
            }
            set
            {
                _concepto = value;
            }
        }
        public string descripcion { get; set; }
        public string referencia { get; set; }
        public int estatusID { get; set; }
        public string aprobacion { get; set; }
        public string ip { get; set; }
        public int geoCountryCode { get; set; }
        public string geoCountryName { get; set; }
        public string geoRegion { get; set; }
        public string geoCity { get; set; }
        public string geoPostalCode { get; set; }
        public DateTime? fechaRegistro { get; set; }
        public DateTime? fechaBaja { get; set; }

        public Decimal? ComissionMX { get; set; }
        public Decimal? ComissionUSD { get; set; }
        public Decimal? TotalMX { get; set; }
        public Decimal? TotalUSD { get; set; }
        public Decimal? ExchangeRateDown { get; set; }
        public Decimal? ExchangeRateUp { get; set; }
        public Boolean? PaidWithCredit { get; set; }

        public Decimal? ServiceInMXN { get; set; }
        public Decimal? ServiceInDlls { get; set; }

        [NotMapped]
        public String Partner { get; set; }
        [NotMapped]
        public String UserEmail { get; set; }
        [NotMapped]
        public Decimal Comissions { get; set; }
    }
}