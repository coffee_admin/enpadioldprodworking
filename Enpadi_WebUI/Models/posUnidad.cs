﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enpadi_WebUI.Models
{
    [Table("posUnidades")]
    public class posUnidad
    {
        [Key]
        public int unidadID { get; set; }

        [Display(Name="Unidad")]
        public string unidad { get; set; }

        [Display(Name="Abreviación")]
        public string abreviacion { get; set; }
    }
}