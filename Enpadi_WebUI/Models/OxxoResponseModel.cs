﻿using System;
using System.Collections.Generic;
using Enpadi_WebUI.DAO;

namespace Enpadi_WebUI.Models
{
    public class OxxoResponseModel : OxxoResponse 
    {

        #region constructors

        public OxxoResponseModel()
        {
            received_locally_at = DateTime.Now;
        }


        #endregion constructors

        #region properties

        public string oxxoresponse_id { get; set; }
        public string data_object_id { get; set; }
        public string data_object_livemode { get; set; }
        public string data_object_created_at { get; set; }
        public string data_object_currency { get; set; }
        public string data_object_amount { get; set; }
        public string data_object_payment_status { get; set; }

        public string customer_email { get; set; }
        public string customer_phone { get; set; }
        public string customer_name { get; set; }

        public string line_item_price { get; set; }
        public string line_item_id { get; set; }
        public string line_item_parent_id { get; set; }

        public string charges_data_id { get; set; }
        public string charges_data_livemode { get; set; }
        public string charges_data_created_at { get; set; }
        public string charges_data_currency { get; set; }
        public string charges_data_description { get; set; }
        public string charges_data_status { get; set; }
        public string charges_data_amount { get; set; }
        public string charges_data_paid_at { get; set; }
        public string charges_data_fee { get; set; }
        public string charges_data_order_id { get; set; }

        public string payment_method_service_name { get; set; }
        public string payment_method_type { get; set; }
        public string payment_method_expires_at { get; set; }
        public string payment_method_reference { get; set; }

        public string livemode { get; set; }
        public string id { get; set; }
        public string type { get; set; }
        public DateTime received_locally_at { get; set; }





        #endregion properties
    }

}
