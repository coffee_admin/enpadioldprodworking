﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    [Table("ServiceCategory")]
    public class ServiceCategory
    {
        [NotMapped]
        public static class Keys {
            public static readonly Guid Basic = Guid.Parse("75BDCA87-8A6A-421F-9E03-63AB825942DA");
            public static readonly Guid Premium = Guid.Parse("D1CF3D87-837E-4AB1-9FFD-7CBBA74005DC");
            public static readonly Guid SuperPremium = Guid.Parse("0D939554-53E0-434B-BB7D-87D684568837");
            public static readonly Guid serviceChargeId = Guid.Parse("9023ECBA-5F2F-477E-8B0E-973014C4F270");
        }

        [Key]
        public Guid Id { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
    }
}