﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using conekta;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;

namespace Enpadi_WebUI.Models
{
    public class ConektaModel
    {

        #region db context and conf 

        private static ApplicationDbContext db = new ApplicationDbContext();
        private static bool bModoProduccion = Convert.ToBoolean(ConfigurationManager.AppSettings["ModoProduccion"]);

        #endregion db context and conf

        #region conekta keys

        private static String CONEKTA_PUBLIC_KEY = (bModoProduccion) ? conekta.SecurityKeys.Enpadi_conekta_publickey : conekta.SecurityKeys.Test_enpadi_conekta_publickey;
        private static String CONEKTA_PRIVATE_KEY = (bModoProduccion) ? conekta.SecurityKeys.Enpadi_conekta_privatekey : conekta.SecurityKeys.Test_enpadi_conekta_privatekey;

        #endregion conketa keys


        #region methods

        /// <summary>
        /// Method to format amount that need to be called before calling RecargarTiendaOxxo()
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public static Int32 FormatOxxoResponseAmount(Decimal amount)
        {
            var amountvar = "0";
            var amountvarsplit = amount.ToString().Replace(",", "").Split('.');
            if (amountvarsplit.ElementAtOrDefault(1) != null)
            {
                if (amountvarsplit[1].Count() > 1)
                {
                    amountvar = amountvarsplit[0].ToString() + amountvarsplit[1];
                }
                else
                {
                    amountvar = amountvarsplit[0].ToString() + amountvarsplit[1] + "0";
                }
            }
            else
            {
                amountvar = amountvarsplit[0].ToString() + "00";
            }

            return Int32.Parse(amountvar);
        }


        /// <summary>
        /// This method is use in conekta for paying with an oxxo stub, amount most be format with FormatOxxoResponseAmount
        /// </summary>
        /// <param name="monto"></param>
        /// <param name="skin"></param>
        /// <param name="currentUserId"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public static OxxoOrderResponseForPaySub RecargarTiendaOxxo(int OrderAmount, List<EnpadiLineItemModel> LineItems, String clientEmail)
        {
            #region data
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.email == clientEmail);

            if (usuario == null)
            {
                throw new ArgumentException("User not found on Enpadi.");
            }

            var contextDB = new IdentityDbContext<IdentityUser>("Enpadi_DB_Connection");
            var storeDB = new UserStore<IdentityUser>(contextDB);
            var managerDB = new UserManager<IdentityUser>(storeDB);
            var userDB = managerDB.FindByEmail(usuario.email);

            #endregion data

            #region conecta connection

            conekta.Api.apiKey = CONEKTA_PRIVATE_KEY;
            conekta.Api.version = conekta.SecurityKeys.Conekta_version;
            conekta.Api.locale = conekta.SecurityKeys.Conekta_lang_es;

            #endregion connection

            #region conekta customer

            //create the customer required for the order
            conekta.Customer customer = new conekta.Customer
            {
                //id = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID),
                name = String.Format("{0} {1} {2}", usuario.datNombre, usuario.datPaterno, usuario.datMaterno),
                email = String.Format("{0}", usuario.email),
                //phone = "+ " + String.Format("{0:0000000000000}", userDB.PhoneNumber) //"5215555555555"
                phone = ("0000000000000" + userDB.PhoneNumber).Substring(userDB.PhoneNumber.Length)
            };

            #endregion conekta client

            #region conekta product
            ////Create a product and added to the conectalist 
            //conekta.LineItem product = new conekta.LineItem
            //{
            //    //id = "1",
            //    name = "Recarga en Oxxo",
            //    //description = "Recarga de " + monto.ToString() + " en tienda Oxxo",
            //    unit_price = LineItemAmount,
            //    quantity = 1,
            //    //sku = "REC-" + monto.ToString()
            //};

            //create the conekta list require for the order
            //conekta.ConektaList lineItems = new ConektaList(typeof(conekta.LineItem));
            //lineItems.data = new object[1];
            //lineItems.data[0] = product;

            #endregion conekta product

            #region conekta shipping 

            //create the shipping line
            conekta.ShippingLine shippingLine = new conekta.ShippingLine
            {
                amount = 16,
                carrier = "Sin envio"
            };

            //create conekta list shipping lines required for the order
            conekta.ConektaList shippingLines = new ConektaList(typeof(conekta.ShippingLine));
            shippingLines.data = new object[1];
            shippingLines.data[0] = shippingLine;

            #endregion conekta shipping

            #region shipping contact

            //create the address required for the shipping contact
            conekta.Address shippingContactAddress = new conekta.Address
            {
                street1 = (usuario.datDir1 != null ? usuario.datDir1 : string.Empty),
                street2 = (usuario.datDir1 != null ? usuario.datDir2 : string.Empty),
                city = (usuario.ciudades != null ? usuario.ciudades.nombre : string.Empty),
                state = (usuario.estados != null ? usuario.estados.nombre : string.Empty),
                country = (usuario.paises != null ? usuario.paises.nombre : string.Empty),
                zip = (usuario.datCP != null ? usuario.datCP : string.Empty)
            };

            //create the shipping contact required for the order
            conekta.ShippingContact shippingContact = new conekta.ShippingContact
            {
                id = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID),
                address = shippingContactAddress,
                phone = ("0000000000000" + userDB.PhoneNumber).Substring(userDB.PhoneNumber.Length),
                //phone = "+ " + String.Format("{0:0000000000000}", userDB.PhoneNumber), //usuario.datTelefono
                receiver = String.Format("{0} {1} {2}", usuario.datNombre, usuario.datPaterno, usuario.datMaterno),
                email = usuario.email
            };

            #endregion shipping contact

            #region charges

            //create the payment method required for the charge
            conekta.PaymentMethod paymentMethod = new conekta.PaymentMethod
            {
                type = conekta.SecurityKeys.Conekta_payment_method_type,
                expires_at = DateTime.Now.AddDays(2).ToString("yyyyMMddHH")
            };

            //create the charge
            conekta.Charge charge = new conekta.Charge
            {
                //id= String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID),
                //created_at = DateTime.Now.ToString("yyyyMMddHHmmssfff"),
                //currency = conekta.SecurityKeys.Conekta_currency_MXN,
                payment_method = paymentMethod,
                //livemode = false,
                amount = OrderAmount
            };

            conekta.ConektaList charges = new ConektaList(typeof(conekta.Charge));
            charges.data = new object[1];
            charges.data[0] = charge;

            #endregion charges

            #region conekta order

            //create the order
            conekta.Order order = new conekta.Order
            {
                currency = conekta.SecurityKeys.Conekta_currency_MXN,
                customer_info = customer,
                //line_items = LineItems,
                shipping_contact = shippingContact,
                //shipping_lines = shippingLines,

                charges = charges,
                param = ""
            };

            #endregion conekta order
            //customer.name 
            #region send to conekta

            String strLineItems = String.Empty;

            foreach (EnpadiLineItemModel lineitem in LineItems)
            {
                strLineItems += @"""name"": """ + lineitem.name + @""",";
                strLineItems += @"""unit_price"": " + lineitem.unit_price.ToString() + "00" + @",";
                strLineItems += @"""quantity"": " + lineitem.quantity;
            }

            conekta.Order response = new conekta.Order().create(@"
                        {""currency"": """ + order.currency + @""" ,
                         ""customer_info"": {
                	        ""name"": """ + Regex.Replace(customer.name, "[0-9]", "") + @""", 
                	        ""phone"": """ + customer.phone + @""",
                	        ""email"": """ + customer.email + @"""
                        },
                         ""line_items"": [{ " + strLineItems + @"
                         }],
                        ""charges"": [{
                	        ""payment_method"": {
                		        ""type"": """ + charge.payment_method.type + @"""
                	        },
                	    ""amount"": " + charge.amount.ToString() + @"
                        }]
                        }");


            #endregion send to conekta

            #region response data

            JObject responseCharge = (JObject)response.charges.data[0];

            OxxoOrderResponseForPaySub item = new OxxoOrderResponseForPaySub();

            item.Oxxo_responseReference = ((JObject)responseCharge.GetValue("payment_method")).GetValue("reference").ToString();
            item.Oxxo_responsePaymentMethod = ((JObject)responseCharge.GetValue("payment_method")).GetValue("service_name").ToString();
            item.Oxxo_responseAmount = decimal.Parse(responseCharge.GetValue("amount").ToString()) / 100;
            item.Oxxo_responseDescription = responseCharge.GetValue("description").ToString();
            item.Oxxo_responseOrderId = responseCharge.GetValue("order_id").ToString();
            item.Oxxo_responseOrderCreationDate = responseCharge.GetValue("created_at").ToString();
            item.Oxxo_responseOrderPaymentStatus = responseCharge.GetValue("status").ToString();

            //fees should not be charge here, it should be charge when the payment is done

            #endregion response data

            return item;
        }

        #endregion methods

        #region classes

        public class OxxoOrderResponseForPaySub
        {

            #region members

            private String oxxo_responseReference = String.Empty;
            private String oxxo_responsePaymentMethod = String.Empty;
            private Decimal oxxo_responseAmount = 0m;
            private String oxxo_responseDescription = String.Empty;
            private String oxxo_responseOrderId = String.Empty;
            private String oxxo_responseOrderCreationDate = String.Empty;
            private String oxxo_responseOrderPaymentStatus = String.Empty;
            private Decimal oxxo_responseFee = 0m;

            #endregion members

            #region properties

            public string Oxxo_responseReference { get => oxxo_responseReference; set => oxxo_responseReference = value; }
            public string Oxxo_responsePaymentMethod { get => oxxo_responsePaymentMethod; set => oxxo_responsePaymentMethod = value; }
            public decimal Oxxo_responseAmount { get => oxxo_responseAmount; set => oxxo_responseAmount = value; }
            public string Oxxo_responseDescription { get => oxxo_responseDescription; set => oxxo_responseDescription = value; }
            public string Oxxo_responseOrderId { get => oxxo_responseOrderId; set => oxxo_responseOrderId = value; }
            public string Oxxo_responseOrderCreationDate { get => oxxo_responseOrderCreationDate; set => oxxo_responseOrderCreationDate = value; }
            public string Oxxo_responseOrderPaymentStatus { get => oxxo_responseOrderPaymentStatus; set => oxxo_responseOrderPaymentStatus = value; }
            public decimal Oxxo_responseFee { get => oxxo_responseFee; set => oxxo_responseFee = value; }

            #endregion propeties
        }

        #endregion classes

    }
}