﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enpadi_WebUI.Models;
using System.Configuration;
using System.Reflection;

namespace Enpadi_WebUI.Models
{
    public class Coffee
    {
        private static string region = ConfigurationManager.AppSettings["Region"];

        public static class keys {
            public static String usa = "usa";
            public static String dev = "dev";
            public static String mex = "mex";
            public static String kiosko = "kiosko";
        }

        public static Guid GetPlatformFromRegion() {

            Guid result = Guid.Empty;

            switch (region) {
                case "usa":
                    result = PlatformModel.Keys.WebUsa;
                    break;
                case "kiosko":
                    result = PlatformModel.Keys.EnpadiKiosko;
                    break;
                case "dev":
                    result = PlatformModel.Keys.AllPlatforms;
                    break;
                default:
                    result = PlatformModel.Keys.AllPlatforms;
                    break;
            }

            return result;
        }

        public static void InsertLog(Int32 idLogeType, String module = "", String description = "", String userId = "", String employeeCompanyNumber = "", String ipAddress = "")
        {

            Models.LogModel newItem = new LogModel(
                idLogeType,
                module,
                description,
                userId,
                GetUser_IP(),
                DateTime.Now
                );
            //TODO save the log on database
           // Models.Log.Save(newItem);
        }

        public static void AssignError(String errorNumber, String spName, String errorMessage, String additionalInfo, Boolean errorFound = true, String employeeCode = "", String employeeCompanyNumber = "", String ipAddress = "", String Module = "")
        {

            SystemErrorModel error = new SystemErrorModel(errorNumber, spName, errorMessage, additionalInfo, errorFound);

            Coffee.InsertLog(Models.LogModel.idLogTypeError, Module, "Se ha producido un error en con el SP " + spName, employeeCode, employeeCompanyNumber, ipAddress);

        }

        public static String GetUser_IP()
        {
            string VisitorsIPAddr = string.Empty;

            if (System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                VisitorsIPAddr = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else if (System.Web.HttpContext.Current.Request.UserHostAddress.Length > 3)
            {
                VisitorsIPAddr = System.Web.HttpContext.Current.Request.UserHostAddress;
            }
            else if (System.Web.HttpContext.Current.Request.UserHostAddress.Length != 0)
            {
                VisitorsIPAddr = "127.0.0.1";
            }

            return VisitorsIPAddr;
        }

        public static void SaveEnpadiPetition(object obj, Guid objId, Int32 objIntId = 0) {

            ApplicationDbContext db = new ApplicationDbContext();

            EnpadiPetition enpadiPetition = null;
            Int32 count = 0;
            DateTime createdAt = DateTime.Now;

            //save all properites send in enpadi petition
            foreach (PropertyInfo propertyInfo in obj.GetType().GetProperties())
            {
                enpadiPetition = new EnpadiPetition
                {
                    ObjIntId = objIntId,
                    ObjId = objId,
                    Name = (propertyInfo.Name == null ? "" : propertyInfo.Name),
                    Value = (propertyInfo.GetValue(obj) == null? "" : propertyInfo.GetValue(obj).ToString()),
                    Order = count,
                    CreatedAt = createdAt
                };

                db.EnpadiPetition.Add(enpadiPetition);

                count++;
            }

            db.SaveChanges();
        }

        public static void SaveEnpadiResponse(object obj, Guid objId, Int32 objIntId = 0)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            EnpadiResponse enpadiResponse = null;
            Int32 count = 0;
            DateTime createdAt = DateTime.Now;

            //save all properites send in enpadi petition
            foreach (PropertyInfo propertyInfo in obj.GetType().GetProperties())
            {
                enpadiResponse = new EnpadiResponse
                {
                    ObjIntId = objIntId,
                    ObjId = objId,
                    Name = (propertyInfo.Name == null ? "" : propertyInfo.Name),
                    Value = (propertyInfo.GetValue(obj) == null ? "" : propertyInfo.GetValue(obj).ToString()),
                    Order = count,
                    CreatedAt = createdAt
                };

                db.EnpadiResponse.Add(enpadiResponse);

                count++;
            }

            db.SaveChanges();
        }
    }
}