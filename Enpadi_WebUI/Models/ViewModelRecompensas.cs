﻿using System.Collections.Generic;
namespace Enpadi_WebUI.Models
{
    public class ViewModelRecompensas
    {
        public int periodo { get; set; }
        public string periodo_descripcion { get; set; }
        public tarjetas usuario { get; set; }
        public List<periodo> meses { get; set; }
        public List<recompensaListado> recompensas { get; set; }
        public decimal saldoInicial { get; set; }
        public decimal abonos { get; set; }
        public decimal cargos { get; set; }
        public decimal saldoFinal { get; set; }
    }
}