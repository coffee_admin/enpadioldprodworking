﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    public class PlatformRegion
    {
        private static bool bModoProduccion = Convert.ToBoolean(ConfigurationManager.AppSettings["ModoProduccion"]);


        /// <summary>
        /// all available regions, te configuration most be on the web.config file
        /// </summary>
        public class Regions {
            public const string dev = "dev";
            public const string usa = "usa";
            public const string mx = "mx";
            public const string kiosko = "kiosko";
            public const string usaAPI = "usaapi";
            public const string mxAPI = "mxapi";
        }

        /// <summary>
        /// Get the region of the web config file
        /// </summary>
        public static String GetPlatformRegion()
        {
            String region = ConfigurationManager.AppSettings["Region"];
            return region;
        }

    }
}