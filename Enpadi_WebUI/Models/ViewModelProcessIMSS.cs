﻿using Enpadi_WebUI.WSDiestel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    public class ViewModelProcessIMSS
    {
        #region notmapped

        //Arreglo de Campos para la Mensajeria Universal
        [NotMapped]
        [ScaffoldColumn(false)]
        public cCampo[] DisplayFields { get; set; }

        #endregion region not mapped

        #region properties

        public Boolean new_registry = false;
        public String sku_service = String.Empty;

        public Guid opagoId { get; set; }
        public decimal totalAmountInUSD = 0;
        public decimal totalAmountInMXN = 0;
        public decimal totalFeesMXN = 0;
        public decimal totalFeesUSD = 0;
        
        #endregion properties
    }
}