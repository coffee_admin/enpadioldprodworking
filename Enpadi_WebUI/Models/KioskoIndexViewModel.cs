﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    public class KioskoIndexViewModel
    {
        private Decimal _availableCredit = 0;

        public EnpadiCardModel enpadiCard { get; set; }
        public AspNetUsersModel user { get; set; }
        public Decimal availableCredit
        {
            get
            {
                _availableCredit = enpadiCard.creditoDisponible - enpadiCard.creditoPorPagar;
                return _availableCredit;
            }
            set
            {
                _availableCredit = value;
            }
        }
        public Decimal AccountBalance { get; set; }
    }
}