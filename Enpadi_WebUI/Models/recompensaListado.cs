﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    public class recompensaListado
    {
        public int recompensaID { get; set; }
        public int tarjetaID { get; set; }
        public string tcNum { get; set; }
        public string origen { get; set; }
        public string socio { get; set; }
        public string transaccion { get; set; }
        public string concepto { get; set; }
        public decimal abono { get; set; }
        public decimal cargo { get; set; }
        public DateTime fechaAlta { get; set; }
    }
}