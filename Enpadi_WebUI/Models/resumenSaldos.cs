﻿using System;
namespace Enpadi_WebUI.Models
{
    public class resumenSaldos
    {
        public int Orden { get; set; }
        public string Periodo { get; set; }
        public int Cuentas { get; set; }
        public decimal Saldo { get; set; }
    }
}