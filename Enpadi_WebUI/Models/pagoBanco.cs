﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enpadi_WebUI.Models
{
    [Table("PagoBancos")]
    public class pagoBanco
    {
        [Key]
        public int pagoId { get; set; }

        [Display(Name="ID")]
        public string id { get; set; }

        [Display(Name="Descripción")]
        public string description { get; set; }

        [Display(Name="Mensaje de Error")]
        public string error_message { get; set; }

        [Display(Name="Autorización")]
        public string authorization { get; set; }

        [Display(Name="Monto")]
        public decimal amount { get; set; }

        [Display(Name="Operación Tipo")]
        public string operation_type { get; set; }

        [Display(Name="Metodo de Pago")]
        public string payment_method { get; set; }

        [Display(Name="CLABE Interbancaria")]
        public string clabe { get; set; }

        [Display(Name="Banco")]
        public string bank { get; set; }

        [Display(Name="Tipo")]
        public string type { get; set; }

        [Display(Name="Nombre")]
        public string name { get; set; }

        [Display(Name="Orden ID")]
        public string order_id { get; set; }

        [Display(Name="Transaccion Tipo")]
        public string transaction_type { get; set; }

        [Display(Name="Fecha de Creación")]
        public string creation_date { get; set; }

        [Display(Name="Moneda")]
        public string currency { get; set; }

        [Display(Name="Estatus")]
        public string status { get; set; }

        [Display(Name="Estatus")]
        public string method { get; set; }
    }
}