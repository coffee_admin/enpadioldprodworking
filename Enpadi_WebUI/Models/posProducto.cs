﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Enpadi_WebUI.Models
{
    [Table("posProductos")]
    [Bind(Exclude = "tienda, socioID, categoria, unidad, abreviacion")]
    public class posProducto
    {
        [Key]
        public int productoID { get; set; }

        [Display(Name = "Tienda")]
        public int tiendaID { get; set; }
        public virtual posTienda posTienda { get; set; }



        [Display(Name = "Categoria")]
        public int categoriaID { get; set; }
        public virtual posCategoria posCategoria { get; set; }



        [Display(Name = "Producto")]
        public string producto { get; set; }

        [Display(Name = "Descripción")]
        public string descripcion { get; set; }

        [Display(Name = "Imagen")]
        public string imagen { get; set; }

        [Display(Name = "Unidad")]
        public int unidadID { get; set; }
        public virtual posUnidad posUnidad { get; set; }

        [ScaffoldColumn(false)]
        public string unidad { get; set; }

        [Display(Name = "Monto")]
        [DisplayFormat(DataFormatString = "{0:c}")]
        public decimal monto { get; set; }

        [Display(Name = "Moneda")]
        public int monedaID { get; set; }
        public virtual moneda moneda { get; set; }


        //TBBM: i don't know whay this was here if there is no columns on the database
        [ScaffoldColumn(false)]
        public string abreviacion { get; set; }

        [ScaffoldColumn(false)]
        public string categoria { get; set; }

        [ScaffoldColumn(false)]
        public string tienda { get; set; }

        [ScaffoldColumn(false)]
        public int socioID { get; set; }
    }
}