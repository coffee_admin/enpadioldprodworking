﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    [Table("opCards")]
    public class openpayCard
    {
        [Key]
        public int id { get; set; }

        [ScaffoldColumn(false)]
        public string CardID { get; set; }

        [ScaffoldColumn(false)]
        public int ExternalID { get; set; }

        [Required]
        [Display(Name="Titular")]
        public string HolderName { get; set; }

        [Required]
        [DataType(DataType.CreditCard)]
        [Display(Name = "Tarjeta")]
        [MaxLength(16, ErrorMessage = "El Número de su Tarjeta debe estar compuesto de 15 o 16 Digitos")]
        [MinLength(15, ErrorMessage = "El Número de su Tarjeta debe estar compuesto de 15 o 16 Digitos")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Número de Tarjeta Incorrecto")]
        public string CardNumber { get; set; }

        [Required]
        [Display(Name = "CVV")]
        [MaxLength(4, ErrorMessage = "El CVV debe estar compuesto de 3 o 4 Digitos")]
        [MinLength(3, ErrorMessage = "El CVV debe estar compuesto de 3 o 4 Digitos")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "CVV Incorrecto")]
        public string CVV2 { get; set; }

        [Required]
        [Display(Name = "Mes")]
        public string ExpirationMonth { get; set; }

        [Required]
        [Display(Name = "Año")]
        public string ExpirationYear { get; set; }

        [Display(Name = "Dirección")]
        public string Address1 { get; set; }

        [Display(Name = " ")]
        public string Address2 { get; set; }

        [Display(Name = " ")]
        public string Address3 { get; set; }

        [Display(Name = "Ciudad")]
        public string City { get; set; }

        [Display(Name = "Estado")]
        public string State { get; set; }

        [Display(Name = "Pais")]
        public string Country { get; set; }

        [Display(Name="CodigoPostal")]
        [MaxLength(5, ErrorMessage = "El Codigo Postal debe estar compuesto de 5 Digitos")]
        [MinLength(5, ErrorMessage = "El Codigo Postal debe estar compuesto de 5 Digitos")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Codigo Postal Incorrecto")]
        public string PostalCode { get; set; }

        [ScaffoldColumn(false)]
        public bool AllowCharges { get; set; }

        [ScaffoldColumn(false)]
        public bool AllowPayouts { get; set; }

        [ScaffoldColumn(false)]
        public string Brand { get; set; }

        [ScaffoldColumn(false)]
        public string Type { get; set; }

        [ScaffoldColumn(false)]
        public string BankName { get; set; }

        [ScaffoldColumn(false)]
        public string BankCode { get; set; }

        [ScaffoldColumn(false)]
        public string CustomerId { get; set; }

        [ScaffoldColumn(false)]
        public bool PointsCard { get; set; }

        [NotMapped]
        [ScaffoldColumn(false)]
        public DateTime CreationDate { get; set; }
    }
}