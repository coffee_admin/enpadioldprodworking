﻿namespace Enpadi_WebUI.Models
{
    public class ViewModelTiendaVirtual
    {
        public posProducto producto { get; set; }
        public int productoID { get; set; }
        public int tarjetaID { get; set; }
        public decimal saldoActual { get; set; }
        public string transaccion { get; set; }
        public string referencia { get; set; }
        public int estatus { get; set; }
        public string aprobacion { get; set; }
        public int codigoRespuesta { get; set; }
        public string mensajeRespuesta { get; set; }
    }
}