﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enpadi_WebUI.Models
{
    [Table("Origen")]
    public class origen
    {
        [Key]
        public int origenID { get; set; }

        [Display(Name="Origen")]
        public string descripcion { get; set; }

        public DateTime? fechaAlta { get; set; }

        public DateTime? fechaBaja { get; set; }
    }
}