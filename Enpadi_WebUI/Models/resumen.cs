﻿using System;

namespace Enpadi_WebUI.Models
{
    public class resumen
    {
        public int Periodo { get; set; }
        public string Mes { get; set; }
        public DateTime Fecha { get; set; }
        public string Socio { get; set; }
        public int Transacciones { get; set; }
        public decimal Abono { get; set; }
        public decimal Cargo { get; set; }
    }
}