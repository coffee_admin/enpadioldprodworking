﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enpadi_WebUI.DAO;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Enpadi_WebUI.Models
{
    [Table("TransactionType")]
    public class TransactionTypeModel 
    {
        public static class Keys {
            public static readonly Guid Charge = Guid.Parse("BA2B5C68-6585-4133-9F9D-06DC987B906C");
            public static readonly Guid Payment = Guid.Parse("D9F3452B-0B19-4FCB-8386-24D5ACCDB90F");
            public static readonly Guid Remittances = Guid.Parse("3E5C22A8-55F2-46BD-889D-3F4CF288AAB6");
            public static readonly Guid Return = Guid.Parse("473A08FF-160A-4068-BD99-5B63EC9E5CE5");
            public static readonly Guid CashOut = Guid.Parse("08CDB6D4-AEA4-4A63-A6BA-B69ED9A18DD8");
            public static readonly Guid TransferOutPlatform = Guid.Parse("5AB02548-20F6-4699-B5C8-E0D785F02BEA");
            public static readonly Guid TransferInPlatform = Guid.Parse("A2B8A527-03C2-458D-B413-FEE247CA2AED");

        } 

        [Key]
        public Guid id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }
}