﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    [Table("TransactionFee")]
    public class TransactionFee
    {
        [Key]
        public Guid Id { get; set; }

        public Guid TransaccionesId { get; set; }
        //public virtual trans trans { get; set; }

        public Guid FeeId { get; set; }
        public virtual Fee Fee { get; set;}

        public Decimal FixAmount { get; set; }

        public Decimal VariableAmount { get; set; }

        public Decimal TaxFeeAmount { get; set; }

        public Decimal? FixAmountDlls { get; set; }

        public Decimal? VariableAmountDlls { get; set; }

        public Decimal? TaxFeeAmountDlls { get; set; }

        public Decimal ChargedByPartnerAmount { get; set; }

        public Boolean PaidOut { get; set; }

        public DateTime? PaidOutAt { get; set; }

        public Decimal? AmountPaid { get; set; }


        [NotMapped]
        public String FeeName { get; set; }
    }
}