﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enpadi_WebUI.Models
{
    [Table("PredialConceptos")]
    public class predialConcepto
    {
        [Key]
        public int conceptoID { get; set; }

        [Display(Name="Concepto")]
        public string descripcion { get; set; }

        [Display(Name="Tipo")]
        public int tipo { get; set; }

        [Display(Name="Clave Contable")]
        public string claveContable { get; set; }
    }
}