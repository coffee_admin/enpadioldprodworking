﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enpadi_WebUI.DAO;

namespace Enpadi_WebUI.Models
{
    public class ClientEncryptionInfoModel : ClientEncryptionInfo
    {

        #region client info

        public Guid Id { get; set; }
        public String Name { get; set; }
        public String Comercial_Name { get; set; }
        public String RFC { get; set; }

        public String WebHook { get; set; }

        public String Contact_name { get; set; }
        public String Contact_email { get; set; }
        public String Contact_phone_number { get; set; }


        public DateTime Acepted_at { get; set; }
        public DateTime Created_at { get; set; }
        public DateTime Deleted_at { get; set; }
        public Boolean Blocked { get; set; }

        public Decimal ChargeRate { get; set; }
        public Decimal Charge { get; set; }

        #endregion client info

        #region enpadi info

        public Guid UserId { get; set; }

        #region only referenced

        public String UserName { get; }
        public String Email { get; }
        public String tcNum { get; }
        public String tcDigitos { get; }

        #endregion only referenced

        #endregion enpadi info

        #region encryuption info

        public String Private_Key { get; set; }
        public String Public_Key { get; set; }

        #endregion encryption info
    }
}