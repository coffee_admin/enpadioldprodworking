﻿using System.Collections.Generic;
namespace Enpadi_WebUI.Models
{
    public class ViewModelServices
    {
        public remesa remesaUSA { get; set; }
        public int fromPaisID { get; set; }
        public int fromEstadoID { get; set; }
        public int fromCiudadID { get; set; }
        public int toPaisID { get; set; }
        public int toEstadoID { get; set; }
        public int toCiudadID { get; set; }
        public IEnumerable<pais> paises { get; set; }
    }
}