﻿namespace Enpadi_WebUI.Models
{
    public class ViewModelNavigation
    {
        public tarjetas usuario { get; set; }
        public decimal entradas { get; set; }
        public decimal salidas { get; set; }
        public decimal comisionesCobradas { get; set; }
        public decimal comisionesPagadas { get; set; }
        public decimal comisionesFacturables { get; set; }
        public decimal saldoActual { get; set; }
        public decimal saldoDisponible { get; set; }
        public decimal saldoRecompensas { get; set; }
        public decimal saldoTiempoAire { get; set; }
        public decimal saldoPagoServicios { get; set; }
    }
}