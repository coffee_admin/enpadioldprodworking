﻿using Enpadi_WebUI.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    [Table("EnpadiLineItem")]
    public class EnpadiLineItemModel : EnpadiLineItem
    {

        #region constructors

        public EnpadiLineItemModel() {
            id = Guid.NewGuid();
        }

        #endregion constructos

        #region keys



        #endregion keys

        [Key]
        public Guid id { get; set; }

        public Guid? orderId { get; set; }
        public Decimal? unit_price { get; set; }
        public Int32? quantity { get; set; }
        public String name { get; set; }
        public String description { get; set; }
        public long? customer_id { get; set; }
        public String customer_email { get; set; }
        public string created_at { get; set; }
    }
}