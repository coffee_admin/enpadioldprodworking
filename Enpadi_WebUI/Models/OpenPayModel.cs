﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enpadi_WebUI.Models;
using Openpay;
using Openpay.Entities;
using Openpay.Entities.Request;
using System.Configuration;
using Enpadi_WebUI.Properties;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;

namespace Enpadi_WebUI.Models
{
    public class OpenPayModel
    {
        #region private members

        private static ApplicationDbContext db = new ApplicationDbContext();
        private static Boolean bModoProduccion = Convert.ToBoolean(ConfigurationManager.AppSettings["ModoProduccion"]);

        #endregion private membres

        #region keys

        public static String OPENPAY_ID = (bModoProduccion) ? ConfigurationManager.AppSettings["Enpadi_OpenPay_Id"] : ConfigurationManager.AppSettings["Test_OpenPay_id"];
        public static String OPENPAY_PUBLIC_KEY = (bModoProduccion) ? ConfigurationManager.AppSettings["Enpadi_OpenPay_PublicKey"] : ConfigurationManager.AppSettings["Test_OpenPay_PublicKey"];

        public static String OPENPAY_PRIVATE_KEY = (bModoProduccion) ? Settings.Default.Enpadi_OpenPay_PrivateKey : Settings.Default.Test_OpenPay_PrivateKey;

        #endregion keys

        #region methods
        /// <summary>
        /// Add a customer to open pay to make,
        /// the opCusomer needs to have 
        ///     id,
        ///     CustomerID,
        ///     ExternalID,
        ///     Name,
        ///     LastName,
        ///     Email,
        ///     PhoneNumber,
        ///     RequiresAccount,
        ///     Address1,
        ///     Address2,
        ///     Address3,
        ///     City,
        ///     State,
        ///     CountryCode,
        ///     PostalCode
        /// </summary>
        /// <param name="opCustomer"></param>
        /// <param name="currentUserId"></param>
        public static void AddCustomer(
            openpayCustomer opCustomer, string currentUserId)
        {
            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.password == currentUserId);
            opCustomer.ExternalID = usuario.tarjetaID;

            OpenpayAPI api = new OpenpayAPI(OPENPAY_PRIVATE_KEY, OPENPAY_ID, bModoProduccion);

            Openpay.Entities.Address dir = new Openpay.Entities.Address()
            {
                Line1 = opCustomer.Address1,
                Line2 = opCustomer.Address2,
                Line3 = opCustomer.Address3,
                City = opCustomer.City,
                State = opCustomer.State,
                CountryCode = opCustomer.CountryCode,
                PostalCode = opCustomer.PostalCode
            };

            Openpay.Entities.Customer cliente = new Openpay.Entities.Customer()
            {
                ExternalId = opCustomer.ExternalID.ToString(),
                Name = opCustomer.Name,
                LastName = opCustomer.LastName,
                Email = opCustomer.Email,
                PhoneNumber = opCustomer.PhoneNumber,
                Address = dir
            };

            cliente.Address = dir;

            cliente = api.CustomerService.Create(cliente);

            if (cliente.Id != null)
            {
                opCustomer.CustomerID = cliente.Id;
                opCustomer.CLABE = cliente.CLABE;

                OpenPayCustomerModel.Save(opCustomer);

                //db.openpayCustomers.Add(opCustomer);
                //db.SaveChanges();
            }

        }

        public static void CheckCreditCardUser(EnpadiPostOrder enpadiOrder)
        {

            //check if user exist in database from customer_info

            //get the enpadi user
            var context = new ApplicationDbContext();
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            ApplicationUser user = userManager.FindByName(enpadiOrder.order.data_object.customer_info.email);
            String userId = String.Empty;
            tarjetas usuarioOpenPayCard = null;
            openpayCard opCard = new openpayCard();

            if (user != null)
            {
                userId = user.Id.ToString();
                usuarioOpenPayCard = db.tarjetas.FirstOrDefault(x => x.password == userId);
                //TODO: if has a card, get opcard
                //opCard = existing user opcar
            }
            else
            {
                //if it does not exist create a enpadi user
                var EnpadiUser = new ApplicationUser
                {
                    UserName = enpadiOrder.order.data_object.customer_info.email,
                    Email = enpadiOrder.order.data_object.customer_info.email,
                    Name = enpadiOrder.order.data_object.customer_info.name,
                    PhoneNumber = enpadiOrder.order.data_object.customer_info.phoneNumber,
                    TwoFactorEnabled = false,
                    EmailConfirmed = false
                };


                userManager.Create(EnpadiUser, ToolsBO.RandomPassword.Generate());
                userManager.AddToRole(EnpadiUser.Id, SecurityRoles.Usuario);
                db.Entry(EnpadiUser).State = System.Data.Entity.EntityState.Modified;

                //create user enpadi card
                tarjetas tc = db.tarjetas.Where(t => t.tcAsignada == false).FirstOrDefault();
                tc.tcAsignada = true;
                tc.datNombre = enpadiOrder.order.data_object.customer_info.name;
                tc.datPaterno = enpadiOrder.order.data_object.customer_info.last_name;
                tc.email = enpadiOrder.order.data_object.customer_info.email;
                tc.password = EnpadiUser.Id;
                db.Entry(tc).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                Openpay.Entities.Address address = new Openpay.Entities.Address()
                {
                    Line1 = enpadiOrder.order.data_object.customer_info.address_line1,
                    Line2 = enpadiOrder.order.data_object.customer_info.address_line2,
                    Line3 = enpadiOrder.order.data_object.customer_info.address_line3,
                    City = enpadiOrder.order.data_object.customer_info.address_city,
                    State = enpadiOrder.order.data_object.customer_info.address_state,
                    CountryCode = enpadiOrder.order.data_object.customer_info.address_country_code,
                    PostalCode = enpadiOrder.order.data_object.customer_info.address_postal_code
                };

                //create an openpay user
                openpayCustomer customer = new openpayCustomer
                {
                    ExternalID = tc.tarjetaID,
                    Name = enpadiOrder.order.data_object.customer_info.name,
                    LastName = enpadiOrder.order.data_object.customer_info.last_name,
                    Email = enpadiOrder.order.data_object.customer_info.email,
                    PhoneNumber = enpadiOrder.order.data_object.customer_info.phoneNumber,
                    Address1 = enpadiOrder.order.data_object.customer_info.address_line1,
                    Address2 = enpadiOrder.order.data_object.customer_info.address_line2,
                    Address3 = enpadiOrder.order.data_object.customer_info.address_line3,
                    State = enpadiOrder.order.data_object.customer_info.address_state,
                    City = enpadiOrder.order.data_object.customer_info.address_city,
                    PostalCode = enpadiOrder.order.data_object.customer_info.address_postal_code,
                    CountryCode = enpadiOrder.order.data_object.customer_info.address_country_code,

                };


                //NEED TO FINISH THIS FUNCTION
                AddCustomer(customer, EnpadiUser.Id);


                //create an openpay credit card
                opCard = AddCreditCard(enpadiOrder, customer, EnpadiUser.Id);

            }
        }




        /// <summary>
        /// Add an openpay creditcard 
        /// </summary>
        /// <param name="enpadiOrder"></param>
        /// <param name="customer"></param>
        /// <param name="EnpadiUserId"></param>
        public static openpayCard AddCreditCard(EnpadiPostOrder enpadiOrder, openpayCustomer customer, string EnpadiUserId)
        {
            try
            {
                openpayCard opCard = new openpayCard() { ExternalID = customer.ExternalID };
                openpayCustomer opCustomer = db.openpayCustomers.FirstOrDefault(x => x.ExternalID == customer.ExternalID);
                opCard.CustomerId = opCustomer.CustomerID;

                OpenpayAPI api = new OpenpayAPI(OPENPAY_PRIVATE_KEY, OPENPAY_ID, bModoProduccion);
                Card card = new Card();
                card.TokenId = enpadiOrder.charge.data_object.payment_method.source_id;
                card.DeviceSessionId = enpadiOrder.charge.data_object.payment_method.deviceSessionId;


                card = api.CardService.Create(opCard.CustomerId.ToString(), card);

                if (card.Id != null)
                {
                    if (card.Address == null)
                    {
                        card.Address = new Openpay.Entities.Address();
                    }

                    opCard.HolderName = enpadiOrder.charge.data_object.payment_method.name_on_card;
                    opCard.CardNumber = enpadiOrder.charge.data_object.payment_method.card_number;
                    opCard.ExpirationMonth = enpadiOrder.charge.data_object.payment_method.credit_card_expiration_month.ToString();
                    opCard.ExpirationYear = enpadiOrder.charge.data_object.payment_method.credit_card_expiration_year.ToString();
                    opCard.CVV2 = enpadiOrder.charge.data_object.payment_method.credit_card_security_code.ToString();
                    opCard.CardID = card.Id;
                    opCard.Brand = card.Brand;
                    opCard.Type = card.Type;
                    opCard.BankName = card.BankName;
                    opCard.BankCode = card.BankCode;
                    opCard.Address1 = card.Address.Line1;
                    opCard.Address2 = card.Address.Line2;
                    opCard.Address3 = card.Address.Line3;
                    opCard.PointsCard = card.PointsCard;
                    opCard.PostalCode = card.Address.PostalCode;
                    opCard.Country = card.Address.CountryCode;
                    opCard.AllowCharges = card.AllowsCharges;
                    opCard.AllowPayouts = card.AllowsPayouts;
                    opCard.BankCode = card.BankCode;
                    opCard.BankName = card.BankName;
                    opCard.City = card.Address.City;
                    opCard.ExpirationMonth = card.ExpirationMonth;
                    opCard.ExpirationYear = card.ExpirationYear;

                    opCard = OpenPayCardModel.Save(opCard);

                    return opCard;

                    //return RedirectToAction("RecargarTC");
                }
                else
                {
                    return opCard;
                    //ModelState.AddModelError("", "Ha ocurrido un Error. Intenta con otro Numero de Tarjeta");
                }

            }
            catch (Exception e)
            {
                throw new ArgumentException(SystemErrorModel.ErrorList.PaymentWithBankFail + e.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="postOrder"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public Boolean ProcessPayment(EnpadiPostOrder postOrder, String type)
        {
            Boolean success = false;

            //cretate a switch to indicate what payment type is gonna be

            switch (type.ToLower().Trim())
            {

                case "bank":
                    ProcessPaymentBank(postOrder);
                    break;
                case "card":
                    ProcessPaymentCreditCard(postOrder);
                    break;
                case "store":
                case "7eleven":
                    ProcessPaymentStore(postOrder);
                    break;
                case "bank_account":
                    ProcessPaymentBank(postOrder);
                    break;
                default:
                    break;
            }



            return success;
        }

        public static pagoBanco ProcessPaymentBank(EnpadiPostOrder postOrder)
        {
            try
            {
                tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.email == postOrder.order.data_object.client_email);

                OpenpayAPI api = new OpenpayAPI(OPENPAY_PRIVATE_KEY, OPENPAY_ID, bModoProduccion);

                Openpay.Entities.Customer cliente = new Openpay.Entities.Customer();
                cliente.ExternalId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);
                cliente.Name = String.Format("{0} {1} {2}", usuario.datNombre, usuario.datPaterno, usuario.datMaterno);
                cliente.Email = String.Format("{0}", usuario.email);
                cliente.RequiresAccount = false;

                ChargeRequest request = new ChargeRequest();
                request.Method = OpenpayAPI.RequestMethod.bank_account;
                request.Amount = postOrder.charge.data_object.amount;
                request.Description = "Pago en Banco";
                request.OrderId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);
                request.Customer = cliente;

                Openpay.Entities.Charge charge = api.ChargeService.Create(request);

                pagoBanco ordenBanco = new pagoBanco()
                {
                    id = charge.Id,
                    description = charge.Description,
                    error_message = charge.ErrorMessage,
                    authorization = charge.Authorization,
                    amount = charge.Amount,
                    operation_type = charge.OperationType,
                    payment_method = request.Method,
                    clabe = charge.PaymentMethod.CLABE,
                    bank = charge.PaymentMethod.BankName,
                    name = charge.PaymentMethod.Name,
                    order_id = charge.OrderId,
                    transaction_type = charge.TransactionType,
                    creation_date = charge.CreationDate.ToString(),
                    status = charge.Status,
                    method = charge.Method
                };

                PagoBancosModel.Save(ordenBanco);

                return ordenBanco;
            }
            catch (Exception e)
            {
                throw new ArgumentException(SystemErrorModel.ErrorList.PaymentWithBankFail + e.Message);
            }

        }

        public static pagoTienda ProcessPaymentStore(EnpadiPostOrder postOrder)
        {

            tarjetas usuario = db.tarjetas.FirstOrDefault(x => x.email == postOrder.order.data_object.client_email);

            OpenpayAPI api = new OpenpayAPI(OPENPAY_PRIVATE_KEY, OPENPAY_ID, bModoProduccion);

            Openpay.Entities.Customer cliente = new Openpay.Entities.Customer();
            cliente.ExternalId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);
            cliente.Name = String.Format("{0} {1} {2}", usuario.datNombre, usuario.datPaterno, usuario.datMaterno);
            cliente.Email = String.Format("{0}", usuario.email);
            cliente.RequiresAccount = false;

            ChargeRequest request = new ChargeRequest();
            request.Method = OpenpayAPI.RequestMethod.store;
            request.Amount = postOrder.charge.data_object.amount;
            request.Description = "Pago en Tienda";
            request.OrderId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), usuario.tarjetaID);
            request.Customer = cliente;

            Openpay.Entities.Charge charge = api.ChargeService.Create(request);

            pagoTienda ordenTienda = new pagoTienda()
            {
                id = charge.Id,
                description = charge.Description,
                error_message = charge.ErrorMessage,
                authorization = charge.Authorization,
                amount = charge.Amount,
                operation_type = charge.OperationType,
                type = charge.PaymentMethod.Type,
                reference = charge.PaymentMethod.Reference,
                barcode_url = charge.PaymentMethod.BarcodeURL,
                order_id = charge.OrderId,
                transaction_type = charge.TransactionType,
                creation_date = charge.CreationDate.ToString(),
                status = charge.Status,
                method = charge.Method
            };

            return ordenTienda;

        }

        public static trans ProcessPaymentCreditCard(EnpadiPostOrder enpadiOrder)
        {

            String strSQL = String.Empty;

            try
            {
                //TODO: remember that x.password is equal to id = guid, need to change that

                var context = new ApplicationDbContext();
                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                String customer_userId = userManager.FindByName(enpadiOrder.order.data_object.customer_info.email).Id.ToString();

                String client_userId = userManager.FindByName(enpadiOrder.order.data_object.client_email).Id.ToString();

                tarjetas customer_usuario = db.tarjetas.FirstOrDefault(x => x.password == customer_userId);

                tarjetas client_usuario = db.tarjetas.FirstOrDefault(x => x.password == client_userId);

                trans t = new trans
                {
                    estatusID = 0,
                    origenID = 1,
                    tipoID = 1,
                    modoID = 1,
                    terminalID = 1,
                    monedaID = 484,
                    Concepto = "Deposito",
                    geoCountryCode = 0,
                    geoCountryName = String.Empty,
                    geoRegion = String.Empty,
                    geoCity = String.Empty,
                    geoPostalCode = String.Empty
                };

                strSQL = string.Format("SELECT COUNT(*) As total FROM logTC WHERE idTarjeta = {0} AND fechaAlta >= DATEADD(DAY, -7, getdate())", customer_usuario.tarjetaID);
                IEnumerable<contador> intentos = db.Database.SqlQuery<contador>(strSQL);
                int iIntentos = intentos.ToList().FirstOrDefault().total;

                strSQL = string.Format("SELECT COUNT(*) As total FROM logTC WHERE idTarjeta = {0} AND result = 1 AND fechaAlta >= DATEADD(DAY, -7, getdate())", customer_usuario.tarjetaID);
                IEnumerable<contador> exitosos = db.Database.SqlQuery<contador>(strSQL);
                int iExitosos = exitosos.ToList().FirstOrDefault().total;


                strSQL = string.Format("SELECT * FROM opCards WHERE DeleteDate IS NULL AND ExternalID = {0}", customer_usuario.tarjetaID);
                IEnumerable<openpayCard> opCard = db.Database.SqlQuery<openpayCard>(strSQL);

                if (opCard.Count() > 0)
                {
                    strSQL = string.Format("SELECT * FROM opCustomers WHERE DateDelete IS NULL AND ExternalID = {0}", customer_usuario.tarjetaID);
                    IEnumerable<openpayCustomer> opCustomer = db.Database.SqlQuery<openpayCustomer>(strSQL);

                    if (opCustomer.Count() > 0)
                    {
                        OpenpayAPI api = new OpenpayAPI(OPENPAY_PRIVATE_KEY, OPENPAY_ID, bModoProduccion);

                        ChargeRequest request = new ChargeRequest();
                        request.Method = OpenpayAPI.RequestMethod.card;
                        request.SourceId = opCard.FirstOrDefault().CardID;
                        request.Amount = Convert.ToDecimal(enpadiOrder.order.data_object.amount);
                        request.Currency = "MXN";
                        request.Description = "Recarga de Monedero Enpadi";
                        request.DeviceSessionId = enpadiOrder.charge.data_object.payment_method.source_id;
                        request.OrderId = String.Format("{0}{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), customer_usuario.tarjetaID);

                        Openpay.Entities.Charge charge = api.ChargeService.Create(opCustomer.FirstOrDefault().CustomerID, request);

                        if (charge.Status == "completed")
                        {
                            t.estatusID = 1;
                            t.tarjetaID = client_usuario.tarjetaID;
                            t.tarjeta = client_usuario.tcNum.Substring(client_usuario.tcNum.Length - 4);
                            t.transaccion = charge.OrderId;
                            t.descripcion = charge.Description;
                            t.referencia = charge.Id;
                            t.aprobacion = charge.Authorization;
                            t.monto = charge.Amount;
                            t.abono1 = charge.Amount;
                            t.abonoTotal = charge.Amount;

                            decimal dIva = (decimal)0.16;
                            string strBrand = string.Empty;
                            if (!string.IsNullOrEmpty(opCard.FirstOrDefault().Brand))
                                strBrand = opCard.FirstOrDefault().Brand.ToLower();

                            if (strBrand == socio.SocioID.VisaStr || strBrand == socio.SocioID.MasterCardStr)
                            {
                                t.cargo2 = t.monto * (decimal)(2.9 / 100);
                                t.cargo2iva = t.cargo2 * dIva;
                                t.cargo5 = (decimal)2.9;
                                t.cargo5iva = t.cargo5 * dIva;
                                t.cargoTotal += t.cargo2 + t.cargo2iva + t.cargo5 + t.cargo5iva;
                                t.socioID = socio.SocioID.Visa;
                            }
                            else
                            {
                                t.cargo2 = t.monto * (decimal)(4.9 / 100);
                                t.cargo2iva = t.cargo2 * dIva;
                                t.cargo5 = (decimal)2.9;
                                t.cargo5iva = t.cargo5 * dIva;
                                t.cargoTotal += t.cargo2 + t.cargo2iva + t.cargo5 + t.cargo5iva;
                                t.socioID = socio.SocioID.Amex;
                            }

                            t.ip = "189.25.169.74";
                            //t.transID = trans.Save(t);
                            db.transacciones.Add(t);

                            recompensa r = new recompensa()
                            {
                                tarjetaID = client_usuario.tarjetaID,
                                origenID = 1,
                                socioID = 2,
                                transaccion = charge.OrderId,
                                concepto = "Recompensa Recarga",
                                abono = charge.Amount * (decimal)0.01,
                                cargo = 0
                            };

                            //TODO: add recompensas to model and DAO to save it
                            //db.recompensas.Add(r);

                            LogTCModel envio = new LogTCModel()
                            {
                                idTarjeta = client_usuario.tarjetaID,
                                result = 1,
                                fechaAlta = DateTime.Now,
                                fechaBaja = null
                            };

                            //LogTCModel.Save(envio);
                            //db.logTCs.Add(envio);

                            //db.SaveChanges();


                            //return RedirectToAction("RespuestaTC", "PanelUsuarios", new { estatus = "APROBADA", autorizacion = string.Format("AUTORIZACIÓN  {0}", t.aprobacion), total = string.Format("MONTO  ${0} MXN", t.monto) });

                            return t;
                        }
                        else
                        {
                            LogTCModel envio = new LogTCModel()
                            {
                                idTarjeta = client_usuario.tarjetaID,
                                result = 0
                            };

                            //LogTCModel.Save(envio);
                            //db.logTCs.Add(envio);

                            //db.SaveChanges();

                            throw new ArgumentException(SystemErrorModel.ErrorList.PaymentCreditCardDeclined + charge.Description);
                        }

                    }
                    else
                    {
                        throw new ArgumentException(SystemErrorModel.ErrorList.PaymentCreditCardDeclined + SystemErrorModel.ErrorList.PaymentUserNotFound);
                    }
                }
                else
                {
                    throw new ArgumentException(SystemErrorModel.ErrorList.PaymentCreditCardDeclined + SystemErrorModel.ErrorList.PaymentCreditCardNotFound);
                }

            }
            catch (Exception e)
            {
                // add error log and things
                throw new ArgumentException(SystemErrorModel.ErrorList.GenericError + e.Message);
            }
        }


        #endregion methods
    }
}