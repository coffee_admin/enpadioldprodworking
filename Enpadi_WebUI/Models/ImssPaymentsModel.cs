﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enpadi_WebUI.DAO;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enpadi_WebUI.Models
{
    [Table("ImssPayments")]
    public class ImssPaymentsModel : ImssPayments
    {
        public ImssPaymentsModel() {
            id = Guid.NewGuid();
        }

        [Key]
        public Guid id { get; set; }

        public string beneficiary_name { get; set; }
        public string beneficiary_last { get; set; }
        public string beneficiary_mothers { get; set; }
        public string beneficiary_phone_number { get; set; }
        public string beneficiary_cell_phone_number { get; set; }
        public string beneficiary_email_address { get; set; }
        public string beneficiary_imss_number { get; set; }

        public string buyer_name { get; set; }
        public string buyer_phone_number { get; set; }
        public string buyer_email { get; set; }
        public string buyer_enpadi_card { get; set; }
        public string buyer_enpadi_card_ending { get; set; }

        public string code_base { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 code_number { get; set; }

        public string remitter_name { get; set; }
        public decimal amout_pay { get; set; }
        public DateTime? payment_date_at { get; set; }

        public Guid? transaccionesId { get; set; }
    }
}