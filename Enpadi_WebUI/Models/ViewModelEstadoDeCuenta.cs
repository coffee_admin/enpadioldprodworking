﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Enpadi_WebUI.Models
{
    public class ViewModelEstadoDeCuenta
    {
        public int periodo { get; set; }
        public string periodo_descripcion { get; set; }
        public List<periodo> meses { get; set; }
        public List<movimientosListado> movimientos { get; set; }
        public tarjetas usuario { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal saldoInicial { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal abonos { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal cargos { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal comision1 { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal iva1 { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal comision2 { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal iva2 { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal comision3 { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal iva3 { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal saldoFinal { get; set; }
    }
}