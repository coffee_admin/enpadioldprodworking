﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    public class OperationObject
    {

        #region constructors

        public OperationObject()
        {
            this.Success = false;
            this.Property = new Dictionary<string, string>();
        }

        #endregion constructors

        public Boolean Success { get; set; }
        public Dictionary<String, String> Property { get; set; }


    }
}