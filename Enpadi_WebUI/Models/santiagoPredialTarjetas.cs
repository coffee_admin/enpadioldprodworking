﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enpadi_WebUI.Models
{
    [Table("SantiagoPredialTarjetas")]
    public class santiagoPredialTarjetas
    {
        [Key]
        public int idMov { get; set; }
        public string expediente { get; set; }
        public string ubicacionPredio { get; set; }
        public decimal saldo { get; set; }
        public string titular { get; set; }
        public string tarjeta { get; set; }
        public string expiracion { get; set; }

        [Display(Name="Fecha Intento")]
        public DateTime fechaAlta { get; set; }
    }
}