﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    public class movimientosListado
    {
        [Key]
        public int transID { get; set; }

        [Display(Name="Origen")]
        public string origen { get; set; }
        
        [Display(Name="Tipo")]
        public string tipo { get; set; }

        [Display(Name = "Socio ID")]
        public int socioID { get; set; }

        [Display(Name = "Socio")]
        public string socio { get; set; }

        [Display(Name = "Terminal")]
        public int terminalID { get; set; }

        [Display(Name="")]
        public string moneda { get; set; }

        [Display(Name = "Tarjeta ID")]
        public int tarjetaID { get; set; }

        [Display(Name = "Tarjeta")]
        public string tarjeta { get; set; }

        [Display(Name = "Monto")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal monto { get; set; }

        [Display(Name = "Comisión")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal comision { get; set; }

        [Display(Name = "Iva")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal iva { get; set; }

        [Display(Name = "Comisión")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal comision1 { get; set; }

        [Display(Name = "Iva")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal iva1 { get; set; }

        [Display(Name = "Comisión")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal comision2 { get; set; }

        [Display(Name = "Iva")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal iva2 { get; set; }

        [Display(Name = "Comisión")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal comision3 { get; set; }

        [Display(Name = "Iva")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal iva3 { get; set; }

        [Display(Name = "Comisión")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal comision4 { get; set; }

        [Display(Name = "Iva")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal iva4 { get; set; }

        [Display(Name = "Comisión")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal comision5 { get; set; }

        [Display(Name = "Iva")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal iva5 { get; set; }

        [Display(Name = "Comisión")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal comision6 { get; set; }

        [Display(Name = "Iva")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal iva6 { get; set; }

        [Display(Name = "Balance")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal abonoTotal { get; set; }

        [Display(Name = "Balance")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal cargoTotal { get; set; }

        [Display(Name = "Facturable")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal facturable1 { get; set; }

        [Display(Name = "Iva")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal facturable1iva { get; set; }

        [Display(Name = "Transacción")]
        public string transaccion { get; set; }

        [Display(Name = "Concepto")]
        public string concepto { get; set; }

        [Display(Name = "Descripción")]
        public string descripcion { get; set; }

        [Display(Name = "Referencia")]
        public string referencia { get; set; }

        [Display(Name = "Estatus")]
        public string estatus { get; set; }

        [Display(Name = "Aprobación")]
        public string aprobacion { get; set; }

        [Display(Name = "IP Address")]
        public string ip { get; set; }

        [Display(Name = "Fecha")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? fechaRegistro { get; set; }
    }
}