﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enpadi_WebUI.Models
{
    [Table("SantiagoPredialConsulta")]
    public class santiagoPredialConsulta
    {
        [Key]
        [Display(Name="ID")]
        public int id { get; set; }

        [Required]
        [Display(Name="Expediente Catastral")]
        public string no_predial { get; set; }

        [Required]
        [Display(Name="Nombre")]
        public string nombre { get; set; }

        [Required]
        [Display(Name="Apellido Paterno")]
        public string paterno { get; set; }

        [Required]
        [Display(Name="Apellido Materno")]
        public string materno { get; set; }

        [Required]
        [Display(Name="Telefono")]
        public string telefono { get; set; }

        [Required]
        [Display(Name="Celular")]
        public string cel { get; set; }

        [Required]
        [Display(Name="Correo Electronico")]
        public string correo { get; set; }

        [Display(Name="Fecha de Registro")]
        public DateTime fecha_registro { get; set; }

        [ScaffoldColumn(false)]
        [NotMapped]
        [Required]
        public bool terminos { get; set; }
    }
}