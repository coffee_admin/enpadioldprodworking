﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Enpadi_WebUI.Startup))]
namespace Enpadi_WebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
