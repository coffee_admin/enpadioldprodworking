﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Enpadi_WebUI.Models;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.IO;

namespace Enpadi_WebUI
{

    #region email service

    

    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Create and Send the Mail Message
            var mailMessage = new MailMessage("info@enpadi.com", message.Destination, message.Subject, message.Body) { IsBodyHtml = true };
            var credential = new NetworkCredential() { UserName = "info@enpadi.com", Password = "93Hj!vh9" };
            SmtpClient client = new SmtpClient("enpadi.com", 25) { Credentials = credential };
            client.SendAsync(mailMessage, null);
            return Task.FromResult(true);
        }
    }

    #endregion email service

    #region sms services

    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            string sPostData = string.Format("usuario=NickBoy76&password=boy76&celular={0}&mensaje={1}", message.Destination, message.Body);
            byte[] byteArray = Encoding.UTF8.GetBytes(sPostData);

            WebRequest wRequest = WebRequest.Create("https://www.masmensajes.com.mx/wss/smsapi13.php");
            wRequest.Method = "POST";
            wRequest.ContentType = "application/x-www-form-urlencoded";
            wRequest.ContentLength = byteArray.Length;

            Stream dataStream = wRequest.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            WebResponse wResponse = wRequest.GetResponse();

            return Task.FromResult(true);
        }
    }

    #endregion sms services

    #region user manager

    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.

            //Desactiva el Registro mediante SMS
            //manager.RegisterTwoFactorProvider("Código vía SMS", new PhoneNumberTokenProvider<ApplicationUser>
            //{
            //    MessageFormat = "El Codigo de Activacion de tu Cuenta es {0}"
            //});
            //manager.SmsService = new SmsService();

            manager.RegisterTwoFactorProvider("Código vía Correo electronico", new EmailTokenProvider<ApplicationUser>
            {
                Subject = "Verificación de Cuenta",
                BodyFormat = "Tu Código de Activación es {0}"
            });
            manager.EmailService = new EmailService();

            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }

    #endregion user manager

    #region role manager

    public class ApplicationRoleManeger : RoleManager<IdentityRole>
    {

        public ApplicationRoleManeger(RoleStore<IdentityRole> store) : base(store) { }

        public static ApplicationRoleManeger Create(IOwinContext context)
        {
            var store = new RoleStore<IdentityRole>(context.Get<ApplicationDbContext>());

            return new ApplicationRoleManeger(store);
        }

    }

    #endregion role manager

    #region signIn manager

    // Configure the application sign-in manager which is used in this application.
    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }
    }

    #endregion signIn manager
}
