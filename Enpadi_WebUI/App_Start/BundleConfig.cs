﻿using System.Web;
using System.Web.Optimization;

namespace Enpadi_WebUI
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-1.12.4.js",
                        "~/Scripts/coffee.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            #region website styles

            bundles.Add(new StyleBundle("~/Content/_layout_website").Include(
                     "~/Content/_layout_website.css"));

            bundles.Add(new StyleBundle("~/Content/website_index").Include(
                     "~/Content/website_index.css"));

            bundles.Add(new StyleBundle("~/Content/website_company").Include(
                     "~/Content/website_company.css"));

            bundles.Add(new StyleBundle("~/Content/website_clients").Include(
                     "~/Content/website_clients.css"));

            bundles.Add(new StyleBundle("~/Content/website_contact").Include(
                     "~/Content/website_contact.css"));

            bundles.Add(new StyleBundle("~/Content/website_login").Include(
                     "~/Content/website_login.css"));

            bundles.Add(new StyleBundle("~/Content/website_registration").Include(
                     "~/Content/website_registration.css"));

            bundles.Add(new StyleBundle("~/Content/website_faq").Include(
                     "~/Content/website_faq.css"));

            bundles.Add(new StyleBundle("~/Content/website_terms").Include(
                     "~/Content/website_terms.css"));

            bundles.Add(new StyleBundle("~/Content/website_notice").Include(
                     "~/Content/website_notice.css"));


            bundles.Add(new StyleBundle("~/Content/website_addcard").Include(
                     "~/Content/website_addcard.css"));

            bundles.Add(new StyleBundle("~/Content/website_addcharge").Include(
                     "~/Content/website_addcharge.css"));

            bundles.Add(new StyleBundle("~/Content/website_addcustomer").Include(
                     "~/Content/website_addcustomer.css"));

            #region kiosko inside website


            bundles.Add(new StyleBundle("~/Content/website_buy").Include(
                     "~/Content/website_buy.css"));

            bundles.Add(new StyleBundle("~/Content/website_pay").Include(
                     "~/Content/website_pay.css"));

            bundles.Add(new StyleBundle("~/Content/website_recharge").Include(
                     "~/Content/website_recharge.css"));

            bundles.Add(new StyleBundle("~/Content/website_transfer").Include(
                     "~/Content/website_transfer.css"));

            bundles.Add(new StyleBundle("~/Content/website_userindex").Include(
                     "~/Content/website_userindex.css"));

            bundles.Add(new StyleBundle("~/Content/website_rewards").Include(
                     "~/Content/website_rewards.css"));


            bundles.Add(new StyleBundle("~/Content/website_processpay").Include(
                     "~/Content/website_processpay.css"));

            bundles.Add(new StyleBundle("~/Content/website_processcheckservice").Include(
                     "~/Content/website_processcheckservice.css"));

            bundles.Add(new StyleBundle("~/Content/website_processservice").Include(
                     "~/Content/website_processservice.css"));

            #endregion kiosko inside website

            #endregion website style

            #region datatable

            #region javascript


            bundles.Add(new ScriptBundle("~/bundles/openpay").Include(
                      "~/Scripts/openpay.v1.js",
                      "~/Scripts/openpay-data.v1.js"));

            bundles.Add(new ScriptBundle("~/bundles/datatable").Include(
                      "~/Scripts/jquery.dataTables.js",
                      "~/Scripts/dataTables.bootstrap.js",
                      "~/Scripts/dataTables.responsive.js",
                       "~/Scripts/dataTables.select.js"));

            bundles.Add(new ScriptBundle("~/bundles/datatableButtons").Include(
                   "~/Scripts/pdfmake.min.js",
                   "~/Scripts/dataTables.buttons.min.js",
                   "~/Scripts/jszip.min.js",
                   "~/Scripts/buttons.flash.min.js",
                   "~/Scripts/buttons.print.min.js",
                   "~/Scripts/vfs_fonts.js",
                   "~/Scripts/buttons.html5.min.js"));

            #region specific pages scripts

            bundles.Add(new ScriptBundle("~/bundles/tesapiusa").Include(
               "~/Scripts/Views/apiusa.js"));

            #endregion specific pages sripts

            #endregion javascript

            #region css




            bundles.Add(new StyleBundle("~/Content/datatables").Include(
                      "~/Content/dataTables.bootstrap.css",
                      "~/Content/responsive.bootstrap.css",
                      "~/Content/buttons.dataTables.min.css",
                      "~/Content/jquery.dataTables.css"));

            #endregion css

            #endregion datatable


            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/bootstrap").Include(
                      "~/Content/bootstrap.css"));

            bundles.Add(new StyleBundle("~/Content/LayoutNewMenu").Include(
                      "~/Content/Menu.css"));

            bundles.Add(new StyleBundle("~/Content/pay").Include(
                        "~/Content/pay.css"));

            bundles.Add(new StyleBundle("~/Content/payment").Include(
                        "~/Content/Payments.css"));

            bundles.Add(new StyleBundle("~/Content/ProcessService").Include(
            "~/Content/processService.css"));


            bundles.Add(new StyleBundle("~/Content/CheckService").Include(
            "~/Content/CheckService.css"));
            
            bundles.Add(new StyleBundle("~/Content/LayoutIndex").Include(
                    "~/Content/KioskoIndex.css"));



            // jquery currency script
            bundles.Add(new ScriptBundle("~/bundles/jquery/currency").Include(
                      "~/Scripts/jquery.formatCurrency-1.4.0.js",
                      "~/Scripts/jquery.formatCurrency-1.4.0.pack.js"));

            // Homer style
            //bundles.Add(new StyleBundle("~/bundles/homer/css").Include(
            //          "~/Content/style.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/bundles/homer/css").Include(
                     "~/Content/style.css", new CssRewriteUrlTransform()));

            // Homer script
            bundles.Add(new ScriptBundle("~/bundles/homer/js").Include(
                      "~/Vendor/metisMenu/dist/metisMenu.min.js",
                      "~/Vendor/iCheck/icheck.min.js",
                      "~/Vendor/peity/jquery.peity.min.js",
                      "~/Vendor/sparkline/index.js",
                      "~/Scripts/homer.js",
                      "~/Scripts/charts.js"));

            // Animate.css
            bundles.Add(new StyleBundle("~/bundles/animate/css").Include(
                      "~/Vendor/animate.css/animate.min.css"));

            // Pe-icon-7-stroke
            bundles.Add(new StyleBundle("~/bundles/peicon7stroke/css").Include(
                      "~/Icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css", new CssRewriteUrlTransform()));

            // Font Awesome icons style
            bundles.Add(new StyleBundle("~/bundles/font-awesome/css").Include(
                      "~/Vendor/fontawesome/css/font-awesome.min.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/bundles/font-awesome-4.7/css").Include(
                      "~/Vendor/fontawesome/css/font-awesome-4.7.0.min.css", new CssRewriteUrlTransform()));

            // Bootstrap style
            bundles.Add(new StyleBundle("~/bundles/bootstrap/css").Include(
                      "~/Vendor/bootstrap/dist/css/bootstrap.min.css", new CssRewriteUrlTransform()));

            // Bootstrap
            bundles.Add(new ScriptBundle("~/bundles/bootstrap/js").Include(
                      "~/Vendor/bootstrap/dist/js/bootstrap.min.js"));

            // jQuery
            bundles.Add(new ScriptBundle("~/bundles/jquery/js").Include(
                      "~/Vendor/jquery/dist/jquery.min.js"));

            // jQuery UI
            bundles.Add(new ScriptBundle("~/bundles/jqueryui/js").Include(
                      "~/Vendor/jquery-ui/jquery-ui.min.js"));

            // Flot chart
            bundles.Add(new ScriptBundle("~/bundles/flot/js").Include(
                      "~/Vendor/flot/jquery.flot.js",
                      "~/Vendor/flot/jquery.flot.tooltip.min.js",
                      "~/Vendor/flot/jquery.flot.resize.js",
                      "~/Vendor/flot/jquery.flot.pie.js",
                      "~/Vendor/flot.curvedlines/curvedLines.js",
                      "~/Vendor/jquery.flot.spline/index.js"));

            // Star rating
            bundles.Add(new ScriptBundle("~/bundles/starRating/js").Include(
                      "~/Vendor/bootstrap-star-rating/js/star-rating.min.js"));

            // Star rating style
            bundles.Add(new StyleBundle("~/bundles/starRating/css").Include(
                      "~/Vendor/bootstrap-star-rating/css/star-rating.min.css", new CssRewriteUrlTransform()));

            // Sweetalert
            bundles.Add(new ScriptBundle("~/bundles/sweetAlert/js").Include(
                      "~/Vendor/sweetalert/lib/sweet-alert.min.js"));

            // Sweetalert style
            bundles.Add(new StyleBundle("~/bundles/sweetAlert/css").Include(
                      "~/Vendor/sweetalert/lib/sweet-alert.css"));

            // Toastr
            bundles.Add(new ScriptBundle("~/bundles/toastr/js").Include(
                      "~/Vendor/toastr/build/toastr.min.js"));

            // Toastr style
            bundles.Add(new StyleBundle("~/bundles/toastr/css").Include(
                      "~/Vendor/toastr/build/toastr.min.css"));

            // Nestable
            bundles.Add(new ScriptBundle("~/bundles/nestable/js").Include(
                      "~/Vendor/nestable/jquery.nestable.js"));

            // Toastr
            bundles.Add(new ScriptBundle("~/bundles/bootstrapTour/js").Include(
                      "~/Vendor/bootstrap-tour/build/js/bootstrap-tour.min.js"));

            // Toastr style
            bundles.Add(new StyleBundle("~/bundles/bootstrapTour/css").Include(
                      "~/Vendor/bootstrap-tour/build/css/bootstrap-tour.min.css"));

            // Moment
            bundles.Add(new ScriptBundle("~/bundles/moment/js").Include(
                      "~/Vendor/moment/moment.js"));

            // Full Calendar
            bundles.Add(new ScriptBundle("~/bundles/fullCalendar/js").Include(
                      "~/Vendor/fullcalendar/dist/fullcalendar.min.js"));

            // Full Calendar style
            bundles.Add(new StyleBundle("~/bundles/fullCalendar/css").Include(
                      "~/Vendor/fullcalendar/dist/fullcalendar.min.css"));

            // Chart JS
            bundles.Add(new ScriptBundle("~/bundles/chartjs/js").Include(
                      //"~/Vendor/chartjs/Chart.min.js",
                      "~/Vendor/chartjs/dist/Chart.bundle.js"));

            // Datatables
            bundles.Add(new ScriptBundle("~/bundles/datatables/js").Include(
                      "~/Vendor/datatables/media/js/jquery.dataTables.min.js"));

            // Datatables bootstrap
            bundles.Add(new ScriptBundle("~/bundles/datatablesBootstrap/js").Include(
                      "~/Vendor/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"));

            // Datatables style
            bundles.Add(new StyleBundle("~/bundles/datatables/css").Include(
                      "~/Vendor/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.css"));

            // Xeditable
            bundles.Add(new ScriptBundle("~/bundles/xeditable/js").Include(
                      "~/Vendor/xeditable/bootstrap3-editable/js/bootstrap-editable.min.js"));

            // Xeditable style
            bundles.Add(new StyleBundle("~/bundles/xeditable/css").Include(
                      "~/Vendor/xeditable/bootstrap3-editable/css/bootstrap-editable.css", new CssRewriteUrlTransform()));

            // Select 2
            bundles.Add(new ScriptBundle("~/bundles/select2/js").Include(
                      "~/Vendor/select2-3.5.2/select2.min.js"));

            // Select 2 style
            bundles.Add(new StyleBundle("~/bundles/select2/css").Include(
                      "~/Vendor/select2-3.5.2/select2.css",
                      "~/Vendor/select2-bootstrap/select2-bootstrap.css"));

            // Touchspin
            bundles.Add(new ScriptBundle("~/bundles/touchspin/js").Include(
                      "~/Vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"));

            // Touchspin style
            bundles.Add(new StyleBundle("~/bundles/touchspin/css").Include(
                      "~/Vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css"));

            // Datepicker
            bundles.Add(new ScriptBundle("~/bundles/datepicker/js").Include(
                      "~/Vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js"));

            // Datepicker style
            bundles.Add(new StyleBundle("~/bundles/datepicker/css").Include(
                      "~/Vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css"));

            // Datepicker
            bundles.Add(new ScriptBundle("~/bundles/summernote/js").Include(
                      "~/Vendor/summernote/dist/summernote.min.js"));

            // Datepicker style
            bundles.Add(new StyleBundle("~/bundles/summernote/css").Include(
                      "~/Vendor/summernote/dist/summernote.css",
                      "~/Vendor/summernote/dist/summernote-bs3.css"));

            // Bootstrap checkbox style
            bundles.Add(new StyleBundle("~/bundles/bootstrapCheckbox/css").Include(
                      "~/Vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css"));

            // Blueimp gallery
            bundles.Add(new ScriptBundle("~/bundles/blueimp/js").Include(
                      "~/Vendor/blueimp-gallery/js/jquery.blueimp-gallery.min.js"));

            // Blueimp gallery style
            bundles.Add(new StyleBundle("~/bundles/blueimp/css").Include(
                      "~/Vendor/blueimp-gallery/css/blueimp-gallery.min.css", new CssRewriteUrlTransform()));



        }
    }
}
