﻿using AutoMapper;
using Enpadi_WebUI.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;


namespace Enpadi_WebUI.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            Mapper.CreateMap<ApplicationUser, IdentityUser>();
            Mapper.CreateMap<IdentityUser, ApplicationUser>();
        }
    }
}