﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Enpadi_WebUI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            //this apply for enpadi usa
            /******************************************************************/
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //this route is for the skin problem, if there is no skin, clean skin is activated
            routes.MapRoute(
                name: "NoRoute",
                url: "",
                defaults: new { controller = "website", action = "RedirectInformation" }
            );

            //this route is for the skin problem, if there is no skin, clean skin is activated
            routes.MapRoute(
                name: "noaction",
                url: "website",
                defaults: new { controller = "website", action = "RedirectInformation" }
            );

            //this route is for the skin problem, if there is no skin, clean skin is activated
            routes.MapRoute(
                name: "onlycontroller",
                url: "{controller}",
                defaults: new { action = "Index" }
            );

            //this route is for the skin problem, if there is no skin, clean skin is activated
            routes.MapRoute(
                name: "General",
                url: "{controller}/{action}",
                defaults: new { skin = "Clean", controller = "Home", action = "Index" }
            );

            //this route is for the skin problem, if there is no skin, clean skin is activated
            routes.MapRoute(
                name: "OxxoWebHook",
                url: "api/{controller}/{action}",
                defaults: new { skin = "Clean", controller = "Conekta", action = "Post" }

            );

            //this is the defualt route, skin most be declared
            routes.MapRoute(
                name: "Default",
                url: "{skin}/{controller}/{action}/{id}",
                defaults: new { skin = "Clean", controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            /*************************************************************************************************/

            //this apply to enpadi mexico
            /****************************************************************************************************/
            //routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            ////this route is for the skin problem, if there is no skin, clean skin is activated
            //routes.MapRoute(
            //    name: "NoRoute",
            //    url: "",
            //    defaults: new
            //    {
            //        skin = "Clean",
            //        controller = "Home",
            //        action = "Index"
            //    }
            //);

            ////this route is for the skin problem, if there is no skin, clean skin is activated
            //routes.MapRoute(
            //    name: "General",
            //    url: "{controller}/{action}",
            //    defaults: new { skin = "Clean", controller = "Home", action = "Index" }
            //);

            ////this route is for the skin problem, if there is no skin, clean skin is activated
            //routes.MapRoute(
            //    name: "OxxoWebHook",
            //    url: "api/{controller}/{action}",
            //    defaults: new { skin = "Clean", controller = "Conekta", action = "Post" }

            //);

            ////this is the defualt route, skin most be declared
            //routes.MapRoute(
            //    name: "Default",
            //    url: "{skin}/{controller}/{action}/{id}",
            //    defaults: new { skin = "Clean", controller = "Home", action = "Index", id = UrlParameter.Optional }
            //);

            /*************************************************************/
        }
    }
}
