﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace conekta
{
    public class SecurityKeys
    {
        public const String Enpadi_conekta_publickey = "key_fzErazyUyYXGAgtkyxC2yqg";
        public const String Enpadi_conekta_privatekey = "key_y7jYr3k5FzEAsL7FqR1Uog";
        public const String Test_enpadi_conekta_publickey = "key_JkN9yT1zY2NJCZ2pTGho8sA";
        public const String Test_enpadi_conekta_privatekey = "key_kLZVdzRSAPPzsZzJqzRqWw";
        public const String Conekta_version = "2.0.0";
        public const String Conekta_lang_es = ":es";
        public const String Conekta_payment_method_type = "oxxo_cash";
        public const String Conekta_currency_MXN = "MXN";
    }
}
